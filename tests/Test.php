<?php
use Goutte\Client;

/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 19/07/16
 * Time: 20:42
 */
class Test extends TestCase
{
    public function testgenerateKeys()
    {
        $config = array(
            "digest_alg" => "sha512",
            "private_key_bits" => 4096,
            "private_key_type" => OPENSSL_KEYTYPE_RSA,
        );

        // Create the private and public key
        $res = openssl_pkey_new($config);

        // Extract the private key from $res to $privKey
        openssl_pkey_export($res, $privKey);
        echo 'PRIVADA ' . $privKey;
        // Extract the public key from $res to $pubKey
        $pubKey = openssl_pkey_get_details($res);
        $pubKey = $pubKey["key"];
        echo 'PUBLICA ' . $pubKey;
        $data = 'plaintext data goes here';

        // Encrypt the data to $encrypted using the public key
        openssl_public_encrypt($data, $encrypted, $pubKey);

        // Decrypt the data using the private key and store the results in $decrypted
        openssl_private_decrypt($encrypted, $decrypted, $privKey);

        echo $decrypted;
    }

    public function testindeedCV()
    {
        $name = "Jorge Amor Morales";
        $place = "Madrid";
        $client = new Client();
        $html_web_search = $client->request('GET', "http://www.indeed.com/resumes?q=%22Jorge+Amor+Morales%22&l=28008");
        $num_people_find = $html_web_search->filter('.clickable_resume_card')->count();
        if ($num_people_find == 1) {
            $html_web_search->filter('.clickable_resume_card')->each(function ($node) {
                $popup = $node->attr('onclick');
                $url_without_back = substr($popup, 0, -12);
                $url_cv = 'http://www.indeed.com' . substr($url_without_back, 13);
                $cv_client = new Client();
                $html_cv = $cv_client->request('GET', $url_cv);
                $html_cv->filter('#education-items')->each(function ($node) {
                    print $node->text();
                });
            });
        } else if ($num_people_find > 1) {
            //TODO ha encontrado a más de uno
        } else {
            echo '0 encontrados';
        }


    }


    public function testjobmas()
    {

        $client = new Client();
        $html_web_search = $client->request('GET', "http://es.jobomas.com/b/abraham_garcia_hernandez-Tipo_personas");
        $num_search = $html_web_search->filter('#resultSearch_1')->count();
        if ($num_search > 1) {
            //TODO MAS DE UN RESULTADO
        } else if ($num_search == 1) {
            echo $html_web_search->filter('article > a')->first()->attr('href');
        } else {
            echo '0 encontrados';
        }

    }


    public function testeinfo()
    {
        $nif = 'B86532348'; //SIRACUSA
        $name = 'CYPREA';
        $html_web = 'https://www.einforma.com/servlet/app/prod/LISTA_EMPRESAS/razonsocial/' . $name;
//        $client = new Client(['cookies' => true]);
//        $guzzleClient = new \GuzzleHttp\Client(array(
//            'request.options' => array(
////                'proxy' => '217.61.0.175:1718'
//            ),
//            'curl' => array(
//                CURLOPT_SSL_VERIFYPEER => false,
//            ),
//        ));
//        $client->setClient($guzzleClient);
        $client = new Client();
        $guzzleClient = new \GuzzleHttp\Client(array(
            'timeout' => 60,
        ));
        $client->setClient($guzzleClient);
        $crawler = $client->request('GET', 'https://www.einforma.com/servlet/app/prod/ACCESO/');
        $form = $crawler->filter('#form_acceder')->form();
        $crawler = $client->submit($form, array('username_login' => 'presidencia@vinoloa.com', 'pass_login' => '00001962'));
        var_dump($crawler->html());

//        $html_search =$client->request('POST', 'https://www.einforma.com/servlet/app/screen/SProductoAJAX/prod/LOGIN_XML/', [
//            'headers' => [
//                'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.92 Safari/537.36',
//                'Accept'     => 'gzip, deflate, br',
//                'x-requested-with' => 'XMLHttpRequest'
//            ],
//            'body' => 'username=presidencia@vinoloa.com&password=00001962&recordar=false'
//        ]);
//
//        ->setHeaders(array(
//        'postman-token' => '004eb5e0-3d80-f8eb-d6e0-8def2611afd4',
//        'cache-control' => 'no-cache',
//        'connection' => 'keep-alive',
//        'x-requested-with' => 'XMLHttpRequest',
//        'referer' => 'https://www.einforma.com/',
//        'accept' => 'application/xml, text/xml, */*; q=0.01',
//        'content-type' => 'application/x-www-form-urlencoded; charset=UTF-8',
//        'user-agent' => '',
//        'accept-language' => 'es,en-GB;q=0.8,en;q=0.6',
//        'accept-encoding' => '',
//        'origin' => 'https://www.einforma.com',
//        'cookie' => 'COOK_USUARIO=TP_PENTP#; G_ENABLED_IDPS=google; JSESSIONID=node1~22F3A6A8DCD062F63D5A30859FB21E05.web1-esp_tomcat3; _gat=1; _ga=GA1.2.904213988.1473158113; linkedin_oauth_772jvgocyzqg0w=null; linkedin_oauth_772jvgocyzqg0w_crc=null'
//    ));

        $html_search = $client->request('GET', $html_web);
        $html = $html_search->filter('#nacional > tbody > tr')->attr('url');
        $html_web_search = $client->request('GET', 'https://www.einforma.com/' . $html);
        $info = new \stdClass();
        $html_web_search->filter('#datos > tr')->each(function ($node) use ($info) {
            echo $node->text(), PHP_EOL;
            if (stristr($node->text(), 'CNAE 2009:')) {
                $info->cnae = trim(str_replace('CNAE 2009:', ' ', $node->text()));
            }
//            if(stristr($node->text(),'Último Balance cargado:')) {
//                $info->ultimo_balance_cargado = trim(str_replace('Último Balance cargado:',' ',$node->text()));
//            }
            if (stristr($node->text(), 'Forma Jurídica:')) {
                $info->forma_juridica = trim(str_replace('Forma Jurídica:', ' ', $node->text()));
            }
            if (stristr($node->text(), 'Actividad Informa:')) {
                $info->actividad_1 = trim(str_replace('Actividad Informa:', ' ', $node->text()));
            }
//            if(stristr($node->text(),'Balances disponibles:')) {
//                $info->balances_disp = trim(str_replace('Balances disponibles:',' ',$node->text()));
//            }
            if (stristr($node->text(), 'Objeto Social:')) {
                $info->obj_social = trim(str_replace('Objeto Social:', ' ', $node->text()));
            }
            if (stristr($node->text(), 'Depósito en R. Mercantil:')) {
                $info->deposito_r_mercantil = trim(str_replace('Depósito en R. Mercantil:', ' ', $node->text()));
            }
            if (stristr($node->text(), 'Popularidad:')) {
                $info->trafico = trim(str_replace(' y ', ' / ', str_replace('veces en total', '', str_replace('Popularidad:Esta empresa ha sido consultada por última vez el ', ' ', $node->text()))));
            }

        });
        if (empty($info)) {
            echo 'No encontrado';
        } else {
//            var_dump($info, true);
        }
    }


    public function testm(){
        $ult_dig= '02';
        $array = array("012345",
            "012346",
            "012347",
            "012348",
            "012349",
            "012356",
            "012357",
            "012358",
            "012359",
            "012367",
            "012368",
            "012369",
            "012378",
            "012379",
            "012389",
            "012456",
            "012457",
            "012458",
            "012459",
            "012467",
            "012468",
            "012469",
            "012478",
            "012479",
            "012489",
            "012567",
            "012568",
            "012569",
            "012578",
            "012579",
            "012589",
            "012678",
            "012679",
            "012689",
            "012789",
            "013456",
            "013457",
            "013458",
            "013459",
            "013467",
            "013468",
            "013469",
            "013478",
            "013479",
            "013489",
            "013567",
            "013568",
            "013569",
            "013578",
            "013579",
            "013589",
            "013678",
            "013679",
            "013689",
            "013789",
            "014567",
            "014568",
            "014569",
            "014578",
            "014579",
            "014589",
            "014678",
            "014679",
            "014689",
            "014789",
            "015678",
            "015679",
            "015689",
            "015789",
            "016789",
            "023456",
            "023457",
            "023458",
            "023459",
            "023467",
            "023468",
            "023469",
            "023478",
            "023479",
            "023489",
            "023567",
            "023568",
            "023569",
            "023578",
            "023579",
            "023589",
            "023678",
            "023679",
            "023689",
            "023789",
            "024567",
            "024568",
            "024569",
            "024578",
            "024579",
            "024589",
            "024678",
            "024679",
            "024689",
            "024789",
            "025678",
            "025679",
            "025689",
            "025789",
            "026789",
            "034567",
            "034568",
            "034569",
            "034578",
            "034579",
            "034589",
            "034678",
            "034679",
            "034689",
            "034789",
            "035678",
            "035679",
            "035689",
            "035789",
            "036789",
            "045678",
            "045679",
            "045689",
            "045789",
            "046789",
            "056789",
            "123456",
            "123457",
            "123458",
            "123459",
            "123467",
            "123468",
            "123469",
            "123478",
            "123479",
            "123489",
            "123567",
            "123568",
            "123569",
            "123578",
            "123579",
            "123589",
            "123678",
            "123679",
            "123689",
            "123789",
            "124567",
            "124568",
            "124569",
            "124578",
            "124579",
            "124589",
            "124678",
            "124679",
            "124689",
            "124789",
            "125678",
            "125679",
            "125689",
            "125789",
            "126789",
            "134567",
            "134568",
            "134569",
            "134578",
            "134579",
            "134589",
            "134678",
            "134679",
            "134689",
            "134789",
            "135678",
            "135679",
            "135689",
            "135789",
            "136789",
            "145678",
            "145679",
            "145689",
            "145789",
            "146789",
            "156789",
            "234567",
            "234568",
            "234569",
            "234578",
            "234579",
            "234589",
            "234678",
            "234679",
            "234689",
            "234789",
            "235678",
            "235679",
            "235689",
            "235789",
            "236789",
            "245678",
            "245679",
            "245689",
            "245789",
            "246789",
            "256789",
            "345678",
            "345679",
            "345689",
            "345789",
            "346789",
            "356789",
            "456789");

        foreach ($array as $d){
            echo '6'.$d.'02',PHP_EOL;
        }


    }

    public function testBasura(){
        var_dump(sha1("gBGx8cwAOOwEXrEWyyEQHe4HpSfr4vGXUhDWjOkS"));
        var_dump(sha1("OfjbZsHGcVRajirXj3fxjOYLRqNO4IAKksZ9MKHk"));
    }

}
