<?php
use App\Http\Controllers\Proxies\Proxies;
use Symfony\Component\DomCrawler\Crawler;

class MilanunciosTest extends TestCase
{

    private $tlf, $nombre_via;

    public function testMa()
    {
        $this->tlf = '669554336';
        $this->nombre_via = 'ALTAMIRANO';
        var_dump($this->search());
    }

//    public function run()
//    {
//        parent::run();
//        $this->worker->addData('milanuncios', $this->search());
//    }

    private function search()
    {
        $milanuncio = new stdClass();
        $milanuncio->score_vivienda = 'OK';
        $milanuncio->score_trabajo = 'OK';

        $milanuncio->hits = [];
        $milanuncio->anuncio = array();
        $crawler = new Crawler();
        $page = 1;
        do {
            $crawler->clear();
            $html_web = 'http://www.milanuncios.com/anuncios/' . $this->tlf . ".htm?pagina=" . $page;
            if ($resp = $this->curl($html_web)) {
                $crawler->addHtmlContent($resp);
            }
            $titulos = array();
            $crawler->filter('#cuerpo > .aditem')->each(function ($node) use ($milanuncio, $titulos) {
                $titulo = $node->filter('.aditem-detail > .aditem-detail-title')->first()->text();
                $url = $node->filter('.aditem-detail > .aditem-detail-title')->first()->attr('href');
                if (!in_array(trim($titulo), $titulos)) {
                    $titulos[] = trim($titulo);
                    $anuncio = new \stdClass();
                    $anuncio->titulo = trim($titulo);
                    $anuncio->url = trim('http://www.milanuncios.com' . $url);
                    $milanuncio->anuncio[] = $anuncio;
                }
            });
            $page++;
        } while ($crawler->filter('#cuerpo > .aditem')->count() > 0);
        if (count($milanuncio->anuncio) > 0) {
            foreach ($milanuncio->anuncio as $anuncio) {
                //vaciamos crawler
                $crawler->clear();
                if ($resp = $this->curl($anuncio->url)) {
                    $crawler->addHtmlContent($resp);
                    if ($crawler->filter('.contenido > .contentInnerBoxPagAnu > #cuerpo > .pagAnuRightColumn > .pagAnuContact >.pagAnuContactNombre')->count() > 0) {
                        $anuncio->alias = trim($crawler->filter('.contenido > .contentInnerBoxPagAnu > #cuerpo > .pagAnuRightColumn > .pagAnuContact >.pagAnuContactNombre')->first()->text());
                    }
                    unset($anuncio->url);
                    if ($crawler->filter('.pagAnuSubtitle > .pagAnuAdType')->count() > 0) {
                        $anuncio->type = trim($crawler->filter('.pagAnuSubtitle > .pagAnuAdType')->text());
                    }
                    $cat_sub = $crawler->filter('.contenido > .cab > .beacrumb-container > .beacrumb > a');
                    if ($cat_sub->count() >= 3) {
                        $anuncio->category = $cat_sub->eq(1)->text();
                        $anuncio->subcategory = $cat_sub->eq(2)->text();
                    } elseif ($cat_sub->count() == 2) {
                        $anuncio->category = $cat_sub->eq(1)->text();
                    } else {
                        continue;
                    }
                    if ($crawler->filter('.pagAnuCuerpoAnu')->count() > 0) {
                        $anuncio->cuerpo = trim($crawler->filter('.pagAnuCuerpoAnu')->text());
                    }
                    if ($crawler->filter('.pagAnuPrecioTexto')->count() > 0) {
                        $anuncio->price = str_replace('.', '', trim(str_replace($crawler->filter('.pagAnuPrecioTexto > sup')->text(), '', $crawler->filter('.pagAnuPrecioTexto')->text())));
                    }

                    if ((isset($anuncio->price) && $anuncio->price > 3000) &&
                        ((isset($anuncio->cuerpo) && (stristr($anuncio->cuerpo, $this->nombre_via) ||
                                    stristr($anuncio->cuerpo, mb_strtolower($this->nombre_via)))) ||
                            (isset($anuncio->titulo) && stristr($anuncio->titulo, mb_strtolower($this->nombre_via)) ||
                                stristr($anuncio->titulo, mb_strtolower($this->nombre_via))))
                    ) {
                        $milanuncio->score_vivienda = 'KO';
                    }
                    if ((isset($anuncio->type) && mb_strtoupper($anuncio->type) == 'DEMANDA') &&
                        (isset($anuncio->category) && mb_strtoupper($anuncio->category) == 'EMPLEO')
                    ) {
                        $milanuncio->score_trabajo = 'KO';
                    }

                    if (isset($anuncio->cuerpo)) unset($anuncio->cuerpo);
                    $milanuncio->hits = $milanuncio->anuncio;
                }
            }
        }
        unset($milanuncio->anuncio);

        return $milanuncio;
    }


    private
    function curl($page)
    {
        $obj_proxy = new Proxies('milanuncios');
        $ip_real = false;
        do {
            $ip = $obj_proxy->get();
            $obj_proxy->setUsed($ip);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $page);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36');
            if ($ip) {
                curl_setopt($ch, CURLOPT_PROXY, $ip);
                curl_setopt($ch, CURLOPT_PROXYUSERPWD, "mrhc:QwEr1234");
            } else  $ip_real = true;
            $result = curl_exec($ch);
            $info = curl_getinfo($ch);
            curl_close($ch);
            if (!$ip_real) {
                $obj_proxy->unlock($info["http_code"]);
            }
        } while ($info["http_code"] == 400 && !$ip_real);
        if ($info["http_code"] == 400 && $ip_real) return false;
        return $result;
    }

//    public function testMilanuncios()
//    {
//        if (($archivo = fopen('Template 10K POC.csv', 'r')) !== FALSE) {
//            while (($datos = fgetcsv($archivo, 1000, ";")) !== FALSE) {
//                set_time_limit(0);
//                $this->tlf = trim($datos[6]);
//                var_dump($this->tlf);
//                $resp = $this->search();
//                file_put_contents('milanuncios.csv', $datos[4] . ';' . json_encode($resp) . PHP_EOL, FILE_APPEND);
//
//            }
//        }
//    }
//
//    private function search()
//    {
//        if (!($result = $this->search_norm())) {
//            $status = 400;
//            $result = 'No encontrado';
//        } else {
//            $status = 200;
//        }
//        return ['hits' => $result, 'status' => $status];
//    }
//
//    private function search_norm()
//    {
//        $html_web = 'http://www.milanuncios.com/anuncios/' . $this->tlf . ".htm";
//        Log::debug($html_web);
//        $milanuncio = new \stdClass();
//        $milanuncio->anuncio = array();
//        $html_search = new Crawler();
//        if (!($web_url = $this->curl($html_web))) return false;
//        $html_search->addHtmlContent($web_url);
//        $titulos = array();
//        $html_search->filter('#cuerpo > .aditem')->each(function ($node) use ($milanuncio, $titulos) {
//            $titulo = $node->filter('.aditem-detail > .aditem-detail-title')->first()->text();
//            $url = $node->filter('.aditem-detail > .aditem-detail-title')->first()->attr('href');
//            if (!in_array(trim($titulo), $titulos)) {
//                $titulos[] = trim($titulo);
//                $anuncio = new \stdClass();
//                $anuncio->titulo = trim($titulo);
//                $anuncio->url = trim('http://www.milanuncios.com' . $url);
//                $milanuncio->anuncio[] = $anuncio;
//            }
//        });
//        if (count($milanuncio->anuncio) > 0) {
//            foreach ($milanuncio->anuncio as $anuncio) {
//                unset($crawler);
//                $crawler = new Crawler();
//                if (!($anuncio_url = $this->curl($anuncio->url))) continue;
//                $crawler->addHtmlContent($anuncio_url);
//                //Tipo PROFESIONAL O PARTICULAR y Nombre
//
//                if ($crawler->filter('.contenido > .contentInnerBoxPagAnu > #cuerpo > .pagAnuRightColumn > .pagAnuContact >.pagAnuContactNombre')->count() > 0) {
//                    $anuncio->contact_name = $crawler->filter('.contenido > .contentInnerBoxPagAnu > #cuerpo > .pagAnuRightColumn > .pagAnuContact >.pagAnuContactNombre')->first()->text();
////                    $cmp_name = $this->nombre . ' ' . $this->ape1;
////                    similar_text(mb_strtoupper($anuncio->contact_name, 'UTF-8'), $cmp_name, $percent);
////                    $anuncio->correlacion = round($percent, PHP_ROUND_HALF_EVEN) . '%';
//
//                }
////                if($crawler->filter('.contenido > .contentInnerBoxPagAnu > #cuerpo > .pagAnuRightColumn >.pagAnuContact > div > .pagAnuContactSellerType')->count() > 0) {
////                    $anuncio->contact_type = $crawler->filter('.contenido > .contentInnerBoxPagAnu > #cuerpo > .pagAnuRightColumn >.pagAnuContact > div > .pagAnuContactSellerType')->first()->text();
////                }
//
//                //Quitamos urls
////                unset($anuncio->url);
//
//                $cat_sub = $crawler->filter('.contenido > .cab > .beacrumb-container > .beacrumb > a');
//                if ($cat_sub->count() >= 3) {
//                    $anuncio->categoria = $cat_sub->eq(1)->text();
//                    $anuncio->subcategoria = $cat_sub->eq(2)->text();
//                } elseif ($cat_sub->count() == 2) {
//                    $anuncio->categoria = $cat_sub->eq(1)->text();
//                } else {
//                    continue;
//                }
//            }
//            return $milanuncio;
//        } else {
//            //No se encontraron anuncios
//            return false;
//        }
//    }
//
//    private function curl($page)
//    {
////        $obj_proxy = new Proxies('milanuncios');
////        $ip_real = false;
////        do {
////            $ip = $obj_proxy->get();
////            $obj_proxy->setUsed($ip);
//            $ch = curl_init();
//            curl_setopt($ch, CURLOPT_URL, $page);
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
////            if ($ip) {
//                curl_setopt($ch, CURLOPT_PROXY, '37.230.135.15:80');
//                curl_setopt($ch, CURLOPT_PROXYUSERPWD, "mrhc:QwEr1234");
////            } else  $ip_real = true;
//            $result = curl_exec($ch);
//            $info = curl_getinfo($ch);
//            curl_close($ch);
////            if (!$ip_real) {
////                $obj_proxy->unlock($info["http_code"]);
////            }
////        } while ($info["http_code"] != 200 && !$ip_real);
//        if ($info["http_code"] != 200) return false;
//        return $result;
//    }

    public function testRiccirado()
    {
        $full_name = 'SAN LUIS MOURE ALEJANDRO';
        $preps_apellidos = ["de", "del", "la", "las", "los", "san", "y"];
        $full_name = mb_strtolower(str_replace('*', ' ', $full_name));
        $explode_full_name = explode(' ', $full_name);
        $first_name_pre = $last_name_pre = false;
        $first_name = $last_name = $name = '';
        $i = 0;
        $c_explode_ini = count($explode_full_name);
        do {
//            var_dump($explode_full_name);
//            var_dump($i);
            if ($i == 0) {
                if (in_array($explode_full_name[$i], $preps_apellidos)) {
                    $first_name = $explode_full_name[$i] . ' ' . $explode_full_name[$i + 1];
                    unset($explode_full_name[$i]);
                    unset($explode_full_name[$i + 1]);
                    $i = $i + 2;
                    $first_name_pre = true;
                } else {
                    $first_name = $explode_full_name[$i];
                    unset($explode_full_name[$i]);
                    $i++;
                }

            } elseif ((!$first_name_pre && $i > 0 && $i < 2) || ($first_name_pre && $i > 0 && $i < 3) && ($c_explode_ini >= 3)) {
                if ($c_explode_ini == 2 || $c_explode_ini <= 3 && $first_name_pre) {
                    $name = $explode_full_name[$i];
                    break;
                } else {
                    if (in_array($explode_full_name[$i], $preps_apellidos)) {
                        $last_name = $explode_full_name[$i] . ' ' . $explode_full_name[$i + 1];
                        unset($explode_full_name[$i]);
                        unset($explode_full_name[$i + 1]);
                        $i = $i + 2;
                        $last_name_pre = true;
                    } else {
                        $last_name = $explode_full_name[$i];
                        unset($explode_full_name[$i]);
                        $i++;
                    }

                }
            } else {
                $name .= ' ' . $explode_full_name[$i];
                unset($explode_full_name[$i]);
                $i++;
            }
        } while ($i < $c_explode_ini);
//        https://api.genderize.io/?name=maria+jose
        var_dump('NOMBRE ' . trim($name));
        var_dump('1 APELLIDO ' . $first_name);
        var_dump('2 APELLIDO ' . $last_name);

    }
}
