<?php

/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 16/07/16
 * Time: 12:37
 */
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverExpectedCondition;
use Facebook\WebDriver\WebDriverKeys;
use Goutte\Client;

class FacebookTest extends TestCase
{

    const BASE_URI = 'https://www.facebook.com/';
    const QUERY = ['__a' => 1];
    const RECOVER = 'login/identify?ctx=recover';
    const TAG_ENDPOINT = '/explore/tags/%s';
    const LOCATION_ENDPOINT = '/explore/locations/%d';
    const USER_ENDPOINT = '/%s';
    const MEDIA_ENDPOINT = '/p/%s';
    const SEARCH_ENDPOINT = '/web/search/topsearch';
    const SEARCH_CONTEXT_PARAM = 'blended';
    /**
     * @var ClientInterface
     */
    private $client;
    /**
     * Initializes a new object
     */
//    public function __construct()
//    {
//        $this->client = new Client([
//            'base_uri' => self::BASE_URI,
//            'query' => self::QUERY,
//        ]);
//    }
    public function testPost(){
        $curl = curl_init();
//                "cookie: sb=ETnNVylEmuhpj6ZA79UONJ1M; lu=gAmogFeeoY2ito9EXTbmmwAw; reg_fb_gate=https%3A%2F%2Fwww.facebook.com%2F; reg_ext_ref=https%3A%2F%2Fwww.google.es%2F; datr=DznNV57bJz10fVZ-FwEiONcT; sfiu=AYi_1Dk9uOe-8GO5gvcMJSi6je-XULaMvAHliLwo6a5YsgKln_Pkl4-vn_7OJwAmJvIBw3GJeaGyxilTTJ3zWXVdg78J0Z4F-i6TO1-5aOiEovB9yd0YFkO8g4o0_jJZDhzJrSSxborRlnl2uy3lS7lbRStXH2zu6eHUlq5Oz3n-8g; fr=0fY4mdX04vFvhYYXq.AWUkFOpWvB_sma-S0qOBLqS18cE.BXyDvp.e7.AAA.0.0.BYU8Df.AWVnC7a_; reg_fb_ref=https%3A%2F%2Fwww.facebook.com%2Flogin%2Fidentify%3Fctx%3Drecover; act=1481884155176%2F3",

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://www.facebook.com/ajax/login/help/identify.php?ctx=recover&dpr=1",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "lsd=AVoVc2wb&email=615896871&did_submit=Buscar&__user=0&__a=1",
            CURLOPT_HTTPHEADER => array(
                "accept-language: es,en-GB;q=0.8,en;q=0.6",
                "authority: www.facebook.com",
                "cookie: datr=F9pTWBjhwxsR3jW5-gjeuIR1;",
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded",
                "origin: https://www.facebook.com",
                "referer: https://www.facebook.com/login/identify?ctx=recover",
                "user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.90 Safari/537.36"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $info = curl_getinfo($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
           // var_dump($info);
            $response = str_replace('for (;;);','',$response);
            var_dump($response);
            $response = json_decode($response);
            var_dump($response);
            $response = trim(str_replace('window.location.href="','',$response->onload[0]));
            $response =str_replace('\\','',$response);
            $response =str_replace('"','',$response);
            var_dump($response);
            $this->recovery("https://www.facebook.com".$response);

        }
    }
    public function recovery($url){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
                "accept-language: es,en-GB;q=0.8,en;q=0.6",
                "authority: www.facebook.com",
                "cache-control: no-cache",
                "cookie: sb=ETnNVylEmuhpj6ZA79UONJ1M; lu=gAmogFeeoY2ito9EXTbmmwAw; reg_fb_gate=https%3A%2F%2Fwww.facebook.com%2F; reg_ext_ref=https%3A%2F%2Fwww.google.es%2F; datr=DznNV57bJz10fVZ-FwEiONcT; fr=0fY4mdX04vFvhYYXq.AWX3oe7djZk5Qc5l_u46Mqf73dI.BXyDvp.e7.AAA.0.0.BYU8-I.AWUccaZ9; reg_fb_ref=https%3A%2F%2Fwww.facebook.com%2Flogin%2Fidentify%3Fctx%3Drecover; sfiu=AYg9j3NdmxZ4NuglWAPBQ6miTukyNg_gQU11HvRZQeLphunHprdBga3lgzM6nziKHgkv13QFA1VRKsqvuZ9QoHMT8vsdxP29gKS93iG_Lf9yCRZQww49SjypSK6wGTizQAkrxBvghWxIkRnPZ4hDwt0U070aGoLcKFSz7TLUS65I5w",
                "referer: https://www.facebook.com/login/identify?ctx=recover",
                "upgrade-insecure-requests: 1",
                "user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.90 Safari/537.36"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response;
        }
    }
    //LSD = AVoVc2wb
    //datr = F9pTWBjhwxsR3jW5-gjeuIR1
    public function testRecoveryForm()
    {
//        $lsd = 'AVpEnkwc';
//        $datr = '_kbQV7Rvai02CoOrMgf0FqFb;';
        $client = new Client();
//        $fields = array('lsd' => $lsd, 'email' => 'alejandrosanluis@hotmail.com', 'did_submit' => "Search", '__user' => 0, '__a' => 1);
        $client->setHeader('User-Agent', "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36");
//        $client->setHeader('cookie', "datr=" . $datr);
//        $content = 'lsd=' . $lsd . '&email=' . '615896877' . '&did_submit=Search&__user=0&__a=1';

//        $crawler = $client->request('POST', 'https://www.facebook.com/ajax/login/help/identify.php?ctx=recover', [], [], ['HTTP_CONTENT_TYPE' => 'application/x-www-form-urlencoded'], $content);

        $crawler = $client->request('GET', 'https://www.facebook.com/login/identify?ctx=recover');
        //$crawler = $client->click($crawler->selectLink('¿Has olvidado los datos de la cuenta?')->link());
       // var_dump($crawler->html());
        $var = strstr($crawler->html(), '["LSD",');
        $array = explode('"', $var);
//        var_dump($array);
        // confirmar
        $lsd = $array[5];
        var_dump($array[5]);

        $var = strstr($crawler->html(), '["_js_datr",');
        $array = explode('"', $var);
        $datr = $array[3];
        echo PHP_EOL;

        var_dump($array[3]);
        echo PHP_EOL;
        echo PHP_EOL;
        echo PHP_EOL;
        unset($array);
        unset($client);
        unset($crawler);
        $cmd = escapeshellcmd('/usr/bin/python /home/asanluis/Proyects/api-hocelot/tests/facebook.py '.$lsd.' '.$datr.' 615896871');
        $python = shell_exec($cmd);
        var_dump(json_decode($python));
//        $form = $crawler->filter('form#identify_yourself_flow')->form();//filter('input[name="did_submit"]')->form();
//        $arr = $form->getPhpValues();
//        $arr['email'] = '615896877';
//        $crawler = $client->submit($form, $arr);
//        var_dump($crawler->current());
//        $form->setValues(array('email' => 'alejandrosanluis@hotmail.com'));
//        $crawler = $client->submit($form, $form->getPhpValues());
//
//        $crawler = $client->submit($form, array(
//            'email' => '615896877'
//        ));
//        echo '------------------', PHP_EOL;
//
//        var_dump($client);
//        echo '------------------', PHP_EOL;
//        echo $crawler->html();
    }
//    public function testRecoveryForm()
//    {
//        putenv("webdriver.chrome.driver=/home/asanluis/Desktop/geckodriver");
//        $host = 'http://localhost:4444/wd/hub';
//
//        $driver = RemoteWebDriver::create($host, DesiredCapabilities::firefox());
//        $driver->manage()->timeouts()->implicitlyWait = 10;
//
//        $driver->manage()->deleteAllCookies();
//
//        $driver->get('https://es-es.facebook.com');
//        $driver->navigate()->to('https://es-es.facebook.com');
//        $driver->findElement(WebDriverBy::linkText("¿Has olvidado los datos de la cuenta?"))->click();
////        # input some text
//        $driver->wait(5)->until(
//            WebDriverExpectedCondition::presenceOfAllElementsLocatedBy(
//                WebDriverBy::id("identify_email")
//            )
//        );
//        $driver->findElement(WebDriverBy::id("identify_email"))->sendKeys('lj@gmail.com')->submit();
//        sleep(0.3);
//        if (count($driver->findElements(WebDriverBy::cssSelector('.uiBoxRed'))) > 0) {
//            $email = 'No hay cuenta';
//            $phone = 'No hay cuenta';
//        } else {
//            $driver->wait(5)->until(
//                WebDriverExpectedCondition::presenceOfAllElementsLocatedBy(
//                    WebDriverBy::cssSelector('label[for="send_email"] > div > div > div')
//                )
//            );
//            if ($driver->findElement(WebDriverBy::cssSelector('label[for="send_email"] > div > div > div'))->isEnabled()) {
//                $email = trim($driver->findElement(WebDriverBy::cssSelector('label[for="send_email"] > div > div > div'))->getText());
//                if (stristr($email, "Email me a link to reset my password")) {
//                    $email = trim(str_replace("Email me a link to reset my password", "", $email));
//
//                }
//            } else $email = 'No tiene correo asociado';
//            if ($driver->findElement(WebDriverBy::cssSelector('tr:last-of-type > td > div > label > div > div > div > div'))->isEnabled()) {
//                $phone = trim($driver->findElement(WebDriverBy::cssSelector('tr:last-of-type > td > div > label > div > div > div > div'))->getText());
//            } else $phone = 'No tiene telefono asociado';
//
//
//        }
//        echo $email;
//        echo $phone;
//        $img = $driver->findElement(WebDriverBy::cssSelector('table > tbody > tr > td:last-child > div > div > div > img'))->getAttribute('src');
//        $name = $driver->findElement(WebDriverBy::cssSelector('table > tbody > tr > td:last-child > div > div'))->getText();
//echo $img;
//        echo PHP_EOL;
//        $name = trim(str_replace('Facebook User','',trim($name)));
//        echo $name;
////        WebDriverBy::cssSelector('label:');
////        $driver->getKeyboard()->sendKeys('615896877');
////
////# send keyboard actions, press `cmd+a` & `delete`
////        $driver->getKeyboard()
////            ->sendKeys(array(
////                WebDriverKeys::ENTER
////            ));
////        echo $driver->getCurrentURL();
//
//
//    }
}
