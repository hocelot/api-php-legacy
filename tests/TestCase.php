<?php

use Symfony\Component\DomCrawler\Crawler;

class TestCase extends Laravel\Lumen\Testing\TestCase
{
    public function testoAuthAccessToken()
    {
        $secret_key = 'OfjbZsHGcVRajirXj3fxjOYLRqNO4IAKksZ9MKHk';
        $client_token = 'mr_UdyAtQkqcKtw8yoRFe7WlJwp9HuGoyRZUcAHj';
        $private_scope = 'mr_EhDnRifP3qZq75VUW5BsSnWh1sMk8Dkb0hKGh';
        $response = $this->call('POST', '/api/oauth/access_token', [
            'grant_type' => 'client_credentials',
            'client_id' => $client_token,
            'client_secret' => $secret_key,
            //'scope'         => $private_scope
        ]);
        echo $response;
        // $this->assertResponseOk();
        return json_decode($response->content());
    }

    protected function curloAuth($page, $access_token, $params)
    {
        $pubKey = 'LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0KTUlJQ0lqQU5CZ2txaGtpRzl3MEJBUUVGQUFPQ0FnOEFNSUlDQ2dLQ0FnRUF0TEovdHFKVEFLWGFvNnNHeXhEeQpIT1JQckhWc1lOckNrdXZqRXB0OFJNLzc0ZmtqRGNMaVh1NGlOQVh2TTNCWEUzbGZrQU9xeStJcThYbjdyV2ZnCis3aGU1VGp5NElDVTdDUHV3WjNtRXV6bUh1eUg4bG1DNHVrRTJuYUcwNmVXYkoyNkV5bzU4UVVOZk00Slk1WEkKalRLenAwU2VFK0tkWWhzMTJiWk4rdndybTExQ1FsM3VsZFpvTDdHL1M4UFNKOXJmZUhsUEU2MEl3Z0srWjNSQQoxaUJ0QzBVNGFZVjJzS2NFYjcxMXArTFlLVlJxSDNnUkJHL1BCclZqZUY4ZEJsQWptdG4zZ2o0TDhjRStvREFRCi9iOGN4R25jMEVZT05TaVJSaTRNWHhaNWVzT0FLMnNIKzJtTDdnT1ZZRGtUWU1oTWlGbUdOUjNqNG45L2dVcmgKS1o3NjNpOW8zc1VzV2htZ3ZMQ1VIVkloSFEwY2pudndZd21MMGk4TGVsaGFNSzQ4T3M4blFNMGliUDc0UE12VApGS0o4eCtIVzRBckM0V2pMMndRbVg5STJNemtla3lSU0lzZCtMM1pmWWhaT0N4QXpCVkVNZEpTMU9jUG10K3g4ClpIN09QRFFvZ0tBbzZyS1I0V1VxNlFjZVphZmp3VWRsS05YRUpnZ0RXeVV5L0lyTVdQS080SVhWb08vdlpWZlMKdU92OWYrOHBBN2JmMmxPQjhnSS8yaTdSQ1BTM09rZW5BOFhkcnNabUV6N0hqSDB6eTIxQ3RBT3hiUDhNZWZjUwphcHVjWjNXdnY0WnVRSkU0dW9JM1daUGdJUFo0NHd6dCtsOE9Uc3FhTWpYRUtmUUNkdEZjRXdreUlHNkVLRzlTClI4YW5NdW1uclhTUlRpVDNBMGluV1ZVQ0F3RUFBUT09Ci0tLS0tRU5EIFBVQkxJQyBLRVktLS0tLQo=';
        openssl_public_encrypt(json_encode($params['params']), $params['params'], base64_decode($pubKey));


        $server = [
            'HTTP_AUTHORIZATION' => 'Bearer ' . $access_token,
            // 'HTTP_CONTENT_TYPE'  => 'charset=UTF-8'
        ];
        $response = $this->call('POST', $page, $params, [], [], $server);
        //print_r($response);
        // $this->assertResponseOk();
        return json_decode($response->getContent());
    }


    protected function desencryptResponse($data)
    {
        Log::debug(print_r($data, TRUE));
        $db_res = app('db')->connection('oauth')->table('oauth_clients_keys')->select('private_key')->where('id', "mr_UdyAtQkqcKtw8yoRFe7WlJwp9HuGoyRZUcAHj")->first();
        openssl_private_decrypt(base64_decode($data), $response, base64_decode($db_res->private_key));

        return json_decode($response);
    }


    public function testelasticSearch()
    {

        $cp = "35508";
        $cprov = substr($cp, 0, 2);
        $codmun = array("018", "024");
        $poblacion_del_pibe = "TEGUISE";


        $client = new \Elastica\Client(array(
            'host' => '192.168.1.3',
            'port' => 9200
        ));
        $busquedas = array('municipio', 'entidadcolectiva', 'entidadsingular', 'nucleo');
        $cont = 0;
        do {
            $query = [
                'query' => [
                    'bool' => [
                        'must' => [
                            'match' => [
                                $busquedas[$cont] => [
                                    "fuzziness" => "AUTO",
                                    "operator" => "and",
                                    "query" => $poblacion_del_pibe,
                                ],
                            ],
                        ],
                        'filter' => [
                            ['term' => ["codprov" => $cprov]],
                            ['terms' => ["codmun" => $codmun]]
                        ]
                    ]
                ]
            ];


            $query = \Elastica\Query::create($query);

            $search = new Elastica\Search($client);
            $search->addType('towns')->addIndex('address');
            //$search->setQuery($query);
//        echo $search->count(); // returns int
            $arr = $search->search($query)->getResults();
            $cont++;
        } while (count($arr) == 0 && $cont < 4);
//        foreach ($arr as $it){
//        }
        if (count($arr) == 0 && $cont < 5) {
            echo 'Nada';
        } else {
            $codine = $arr[0]->getData()['codprov'] . $arr[0]->getData()['codmun'];


            echo $codine;
        }
    }

    public function testaddress()
    {

        $cod_ine = "28079";

        //Busqueda en orden
//        "entidadcolectiva":,
//                      "entidadsingular",
//                      "nucleo":,

        $client = new \Elastica\Client(array(
            'host' => '192.168.1.3',
            'port' => 9200
        ));
        $cont = 0;


        $query = [
            'query' => [
                'bool' => [
                    'must' => [
                        'match' => [
                            'nombredevia' => [
                                "fuzziness" => "AUTO",
                                "operator" => "and",
                                "query" => $nombredevia,
                            ],
                        ],
                    ],
                    'filter' => [
                        ['term' => ["tipodevia" => $tipodevia]],
                        //  ['term' => ["codmun" => $codmun[0] . 'OR' . $codmun[1]]]
                    ]
                ]
            ]
        ];


        $query = \Elastica\Query::create($query);

        $search = new Elastica\Search($client);
        $search->addType('town')->addIndex('towns');
        //$search->setQuery($query);
//        echo $search->count(); // returns int
        $arr = $search->search($query)->getResults();
        $cont++;

        if (count($arr) == 0 && $cont < 5) {
            echo 'Nada';
        } else {
            $codine = $arr[0]->getData()['codprov'] . $arr[0]->getData()['codmun'];


            echo $codine;
        }
    }

    public function teststreetname()
    {
//        $street = 'ALTAMIRANO';
//        $rt = 'CL';
//        $ine = '28079';
//        $pn = 48;
//
////    $street = GeoCheckModel::del_articulos($street);
        $client = new \Elastica\Client(array(
            'host' => '192.168.1.3',
            'port' => 9200
        ));
//        $result = false;
//        $query = [
//            'query' => [
//                'bool' => [
//                    'must' => [
//                        'match' => [
//                            'nombredevia' => [
//                                "fuzziness" => "AUTO",
//                                "operator" => "and",
//                                "query" => "ALTAMIRANO",
//                            ],
//                        ],
//                    ],
//                    'filter' => [
//                        ['term' => ["tipodevia.keyword" => $rt]]
//                    ]
//                ]
//            ],
////            'size' => 0,
//            "aggs" => [
//                "letras" => [
//                    "terms" => [
//                        "field" => "letra.keyword"
//                    ],
//                ],
//                "bloques" => [
//                    "terms" => [
//                        "field" => "bloque.keyword"
//                    ],
//                ]
//            ]
//        ];
//    $query = [
//        'query' => [
//            'bool' => [
//                'must' => [
//                    'match' => [
//                        'nombredevia' => [
//                            "fuzziness" => "AUTO",
//                            "operator" => "and",
//                            "query" => $street,
//                        ],
//                    ],
//                ],
//                'filter' => [
//                    ['term' => ["tipodevia.keyword" => $rt]],
//                    ['term' => ["primernumeropolicia.keyword" => $pn]],
//                ]
//            ]
//        ],
//        'size' => 0,
//        "aggs" => [
//            "street_names" => [
//                "terms" => [
//                    "field" => "nombredevia.keyword"
//                ]
//            ]
//        ]
//    ];
//        $query = \Elastica\Query::create($query);
//        $search = new \Elastica\Search($client);
//        $search->addType($ine)->addIndex('catastro');
//        $arr = $search->search($query)->getResults();//    var_dump($arr);
//        print_r($arr);
//        $ine='28079';
//        $rt='CL';
//        $street='escosura';
//        $pnp='4';
//        $letra='';
//        $km='';
//        $bloque='';
//        $escalera='';
//        $planta='4';
//        $puerta='I2';
        $ine = '28079';
        $rt = 'CL';
        $street = 'caleruega';
        $pnp = '18';
        $letra = '';
        $km = '';
        $bloque = '';
        $escalera = '';
        $planta = '2';
        $puerta = '';
        $querybool = new \Elastica\Query\BoolQuery();
        //Añadimos filtro genérico
        $filter_tipovia = new Elastica\Query\Term();
        $filter_tipovia->setTerm('tipodevia.keyword', $rt);
        $querybool->addFilter($filter_tipovia);

        $filter_pnp = new Elastica\Query\Term();
        $filter_pnp->setTerm('primernumeropolicia.keyword', $pnp);
        $querybool->addFilter($filter_pnp);
        if (!empty($planta)) {
            $filter_planta = new Elastica\Query\Term();
            $filter_planta->setTerm('planta.keyword', $planta);
            $querybool->addFilter($filter_planta);
//            $field = 'planta';
//            $operator = 'and';
//            $fuzziness = "AUTO";
//            $planta_match = new \Elastica\Query\Match();
//            $planta_match->setFieldQuery($field, $planta);
//            $planta_match->setFieldOperator($field, $operator);
//            $planta_match->setFieldFuzziness($field, $fuzziness);
//            $querybool->addMust($planta_match->toArray());
        }

        //Añadimos match nombredevia y primernumeropolicia
        $field = 'nombredevia';
        $operator = 'and';
        $fuzziness = "AUTO";
        $street_match = new \Elastica\Query\Match();
        $street_match->setFieldQuery($field, $street);
        $street_match->setFieldOperator($field, $operator);
        $street_match->setFieldFuzziness($field, $fuzziness);
        $querybool->addMust($street_match->toArray());
//        $field = 'primernumeropolicia';
//        $pnp_match = new \Elastica\Query\Match();
//        $pnp_match->setFieldQuery($field, $pnp);
//        $querybool->addMust($pnp_match->toArray());

        if (!empty($letra)) {
            $field = 'letra';
            $operator = 'and';
            $fuzziness = "AUTO";
            $letra_match = new \Elastica\Query\Match();
            $letra_match->setFieldQuery($field, $letra);
            $letra_match->setFieldOperator($field, $operator);
            $letra_match->setFieldFuzziness($field, $fuzziness);
            $querybool->addMust($letra_match->toArray());
        }
        if (!empty($km)) {
            $field = 'kilometro';
            $operator = 'and';
            $fuzziness = "AUTO";
            $km_match = new \Elastica\Query\Match();
            $km_match->setFieldQuery($field, $km);
            $km_match->setFieldOperator($field, $operator);
            $km_match->setFieldFuzziness($field, $fuzziness);
            $querybool->addMust($km_match->toArray());
        }
        if (!empty($bloque)) {
            $field = 'bloque';
            $operator = 'and';
            $fuzziness = "AUTO";
            $bloque_match = new \Elastica\Query\Match();
            $bloque_match->setFieldQuery($field, $bloque);
            $bloque_match->setFieldOperator($field, $operator);
            $bloque_match->setFieldFuzziness($field, $fuzziness);
            $querybool->addMust($bloque_match->toArray());
        }
        if (!empty($escalera)) {
            $field = 'letra';
            $operator = 'and';
            $fuzziness = "AUTO";
            $escalera_match = new \Elastica\Query\Match();
            $escalera_match->setFieldQuery($field, $escalera);
            $escalera_match->setFieldOperator($field, $operator);
            $escalera_match->setFieldFuzziness($field, $fuzziness);
            $querybool->addMust($escalera_match->toArray());
        }

        if (!empty($puerta)) {
            $field = 'puerta';
            $operator = 'and';
            $fuzziness = "AUTO";
            $puerta_match = new \Elastica\Query\Match();
            $puerta_match->setFieldQuery($field, $puerta);
            $puerta_match->setFieldOperator($field, $operator);
            $puerta_match->setFieldFuzziness($field, $fuzziness);
            $querybool->addMust($puerta_match->toArray());
        }
        $query = [
            'query' => $querybool->toArray()
        ];

        $query = \Elastica\Query::create($query);
        $search = new \Elastica\Search($client);
        $search->addType($ine)->addIndex('catastro');
        $arr = $search->search($query)->getResults();
        var_dump($arr);
        foreach ($arr as $results){
            $results =$results->getData();
            echo $results['m2'],PHP_EOL;
        }
    }

    public function testFacebook()
    {
        $query = 'miryam.estevez@gmail.com';
        $url = 'http://www.facebook.com/search/people/?q=' . $query;
        $user_agent = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.109 Safari/537.36';

        $c = curl_init();
        curl_setopt_array($c, array(
            CURLOPT_URL => $url,
            CURLOPT_USERAGENT => $user_agent,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_FOLLOWLOCATION => TRUE,
            CURLOPT_SSL_VERIFYPEER => FALSE
        ));
        $data = curl_exec($c);

        preg_match_all('/href=\"https:\/\/www.facebook.com\/(([^\"\/]+)|people\/([^\"]+\/\d+))[\/]?\"/', $data, $matches);
        if ($matches[3][0] != FALSE) {                // facebook.com/people/name/id
            $pages = array_map(function ($el) {
                return explode('/', $el)[0];
            }, $matches[3]);
        } else                                      // facebook.com/name
            $pages = $matches[2];
        $result = array_filter(array_unique($pages));
        if (isset($result[0]) && !empty($result[0])) {

        }
    }

    public function testFBProfile()
    {
        $query = 'miryam.estevez@gmail.com';
        $url = 'http://www.facebook.com/danimm.martinez';
        $user_agent = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.109 Safari/537.36';

        $c = curl_init();
        curl_setopt_array($c, array(
            CURLOPT_URL => $url,
            CURLOPT_USERAGENT => $user_agent,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_FOLLOWLOCATION => TRUE,
            CURLOPT_SSL_VERIFYPEER => FALSE
        ));
        $profile = curl_exec($c);
        curl_close($c);
//        var_dump($profile);


        /***********************CRAWLEEER**************************************************/

        $crawler = new Crawler();
        $crawler->addHtmlContent($profile);
        $facebook = new stdClass();
        $facebook->cover_image = $crawler->filterXPath('//html/body/div/div[@id=\'globalContainer\']/div[@id=\'content\']/div/div[@id=\'mainContainer\']/div[@id=\'contentCol\']/div[@id=\'contentArea\']/div/div[1]/div[1]/div[3]/div/div[@id=\'fbTimelineHeadline\']/div[3]/div/div/div/img')->attr('src');
        $facebook->name = $crawler->filterXPath('//html/body/div/div[@id=\'globalContainer\']/div[@id=\'content\']/div/div[@id=\'mainContainer\']/div[@id=\'contentCol\']/div[@id=\'contentArea\']/div/div[1]/div[1]/div[3]/div/div[@id=\'u_0_5\']/div/h1/a/span')->text();
        $facebook->profile_pic = $crawler->filterXPath('//html/body/div/div[@id=\'globalContainer\']/div[@id=\'content\']/div/div[@id=\'mainContainer\']/div[@id=\'contentCol\']/div[@id=\'contentArea\']/div/div[1]/div[1]/div[3]/div/div[@id=\'fbTimelineHeadline\']/div[3]/div/div/div/img')->attr('src');

        // Si acaba con ) hay un alias
        if (substr(trim($facebook->name), -1) == ')') {
            $searx_ap = strpos($facebook->name, '(');
            $facebook->alias = trim(str_replace(')', '', substr(trim($facebook->name), $searx_ap + 1)));
            $facebook->name = trim(str_replace(substr(trim($facebook->name), $searx_ap), '', trim($facebook->name)));
        }

        //Empleo
        $crawler->filterXPath('//html/body/div/div[@id=\'globalContainer\']/div[@id=\'content\']/div/div[@id=\'mainContainer\']/div[@id=\'contentCol\']/div[@id=\'contentArea\']/div/div[2]/div/div[1]/div[@id=\'pagelet_timeline_medley_about\']/div[@id=\'collection_wrapper_2327158227\']/div/div/div[@id=\'pagelet_eduwork\']/div/div[1]/ul/li')
            ->each(function ($node) use ($facebook) {
                $job = new stdClass();
                $nom = $node->filter('div > div > div > div > div')->eq(1)->filter('div > a');

                if ($nom->count() > 0)
                    $job->name = $nom->text();
                else $job->name = false;

                $fech = $node->filter('div > div > div > div > div')->eq(1)->filter('div')->eq(2)->filter('div');
                if ($fech->count() > 0)
                    $job->date = $fech->text();
                else $job->date = false;


                $facebook->jobs[] = $job;
            });

        //Studies
        $crawler->filterXPath('//html/body/div/div[@id=\'globalContainer\']/div[@id=\'content\']/div/div[@id=\'mainContainer\']/div[@id=\'contentCol\']/div[@id=\'contentArea\']/div/div[2]/div/div[1]/div[@id=\'pagelet_timeline_medley_about\']/div[@id=\'collection_wrapper_2327158227\']/div/div/div[@id=\'pagelet_eduwork\']/div/div[2]/ul/li')
            ->each(function ($node) use ($facebook) {
                $study = new stdClass();
                $li_s = $node->filter('div > div > div > div > div')->eq(1);
                $ins = $li_s->filter('div > a');
                $sub = $li_s->filter('div')->eq(2);

                if ($ins->count() > 0)
                    $study->institution = $ins->text();
                else  $study->institution = false;

                if ($sub->count() > 0)
                    $study->sub = $sub->text();
                else  $study->sub = false;

                $facebook->studies[] = $study;
            });

        //Lugares
        $crawler->filterXPath('//html/body/div/div[@id=\'globalContainer\']/div[@id=\'content\']/div/div[@id=\'mainContainer\']/div[@id=\'contentCol\']/div[@id=\'contentArea\']/div/div[2]/div/div[1]/div[@id=\'pagelet_timeline_medley_about\']/div[@id=\'collection_wrapper_2327158227\']/div/div/div[@id=\'pagelet_hometown\']/div/div/ul')
            ->each(function ($node) use ($facebook) {
                $node->filter('li')->each(function ($n) use ($facebook) {
                    $place = new stdClass();
                    $li_s = $n->filter('div > div > div > div > div')->eq(1);
                    $name = $li_s->filter('span > a');
                    $status = $li_s->filter('div > div');

                    if ($name->count() > 0)
                        $place->name = $name->text();
                    if ($status->count() > 0)
                        $place->status = $status->text();

                    if (!isset($place->name) && empty($place->name)) {
                        $li_s = $n->filter('div > div > div > div > div > div')->eq(1);
                        $name = $li_s->filter('span > a');
                        $status = $li_s->filter('div > div');

                        if ($name->count() > 0)
                            $place->name = $name->text();
                        if ($status->count() > 0)
                            $place->status = $status->text();
                    }

                    $facebook->places[] = $place;

                });

            });

        $crawler->filterXPath('//html/body/div/div[@id=\'globalContainer\']/div[@id=\'content\']/div/div[@id=\'mainContainer\']/div[@id=\'contentCol\']/div[@id=\'contentArea\']/div/div[2]/div/div[1]/div[@id=\'pagelet_all_favorites\']/div/div/div[2]/table/tbody')
            ->each(function ($node) use ($facebook) {
                $fav = new stdClass();
                $fav->titulo = $node->filter('tr > th')->text();
                $node->filter('td > div > ul > li')->each(function ($n) use ($fav, $facebook) {
                    $facebook->details[$fav->titulo] = $n->filter('div')->text();
                });
                $node->filter('td > div > span')->each(function ($n) use ($fav, $facebook) {
                    $n->filter('a')->each(function ($a) use ($fav, $facebook) {
                        $facebook->details[$fav->titulo][] = $a->text();
                    });
                });
            });


        var_dump($facebook, true);

    }

    public function testuserinstagram()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://www.instagram.com/web/search/topsearch/?context=blended&query=elneura&rank_token=0.5",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "accept: */*",
                "accept-encoding: gzip, deflate, sdch, br",
                "accept-language: es-ES,es;q=0.8",
                "authority: www.instagram.com",
                "cache-control: no-cache",
                "referer: https://www.instagram.com/gorda/",
                "user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36",
                "x-requested-with: XMLHttpRequest"
            ),
        ));
//http://deskgram.com/gorda
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response;
        }

    }

    public function testPinterest()
    {
        similar_text('ISLA DE AROSA','ISLA DE AROSA (P GRANDE)',$percent);
        echo $percent,PHP_EOL;
        similar_text('CASTILLA','CASTELLO',$percent);
        echo $percent,PHP_EOL;
    }
//    https://badoo.com/es/import/
    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__ . '/../bootstrap/app.php';
    }
}
