<?php


use App\Http\Controllers\Proxies\Proxies;

class IdCheckTest extends TestCase
{
    public function testIdCheck()
    {
//        if (($archivo = fopen('id_check_prueba.csv', 'r')) !== FALSE) {
//            while (($datos = fgetcsv($archivo, 1000, ";")) !== FALSE) {
//                $dni = mb_convert_encoding($datos[0], 'UTF-8');
//                $name = mb_convert_encoding($datos[1], 'UTF-8');
//                $first_name = mb_convert_encoding($datos[2], 'UTF-8');
//                $last_name = mb_convert_encoding($datos[3], 'UTF-8');

//                if (strlen($dni) < 9) {
//                    $ic_result = ['dni_encontrado' => $dni, 'hits' => ['score_id_check' => 'KO', 'aeat_name' => '-', 'correlacion_id_check' => 0.0], 'status' => 400];
//                } else {

//                    $dni = strtoupper($dni);
        $dni = '78585891C';
        $name = 'ALEJANDRO';
        $first_name = 'SAN LUIS';
        $last_name = 'MOURE';
        $curl_results = array(
            'dni' => $dni,
            'aeat_name' => 'SAN LUIS MOURE PATRICIA',
            'status' => 200
        );

        $letra = substr($dni, -1, 1);
        $numero = substr($dni, 0, 8);

        // Si es un NIE hay que cambiar la primera letra por 0, 1 ó 2 dependiendo de si es X, Y o Z.
        $numero = str_replace(array('X', 'Y', 'Z'), array(0, 1, 2), $numero);

        $modulo = $numero % 23;
        $letras_validas = "TRWAGMYFPDXBNJZSQVHLCKE";
        $letra_correcta = substr($letras_validas, $modulo, 1);

        if ($letra_correcta != $letra) {
            $ic_result = ['dni_encontrado' => $dni, 'hits' => ['score_id_check' => 'KO', 'aeat_name' => 'dni o nie mal formado', 'correlacion_id_check' => 0.0], 'status' => 400];
        } else {

            $apellido1 = str_replace(' ', '+', $this->del_articulos(mb_strtoupper($this->elimina_acentos(trim($first_name)))));
            $search = array($apellido1, substr($apellido1, 0, 1));
            $c = 0;
//                        do {
//                            $curl_results = self::searchAeat($dni, $search[$c]);
//                            $c++;
//                        } while ($c < count($search) && !$curl_results["aeat_name"]);

            if (!$curl_results["aeat_name"] && $curl_results["status"] != 200) {
                $aeat_name = "";
                $dni_encontrado = $dni;
            } else if (!$curl_results["aeat_name"] && $curl_results["status"] == 200) {
                $aeat_name = "-";
                $dni_encontrado = $dni;
                $percent = 0.0;
            } else {
                $status = 200;
                $dni_encontrado = $curl_results['dni'];
                $aeat_name = $this->del_articulos(mb_strtoupper($this->elimina_acentos(trim(str_replace('-', ' ', str_replace('.', '', str_replace(',', '', $curl_results["aeat_name"])))))));
                $name = $this->del_articulos(mb_strtoupper($this->elimina_acentos(trim(str_replace('-', ' ', $name)))));
                $first_name = $this->del_articulos(mb_strtoupper($this->elimina_acentos(trim(str_replace('-', ' ', $first_name)))));
                $last_name = $this->del_articulos(mb_strtoupper($this->elimina_acentos(trim(str_replace('-', ' ', $last_name)))));

                $explode_aeat_name = explode(' ', $aeat_name);
                $explode_name = explode(' ', $name);
                $explode_first_name = explode(' ', $first_name);
                $explode_last_name = explode(' ', $last_name);

                //Nombre : 33%, Apellido 1: 33% y Apellido 2: 33%
                //Nº palabras * 100 == porcentaje total del apartado
                // sum de encontrado en el aprato * porcentaje total del apartado / 33
                $final_percent = 0;
                //Nombre
                $sum_percent_name = 0;
                foreach ($explode_name as $name_word) {
                    $max_percent = 0;
                    $selected_index = -1;
                    foreach ($explode_aeat_name as $key => $aeat_word) {
                        similar_text($aeat_word, $name_word, $percent);
                        if ($max_percent < $percent) {
                            $max_percent = $percent;
                            $selected_index = $key;
                        }
                    }
                    if ($selected_index != -1) {
                        unset($explode_aeat_name[$selected_index]);
                    }
                    $sum_percent_name += $max_percent;
                }
                $final_percent += ($sum_percent_name * (100 / 3)) / (count($explode_name) * 100);
                //Apellido 1
                $sum_percent_first_name = 0;
                foreach ($explode_first_name as $first_name_word) {
                    $max_percent = 0;
                    $selected_index = -1;
                    foreach ($explode_aeat_name as $key => $aeat_word) {
                        similar_text($aeat_word, $first_name_word, $percent);
                        if ($max_percent < $percent) {
                            $max_percent = $percent;
                            $selected_index = $key;
                        }
                    }
                    if ($selected_index != -1) {
                        unset($explode_aeat_name[$selected_index]);
                    }
                    $sum_percent_first_name += $max_percent;
                }
                $final_percent += ($sum_percent_first_name * (100 / 3)) / (count($explode_first_name) * 100);

                //Apellido 2
                $sum_percent_last_name = 0;
                foreach ($explode_last_name as $last_name_word) {
                    $max_percent = 0;
                    $selected_index = -1;
                    foreach ($explode_aeat_name as $key => $aeat_word) {
                        similar_text($aeat_word, $last_name_word, $percent);
                        if ($max_percent < $percent) {
                            $max_percent = $percent;
                            $selected_index = $key;
                        }
                    }
                    if ($selected_index != -1) {
                        unset($explode_aeat_name[$selected_index]);
                    }
                    $sum_percent_last_name += $max_percent;
                }


                $final_percent += ($sum_percent_last_name * (100 / 3)) / (count($explode_last_name) * 100);
                $percent = (round($final_percent, 2));
                var_dump('PRIMER FILTRO ' . $percent);


                if (isset($percent) && $percent < 80.0) {

                    $explode_aeat_name = explode(' ', $aeat_name);
                    $explode_full_name = explode(' ', $name . ' ' . $first_name . ' ' . $last_name);
                    $sum_percent_aeat_name = 0;
                    foreach ($explode_full_name as $full_name_word) {
                        $max_percent = 0;
                        $selected_index = -1;
                        foreach ($explode_aeat_name as $key => $aeat_word) {
                            similar_text($aeat_word, $full_name_word, $percent);
                            if ($max_percent < $percent) {

                                $max_percent = $percent;
                                $selected_index = $key;
                            }
                        }
                        if ($selected_index != -1) {
                            unset($explode_full_name[$selected_index]);
                        }
                        $sum_percent_aeat_name += $max_percent;
                    }

                    $final_percent = $sum_percent_aeat_name / count($explode_aeat_name);
                    $percent = (round($final_percent, 2));
                    var_dump('SEGUNDO FILTRO ' . $percent);
                }

            }
            $id_check = array(
                'nif' => $dni,
                'name_surname' => $name . ' ' . $first_name . ' ' . $last_name
            );

            if (!isset($percent)) {
                $aeat_name = '-';
                $percent = 0.0;
                $id_check['score_id_check'] = 'NC';
                $status = 402;
            } else if ($percent < 90.0) {
                $id_check['score_id_check'] = 'KO';
                $status = 400;
            } else {
                $status = 200;
                $id_check['score_id_check'] = 'OK';

            }
            $percent = number_format($percent / 100, 2, ',', '.');
            $id_check['aeat_name'] = $aeat_name;
            $id_check['correlacion_id_check'] = $percent;


            //Log::debug(print_r($save, true));
            $ic_result = ['dni_encontrado' => $dni_encontrado, 'hits' => $id_check, 'status' => $status];
        }
        var_dump($ic_result);
//                }
//                $txt = '';
//                foreach ($ic_result as $key => $res) {
//                    if ($key == 'hits') {
//                        foreach ($res as $hit) {
//                            $txt .= $hit . ';';
//                        }
//                    } else $txt .= $res . ';';
//                }
//                $txt = ($txt);
//                var_dump($txt);
//                exit();
//                file_put_contents('IC_RESULT.CSV', ($txt) . PHP_EOL, FILE_APPEND);
//            }
//        }
    }

    public function testFirstFunction()
    {
        $dni = '78585891C';
        $name = 'RUVEN';
        $first_name = 'DOMINGUEZ';
        $last_name = 'MERINO';
        $curl_results = array(
            'dni' => $dni,
            'aeat_name' => 'DOMINGEZ MERINO RUBEN',
            'status' => 200
        );


        $dni_encontrado = $curl_results['dni'];
        $aeat_name = $this->del_articulos(mb_strtoupper($this->elimina_acentos(trim(str_replace('-', ' ', str_replace('.', '', str_replace(',', '', $curl_results["aeat_name"])))))));
        $name = $this->del_articulos(mb_strtoupper($this->elimina_acentos(trim(str_replace('-', ' ', $name)))));
        $first_name = $this->del_articulos(mb_strtoupper($this->elimina_acentos(trim(str_replace('-', ' ', $first_name)))));
        $last_name = $this->del_articulos(mb_strtoupper($this->elimina_acentos(trim(str_replace('-', ' ', $last_name)))));

        $explode_aeat_name = explode(' ', $aeat_name);
        $explode_name = explode(' ', $name);
        $explode_first_name = explode(' ', $first_name);
        $explode_last_name = explode(' ', $last_name);

        $intersect_name_aeat = array_intersect($explode_aeat_name, $explode_name);
        $intersect_first_name = array_intersect($explode_aeat_name, $explode_first_name);
        $intersect_last_name = array_intersect($explode_aeat_name, $explode_last_name);

        //Nombre : 33%, Apellido 1: 33% y Apellido 2: 33%
        //Nº palabras * 100 == porcentaje total del apartado
        // sum de encontrado en el aprato * porcentaje total del apartado / 33
        $keys_assigned = [];
        $sum_percent_name = $sum_percent_first_name = $sum_percent_last_name = 0;
        $num_names = count($explode_name);
        $num_first_names = count($explode_first_name);
        $num_last_names = count($explode_last_name);

        foreach ($intersect_name_aeat as $key => $int_name) {
            if (!in_array($key, $keys_assigned)) {
                foreach ($explode_name as $key_ln => $name) {
                    if ($name == $int_name) {
                        $sum_percent_name+=100;
                        unset($explode_aeat_name[$key]);
                        unset($explode_name[$key_ln]);
                        $keys_assigned[] = $key;
                    }
                }
            }
        }
        foreach ($intersect_first_name as $key => $int_name) {
            if (!in_array($key, $keys_assigned)) {
                foreach ($explode_first_name as $key_ln => $f_name) {
                    if ($f_name == $int_name) {
                        $sum_percent_first_name+=100;
                        unset($explode_first_name[$key_ln]);
                        unset($explode_aeat_name[$key]);
                        $keys_assigned[] = $key;
                    }
                }
            }
        }
        foreach ($intersect_last_name as $key => $int_name) {
            if (!in_array($key, $keys_assigned)) {
                foreach ($explode_last_name as $key_ln => $last_name) {
                    if ($last_name == $int_name) {
                        $sum_percent_last_name+=100;
                        unset($explode_last_name[$key_ln]);
                        unset($explode_aeat_name[$key]);
                        $keys_assigned[] = $key;
                    }
                }
            }
        }

        $final_percent = 0;
        if (count($explode_name) == 0) {
            $final_percent += 100 / 3;
        }else{
            //Nombre
            foreach ($explode_name as $name_word) {
                $max_percent = 0;
                $selected_index = -1;
                foreach ($explode_aeat_name as $key => $aeat_word) {
                    similar_text($aeat_word, $name_word, $percent);
                    if ($max_percent < $percent) {
                        $max_percent = $percent;
                        $selected_index = $key;
                    }
                }
                if ($selected_index != -1) {
                    unset($explode_aeat_name[$selected_index]);
                }
                $sum_percent_name += $max_percent;
            }
            var_dump('PERCENT NAME '.$sum_percent_name);
            $final_percent += (($sum_percent_name*100)/($num_names*100))/3;
//            var_dump($sum_percent_name);
        }
        if (count($explode_first_name) == 0) {
            $final_percent += 100 / 3;
        }else{
            //Apellido 1
            foreach ($explode_first_name as $first_name_word) {
                $max_percent = 0;
                $selected_index = -1;
                foreach ($explode_aeat_name as $key => $aeat_word) {
                    similar_text($aeat_word, $first_name_word, $percent);
                    if ($max_percent < $percent) {
                        $max_percent = $percent;
                        $selected_index = $key;
                    }
                }
                if ($selected_index != -1) {
                    unset($explode_aeat_name[$selected_index]);
                }
                $sum_percent_first_name += $max_percent;
            }
            var_dump('PERCENT FIRST NAME '.$sum_percent_first_name);
            $final_percent += (($sum_percent_first_name*100)/($num_first_names*100))/3;

//            $final_percent += ($sum_percent_first_name * (100 / 3)) / ($num_first_names * 100);
        }

        if (count($explode_last_name) == 0) {
            $final_percent += 100 / 3;
        }else{
            //Apellido 2
            $sum_percent_last_name = 0;
            foreach ($explode_last_name as $last_name_word) {
                $max_percent = 0;
                $selected_index = -1;
                foreach ($explode_aeat_name as $key => $aeat_word) {
                    similar_text($aeat_word, $last_name_word, $percent);
                    if ($max_percent < $percent) {
                        $max_percent = $percent;
                        $selected_index = $key;
                    }
                }
                if ($selected_index != -1) {
                    unset($explode_aeat_name[$selected_index]);
                }
                $sum_percent_last_name += $max_percent;
            }
            var_dump('PERCENT LAST NAME '.$sum_percent_last_name);

            $final_percent += (($sum_percent_last_name*100)/($num_last_names*100))/3;

//            $final_percent += ($sum_percent_last_name * (100 / 3)) / ($num_last_names * 100);
        }

        $percent = (round($final_percent, 2));
        var_dump('PRIMER FILTRO ' . $percent);
        $id_check = array(
            'nif' => $dni,
            'name_surname' => $name . ' ' . $first_name . ' ' . $last_name
        );

        if (!isset($percent)) {
            $aeat_name = '-';
            $percent = 0.0;
            $id_check['score_id_check'] = 'NC';
            $status = 402;
        } else if ($percent < 80.0) {
            $id_check['score_id_check'] = 'KO';
            $status = 400;
        } else {
            $status = 200;
            $id_check['score_id_check'] = 'OK';

        }
        $percent = number_format($percent / 100, 2, ',', '.');
        $id_check['aeat_name'] = $aeat_name;
        $id_check['correlacion_id_check'] = $percent;


        //Log::debug(print_r($save, true));
        $ic_result = ['dni_encontrado' => $dni_encontrado, 'hits' => $id_check, 'status' => $status];
        var_dump($ic_result);
    }

    public function testSecondFunction(){
        $dni = '78585891C';
        $name = 'PATRICIA';
        $first_name = 'SAN LUIS';
        $last_name = 'MOURE';
        $curl_results = array(
            'dni' => $dni,
            'aeat_name' => 'SAN LUIS MOURE ALEJANDRO',
            'status' => 200
        );


        $dni_encontrado = $curl_results['dni'];
        $aeat_name = $this->del_articulos(mb_strtoupper($this->elimina_acentos(trim(str_replace('-', ' ', str_replace('.', '', str_replace(',', '', $curl_results["aeat_name"])))))));
        $name = $this->del_articulos(mb_strtoupper($this->elimina_acentos(trim(str_replace('-', ' ', $name)))));
        $first_name = $this->del_articulos(mb_strtoupper($this->elimina_acentos(trim(str_replace('-', ' ', $first_name)))));
        $last_name = $this->del_articulos(mb_strtoupper($this->elimina_acentos(trim(str_replace('-', ' ', $last_name)))));


        $explode_aeat_name = explode(' ', $aeat_name);
        $explode_full_name = explode(' ', $name . ' ' . $first_name . ' ' . $last_name);
        $sum_percent_aeat_name = 0;
        $num_aeat = count($explode_aeat_name);
        $intersect = array_intersect($explode_aeat_name,$explode_full_name);
        foreach ($intersect as $key => $int_name) {
                foreach ($explode_full_name as $key_fn => $full_name) {
                    if ($full_name == $int_name) {
                        $sum_percent_aeat_name+=100;
                        unset($explode_aeat_name[$key]);
                        unset($explode_full_name[$key_fn]);
                    }
                }
        }

        foreach ($explode_full_name as $full_name_word) {
            $max_percent = 0;
            $selected_index = -1;
            foreach ($explode_aeat_name as $key => $aeat_word) {
                similar_text($aeat_word, $full_name_word, $percent);
                if ($max_percent < $percent) {

                    $max_percent = $percent;
                    $selected_index = $key;
                }
            }
            if ($selected_index != -1) {
                unset($explode_full_name[$selected_index]);
            }
            $sum_percent_aeat_name += $max_percent;
        }

        $final_percent = $sum_percent_aeat_name / $num_aeat;
        $percent = (round($final_percent, 2));
        var_dump('SEGUNDO FILTRO ' . $percent);
    }

    private
    function searchAeat($dni, $ap)
    {
        $ip_real = false;
        $obj_proxy = new Proxies('aeat');
        do {
            sleep(1);
            $ip = $obj_proxy->get();
            $obj_proxy->setUsed($ip);
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, "https://www2.agenciatributaria.gob.es/es13/s/tccitccppci0");
            if ($ip) {
                curl_setopt($curl, CURLOPT_PROXY, $ip);
                curl_setopt($curl, CURLOPT_PROXYUSERPWD, "mrhc:QwEr1234");
            } else $ip_real = true;

            // curl_setopt($curl, CURLOPT_VERBOSE, 1);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curl, CURLOPT_POSTFIELDS,
                'CAB-MOTIVO=EJECUTADO+EN+INTERNET&EISLW=++++++++++++++++++++++++++++++++++++&EUSUARIO=++++++++&ESW=1&ENIF=' . $dni . '&EAPELLIDO=' . $ap . '&LimpiarSinCert=Limpiar&EnviarSinCert=Acceder');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            $result = curl_exec($curl);
            $info = curl_getinfo($curl);
            //Log::debug('Res'. print_r($result,true));
//            Log::debug(print_r($status,true));
//            Log::debug(print_r(curl_errno($curl),true));
            curl_close($curl);
            if (!$ip_real) {
                $obj_proxy->unlock($info["http_code"]);
            }

        } while ($info["http_code"] != 200 && !$ip_real);
        $var = strstr($result, 'pintarMigas(');
        $array = explode("'", $var);
//        Log::debug(print_r($array, true));
        if (empty($array[3]) || !isset($array[3])) {
            $res = false;
        } else $res = $array[3];
        if (empty($array[1]) || !isset($array[1])) {
            $dni = false;
        } else $dni = $array[1];

        return ['dni' => $dni, 'aeat_name' => utf8_encode($res), 'status' => $info["http_code"]];
    }

    private
    function elimina_acentos($cadena)
    {
        $text = htmlentities($cadena, ENT_SUBSTITUTE, 'UTF-8');
        $text = strtolower($text);
        $patron = array(
            '/\+/' => '',
            '/&agrave;/' => 'a',
            '/&egrave;/' => 'e',
            '/&igrave;/' => 'i',
            '/&ograve;/' => 'o',
            '/&ugrave;/' => 'u',
            '/&aacute;/' => 'a',
            '/&eacute;/' => 'e',
            '/&iacute;/' => 'i',
            '/&oacute;/' => 'o',
            '/&uacute;/' => 'u',
            '/&acirc;/' => 'a',
            '/&ecirc;/' => 'e',
            '/&icirc;/' => 'i',
            '/&ocirc;/' => 'o',
            '/&ucirc;/' => 'u',
            '/&atilde;/' => 'a',
            '/&etilde;/' => 'e',
            '/&itilde;/' => 'i',
            '/&otilde;/' => 'o',
            '/&utilde;/' => 'u',
            '/&auml;/' => 'a',
            '/&euml;/' => 'e',
            '/&iuml;/' => 'i',
            '/&ouml;/' => 'o',
            '/&uuml;/' => 'u',
            '/&auml;/' => 'a',
            '/&euml;/' => 'e',
            '/&iuml;/' => 'i',
            '/&ouml;/' => 'o',
            '/&uuml;/' => 'u',
            '/&aring;/' => 'a',
            '/&ntilde;/' => 'ñ',
            '/&Ccedil;/' => 'Ç',
            '/&ccedil;/' => 'ç',
            "/&#039;/" => "'"
        );

        $text = preg_replace(array_keys($patron), array_values($patron), $text);
        return $text;
    }

    private
    function del_articulos($str)
    {
        //Quitamos determinantes del nombre de la calle
        $articulos = array('DEL', 'DE', 'LA', 'EL', 'LO', 'LAS', 'LOS', "L'", "N'", "A", "VAN");

        $str_aux = explode(" ", $str);

        $resultado = array_diff($str_aux, $articulos);

        $str = implode(" ", $resultado);
        return $str;
    }


    public
    function testmierda()
    {
        $explode_full_name = explode(' ', 'MARIA SONIA FERNANDEZ ALVAREZ FERNANDEZ');
        $explode_aeat_name = explode(' ', 'FERNANDEZ ALVAREZ SONIA');
        $tot_percent = 100 * count($explode_aeat_name);

        $sum_percent_aeat_name = 0;
        foreach ($explode_aeat_name as $aeat_word) {
            $max_percent = 0;
            $selected_index = -1;
            foreach ($explode_full_name as $key => $full_name_word) {
                similar_text($aeat_word, $full_name_word, $percent);
                if ($max_percent < $percent) {
                    $max_percent = $percent;
                    $selected_index = $key;
                }
            }
            if ($selected_index != -1) {
                unset($explode_full_name[$selected_index]);
            }
            $sum_percent_aeat_name += $max_percent;
        }
        $final_percent = $sum_percent_aeat_name * 100 / $tot_percent;
        $percent = (round($final_percent, 2));
    }


}
