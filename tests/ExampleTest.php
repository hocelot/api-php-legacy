<?php

class ExampleTest extends TestCase {


	private function oAuthAccessToken()
	{
		$secret_key = 'OfjbZsHGcVRajirXj3fxjOYLRqNO4IAKksZ9MKHk';
		$client_token = 'mr_UdyAtQkqcKtw8yoRFe7WlJwp9HuGoyRZUcAHj';
		$private_scope = 'mr_EhDnRifP3qZq75VUW5BsSnWh1sMk8Dkb0hKGh';
		$response = $this->call('POST', '/api/oauth/access_token', [
			'grant_type'    => 'client_credentials',
			'client_id'     => $client_token,
			'client_secret' => $secret_key,
			'scope'         => $private_scope
		]);

//		$this->assertResponseOk();

		return json_decode($response->content());
	}

	private function curloAuth($page, $access_token, $params)
	{
		//$headers = array ();
		$server = [
			'HTTP_AUTHORIZATION' => 'Bearer ' . $access_token,
			'HTTP_CONTENT_TYPE'  => 'application/x-www-form-urlencoded;charset=UTF-8'
		];
		//print_r($server);
		//$headers['Authorization'] = 'Bearer ' . $access_token;
		//$headers['HTTP_ACCEPT'] = 'application/x-www-form-urlencoded;charset=UTF-8';
		$response = $this->call('POST', $page, $params, [ ], [ ], $server);

		//echo $response;
		$this->assertResponseOk();

		return $response;
	}

//	public function testAddressTown()
//	{
//		$oauth_response = $this->oAuthAccessToken();
//		//var_dump($oauth_response);
//		$params = array (
//			'params' => base64_encode(serialize(array (
//				'zip_code' => '28008'
//			)))
//		);
//		$access_token = $oauth_response->access_token;
//		$auth = $this->curloAuth('address/town', $access_token, $params);
//		$this->assertResponseOk();
//		echo $auth;
//	}
//
//	public function testAddressIneCode()
//	{
//		$oauth_response = $this->oAuthAccessToken();
//		//var_dump($oauth_response);
//		$params = array (
//			'params' => base64_encode(serialize(array (
//				'zip_code' => '28008',
//				'town'     => 'Madrid'
//			)))
//		);
//		$access_token = $oauth_response->access_token;
//		$auth = $this->curloAuth('address/code', $access_token, $params);
//		$this->assertResponseOk();
//		echo $auth;
//	}
//
//	public function testAddressRoadType()
//	{
//		$oauth_response = $this->oAuthAccessToken();
//		//var_dump($oauth_response);
//		$params = array (
//			'params' => base64_encode(serialize(array (
//				'ine_code' => '28079'
//			)))
//		);
//		$access_token = $oauth_response->access_token;
//		$auth = $this->curloAuth('address/roadtype', $access_token, $params);
//		$this->assertResponseOk();
//		echo $auth;
//	}
//
//	public function testAddressStreetName()
//	{
//		$oauth_response = $this->oAuthAccessToken();
//		//var_dump($oauth_response);
//		$params = array (
//			'params' => base64_encode(serialize(array (
//				'ine_code'    => '28079',
//				'rt_id'       => 'CL',
//				'street_name' => 'ALTAMIR'
//			)))
//		);
//		$access_token = $oauth_response->access_token;
//		$auth = $this->curloAuth('address/streetname', $access_token, $params);
//		$this->assertResponseOk();
//		echo $auth;
//	}
//
//	public function testAddressNumber()
//	{
//		$oauth_response = $this->oAuthAccessToken();
//		//var_dump($oauth_response);
//		$params = array (
//			'params' => base64_encode(serialize(array (
//				'ine_code'    => '28079',
//				'rt_id'       => 'CL',
//				'street_name' => 'ALTAMIRANO',
//				'number'      => '4'
//			)))
//		);
//		$access_token = $oauth_response->access_token;
//		$auth = $this->curloAuth('address/number', $access_token, $params);
//		$this->assertResponseOk();
//		echo $auth;
//	}
//
//	public function testAddressLetter()
//	{
//		$oauth_response = $this->oAuthAccessToken();
//		//var_dump($oauth_response);
//		$params = array (
//			'params' => base64_encode(serialize(array (
//				'ine_code'    => '28079',
//				'rt_id'       => 'CL',
//				'street_name' => 'ALTAMIRANO',
//				'number'      => '4',
//				'letter'      => ''
//			)))
//		);
//		$access_token = $oauth_response->access_token;
//		$auth = $this->curloAuth('address/letter', $access_token, $params);
//		$this->assertResponseOk();
//		echo $auth;
//	}
//
//	public function testAddressKm()
//	{
//		$oauth_response = $this->oAuthAccessToken();
//		//var_dump($oauth_response);
//		$params = array (
//			'params' => base64_encode(serialize(array (
//				'ine_code'    => '28079',
//				'rt_id'       => 'CL',
//				'street_name' => 'ALTAMIRANO',
//				'number'      => '4',
//				'km'          => ''
//			)))
//		);
//		$access_token = $oauth_response->access_token;
//		$auth = $this->curloAuth('address/km', $access_token, $params);
//		$this->assertResponseOk();
//		echo $auth;
//	}
//
//	public function testAddressBlock()
//	{
//		$oauth_response = $this->oAuthAccessToken();
//		//var_dump($oauth_response);
//		$params = array (
//			'params' => base64_encode(serialize(array (
//				'ine_code'    => '28079',
//				'rt_id'       => 'CL',
//				'street_name' => 'PRINCESA',
//				'number'      => '3',
//				'block'       => ''
//			)))
//		);
//		$access_token = $oauth_response->access_token;
//		$auth = $this->curloAuth('address/block', $access_token, $params);
//		$this->assertResponseOk();
//		echo $auth;
//	}
//
//	public function testAddressStairs()
//	{
//		$oauth_response = $this->oAuthAccessToken();
//		//var_dump($oauth_response);
//		$params = array (
//			'params' => base64_encode(serialize(array (
//				'ine_code'    => '28079',
//				'rt_id'       => 'CL',
//				'street_name' => 'ALTAMIRANO',
//				'number'      => '48',
//				'stairs'      => ''
//			)))
//		);
//		$access_token = $oauth_response->access_token;
//		$auth = $this->curloAuth('address/stairs', $access_token, $params);
//		$this->assertResponseOk();
//		echo $auth;
//	}
//
//	public function testAddressFloor()
//	{
//		$oauth_response = $this->oAuthAccessToken();
//		//var_dump($oauth_response);
//		$params = array (
//			'params' => base64_encode(serialize(array (
//				'ine_code'    => '28079',
//				'rt_id'       => 'CL',
//				'street_name' => 'ALTAMIRANO',
//				'number'      => '48',
//				'floor'       => ''
//			)))
//		);
//		$access_token = $oauth_response->access_token;
//		$auth = $this->curloAuth('address/floor', $access_token, $params);
//		$this->assertResponseOk();
//		echo $auth;
//	}
//
//	public function testAddressDoor()
//	{
//		$oauth_response = $this->oAuthAccessToken();
//		//var_dump($oauth_response);
//		$params = array (
//			'params' => base64_encode(serialize(array (
//				'ine_code'    => '28079',
//				'rt_id'       => 'CL',
//				'street_name' => 'ALTAMIRANO',
//				'number'      => '48',
//				'floor'       => '02',
//				'door'        => ''
//			)))
//		);
//		$access_token = $oauth_response->access_token;
//		$auth = $this->curloAuth('address/door', $access_token, $params);
//		$this->assertResponseOk();
//		echo $auth;
//	}
//
//	public function testAuthLogin()
//	{
//		$oauth_response = $this->oAuthAccessToken();
//		//var_dump($oauth_response);
//		$params = array (
//				'params' => base64_encode(serialize(array (
//				'email'    => 'a.sanluis@bowbuy.com',
//				'password' => hash('sha256', '1234'),
//				'ip'       => '222.222.22.222',
//				'nav'      => 'TU MADRE',
//			)))
//		);
//		$access_token = $oauth_response->access_token;
//		$auth = $this->curloAuth('auth/login', $access_token, $params);
//		$this->assertResponseOk();
//		echo $auth;
//	}
//
//	public function testAuthJoin()
//	{
//		$oauth_response = $this->oAuthAccessToken();
//		//var_dump($oauth_response);
//		$params = array (
//			'params' => array (
//			'user'      => new stdClass(),
//			'address' => new stdClass()
//		));
//		$params['params']['user']->business_name = 'API KEY PRUEBA';
//		$params['params']['user']->name = "API";
//		$params['params']['user']->first_name = "KEY";
//		$params['params']['user']->last_name = "PRUEBA";
//		$params['params']['user']->doc_type = "NIF";
//		$params['params']['user']->cif_dni = "78585891C";
//		$params['params']['user']->phone = "61596877";
//		$params['params']['user']->gender = "M";
//		$params['params']['user']->birthday = '1980-01-01';
//		$params['params']['user']->email = "API@API.es";
//		$params['params']['user']->ter_type= "F";
//		$params['params']['user']->password=hash('sha256',"1234");
//
//		$params['params']['address']->road_type = 'CL';
//		$params['params']['address']->street = 'ALTAMIRANO';
//		$params['params']['address']->number = '48';
//		$params['params']['address']->stair = '';
//		$params['params']['address']->floor = '2';
//		$params['params']['address']->door = 'IC';
//		$params['params']['address']->block = '';
//		$params['params']['address']->province = 'MADRID';
//		$params['params']['address']->town = 'MADRID';
//		$params['params']['address']->zip_code = '28008';
//		$params['params']['address']->km = '';
//		$params['params']['address']->letter = '';
//
//		$params['params'] =  base64_encode(serialize($params['params']));
//
//		$access_token = $oauth_response->access_token;
//		$auth = $this->curloAuth('auth/join', $access_token, $params);
//		$this->assertResponseOk();
//		echo $auth;
//	}


//	public function testOwnScore()
//	{
//		$oauth_response = $this->oAuthAccessToken();
//		$params = array (
//			'params' => array (
//				'user'      => new stdClass(),
//				'address' => new stdClass()
//			));
//		$params['params']['user']->name = "API";
//		$params['params']['user']->first_name = "KEY";
//		$params['params']['user']->last_name = "PRUEBA";
//		$params['params']['user']->doc_type = "NIF";
//		$params['params']['user']->cif_dni = "78585891C";
//		$params['params']['user']->phone = "61596877";
//		$params['params']['user']->gender = "M";
//		$params['params']['user']->birthday = '1980-01-01';
//		$params['params']['user']->client_id= "3295";
//		$params['params']['user']->assoc_id= "2";//Para saber si es consulta propia
//
//		$params['params']['address']->road_type = 'CL';
//		$params['params']['address']->street = 'ALTAMIRANO';
//		$params['params']['address']->number = '48';
//		$params['params']['address']->floor = '02';
//		$params['params']['address']->door = 'IC';
//		$params['params']['address']->province = 'MADRID';
//		$params['params']['address']->town = 'MADRID';
//		$params['params']['address']->zip_code = '28008';
//		$params['params']['address']->level = '1';
//		$params['params']['address']->type = 'flat';
//
//		$params['params'] =  base64_encode(serialize($params['params']));
//
//		$access_token = $oauth_response->access_token;
//		$auth = $this->curloAuth('score', $access_token, $params);
//		$this->assertResponseOk();
//		echo $auth;
//	}

}
