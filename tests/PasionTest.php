<?php
use App\Http\Controllers\Proxies\Proxies;
use Symfony\Component\DomCrawler\Crawler;

class PasionTest extends TestCase
{

    private $tlf;

    public function testMilanuncios()
    {
        if (($archivo = fopen('Template 10K POC.csv', 'r')) !== FALSE) {
            while (($datos = fgetcsv($archivo, 1000, ";")) !== FALSE) {
                set_time_limit(0);
                $this->tlf = trim($datos[6]);
                var_dump($this->tlf);
                $resp = $this->search();
                file_put_contents('pasion.csv', $datos[4] . ';' . json_encode($resp) . PHP_EOL, FILE_APPEND);

            }
        }
    }

    private function search()
    {
        if (!($result = $this->search_norm())) {
            $status = 400;
            $result = 'No encontrado';
        } else {
            $status = 200;
        }
        return ['hits' => $result, 'status' => $status];
    }

    private function search_norm()
    {
        if (empty($this->tlf)) return false;
        $html_web = 'http://www.pasion.com/contactos/' . $this->tlf . ".htm";
        var_dump($html_web);
        $milanuncio = new \stdClass();
        $milanuncio->anuncio = array();
        $html_search = new Crawler();
        if (!($web_url = $this->curl($html_web))) return false;
        $html_search->addHtmlContent($web_url);
        $titulos = array();
        if ($html_search->filter('#cuerpo > div.x1')->count() > 0) {
            $html_search->filter('#cuerpo > div.x1')->each(function ($node) use ($milanuncio, $titulos) {
                if ($node->filter('div.x7 > a')->count() > 0) {
                    $titulo = $node->filter('div.x7 > a')->first()->text();
                    $url = $node->filter('div.x7 > a')->first()->attr('href');
                    if (!in_array(trim($titulo), $titulos)) {
                        $titulos[] = trim($titulo);
                        $anuncio = new \stdClass();
                        $anuncio->titulo = trim($titulo);
                        $anuncio->url = trim('http://www.pasion.com' . $url);
                        $milanuncio->anuncio[] = $anuncio;
                    }
                }
            });

            if (count($milanuncio->anuncio) > 0) {
                foreach ($milanuncio->anuncio as $anuncio) {
                    unset($crawler);
                    $crawler = new Crawler();
                    if (!($anuncio_url = $this->curl($anuncio->url))) continue;
//                var_dump($anuncio_url);
                    $crawler->addHtmlContent($anuncio_url);
                    //Tipo PROFESIONAL O PARTICULAR y Nombre

//                if ($crawler->filter('.contenido > .contentInnerBoxPagAnu > #cuerpo > .pagAnuRightColumn > .pagAnuContact >.pagAnuContactNombre')->count() > 0) {
//                    $anuncio->contact_name = $crawler->filter('.contenido > .contentInnerBoxPagAnu > #cuerpo > .pagAnuRightColumn > .pagAnuContact >.pagAnuContactNombre')->first()->text();
//
//
//                }
                    if ($crawler->filter('div.pagAnuDatosBox > div.pagAnuCuerpoAnu')->count() > 0) {
                        $anuncio->contenido = $crawler->filter('div.pagAnuDatosBox > div.pagAnuCuerpoAnu')->first()->text();
                    }

//                 $crawler->filter('div.pagAnuFotoBox > div.pagAnuFoto')->each(function ($node) use ($anuncio) {
//                     var_dump($node->html());
//                     $anuncio->fotos[] = $node->filter('img')->first()->attr('src');
//                 });
                    //Quitamos urls
//                unset($anuncio->url);
//
//                $cat_sub = $crawler->filter('.contenido > .cab > .beacrumb-container > .beacrumb > a');
//                if ($cat_sub->count() >= 3) {
//                    $anuncio->categoria = $cat_sub->eq(1)->text();
//                    $anuncio->subcategoria = $cat_sub->eq(2)->text();
//                } elseif ($cat_sub->count() == 2) {
//                    $anuncio->categoria = $cat_sub->eq(1)->text();
//                } else {
//                    continue;
//                }
                }
                return $milanuncio;
            } else return false;
        } else {
            //No se encontraron anuncios
            return false;
        }
    }

    private function curl($page)
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $page);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_PROXY, "37.230.135.15:80");
        curl_setopt($ch, CURLOPT_PROXYUSERPWD, "mrhc:QwEr1234");
        $result = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
        if ($info["http_code"] != 200) return false;
//        var_dump($result);
        return $result;
    }
}
