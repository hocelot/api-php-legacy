<?php
use App\Http\Controllers\Proxies\Proxies;
use Symfony\Component\DomCrawler\Crawler;

class VibboTest extends TestCase
{

    private $tlf, $nombre_via;

    public function testVibbo()
    {
        $this->tlf = '666666';
        $this->nombre_via = 'COCHERAS';

        var_dump($this->search());
    }

//    public function run()
//    {
//        parent::run();
//        $this->worker->addData('vibbo', $this->search());
//    }


    private function curl($page)
    {
        $obj_proxy = new Proxies('vibbo');
        $ip_real = false;
        do {
            $ip = $obj_proxy->get();
            $obj_proxy->setUsed($ip);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $page);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            if ($ip) {
                curl_setopt($ch, CURLOPT_PROXY, $ip);
                curl_setopt($ch, CURLOPT_PROXYUSERPWD, "mrhc:QwEr1234");
            } else  $ip_real = true;
            $result = curl_exec($ch);
            $info = curl_getinfo($ch);
            curl_close($ch);
            if (!$ip_real) {
                $obj_proxy->unlock($info["http_code"]);
            }
        } while ($info["http_code"] == 400 && !$ip_real);
        if ($info["http_code"] == 400 && $ip_real) return false;
//        var_dump($result);
        return $result;
    }

    public function search()
    {
        $vibbo = new stdClass();
        $vibbo->score = 'OK';
        $vibbo->hits = [];
        $vibbo->anuncio_urls = [];

        $crawler = new Crawler();
        $page = 1;

        do {
            $html_web = 'https://www.vibbo.com/anuncios-toda-espana/' . $this->tlf . '.htm?ca=0_s&x=3&w=3&c=0&o=' . $page;
            $crawler->clear();
            if ($resp = $this->curl($html_web)) {
                $crawler->addHtmlContent($resp);

                if ($crawler->filter('div[class="basicList flip-container list_ads_row  "]')->count() > 0) {
                    $crawler->filter('div[class="basicList flip-container list_ads_row  "]')->each(function ($node) use ($vibbo) {
                        $url = preg_replace('|//|', 'https://', $node->filter('div > div > div > div > div > a')->attr('href'));
                        if (!in_array($url, $vibbo->anuncio_urls)) {
                            $vibbo->anuncio_urls[] = $url;
                        }
                    });
                }
                $page++;
            } else break;
        } while ($crawler->filter('a[class="box-icon-filter box-icon-pagination-top"] > span[class="icon-ics_navigation_ic_Forward"]')->count() > 0);

        if (count($vibbo->anuncio_urls) > 0) {
            foreach ($vibbo->anuncio_urls as $anuncio_url) {
                $crawler->clear();
                $crawler->addHtmlContent($this->curl($anuncio_url));
                $anuncio = new stdClass();
                $anuncio->url = $anuncio_url;
                if ($crawler->filter('.sellerBox__info__name')->count() > 0) {
                    $anuncio->alias = trim(mb_strtolower($crawler->filter('.sellerBox__info__name')->text()));
                }
                if ($crawler->filter('#breadcrumbsContainer > p > a')->eq(4)->count() > 0) {
                    $anuncio->type = trim(mb_strtolower($crawler->filter('#breadcrumbsContainer > p > a')->eq(4)->text()));
                    if ($anuncio->type != 'venta' || $anuncio->type != 'compra') {
                        $anuncio->type = 'venta';
                    }
                }
                if ($crawler->filter('#breadcrumbsContainer > p > a')->eq(2)->count() > 0) {
                    $anuncio->category = trim(mb_strtolower($crawler->filter('#breadcrumbsContainer > p > a')->eq(2)->text()));
                }
                if ($crawler->filter('h1.productTitle')->count() > 0) {
                    $anuncio->titulo = trim($crawler->filter('h1.productTitle')->text());
                }

                if ($crawler->filter('#descriptionText')->count() > 0) {
                    $anuncio->cuerpo = '';
                    $eq_max = $crawler->filter('#descriptionText')->count();
                    $eq = 0;
                    do {
                        $anuncio->cuerpo .= ' ' . $crawler->filter('#descriptionText')->eq($eq)->text();
                        $eq++;
                    } while ($eq < $eq_max);
                    $anuncio->cuerpo = trim($anuncio->cuerpo);
                }

                if ($crawler->filter('.priceTags > .priceLevel1')->count() > 0) {
                    $anuncio->price = str_replace('.', '', str_replace('€', '', trim($crawler->filter('.priceTags > .priceLevel1')->text())));
                }

                if ((isset($anuncio->price) && $anuncio->price > 3000) &&
                    ((isset($anuncio->cuerpo) && (stristr($anuncio->cuerpo, $this->nombre_via) ||
                                stristr($anuncio->cuerpo, mb_strtolower($this->nombre_via)))) ||
                        (isset($anuncio->titulo) && stristr($anuncio->titulo, mb_strtolower($this->nombre_via)) ||
                            stristr($anuncio->titulo, mb_strtolower($this->nombre_via))))
                ) {
                    if (isset($anuncio->type) && $anuncio->type == 'venta')
                        $vibbo->score = 'KO';
                    elseif (!isset($anuncio->type)) $vibbo->score = 'KO';
                }
                $vibbo->hits[] = $anuncio;
            }


        }
        unset($vibbo->anuncio_urls);

        return $vibbo;
    }
}
