<?php
class ApiTest extends TestCase {
    public function testScore()
	{
		$oauth_response = $this->oAuthAccessToken();
		$params = array (
			'params' => array (
				'user'      => new stdClass(),
				'address' => new stdClass()
			));
		$params['params']['user']->name = "Pedro";
		$params['params']['user']->first_name = "Almodovar";
		$params['params']['user']->last_name = "PRUEBA";
		$params['params']['user']->doc_type = "NIF";
		$params['params']['user']->cif_dni = "03559389R";
		$params['params']['user']->phone = "61596877";
		$params['params']['user']->gender = "M";
        $params['params']['user']->email= "prueba@djkdjkds.es";
		$params['params']['user']->birthday = '1992-01-01';

		$params['params']['address']->road_type = 'CL';
		$params['params']['address']->street = 'AGUSTIN DURAN';
		$params['params']['address']->number = '24';
		$params['params']['address']->floor = '00';
		$params['params']['address']->door = 'YD';
        $params['params']['address']->stair = '1';
        $params['params']['address']->km = '';
        $params['params']['address']->block = '';
		$params['params']['address']->province = 'MADRID';
		$params['params']['address']->town = 'MADRID';
		$params['params']['address']->zip_code = '28028';
		$params['params']['address']->level = '1';
		$params['params']['address']->type = 'flat';


		$access_token = $oauth_response->access_token;

		$auth = $this->curloAuth('testcliente', $access_token, $params);
        print_r($auth,TRUE);

	}
}
