<?php

/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 19/07/16
 * Time: 20:42
 */
class KeysGenerator extends TestCase
{
    public function testgenerateKeys(){
        $config = array(
            "digest_alg" => "sha512",
            "private_key_bits" => 8192,
            "private_key_type" => OPENSSL_KEYTYPE_RSA,
        );

        // Create the private and public key
        $res = openssl_pkey_new($config);

        // Extract the private key from $res to $privKey
        openssl_pkey_export($res, $privKey);
        $private64= base64_encode($privKey);

        // Extract the public key from $res to $pubKey

        $pubKey = openssl_pkey_get_details($res);
        $pubKey = $pubKey["key"];
        $public64= base64_encode($pubKey);
        echo $private64,PHP_EOL;

        echo $public64,PHP_EOL;

//        $response = json_encode(['public_key'=>$public64,'private_key'=>$private64]);
       // var_dump (json_decode($response),true);
//        $data = 'plaintext data goes here';
//        $oauth_clients_keys = array(
//            'id' => "mr_UdyAtQkqcKtw8yoRFe7WlJwp9HuGoyRZUcAHj",
//            'private_key' => base64_encode($privKey),
//            'public_key' => base64_encode($pubKey)
//        );
////mr_UdyAtQkqcKtw8yoRFe7WlJwp9HuGoyRZUcAHj
//        app('db')->connection('oauth')->table('oauth_clients_keys')->insert($oauth_clients_keys);
//
////        // Encrypt the data to $encrypted using the public key
//        openssl_public_encrypt($data, $encrypted, $pubKey);
////
////
////        // Decrypt the data using the private key and store the results in $decrypted
//       openssl_private_decrypt($encrypted, $decrypted, $privKey);
////
//      echo $decrypted;
    }
}
