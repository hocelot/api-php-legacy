<?php
use App\Http\Controllers\GeoCheck\GeoCheck;
use Carbon\Carbon;

/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 16/07/16
 * Time: 12:37
 */
class GeoCheckTest extends TestCase
{
    public function testexist()
    {
        $busqueda_exacta = true;
        $rt = 'CL';
        $street = 'ADOLFO MARSILLACH';
        $pnp = '56';
        $planta = '3';
        $puerta = '1';
        $ine = '28079';
        $letra = $escalera = $km = $bloque = '';


        $querybool = new \Elastica\Query\BoolQuery();
        //Añadimos filtro genérico (.raw antiguo .keyword)
        $filter_tipovia = new \Elastica\Query\Term();
        $filter_tipovia->setTerm('tipodevia.raw', $rt);
        $querybool->addFilter($filter_tipovia);

        $filter_pnp = new \Elastica\Query\Term();
        $filter_pnp->setTerm('primernumeropolicia.raw', $pnp);
        $querybool->addFilter($filter_pnp);


        if (!empty($planta)) {
            if (strlen($planta) == 1) $planta = '0' . $planta;
            $filter_planta = new \Elastica\Query\Term();
            $filter_planta->setTerm('planta.raw', $planta);
            $querybool->addFilter($filter_planta);
        }

        //Añadimos match nombredevia y primernumeropolicia
        if (!$busqueda_exacta) {
            $field = 'nombredevia';
            $operator = 'and';
            $fuzziness = "AUTO";
            $street_match = new \Elastica\Query\Match();
            $street_match->setFieldQuery($field, $street);
            $street_match->setFieldOperator($field, $operator);
            $street_match->setFieldFuzziness($field, $fuzziness);
            $querybool->addMust($street_match->toArray());
        } else {
            $field = 'nombredevia';
            $street_match = new \Elastica\Query\Match();
            $street_match->setFieldQuery($field, $street);
            $querybool->addMust($street_match->toArray());
        }

        //Filtramos que los inmuebles que vengan sean distinto de almacen
//        $filter_uso = new \Elastica\Query\Match();
//        $filter_uso->setFieldOperator('uso','OR');
        $filter_uso = new \Elastica\Query\MatchPhrasePrefix('uso', 'A');
        $querybool->addMustNot($filter_uso->toArray());

        if (!empty($letra)) {
            $field = 'letra';
            $operator = 'and';
            $fuzziness = "AUTO";
            $letra_match = new \Elastica\Query\Match();
            $letra_match->setFieldQuery($field, $letra);
            $letra_match->setFieldOperator($field, $operator);
            $letra_match->setFieldFuzziness($field, $fuzziness);
            $querybool->addMust($letra_match->toArray());
        }
        if (!empty($km)) {
            $field = 'kilometro';
            $operator = 'and';
            $fuzziness = "AUTO";
            $km_match = new \Elastica\Query\Match();
            $km_match->setFieldQuery($field, $km);
            $km_match->setFieldOperator($field, $operator);
            $km_match->setFieldFuzziness($field, $fuzziness);
            $querybool->addMust($km_match->toArray());
        }
        if (!empty($bloque)) {
            $field = 'bloque';
            $operator = 'and';
            $fuzziness = "AUTO";
            $bloque_match = new \Elastica\Query\Match();
            $bloque_match->setFieldQuery($field, $bloque);
            $bloque_match->setFieldOperator($field, $operator);
            $bloque_match->setFieldFuzziness($field, $fuzziness);
            $querybool->addMust($bloque_match->toArray());
        }
        if (!empty($escalera)) {
            $field = 'escalera';
            $operator = 'and';
            $fuzziness = "AUTO";
            $escalera_match = new \Elastica\Query\Match();
            $escalera_match->setFieldQuery($field, $escalera);
            $escalera_match->setFieldOperator($field, $operator);
            $escalera_match->setFieldFuzziness($field, $fuzziness);
            $querybool->addMust($escalera_match->toArray());
        }

        if (!empty($puerta)) {
            $field = 'puerta';
            $operator = 'and';
            $f = $puerta;
            if (strlen($puerta) < 2 && intval($puerta)) {
                $operator = 'or';
                $f = '0' . $puerta . ' ' . $puerta;
            }
            $fuzziness = "AUTO";
            $puerta_match = new \Elastica\Query\Match();
            $puerta_match->setFieldQuery($field, $f);
            $puerta_match->setFieldOperator($field, $operator);
            $puerta_match->setFieldFuzziness($field, $fuzziness);
            $querybool->addMust($puerta_match->toArray());
        }
        $query = [
            'query' => $querybool->toArray()
        ];
//        Log::debug('ELASTIC busco' . print_r($query, true));
        $arr = $this->elastica_search('catastro', $ine, $query)->getResults();//->getAggregation("street_names")["buckets"];
        Log::debug('ELASTIC RESULT' . print_r($arr, true));
        if (count($arr) > 0) {
            $result = $arr;
        } else $result = false;

        var_dump($result);

    }

    public function elastica_search($index, $type, $search_query)
    {
        $client = new \Elastica\Client(array(
            'host' => '192.168.1.3',
            'port' => 9200
        ));
        if ($index == 'catastro') {
            //Vemos si el codigo ine existe en ese indice, si no existe estara en catastro15
//            Log::debug('CATASTRO EXISTE? indice '.$client->getIndex($index)->getType($type)->exists());
            if (!$client->getIndex($index)->getType($type)->exists()) {
                $index = 'catastro15';
            }
        }
        $query = \Elastica\Query::create($search_query);
        return $client->getIndex($index)->getType($type)->search($query, 50);

    }

    public function testmienda()
    {
        $str = 'BEETHOVEN, LUDWIG';
        //SOLUCION DOBLE CHELK
        if ($pos_coma = strpos($str, ',')) {
            $final_via = trim(substr($str, 0, $pos_coma));
            $p_via = trim(substr($str, $pos_coma + 1));
            $str = $p_via . ' ' . $final_via;
            echo $str . PHP_EOL;
        }
        similar_text('LUDWIG BEETHOVEN', $str, $percent);

        var_dump($percent);
//                if (in_array(strtoupper(substr($this->door, 0, 1)), $array_abc)
    }


    public function testsearch_address_fenosa()
    {
        $cp = '37799';
        $street = 'MERCED';
        $number = '21';
        $response = $this->curl_fenosa("https://contrata.gasnaturalfenosa.es/mascaraxpress/api/findCP.json?text=" . $cp);
        if ($response->status) {
            foreach ($response->data as $data) {
                $result = $this->curl_fenosa("https://contrata.gasnaturalfenosa.es/mascaraxpress/api/findCUPSByAddress.json?cp=" . $data->cp . "&town=" . urlencode($data->town) . "&energy=luz&text=" . urlencode($street . ' ' . $number));
                if ($result->status) {
                    if (count($result->data) > 1) {
                        // Más de una direccion 'flat'
                        break;
                    } elseif (count($result->data) == 1) {
                        // house
                        break;
                    }
                }
            }
        } else { //No existe direccion
        }
    }

    private function curl_fenosa($url)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        return json_decode($response);
    }

    public function testExplode()
    {
        $curl = curl_init();
        //m527868@mvrht.com
        //Pass: 123456789Abcd
        //Gratis
//        $app_id = 'BnboVFMxgw7vEvfqnjueL';
//        $app_code = 'wKwo7LGdj-9SCnfQectjMw';
//Premium
        $app_id = 'y08K6Yp96UADcv797dmX';
        $app_code = '_4yy7Yc__yhlbqqaDgFISg';

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://geocoder.cit.api.here.com/6.2/geocode.json?searchtext=" . urlencode("CL DE L'ARGENTERA 21, REUS, TARRAGONA") . "&gen=9&app_id=" . $app_id . "&app_code=" . $app_code,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $response = json_decode($response);
            if (isset($response->Response->View[0]->Result[0]->MatchQuality->County) &&
                isset($response->Response->View[0]->Result[0]->MatchQuality->City) &&
                isset($response->Response->View[0]->Result[0]->MatchQuality->Street) &&
                isset($response->Response->View[0]->Result[0]->MatchQuality->HouseNumber)
            ) {

                if ($response->Response->View[0]->Result[0]->MatchQuality->County > 0.5 &&
                    $response->Response->View[0]->Result[0]->MatchQuality->City > 0.5 &&
                    $response->Response->View[0]->Result[0]->MatchQuality->Street[0] > 0.5 &&
                    $response->Response->View[0]->Result[0]->MatchQuality->HouseNumber > 0.5
                ) {
                    //Nivel 1

                } else {
                    //Nivel 5
                }
                if (isset($response->Response->View[0]->Result[0]->NavigationPosition[0]->Latitude))
                    $latitude = $response->Response->View[0]->Result[0]->NavigationPosition[0]->Latitude;
                if (isset($response->Response->View[0]->Result[0]->NavigationPosition[0]->Longitude))
                    $longitude = $response->Response->View[0]->Result[0]->NavigationPosition[0]->Longitude;


            }

            if (isset($response->Response->View[0]->Result[0]->Address->County))
                $country = $response->Response->View[0]->Result[0]->Address->County;
            if (isset($response->Response->View[0]->Result[0]->Address->City))
                $city = $response->Response->View[0]->Result[0]->Address->City;
            if (isset($response->Response->View[0]->Result[0]->Address->Street))
                $street = $response->Response->View[0]->Result[0]->Address->Street;
            if (isset($response->Response->View[0]->Result[0]->Address->HouseNumber))
                $house_number = $response->Response->View[0]->Result[0]->Address->HouseNumber;


            $otro_numero = false;
            foreach ($response->Response->View[0]->Result[0]->AdditionalData as $additionalData) {
                if ($additionalData->key == "houseNumberFallbackDifference")
                    if ($additionalData->value > 0) $otro_numero = true; //otro numero
            }

            if($otro_numero){
                //Nivel 5
            }else{
                //Nivel 1?
            }
        }

    }

    public function testmierda (){
        $calle = 'LAGOS';
        stristr('PASEO LOS LAGOS 2  URB LA FINCA   BARRIO LA FINCA   DISTRITO ZONA PRADO DE SOMOSAGUAS - LA FINCA   POZUELO DE ALARCÓN   ZONA NOROESTE MADRID')
    }


}