<?php

use Symfony\Component\DomCrawler\Crawler;

class SearxmlTest extends TestCase
{
    /*
     * Carrot Format:
     <searchresult>
           <query>query (optional)</query>
           <document>
              <title>Document 1 Title</title>
              <snippet>Document 1 Content.</snippet>
              <url>http://document.url/1</url>
           </document>
           <document>
              <title>Document 2 Title</title>
              <snippet>Document 2 Content.</snippet>
              <url>http://document.url/2</url>
           </document>
           <document>
              <title>Document 3 Title</title>
              <snippet>Document 3 Content.</snippet>
              <url>http://document.url/3</url>
           </document>
        </searchresult>
     */

    public function testCarrot()
    {


        $unit_results = array();
        //eTools
        $etools = $this->eTools();
        //Searxme
        $searxme = $this->searxme();

        foreach ($searxme['hits'] as $hit) {
            if (!in_array($hit['url'], $etools['urls'])) {
                $etools['hits'][] = $hit;
            }
        }
        unset($etools['urls']);
        unset($searxme);
        // var_dump($etools);
        $xml = new SimpleXMLElement('<?xml version=\'1.0\' encoding=\'utf-8\'?><searchresult/>');

        $xml->addChild('query', '"ricardo marcos"');
        foreach($etools['hits'] as $hit) {
            $document = $xml->addChild('document');
            $document->addChild('title', htmlspecialchars($hit['title']));
            $document->addChild('snippet', htmlspecialchars($hit['text']));
            $document->addChild('url', htmlspecialchars($hit['url']));
        }
//var_dump($xml->asXML());
       $xml->saveXML('carrot_searx_etools.xml');

/*
 * my_url = 'http://localhost:8080/dcs/rest?dcs.c2stream=xml'
xml_string = etree.tostring(xml)
http.request(my_url, 'POST', body=xml_string, headers={'Content-type': 'application/x-www-form-urlencoded'})
 * */
        $ch = curl_init();
//        $fields['dcs.c2stream'] = $this->generateXml($documents);
//    }
//self::addIfNotNull($fields, 'dcs.source', $job->getSource());
//self::addIfNotNull($fields, 'dcs.algorithm', $job->getAlgorithm());
//self::addIfNotNull($fields, 'query', $job->getQuery());
        $fields['dcs.c2stream'] = $xml->asXML();
        //$fields['dcs.source'] = 'xml';
        curl_setopt_array($ch,
            array(
                CURLOPT_URL => "http://localhost:8080/dcs/rest",
                CURLOPT_POST           => true,
                CURLOPT_HTTPHEADER     => array('Content-Type: multipart/formdata'),
                CURLOPT_HEADER         => false,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POSTFIELDS     => $fields
            )
        );

        $result = curl_exec($ch);
        curl_close($ch);
        var_dump($result);


    }

    private function eTools()
    {
        $results = new stdClass();
        $crawler = new Crawler();
        $crawler->addXmlContent(file_get_contents('ricardo+marcos.xml'));
        $crawler->filterXPath('//result/mergedRecords/record')
            ->each(function ($node) use ($results) {
                $aux['title'] = $node->filter('title')->text();
                $aux['text'] = $node->filter('text')->text();
                $aux['url'] = $node->filter('url')->text();
                $results->urls[] = $aux['url'];
                $results->hits[] = $aux;
            });
        return array('hits' => $results->hits, 'urls' => $results->urls);
    }


    private function searxme()
    {
        $site = 'https://searx.laquadrature.net/';
        $str_search = urlencode('"ricardo marcos"');
        //85.214.29.216/searx
        $search = $this->curl($site . '?q=' . $str_search . '&categories=general&format=json');
        if (!empty($search->results)) {
            $res = new stdClass();
            foreach ($search->results as $resultados) {
                $res->hits[] = array('title' => $resultados->title, 'text' => $resultados->content, 'url' => $resultados->url);
            }
            return array('hits' => $res->hits);
        } else {
            return false;
        }
    }

    private
    function curl($page)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $page);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result);
    }
}
