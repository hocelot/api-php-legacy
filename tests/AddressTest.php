<?php

/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 16/07/16
 * Time: 12:37
 */
class AddressTest extends TestCase
{
    public function testAddressTown()
    {

        $oauth_response = $this->oAuthAccessToken();
//        print_r($oauth_response);
        $params = array(
            'params' => array(
                'zip_code' => '28008'
            )
        );
//        $params=json_encode(['params' => array(
//            'zip_code' => '28008'
//        )]);
        $access_token = $oauth_response->access_token;
        $auth = $this->curloAuth('address/town', $access_token, $params);
        $this->assertResponseOk();
        if($auth->status == 400){
            echo $auth->towns;
        }else {
            var_dump($auth);
            $d_desencrypt = $this->desencryptResponse($auth->towns);
            var_dump($d_desencrypt);
        }
    }

    public function testAddressIneCode()
    {
        $oauth_response = $this->oAuthAccessToken();
        //var_dump($oauth_response);
        $params = array(
            'params' => array(
                'zip_code' => '28008',
                'town' => 'Madrid'
            )
        );
        $access_token = $oauth_response->access_token;
        $auth = $this->curloAuth('address/code', $access_token, $params);
        $this->assertResponseOk();
        if($auth->status == 400){
            echo $auth->code;
        }else {
            $d_desencrypt = $this->desencryptResponse($auth->code);
            echo json_decode($d_desencrypt);
        }
    }

    public function testAddressRoadType()
    {
        $oauth_response = $this->oAuthAccessToken();
        //var_dump($oauth_response);
        $params = array(
            'params' => array(
                'ine_code' => '1'
            )
        );
        $access_token = $oauth_response->access_token;
        $auth = $this->curloAuth('address/roadtype', $access_token, $params);
        $this->assertResponseOk();
        echo $auth;
    }

    public function testAddressStreetName()
    {
        $oauth_response = $this->oAuthAccessToken();
        //var_dump($oauth_response);
        $params = array(
            'params' => array(
                'ine_code' => '28079',
                'rt_id' => 'CL',
                'street_name' => 'ALTAMIR'
            )
        );
        $access_token = $oauth_response->access_token;
        $auth = $this->curloAuth('address/streetname', $access_token, $params);
        $this->assertResponseOk();
        echo $auth;
    }

    public function testAddressNumber()
    {
        $oauth_response = $this->oAuthAccessToken();
        //var_dump($oauth_response);
        $params = array(
            'params' =>array(
                'ine_code' => '28079',
                'rt_id' => 'CL',
                'street_name' => 'ALTAMIRANO',
                'number' => '4'
            )
        );
        $access_token = $oauth_response->access_token;
        $auth = $this->curloAuth('address/number', $access_token, $params);
        $this->assertResponseOk();
        echo $auth;
    }

    public function testAddressLetter()
    {
        $oauth_response = $this->oAuthAccessToken();
        //var_dump($oauth_response);
        $params = array(
            'params' => array(
                'ine_code' => '28079',
                'rt_id' => 'CL',
                'street_name' => 'ALTAMIRANO',
                'number' => '4',
                'letter' => ''
            )
        );
        $access_token = $oauth_response->access_token;
        $auth = $this->curloAuth('address/letter', $access_token, $params);
        $this->assertResponseOk();
        echo $auth;
    }

    public function testAddressKm()
    {
        $oauth_response = $this->oAuthAccessToken();
        //var_dump($oauth_response);
        $params = array(
            'params' => array(
                'ine_code' => '28079',
                'rt_id' => 'CL',
                'street_name' => 'ALTAMIRANO',
                'number' => '4',
                'km' => ''
            )
        );
        $access_token = $oauth_response->access_token;
        $auth = $this->curloAuth('address/km', $access_token, $params);
        $this->assertResponseOk();
        echo $auth;
    }

    public function testAddressBlock()
    {
        $oauth_response = $this->oAuthAccessToken();
        //var_dump($oauth_response);
        $params = array(
            'params' => array(
                'ine_code' => '28079',
                'rt_id' => 'CL',
                'street_name' => 'PRINCESA',
                'number' => '3',
                'block' => ''
            )
        );
        $access_token = $oauth_response->access_token;
        $auth = $this->curloAuth('address/block', $access_token, $params);
        $this->assertResponseOk();
        echo $auth;
    }

    public function testAddressStairs()
    {
        $oauth_response = $this->oAuthAccessToken();
        //var_dump($oauth_response);
        $params = array(
            'params' => array(
                'ine_code' => '28079',
                'rt_id' => 'CL',
                'street_name' => 'ALTAMIRANO',
                'number' => '48',
                'stairs' => ''
            )
        );
        $access_token = $oauth_response->access_token;
        $auth = $this->curloAuth('address/stairs', $access_token, $params);
        $this->assertResponseOk();
        echo $auth;
    }

    public function testAddressFloor()
    {
        $oauth_response = $this->oAuthAccessToken();
        //var_dump($oauth_response);
        $params = array(
            'params' => array(
                'ine_code' => '28079',
                'rt_id' => 'CL',
                'street_name' => 'ALTAMIRANO',
                'number' => '48',
                'floor' => ''
            )
        );
        $access_token = $oauth_response->access_token;
        $auth = $this->curloAuth('address/floor', $access_token, $params);
        $this->assertResponseOk();
        echo $auth;
    }

    public function testAddressDoor()
    {
        $oauth_response = $this->oAuthAccessToken();
        //var_dump($oauth_response);
        $params = array(
            'params' => array(
                'ine_code' => '28079',
                'rt_id' => 'CL',
                'street_name' => 'ALTAMIRANO',
                'number' => '48',
                'floor' => '02',
                'door' => ''
            )
        );
        $access_token = $oauth_response->access_token;
        $auth = $this->curloAuth('address/door', $access_token, $params);
        $this->assertResponseOk();
        echo $auth;
    }
    public function testMierda(){
        $str = 'h**********0@gmail.com';
        var_dump(substr($str,strpos($str,'@')-1));
    }
}
