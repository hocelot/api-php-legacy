<?php

class ProxiesUpdateTest extends TestCase
{
    public function testupdate()
    {
        $date = date('c');
        $status = "OK";
        $cc = 0;
        $c = curl_init();
        curl_setopt_array($c, array(
            CURLOPT_URL => "http://api.buyproxies.org/?a=showProxies&pid=69478&key=00d5a9255988062e90baf79491af5f93",
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_FOLLOWLOCATION => TRUE
        ));
        $data = curl_exec($c);
        curl_close($c);
        $str = str_replace(':mrhc:QwEr1234', ';', trim($data));
        $str = explode(';', $str);

        app('db')->connection('apidb')->table('proxies')->truncate();
        foreach ($str as $proxy) {
            if (!empty($proxy)) {
                app('db')->connection('apidb')->table('proxies')->insert([
                    ['ip' => trim($proxy)]
                ]);
            }
        }
    }

    public function testCatastro()
    {
        $result = $this->curl_cat('ovc.catastro.meh.es/ovcservweb/OVCSWLocalizacionRC/OVCCallejero.asmx/ConsultaProvincia?');
        $xml_provincias = new SimpleXMLElement($result);
        foreach ($xml_provincias->provinciero->prov as $provincia) {
            $prov = trim((string)$provincia->np);
            $page = 'ovc.catastro.meh.es/ovcservweb/OVCSWLocalizacionRC/OVCCallejero.asmx/ConsultaMunicipio?Provincia=' . urlencode($prov) . '&Municipio=';
            $result = $this->curl_cat($page);
            $xml_mun = new SimpleXMLElement($result);
            foreach ($xml_mun->municipiero->muni as $muni) {
                $codine = str_pad((string)$muni->loine->cp, 2, '0', STR_PAD_LEFT) . str_pad((string)$muni->loine->cm, 3, '0', STR_PAD_LEFT);
                $municipio = trim((string)$muni->nm);
                app('db')->connection('catastro')->table('codine_pob')->insert([
                    ['codine' => $codine,
                        'provincia' => utf8_decode($prov),
                        'poblacion' => utf8_decode($municipio)]
                ]);
            }

        }
    }

    private $count = 0;

    private function curl_cat($page)
    {
        $this->count++;
        if ($this->count == 3000) sleep(3600);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $page);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        /*PROBAR*/
//        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml; charset=utf-8'));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}