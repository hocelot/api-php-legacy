<?php

class AuthTest extends TestCase
{

    public function testAuthLogin()
    {
        $oauth_response = $this->oAuthAccessToken();
        //var_dump($oauth_response);
        $params = array(
            'params' => base64_encode(serialize(array(
                'email' => 'a.sanluis@bowbuy.com',
                'password' => hash('sha256', '1234'),
                'ip' => '222.222.22.222',
                'nav' => 'TU MADRE',
            )))
        );
        $access_token = $oauth_response->access_token;
        $auth = $this->curloAuth('auth/login', $access_token, $params);
        $this->assertResponseOk();
        echo $auth;
    }

    public function testAuthJoin()
    {
        $oauth_response = $this->oAuthAccessToken();
        //var_dump($oauth_response);
        $params = array(
            'params' => array(
                'user' => new stdClass(),
                'address' => new stdClass()
            ));
        $params['params']['user']->business_name = 'API KEY PRUEBA';
        $params['params']['user']->name = "API";
        $params['params']['user']->first_name = "KEY";
        $params['params']['user']->last_name = "PRUEBA";
        $params['params']['user']->doc_type = "NIF";
        $params['params']['user']->cif_dni = "78585891C";
        $params['params']['user']->phone = "61596877";
        $params['params']['user']->gender = "M";
        $params['params']['user']->birthday = '1980-01-01';
        $params['params']['user']->email = "API@API.es";
        $params['params']['user']->ter_type = "F";
        $params['params']['user']->password = hash('sha256', "1234");

        $params['params']['address']->road_type = 'CL';
        $params['params']['address']->street = 'ALTAMIRANO';
        $params['params']['address']->number = '48';
        $params['params']['address']->stair = '';
        $params['params']['address']->floor = '2';
        $params['params']['address']->door = 'IC';
        $params['params']['address']->block = '';
        $params['params']['address']->province = 'MADRID';
        $params['params']['address']->town = 'MADRID';
        $params['params']['address']->zip_code = '28008';
        $params['params']['address']->km = '';
        $params['params']['address']->letter = '';

        $params['params'] = base64_encode(serialize($params['params']));

        $access_token = $oauth_response->access_token;
        $auth = $this->curloAuth('auth/join', $access_token, $params);
        $this->assertResponseOk();
        echo $auth;
    }

}
