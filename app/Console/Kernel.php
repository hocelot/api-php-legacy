<?php

namespace App\Console;

use App\Http\Controllers\BatchController;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //Batch Schedule
        $schedule->call(function () {
            $dir = new \FilesystemIterator('/var/www/hocelot/files/batch', \FilesystemIterator::SKIP_DOTS);
            if(iterator_count($dir) > 0){
                $filesnames = array();
                foreach ($dir as $fileinfo) {
                    $filesnames[]='/var/www/hocelot/files/processing/'.$fileinfo->getFilename();
                    rename('/var/www/hocelot/files/batch/'.$fileinfo->getFilename(), '/var/www/hocelot/files/processing/'.$fileinfo->getFilename());
                }
                BatchController::process($filesnames);
            }
        })->everyMinute();

    }
}
