<?php

/**
 * Created by PhpStorm.
 * User: a.luis
 * Date: 08/04/2016
 * Time: 10:53
 */

namespace App\Libraries;
use Log;
class SmsAltiria
{

    var $vars_auth = array();
    var $debug = false;

    public function __construct()
    {
        if ($this->debug) {
            $this->vars_auth[0] = 'demopr';
            $this->vars_auth[1] = 'bowbuy';
            $this->vars_auth[2] = 'nphcuekq';
            $this->vars_auth[3] = 'bowbuy';
        } else {
            $this->vars_auth[0] = 'siracusa';
            $this->vars_auth[1] = 'siracusa';
            $this->vars_auth[2] = 'si3rAc2015';
            $this->vars_auth[3] = 'HOCELOT';
        }
    }

    public function sendMessage($movil, $verif_code)
    {

//        $verif_code = 'demo_' . rand(10000000, 99999999);
        $mensaje = 'Bienvenido a Hocelot. Aquí tienes tu clave: ' . $verif_code;

        $sData = 'cmd=sendsms&';
        $sData .= 'domainId=' . $this->vars_auth[0] . '&';
        $sData .= 'login=' . $this->vars_auth[1] . '&';
        $sData .= 'passwd=' . $this->vars_auth[2] . '&';
        $sData .= 'dest=' . str_replace(',', '&dest=', $movil) . '&';
        $sData .= 'senderId=' . $this->vars_auth[3] . '&';
        //$sData .= "msg=".urlencode(utf8_encode(substr($sMessage,0,160)));
        $sData .= 'msg=' . urlencode(substr($mensaje, 0, 160));
        $fp = fsockopen('www.altiria.net', 80, $errno, $errstr, 10);
        if (!$fp) {
            //Error de conexion
            $output = "ERROR de conexion: $errno - $errstr\n";
            $output .= "Compruebe que ha configurado correctamente la direccion/url ";
            $output .= "suministrada por altiria\n";
            Log::debug($output);
        } else {
            $buf = "POST /api/http HTTP/1.0\r\n";
            $buf .= "Host: www.altiria.net\r\n";
            $buf .= "Content-type: application/x-www-form-urlencoded; charset=UTF-8\r\n";
            $buf .= "Content-length: " . strlen($sData) . "\r\n";
            $buf .= "\r\n";
            $buf .= $sData;
            fputs($fp, $buf);
            $buf = "";
            while (!feof($fp))
                $buf .= fgets($fp, 128);
            fclose($fp);
            //Si la llamada se hace con debug, se muestra la respuesta completa del servidor
            if ($this->debug) {
                Log::debug("Respuesta del servidor: \n" . $buf . "\n");
            }

            //Se comprueba que se ha conectado realmente con el servidor
            //y que se obtenga un codigo HTTP OK 200
//            Log::debug(print_r($buf, true));
            if (strpos($buf, "200 OK") === false) {
                $output = "ERROR. Codigo error HTTP: " . substr($buf, 9, 3) . "\n";
                $output .= "Compruebe que ha configurado correctamente la direccion/url ";
                $output .= "suministrada por Altiria\n";
                Log::debug($output);
            }

            //Se comprueba la respuesta de Altiria
            if (strstr($buf, "ERROR")) {
                $output = $buf . "\n";
                $output .= " Codigo de error de Altiria. Compruebe la especificacion\n";
                Log::debug($output);
            }

        }

        return $verif_code;
    }
}