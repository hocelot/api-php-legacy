<?php

namespace App\Http\Middleware;

use Closure;
use Log;
class cPanelMiddleware
{

    public function handle($request, Closure $next)
    {
        Log::debug('IP MIDLEWARE '.$request->ip());
        if (substr($request->ip(),0,10) == '192.168.74' || substr($request->ip(),0,10) == '192.168.79.' || substr($request->ip(),0,10) == '192.168.1.' )
        {
            return $next($request);

        }
        else
        {

            header("Location: http://api.hocelot.com");
            die();
        }
	}
}