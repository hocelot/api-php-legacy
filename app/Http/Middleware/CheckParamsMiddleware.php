<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 18/06/16
 * Time: 0:19
 */

namespace App\Http\Middleware;


use Closure;
use Illuminate\Support\Facades\Log;

class CheckParamsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $check_array = $this->selectType($request->path());
        if (!empty($check_array)) {
            if (!$request->has('params')) {
                return response()->json(['msg' => 'Error el array params no declarado','status'=>400]);
            } else {
                $params = unserialize(base64_decode($request->input('params')));
                $msg = $this->checkParams($params, $check_array);
                if (!empty($msg)) {
                    return response()->json(['msg' => $msg,'status'=>400]);
                }
            }
        }
        return $next($request);
    }

    private
    function selectType($path)
    {
        $arr = '';
        /*
         * Idea meter serializado el array en base de datos y rescatarlo 
         * $request->path();
         * */
        $db_mr = app('db')->connection('mr_test');
        $resp = $db_mr->table('middleware_params')->where('mid_id', $path)->first();
        if(!empty($resp->array)){
            $arr=unserialize($resp->array);
        }
        return $arr;
    }

    private
    function checkParams($params, $array_format)
    {
        $msg = '';
        foreach ($array_format as $key => $value) {
            if (is_array($value)) {
                if (!isset($params[$key])) {
                    $msg = 'El array ' . $key . ' no ha sido incluido';
                } else {
                    $aux = $this->checkParams($params[$key], $value);
                    if (!empty($aux)) {
                        $msg = $aux;
                    }
                }
            } else {
                if (!isset($params[$key])) {
                    $msg = 'El valor ' . $key . ' no ha sido incluido';
                }
            }
        }
        return $msg;
    }
}
