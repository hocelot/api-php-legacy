<?php
/**
 * Created by PhpStorm.
 * User: a.luis
 * Date: 15/06/2016
 * Time: 13:54
 */

namespace App\Http\Controllers\RiskScore;


use App\Http\Models\AddressModel;
use App\Http\Models\ScoreModel;
use App\Http\Models\UtilClass;
use Log;

class ScoreController extends \Threaded
{
    private $params, $eval_id, $latitude, $longitude, $m2, $fecha, $usuario, $ip, $origen, $ine;

    public function __construct($eval_id, $array, $lat, $long, $m2, $fecha, $usuario, $ip, $origen, $ine)
    {
        $this->eval_id = $eval_id;
        $this->params = $array;
        $this->latitude = $lat;
        $this->longitude = $long;
        $this->m2 = $m2;
        $this->fecha = $fecha;
        $this->origen = $origen;
        $this->usuario = $usuario;
        $this->ip = $ip;
        $this->ine = $ine;
    }

    public function run()
    {
        require_once(__DIR__ . '/../../../../vendor/autoload.php');
        require_once(__DIR__ . '/../../../../bootstrap/app.php');
        $this->worker->addData('risk_score', $this->calculateScore());
    }

    private function calculateScore()
    {
        /*
         * $array:
         *  [m2]
         *  [lat]
         *  [lon]
         *  [type]
         *  [level]
         * */
        // Exactitud 1

        $params = $this->getAddressData();

//        Log::debug('PARAMS ' . print_r($params, TRUE));
        if (is_array($params)) {
            $params['zip_code'] = $this->params->address->zip_code;
            $params['lp_type'] = $this->params->address->type;

//            if ($this->params->address->level == 3) {
//                $params['id_com'] = AddressModel::getTownId(substr($this->params->address->zip_code, 0, 2));
//
//                $params['radio'] = UtilClass::densityRadio(AddressModel::densityTown($this->params->address->zip_code)->ref_as_tipo_municipio);
//                $m2 = ScoreModel::getM2forLevel3($params)->ref_as_metros;
//
//            } else {
            //------------COMENTADO EL 24/01/2017----------------------------
//            if (empty($this->m2) || $this->m2 == 0) {
//                $this->m2 = ScoreModel::getM2Level1or2($this->params->address);
//
//                // NUEVO PARA CSV SI NO ENCONTRAMOS NIVEL 1, DAMOS NIVEL 2 Y SI TAMPOCO HAY NIVEL 2 VAMOS AL 3
//                if ($this->m2 == 0 && $this->params->address->level == 1) {
//                    $this->params->address->level = 2;
//                    $this->m2 = ScoreModel::getM2Level1or2($this->params->address);
//                }
//
//                if ($this->m2 == 0 && $this->params->address->level == 2) {
//                    $params['id_com'] = AddressModel::getTownId(substr($this->params->address->zip_code, 0, 2));
//                    $params['radio'] = UtilClass::densityRadio(AddressModel::densityTown($this->params->address->zip_code)->ref_as_tipo_municipio);
//                    $d = ScoreModel::getM2forLevel3($params);
//                    $this->m2 = $d->ref_as_metros;
//                }
//            }
            //-------------------------------
//            }

            //Equifax
            //TODO EQUIFAX AHORA MISMO FALSEADO
            //COMMENTADO 19/04/2017
//            $f_nac = str_replace('-', '', $this->client_id);
//            $datos['scofecna'] = $f_nac;
//            $datos['incorazo'] = '-';
//            $datos['incotipv'] = $this->params->address->road_type;
//            $datos['inconumv'] = $this->params->address->number;
//            $datos['incoresv'] = trim($this->params->address->floor . ' ' . $this->params->address->door);
//            $datos['incoprov'] = $this->params->address->town;
//            $datos['inconomv'] = $this->params->address->street;
//            $datos['scomunic'] = $this->params->address->province;
//            $datos['ident'] = $this->params->user->cif_dni;
//            $datos['codpos'] = $this->params->address->zip_code;
//            $datos['sconom'] = $this->params->user->name;
//            $datos['scoape1'] = $this->params->user->first_name;
//            $datos['scoape2'] = $this->params->user->last_name;
//            if (empty($datos['incoresv'])) {
//                $datos['incoresv'] = '-';
//            }
//
//
//            $str_tgclientesequifax = array(
//                'ter_codigo' => $this->client_id,
//                'cldel_codigo' => 1,
//                'cexq_id' => 0,
//                'ejercicio' => 0,
//                'ser_serie' => '',
//                'cex_numero' => 0,
//                'cexq_fechacons' => date('c'),
//                'cexq_e_ident' => 0,
//                'cexq_e_sconom' => 0,
//                'cexq_e_scoape1' => 0,
//                'cexq_e_scoape2' => 0,
//                'cexq_e_scofecna' => 0,
//                'cexq_e_codpos' => 0,
//                'cexq_e_incorazo' => 0,
//                'cexq_e_incotipv' => 0,
//                'cexq_e_inconumv' => 0,
//                'cexq_e_incoresv' => 0,
//                'cexq_e_incoprov' => 0,
//                'cexq_e_inconomv' => 0,
//                'cexq_e_scomunic' => 0,
//                'cexq_s_incide' => '--', //(string) $res_equifax['INCIDE'],
//                'cexq_s_presenci' => '01',
//                'cexq_s_notaadm' => 0,
//                'cexq_s_sevnumop' => 0,
//                'cexq_s_sevnum02' => 0,
//                'cexq_s_sevnum06' => 0,
//                'cexq_s_sevnum07' => 0,
//                'cexq_s_sevnum09' => 0,
//                'cexq_s_sevnum14' => 0,
//                'cexq_s_sevnumre' => 0,
//                'cexq_s_sevimtot' => 0,
//                'cexq_s_seviment' => 0,
//                'cexq_s_sevimres' => 0,
//                'cexq_s_sevimp02' => 0,
//                'cexq_s_sevimp06' => 0,
//                'cexq_s_sevimp07' => 0,
//                'cexq_s_sevimp09' => 0,
//                'cexq_s_sevimp14' => 0,
//                'cexq_s_sevimprp' => 0,
//                'cexq_s_sevpeori' => 0,
//                'cexq_s_sevpeors' => 0,
//                'cexq_s_sevnumdi' => 0,
//                'cexq_s_sevnumac' => 0,
//                'cexq_s_sevnumdm' => 0,
//                'cexq_s_preseniv' => 000, //presencia en id verifier 001 confirmado y 000 no confirmado
//                'cexq_s_vfletpre' => 100, //porcentaje de similitud del registro verificado
//                'cexq_s_increnmn' => 0,
//                'cexq_s_increnmx' => 0,
//                'cexq_s_increnmd' => 0
//            );
//
//            ScoreModel::wsdlsetEquifaxWS($this->client_id, $str_tgclientesequifax);


            $params['eval_id'] = $this->eval_id;
            $params['m2'] = $this->m2;
            $resp = ScoreModel::calculateScore($params, $this->fecha, $this->usuario, $this->ip, $this->origen, $this->ine, $this->params);
//            Log::debug('RISK ANDRES ' . print_r($resp, true));
            if ($resp->mr_calculaevaluacionResult === 1) {
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->asnef);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevnumop);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevnum02);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevnum06);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevnum07);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevnum09);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevnum14);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevnumre);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevimtot);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevniment);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevimp02);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevimp06);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevimp07);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevimp09);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevimp14);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevimprp);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevpeori);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevpeors);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevnumdi);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevnumac);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevnumdm);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->income);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ref_catastral);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ter_codigo);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->coeficiente);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->renta_minima);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->renta_mensual);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->renta_media);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->plazo_propuesto);

//                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->rating_eq);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->cliente);
                $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual = new \stdClass();

                $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->alimentacion = $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo1 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_individual;
                $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->alcohol_tabaco = $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo2 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_individual;
                $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->ropa_calzado = $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo3 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_individual;
                $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->vivienda_suministros = $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo4 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_individual;
                $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->mobiliario = $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo5 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_individual;
                $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->salud = $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo6 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_individual;
                $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->transporte = $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo7 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_individual;
                $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->comunicaciones = $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo8 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_individual;
                $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->ocio = $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo9 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_individual;
                $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->educacion = $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo10 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_individual;
                $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->restaurantes = $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo11 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_individual;
                $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->otros = $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo12 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_individual;
                $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar = new \stdClass();
                $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->alimentacion = $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo1 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_hogar;
                $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->alcohol_tabaco = $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo2 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_hogar;
                $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->ropa_calzado = $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo3 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_hogar;
                $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->vivienda_suministros = $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo4 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_hogar;
                $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->mobiliario = $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo5 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_hogar;
                $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->salud = $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo6 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_hogar;
                $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->transporte = $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo7 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_hogar;
                $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->comunicaciones = $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo8 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_hogar;
                $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->ocio = $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo9 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_hogar;
                $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->educacion = $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo10 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_hogar;
                $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->restaurantes = $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo11 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_hogar;
                $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->otros = $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo12 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_hogar;
                $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->umbral_pobreza = $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->porcent_umbral_pobreza_ind;
                $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->tasa_esfuerzo = $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->tasa_esfuerzo_hogar;



                if ($this->origen == 'B') {
//                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->morosidad = number_format(round($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->morosidad / 100, 2), 2, ',', '.');

                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->alimentacion = number_format($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->alimentacion, 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->alcohol_tabaco = number_format($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->alcohol_tabaco, 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->ropa_calzado = number_format($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->ropa_calzado, 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->vivienda_suministros = number_format($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->vivienda_suministros, 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->mobiliario = number_format($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->mobiliario, 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->salud = number_format($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->salud, 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->transporte = number_format($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->transporte, 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->comunicaciones = number_format($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->comunicaciones, 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->ocio = number_format($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->ocio, 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->educacion = number_format($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->educacion, 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->restaurantes = number_format($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->restaurantes, 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->otros = number_format($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->otros, 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->alimentacion = number_format($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->alimentacion, 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->alcohol_tabaco = number_format($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->alcohol_tabaco, 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->ropa_calzado = number_format($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->ropa_calzado, 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->vivienda_suministros = number_format($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->vivienda_suministros, 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->mobiliario = number_format($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->mobiliario, 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->salud = number_format($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->salud, 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->transporte = number_format($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->transporte, 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->comunicaciones = number_format($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->comunicaciones, 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->ocio = number_format($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->ocio, 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->educacion = number_format($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->educacion, 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->restaurantes = number_format($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->restaurantes, 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->otros = number_format($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->otros, 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->tasa_esfuerzo = round($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->tasa_esfuerzo_hogar, 2);
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->tasa_esfuerzo = number_format($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->tasa_esfuerzo, 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->trabajo = number_format($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->trabajo / 100, 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->probabilidad_impago = number_format(round($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->probabilidad_impago,2), 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->desviacion_renta = number_format($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->desviacion_renta, 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->renta_vivienda = number_format(round($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->renta_vivienda, 2), 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->fiabilidad_pago = number_format(round($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->fiabilidad_pago / 100, 2), 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->metros_cuadrados = number_format(round($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->metros_cuadrados, 2), 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->precio = number_format(round($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->precio, 2), 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->umbral_pobreza = number_format(round($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->umbral_pobreza / 100, 2), 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->tasa_paro_correg = number_format(round($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->tasa_paro_correg, 2), 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->renta_vivienda_hogar = number_format(round($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->renta_vivienda_hogar, 2), 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->renta_vivienda_individual = number_format(round($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->renta_vivienda_individual, 2), 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->capacidad_ahorro = number_format(round($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->capacidad_ahorro, 2), 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_hogar = number_format(round($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_hogar, 2), 2, ',', '.');
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_individual = number_format(round($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_individual, 2), 2, ',', '.');
                    $date = new \DateTime($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->cnt_fechanac);
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->cnt_fechanac = $date->format('Y-m-d');
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->nombre);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ape);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->direccion);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->dni);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->morosidad);
                }
                if($this->params->user->client_id != '52301' && isset($this->params->economics->tin) && $this->origen === 'A') {
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->finalidad);
                }
//                else  unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->finalidad);
//                if ($this->params->user->client_id != '52301' && $this->params->user->client_id != '6') {
//                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->nombre);
//                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ape);
//                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->direccion);
//                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->dni);
//                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->respuesta);
//                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->finalidad);
//
//                }else unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->morosidad);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo1nac);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo2nac);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo3nac);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo4nac);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo5nac);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo6nac);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo7nac);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo8nac);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo9nac);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo10nac);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo11nac);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo12nac);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo1);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo2);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo3);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo4);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo5);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo6);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo7);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo8);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo9);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo10);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo11);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo12);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->propia);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->alquilada);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->cedida);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->otra);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->fecha);
                $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->periodo_medio_pago = $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->plazo_gen_renta;
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->plazo_gen_renta);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->renta);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->equivalencia_asnef);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->tasa_nac_esfuerzo);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->tasa_paro);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->seviment);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevimres);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->comunidad);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->paro_hombre_total);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->paro_mujer_total);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->paro_indust_porcent);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->paro_agric_porcent);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->paro_const_porcent);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->paro_serv_porcent);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->saldo_trabajadores);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->morosidad_media);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->morosidad_nacional);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->compra_tot);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->compra_pagos);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->propia_edad);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->pagos_edad);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->herencia_edad);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->alquilada_edad);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->cedida_edad);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->otra_edad);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->tasaesfuerzo_ccaa);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->tasaesfuerzo_nacional);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->fortuito);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->incompetente);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->negligente);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->recurrente);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->intencional);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->id_comunidad);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->umbral_pobreza_ind);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->porcent_umbral_pobreza);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->porcent_umbral_pobreza_ind);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->tasa_esfuerzo_hogar);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->importe_financiable_bowbuy);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->porcentaje_financiable_bowbuy);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->evolucion_patrimonial);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->porcent_agricultura);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->porcent_industria);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->porcent_construccion);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->porcent_servicios);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->hipoteca);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->coche);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->credito365);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->total_nacional);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->posicion_personal_nacional);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->total_comunidad);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->total_ciudad);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->posicion_personal_comunidad);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->posicion_personal_ciudad);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->renta_libre);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->origen);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ip);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->usuario);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->status);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->tramo_precio);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->plazo_ped);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->importe);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->tin);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->tae);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->producto);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->empleado_carrefour);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->nivel1);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->nivel2);
                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->nivel3);

                return ['hits' => $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0], 'status' => 200];
            } else {
                if (isset($resp->ref_as_mensaje))
                    Log::debug('ERROR ANDRÉS ' . print_r($resp->ref_as_mensaje, TRUE));

                return ['msg' => 'Ha ocurrido un error al realizar el score', 'status' => 400];
            }
        } else {
            return ['msg' => 'Direccion inexistente', 'status' => 401];

        }

    }


    private function getAddressData()
    {
        if (empty($this->latitude) || empty($this->longitude)) {
            $direccion = $this->params->address->road_type . ' ' . $this->params->address->street . ' ,' . $this->params->address->number . ', ';
            $pos = strpos($this->params->address->town, ',');
            if ($pos !== false) {
                $nombre_pob = substr($this->params->address->town, 0, $pos);
                $pronombre_pob = substr($this->params->address->town, $pos + 1);
                $poblacion = $pronombre_pob . ' ' . $nombre_pob;
                $direccion .= $poblacion;
            } else {
                $direccion .= $this->params->address->town;
            }
            $this->cartoCiudadLatLong($direccion);
            //GOOGLE CORTE
            if (empty($this->latitude) || empty($this->longitude)) {
                $direccion = $this->params->address->road_type . ' ' . $this->params->address->street . ' ,' . $this->params->address->number . ', ' . $this->params->address->zip_code . ' ' . $this->params->address->town;
                $this->googleMapsLatLong($direccion);
            }
            //No existe direccion
            if (empty($this->latitude) || empty($this->longitude)) {
                $this->latitude = $this->longitude = 0;
            }
        }

        // VECINDARIO GOOGLE
        $nbhood = $this->checkNeighborhood();

        return array(
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'nbhood' => $nbhood
        );

    }


    private static function curl($page)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $page);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }


    private function googleMapsLatLong($direccion)
    {
        $resultado = json_decode(file_get_contents(sprintf('https://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=%s', urlencode($direccion))));

        $estado = $resultado->status;
        if ($estado == 'OK') {
            $this->latitude = $resultado->results[0]->geometry->location->lat;
            $this->longitude = $resultado->results[0]->geometry->location->lng;
            return true;
        } else {
            return false;
        }
    }

    private function cartoCiudadLatLong($address)
    {
        $address = str_replace(' ', '%20', $address);
        $page = 'http://www.cartociudad.es/CartoGeocoder/Geocode?address=' . $address;
        $json = self::curl($page);
        //$var = utf8_decode($json);
        $var = json_decode($json);
//        Log::debug(print_r($var, true));
        if (isset($var->success) && $var->success != false) {
            if ($var->result[0]->status == 1) {
                $this->latitude = $var->result[0]->latitude;
                $this->longitude = $var->result[0]->longitude;
                return true;
            } else if ($var->result[0]->status == 2) {
                $this->latitude = $var->result[0]->latitude;
                $this->longitude = $var->result[0]->longitude;
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    private function googleMapsNeighborhood()
    {
        $resultnbhood = json_decode(file_get_contents(sprintf('https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $this->latitude . ',' . $this->longitude . '&result_type=neighborhood&key=AIzaSyD0O7jxiQQ7b2XHShygw-eFvEXMPvqfFPo')));
        $estado = $resultnbhood->status;

        if ($estado == 'OK') {
            for ($i = 0; $i < count($resultnbhood->results); $i++) {
                if ($resultnbhood->results[$i]->address_components[0]->types[0] == 'neighborhood') {
                    //Erro utf8_decode utf8_decode($resultnbhood->results[$i]->address_components[0]->long_name)
                    return $resultnbhood->results[$i]->address_components[0]->long_name;
                }
            }
        }
        return 'No vecindario';
    }

    private function checkNeighborhood()
    {
        $cp_mad = array(
            "28001",
            "28002",
            "28003",
            "28004",
            "28005",
            "28006",
            "28007",
            "28008",
            "28009",
            "28010",
            "28011",
            "28012",
            "28013",
            "28014",
            "28015",
            "28016",
            "28017",
            "28018",
            "28019",
            "28020",
            "28021",
            "28022",
            "28023",
            "28024",
            "28025",
            "28026",
            "28027",
            "28028",
            "28029",
            "28030",
            "28031",
            "28032",
            "28033",
            "28034",
            "28035",
            "28036",
            "28037",
            "28038",
            "28039",
            "28040",
            "28041",
            "28042",
            "28043",
            "28044",
            "28045",
            "28046",
            "28047",
            "28048",
            "28049",
            "28050",
            "28051",
            "28052",
            "28053",
            "28054",
            "28055",
            "28056",
            "28057",
            "28058",
            "28059",
            "28060"
        );
        $cp_bcn = array(
            "08001",
            "08002",
            "08003",
            "08004",
            "08005",
            "08006",
            "08007",
            "08008",
            "08009",
            "08010",
            "08011",
            "08012",
            "08013",
            "08014",
            "08015",
            "08016",
            "08017",
            "08018",
            "08019",
            "08020",
            "08021",
            "08022",
            "08023",
            "08024",
            "08025",
            "08026",
            "08027",
            "08028",
            "08029",
            "08030",
            "08031",
            "08032",
            "08033",
            "08034",
            "08035",
            "08036",
            "08037",
            "08038",
            "08039",
            "08040",
            "08041",
            "08042",
            "08043",
            "08044",
            "08045",
            "08046",
            "08047",
            "08048",
            "08049",
            "08050",
            "08051",
            "08052",
            "08053",
            "08054",
            "08055",
            "08056",
            "08057",
            "08058",
            "08059",
            "08060"
        );
        $nbhood = '';
        if (in_array($this->params->address->zip_code, $cp_mad) || in_array($this->params->address->zip_code, $cp_bcn)) {
            $nbhood = $this->googleMapsNeighborhood();
        } else {
            $cp_m = substr($this->params->address->zip_code, 0, 2);
            if ($cp_m == 28) {
                $nbhood = 'Total Madrid';
            }
            if ($cp_bcn == '08') {
                $nbhood = 'Total Barcelona';
            }
        }
        unset($cp_mad);
        unset($cp_bcn);

        return $nbhood;
    }


}