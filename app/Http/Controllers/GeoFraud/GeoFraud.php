<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 4/11/16
 * Time: 13:09
 */

namespace App\Http\Controllers\GeoFraud;



class GeoFraud extends \Threaded
{
    private $name, $last_name, $first_name, $address;

    public function __construct($name, $first_name, $last_name, $address)
    {
        $this->name = $name;
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->address = $address;
    }

    public function run()
    {
        require_once(__DIR__ . '/../../../../vendor/autoload.php');
        require_once(__DIR__ . '/../../../../bootstrap/app.php');
        $this->worker->addData('geo_fraud',$this->search());
    }

    private function search()
    {
        //TODO añadir más fuentes
        return PaginasBlancas::search($this->name, $this->first_name, $this->last_name, $this->address->street, $this->address->number, substr($this->address->zip_code, 0, 2));
    }

}