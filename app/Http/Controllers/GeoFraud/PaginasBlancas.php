<?php
/**
 * Created by PhpStorm.
 * User: rmarcos
 * Date: 7/10/16
 * Time: 11:53
 */

namespace App\Http\Controllers\GeoFraud;

use stdClass;
use Symfony\Component\DomCrawler\Crawler;

class PaginasBlancas
{
    public static function search($nombre, $ape1, $ape2, $street, $number, $idProv)
    {
        $street = urlencode(utf8_decode(trim($street)));
        $prov = urlencode(utf8_decode(self::provPB($idProv)));

        $c = 0;
        $search = array(urlencode(utf8_decode($ape1)), urlencode(utf8_decode($ape2)));
        $cmp_name = mb_strtoupper($nombre.' '.$ape1.' '.$ape2,'UTF-8');
        do {
            $result = self::searchQuery($search[$c], urlencode(utf8_decode($street)), $number, $prov,$cmp_name);
            $c++;
        } while ($c < count($search) && $result == false);
        return $result;
    }

    private static function searchQuery($result, $street, $number, $prov, $cmp_name)
    {
        $url = 'http://blancas.paginasamarillas.es/jsp/resultados.jsp?ap1=' . $result . '&sec=35&calle=' . $street . '&numero=' . $number . '&pgpv=0&tbus=0&nomprov=' . $prov . '&idioma=tml_lang';
        $response = self::curlPB($url);
        $crawler = new Crawler();
        $crawler->addHtmlContent($response);
        // if($crawler->filter('#PPAL')->count() < 1 ) return false;
        $paginasblancas = new stdClass();
        $crawler->filter('#PPAL > .resul')->each(function ($node) use ($paginasblancas,$cmp_name) {
            $persona = new stdClass();
            $name = $node->filter('h3');
            if ($name->count() > 0) {
                $persona->name = self::trim_all(str_replace('Imprimir Ficha', '', $name->text()));
                similar_text(mb_strtoupper($persona->name,'UTF-8'),$cmp_name,$percent);
                $persona->correlacion = number_format(round($percent,PHP_ROUND_HALF_EVEN)/100,2,',','.');
                $persona->phone = self::trim_all($node->filter('p > span')->text());
                $persona->address = self::trim_all(str_replace($persona->phone, '', $node->filter('p')->text()));
                $paginasblancas->hits[] = $persona;
            }
        });
        if (isset($paginasblancas->hits)) {
            return ['hits'=>$paginasblancas->hits[0],'status'=>200];
        } else {
            return ['hits'=>"No encontrado",'status'=>400];
        }
    }

    private static function curlPB($url)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
                "accept-encoding: gzip, deflate, sdch",
                "accept-language: es,en-GB;q=0.8,en;q=0.6",
                "cache-control: no-cache",
                "connection: keep-alive",
                "upgrade-insecure-requests: 1",
                "user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.92 Safari/537.36"
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }

    private static function provPB($np)
    {
        $res = false;
        switch ($np) {
            case '15':
                $res = "A Coruña";
                break;
            case '02':
                $res = "Albacete";
                break;
            case '03':
                $res = "Alicante";
                break;
            case '04':
                $res = "Almería";
                break;
            case '01':
                $res = "Araba/Álava";
                break;
            case '33':
                $res = "Asturias";
                break;
            case '05':
                $res = "Ávila";
                break;
            case '06':
                $res = "Badajoz";
                break;
            case '08':
                $res = "Barcelona";
                break;
            case '48':
                $res = "Bizkaia";
                break;
            case '09':
                $res = "Burgos";
                break;
            case '10':
                $res = "Cáceres";
                break;
            case '11':
                $res = "Cádiz";
                break;
            case '39':
                $res = "Cantabria";
                break;
            case '12':
                $res = "Castellón";
                break;
            case '51':
                $res = "Ceuta";
                break;
            case '13':
                $res = "Ciudad Real";
                break;
            case '14':
                $res = "Córdoba";
                break;
            case '16':
                $res = "Cuenca";
                break;
            case '20':
                $res = "Gipuzkoa";
                break;
            case '17':
                $res = "Girona";
                break;
            case '18':
                $res = "Granada";
                break;
            case '19':
                $res = "Guadalajara";
                break;
            case '21':
                $res = "Huelva";
                break;
            case '22':
                $res = "Huesca";
                break;
            case '07':
                $res = "Illes Balears";
                break;
            case '23':
                $res = "Jaén";
                break;
            case '26':
                $res = "La Rioja";
                break;
            case '35':
                $res = "Las Palmas";
                break;
            case '24':
                $res = "León";
                break;
            case '25':
                $res = "Lleida";
                break;
            case '27':
                $res = "Lugo";
                break;
            case '28':
                $res = "Madrid";
                break;
            case '29':
                $res = "Málaga";
                break;
            case '52':
                $res = "Melilla";
                break;
            case '30':
                $res = "Murcia";
                break;
            case '31':
                $res = "Navarra";
                break;
            case '32':
                $res = "Ourense";
                break;
            case '34':
                $res = "Palencia";
                break;
            case '36':
                $res = "Pontevedra";
                break;
            case '37':
                $res = "Salamanca";
                break;
            case '38':
                $res = "Santa Cruz De Tenerife";
                break;
            case '40':
                $res = "Segovia";
                break;
            case '41':
                $res = "Sevilla";
                break;
            case '42':
                $res = "Soria";
                break;
            case '43':
                $res = "Tarragona";
                break;
            case '44':
                $res = "Teruel";
                break;
            case '45':
                $res = "Toledo";
                break;
            case '46':
                $res = "Valencia";
                break;
            case '47':
                $res = "Valladolid";
                break;
            case '49':
                $res = "Zamora";
                break;
            case 50:
                $res = "Zaragoza";
                break;
        }
        return $res;
    }

    private static function trim_all($str, $with = ' ')
    {
        $str = html_entity_decode(str_replace('&nbsp;', ' ', htmlentities($str)));
        $what = "\\x00-\\x20";    //all white-spaces and control chars
        return trim(preg_replace("/[" . $what . "]+/", $with, $str));
    }
}