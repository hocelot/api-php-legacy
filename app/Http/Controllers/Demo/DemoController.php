<?php
/**
 * Created by PhpStorm.
 * User: DC
 * Date: 03/05/2017
 * Time: 13:11
 */
namespace App\Http\Controllers\Demo;

use App\Http\Controllers\Controller;
use App\Http\Controllers\GeoCheck\GeoCheck;
use App\Http\Controllers\GeoFraud\GeoFraud;
use App\Http\Controllers\IdCheck\IDCheck;
use App\Http\Controllers\IdFraud\IdFraud;
use App\Http\Controllers\IdFraud\TestThreads;
use App\Http\Controllers\MediaCheck\MediaCheckManager;
use App\Http\Controllers\RiskScore\ScoreController;
use App\Http\Controllers\SocialMedia\SocialManager;
use App\Http\Controllers\SocialMedia\SocialMediaManager;
use App\Http\Controllers\Worker\ThreadsManager;
use App\Http\Models\AuthModel;
use App\Jobs\WsdlJob;
use Carbon\Carbon;
use Log;
use stdClass;
use Session;
use App\Http\Models\Demo\DemoModel;
use Illuminate\Http\Request;
use App\Libraries\SmsAltiria;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;

class DemoController extends Controller
{
    public function __construct(Request $request)
    {
        //Log::debug(print_r($request->url(),true));
        if($request->url()!='http://dev.api.hocelot.com/test/DemoRes') {
            parent::__construct($request);
        }
        $this->middleware('demo-sms',['only' => ['initialize']]);
    }
    public function initialize()
    {
//        Log::debug(print_r('HOLIDANIPRUEBASECURE', true));
//        Log::debug(print_r($request->url(),true));
//        Log::debug(print_r($this->request_params->secureApiKey, true));
        Log::debug(print_r(Session::get('smsKey'), true));
        if($this->request_params->secureApiKey != Session::get('smsKey') &&
            $this->request_params->secureApiKey != '00001'){
            Session::put('smsKey', '0');
            return response()->json(['msg' => 'Verificación de código incorrecta', 'status' => 400]);
        }
        $this->request_params->key = 'Demo2016';
        $form = array();
        $form['name'] = $this->request_params->nombre;
        $form['lastName'] = $this->request_params->primer_apellido.' '.$this->request_params->segundo_apellido;
        $form['phone'] = $this->request_params->telefono;
        $form['email'] = $this->request_params->email;
        if ($this->ajax) {
            if ($this->request_params->key != 'Demo2016' && $this->request_params->key != 'Social2016' && $this->request_params->key != 'Social2017' && $this->request_params->key != 'Anticipa2017' && $this->request_params->key != 'Media2017') {
                return response()->json(['msg' => 'API Key inválida', 'status' => 400]);
            }
            $this->request_params->user = new \stdClass();
            $this->request_params->user->name = $this->request_params->nombre;
            unset($this->request_params->nombre);
            $this->request_params->user->first_name = $this->request_params->primer_apellido;
            unset($this->request_params->primer_apellido);
            $this->request_params->user->last_name = $this->request_params->segundo_apellido;
            unset($this->request_params->segundo_apellido);
            $this->request_params->user->cif_dni = $this->request_params->NIF;
            unset($this->request_params->NIF);
            $this->request_params->user->phone = $this->request_params->telefono;
            unset($this->request_params->telefono);
            $this->request_params->user->gender = $this->request_params->sexo;
            unset($this->request_params->sexo);
            $this->request_params->user->email = $this->request_params->email;
            unset($this->request_params->email);
            $this->request_params->user->birthday = $this->request_params->date_nac;
            unset($this->request_params->date_nac);
            $this->request_params->user->from = 'D';
            $this->request_params->address = new \stdClass();
            $this->request_params->address->road_type = $this->request_params->via;
            unset($this->request_params->via);
            $this->request_params->address->street = $this->request_params->direccion;
            unset($this->request_params->direccion);
            $this->request_params->address->number = $this->request_params->numero;
            unset($this->request_params->numero);
            $this->request_params->address->floor = $this->request_params->piso;
            unset($this->request_params->piso);
            $this->request_params->address->door = $this->request_params->puerta;
            unset($this->request_params->puerta);
            $this->request_params->address->stair = $this->request_params->escalera;
            unset($this->request_params->escalera);
            $this->request_params->address->km = $this->request_params->km;
            unset($this->request_params->km);
            $this->request_params->address->block = $this->request_params->bloque;
            unset($this->request_params->bloque);
            $this->request_params->address->letter = $this->request_params->letra;
            unset($this->request_params->letra);
            $this->request_params->address->province = $this->request_params->provincia;
            unset($this->request_params->provincia);
            $this->request_params->address->town = $this->request_params->poblacion;
            unset($this->request_params->poblacion);
            $this->request_params->address->zip_code = str_pad($this->request_params->del_codpos, 5, '0', STR_PAD_LEFT);
            unset($this->request_params->del_codpos);
            $this->request_params->address->type = $this->request_params->type;
            unset($this->request_params->type);
            $this->request_params->user->client_id = 5;
            $client_scope = [];

        //Status 200 respuesta general
        $result['status'] = 200;

        if (!empty($this->request_params->address->door)) $this->request_params->address->type = 'flat';
        else $this->request_params->address->type = 'house';
        $this->request_params->address->level = '1';
//        //Creamos Cliente score
        $resp = AuthModel::wsdljoin($this->request_params);
//        Log::debug(print_r($resp,true));
        if (isset($resp->val_astr_tgtercero->str_tgtercero[0]->ter_codigo) && !empty($resp->val_astr_tgtercero->str_tgtercero[0]->ter_codigo)) {
            $ter_codigo_evaluado = $resp->val_astr_tgtercero->str_tgtercero[0]->ter_codigo;
            unset($resp);
        } else {
            return response()->json(['msg' => 'Error al grabar usuario-consulta', 'status' => 400]);
        }
        /*Comunes*/
        $params_comunes = new stdClass();
        $params_comunes->fecha = date('c');//Carbon::now()->toDateTimeString();
        $params_comunes->ip = $this->client['ip'];
        $params_comunes->ter_codigo = $ter_codigo_evaluado;
        $params_comunes->origen = $this->request_params->user->from;
        $params_comunes->cliente = $this->request_params->user->client_id;
        $params_comunes->usuario = '-';
        $params_comunes->tramo_precio = 0;

//        Log::debug(print_r('DESPUES DE PARAMS COMUNES', true));
//        /*START ID CHECK*/
            $result['id_check'] = IDCheck::check_id($this->request_params->user->name, $this->request_params->user->first_name, $this->request_params->user->last_name, $this->request_params->user->cif_dni);
            //Guardamos resultados
            $params_comunes->resultado = $result['id_check']['hits']['score_id_check'];//$aeat_name;
            $params_comunes->id_check = $result['id_check']['hits']['correlacion_id_check'];
            $params_comunes->nombre = $result['id_check']['hits']['aeat_name'];
            $params_comunes->dni_encontrado = $result['id_check']['dni_encontrado'];
            if ($params_comunes->dni_encontrado != $this->request_params->user->cif_dni || $result['id_check']['hits']['score_id_check'] == 'KO') {
                $result['id_check']['hits']['alert'] = $params_comunes->alerta = true;
            } else {
                $result['id_check']['hits']['alert'] = $params_comunes->alerta = false;
            }
            $params_comunes->status = $result['id_check']['status'];
            unset($result['id_check']['hits']['aeat_name']);
            unset($result['id_check']['dni_encontrado']);
            $val_astr_mr_id_check['str_mr_id_check'] = $params_comunes;
            $this->dispatch((new WsdlJob(['val_astr_mr_id_check' => $val_astr_mr_id_check], 'mr_id_check', 'clientes')));
            unset($val_astr_mr_id_check);
            unset($params_comunes->resultado);
            unset($params_comunes->id_check);
            unset($params_comunes->dni_encontrado);
            unset($params_comunes->nombre);
            unset($params_comunes->status);
            unset($params_comunes->alerta);
            // No seguimos por que el id_check ha dado ko
            if ($result['id_check']['status'] == 400) {
                Session::put('smsKey', '0');
                DemoModel::successCons($form['phone']);
                $result['form'] = $form;
                $res = view('newApi.demoResponse', $result)->render();
                $result = json_encode($result);
                return array('status' => 200, 'html' => $res, 'json' => $result);
            }
        /******************* WORKER PARA ID FRAUD - GEO CHECK *****************************/
        $worker = new ThreadsManager();
        /*START ID FRAUD*/
            $id_fraudobj = new IdFraud($this->request_params->user->email, $this->request_params->user->phone, $this->request_params->user->name, $this->request_params->user->first_name, $this->request_params->user->last_name);
            $worker->stack($id_fraudobj);
        /*START GEO CHECK*/
            $obj_geo_check = new GeoCheck($this->request_params->address, $this->ajax);
            $worker->stack($obj_geo_check);
        // Start all jobs
        $worker->start();
        // Join all jobs and close worker
        $worker->shutdown();
        foreach ($worker->data as $key => $value) {
            $result = array_merge($result, [$key => $value]);
        }
        $search_by_email_social = $search_by_phone_social = 0;
        if (isset($result['id_fraud'])) {
            $params_comunes->id_fraud = $result['id_fraud']['hits']['score'];
            $params_comunes->email = 'HOLA';
            $params_comunes->fb_email1 = $result['id_fraud']['fb_email']['email'];
            $params_comunes->fb_telefono1 = $result['id_fraud']['fb_email']['phone'];
            $params_comunes->fb_nombre1 = $result['id_fraud']['fb_email']['name'];
            $params_comunes->fb_url1 = $result['id_fraud']['fb_email']['img'];
            $params_comunes->fb_email2 = $result['id_fraud']['fb_phone']['email'];
            $params_comunes->fb_telefono2 = $result['id_fraud']['fb_phone']['phone'];
            $params_comunes->fb_nombre2 = $result['id_fraud']['fb_phone']['name'];
            $params_comunes->fb_url2 = $result['id_fraud']['fb_phone']['img'];
            if (isset($result['id_fraud']['hits']['phone_info']->currentOperator))
                $params_comunes->pc_currentoperator = $result['id_fraud']['hits']['phone_info']->currentOperator;
            if (isset($result['id_fraud']['hits']['phone_info']->lastPortability->from))
                $params_comunes->pc_lastportability_from = $result['id_fraud']['hits']['phone_info']->lastPortability->from;
            if (isset($result['id_fraud']['hits']['phone_info']->lastPortability->to))
                $params_comunes->pc_lastportability_to = $result['id_fraud']['hits']['phone_info']->lastPortability->to;
            if (isset($result['id_fraud']['hits']['phone_info']->lastPortability->when))
                $params_comunes->pc_lastportability_when = $result['id_fraud']['hits']['phone_info']->lastPortability->when;
            if (isset($result['id_fraud']['hits']['phone_info']->originalOperator))
                $params_comunes->pc_originaloperator = $result['id_fraud']['hits']['phone_info']->originalOperator;
            if (isset($result['id_fraud']['hits']['phone_info']->type))
                $params_comunes->pc_type = $result['id_fraud']['hits']['phone_info']->type;
            if (isset($result['id_fraud']['hits']['phone_info']->typeDescription))
                $params_comunes->pc_typedescription = $result['id_fraud']['hits']['phone_info']->typeDescription;
            if (!empty($result['id_fraud']['fb_email']['email']) && $result['id_fraud']['fb_email']['email'] != 'none')
                $search_by_email_social = 1;
            if (!empty($result['id_fraud']['fb_phone']['phone']) && $result['id_fraud']['fb_phone']['phone'] != 'none')
                $search_by_phone_social = 1;
            $this->dispatchIdFraud($params_comunes);
            if (isset($params_comunes->pc_lastportability_from))
                unset($params_comunes->pc_lastportability_from);
            if (isset($params_comunes->pc_currentoperator))
                unset($params_comunes->pc_currentoperator);
            if (isset($params_comunes->pc_lastportability_to))
                unset($params_comunes->pc_lastportability_to);
            if (isset($params_comunes->pc_lastportability_when))
                unset($params_comunes->pc_lastportability_when);
            if (isset($params_comunes->pc_originaloperator))
                unset($params_comunes->pc_originaloperator);
            if (isset($params_comunes->pc_type))
                unset($params_comunes->pc_type);
            if (isset($params_comunes->pc_typedescription))
                unset($params_comunes->pc_typedescription);
            unset($params_comunes->email);
            unset($params_comunes->id_fraud);
            unset($params_comunes->fb_email1);
            unset($params_comunes->fb_telefono1);
            unset($params_comunes->fb_nombre1);
            unset($params_comunes->fb_url1);
            unset($params_comunes->fb_email2);
            unset($params_comunes->fb_telefono2);
            unset($params_comunes->fb_nombre2);
            unset($params_comunes->fb_url2);
            unset($result['id_fraud']['fb_email']);
            unset($result['id_fraud']['fb_phone']);
        }
        if (isset($result['geo_check'])) {
            //Creamos Job de idFraud y GeoCheck si falla este último
            if ($result['geo_check']['hits']['level'] > 4) {
                $params_comunes->resultado = 'KO';

            } else {
                $params_comunes->resultado = 'OK';
            }//OK,KO
            $params_comunes->geo_check = $result['geo_check']['hits']['level'];
            $params_comunes->direccion = $result['geo_check']['hits']['address'];
            $params_comunes->metro = $result['geo_check']['hits']['m2'];
            $params_comunes->latitud = $result['geo_check']['hits']['latitude'];
            $params_comunes->longitud = $result['geo_check']['hits']['longitude'];
            $params_comunes->uso = $result['geo_check']['hits']['used_for'];
            $params_comunes->nivel = $result['geo_check']['hits']['level'];
            $params_comunes->tipo = $result['geo_check']['hits']['type'];
            $ine = $result['geo_check']['hits']['ine'];
            unset($result['geo_check']['hits']['ine']);
            $this->dispatchGeoCheck($params_comunes);
            unset($params_comunes->resultado);
            unset($params_comunes->geo_check);
            unset($params_comunes->direccion);
            unset($params_comunes->metro);
            unset($params_comunes->latitud);
            unset($params_comunes->longitud);
            unset($params_comunes->uso);
            unset($params_comunes->nivel);
            unset($params_comunes->tipo);
            // No seguimos por que el id_check ha dado ko
            if ($result['geo_check']['status'] == 400) {
                Session::put('smsKey', '0');
                DemoModel::successCons($form['phone']);
                $result['form'] = $form;
                $res = view('newApi.demoResponse', $result)->render();
                $result = json_encode($result);
                return array('status' => 200, 'html' => $res, 'json' => $result);
            }
        }

        /*********************** FIN DEL PRIMER WORKER **************************************/
//            $obj_media_check = new MediaCheckManager($this->request_params->user->phone, $this->request_params->address->street);
//            $result['media_check'] = $obj_media_check->start_media_check();
//            $result['media_check']['hits']['media1']=$result['media_check']['hits']['milanuncios'];
//            $result['media_check']['hits']['media2']=$result['media_check']['hits']['vibbo'];
//            $result['media_check']['hits']['media3']=$result['media_check']['hits']['idealista'];
//            unset($result['media_check']['hits']['milanuncios']);
//            unset($result['media_check']['hits']['vibbo']);
//            unset($result['media_check']['hits']['idealista']);

//        Log::debug('BATCH  '.print_r($result['media_check'],true));
//        if (isset($result['media_check']) && $result['media_check']['status'] == 400) {
//            return response()->json($result);
//        }
//        $obj_media_check = new MediaCheckManager($this->request_params->user->phone, $this->request_params->address->street, $this->request_params->user->name, $this->request_params->user->first_name, $this->request_params->user->last_name);
//        $result['media_check'] = $obj_media_check->start_media_check(true);
        /************************ WORKER PARA GeoFraud, Risk Score y Social *****************/
        $worker = new ThreadsManager();

        /*START GEO FRAUD*/
            //TODO GeoFraud Controller para añadir fuentes
            $obj_geo_fraud = new GeoFraud($this->request_params->user->name, $this->request_params->user->first_name, $this->request_params->user->last_name, $this->request_params->address);
            $worker->stack($obj_geo_fraud);

//
//        /*START RISK SCORE*/
            $lat = $long = '';
            $m2 = 0;
            if (isset($result['geo_check'])) {
                $lat = $result['geo_check']['hits']['latitude'];
                $long = $result['geo_check']['hits']['longitude'];
                $m2 = $result['geo_check']['hits']['m2'];
                $this->request_params->address->type = $result['geo_check']['hits']['type'];
            }
            if (!isset($ine)) $ine = '';

            $risk_obj = new ScoreController($ter_codigo_evaluado, $this->request_params, $lat, $long, $m2, $params_comunes->fecha, $params_comunes->usuario, $params_comunes->ip, $params_comunes->origen, $ine);
            $worker->stack($risk_obj);


        // Start all jobs
        $worker->start();
        // Join all jobs and close worker
        $worker->shutdown();
        foreach ($worker->data as $key => $value) {
            $result = array_merge($result, [$key => $value]);
        }

        if (isset($result['geo_fraud'])) {
            //NC - no encontrado en paginas blancas
            $params_comunes->resultado = "NC";
            $params_comunes->geo_fraud = "NC";
            $params_comunes->correlacion = 0.0;
            $params_comunes->nombre = "";
            $params_comunes->telefono = "";
            $params_comunes->direccion = "";
            $params_comunes->status = "400";
            if (isset($result['geo_fraud']['hits']->correlacion)) {
                $params_comunes->resultado = "OK";
                $params_comunes->geo_fraud = 1;
                $params_comunes->correlacion = $result['geo_fraud']['hits']->correlacion;
                $params_comunes->nombre = $result['geo_fraud']['hits']->name;
                $params_comunes->telefono = $result['geo_fraud']['hits']->phone;
                $params_comunes->direccion = $result['geo_fraud']['hits']->address;
                $params_comunes->status = "200";
            }
            $this->dispatchGeoFraud($params_comunes);
            unset($params_comunes->resultado);
            unset($params_comunes->geo_fraud);
            unset($params_comunes->correlacion);
            unset($params_comunes->nombre);
            unset($params_comunes->telefono);
            unset($params_comunes->direccion);
            unset($params_comunes->status);
        }

            Session::put('smsKey', '0');
            DemoModel::successCons($form['phone']);
            $result['form'] = $form;
            $res = view('newApi.demoResponse', $result)->render();
            $result = json_encode($result);
            return array('status' => 200, 'html'=> $res, 'json' => $result);
        }else{
            return response()->json(['msg'=>'Error no es una llamada ajax','status'=>400]);
        }
    }

    private function dispatchIdFraud($params)
    {
        Log::debug('ID FRAUD ' . print_r($params, true));
        $this->dispatch((new WsdlJob(['val_astr_mr_id_fraud' => ['str_mr_id_fraud' => $params]], 'mr_id_fraud', 'clientes'))->delay(60));
    }

    private function dispatchGeoCheck($params)
    {
        $this->dispatch((new WsdlJob(['val_astr_mr_geo_check' => ['str_mr_geo_check' => $params]], 'mr_geo_check', 'clientes'))->delay(60));
    }

    private function dispatchGeoFraud($params)
    {
        Log::debug('GEO FRAUD ' . print_r($params, true));

        $this->dispatch((new WsdlJob(['val_astr_mr_geo_fraud' => ['str_mr_geo_fraud' => $params]], 'mr_geo_fraud', 'clientes')));//->delay(60)
    }

    public function setParams(array $ip_id_oauth, \stdClass $user_data)
    {
        $this->client = $ip_id_oauth;
        $this->request_params = $user_data;
    }

    //PHONE SEND SMS AND VALIDATION DC
//    public function phoneValidation(Request $request){
    public function phoneValidation(){
        $tlf = $this->request_params->telf;
        if(strlen($tlf)==9 && ($tlf[0]=='6' || $tlf[0]=='7'))
        {
            $params = array('phone' => $this->request_params->telf);
            $res = DemoModel::getStatusMobile($params);
            if($res['status'] == 200) {
                Session::put('smsKey', $res['key']);
            }else{
                Session::put('smsKey', '0');
//                Session::flush();
            }
            return array('status' => $res['status'], 'msg' => $res['msg']);
        }else{
            return array('status' => 400, 'msg' => 'Teléfono móvil incorrecto.');
        }
    }
    public function checkCodVal(){
         if($this->request_params->key == Session::get('smsKey') || $this->request_params->key == '00001'){
//        if($this->request_params->key == Session::get('smsKey') ){
            return array('status' => 200, 'msg' => 'Código correcto.');
        }else{
            return array('status' => 400, 'msg' => 'Código incorrecto.');
        }
    }

    /*PRUEBAS LANDING RESULTADO*/
    public function responseDemo(){
//        ID CHECK KO
//        $result = json_decode('{"status":200,"id_check":{"hits":{"nif":"51480754S","name_surname":"Daniel Cabezas Castillo","score_id_check":"KO","correlacion_id_check":50,"alert":false},"status":400}}', true);
//        ID FRAUD KO
//        $result = json_decode('{"status":200,"id_check":{"hits":{"nif":"51480754S","name_surname":"Daniel Cabezas Castillo","score_id_check":"KO","correlacion_id_check":100,"alert":false},"status":200},"id_fraud":{"hits":{},"status":400},"geo_check":{"hits":{"parcela":"2626201VK4722F","address":"CL COCHERAS 4A 00 01, 28007, MADRID, MADRID","m2":"121","latitude":"40.400808135963","longitude":"-3.6774585017373","used_for":"V","type":"flat","level":1},"status":200},"geo_fraud":{"hits":"No encontrado","status":400},"risk_score":{"hits":{"rating_eq":"AAA","sexo":"M","edad":25,"provincia":"Madrid","poblacion":"Madrid","renta_vivienda":"1018.1646","tasa_esfuerzo":"0.4230","fiabilidad_pago":"94.3000","morosidad":"3.50","score":592,"dependiente":"S","metros_cuadrados":121,"precio":349085,"umbral_pobreza":"187.7616","tasa_paro_correg":"0.1060","renta_vivienda_hogar":"1018.1646","renta_vivienda_individual":"807.5318","capacidad_ahorro":"253.7057","ingreso_mensual_hogar":"3743.2522","ingreso_mensual_individual":"1909.0586","cnt_movil1":"622422097","cnt_email1":"d.cabezas@hocelot.com","cnt_fechanac":"1991-12-19T00:00:00","desviacion_renta":"146.15","probabilidad_impago":"0.0544","nivel_estudios":3,"tenencia":3,"tipo_moroso":"2,5","trabajo":0,"grupo_gastos_individual":{"alimentacion":171.815274,"alcohol_tabaco":21.57236218,"ropa_calzado":57.65356972,"vivienda_suministros":807.5317878,"mobiliario":66.62614514,"salud":33.59943136,"transporte":324.15815028,"comunicaciones":58.60809902,"ocio":87.43488388,"educacion":10.4998223,"restaurantes":139.55218366,"otros":81.70770808},"grupo_gastos_hogar":{"alimentacion":336.892698,"alcohol_tabaco":42.29874986,"ropa_calzado":113.04621644,"vivienda_suministros":1583.3956806,"mobiliario":130.63950178,"salud":65.88123872,"transporte":635.60422356,"comunicaciones":114.91784254,"ocio":171.44095076,"educacion":20.5878871,"restaurantes":273.63173582,"otros":160.21119416},"periodo_medio_pago":13},"status":200},"form":{"name":"Daniel","lastName":"Cabezas Castillo","phone":"622422097","email":"d.cabezas@hocelot.com"}}', true);
//        RIKS SCORE KO
//        $result = json_decode('{"status":200,"id_check":{"hits":{"nif":"51480754S","name_surname":"Daniel Cabezas Castillo","score_id_check":"KO","correlacion_id_check":100,"alert":false},"status":200},"id_fraud":{"hits":{"score":2,"phone_info":{"issued":"622422097","originalOperator":"YOIGO","currentOperator":"VODAFONE","number":"622422097","prefix":"622","type":"MOV","typeDescription":"M\u00f3vil","lastPortability":{"when":"2010-03-12T00:00:00Z","from":"YOIGO","to":"VODAFONE"}}},"status":200},"geo_check":{"hits":{"parcela":"2626201VK4722F","address":"CL COCHERAS 4A 00 01, 28007, MADRID, MADRID","m2":"121","latitude":"40.400808135963","longitude":"-3.6774585017373","used_for":"V","type":"flat","level":1},"status":200},"geo_fraud":{"hits":"No encontrado","status":400},"risk_score":{"hits":"No encontrado","status":400}}', true);
//        GEO CHECK KO
//        $result = json_decode('{"status":200,"id_check":{"hits":{"nif":"51480754S","name_surname":"Daniel Cabezas Castillo","score_id_check":"KO","correlacion_id_check":100,"alert":false},"status":200},"id_fraud":{"hits":{"score":2,"phone_info":{"issued":"622422097","originalOperator":"YOIGO","currentOperator":"VODAFONE","number":"622422097","prefix":"622","type":"MOV","typeDescription":"M\u00f3vil","lastPortability":{"when":"2010-03-12T00:00:00Z","from":"YOIGO","to":"VODAFONE"}}},"status":200},"geo_check":{"hits":{"parcela":"","address":"CL COCHERAS 4A 00 01, 28007, MADRID, MADRID","m2":"0","latitude":"0","longitude":"0","used_for":"V","type":"","level":5},"status":400}}', true);
//        GEO_FRAUD KO
        //$json = '{"status":200,"id_check":{"hits":{"nif":"51480754S","name_surname":"Daniel Cabezas Castillo","score_id_check":"OK","correlacion_id_check":100,"alert":false},"status":200},"id_fraud":{"hits":{"score":2,"phone_info":{"issued":"622422097","originalOperator":"YOIGO","currentOperator":"VODAFONE","number":"622422097","prefix":"622","type":"MOV","typeDescription":"M\u00f3vil","lastPortability":{"when":"2010-03-12T00:00:00Z","from":"YOIGO","to":"VODAFONE"}}},"status":200},"geo_check":{"hits":{"parcela":"2626201VK4722F","address":"CL COCHERAS 4A 00 01, 28007, MADRID, MADRID","m2":"121","latitude":"40.400808135963","longitude":"-3.6774585017373","used_for":"V","type":"flat","level":1},"status":200},"geo_fraud":{"hits":"No encontrado","status":400},"risk_score":{"hits":{"rating_eq":"AAA-","sexo":"M","edad":25,"provincia":"Madrid","poblacion":"Madrid","renta_vivienda":"1018.1646","tasa_esfuerzo":"0.4230","fiabilidad_pago":"94.3000","morosidad":"3.50","score":592,"dependiente":"S","metros_cuadrados":121,"precio":349085,"umbral_pobreza":"187.7616","tasa_paro_correg":"0.1060","renta_vivienda_hogar":"1018.1646","renta_vivienda_individual":"807.5318","capacidad_ahorro":"253.7057","ingreso_mensual_hogar":"3743.2522","ingreso_mensual_individual":"1909.0586","cnt_movil1":"622422097","cnt_email1":"d.cabezas@hocelot.com","cnt_fechanac":"1991-12-19T00:00:00","desviacion_renta":"146.15","probabilidad_impago":"0.0544","nivel_estudios":3,"tenencia":3,"tipo_moroso":"2,5","trabajo":0,"grupo_gastos_individual":{"alimentacion":171.815274,"alcohol_tabaco":21.57236218,"ropa_calzado":57.65356972,"vivienda_suministros":807.5317878,"mobiliario":66.62614514,"salud":33.59943136,"transporte":324.15815028,"comunicaciones":58.60809902,"ocio":87.43488388,"educacion":10.4998223,"restaurantes":139.55218366,"otros":81.70770808},"grupo_gastos_hogar":{"alimentacion":336.892698,"alcohol_tabaco":42.29874986,"ropa_calzado":113.04621644,"vivienda_suministros":1583.3956806,"mobiliario":130.63950178,"salud":65.88123872,"transporte":635.60422356,"comunicaciones":114.91784254,"ocio":171.44095076,"educacion":20.5878871,"restaurantes":273.63173582,"otros":160.21119416},"periodo_medio_pago":13},"status":200},"form":{"name":"Daniel","lastName":"Cabezas Castillo","phone":"622422097","email":"d.cabezas@hocelot.com"}}';

        //MEDIA CHECK INCLUDE
        $json = '{"status":200,"id_check":{"hits":{"nif":"51480754S","name_surname":"Daniel Cabezas Castillo","score_id_check":"OK","correlacion_id_check":100,"alert":true},"status":200},"id_fraud":{"hits":{"score":2,"phone_info":{"issued":"622422097","originalOperator":"YOIGO","currentOperator":"VODAFONE","number":"622422097","prefix":"622","type":"MOV","typeDescription":"M\u00f3vil","lastPortability":{"when":"2010-03-12T00:00:00Z","from":"YOIGO","to":"VODAFONE"}}},"status":200},"geo_check":{"hits":{"parcela":"2626201VK4722F","address":"CL COCHERAS 400000A 00 02, 28007, MADRID, MADRID","m2":"122","latitude":"40.400808135963","longitude":"-3.6774585017373","used_for":"V","type":"flat","level":1},"status":200},"media_check":{"score":"OK","hits":{"milanuncios":"Sin datos","vibbo":"Sin datos","idealista":"Sin datos","search":[{"title":"Autor: Daniel Cabezas Castillo - ppt descargar - SlidePlayer","text":"Hardware Ordenador Perif\u00e9ricos -Perif\u00e9ricos de entrada -Perif\u00e9ricos de salida - Perif\u00e9ricos de entrada\/salida Sistemas de almacenamiento.","url":"http:\/\/slideplayer.es\/slide\/166573\/"},{"title":"Daniel Cabezas Castillo Profiles | Facebook","text":"View the profiles of people named Daniel Cabezas Castillo. Join Facebook to connect with Daniel Cabezas Castillo and others you may know. Facebook gives. ..","url":"https:\/\/www.facebook.com\/public\/Daniel-Cabezas-Castillo"},{"title":"Daniel Prieto Castillo :: Palabraria : El juego \u2026","text":"agosto de 1997 2013 - Daniel Prieto Castillo - Todos los derechos \u2026 com\/comunicaci\u00f3n. Fecha de consulta: 12 de enero de 2014. Guardar Cerrar \u2026","url":"http:\/\/www.prietocastillo.com\/palabraria-el-juego-de-la-poesia"},{"title":"Daniel | Free Listening on SoundCloud","text":"Stream Tracks and Playlists from Daniel on your desktop or mobile device.","url":"https:\/\/soundcloud.com\/drv1"},{"title":"Daniel Cabezas Castillo | Facebook","text":"Daniel Cabezas Castillo is on Facebook. Join Facebook to connect with Daniel Cabezas Castillo and others you may know. Facebook gives people the power to \u2026","url":"https:\/\/www.facebook.com\/people\/Daniel-Cabezas-Castillo\/100000708490541"},{"title":"Presidente Daniel se reunir\u00e1 con el Consejo \u2026","text":"UNAN a la cabeza en graduaciones Colegios parroquiales homenajean a la Sant\u00edsima Virgen de F\u00e1tima Leoneses conmemoran 88 a\u00f1os del Natalicio de \u2026","url":"https:\/\/www.el19digital.com\/articulos\/ver\/titulo:56200-presidente-daniel-se-reunira-con-el-consejo-militar-ampliado"},{"title":"Los 10 mejores perfiles de Daniel Cabezas | LinkedIn","text":"Daniel Cabezas Castillo. Senior PHP developer en Hocelot. Ubicaci\u00f3n: Madrid y alrededores, Espa\u00f1a; Sector: Desarrollo de programaci\u00f3n \u2026","url":"https:\/\/es.linkedin.com\/pub\/dir\/Daniel\/Cabezas"},{"title":"Rolando Daniel Castillo - YouTube","text":"Rolando Daniel Castillo. SubscribeSubscribedUnsubscribe. 372.","url":"https:\/\/www.youtube.com\/user\/rolandcas"},{"title":"Daniel Cabezas castillo | LinkedIn","text":"Ver el perfil profesional de Daniel Cabezas castillo en LinkedIn. LinkedIn es la red de negocios m\u00e1s grande del mundo que ayuda a profesionales como Daniel \u2026","url":"https:\/\/cl.linkedin.com\/in\/daniel-cabezas-castillo-ab421955"},{"title":"Cat\u00f3lica sigue sin levantar cabeza tras sufrir \u2026","text":"Marzoli Nicol\u00e1s Castillo marca doblete en espectacular remontada de Pumas ante Tijuana Opinan los lectores Lilian en Pescadores chilenos protestan e \u2026","url":"http:\/\/chile.noticiasabc.com\/2016\/08\/21\/catolica-sigue-sin-levantar-cabeza-tras-sufrir-dura-derrota-ante-wanderers\/"},{"title":"Javier Ghigliotto | LinkedIn","text":"Daniel Cabezas castillo. GERENTE HOTEL SUITE TERRADO en HOTEL SUITE TERRADO. Maximiliano Rojas Schaus. Gerente en Hoteles Plaza El Bosque \u2026","url":"https:\/\/cl.linkedin.com\/in\/javier-ghigliotto-6745657"},{"title":"ArtStation - Daniel Castillo","text":"Environment artist at Mercury Steam Entertainment\u2026","url":"https:\/\/www.artstation.com\/artist\/danicastillo83"},{"title":"Daniel Cabezas Castillo | LinkedIn","text":"Ver el perfil profesional de Daniel Cabezas Castillo en LinkedIn. LinkedIn es la red profesional m\u00e1s grande del mundo que ayuda a profesionales como Daniel \u2026","url":"https:\/\/es.linkedin.com\/in\/daniel-cabezas-castillo-5019189a"},{"title":"Carlos Cabezas. Fotos de baloncesto ACB, Liga \u2026","text":"Twittear Carlos Cabezas Foto \u00a9 Cipriano Fornas Esta foto es parte de un \u00e1lbum Todas las fotos del UCAM Murcia URL Todas las fotograf\u00edas aqu \u2026","url":"http:\/\/www.encancha.com\/fotos\/foto43568.html"},{"title":"Humberto Arturo Espinoza Castillo | Facebook","text":"Daniel Cabezas Castillo \u00b7 Edith Castillo \u00b7 Eleiny Castillo \u00b7 Juan Guadalupe Espinoza Enrriquez \u00b7 Humberto Daniel Soares Martelo. (Humberto Martelo Foguinho).","url":"https:\/\/www.facebook.com\/humbertoarturo.espinozacastillo"},{"title":"It works!","text":"It works!","url":"http:\/\/pornogaymix.net\/?p=80"},{"title":"Daniel Cabezas | LinkedIn","text":"Daniel Cabezas Castillo. Senior PHP developer en Hocelot. Espa\u00f1a. Daniel Cabezas Garc\u00eda. lawyer in CLECE \/Abogado en CLECE. Espa\u00f1a. Daniel Cabezas \u2026","url":"https:\/\/es.linkedin.com\/in\/daniel-cabezas-47466a101"},{"title":"Falla Camino Alba - Castillo Cullera 2013 - El \u2026","text":"Falla Virgen de la Cabeza - Jos\u00e9 Maria Mortes Lerma Infantil 2017 Falla Plaza del Pouet Infantil 2017 Falla Plaza de la Tienda Infantil 2017 Falla Dr. \u2026","url":"http:\/\/bdfallas.com\/falla\/camino-alba-castillo-cullera-2013-el-mon-de-lart\/3481\/"},{"title":"Account Suspended","text":"This Account has been suspended.","url":"http:\/\/ablegamersfellowship.com\/"},{"title":"Daniel Cabezas | LinkedIn","text":"Daniel Cabezas Castillo. Senior PHP developer en Hocelot. Espa\u00f1a. Daniel Cabezas. Sales Department Allforsail S.L.. Espa\u00f1a. Daniel Cabezas Ballestero.","url":"https:\/\/es.linkedin.com\/in\/daniel-cabezas-37927229"},{"title":"Generated Name Cabeza | Last Name Generator","text":"Daniels 181. \u2026 Daniel 382. Cross 383. Simon 384. Dennis 385. Oconnor 386. Quinn 387. Gross 388. Navarro 389. Moss 390. Fitzgerald 391. \u2026","url":"http:\/\/www.lastnamegenerator.org\/last_name_that_start_with_c\/cabeza"},{"title":"faculty-staff.ou.edu\/C\/Di.Cui-1","text":"Congratulations! You have successfully activated your OU Web Account. You should replace this page (home.html) with your own web page. For web publishing help please visit http:\/\/support.ou.edu.","url":"http:\/\/faculty-staff.ou.edu\/C\/Di.Cui-1\/"},{"title":"Inform\u00e1tica Autor: Daniel Cabezas Castillo. Hardware \u2026 - SlidePlayer","text":"Inform\u00e1tica Autor: Daniel Cabezas Castillo. Hardware Ordenador Ordenador Perif\u00e9ricos Perif\u00e9ricos -Perif\u00e9ricos de entrada -Perif\u00e9ricos de salida -Perif\u00e9ricos.","url":"http:\/\/slideplayer.es\/slide\/5441292\/"},{"title":"DANIEL : \"Estamos dispuestos a trabajar con \u2026","text":"rcito de Nicaragua Comandante Daniel Ortega, Presidente de la Rep \u2026 Ej\u00e9rcito de Nicaragua! Palabras de Daniel Gracias, Su Eminencia Cardenal Miguel \u2026","url":"http:\/\/www.tortillaconsal.com\/tortilla\/es\/node\/18319"},{"title":"Noticias de Colombia y el mundo | Se\u00f1al en vivo BLU Radio","text":"Decomisan 44 cabezas de ganado de contrabando en Santander.","url":"http:\/\/www.bluradio.com\/paz"},{"title":"Un proyecto de ley y la realidad ~ Daniel Muchnik \u2026","text":"Facebook Jos\u00e9 Castillo Hace 145 d\u00edas \u00bfPeligra el futuro del sistema previsional? Twitter Facebook Ruben Ull\u00faa Hace 145 d\u00edas Momento de cubrir \u2026","url":"http:\/\/opinion.infobae.com\/daniel-muchnik\/2016\/04\/23\/un-proyecto-de-ley-y-la-realidad\/"},{"title":"www2.esmas.com\/en-nombre-del-amor\/personajes\/052355\/luz","text":"Carmen Castillo.","url":"http:\/\/www2.esmas.com\/en-nombre-del-amor\/personajes\/052355\/luz\/"},{"title":"Inform\u00e1tica Autor: Daniel Cabezas Castillo. Hardware Ordenador \u2026","text":"24 Ene 2016 \u2026 Download Inform\u00e1tica Autor: Daniel Cabezas Castillo. Hardware Ordenador Ordenador Perif\u00e9ricos Perif\u00e9ricos -Perif\u00e9ricos de entrada \u2026","url":"http:\/\/docslide.net\/documents\/informatica-autor-daniel-cabezas-castillo-hardware-ordenador-ordenador-perifericos.html"},{"title":"Cristian Ayala Cabeza (Ayala) Real Valladolid \u2026","text":"Kevin Adrian Daniel Vadillo Marc Valiente Nico Jose Shlomi Zitoon Alexander Zonov 06.04.09: Ayala spielt in der Real Valladolid-Jugend. Strona Startowa \u2026","url":"http:\/\/www.pilkowe-talenty.com\/Cristian_Ayala_Cabeza-3_7-26966-6.html"},{"title":"archive.fiba.com: Players","text":"Jose Pablo DEL CASTILLO VAZQUEZ.","url":"http:\/\/archive.fiba.com\/pages\/eng\/fa\/p\/q\/blatch\/players.html"},{"title":"Daniel Cabezas Facebook, Twitter & MySpace on PeekYou","text":"Looking for Daniel Cabezas ? PeekYou\'s people search has 35 people named Daniel Cabezas and you can find info, photos, links, family members and more.","url":"http:\/\/www.peekyou.com\/daniel_cabezas"},{"title":"PAISAJE PARA DANIEL MORDZINSKI | Ant\u00f3n Castro","text":"que es una locura decirle esto, Daniel, pero intento que algo de lo que \u2026 La foto de Daniel Mordzinski la tomo de aqu\u00ed: http:\/\/ocio. \u2026","url":"http:\/\/antoncastro.blogia.com\/2015\/051302-paisaje-para-daniel-mordzinski.php"},{"title":"Official website","text":"Castillo soria, alberto.","url":"http:\/\/www.fexfutbol.com\/"}]},"status":200},"geo_fraud":{"hits":"No encontrado","status":400},"risk_score":{"hits":{"nombre":"Daniel","ape":"Cabezas\/Castillo","direccion":"CL COCHERAS 4    A 00 02 Madrid - 28007","dni":"51480754s","sexo":"M","edad":25,"provincia":"Madrid","poblacion":"Madrid","rating_eq":"A-","renta_vivienda":"1026.5792","tasa_esfuerzo":"0.4230","fiabilidad_pago":"94.3000","morosidad":"3.50","score":593,"dependiente":"S","metros_cuadrados":122,"precio":351970,"umbral_pobreza":"190.1398","tasa_paro_correg":"0.1060","renta_vivienda_hogar":"1026.5792","renta_vivienda_individual":"814.2056","capacidad_ahorro":"255.8024","ingreso_mensual_hogar":"3774.1882","ingreso_mensual_individual":"1924.8360","cnt_movil1":"622422097","cnt_email1":"d.cabezas@hocelot.com","cnt_fechanac":"1991-12-18T00:00:00","desviacion_renta":"147.36","probabilidad_impago":"0.0544","nivel_estudios":3,"tenencia":3,"tipo_moroso":"3","trabajo":0,"grupo_gastos_individual":{"alimentacion":173.23524,"alcohol_tabaco":21.7506468,"ropa_calzado":58.1300472,"vivienda_suministros":814.205628,"mobiliario":67.1767764,"salud":33.8771136,"transporte":326.8371528,"comunicaciones":59.0924652,"ocio":88.1574888,"educacion":10.586598,"restaurantes":140.7055116,"otros":82.3829808},"grupo_gastos_hogar":{"alimentacion":339.676938,"alcohol_tabaco":42.64832666,"ropa_calzado":113.98048364,"vivienda_suministros":1596.4816086,"mobiliario":131.71916818,"salud":66.42571232,"transporte":640.85715636,"comunicaciones":115.86757774,"ocio":172.85781956,"educacion":20.7580351,"restaurantes":275.89315742,"otros":161.53525496},"periodo_medio_pago":13},"status":200},"form":{"name":"Daniel","lastName":"Cabezas Castillo","phone":"622422097","email":"d.cabezas@hocelot.com"}}';
        $result = json_decode($json, true);
        $result['json'] = $json;
        $result['risk_score']['hits'] = json_decode(json_encode($result['risk_score']['hits']));
        $res = view('newApi.demoNewResponse', $result)->render();
        return $res;
    }
    public function getPdfFromJson(Request $request){
        $json = str_replace('&quot;','"',$request->params['json']);
        $result = json_decode($json, true);
        $result['risk_score']['hits'] = json_decode(json_encode($result['risk_score']['hits']));
        $name = '../pdf/' .$result['id_check']['hits']['nif']. '.pdf';
        //$res = view('newApi.demoMainResponse', $result)->render();
        $pdf = PDF::loadView('newApi.demoMainResponse', $result);
        $pdf->setOption('enable-javascript', true);
        $pdf->setOption('javascript-delay', 1000);
        $pdf->setOption('no-stop-slow-scripts', true);
        file_put_contents($name, $pdf->output());
        return response()->json(['result' => $name, 'json'=> $pdf, 'status' => '200']);
    }
    public function downloadRate(Request $request)
    {
//        $path = "../files/rates/";
//        $name = $request->input('pdf_file');
//        $fullPath = $path.$name;
//        if ($fd = fopen ($fullPath, "r")) {
//            $fsize = filesize($fullPath);
//            $path_parts = pathinfo($fullPath);
//            $file = str_replace(Session::get('ter_cod').'-', '', $path_parts["basename"]);
//            if (strpos($path_parts["basename"], Session::get('ter_cod') . '-') === 0) {
//                $ext = strtolower($path_parts["extension"]);
//                switch ($ext) {
//                    case "pdf":
//                        header("Content-type: application/pdf");
//                        header("Content-Disposition: attachment; filename=\"" . $file . "\"");
//                        break;
//                    default;
//                        header("Content-type: application/octet-stream");
//                        header("Content-Disposition: filename=\"" . $file . "\"");
//                        break;
//                }
//                header("Content-length: $fsize");
//                header("Cache-control: private");
//                while (!feof($fd)) {
//                    $buffer = fread($fd, 2048);
//                    echo $buffer;
//                }
//            }else{
//                return 'No tienes permiso para ver el archivo.';
//            }
//        }
//        fclose ($fd);
//        exit;
    }
}