import requests
import sys
import json
from bs4 import BeautifulSoup

dni = sys.argv[2]
name = sys.argv[3]
url = "https://www2.agenciatributaria.gob.es/wlpl/TOCP-MULT/Identificacion"

proxies = {'https': 'http://mrhc:QwEr1234@{0}'.format(sys.argv[1])}

sess = None
sess = requests.Session()

payload = "faccion=V&USUARIO=&fnif={0}&fnombre={1}".format(dni,name)

headers = {
    'origin': "https://www2.agenciatributaria.gob.es",
    'accept-encoding': "gzip, deflate, br",
    'accept-language': "es,en-GB;q=0.8,en;q=0.6",
    'upgrade-insecure-requests': "1",
    'user-agent': "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36",
    'content-type': "application/x-www-form-urlencoded",
    'accept': "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
    'referer': "https://www2.agenciatributaria.gob.es/wlpl/TOCP-MULT/Identificacion",
    'connection': "keep-alive",
    'cache-control': "no-cache"
    }
try:
    response = sess.request("POST", url, proxies = proxies, data=payload, headers=headers, timeout=(0.2,None))
    soup = BeautifulSoup(response.text,"html.parser")

    array = soup.select("#contenedor > .AEAT_navegacion > h1 > h2")
    if array:
        array = array[0].getText().split('-')
        array[0] = array[0].strip()
        array[1] = array[1].strip()
        print(json.dumps(array))
    else:
        print(json.dumps("NONE"))
except Exception as e:
    print(json.dumps("TIMEOUT"))