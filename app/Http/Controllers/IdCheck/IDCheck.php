<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 6/09/16
 * Time: 12:56
 */

namespace App\Http\Controllers\IdCheck;


use App\Http\Controllers\Proxies\Proxies;
use App\Http\Models\UtilClass;
use Elastica\Util;
use Log;


class IDCheck
{
    public static function check_id($name, $first_name, $last_name, $dni)
    {
//        if (strlen($dni) < 9) {
//            return ['dni_encontrado' => $dni, 'hits' => ['name_surname' => $name . ' ' . $first_name . ' ' . $last_name, 'nif' => $dni, 'score_id_check' => 'KO', 'aeat_name' => '-', 'correlacion_id_check' => 0.0], 'status' => 400];
//        }

//        return ['dni_encontrado' => $dni, 'hits' => ['name_surname' => $name . ' ' . $first_name . ' ' . $last_name, 'nif' => $dni, 'score_id_check' => 'OK', 'aeat_name' =>$first_name . ' ' . $last_name.' '.$name, 'correlacion_id_check' => 100], 'status' => 200];

        if (preg_match('/[A-Za-z]/', substr($dni, 0, 1))) {
            $letter = substr($dni, 0, 1);
            $rest = substr($dni, 1);
            $rest = str_pad($rest, 8, '0', STR_PAD_LEFT);
            $dni = $letter . $rest;
        } else $dni = str_pad($dni, 9, '0', STR_PAD_LEFT);

        $dni = strtoupper($dni);

        $letra = substr($dni, -1, 1);
        $numero = substr($dni, 0, 8);

        // Si es un NIE hay que cambiar la primera letra por 0, 1 ó 2 dependiendo de si es X, Y o Z.
        $numero = str_replace(array('X', 'Y', 'Z'), array(0, 1, 2), $numero);

        $modulo = $numero % 23;
        $letras_validas = "TRWAGMYFPDXBNJZSQVHLCKE";
        $letra_correcta = substr($letras_validas, $modulo, 1);

        if ($letra_correcta != $letra) {
            return ['dni_encontrado' => $dni, 'hits' => ['name_surname' => $name . ' ' . $first_name . ' ' . $last_name, 'nif' => $dni, 'score_id_check' => 'KO', 'aeat_name' => 'dni o nie mal formado', 'correlacion_id_check' => 0.0], 'status' => 400];
        }

        $searchs = [
            self::encodeUrlParam($first_name),
            self::encodeUrlParam($first_name) . '/' . self::encodeUrlParam($last_name),
            self::encodeUrlParam($first_name . ' ' . $last_name . ' ' . $name)
        ];
        foreach ($searchs as $search) {
            $daas_aeat = self::file_get_contents_curl(env("ZUUL_URL") . '/daas-aeat/aeat/' . urlencode($dni) . '/' . $search);
            if ($daas_aeat['http_status'] === 200) break;
        }

        $id_check = array(
            'nif' => $dni,
            'name_surname' => $name . ' ' . $first_name . ' ' . $last_name
        );
        //json_decode
        if ($daas_aeat['http_status'] === 404) {
            $id_check['score_id_check'] = 'KO';
            $status = 400;
            $aeat_name = "-";
            $dni_encontrado = $dni;
            $percent = 0.0;
        } else if ($daas_aeat['http_status'] === 503 || $daas_aeat['http_status'] !== 200) {
            $id_check['score_id_check'] = 'NC';
            $status = 402;
            $aeat_name = "-";
            $dni_encontrado = $dni;
            $percent = 0.0;
        } else {
            $curl_results = json_decode($daas_aeat['response']);
            $dni_encontrado = $curl_results->documentNumber;
            $aeat_name = mb_convert_encoding($curl_results->fullName, 'UTF-8');
            $cmp_name = mb_strtoupper(self::cleanStr($first_name . ' ' . $last_name . ' ' . $name), 'UTF-8');
            similar_text(mb_strtoupper(self::cleanStr($aeat_name), 'UTF-8'), $cmp_name, $percent);
            $percent = round($percent, PHP_ROUND_HALF_EVEN);

            if ($percent == 0.0) {
                //Nuevo del 28-03-2017
                $contenido = false;
                $array_names = explode(' ', $name);
                $cmp_f_n = mb_strtoupper(self::cleanStr($first_name), 'UTF-8');
                $cmp_l_n = mb_strtoupper(self::cleanStr($last_name), 'UTF-8');
                $cmp_aeat_name = mb_strtoupper(self::cleanStr($aeat_name), 'UTF-8');
                foreach ($array_names as $aux_name) {
                    $cmp_name = mb_strtoupper(self::cleanStr($aux_name), 'UTF-8');
                    if (!empty($cmp_name) && !empty($cmp_f_n) && !empty($cmp_l_n)) {
                        if (stristr($cmp_aeat_name, $cmp_name) && stristr($cmp_aeat_name, $cmp_f_n) && stristr($cmp_aeat_name, $cmp_l_n)) {
                            $contenido = true;
                            break;
                        }
                    }
                }
                if (!$contenido) {
                    $id_check['score_id_check'] = 'KO';
                    $status = 400;
                } else {
                    $id_check['score_id_check'] = 'OK';
                    $status = 200;
                }
            } else {
                $id_check['score_id_check'] = 'OK';
                $status = 200;
            }

        }


        $id_check['aeat_name'] = $aeat_name;
        $id_check['correlacion_id_check'] = $percent;

        //Log::debug(print_r($save, true));
        return ['dni_encontrado' => $dni_encontrado, 'hits' => $id_check, 'status' => $status];

    }

    private static function encodeUrlParam($param)
    {
        return str_replace("+","%20",urlencode(self::cleanStr($param)));

    }

    private static function cleanStr($str)
    {
        return UtilClass::del_articulos(mb_strtoupper(UtilClass::elimina_acentos(trim($str))));
    }

    private static function file_get_contents_curl($url)
    {
        $fc = curl_init();
        curl_setopt($fc, CURLOPT_URL, $url);
        curl_setopt($fc, CURLOPT_HTTPGET, true);
        curl_setopt($fc, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($fc, CURLOPT_HEADER, 0);
        curl_setopt($fc, CURLOPT_VERBOSE, 0);
        $res = curl_exec($fc);
        $http_status = curl_getinfo($fc, CURLINFO_HTTP_CODE);
        curl_close($fc);
        Log::debug("AEAT HTTP ".$http_status);
	return ['response' => $res, 'http_status' => $http_status];

    }
}
