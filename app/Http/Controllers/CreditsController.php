<?php
/**
 * Created by PhpStorm.
 * User: a.luis
 * Date: 14/07/2016
 * Time: 14:30
 */

namespace App\Http\Controllers;



use App\Http\Models\CreditsModel;
use App\Http\Models\OfferModel;
use App\Http\Models\UserModel;
use App\Libraries\RedsysAPI;
use Illuminate\Http\Request;

class CreditsController extends Controller {


	public function __construct(Request $request)
	{
		parent::__construct($request);
	}

    /**
     *
     *
     *
     *
     * PROBAR FORMA CALLBACK PARA SABER A QUIEN TENGO QUE LLAMAR O VOLER URL_CALLBACK
     * GUARDARLA EN DB_SESSION_BUY TODO
     *
     *
     *
     */

    public function generateFormTPV(){

        //desencriptar params...
        $request_params = unserialize(base64_decode($this->request_params->input('params')));

        // Me pasan num_creditos a comprar, id_cliente, urlokko
        $miObj = new RedsysAPI;

        $fuc = "335001665"; //IDENTIFICADOR DE TIENDA
        $terminal = "001"; //TERMINAL DE TIENDA
        $moneda = "978"; //MONEDA EURO
        $trans = "0"; //TIPO DE TRANSACCIÓN:0 NORMAL, 5 RECURRENTE
        $amount = OfferModel::getOfferPrice(UserModel::getType($this->request_params['client_id']),$this->request_params['num_credits']);
        // $url=""; //NADA
        //PASAMOS LA URL DE SITIO DEL CLIENTE
        //LOS DOS ÚLTIMOS NÚMEROS SON LOS DECIMALES PARA REDSYS.
        //Genero numero pedido
        $chars = 'QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm0123456789';
        $length_chars = strlen($chars);
        $result = '';
        for ($x = 0; $x <= 1; $x++)
        {
            $pos = rand(0, $length_chars);
            $result .= substr($chars, $pos, 1);
        }
        $num_pedido = date('Y') . date('m') . date('H') . date('s') . $result;

        // Se Rellenan los campos
        $miObj->setParameter("DS_MERCHANT_AMOUNT", $amount);
        $miObj->setParameter("DS_MERCHANT_ORDER", $num_pedido);
        $miObj->setParameter("DS_MERCHANT_MERCHANTCODE", $fuc);
        $miObj->setParameter("DS_MERCHANT_CURRENCY", $moneda);
        $miObj->setParameter("DS_MERCHANT_TRANSACTIONTYPE", $trans);
        $miObj->setParameter("DS_MERCHANT_TERMINAL", $terminal);
        $miObj->setParameter("DS_MERCHANT_MERCHANTURL", $this->request_params['urlkook']);//Cambiar a ruta api para procesar pago
        $miObj->setParameter("DS_MERCHANT_URLOK", $this->request_params['urlok']);
        $miObj->setParameter("DS_MERCHANT_URLKO", $this->request_params['urlkook']);
        //Datos de configuración
        //$kc = 'sq7HjrUOBfKmC576ILgskD5srU870gJ7'; //Clave EN PRUEBAS DE REDSYS
        $kc = 'O8aNhRlx9r3RGWWKNBAYrIL5OdkbfN+i'; // R. Marcos 14-04-2016
        $params = $miObj->createMerchantParameters();
        $signature = $miObj->createMerchantSignature($kc);
        $data = array (
            'params'       => $params,
            'signature'    => $signature,
            'num_pedido'   => $num_pedido,
            'importe'      => $amount
        );
        CreditsModel::setSessionBuy($this->request_params['client_id'], $num_pedido);
        return response()->json(['form_params'=>$data,'status'=>200]);
    }

	public function buyCredits()
	{

	}

	public function consumeCredits()
	{

	}

	public function returnCredits()
	{

	}
}