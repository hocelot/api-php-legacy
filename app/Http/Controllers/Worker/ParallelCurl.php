<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 27/04/17
 * Time: 13:03
 */

namespace App\Http\Controllers\Worker;


use App\Http\Controllers\Proxies\Proxies;

class ParallelCurl
{
    private $busy;

    public function __construct()
    {
        $this->busy = false;
    }

    public function free(){
        $this->busy = false;
    }

    public function setBusy(){
        $this->busy = true;
    }

    public function isBusy(){
        return $this->busy;
    }

    public function startRequest($page,$service, $options)
    {
        $ch = curl_init();
        $obj_proxy = new Proxies($service);
        $ip_real = false;
        do {
            $ip = $obj_proxy->get();
            $obj_proxy->setUsed($ip);
            curl_setopt_array($ch, $options);
            curl_setopt($ch, CURLOPT_URL, $page);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            if ($ip) {
                curl_setopt($ch, CURLOPT_PROXY, $ip);
                curl_setopt($ch, CURLOPT_PROXYUSERPWD, "mrhc:QwEr1234");
            } else  $ip_real = true;
            $result = curl_exec($ch);
            $info = curl_getinfo($ch);
            curl_close($ch);
            if (!$ip_real) {
                $obj_proxy->unlock($info["http_code"]);
            }
        } while ($info["http_code"] == 400 && !$ip_real);
        $this->busy = false;
        if ($info["http_code"] == 400 && $ip_real) return false;
        return ['url' => $info['url'], 'html' => $result];
    }

}
