<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 1/12/16
 * Time: 19:42
 */

namespace App\Http\Controllers\Worker;

class Threaded extends \Threaded
{
    public function run()
    {
        require_once(__DIR__.'/../../../../vendor/autoload.php');
        require_once(__DIR__.'/../../../../bootstrap/app.php');
        parent::run();

    }
}