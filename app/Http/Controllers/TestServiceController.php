<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 27/10/16
 * Time: 21:48
 */

namespace App\Http\Controllers;


use App\Http\Controllers\GeoCheck\GeoCheck;
use App\Http\Controllers\GeoFraud\GeoFraud;
use App\Http\Controllers\IdFraud\IdFraud;
use App\Http\Controllers\Proxies\Proxies;
use App\Http\Controllers\RiskScore\ScoreController;
use App\Http\Controllers\SocialMedia\SocialMediaManager;
use App\Http\Controllers\Worker\ThreadsManager;
use Goutte\Client;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Jobs\WsdlJob;
use Log;
use stdClass;

class TestServiceController extends BaseController
{

    public function initialize()
    {

        echo 'Empiezo';
        if (($archivo = fopen('/var/www/api/storage/logs/prueba.csv', 'r')) !== FALSE) {
            while (($datos = fgetcsv($archivo, 1000, ";")) !== FALSE) {

                $result = array();
                //nombre,ape1,ape2,dni,fecha_nac,pais,sexo,cp,municipio,tv,nv,num,otro,movil,email
                $name = $datos[0];
                $ape1 = $datos[1];
                $ape2 = $datos[2];
                $dni = $datos[3];
                $mun = $datos[7];
                $tlf = $datos[13];
                $email = $datos[14];

                $worker = new ThreadsManager();
                /*START ID FRAUD*/
                $id_fraudobj = new IdFraud($dni,$email, $tlf, $name, $ape1, $ape2);
                $worker->stack($id_fraudobj);

                // Start all jobs
                $worker->start();
                // Join all jobs and close worker
                $worker->shutdown();
                foreach ($worker->data as $key => $value) {
//                    if ($key == 'fb') {
//                        $facebook = $value;
//                    } else {
                        $result = array_merge($result, [$key => $value]);
//                    }
                }
                /*********************** FIN DEL PRIMER WORKER **************************************/

                /************************ WORKER PARA GeoFraud, Risk Score y Social *****************/
//                $worker = new ThreadsManager();
//
//                if (!isset($facebook) || !$facebook) $facebook = ['hits' => 'No encontrado', 'status' => 400];
//                $search_social = new SocialMediaManager($facebook, $name, $ape1, $ape2, $email, $tlf, $mun);
//                //$result['social'] = $search_social->searchAll();
//                $worker->stack($search_social);
//
//
//                // Start all jobs
//                $worker->start();
//                // Join all jobs and close worker
//                $worker->shutdown();
//                foreach ($worker->data as $key => $value) {
//                    $result = array_merge($result, [$key => $value]);
//                }
//        //Destruimos el worker y los objetos
//        unset($risk_obj);
//        unset($obj_geo_fraud);
//        unset($worker);
//                $result = array_merge($result, ['fb_email' => $this->comprobar($email)]);
//                $result = array_merge($result, ['fb_tlf' => $this->comprobar($tlf)]);
                $dir = '/var/www/api/storage/logs/'.$dni.'.txt';
                file_put_contents($dir,print_r($result,true));
                echo 'FIN';
//                break;
            }
        }
    }

    private function comprobar($search)
    {
//        $obj_proxy = new Proxies('fb');
//        $ip_real = false;
//        $ip = $obj_proxy->get();
//        $obj_proxy->setUsed($ip);
//        $obj_proxy->unlock(400);
        sleep(120);
        $client = new Client();
        $client->setHeader('User-Agent', "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36");
        $crawler = $client->request('GET', 'https://www.facebook.com/login/identify?ctx=recover');
        $var = strstr($crawler->html(), '["LSD",');
        $array = explode('"', $var);
        $lsd = $array[5];
        $var = strstr($crawler->html(), '["_js_datr",');
        $array = explode('"', $var);
        $datr = $array[3];
        unset($array);
        unset($client);
        unset($crawler);
        $cmd = escapeshellcmd('/usr/bin/python /var/www/api/storage/logs/facebook.py '.$lsd.' '.$datr.' '.$search);
        $python = shell_exec($cmd);
        Log::debug($python);
        $crawler = new \Symfony\Component\DomCrawler\Crawler();
        $crawler->addHtmlContent(json_decode($python));
        $email = $phone = $img = $name = false;
//        echo $crawler->html();
//        if ($crawler->filter('.uiBoxRed')->count() < 1) {
        if ($crawler->filter('label[for="send_email"] > div > div > div > div')->count() > 0) {
            $email = trim($crawler->filter('label[for="send_email"] > div > div > div > div')->text());
            if (stristr($email, "Email me a link to reset my password")) {
                $email = trim(str_replace("Email me a link to reset my password", "", $email));
            }
        } else $email = 'No tiene correo asociado';

        if ($crawler->filter('tr:last-of-type > td > div > label > div > div > div > div')->count() > 0) {
            $phone = trim($crawler->filter('tr:last-of-type > td > div > label > div > div > div > div')->text());
        } else $phone = 'No tiene telefono asociado';

        if ($crawler->filterXPath("//html/body/div/div[@id='globalContainer']/div[@id='content']/div/form/div/div[2]/table/tr/td[2]/div/div/div[1]/img")->count() > 0) {
            $img = $crawler->filterXPath("//html/body/div/div[@id='globalContainer']/div[@id='content']/div/form/div/div[2]/table/tr/td[2]/div/div/div[1]/img")->attr('src');
        } else $img = 'No tiene imagen asociada';

        if ($crawler->filterXPath("//html/body/div/div[@id='globalContainer']/div[@id='content']/div/form/div/div[2]/table/tr/td[2]/div/div/div[2]/div")->count() > 0) {
            $name = trim($crawler->filterXPath("//html/body/div/div[@id='globalContainer']/div[@id='content']/div/form/div/div[2]/table/tr/td[2]/div/div/div[2]/div")->text());
            $name = trim(str_replace('Facebook User', '', trim($name)));
        } else $name = 'No tiene nombre público';

//        }
        return [$email,$phone,$img,$name];
    }

//
//
//// Start all jobs
//        $worker->start();
//
//// Join all jobs and close worker
//        $worker->shutdown();
//        Log::debug(print_r($search->getTerminationInfo(), true));
//        Log::debug(print_r($worker->getTerminationInfo(), true));
//
//        Log::debug(print_r($worker->data, true));
//        foreach ($worker->data as $html) {
//            echo substr($html, 0, 20) . PHP_EOL;
//        }
}


//Guardamos results andrés id frau

//            $params_comunes->resultado = $result['id_fraud']['hits']['email']['estado'];
//            $params_comunes->id_fraud = $result['id_fraud']['hits']['email']['score'];
//            $params_comunes->nick = $result['id_fraud']['hits']['email']['alias'];
//
//            //Nos guardamos los resultados de facebook para no tener que volver a llamar
//            $facebook = $id_fraudobj->facebook;
//            unset($id_fraudobj);
//
//            $val_astr_mr_id_fraud['str_mr_id_fraud'] = $params_comunes;
//            $this->dispatch((new WsdlJob(['val_astr_mr_id_fraud' => $val_astr_mr_id_fraud], 'mr_id_fraud', 'clientes')));
//
//            unset($val_astr_mr_id_fraud);
//            unset($params_comunes->resultado);
//            unset($params_comunes->id_fraud);
//            unset($params_comunes->nick);


// GEO-CHECK
//            $result['geo_check'] = $obj_geo_check->normalizeAddress();
//
//            if ($result['geo_check']['hits']['level'] == 3) $params_comunes->resultado = 'KO';//OK,KO
//            else $params_comunes->resultado = 'OK';//OK,KO
//            $params_comunes->geo_check = $result['geo_check']['hits']['level'];
//            $params_comunes->direccion = $result['geo_check']['hits']['address'];
//            unset($obj_geo_check);
//
//            $val_astr_mr_geo_check['str_mr_geo_check'] = $params_comunes;
//
//
//            $this->dispatch((new WsdlJob(['val_astr_mr_geo_check' => $val_astr_mr_geo_check], 'mr_geo_check', 'clientes')));
//
//            unset($val_astr_mr_geo_check);
//            unset($params_comunes->resultado);
//            unset($params_comunes->geo_check);
//            unset($params_comunes->direccion);
//
//            if ($result['geo_check']['status'] == 400) {
//                return $result;
//            }