<?php
/**
 * Created by PhpStorm.
 * User: Alex San Luis
 * Date: 16/06/2016
 * Time: 16:18
 */

namespace App\Http\Controllers\Address;


use App\Http\Controllers\Controller;
use App\Http\Models\AddressModel;
use Illuminate\Http\Request;
use Log;

class AddressController extends Controller
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
    }

    /**
     * @return mixed
     */

//    public function getIneCode()
//    {
//        $status = 200;
//        $this->request_params->zip_code = str_pad($this->request_params->zip_code, 5, "0", STR_PAD_LEFT);
//        $pob = AddressModel::getPoblation($this->request_params->zip_code);
//        if (!isset($pob->ref_astr_codpost->str_adcodpost)) {
//            $status = 400;
//            $resp = 'Error codigo postal incorrecto';
//        } else {
//            $this->request_params->town = $pob->ref_astr_codpost->str_adcodpost[0]->poblacion;
//            $resp = AddressModel::getIneCode($this->request_params);
//            if (!empty($resp->ref_as_mensaje)) {
//                $resp = 'Hay algún parametro incorrecto';
//                $status = 400;
//            } else {
//                $resp = $resp->ref_as_cod_ine;
//            }
////            $res = $this->sendResponseEncrypted($resp, $status);
//        }
//
//        return response()->json($this->sendResponseEncrypted('code', $resp, $status));
//    }

    public function getIneCode()
    {
        /*
         * Params de entrada:
         *  zip_code codigo postal del que sacamos codigo provincia
         *  town poblacion que ha metido el usuario
         *  cod_muns array con los posibles codigos de municipios
         * */

        $status = 200;
        $this->request_params->zip_code = str_pad($this->request_params->zip_code, 5, "0", STR_PAD_LEFT);
        $resp = AddressModel::getIneCode($this->request_params);
        if (!$resp) {
            $status = 400;
            $resp = 'Error no se ha encontrado municipio';
            return response()->json(['msg' => $resp, 'status' => $status]);

        }
//            $res = $this->sendResponseEncrypted($resp, $status);


        return response()->json(['code' => $resp, 'status' => $status]);
    }

    public function getTown()
    {
        $status = 200;
        $town = AddressModel::getPoblation(str_pad($this->request_params->zip_code, 5, "0", STR_PAD_LEFT));
//        Log::debug(print_r($town, true));

        if (!isset($town->ref_astr_codpost->str_adcodpost_nuevo)) {
            $status = 400;
            $town = 'Error codigo postal incorrecto';
            return response()->json(['msg' => $town, 'status' => $status]);

        } else {
            $town = $town->ref_astr_codpost->str_adcodpost_nuevo;
        }
        return response()->json(['towns' => $town, 'status' => $status]);

//        return response()->json($this->sendResponseEncrypted('towns', $town, $status));

    }

    public function getRoadType()
    {
        $results = AddressModel::getRoadType($this->request_params->code);
        $status = 200;
        if (!is_array($results)) {
            $status = 400;
            return response()->json(['msg' => $results, 'status' => $status]);

        }
        return response()->json(['street' => array('road_types' => $results), 'status' => $status]);
    }

    public function getStreetName()
    {
        //Log::debug(print_r($this->request_params,true));
        $results = AddressModel::getStreets($this->request_params);
//        Log::debug(print_r($results,true));
        $status = 200;
        if (!is_array($results)) {
            $status = 400;
            return response()->json(['msg' => $results, 'status' => $status]);
        }
        return response()->json(['street' => array('names' => $results), 'status' => $status]);

//        return response()->json(['street' =>$res, 'status' => $status]);
    }

    public function getNumber()
    {
        $results = AddressModel::getNumber($this->request_params);
        $status = 200;
        if (!is_array($results)) {
            $status = 400;
        }
//        return response()->json($this->sendResponseEncrypted('street', array('numbers' => $results), $status));
        return response()->json(['street' => array('numbers' => $results), 'status' => $status]);

//        return response()->json(['street' =>$res, 'status' => $status]);
    }

    public function getLetter()
    {
        $results = AddressModel::getLetter($this->request_params);
        $status = 200;
        if (empty($results) || $results == '' || !is_array($results)) {
            $status = 401;
        } else if (!$results) {
            $status = 400;
        }

        return response()->json(['street' => array('letters' => $results), 'status' => $status]);
    }

    public function getKm()
    {
        $results = AddressModel::getKm($this->request_params);
        $status = 200;
        if (!$results) {
            $status = 400;

        }
        if (empty($results)) {
            $status = 401;
        }
        return response()->json(['street' => array('kms' => $results), 'status' => $status]);
//        return response()->json($this->sendResponseEncrypted('street', array('kms' => $results), $status));

    }

    public function getBlock()
    {
        $results = AddressModel::getBlock($this->request_params);
        $status = 200;
        if (!$results) {
            $status = 400;
        }
        if (empty($results)) {
            $status = 401;
        }
//        $res = $this->sendResponseEncrypted(array('blocks' => $results), $status);
        return response()->json(['street' => array('blocks' => $results), 'status' => $status]);

//        return response()->json($this->sendResponseEncrypted('street', array('blocks' => $results), $status));

    }

    public function getStairs()
    {
        $results = AddressModel::getStairs($this->request_params);
        $status = 200;
        if (!$results) {
            $status = 400;
        }
        if (empty($results)) {
            $status = 401;
        }
//        $res = $this->sendResponseEncrypted(array('stairs' => $results), $status);
        return response()->json(['street' => array('stairs' => $results), 'status' => $status]);

//        return response()->json($this->sendResponseEncrypted('street', array('stairs' => $results), $status));

    }

    public function getFloor()
    {
        $results = AddressModel::getFloor($this->request_params);
        $status = 200;
        if (!$results) {
            $status = 400;
        }
        if (empty($results)) {
            $status = 401;
        }
//        $res = $this->sendResponseEncrypted(array('floors' => $results), $status);
        //Log::debug(print_r($results,true));
//        return response()->json($this->sendResponseEncrypted('street', array('floors' => $results), $status));
        return response()->json(['street' => array('floors' => $results), 'status' => $status]);

    }

    public function getDoor()
    {
        //Log::debug(print_r($this->request_params,true));
        $results = AddressModel::getDoor($this->request_params);
        $status = 200;
        if (!$results) {
            $status = 400;
        }
        if (empty($results)) {
            $status = 401;
        }

        return response()->json(['street' => array('doors' => $results), 'status' => $status]);

    }
//
//    public function getAccuracy()
//    {
//        return AddressModel::getAccuracy();
//    }


}