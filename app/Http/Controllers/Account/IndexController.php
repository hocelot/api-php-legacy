<?php
/**
 * Created by PhpStorm.
 * User: DC
 * Date: 05/01/17
 * Time: 16:05
 */

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use App\Http\Models\Account\IndexModel;
use Illuminate\Http\Request;
use Log;

class IndexController extends Controller
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
    }

    public function getIndex()
    {
        $result = IndexModel::getIndex($this->request_params->client_id);
        if ($result->status == 200) {
            return response()->json($result);
            // return response()->json(["status" => 200, "data" => $result]);
        } else {
            return response()->json(['msg' => 'Error al realizar petición', 'status' => $result->status]);
            //return response()->json(["status" => 400, "data" => 'Error al realizar petición']);
        }
    }

    public function getValMap()
    {
        $result = IndexModel::getValMap(json_decode($this->request_params->client_id));
        if ($result->status == 200) {
            return response()->json(['data' => $result->data, 'status' => $result->status]);
        } else {
            return response()->json(['msg' => 'Error al realizar petición', 'status' => $result->status]);
            //return response()->json(["status" => 400, "data" => 'Error al realizar petición']);
        }
    }
}