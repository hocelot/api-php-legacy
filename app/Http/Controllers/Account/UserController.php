<?php
/**
 * Created by PhpStorm.
 * User: Alex San Luis
 * Date: 16/06/2016
 * Time: 21:47
 */

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use App\Http\Models\Account\UserModel;
use Log;

class UserController extends Controller {

    /**
     * @return mixed
     */
    public function getProfile()
    {
        $result = UserModel::wsdlProfile(($this->request_params->client_id));
        if(empty($result->data->ref_astr_tgtercero)){
            $result->status = 400;
        }
        //Log::debug(print_r($result, true));
        return response()->json($result);
    }
    public function getSidebarProfile()
    {
        Log::debug('AQUI USERCONTROLLER '. print_r($this->request_params,true));
        $result = UserModel::wsdlSidebarProfile($this->request_params->client_id);
        return response()->json($result);

    }
//	public function getProfile()
//	{
//		$result = UserModel::wsdlProfile($this->request_params->client_id);
//		if (empty($result->ref_as_mensaje))
//		{
//
//			$slug_resp = UserModel::sqlProfile($this->request_params->client_id);
//
//			$apellidos = $result->ref_astr_tgtercero->str_tgterclientes_mirating[0]->apellidos;
//
//			$data = array (
//				'nombre'                     => $result->ref_astr_tgtercero->str_tgterclientes_mirating[0]->nombre,
//				'tipoindentif'               => $result->ref_astr_tgtercero->str_tgterclientes_mirating[0]->tipoindentif,
//				'numidentif'                 => $result->ref_astr_tgtercero->str_tgterclientes_mirating[0]->numidentif,
//				'nombrecom'                  => $result->ref_astr_tgtercero->str_tgterclientes_mirating[0]->nomcom,
//				'tipo_via'                   => $result->ref_astr_tgtercero->str_tgterclientes_mirating[0]->sigla,
//				'nombre_via'                 => $result->ref_astr_tgtercero->str_tgterclientes_mirating[0]->viapublica,
//				'casakm'                     => $result->ref_astr_tgtercero->str_tgterclientes_mirating[0]->casakm,
//				'dir_piso'                   => $result->ref_astr_tgtercero->str_tgterclientes_mirating[0]->piso,
//				'dir_puerta'                 => $result->ref_astr_tgtercero->str_tgterclientes_mirating[0]->puerta,
//				'sigla'                      => $result->ref_astr_tgtercero->str_tgterclientes_mirating[0]->sigla,
//				'viapublica'                 => $result->ref_astr_tgtercero->str_tgterclientes_mirating[0]->viapublica,
//				'escalera'                   => $result->ref_astr_tgtercero->str_tgterclientes_mirating[0]->escalera,
//				'bloque'                     => $result->ref_astr_tgtercero->str_tgterclientes_mirating[0]->bloque,
//				'km'                         => $result->ref_astr_tgtercero->str_tgterclientes_mirating[0]->dir_km,
//				'letra'                      => $result->ref_astr_tgtercero->str_tgterclientes_mirating[0]->dir_letra,
//				'poblacion'                  => $result->ref_astr_tgtercero->str_tgterclientes_mirating[0]->poblacion,
//				'provincia'                  => $result->ref_astr_tgtercero->str_tgterclientes_mirating[0]->provincia,
//				'codpostal'                  => $result->ref_astr_tgtercero->str_tgterclientes_mirating[0]->codpostal,
//				'telefono'                   => $result->ref_astr_tgtercero->str_tgterclientes_mirating[0]->telefono1,
//				'email'                      => $result->ref_astr_tgtercero->str_tgterclientes_mirating[0]->email,
//				'tipo'                       => $result->ref_astr_tgtercero->str_tgterclientes_mirating[0]->tipotercero,
//				'email_validado'             => $slug_resp,
//				'renta_libre'                => $result->ref_astr_tgtercero->str_tgterclientes_mirating[0]->renta_libre,
//				'score'                      => $result->ref_astr_tgtercero->str_tgterclientes_mirating[0]->score,
//				'importe_financiable_bowbuy' => $result->ref_astr_tgtercero->str_tgterclientes_mirating[0]->importe_financiable_bowbuy
//			);
//			if ($data['tipo'] == 'J')
//			{ // Preparamos el array para empresa
//				$data['nombre_empresa'] = $result->ref_astr_tgtercero->str_tgterclientes_mirating[0]->nomsoc;
//				$data['apellidos'] = str_replace('/', ' ', $apellidos);
//			}
//			else
//			{
//				$pos = strpos($apellidos, '/');
//				$ap1 = substr($apellidos, 0, $pos);
//				$ap2 = substr($apellidos, $pos + 1);
//				$data['sexo'] = $result->ref_astr_tgtercero->str_tgterclientes_mirating[0]->sexo;
//				$data['apellido1'] = $ap1;
//				$data['apellido2'] = $ap2;
//				$data['fechanac'] = $result->ref_astr_tgtercero->str_tgterclientes_mirating[0]->fechanac;
//			}
//			$status = 200;
//		}
//		else
//		{
//			$data = 'No existe el usuario';
//			$status = 400;
//		}
//
//		return response()->json([ 'user_data' => $data, 'status' => $status ]);
//	}

	//TODO UPDATE
	
}