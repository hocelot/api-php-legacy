<?php
/**
 * Created by PhpStorm.
 * User: DC
 * Date: 29/11/16
 * Time: 15:43
 */
namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use App\Http\Models\Account\RatesModel;
use Illuminate\Http\Request;
use Log;

class RatesController extends Controller
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
    }

    public function getRates()
    {
        $result = RatesModel::getRates(json_decode($this->request_params->client_id));
        if($result->status == 200) {
            if(!isset($result->data->val_asrt_tgmiratingeval->str_consultas)){
                //return response()->json(["status" => 200, "data" => $result->data->ref_as_mensaje, "empty" => true]);
                $result->empty = true;
                return response()->json(['data' => $result, 'status' => $result->status]);
            }else{
                //return response()->json(["status" => 200, "data" => $result->data->val_asrt_tgmiratingeval->str_consultas]);
                return response()->json(['data' => $result, 'status' => $result->status]);
            }
        }
        else{
            return response()->json(['msg' => $result, 'status' => $result->status]);
            //return response()->json(["status" => 400, "data" => 'Error al realizar petición']);
        }
    }
    public function getTerRates(){
        $result = RatesModel::getDataRates(json_decode($this->request_params->client_id));
        if($result->status == 200) {
            return response()->json(['data' => $result, 'status' => $result->status]);
            //return json_encode($result);
            //return response()->json(["status" => 200, "data" => $result->data->val_asrt_tgmiratingeval->str_consultas]);
        }
        else{
            return response()->json(['msg' => $result, 'status' => $result->status]);

            //return response()->json(["status" => 400, "data" => 'Error al realizar petición']);
        }

    }
}