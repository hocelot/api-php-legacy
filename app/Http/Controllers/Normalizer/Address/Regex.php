<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 3/07/17
 * Time: 12:05 PM
 */

namespace App\Http\Controllers\Normalizer\Address;


class Regex {

    public static $ab_street__exp = array(
        'DR|DOC|DTR|DOCT' => 'DOCTOR',
        'ST(A|O)?'        => 'SANT',//No podemos saber si es masculino o femenino
        'FCO'             => 'FRANCISCO',
        'VTE'             => 'VICENTE',
        'PTE'             => 'PRESIDENTE',
        'M(ARIA|(ª)| )'   => 'MARIA',//Generalment M es maria
        'NTRA'            => 'NUESTRA',
        'SRA'             => 'SEÑORA',
        'MTRA'            => 'MAESTRA',
        'GRAL'            => 'GENERAL',
        'P(A)?RJ'         => 'PARAJE',
        'SRTA'            => 'SEÑORITA',
        'GRP'             => 'GRUPO',
        'CTRA'            => 'CARRETERA',
        'HMNOS'           => 'HERMANOS',
        'URB'             => 'URBANIZACION',
        'RESD(I)?(D)?'    => "RESIDENCIAL",
        'PZ'              => 'PLAZA',
        'CZDA'            => 'CALZADA'
    );
    public static $ab_number__exp = array(
        '(SIN|S|NO|N)(\\\| |/)?(N(U)?(M)?(E)?(R)?(O)?)' => 'SN',
        'N(U)?(M)?(E)?(R)?(O)?'                         => 'NUM'
    );
    public static $ab_level__exp = array(
        'km'     => 'K(I)?(L)?(O)?M(E)?(T)?(R)?(O)?',
        'stairs' => 'ES(C)?(A)?(L)?(E)?(R)?(A)?',
        'block'  => '(B(L|Q)(O)?(Q|C)?(UE)?)|(PORT(A)?(L)?|PRTL)',
        'floor'  => 'P(L)?(ANTA|ISO)',
        'door'   => 'P(UER)?TA|BUZON|CASA',
        'edif'   => '(ED)(I)?(F)?(ICIO)?',
        'DR'     => '(D)(E)?(R)(E)?(ECH|EX)?(A|O)*',
        'IZ'     => '(IZ)(Q)?(UIERD|U|IRD)(A|O)*'
    );
    public static $numbers_str__exp = array(
        "1"  => "PRIMERO",
        "2"  => "SEGUNDO",
        "3"  => "TERCERO",
        '4'  => 'CUARTO',
        '5'  => 'QUINTO',
        '6'  => 'SEXTO',
        '7'  => 'SEPTIMO',
        '8'  => 'OCTAVO',
        '9'  => 'NOVENO',
        '10' => 'DECIMO'
    );
    public static $roadtypes__exp = array(
        'CL' => 'C(A)?(L)?(L)?(E)?|CARRER',
        'AV' => 'AV(INGU|ENI|A|(E)?N(I)|D(A)?)*',
        'PZ' => 'PL|PZ(A)?|PL(A)?(Z|Ç)(A)?',
        'PS' => 'P(A)?S(S)?(E)?(O|(E)?(I)?(G)?)?',
        'CR' => 'C(A)?(R)?(R)?(E)?T(E)?(R)?(A)?|CR',
        'TR' => '(TV)|(SIA)|(TR(A)?V(E)?(S)?((I)?(A)?|(S)?(E)?(R)?(A)?)?)',
        'UR' => '((UR)(B)?(ANI(T)?ZACIO(N)?)?)|(RESIDENCIA(L)?)',
        'GV' => 'GRAN( )?VIA',
        'VI' => 'VIA',
        'AC' => 'ACCESO',
        'AG' => 'AGREGADO',
        'AL' => 'ALDEA|ALAMEDA',
        'AN' => 'ANDADOR',
        'AR' => 'AREA|ARRABAL',
        'AY' => 'ARROYO|RIEROL',
        'AU' => 'AUTOPISTA|AUTOVIA|AT',
        'BJ' => 'BAJADA|BAIXADA',
        'BL' => 'BLOQUE',
        'BR' => 'BARRANC(O)?',
        'BQ' => 'BARRANQUIL',
        'BO' => 'BARRI(O)?|B(A)?(R)?(R)?(I)?(A)?(D)?(A)?',
        'BV' => 'BULEVAR',
        'CY' => 'CALEYA',
        'CJ' => 'CALLEJA|CALLEJON',
        'CZ' => 'CALLIZO',
        'CM' => 'C(A)?M(I)?(N)?(O)?|CARMEN|CNO',
        'CP' => 'CAMP(O|A)?',
        'CA' => 'CA(Ñ|NY)ADA',
        'CS' => 'CASERIO',
        'CH' => '(CH|X)ALET',
        'CI' => 'CINTURO(N)?',
        'CG' => 'CO(L|L( )?L)EGI(O)?|CIGARRAL',
        'CN' => 'COLONIA',
        'CO' => 'CONCEJO|COLEGIO',
        'CU' => 'CONJUNT(O)?',
        'CT' => 'CUESTA|COSTANILLA',
        'DE' => 'DETRAS|DARRERA',
        'DP' => 'DIPUTACION',
        'DS' => 'DISEMINADOS',
        'ED' => 'EDIFICI(O)?(S)?',
        'EN' => '(E)(NTRADA|NSANCHE|IXAMPLE)',
        'ES' => 'ESCALINATA|ESPALDA',
        'EX' => 'EXPLANADA',
        'EM' => 'EXTRAMUROS',
        'ER' => 'EXTRA(R)?RADI(O)?',
        'FC' => 'FERROCARRIL',
        'FN' => 'FINCA',
        'GL' => 'GLORIETA',
        'GR' => 'GR(U)?P(O)?',
        'HT' => 'HUERT(O|A)|HORT',
        'JR' => 'JARDI(N)?(E)?(S)?',
        'LD' => 'LADO|LADERA|VESSANT|COSTAT',
        'LA' => 'LAGO',
        'LG' => 'L(U)?G(A)?(R)?|LLOC',
        'MA' => 'MALECON',
        'MZ' => 'MANZANA|ILLA',
        'MS' => 'MASIA(S)?',
        'MC' => 'MERCA(DO|T)',
        'MT' => 'MONTE',
        'ML' => 'MUELLE|MOLL',
        'MN' => 'MUNICIPI(O)?',
        'PM' => 'PARAMO',
        'PQ' => 'PARROQUIA|P((A)?(R)?(QUE)|ARÇ)',
        'PI' => 'PARTICULAR',
        'PD' => 'P(A)?(R)?T(I)?DA',
        'PU' => 'PASADIZO|PASSADIS',
        'PJ' => 'P(A)?(S)?(A)?J(E)?|P(A)?S(A)?TG(E)?',
        'PC' => 'PLACETA',
        'PB' => 'POBLA(DO|T)',
        'PL' => 'POLIG(O)?(N)?(O)?',
        'PR' => 'PROLONGACIO(N)?|CONTINUACIO(N)?',
        'PT' => 'P(UE|O)NT(E)?',
        'QT' => 'QUINTA|CINQUENA',
        'RA' => 'RACONADA',
        'RM' => 'RAMAL',
        'RB' => 'R(A)?(M)?BLA',
        'RC' => 'R(INCON(A)?|ACO)',
        'RD' => 'R(O)?ND(A)?',
        'RP' => 'RAMPA',
        'RR' => 'RIERA',
        'RU' => 'RUA',
        'SA' => 'S(AL|ORT)IDA',
        'SN' => 'SALON',
        'SC' => 'SECTOR',
        'SD' => 'SENDA',
        'SL' => 'SOLAR',
        'SU' => '(SUBI|PUJA)DA',
        'TN' => 'TERREN(OS|Y)',
        'TO' => 'TORRENT(E)?',
        'VA' => 'VALLE',
        'VR' => 'VEREDA',
        'VD' => 'VIADUCTO',
        'VL' => 'VIAL'
    );

    public static function get__dates__exp()
    {
        return [
            '(([0-2])?[0-9]|3[0-1])( )?(' . self::$det_dates_exp . ')?( )?(' . implode("|", self::$months_31_exp) . ')( )?(' . self::$det_dates_exp . ')?( )?',
            '(([0-2])?[0-9]|30)( )?(' . self::$det_dates_exp . ')?( )?(' . implode("|", self::$months_30_exp) . ')( )?(' . self::$det_dates_exp . ')?( )?',
            '(([0-2])?[0-9])( )?(' . self::$det_dates_exp . ')?( )?(' . self::$months_29_exp . ')( )?(' . self::$det_dates_exp . ')?( )?'
        ];
    }

    private static $det_dates_exp = "D('|E|L)*";


    private static $months_29_exp = 'FEBRER(O)?';


    private static $months_30_exp = array(
        'ABRIL',
        'JUNIO|JUNY',
        'SE(P)?T(I)?EMBRE',
        'NOV(I)?EMBRE',
    );
    private static $months_31_exp = array(
        '(ENERO|GENER)',
        '(MAR(ZO|Ç))',
        '(MAYO|MAIG)',
        '(JULIO(L)?)',
        '(AGOST(O)?)',
        '(OCTUBRE)',
        '(DICIEMBRE|DESEMBRE)'
    );

    public static $ab_to_remove = array(
        'CHALET',
        'DUPLEX',
        'OV',
        'OTROS TIPOS VIA'
    );

    

}