<?php

/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 5/05/17
 * Time: 12:49
 */

namespace App\Http\Controllers\Normalizer\Address;


class UtilClass {

    public static function elimina_acentos($cadena)
    {
        $text = htmlentities($cadena, ENT_SUBSTITUTE, 'UTF-8');
        $text = strtolower($text);
        $patron = array(
            '/\+/'       => '',
            '/&agrave;/' => 'a',
            '/&egrave;/' => 'e',
            '/&igrave;/' => 'i',
            '/&ograve;/' => 'o',
            '/&ugrave;/' => 'u',
            '/&aacute;/' => 'a',
            '/&eacute;/' => 'e',
            '/&iacute;/' => 'i',
            '/&oacute;/' => 'o',
            '/&uacute;/' => 'u',
            '/&acirc;/'  => 'a',
            '/&ecirc;/'  => 'e',
            '/&icirc;/'  => 'i',
            '/&ocirc;/'  => 'o',
            '/&ucirc;/'  => 'u',
            '/&atilde;/' => 'a',
            '/&etilde;/' => 'e',
            '/&itilde;/' => 'i',
            '/&otilde;/' => 'o',
            '/&utilde;/' => 'u',
            '/&auml;/'   => 'a',
            '/&euml;/'   => 'e',
            '/&iuml;/'   => 'i',
            '/&ouml;/'   => 'o',
            '/&uuml;/'   => 'u',
            '/&auml;/'   => 'a',
            '/&euml;/'   => 'e',
            '/&iuml;/'   => 'i',
            '/&ouml;/'   => 'o',
            '/&uuml;/'   => 'u',
            '/&aring;/'  => 'a',
            '/&ntilde;/' => 'ñ',
            '/&Ccedil;/' => 'Ç',
            '/&ccedil;/' => 'ç',
            "/&#039;/"   => "'"
        );

        $text = preg_replace(array_keys($patron), array_values($patron), $text);

        return $text;
    }

    public static function del_articulos($str)
    {
        //Quitamos determinantes del nombre de la calle
        $articulos = array('DEL', 'DE',"D'", 'LA', 'EL', 'LO', 'LAS', 'LOS', "L'", "N'", "VAN", "DELLA");

        $str_aux = explode(" ", $str);

        $resultado = array_diff($str_aux, $articulos);

        $str = implode(" ", $resultado);

        return $str;
    }

    public static function cleanStr($str, $town = false)
    {
        $str = str_replace('º', ' ', $str);
        $str = str_replace('ª', ' ', $str);
        $_str = self::del_articulos($str);
        $_str = self::elimina_acentos($_str);
        $_str = mb_strtoupper($_str);
        $_str = str_replace('.', ' ', $_str);
        $_str = str_replace(',', ' ', $_str);
        $_str = str_replace('-', ' ', $_str);
        $_str = str_replace('$', ' ', $_str);
        $_str = str_replace('*', ' ', $_str);
        $_str = str_replace('º', ' ', $_str);
        //Me cargo todo lo que venga entre parentesis, odiamos los parentisis, haters de parentisis? yeah...
        $_str = preg_replace('@[(](.)*[)]@', ' ', $_str);
        //Remplazo todo lo que no sea nº o letras
        if ( ! $town)
            $_str = preg_replace('/[^A-Za-z0-9ÇçÑñ]/', ' ', $_str);
        else
            $_str = preg_replace('/[^A-Za-zÇçÑñ]/', ' ', $_str);
        $_str = preg_replace('/[\x00-\x1F\x7F]/u', '', $_str);

        return str_replace('  ', ' ', $_str);
    }
}