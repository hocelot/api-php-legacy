<?php

/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 24/07/17
 * Time: 1:51 PM
 */

namespace App\Http\Controllers\Normalizer;

class Names {

    public static function normalize($fullname)
    {
        $parser = file_get_contents(env("ZUUL_URL").'/bs-id-normalizer/id-normalizer/'.rawurlencode($fullname));
        $parser = json_decode($parser);
        if(isset($parser->gender)){
            if($parser->gender == 'male')
                $parser->gender ='M';
            else $parser->gender = 'F';
            return $parser;
        }else return false;
    }

}