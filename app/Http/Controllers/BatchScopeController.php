<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 27/10/16
 * Time: 21:48
 */

namespace App\Http\Controllers;

use App\Http\Controllers\MediaCheck\MediaCheckManager;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Http\Controllers\GeoCheck\GeoCheck;
use App\Http\Controllers\GeoFraud\GeoFraud;
use App\Http\Controllers\IdCheck\IDCheck;
use App\Http\Controllers\IdFraud\IdFraud;
use App\Http\Controllers\RiskScore\ScoreController;
use App\Http\Controllers\SocialMedia\SocialMediaManager;
use App\Http\Controllers\Worker\ThreadsManager;
use App\Http\Models\AuthModel;
use App\Jobs\WsdlJob;
use Log;
use stdClass;

class BatchScopeController extends BaseController
{
    /*
     * Clase que dependiendo de los permisos del cliente realizara los siguientes productos
     * **La normalización del formulario será un permiso independiente, tendrá su propia ruta
     *----PRODUCTO---PERMISO-----
     *  ID CHECK : ic_Tl3h7b3HsEeAe2b0DtW8zmpQ9EnBc9XjCsM7
     *  ID FRAUD : if_Fcz72MajCgEVDnf0s2ct8M52RffsQlI1tZM6
     *  GEO CHECK : gc_pzP9O2TW9eOiCXn4Mo0gVUroCpg1wA5EuWWQ
     *  GEO FRAUD : gf_fjw6HcqXaXcJHkRUNSe2xbMygiiCbwkBltJ2
     *  BUREAU(EQUIFAX) : br_W3C5b4A0giMfbZC8b6ayjIEvihhubYsf0TsJ
     *  RISK SCORE : sc_IpqAf3mZZVVnUcMW56PBDfndae94DTIQOPbY
     */

    /*
     * Params que envia el cliente:
        $params->user->name
        $params->user->first_name
        $params->user->last_name
        $params->user->cif_dni
        $params->user->phone***
        $params->user->gender***
        $params->user->email***
        $params->user->birthday***
        $params->user->doc_type*** Solo para Hocelot/Mirating Registro
        $params->user->ter_type*** Solo para Hocelot/Mirating Registro
$params->user->from**** W-WEB,B-BATCH, A-API(si,no hay api), D-DEMO(solo llamadas ajax)

        $params->user->client_id*** Solo para consultas via web

        $params->address->road_type***
        $params->address->street***
        $params->address->number***
        $params->address->floor***
        $params->address->door***
        $params->address->stair***
        $params->address->km***
        $params->address->block***
        $params->address->letter***
        $params->address->province***
        $params->address->town***
        $params->address->zip_code***
        $params->address->type***
        ***Opcional****
     */
    private $request_params, $client;
    private $ajax = false;
    private $scope = array(
        'id_check' => 'ic_Tl3h7b3HsEeAe2b0DtW8zmpQ9EnBc9XjCsM7',
        'id_fraud' => 'if_Fcz72MajCgEVDnf0s2ct8M52RffsQlI1tZM6',
        'geo_check' => 'gc_pzP9O2TW9eOiCXn4Mo0gVUroCpg1wA5EuWWQ',
        'geo_fraud' => 'gf_fjw6HcqXaXcJHkRUNSe2xbMygiiCbwkBltJ2',
        //       'bureau' => 'br_W3C5b4A0giMfbZC8b6ayjIEvihhubYsf0TsJ ',
        'risk_score' => 'sc_IpqAf3mZZVVnUcMW56PBDfndae94DTIQOPbY',
        'media_check' => 'mc_neoUmzccfNTAm1vZOaYvnr2XEMFAu7MSemCW'
    );


    //TODO middleware que controle si falta algún parámetro

    public function initialize()
    {
        $start = microtime(true);
        Log::debug('Empieza : ' . $start);
        //Inicializamos variables a empty
        $client_scope = $result = [];

        //Array con los permisos del cliente
        // Via Batch o API
        $client_scope = app('db')->connection('oauth')->table('oauth_client_scopes')->where('client_id', $this->client['oauth_id'])->lists('scope_id');
//            if (in_array('mr_EhDnRifP3qZq75VUW5BsSnWh1sMk8Dkb0hKGh', $client_scope)) {
//                    foreach ($this->scope as $scopes)
//                        $client_scope[] = $scopes;
//            }

        //Si no tiene permisos
        if (empty($client_scope)) {
            return response()->json(['msg' => 'No se le ha asignado ningún permiso', 'status' => 400]);
        }


        // Si no esta el campo es que viene via api y el client_id es el ter_codigo del cliente
//        Log::debug(print_r($this->request_params, true));
        if (!isset($this->request_params->user->client_id))
            $this->request_params->user->client_id = $this->client['id'];


        //Status 200 respuesta general
        $result['status'] = 200;

        if (!empty($this->request_params->address->door)) $this->request_params->address->type = 'flat';
        else $this->request_params->address->type = 'house';
        $this->request_params->address->level = '1';


//        //Creamos Cliente score
        $resp = AuthModel::wsdljoin($this->request_params);
        if (isset($resp->val_astr_tgtercero->str_tgtercero[0]->ter_codigo) && !empty($resp->val_astr_tgtercero->str_tgtercero[0]->ter_codigo)) {
            $ter_codigo_evaluado = $resp->val_astr_tgtercero->str_tgtercero[0]->ter_codigo;
            unset($resp);
        } else {
            return response()->json(['msg' => 'No se ha podido realizar la consulta', 'status' => 400]);
        }


        /*Comunes*/
        $params_comunes = new stdClass();
        $params_comunes->fecha = date('c');//Carbon::now()->toDateTimeString();
        $params_comunes->ip = $this->client['ip'];
        $params_comunes->ter_codigo = $ter_codigo_evaluado;
        $params_comunes->origen = $this->request_params->user->from;
        $params_comunes->cliente = $this->request_params->user->client_id;
        $params_comunes->usuario = '-';
        $params_comunes->tramo_precio = 0;


//        /*START ID CHECK*/
        if (in_array($this->scope['id_check'], $client_scope)) {
            $result['id_check'] = IDCheck::check_id($this->request_params->user->name, $this->request_params->user->first_name, $this->request_params->user->last_name, $this->request_params->user->cif_dni);
            //Guardamos resultados


            //FALSEO ID CHECK
            $params_comunes->resultado = $result['id_check']['hits']['score_id_check'];//$aeat_name;
            $params_comunes->id_check = $result['id_check']['hits']['correlacion_id_check'];
            $params_comunes->nombre = $result['id_check']['hits']['aeat_name'];
            $params_comunes->dni_encontrado = $result['id_check']['dni_encontrado'];
            if ($params_comunes->dni_encontrado != $this->request_params->user->cif_dni) {
                $result['id_check']['hits']['alert'] = $params_comunes->alerta = true;
            } else {
                $result['id_check']['hits']['alert'] = $params_comunes->alerta = false;
            }
            $params_comunes->status = $result['id_check']['status'];
//            unset($result['id_check']['hits']['aeat_name']);
            unset($result['id_check']['dni_encontrado']);
            $val_astr_mr_id_check['str_mr_id_check'] = $params_comunes;
            $this->dispatch((new WsdlJob(['val_astr_mr_id_check' => $val_astr_mr_id_check], 'mr_id_check', 'clientes')));
            unset($val_astr_mr_id_check);
            unset($params_comunes->resultado);
            unset($params_comunes->id_check);
            unset($params_comunes->dni_encontrado);
            unset($params_comunes->nombre);
            unset($params_comunes->status);
            unset($params_comunes->alerta);

            //CARREFOUR
            if ($this->request_params->user->client_id == '6' || $this->request_params->user->client_id == '52301' || $this->request_params->user->client_id == '186343' || $this->request_params->user->client_id == '186342') {
                if ($result['id_check']['hits']['correlacion_id_check'] > 0 && $result['id_check']['hits']['correlacion_id_check'] <= 89.0) {
                    $result['id_check']['hits']['score_id_check'] = 'OK-R';
                    $result['id_check']['status'] = 200;
                }

            }
            // No seguimos por que el id_check ha dado ko
            $result['id_check']['hits']['correlacion_id_check'] = number_format(round($result['id_check']['hits']['correlacion_id_check'], 2), 4, ',', '.') / 100;

            if ($result['id_check']['status'] == 400)
                return response()->json($result);
        }

        /******************* WORKER PARA ID FRAUD - GEO CHECK *****************************/
        $worker = new ThreadsManager();

        /*START ID FRAUD*/
//        if (in_array($this->scope['id_fraud'], $client_scope)) {
//            $id_fraudobj = new IdFraud($this->request_params->user->email, $this->request_params->user->phone, $this->request_params->user->name, $this->request_params->user->first_name, $this->request_params->user->last_name);
//            $worker->stack($id_fraudobj);
//
//        }
        /*START GEO CHECK*/
        if (in_array($this->scope['geo_check'], $client_scope)) {
            $obj_geo_check = new GeoCheck($this->request_params->address, $this->ajax);
            $worker->stack($obj_geo_check);
        }
        // Start all jobs
        $worker->start();
        // Join all jobs and close worker
        $worker->shutdown();
        foreach ($worker->data as $key => $value) {
//            if ($key == 'fb') {
//                $facebook = $value;
//            } else {
            $result = array_merge($result, [$key => $value]);
//            }
        }
        //Destruimos el worker y los objetos
//        unset($id_fraudobj);
//        unset($obj_geo_check);
//        unset($worker);
//        if (isset($result['id_fraud'])) {
//            $params_comunes->id_fraud = $result['id_fraud']['hits']['score'];
//            $params_comunes->email = 'HOLA';
//            $params_comunes->fb_email1 = $result['id_fraud']['fb_email']['email'];
//            $params_comunes->fb_telefono1 = $result['id_fraud']['fb_email']['phone'];
//            $params_comunes->fb_nombre1 = $result['id_fraud']['fb_email']['name'];
//            $params_comunes->fb_url1 = $result['id_fraud']['fb_email']['img'];
//            $params_comunes->fb_email2 = $result['id_fraud']['fb_phone']['email'];
//            $params_comunes->fb_telefono2 = $result['id_fraud']['fb_phone']['phone'];
//            $params_comunes->fb_nombre2 = $result['id_fraud']['fb_phone']['name'];
//            $params_comunes->fb_url2 = $result['id_fraud']['fb_phone']['img'];
//            if (isset($result['id_fraud']['hits']['phone_info']->currentOperator))
//                $params_comunes->pc_currentoperator = $result['id_fraud']['hits']['phone_info']->currentOperator;
//            if (isset($result['id_fraud']['hits']['phone_info']->lastPortability->from))
//                $params_comunes->pc_lastportability_from = $result['id_fraud']['hits']['phone_info']->lastPortability->from;
//            if (isset($result['id_fraud']['hits']['phone_info']->lastPortability->to))
//                $params_comunes->pc_lastportability_to = $result['id_fraud']['hits']['phone_info']->lastPortability->to;
//            if (isset($result['id_fraud']['hits']['phone_info']->lastPortability->when))
//                $params_comunes->pc_lastportability_when = $result['id_fraud']['hits']['phone_info']->lastPortability->when;
//            if (isset($result['id_fraud']['hits']['phone_info']->originalOperator))
//                $params_comunes->pc_originaloperator = $result['id_fraud']['hits']['phone_info']->originalOperator;
//            if (isset($result['id_fraud']['hits']['phone_info']->type))
//                $params_comunes->pc_type = $result['id_fraud']['hits']['phone_info']->type;
//            if (isset($result['id_fraud']['hits']['phone_info']->typeDescription))
//                $params_comunes->pc_typedescription = $result['id_fraud']['hits']['phone_info']->typeDescription;
//
//            $this->dispatchIdFraud($params_comunes);
//            if (isset($params_comunes->pc_lastportability_from))
//                unset($params_comunes->pc_lastportability_from);
//            if (isset($params_comunes->pc_currentoperator))
//                unset($params_comunes->pc_currentoperator);
//            if (isset($params_comunes->pc_lastportability_to))
//                unset($params_comunes->pc_lastportability_to);
//            if (isset($params_comunes->pc_lastportability_when))
//                unset($params_comunes->pc_lastportability_when);
//            if (isset($params_comunes->pc_originaloperator))
//                unset($params_comunes->pc_originaloperator);
//            if (isset($params_comunes->pc_type))
//                unset($params_comunes->pc_type);
//            if (isset($params_comunes->pc_typedescription))
//                unset($params_comunes->pc_typedescription);
//            unset($params_comunes->email);
//            unset($params_comunes->id_fraud);
//            unset($params_comunes->fb_email1);
//            unset($params_comunes->fb_telefono1);
//            unset($params_comunes->fb_nombre1);
//            unset($params_comunes->fb_url1);
//            unset($params_comunes->fb_email2);
//            unset($params_comunes->fb_telefono2);
//            unset($params_comunes->fb_nombre2);
//            unset($params_comunes->fb_url2);
//            unset($result['id_fraud']['fb_email']);
//            unset($result['id_fraud']['fb_phone']);
//        }
        if (isset($result['geo_check'])) {
            //Creamos Job de idFraud y GeoCheck si falla este último
            if ($result['geo_check']['hits']['level'] > 4) {
                $params_comunes->resultado = 'KO';

            } else {
                $params_comunes->resultado = 'OK';
            }//OK,KO
            $params_comunes->geo_check = $result['geo_check']['hits']['level'];
            $params_comunes->direccion = $result['geo_check']['hits']['address'];
            $params_comunes->metro = $result['geo_check']['hits']['m2'];
            $params_comunes->latitud = $result['geo_check']['hits']['latitude'];
            $params_comunes->longitud = $result['geo_check']['hits']['longitude'];
            $params_comunes->uso = $result['geo_check']['hits']['used_for'];
            $params_comunes->nivel = $result['geo_check']['hits']['level'];
            $params_comunes->tipo = $result['geo_check']['hits']['type'];
            $ine = $result['geo_check']['hits']['ine'];
            //unset($result['geo_check']['hits']['ine']);
            $this->dispatchGeoCheck($params_comunes);
            unset($params_comunes->resultado);
            unset($params_comunes->geo_check);
            unset($params_comunes->direccion);
            unset($params_comunes->metro);
            unset($params_comunes->latitud);
            unset($params_comunes->longitud);
            unset($params_comunes->uso);
            unset($params_comunes->nivel);
            unset($params_comunes->tipo);
            // No seguimos por que el id_check ha dado ko
            if ($result['geo_check']['status'] == 400)
                return response()->json($result);
//            return response()->json($result);
        }

        /*********************** FIN DEL PRIMER WORKER ***************1***********************/


        /*********************** EMPIEZA MEDIA CHECK ***************1***********************/
        if (in_array($this->scope['media_check'], $client_scope)) {
            $obj_media_check = new MediaCheckManager($this->request_params->user->phone, $this->request_params->address->street, $this->request_params->user->name, $this->request_params->user->first_name, $this->request_params->user->last_name);
            $result['media_check'] = $obj_media_check->start_media_check();
        }
//        Log::debug('BATCH  '.print_r($result['media_check'],true));
        if (isset($result['media_check']) && $result['media_check']['status'] == 400) {
            return response()->json($result);
        }


        /*********************** FIN MEDIA CHECK ***************1***********************/

//        /************************ WORKER PARA GeoFraud, Risk Score y Social *****************/
        $worker = new ThreadsManager();


        /*START GEO FRAUD*/
        if (in_array($this->scope['geo_fraud'], $client_scope)) {
            //TODO GeoFraud Controller para añadir fuentes
            $obj_geo_fraud = new GeoFraud($this->request_params->user->name, $this->request_params->user->first_name, $this->request_params->user->last_name, $this->request_params->address);
            $worker->stack($obj_geo_fraud);
        }

//
//        /*START RISK SCORE*/
        if (in_array($this->scope['risk_score'], $client_scope)) {
//            Log::debug('Entro en risk');
            $lat = $long = '';
            $m2 = 0;
            if (isset($result['geo_check'])) {
                $lat = $result['geo_check']['hits']['latitude'];
                $long = $result['geo_check']['hits']['longitude'];
                $m2 = $result['geo_check']['hits']['m2'];
                $this->request_params->address->type = $result['geo_check']['hits']['type'];
            }
            if (!isset($ine)) $ine = '';
            $risk_obj = new ScoreController($ter_codigo_evaluado, $this->request_params, $lat, $long, $m2, $params_comunes->fecha, $params_comunes->usuario, $params_comunes->ip, $params_comunes->origen, $ine);
            $worker->stack($risk_obj);
        }

        // Start all jobs
        $worker->start();
        // Join all jobs and close worker
        $worker->shutdown();
        foreach ($worker->data as $key => $value) {
            $result = array_merge($result, [$key => $value]);
        }

        if (isset($result['risk_score']['hits']->metros_cuadrados) && isset($result['geo_check']['hits']['m2'])) {
            $result['geo_check']['hits']['m2'] = $result['risk_score']['hits']->metros_cuadrados;
            unset($result['risk_score']['hits']->metros_cuadrados);
        }

        if (isset($result['geo_fraud'])) {
            //NC - no encontrado en paginas blancas
            $params_comunes->resultado = "NC";
            $params_comunes->geo_fraud = "NC";
            $params_comunes->correlacion = 0.0;
            $params_comunes->nombre = "";
            $params_comunes->telefono = "";
            $params_comunes->direccion = "";
            $params_comunes->status = "400";
            if (isset($result['geo_fraud']['hits']->correlacion)) {
                $params_comunes->resultado = "OK";
                $params_comunes->geo_fraud = 1;
                $params_comunes->correlacion = $result['geo_fraud']['hits']->correlacion;
                $params_comunes->nombre = $result['geo_fraud']['hits']->name;
                $params_comunes->telefono = $result['geo_fraud']['hits']->phone;
                $params_comunes->direccion = $result['geo_fraud']['hits']->address;
                $params_comunes->status = "200";
            }
            $this->dispatchGeoFraud($params_comunes);
            unset($params_comunes->resultado);
            unset($params_comunes->geo_fraud);
            unset($params_comunes->correlacion);
            unset($params_comunes->nombre);
            unset($params_comunes->telefono);
            unset($params_comunes->direccion);
            unset($params_comunes->status);
        }
//        //Destruimos el worker y los objetos
//        unset($risk_obj);
//        unset($obj_geo_fraud);
//        unset($worker);


        $end = microtime(true) - $start;
        Log::debug('Termina : ' . $end);

        return response()->json($result);

    }

    private function dispatchIdFraud($params)
    {
//        Log::debug('ID FRAUD ' . print_r($params, true));
        $this->dispatch((new WsdlJob(['val_astr_mr_id_fraud' => ['str_mr_id_fraud' => $params]], 'mr_id_fraud', 'clientes'))->delay(60));
    }

    private function dispatchGeoCheck($params)
    {
        $this->dispatch((new WsdlJob(['val_astr_mr_geo_check' => ['str_mr_geo_check' => $params]], 'mr_geo_check', 'clientes'))->delay(60));
    }

    private function dispatchGeoFraud($params)
    {
//        Log::debug('GEO FRAUD ' . print_r($params, true));
//
        $this->dispatch((new WsdlJob(['val_astr_mr_geo_fraud' => ['str_mr_geo_fraud' => $params]], 'mr_geo_fraud', 'clientes')));//->delay(60)
    }

    public function setParams(array $ip_id_oauth, \stdClass $user_data)
    {
        $this->client = $ip_id_oauth;
        $this->request_params = $user_data;
    }
}