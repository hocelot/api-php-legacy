<?php
/**
 * Created by PhpStorm.
 * User: daniel
 * Date: 17/03/17
 * Time: 12:27
 */

namespace app\Http\Controllers\MediaCheck;

use App\Http\Controllers\Proxies\Proxies;
use App\Http\Models\UtilClass;
use stdClass;
use Symfony\Component\DomCrawler\Crawler;
use Log;

class Idealista extends \Threaded
{
    private $tlf, $nombre_via;

    public function __construct($mobile, $nombre_via)
    {
        $this->tlf = trim($mobile);
        $this->nombre_via = UtilClass::del_articulos(mb_strtoupper(UtilClass::elimina_acentos($nombre_via)));
    }

    public function run()
    {
        require_once(__DIR__ . '/../../../../vendor/autoload.php');
        require_once(__DIR__ . '/../../../../bootstrap/app.php');
        $this->worker->addData('idealista', $this->search());
    }


    public function search()
    {
        $html_web = 'https://www.idealista.com/telefono-inmueble/' . $this->tlf . "/";
        $idealista = new stdClass();
        $idealista->score_vivienda = 'OK';
        $idealista->hits = [];
        $crawler = new Crawler();
        //Vemos si es un profesional, particular con un anuncio o varios anuncios
        if ($resp = $this->curl($html_web)) {
            $crawler->addHtmlContent($resp['html']);
            //Profesional
            if ($crawler->filter('div.alertBlack')->count() > 0) {
                $buisness_idealista = $this->professional_user($crawler);
                $idealista->hits = $buisness_idealista->hits;
                if(isset($buisness_idealista->url)){
                    $idealista->buisness['url'] =$buisness_idealista->url;
                }
                if(isset($buisness_idealista->phone)){
                    $idealista->buisness['phone'] =$buisness_idealista->phone;
                }
                if(isset($buisness_idealista->dir)){
                    $idealista->buisness['dir'] =$buisness_idealista->dir;
                }

            }
            //Particular
            if (preg_match('/https:\/\/www.idealista.com\/inmueble\/[0-9A-Za-z]*\//', $resp['url'])) {
                //No he encontrado un usuario con varios anuncios
                $idealista->hits[] = $this->getAnnouncement($crawler, $resp['url']);
            }


        }

        $key_words_types_house = ['PISO', 'ATICO', 'CHALET', 'CASA', 'DUPLEX'];
        $vivienda = false;
        if (!empty($idealista->hits)) {
            foreach ($idealista->hits as $hit) {
                if ((isset($hit->ubicacion) && stristr(UtilClass::del_puntoycoma(UtilClass::del_articulos(UtilClass::elimina_acentos($hit->ubicacion))),$this->nombre_via)) ||
                    (isset($hit->title) && stristr(UtilClass::del_puntoycoma(UtilClass::del_articulos(UtilClass::elimina_acentos($hit->title))),$this->nombre_via))) {
                    foreach ($key_words_types_house as $keyword) {
                        if (isset($hit->title) && stristr(UtilClass::del_puntoycoma(UtilClass::del_articulos(UtilClass::elimina_acentos($hit->title))),$keyword)) {
                            $vivienda = true;
                            break;
                        }
                    }
                }

                if ($vivienda) {
                    $idealista->score_vivienda = 'KO';
                    break;
                }
            }

        }
        return $idealista;
    }

    private function professional_user(Crawler $crawler)
    {
        $buisness_info = new stdClass();
        $buisness_info->dir = '';
        $buisness_info->hits = [];
        if ($crawler->filter('div.infoText')->count() > 0) {
            if ($crawler->filter('div.infoText > p')->count() > 0) {
                //El ultimo url y el primero Tlf
                $max_p = $crawler->filter('div.infoText > p')->count();
                $actual_p = 0;
                do {
                    if ($actual_p == 0 && $crawler->filter('div.infoText > p')->eq($actual_p)->count() > 0) {
                        $buisness_info->phone = trim($crawler->filter('div.infoText > p')->eq($actual_p)->text());
                        $buisness_info->phone = trim(str_replace('Tlf.:', '', $buisness_info->phone));
                    } else
                        if ($actual_p == $max_p - 1) {
                            $buisness_info->url = 'https://www.idealista.com' . trim($crawler->filter('div.infoText > p')->eq($actual_p)->filter('a')->attr('href'));
                        } else {
                            $buisness_info->dir .= ' ' . $crawler->filter('div.infoText > p')->eq($actual_p)->text();
                        }
                    $actual_p++;
                } while ($actual_p < $max_p);

                if (isset($buisness_info->dir))
                    $buisness_info->dir = trim($buisness_info->dir);

            }
        }
        if (isset($buisness_info->url)) {
//            /venta-viviendas/pagina-2.htm
            $page = 1;
            do {
                $url = $buisness_info->url . 'venta-viviendas/pagina-' . $page . '.htm';
                $crawler->clear();
                if ($resp = $this->curl($url)) {
                    $crawler->addHtmlContent($resp['html']);
                    if ($crawler->filter('div.items-container > article')->count() > 0) {
                        $aux_crawler = new Crawler();
                        $crawler->filter('div.items-container > article')->each(function ($node) use ($buisness_info, $aux_crawler) {
                            $aux_crawler->clear();
                            if ($node->filter('div.item-info-container > a')->count() > 0) {
                                $url2 = 'https://www.idealista.com' . trim($node->filter('div.item-info-container > a')->attr('href'));
                                if ($resp2 = $this->curl($url2)) {
                                    $aux_crawler->addHtmlContent($resp2['html']);
                                    $buisness_info->hits[] = $this->getAnnouncement($aux_crawler, $url2);
                                }
//                                var_dump($node->filter('div.item-info-container > a')->attr('href'));

                            }
                        });
                    }


                }
                $page++;
            } while ($crawler->filter('#zero-results-container')->count() < 0);
        }
        return $buisness_info;
//        return false;
    }

    private function getAnnouncement(Crawler $crawler, $url)
    {

        $anuncio = new stdClass();
        $anuncio->url = $url;
        //Seccion ubicacion
        if ($crawler->filter('#addressPromo')->count() > 0) {
            $anuncio->ubicacion = trim($crawler->filter('#addressPromo > ul')->text());
        }
        //Seccion titulo, m2 y precio
        if ($crawler->filter('section.main-info')->count() > 0) {
            if ($crawler->filter('section.main-info > h1')->count() > 0) {
                $anuncio->title = trim($crawler->filter('section.main-info > h1')->text());
            }
            if ($crawler->filter('section.main-info > div.info-data > span')->first()->filter('span')->first()->count() > 0) {
                $anuncio->precio = str_replace('.', '', trim($crawler->filter('section.main-info > div.info-data > span')->first()->filter('span > span')->first()->text()));
            }

            if ($crawler->filter('section.main-info > div.info-data > span')->eq(1)->filter('span')->first()->count() > 0) {
                $anuncio->m2 = trim($crawler->filter('section.main-info > div.info-data > span')->eq(1)->filter('span > span')->first()->text());
            }
        }
        return $anuncio;

    }

    private function curl($page)
    {
        $obj_proxy = new Proxies('idealista');
        $ip_real = false;
        do {
            $ip = $obj_proxy->get();
            $obj_proxy->setUsed($ip);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $page);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            if ($ip) {
                curl_setopt($ch, CURLOPT_PROXY, $ip);
                curl_setopt($ch, CURLOPT_PROXYUSERPWD, "mrhc:QwEr1234");
            } else  $ip_real = true;
            $result = curl_exec($ch);
            $info = curl_getinfo($ch);
            curl_close($ch);
            if (!$ip_real) {
                $obj_proxy->unlock($info["http_code"]);
            }
        } while ($info["http_code"] == 400 && !$ip_real);
        if ($info["http_code"] == 400 && $ip_real) return false;
        return ['url' => $info['url'], 'html' => $result];
    }
}