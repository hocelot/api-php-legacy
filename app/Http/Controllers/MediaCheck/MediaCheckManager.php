<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 26/11/16
 * Time: 17:25
 */

namespace App\Http\Controllers\MediaCheck;


use App\Http\Controllers\Worker\ParallelCurl;
use App\Http\Controllers\Worker\ThreadsManager;
use App\Http\Models\UtilClass;
use Log;

class MediaCheckManager
{
    private $nv, $phone, $name, $first_name, $last_name;

    public function __construct($phone, $nv, $name, $first_name, $last_name)
    {

        $this->name = $name;
        $this->first_name = $first_name;
        $this->last_name = $last_name;
//        $this->email = $email;
        $this->phone = trim($phone);
        $this->nv = mb_strtoupper(UtilClass::elimina_acentos($nv));
    }
//    public function run()
//    {
//        require_once(__DIR__ . '/../../../../vendor/autoload.php');
//        require_once(__DIR__ . '/../../../../bootstrap/app.php');
//        $this->worker->addData('media_check', $this->media_check());
//    }
    public function start_media_check($demo = false)
    {
        Log::debug('ENTRO MEDIACHECK ');

        //Aqui lanzaria los hilos
        $result = new \stdClass();
        $result->score = 'OK';
        $result->status = 200;
        $result->hits['milanuncios'] = '';
        $result->hits['vibbo'] = '';
        $result->hits['idealista'] = '';
        $result->hits['search'] = '';

        $worker = new ThreadsManager();
        if (!empty($this->phone)) {
            $obj_milanuncios = new MilAnuncios($this->phone, $this->nv);
            $worker->stack($obj_milanuncios);
//            $result_prov = $obj_milanuncios->search();
            $obj_vibbo = new Vibbo($this->phone, $this->nv);
            $worker->stack($obj_vibbo);

            $obj_idealista = new Idealista($this->phone, $this->nv);
            $worker->stack($obj_idealista);
        }
        $obj_etools = new eTools($this->name, $this->first_name, $this->last_name);
        $worker->stack($obj_etools);
        // Start all jobs
        $worker->start();
        // Join all jobs and close worker
        $worker->shutdown();
        $result_prov = [];
        foreach ($worker->data as $key => $value) {
            $result_prov = array_merge($result_prov, [$key => $value]);
        }
//
        $result->hits['milanuncios'] = 'Sin datos';
        $result->hits['vibbo'] = 'Sin datos';
        $result->hits['idealista'] = 'Sin datos';
        $result->hits['search'] = 'Sin datos';
        Log::debug('ALEX MEDIACHECK ' . print_r($result_prov, true));
        if (isset($result_prov['search']) && !empty($result_prov['search']->hits)) {
            $result->hits['search'] = $result_prov['search']->hits;
        }

        if ((isset($result_prov['idealista']) && ($result_prov['idealista']->score_vivienda == 'KO')) || (isset($result_prov['vibbo']) && ($result_prov['vibbo']->score_vivienda == 'KO')) ||
            (isset($result_prov['milanuncios']) && ($result_prov['milanuncios']->score_vivienda == 'KO')) || (isset($result_prov['milanuncios']) && ($result_prov['milanuncios']->score_finanziacion == 'KO'))
            || (isset($result_prov['milanuncios']) && ($result_prov['milanuncios']->score_trabajo == 'KO')) || (isset($result_prov['vibbo']) && ($result_prov['vibbo']->score_trabajo == 'KO'))
        ) {
            $result->score = 'KO';
            $result->status = 400;
            if (isset($result_prov['milanuncios']->hits)) {
                if ($demo)
                    $result->hits['milanuncios'] = $result_prov['milanuncios'];
                else
                    $result->hits['milanuncios'] = $result_prov['milanuncios']->hits;
            }
            if (isset($result_prov['vibbo']->hits)) {
                if ($demo)
                    $result->hits['vibbo'] = $result_prov['vibbo'];
                else
                    $result->hits['vibbo'] = $result_prov['vibbo']->hits;
            }
            if (isset($result_prov['idealista']->hits)) {
                if ($demo)
                    $result->hits['idealista'] = $result_prov['idealista'];
                else
                    $result->hits['idealista'] = $result_prov['idealista']->hits;
            }
        }


        return ['score' => $result->score, 'hits' => $result->hits, 'status' => $result->status];

//            $result->hits = array_merge($result->hits,$result_prov['idealista']->hits);
//            $result->hits = array_merge($result->hits,$result_prov['vibbo']->hits);
//            $result->hits = array_merge($result->hits,$result_prov['milanuncios']->hits);


        //Log::debug('SALGO MEDIACHECK ');


    }

}