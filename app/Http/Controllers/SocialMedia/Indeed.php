<?php
/**
 * Created by PhpStorm.
 * User: DC
 * Date: 15/02/2017
 * Time: 16:02
 */

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use App\Http\Controllers\Proxies\Proxies;
use Goutte\Client;

class Indeed
{
    public function __construct($name, $first_name, $last_name, $city)
    {
        $this->name = $name;
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->city = $city;
    }
    private $cont = 0;
    public function testindeedCV()
    {
        $client = $this->loginIndeed();
        $name = $this->name;
        $ape1 = $this->first_name;
        $ape2 = $this->last_name;
        $place = $this->city;
        $name = $this->quitarTildes($name);
        $ape1 = $this->quitarTildes($ape1);
        $ape2 = $this->quitarTildes($ape2);
        $place = $this->quitarTildes($place);
        $coincidencia = false;
        $html_web = "http://www.indeed.com/resumes?q=" . $name . "+" . $ape1 . "&l=" . $place . "";
        $html_web_search = $client->request('GET', $html_web);
        $cont_nomb = $html_web_search->filter('#results > li > div > .sre-content > .app_name > a')->count();
        $url_nomb = array();
        $indeed = array();
        $userExact = array();
        if ($cont_nomb >= 1) {
            for ($a = 0; $a < $cont_nomb; $a++) {
                $nombre_url = $html_web_search->filter('#results > li > div > .sre-content > .app_name > a')->eq($a)->text();
                $nombre_url = $this->quitarTildes($nombre_url);
                $nombre_completo = $name . ' ' . $ape1 . ' ' . $ape2;
                $nombre_introducido = $name . ' ' . $ape1;
                $url_coincidente[$a] = 'NoExacto';
                if (stristr($nombre_url, $nombre_introducido) !== FALSE) {
                    $coincidencia = true;
                    $url_coincidente[$a] = $html_web_search->filter('#results > li > div > .sre-content > .app_name > a')->eq($a)->attr('href');
                    if ($html_web_search->filter('#results > li > div > .sre-content > .app_name > .location')->count() > 0) {
                        $place_url = $html_web_search->filter('#results > li > div > .sre-content > .app_name > .location')->eq($a)->text();
                        if (stristr($nombre_url, $ape2) !== FALSE) {
                            $place_url = $this->quitarTildes($place_url);
                            $cont_url = strpos($place_url, ',');
                            $con_prov = strpos($place_url, ',', $cont_url);
                            $sub_url = substr($place_url, $cont_url + 2, $con_prov - 2);
                            if ($sub_url == $place) {
                                unset($url_coincidente);
                                $userExact[] = $html_web_search->filter('#results > li > div > .sre-content > .app_name > a')->eq($a)->attr('href');
                            } else {
                            }
                        } else {
                        }
                    } else {
                        $place_url = 'No es posible comparar';
                    }
                } else {
                    $url_nomb[$a] = $html_web_search->filter('#results > li > div > .sre-content > .app_name > a')->eq($a)->attr('href');
                }
            }
            if ($coincidencia) {
                if(count($userExact) > 1) {
                    $indeed['TotalUsers'] = count($userExact);
                }
                for ($i = 0; $i < count($userExact); $i++) {
                    unset($client);
                    $client = new Client();
                    $html_search_people = $client->request('GET', 'http://www.indeed.com' . $userExact[$i]);
                    $countAct = $html_search_people->filter('#resume_actions_contacted > p')->count();
                    if ($countAct != 0) {
                        $indeed[$i]['Actualizado'] = $html_search_people->filter('#resume_actions_contacted > p')->text();
                    } else {
                        $indeed[$i]['Actualizado'] = 'No definido';
                    }
                    $cont_ExpLaboral = $html_search_people->filter('#work-experience-items > div')->count();
                    for ($j = 0; $j < $cont_ExpLaboral; $j++) {
                        if ($html_search_people->filter('#work-experience-items > div > .data_display > .work_company')->eq($j)->count() > 0) {
                            $indeed[$i]['ExpLaboral'][$j]['NomEmpresa'] = $html_search_people->filter('#work-experience-items > div > .data_display > .work_company')->eq($j)->text();
                        } else {
                            $indeed[$i]['ExpLaboral'][$j]['NomEmpresa'] = 'No definido';
                        }
                        if ($html_search_people->filter('#work-experience-items > div > .data_display > .work_title')->eq($j)->count() > 0) {
                            $indeed[$i]['ExpLaboral'][$j]['Puesto'] = $html_search_people->filter('#work-experience-items > div > .data_display > .work_title')->eq($j)->text();
                        } else {
                            $indeed[$i]['ExpLaboral'][$j]['Puesto'] = 'No definido';
                        }
                        if ($html_search_people->filter('#work-experience-items > div > .data_display > .work_dates')->eq($j)->count() > 0) {
                            $date_lab = $html_search_people->filter('#work-experience-items > div > .data_display > .work_dates')->eq($j)->text();
                            $prueba = preg_split("/[\s,]+/", $date_lab);
                            if (count($prueba) == 1) {
                                $indeed[$i]['ExpLaboral'][$j]['FechDesde'] = $prueba[0];
                                $indeed[$i]['ExpLaboral'][$j]['FechHasta'] = $prueba[0];
                            } else if (count($prueba) == 2) {
                                $indeed[$i]['ExpLaboral'][$j]['FechDesde'] = $prueba[0];
                                $indeed[$i]['ExpLaboral'][$j]['FechHasta'] = $prueba[1];
                            } else if (count($prueba) == 3) {
                                $indeed[$i]['ExpLaboral'][$j]['FechDesde'] = $prueba[0];
                                $indeed[$i]['ExpLaboral'][$j]['FechHasta'] = $prueba[2];
                            } else if (count($prueba) == 4) {
                                $indeed[$i]['ExpLaboral'][$j]['FechDesde'] = $prueba[0];
                                $indeed[$i]['ExpLaboral'][$j]['FechHasta'] = $prueba[2] . ' ' . $prueba[3];
                            } else {
                                $indeed[$i]['ExpLaboral'][$j]['FechDesde'] = $prueba[0] . ' ' . $prueba[1];
                                $indeed[$i]['ExpLaboral'][$j]['FechHasta'] = $prueba[3] . ' ' . $prueba[4];
                            }
                            $countLaboral = $html_search_people->filter('#work-experience-items > div > .data_display > .work_company > .inline-block')->eq($j)->count();
                            if ($countLaboral != 0) {
                                $indeed[$i]['ExpLaboral'][$j]['Lugar'] = $html_search_people->filter('#work-experience-items > div > .data_display > .work_company > .inline-block')->eq($j)->text();
                            } else {
                                $indeed[$i]['ExpLaboral'][$j]['Lugar'] = 'No definido';
                            }
                        } else {
                            $indeed[$i]['ExpLaboral'][$j]['FechDesde'] = 'No definido';
                            $indeed[$i]['ExpLaboral'][$j]['FechHasta'] = 'No definido';
                            $indeed[$i]['ExpLaboral'][$j]['Lugar'] = 'No definido';
                        }
                    }
                    $cont_Educacion = $html_search_people->filter('#education-items > div')->count();
                    if ($cont_Educacion != 0) {
                        for ($z = 0; $z < $cont_Educacion; $z++) {
                            if ($html_search_people->filter('#education-items > div > .data_display > .edu_school')->eq($z)->count() > 0) {
                                $indeed[$i]['Educacion'][$z]['NombreInstitucion'] = $html_search_people->filter('#education-items > div > .data_display > .edu_school')->eq($z)->text();
                            } else {
                                $indeed[$i]['Educacion'][$z]['NombreInstitucion'] = 'No definido';
                            }
                            $countEdu = $html_search_people->filter('#education-items > div > .data_display > .edu_title')->eq($z)->count();
                            if ($countEdu != 0) {
                                $indeed[$i]['Educacion'][$z]['Titulo'] = $html_search_people->filter('#education-items > div > .data_display > .edu_title')->eq($z)->text();
                            } else {
                                $indeed[$i]['Educacion'][$z]['Titulo'] = 'No definido';
                            }
                            if ($html_search_people->filter('#education-items > div > .data_display > .edu_dates')->eq($z)->count() > 0) {
                                $date_lab = $html_search_people->filter('#education-items > div > .data_display > .edu_dates')->eq($z)->text();
                                //FORMAT DATE DC
                                $prueba = preg_split("/[\s,]+/", $date_lab);
                                //echo count($prueba).'----><br>';
                                if (count($prueba) == 1) {
                                    $indeed[$i]['Educacion'][$z]['FechDesde'] = $prueba[0];
                                    $indeed[$i]['Educacion'][$z]['FechHasta'] = $prueba[0];
                                } else if (count($prueba) == 2) {
                                    $indeed[$i]['Educacion'][$z]['FechDesde'] = $prueba[0];
                                    $indeed[$i]['Educacion'][$z]['FechHasta'] = $prueba[1];
                                } else if (count($prueba) == 3) {
                                    $indeed[$i]['Educacion'][$z]['FechDesde'] = $prueba[0];
                                    $indeed[$i]['Educacion'][$z]['FechHasta'] = $prueba[2];
                                } else if (count($prueba) == 4) {
                                    $indeed[$i]['Educacion'][$z]['FechDesde'] = $prueba[0];
                                    $indeed[$i]['Educacion'][$z]['FechHasta'] = $prueba[2] . ' ' . $prueba[3];
                                } else {
                                    $indeed[$i]['Educacion'][$z]['FechDesde'] = $prueba[0] . ' ' . $prueba[1];
                                    $indeed[$i]['Educacion'][$z]['FechHasta'] = $prueba[3] . ' ' . $prueba[4];
                                }
                            } else {
                                $indeed[$i]['Educacion'][$z]['FechDesde'] = 'No definido';
                                $indeed[$i]['Educacion'][$z]['FechHasta'] = 'No definido';
                            }
                        }
                    }else {
                        $indeed[$i]['Educacion'] = 'No definido';
                    }
                    if(count($html_search_people->filter('#military-items')) > 0){
                        if ($html_search_people->filter('#military-items > div > .data_display > .military_country')->count() > 0) {
                            $indeed[$i]['Militar']['Pais'] = $html_search_people->filter('#military-items > div > .data_display > .military_country')->text();
                        }
                        if ($html_search_people->filter('#military-items > div > .data_display > .military_branch')->count() > 0) {
                            $indeed[$i]['Militar']['Rama'] = $html_search_people->filter('#military-items > div > .data_display > .military_branch')->text();
                        }
                        if ($html_search_people->filter('#military-items > div > .data_display > .military_rank')->count() > 0) {
                            $indeed[$i]['Militar']['Rango'] = $html_search_people->filter('#military-items > div > .data_display > .military_rank')->text();
                        }
                        if ($html_search_people->filter('#military-items > div > .data_display > .military_description')->count() > 0) {
                            $indeed[$i]['Militar']['Descripción'] = $html_search_people->filter('#military-items > div > .data_display > .military_description')->text();
                        }
                    }
                    if(count($html_search_people->filter('#additionalinfo-items')) > 0){
                        if ($html_search_people->filter('#additionalinfo-items > div > .data_display > p')->count() > 0) {
                            $indeed[$i]['Militar']['Pais'] = $html_search_people->filter('#additionalinfo-items > div > .data_display')->text();
                        }
                    }
                }
            } else {
                unset($url_nomb);
                $this->cont = $this->cont + 1;
                $cont = $this->cont;
                if ($cont == 5) {
                    $url_nomb[0] = 'Ningún sujeto encontrado';
                } else {
                    sleep(3);
                    $this->testindeedCV();
                }
            }
        } else {
            $indeed='';
        }
        return $indeed;
    }

    private function loginIndeed()
    {
        $client = new Client();
        $guzzleClient = new \GuzzleHttp\Client(array(
            'request.options' => array(
                'proxy' => '134.58.64.158:80'
            ),
            'curl' => array(
                CURLOPT_SSL_VERIFYPEER => false,
            ),
        ));
        $client->setClient($guzzleClient);
        $crawler = $client->request('GET', 'https://secure.indeed.com/account/login');
        if (strpos($crawler->html(), '"form_tk\":\"') !== false) {
            $pieces = explode('\"form_tk\":\"', $crawler->html());
            $resp = explode('\"', $pieces[1]);
            $form_tk = $resp[0];
        }
        if (strpos($crawler->html(), '"surftok\":\"') !== false) {
            $pieces = explode('\"surftok\":\"', $crawler->html());
            $resp = explode('\"', $pieces[1]);
            $surftok = $resp[0];
        }
        $auth = $client->request('POST', 'https://secure.indeed.com/account/login', array('action' => 'login', '__email' => 'pedro.garcia89@aol.com', '__password' => '123456789Abcd', 'remember' => '0', 'hl' => 'es_ES', 'cfb' => '0', 'prv' => '0', 'form_tk' => $form_tk, 'surftok' => $surftok, 'tmpl' => ''));
        return $client;

    }
    private function quitarTildes($cadena)
    {
        $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
        $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
        $cadena = utf8_decode($cadena);
        $cadena = strtr($cadena, utf8_decode($originales), $modificadas);
        $cadena = strtolower($cadena);
        return utf8_encode($cadena);
    }
}