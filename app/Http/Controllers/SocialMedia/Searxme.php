<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 6/09/16
 * Time: 19:23
 */

namespace App\Http\Controllers\SocialMedia;


use Thread;

class Searxme extends Thread
{
// Busqueda1 = ("52889883A" AND "JULIA" AND "VERDEJO" AND "SINTAS")
//Busqueda2 solo primer resultado= site:facebook.com ("EDMUNDO LUJAN OCEJO") intitle:"EDMUNDO LUJAN" -inurl:"public" -inurl:"people"
//http://gplus.slfeed.net/?id=ENRIQUE+MU%C3%91OZ+HURTADO
    public $results;
    private $first_name, $last_name, $name, $dni;
    public function __construct($name, $first_name, $last_name, $dni)
    {
        $this->name = $name;
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->dni = $dni;
    }

    public function run(){
        $this->results = $this->getSearch();
    }
    private function getSearch(){
        $site = 'https://searx.laquadrature.net/';
        $str_search = urlencode('("'.$this->dni.'" AND "'.$this->name.'" AND "'.$this->first_name.'" AND "'.$this->last_name.'")');
        //85.214.29.216/searx
        $search = $this->curl($site . '?q=' . $str_search . '&categories=general&format=json');
        if (!empty($search->results)) {
            $res= array();
            foreach($search->results as $resultados){
                $res[]=array($resultados->title,$resultados->url);
            }
            return $res;
        }else{
            return 'No encontrado';
        }
    }
    private
    function curl($page)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $page);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result);
    }


}