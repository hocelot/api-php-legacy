<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 26/11/16
 * Time: 17:25
 */

namespace App\Http\Controllers\SocialMedia;


use App\Http\Controllers\Worker\ThreadsManager;
use Log;

class SocialManager extends \Threaded
{
    private $name, $first_name, $last_name, $email, $phone, $city, $fb_email_search, $fb_phone_search;

    public function __construct($search_by_email_social, $search_by_phone_social, $name, $first_name, $last_name, $email, $phone, $city)
    {
        $this->fb_email_search = $search_by_email_social;
        $this->fb_phone_search = $search_by_phone_social;
        $this->name = $name;
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->email = $email;
        $this->phone = $phone;
        $this->city = $city;
//        $this->facebook = $facebook_results;
    }

    public function run()
    {
        //Aqui lanzaria los hilos
        require_once(__DIR__ . '/../../../../vendor/autoload.php');
        require_once(__DIR__ . '/../../../../bootstrap/app.php');
//        $worker = new ThreadsManager();
//
//        $linkedin = new Linkedin($this->name, $this->first_name, $this->last_name, $this->city);
//        $worker->stack($linkedin);
//
//        $milanuncios = new MilAnuncios($this->name, $this->first_name, $this->phone);
//        $worker->stack($milanuncios);
//
//
//        $libreborme = new Libreborme($this->name, $this->first_name, $this->last_name);
//        $worker->stack($libreborme);
//
//
//        $relaciones = new FindRelations($this->name, $this->first_name, $this->last_name);
//        $worker->stack($relaciones);
//
////        $instagram = new Instagram($this->name.' '.$this->first_name.' '.$this->last_name,$this->email);
////        $worker->stack($instagram);
//
//        $etools = new eTools($this->name,$this->first_name,$this->last_name);
//        $worker->stack($etools);
//
//        // Start all jobs
//        $worker->start();
//        // Join all jobs and close worker
//        $worker->shutdown();
//        $result[] = $this->curl($this->phone,$this->email);
//        foreach ($worker->data as $key => $value) {
//            $result = array_merge($result, [$key => $value]);
//        }
        //Worker heredado
        $this->worker->addData('social', $this->curl());

    }

    public function curl()
    {
        Log::debug('SOCIAL-');

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://localhost:8082",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "phone=" . $this->phone . "&name=" . $this->name . "&first_name=" . $this->first_name . "&last_name=" . $this->last_name . "&email=" . $this->email . "&fb_phone_search=" . $this->fb_phone_search . "&fb_email_search=" . $this->fb_email_search,
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        Log::debug(print_r(curl_getinfo($curl), true));

        Log::debug(print_r($response, true));
        curl_close($curl);
        return json_decode($response);
    }

}