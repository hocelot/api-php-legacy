<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 25/10/16
 * Time: 15:47
 */

namespace App\Http\Controllers\SocialMedia;


use SimpleXMLElement;
use stdClass;
use Symfony\Component\DomCrawler\Crawler;

class Carrot2
{
    /*
        * Carrot Format:
        <searchresult>
              <query>query (optional)</query>
              <document>
                 <title>Document 1 Title</title>
                 <snippet>Document 1 Content.</snippet>
                 <url>http://document.url/1</url>
              </document>
              <document>
                 <title>Document 2 Title</title>
                 <snippet>Document 2 Content.</snippet>
                 <url>http://document.url/2</url>
              </document>
              <document>
                 <title>Document 3 Title</title>
                 <snippet>Document 3 Content.</snippet>
                 <url>http://document.url/3</url>
              </document>
           </searchresult>
        */
    private $dni, $name, $first_name, $last_name;

    public function __construct($dni, $name, $first, $last)
    {
        $this->dni = $dni;
        $this->name = $name;
        $this->first_name = $first;
        $this->last_name = $last;
    }

    public function search()
    {
        $unit_results = array();
        //eTools
        $etools = $this->eTools();
        //Searxme
        $searxme = $this->searxme();

        if(!$searxme && !$etools) return false;

        foreach ($searxme['hits'] as $hit) {
            if (!in_array($hit['url'], $etools['urls'])) {
                $etools['hits'][] = $hit;
            }
        }
        unset($etools['urls']);
        unset($searxme);
        // var_dump($etools);
        $xml = new SimpleXMLElement('<?xml version=\'1.0\' encoding=\'utf-8\'?><searchresult/>');

        $xml->addChild('query', '("' . $this->dni . '" AND "' . $this->name . '" AND "' . $this->first_name . '" AND "' . $this->last_name . '")');
        foreach ($etools['hits'] as $hit) {
            $document = $xml->addChild('document');
            $document->addChild('title', htmlspecialchars($hit['title']));
            $document->addChild('snippet', htmlspecialchars($hit['text']));
            $document->addChild('url', htmlspecialchars($hit['url']));
        }

        $ch = curl_init();
//$fields['query']
        $fields['dcs.c2stream'] = $xml->asXML();
        curl_setopt_array($ch,
            array(
                CURLOPT_URL => "http://localhost:8080/dcs/rest",
                CURLOPT_POST => true,
                CURLOPT_HTTPHEADER => array('Content-Type: multipart/formdata'),
                CURLOPT_HEADER => false,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POSTFIELDS => $fields
            )
        );

        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    private function searxme()
    {
        $site = 'https://searx.laquadrature.net/';
        $str_search = urlencode('("' . $this->dni . '" AND "' . $this->name . '" AND "' . $this->first_name . '" AND "' . $this->last_name . '")');
        //85.214.29.216/searx
        $search = $this->curl($site . '?q=' . $str_search . '&categories=general&format=json');
        if (!empty($search->results)) {
            $res = new stdClass();
            foreach ($search->results as $resultados) {
                $res->hits[] = array('title' => $resultados->title, 'text' => $resultados->content, 'url' => $resultados->url);
            }
            return array('hits' => $res->hits);
        } else {
            return false;
        }
    }

    private function eTools()
    {
        $site = 'http://www.etools.ch';
        $str_search = urlencode('("' . $this->dni . '" AND "' . $this->name . '" AND "' . $this->first_name . '" AND "' . $this->last_name . '")');
        $search = $this->curletools($site . '/partnerSearch.do?partner=TestAdvanced&query=' . $str_search . '&language=es&country=web&includeClusters=true');//country=web para todos //&maxRecords=200
        $results = new stdClass();
        $crawler = new Crawler();
        $crawler->addXmlContent($search);
        $crawler->filterXPath('//result/mergedRecords/record')
            ->each(function ($node) use ($results) {
                $aux['title'] = $node->filter('title')->text();
                $aux['text'] = $node->filter('text')->text();
                $aux['url'] = $node->filter('url')->text();
                $results->urls[] = $aux['url'];
                $results->hits[] = $aux;
            });
        if (isset($results->hits))
            return array('hits' => $results->hits, 'urls' => $results->urls);
        else return false;
    }

    private
    function curletools($page)
    {
        $url = $page;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    private
    function curl($page)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $page);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result);
    }
}