<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 1/09/16
 * Time: 10:08
 */

namespace App\Http\Controllers\SocialMedia;

use stdClass;
use Symfony\Component\DomCrawler\Crawler;
use Thread;

class FindRelations extends \Threaded
//class FindRelations
{
    private $first_name, $last_name, $name;
//    public $results;

    public function __construct($name, $first_name, $last_name)
    {
        $this->name = $name;
        $this->first_name = $first_name;
        $this->last_name = $last_name;
    }

    public function run()
    {
        require_once(__DIR__ . '/../../../../vendor/autoload.php');
        require_once(__DIR__ . '/../../../../bootstrap/app.php');
        $this->worker->addData('relations', $this->findRelations());
//        $this->results = $this->findRelations();
    }

//    public function search()
    public function findRelations()
    {
        $page = 'http://gplus.slfeed.net/?id=' . urlencode($this->name . ' ' . $this->first_name . ' ' . $this->last_name);

        $crawler = new Crawler();
        $crawler->addHtmlContent($this->curl($page));


        if ($crawler->filter('#content > .searchlist > .item')->count() > 0) {
            $url_per = $crawler->filter('#content > .searchlist > .item')->first()->filter('.caption > p > a')->attr('href');
            $crawler->clear();
            $crawler->addHtmlContent($this->curl($url_per));
            // Foto
            if ($crawler->filter('#content > #imgbox > #top > img')->count() > 0) {
                $img = $crawler->filter('#content > #imgbox > #top > img')->attr('src');
            }
            // Amigos
            $relations = new stdClass();
            $crawler2 = new Crawler();
            $count = $crawler->filter('#content > .list')->last()->filter('.item')->count();

            for ($i = 0; $i < $count; $i++) {
                $crawler2->clear();
                $url = $crawler->filter('#content > .list')->last()->filter('.item')->eq($i)->filter('.caption > p > a')->attr('href');
                $crawler2->addHtmlContent($this->curl($url));
                $relations->ten_prob_friends[] = strtoupper(trim(str_replace(' Google Plus friends list.', '', $crawler2->filter('#content > h1')->first()->text())));
                if ($i == 5) {
                    break;
                }
            }
            $result = array();
            if (isset($img)) {
                $result['img'] = $img;
            }
            if (isset($relations->ten_prob_friends)) {
                $result['relations'] = $relations;
            }
            if (empty($result)) {
                return 'No encontrado';
            }
            return $result;
        } else {
            return 'No encontrado';
        }
    }

    private function curl($page)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $page);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}