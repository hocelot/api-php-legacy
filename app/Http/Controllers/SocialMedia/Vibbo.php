<?php
/**
 * Created by PhpStorm.
 * User: rmarcos
 * Date: 7/10/16
 * Time: 11:53
 */

namespace App\Http\Controllers\SocialMedia;


use Symfony\Component\DomCrawler\Crawler;

class Vibbo
{
    private $tlf;

    public function __construct($mobile)
    {
        $this->tlf = trim($mobile);
    }

    public function search()
    {
        $html_web = 'http://www.vibbo.com/anuncios-toda-espana/' . $this->tlf.".htm";
        $vibbo = new \stdClass();
        $vibbo->anuncio = array();
        $html_search = new Crawler();
        $html_search->addHtmlContent($this->curl($html_web));
        $html_search->filter('#content > #main > .wrapper-content > #list_container > #list_container_left > .gallery > #list_ads_table > div')->each(function ($node) use ($vibbo) {
            $web = $node->filter('.flipper > .front >.add-image > .thumbnail_container > a')->href();
            unset($crawler);
            $crawler = new Crawler();
            $crawler->addHtmlContent($this->curl($web));
            $enlaces = $crawler->filter('#generalContent > #content > #main > .adview_mainInfo > .adview_mainInfo__mainCol > .adview_navigation > #breadcrumbsContainer > .breadcrumbs > a');
            $place = $enlaces->eq(1);
            $category = $enlaces->eq(2);
        });
            return $vibbo;

    }
    private function curl($page)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $page);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        //var_dump($result);
        return $result;
    }
}