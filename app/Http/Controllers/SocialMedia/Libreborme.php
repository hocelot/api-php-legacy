<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 1/09/16
 * Time: 10:36
 */

namespace App\Http\Controllers\SocialMedia;

use stdClass;
use Symfony\Component\DomCrawler\Crawler;

//use Thread;

class Libreborme extends \Threaded
{
    private $first_name, $last_name, $name;

    public function __construct($name, $first_name, $last_name)
    {
        $this->name = $name;
        $this->first_name = $first_name;
        $this->last_name = $last_name;
    }

    public function run()
    {
        require_once(__DIR__ . '/../../../../vendor/autoload.php');
        require_once(__DIR__ . '/../../../../bootstrap/app.php');
        $this->worker->addData('libreborme', $this->search());
    }

    private function search()
    {
//        Log::debug('Lanza libreborme '.microtime(true));
        $libreborme = $this->getData();
        $einforma = new \stdClass();
        $einforma_thread = new Einforma();
        if (isset($libreborme->persona) && is_array($libreborme->persona)) {
            foreach ($libreborme->persona as $per) {
                foreach ($per->companies as $comp) {
                    sleep(1);
                    $einforma->companies[] = array($comp => $einforma_thread->parseEinforma($comp));
                }
//            Log::debug(print_r($aux,true));
            }
            unset($einforma_thread);

            $status = 200;
        } else {
            $einforma->companies = 'No se han encontrado datos';
            $status = 400;
        }
        return ['hits' => ['borme' => $libreborme, 'companies_info' => $einforma->companies], 'status' => $status];

    }

    private function getData()
    {
        $search = str_replace(' ', '+', $this->first_name . ' ' . $this->last_name . ' ' . $this->name);
        $page = "http://libreborme.net/borme/api/v1/persona/search/?q=" . $search . "&page=1";
        $result = $this->curl($page);
        $borme = new stdClass();
        if (count($result->objects) > 0) {
            // Pueden haber mas de uno
            foreach ($result->objects as $persona) {
                //Para no saturar el servidor
                $page = 'http://libreborme.net' . $persona->resource_uri;
                $persona_borme = new stdClass();
                sleep(5);
                $res = $this->curl($page);
//                if (strtoupper($res->name) == strtoupper($this->first_name.' '.$this->last_name.' '.$this->name)) {
                $persona_borme->nombre = $res->name;
                $persona_borme->cargos_actuales = $res->cargos_actuales;
                $persona_borme->cargos_historicos = $res->cargos_historial;
                $persona_borme->companies = $res->in_companies;
                //                foreach ($res->in_companies as $comp) {
//                    $einforma_thread = new Einforma($comp);
//                    $einforma_thread->start() && $einforma_thread->join();
//                    $persona_borme->companies[] = $einforma_thread->result;
//                    unset($einforma_thread);
//                }
                $borme->persona[] = $persona_borme;
//                }
            }
        } else {
            $borme = 'No se encuentra en ningún acto del borme';
        }
        return $borme;
    }


    private function curl($page)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $page);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result);
    }
}