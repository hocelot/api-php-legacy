<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 13/07/16
 * Time: 19:58
 */

namespace App\Http\Models\Account;
use Log;
use StdClass;

class BillModel {
    public static function prepareParams($id)
    {
        $res['val_as_base_url'] = app('wsdl')->getBaseUrl();
        $res['ref_as_mensaje'] = '';
        $res['val_astr_mr_compras'] = array();
        $res['val_ad_ter_codigo'] = $id;
        return $res;
    }

    public static function getBills($id)
    {
        $params = self::prepareParams($id);
        try {
            $result = new stdClass();
            $result->data = app('wsdl')->call_ws(app('wsdl')->getWsUrl('clientes'), 'hc_facturas', $params);
            $result->status = 200;
            return $result;
        } catch (\Exception $f) {
            Log::error('Exception [listQueries] ' . $f);
            $result = new stdClass();
            $result->data = $f;
            $result->status = 400;
            return $result;
        }
    }

    public static function getDataBill($idFact)
    {
        $params['val_as_base_url'] = app('wsdl')->getBaseUrl();
        $params['ref_as_mensaje'] = '';
        $params['val_as_num_factura'] = $idFact;
        $params['val_asrt_datos_factura'] = array ();
        $params['val_asrt_desglose_batch'] = array ();
        $params['val_asrt_desglose_web'] = array ();
        $params['val_asrt_desglose_api'] = array ();
        $result = new stdClass();
        $result->status = 200;
        try
        {
            $return = app('wsdl')->call_ws(app('wsdl')->getWsUrl('clientes'), 'hc_desglose_factura', $params);
            $result->data = $return;
            //Log::debug(print_r($result, true));
            return $result;

        } catch (\Exception $f)
        {
            Log::error('Exception [getDataQuery] ' . $f);
            $result->status = 400;
            $result->data = 'Error interno';
            return $result;
        }
    }

}