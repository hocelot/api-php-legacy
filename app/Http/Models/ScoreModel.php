<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 19/06/16
 * Time: 19:43
 */

namespace App\Http\Models;

use Log;

class ScoreModel {

    public static function calculateScore($params, $fecha, $usuario, $ip, $origen, $ine, $params_client)
    {

//        'srt_tgmiratingeval' => array(
//        'origen' => $origen,
//        'fecha' => $fecha,
//        'usuario' => $usuario,
//        'ip' => $ip
//    )

        $params_calc_mirating = array(
            'val_as_base_url'       => app('wsdl')->getBaseUrl(),
            'val_al_ter_codigo'     => $params['eval_id'],
            'val_as_latitud'        => $params['latitude'],
            'val_as_longitud'       => $params['longitude'],
            'val_as_tipo_vivienda'  => $params['lp_type'],
            'val_as_codpostal'      => $ine,//$params['zip_code'],
            'ref_as_tgmiratingeval' => array(),
            'ref_as_prevision'      => array(
                'str_prevision' => array(
                    'origen' => $origen,
                    'tin'    => 0
                )
            ),
            'ref_ll_metros'         => round($params['m2']),
            'ref_as_nbhood'         => $params['nbhood'],//utf8_encode($params['nbhood']),
            'ref_as_mensaje'        => ''
        );
        // Carrefour
        if (isset($params_client->economics->tin))
            $params_calc_mirating['ref_as_prevision']['str_prevision']['tin'] = $params_client->economics->tin;

        if (isset($params_client->economics->tae))
            $params_calc_mirating['ref_as_prevision']['str_prevision']['tae'] = $params_client->economics->tae;

        if (isset($params_client->economics->plazo))
            $params_calc_mirating['ref_as_prevision']['str_prevision']['plazo_ped'] = $params_client->economics->plazo;

        if (isset($params_client->economics->importe))
            $params_calc_mirating['ref_as_prevision']['str_prevision']['importe'] = $params_client->economics->importe;

        if (isset($params_client->economics->producto))
            $params_calc_mirating['ref_as_prevision']['str_prevision']['producto'] = $params_client->economics->producto;

        if (isset($params_client->economics->finalidad))
            $params_calc_mirating['ref_as_prevision']['str_prevision']['finalidad'] = $params_client->economics->finalidad;

        if (isset($params_client->economics->tipo_cliente))
            $params_calc_mirating['ref_as_prevision']['str_prevision']['empleado_carrefour'] = $params_client->economics->tipo_cliente;
//            $params_calc_mirating['ref_as_tgmiratingeval']['str_tgmiratingeval']['tin'] = 0.04;
//            $params_calc_mirating['ref_as_tgmiratingeval']['tae'] = 0.3;
//            $params_calc_mirating['ref_as_tgmiratingeval']['plazo_ped'] = '24';
//            $params_calc_mirating['ref_as_tgmiratingeval']['importe'] = '10000';
//            $params_calc_mirating['ref_as_tgmiratingeval']['producto'] = 'COCHE';
//            $params_calc_mirating['ref_as_tgmiratingeval']['finalidad'] ='CASA';


        try
        {
//            Log::debug('Envio mr_calculaevaluacion');
           Log::debug(print_r($params_calc_mirating, true));
            return app('wsdl')->call_ws(app('wsdl')->getWsUrl('clientes'), 'mr_calculaevaluacion', $params_calc_mirating);
        } catch (\Exception $f)
        {
            Log::error('Exception calculateScore ' . $f);

            return ['status' => 100, 'exception' => $f];
        }
    }

    public static function unemploymentRate($town)
    {
        $params_calc_tasaparo = array(
            'val_as_base_url'     => app('wsdl')->getBaseUrl(),
            'val_ai_comunidad'    => $town,
            'ref_str_tasaparocom' => '',
            'ref_as_mensaje'      => ''
        );
        try
        {
            return app('wsdl')->call_ws(app('wsdl')->getWsUrl('clientes'), 'dametasaparocom', $params_calc_tasaparo);
        } catch (\Exception $f)
        {
            Log::error('Exception unemploymentRate ' . $f);

            return ['status' => 100, 'exception' => $f];
        }
    }

    public static function getM2forLevel3($params)
    {
        $params_calc_mirating = array(
            'val_as_base_url'      => app('wsdl')->getBaseUrl(),
            'val_as_comunidad'     => $params['id_com'],
            'val_as_latitud'       => $params['latitude'],
            'val_as_longitud'      => $params['longitude'],
            'val_as_tipo_vivienda' => $params['lp_type'],
            'val_as_codpostal'     => $params['zip_code'],
            'val_as_radio_pisos'   => $params['radio'],
            'ref_as_metros'        => '',
            'ref_as_precio'        => '',
            'ref_as_mensaje'       => ''
        );
        try
        {
//            Log::debug('Envio mr_precio_m2');
//            Log::debug(print_r($params_calc_mirating, true));
            return app('wsdl')->call_ws(app('wsdl')->getWsUrl('clientes'), 'mr_precio_m2', $params_calc_mirating);
        } catch (\Exception $f)
        {
            Log::error('Exception getM2forLevel3 ' . $f);

            return ['status' => 100, 'exception' => $f];
        }
    }

    public static function getM2Level1or2($address)
    {
        $m2 = 0;
        $ine_code = AddressModel::getIneCode($address);
        Log::debug('AQUI INE   ' . $ine_code);
        $results = app('db')->connection('catastro')->table('cat_15_' . $ine_code)->select('*')->where('tipodevia', $address->road_type)
            ->where('primernumeropolicia', $address->number);
        if ($address->level == 1)
        {
            if ( ! empty($address->letter))
            {
                $results = $results->where('primeraletra', $address->letter);
            }
            if ( ! empty($address->km))
            {
                $results = $results->where('kilometro', $address->km);
            }
            if ( ! empty($address->block))
            {
                $results = $results->where('bloque', $address->block);
            }
            if ( ! empty($address->stair))
            {
                $results = $results->where('escalera', $address->stair);
            }
            if ( ! empty($address->floor))
            {
                $results = $results->where('planta', $address->floor);
            }
            if ( ! empty($address->door))
            {
                $results = $results->where('puerta', $address->door);
            }
        }
        $results = $results->where('nombredevia', $address->street)->get();
        if ($address->level == 2)
        {
            foreach ($results as $row)
            {
                $m2 += $row->supelementosconstructivos;
            }
            if ($m2 > 0)
            {
                $m2 = $m2 / count($results);
            }
        }
        else
        {

            foreach ($results as $row)
            {
                $m2 = $row->supelementosconstructivos;
            }
        }

//        Log::debug('Envio m2 nivel 1');
//        Log::debug(print_r($m2, true));
        return $m2;
    }

    public
    static function wsdlsetEquifaxWS($client_id, $str_tgclientesequifax)
    {


        $val_astr_tgclientesequifax['str_tgclientesequifax'] = $str_tgclientesequifax;
        $params_grabar_equifax = array(
            'val_as_base_url'            => app('wsdl')->getBaseUrl(),
            'val_al_ter_codigo'          => $client_id,
            'val_al_cldel_codigo'        => 1,
            'val_astr_tgclientesequifax' => $val_astr_tgclientesequifax,
            'ref_al_id_equifax'          => 0,
            'ref_as_mensaje'             => ''
        );
        try
        {
            return app('wsdl')->call_ws(app('wsdl')->getWsUrl('clientes'), 'grabarconsultaequifax', $params_grabar_equifax);
        } catch (\Exception $f)
        {
            Log::error('Exception Equifax ' . $f);

            return ['status' => 100, 'exception' => $f];
        }
    }


}
