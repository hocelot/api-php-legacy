<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 2/11/16
 * Time: 8:51
 */

namespace App\Http\Models\GeoCheck;


class TownModel
{
    public static function normalize($zip_code,$town){
        $pob = AddressModel::getPoblation($zip_code);
        if (!isset($pob->ref_astr_codpost->str_adcodpost_nuevo)) {
            return false;
        }

        $cod_muns = [];
        foreach ($pob->ref_astr_codpost->str_adcodpost_nuevo as $arr) {
            $cod_muns[] = $arr->cod_poblacion;
        }

        $cod_prov = substr($zip_code, 0, 2);

        $busquedas = array('municipio', 'entidadcolectiva', 'entidadsingular', 'nucleo');
        $cont = 0;
        $arr = [];
        do {
            $query = [
                'query' => [
                    'bool' => [
                        'must' => [
                            'match' => [
                                $busquedas[$cont] => [
                                    "fuzziness" => "AUTO",
                                    "operator" => "and",
                                    "query" => $town,
                                ],
                            ],
                        ],
                        'filter' => [
                            ['term' => ["codprov" => $cod_prov]],
                            ['terms' => ["codmun" => $cod_muns]]
                        ]
                    ]
                ]
            ];

            $arr = self::elastica_search('towns','address',$query);
            $cont++;
        } while (count($arr) == 0 && $cont < 4);

        if (count($arr) == 0 && $cont < 5) {
            return false;
        } else {
            return $arr[0]->getData()['codprov']. $arr[0]->getData()['codmun'];
        }

    }
}