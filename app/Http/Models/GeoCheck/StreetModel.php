<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 2/11/16
 * Time: 8:52
 */

namespace App\Http\Models\GeoCheck;


class StreetModel
{
    public static function normalize($ine,$rt, $street)
    {
        //TODO añadir nombre de vias unicos
        $street = GeoCheckModel::del_articulos($street);
        $result = false;

        $query = [
            'query' => [
                'bool' => [
                    'must' => [
                        'match' => [
                            'nombredevia' => [
                                "fuzziness" => "AUTO",
                                "operator" => "and",
                                "query" => $street,
                            ],
                        ],
                    ],
                    'filter' => [
                        ['term' => ["tipodevia" => $rt]],
                    ]
                ]
            ]
        ];
        $arr = GeoCheckModel::elastica_search('catastro', $ine, $query);
        if (count($arr) == 1) {
            $result = $arr[0]->getData()['nombredevia'];
        } elseif (count($arr) > 1) {
            $max_percent = 0;
            foreach ($arr as $res) {
                similar_text($res->getData()['nombredevia'], $street, $percent);
                if ($percent > $max_percent) {
                    $max_percent = $percent;
                    $result = $res->getData()['nombredevia'];
                }
            }
        }
        return $result;
    }
}