<?php

/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 29/10/16
 * Time: 17:46
 */

namespace App\Http\Models\GeoCheck;

use App\Http\Controllers\Proxies\Proxies;
use App\Http\Models\AddressModel;
use stdClass;
use Log;

class GeoCheckModel
{
    public static function elastica_search($index, $type, $search_query)
    {
        $start = microtime(true);
        Log::debug("EMPIEZA ELASTIC ".$start);

        $client = new \Elastica\Client(array(
            'host' => '192.168.79.20',
            'port' => 9200
        ));
        if ($index == 'catastro') {
            //Vemos si el codigo ine existe en ese indice, si no existe estara en catastro15
//            Log::debug('CATASTRO EXISTE? indice '.$client->getIndex($index)->getType($type)->exists());
            if (!$client->getIndex($index)->getType($type)->exists()) {
                $index = 'catastro15';
            }
        }
        $query = \Elastica\Query::create($search_query);
//       Log::debug(print_r($query,true));
        Log::debug("TERMINA ELASTIC ".(microtime(true) - $start));
        return $client->getIndex($index)->getType($type)->search($query, 100);

    }

    public static function del_articulos($str)
    {
        $str = str_replace("&#039;", "'", $str);
        //Quitamos determinantes del nombre de la calle
        $articulos = array('DEL', 'DE', 'LA', 'EL', 'LO', 'LAS', 'LOS', "L'", "N'", "A", "VAN");
//        foreach ($articulos as $articulo) {
//          $str =  preg_replace('/'.$articulo.'/',$articulo,$str);
        //Con espacios delante tipo L' ALBUFERA
//            $str = str_replace($articulo . ' ', '', $str);
//            Con espacios detrás tipo ALBUFERA L'
//            $str = str_replace(' ' . $articulo, '', $str);
//        }
        $str_aux = explode(" ", $str);

        $resultado = array_diff($str_aux, $articulos);

        $str = implode(" ", $resultado);
        return $str;
    }

    public static function normalize_roadType($rt_search)
    {
        $rt_search = self::del_articulos($rt_search);
        $result = false;
        $query = [
            'query' => [
                'bool' => [
                    'must' => [
                        'match' => [
                            'full_name' => [
                                "fuzziness" => "AUTO",
                                "operator" => "and",
                                "query" => $rt_search,
                            ],
                        ],
                    ]
                ]
            ]
        ];

        $arr = self::elastica_search('address', 'roadtypes', $query)->getResults();
        if (count($arr) == 1) {
            $result = $arr[0]->getData()['name'];
        } elseif (count($arr) > 1) {
            $max_percent = 0;
            foreach ($arr as $res) {
                similar_text($res->getData()['full_name'], $rt_search, $percent);
                if ($percent > $max_percent) {
                    $max_percent = $percent;
                    $result = $res->getData()['name'];
                }
            }
        }
        return $result;
    }
//    public static function findAdress($ine, $rt, $street, $pnp, $letra, $km, $bloque, $escalera, $planta, $puerta)
// []['must']
    public static function search($ine, $rt, $street, $pnp, $letra = '', $km = '', $bloque = '', $escalera = '', $planta = '', $puerta = '', $busqueda_exacta = false)
    {

        $querybool = new \Elastica\Query\BoolQuery();


        $filter_tipovia = new \Elastica\Query\MultiMatch();
        $filter_tipovia->setFields(["tipodevia.raw","tipodevia.keyword"]);
        $filter_tipovia->setQuery($rt);
        $querybool->addFilter($filter_tipovia);

        $filter_pnp = new \Elastica\Query\MultiMatch();
        $filter_pnp->setFields(["primernumeropolicia.raw","primernumeropolicia.keyword"]);
        $filter_pnp->setQuery($pnp);
        $querybool->addFilter($filter_pnp);

        //--------- 05/09/2017 -------------------
//        $filter_tipovia = new \Elastica\Query\Term();
//        $filter_tipovia->setTerm('tipodevia.raw', $rt);
//        $querybool->addFilter($filter_tipovia);

//        $filter_pnp = new \Elastica\Query\Term();
//        $filter_pnp->setTerm('primernumeropolicia.raw', $pnp);
//        $querybool->addFilter($filter_pnp);


//        if (!empty($planta)) {
//            if (strlen($planta) == 1) $planta = '0' . $planta;
//            $filter_planta = new \Elastica\Query\Term();
//            $filter_planta->setTerm('planta.raw', $planta);
//            $querybool->addFilter($filter_planta);
//        }

        //Añadimos match nombredevia y primernumeropolicia
        if (!$busqueda_exacta) {
            $field = 'nombredevia';
            $operator = 'and';
            $fuzziness = "AUTO";
            $street_match = new \Elastica\Query\Match();
            $street_match->setFieldQuery($field, $street);
            $street_match->setFieldOperator($field, $operator);
            $street_match->setFieldFuzziness($field, $fuzziness);
            $querybool->addMust($street_match->toArray());
        } else {
            $field = 'nombredevia';
            $street_match = new \Elastica\Query\Match();
            $street_match->setFieldQuery($field, $street);
            $querybool->addMust($street_match->toArray());
        }

        //Filtramos que los inmuebles que vengan sean distinto de almacen
//        $filter_uso = new \Elastica\Query\Match();
//        $filter_uso->setFieldOperator('uso','OR');
        $filter_uso = new \Elastica\Query\MatchPhrasePrefix('uso', 'A');
        $querybool->addMustNot($filter_uso->toArray());

        if (!empty($letra)) {
            $field = 'letra';
            $operator = 'and';
            $fuzziness = "AUTO";
            $letra_match = new \Elastica\Query\Match();
            $letra_match->setFieldQuery($field, $letra);
            $letra_match->setFieldOperator($field, $operator);
            $letra_match->setFieldFuzziness($field, $fuzziness);
            $querybool->addMust($letra_match->toArray());
        }
        if (!empty($km)) {
            $field = 'kilometro';
            $operator = 'and';
            $fuzziness = "AUTO";
            $km = str_replace(',', '.', $km);
            if (floatval($km)) $km = $km * 1000;
            $km_match = new \Elastica\Query\Match();
            $km_match->setFieldQuery($field, $km);
            $km_match->setFieldOperator($field, $operator);
            $km_match->setFieldFuzziness($field, $fuzziness);
            $querybool->addMust($km_match->toArray());
        }
        if (!empty($bloque)) {
            if ($bloque == '00') $bloque = 0;
            else
                $bloque = ltrim($bloque, '0');

            $field = 'bloque';
            $operator = 'and';
            if ((strlen($bloque) < 2 && preg_match("/^[0-9]+$/", $bloque)) || $bloque == 0) {
                $operator = 'or';
                $bloque = '0' . $bloque . ' ' . $bloque;
            }
            $fuzziness = "AUTO";
            $bloque_match = new \Elastica\Query\Match();
            $bloque_match->setFieldQuery($field, $bloque);
            $bloque_match->setFieldOperator($field, $operator);
            $bloque_match->setFieldFuzziness($field, $fuzziness);
            $querybool->addMust($bloque_match->toArray());
        }
        if (!empty($escalera)) {
            $field = 'escalera';
            $operator = 'and';
            if ($escalera == '00') $escalera = 0;
            else
                $escalera = ltrim($escalera, '0');

            if ((strlen($escalera) < 2 && preg_match("/^[0-9]+$/", $escalera)) || $escalera == 0) {
                $operator = 'or';
                $escalera = '0' . $escalera . ' ' . $escalera;
            }
            $fuzziness = "AUTO";
            $escalera_match = new \Elastica\Query\Match();
            $escalera_match->setFieldQuery($field, $escalera);
            $escalera_match->setFieldOperator($field, $operator);
            $escalera_match->setFieldFuzziness($field, $fuzziness);
            $querybool->addMust($escalera_match->toArray());
        }
        if (!empty($planta)) {
            $field = 'planta';
            $operator = 'and';
            if ($planta == '00') $planta = 0;
            else
                $planta = ltrim($planta, '0');

            if ((strlen($planta) < 2 && preg_match("/^[0-9]+$/", $planta)) || $planta == 0) {
                $operator = 'or';
                $planta = '0' . $planta . ' ' . $planta;
            }
            similar_text($planta, 'BAJO', $percent);
            if ($percent > 40.1) {
                $operator = 'or';
                $planta = '00 BJ B BP1 BP2 B1 B2 B3 BG OB B0 BB BA BC BE BI BD BX PB';
            }

            $fuzziness = "AUTO";
            $planta_match = new \Elastica\Query\Match();
            $planta_match->setFieldQuery($field, $planta);
            $planta_match->setFieldOperator($field, $operator);
            $planta_match->setFieldFuzziness($field, $fuzziness);
            $querybool->addMust($planta_match->toArray());
        }

        if (!empty($puerta)) {
            if ($puerta == '00') $puerta = 0;
            else
                $puerta = ltrim($puerta, '0');
            $array_abc = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'Ñ', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'Y', 'Z', 'X');
            $field = 'puerta';
            $operator = 'and';
            if (preg_match("/^[0-9]+$/", $puerta)) {
                $aux = '';
                if ($puerta < 25) {
                    $operator = 'or';
                    $aux = $array_abc[$puerta - 1];
                }
                if (strlen($puerta) < 2) {
                    $operator = 'or';
                    $aux = trim('0' . $puerta . ' ' . $puerta . ' ' . $aux);
                }
                if (!empty($aux))
                    $puerta = $aux;
            } else {
                //si es solo nº o letras entro
                if (!preg_match('/(([0-9])+([A-Za-z])+)|(([A-Za-z])+([0-9])+)/', $puerta) && in_array(strtoupper(substr($puerta, 0, 1)), $array_abc)) {
                    $operator = 'or';
                    $aux = 1 + array_search(strtoupper(substr($puerta, 0, 1)), $array_abc);
                    if (strlen($aux) < 2) {
                        $operator = 'or';
                        $aux = '0' . $aux . ' ' . $aux;
                    }
                    $puerta = $puerta . ' ' . $aux;
                }
            }
            $fuzziness = "AUTO";
            $puerta_match = new \Elastica\Query\Match();
            $puerta_match->setFieldQuery($field, $puerta);
            $puerta_match->setFieldOperator($field, $operator);
            $puerta_match->setFieldFuzziness($field, $fuzziness);
            $querybool->addMust($puerta_match->toArray());
        }
        $query = [
            'query' => $querybool->toArray()
        ];
//        Log::debug('ELASTIC busco' . print_r($query, true));
        $arr = self::elastica_search('catastro', $ine, $query)->getResults();//->getAggregation("street_names")["buckets"];
//        Log::debug('ELASTIC RESULT' . print_r($arr, true));
        if (count($arr) > 0) {
            $result = $arr;
        } else $result = false;

        return $result;
    }

    public static function search_street_lat_long($ine, $street)
    {
        $querybool = new \Elastica\Query\BoolQuery();
        //Añadimos filtro genérico (.raw antiguo .keyword)

        $field = 'nombredevia';
        $street_match = new \Elastica\Query\Match();
        $street_match->setFieldQuery($field, $street);
        $querybool->addMust($street_match->toArray());


        //Filtramos que los inmuebles que vengan sean distinto de almacen
        $filter_uso = new \Elastica\Query\MatchPhrasePrefix('uso', 'A');
        $querybool->addMustNot($filter_uso->toArray());

        $query = [
            'query' => $querybool->toArray()
        ];
        $arr = self::elastica_search('catastro', $ine, $query)->getResults();//->getAggregation("street_names")["buckets"];
//        Log::debug('ELASTIC RESULT' . print_r($arr, true));
        if (count($arr) > 0) {
            $result = $arr;
        } else $result = false;

        return $result;
    }

    public static function normalize_Town($zip_code, $town)
    {
        $pob = AddressModel::getPoblation($zip_code);
//        Log::debug(print_r($pob, true));
        if (!isset($pob->ref_astr_codpost->str_adcodpost_nuevo)) {
            return false;
        }
        $provincia = $pob->ref_astr_codpost->str_adcodpost_nuevo[0]->descripcion_provincia;
        $cod_muns = [];
        foreach ($pob->ref_astr_codpost->str_adcodpost_nuevo as $arr) {
            $cod_muns[] = $arr->cod_poblacion;
        }
        $cod_prov = substr($zip_code, 0, 2);

        if (count($cod_muns) == 1)
            return [
                'town' => $pob->ref_astr_codpost->str_adcodpost_nuevo[0]->poblacion,
                'provincia' => $provincia,
                'ine' => $cod_prov . $cod_muns[0]];

        $town = self::del_articulos(trim(str_replace(',', '', $town)));


        $busquedas = array('municipio', 'entidadcolectiva', 'entidadsingular', 'nucleo');
        $cont = 0;
        $arr = [];
        do {
            $query = [
                'query' => [
                    'bool' => [
                        'must' => [
                            'match' => [
                                $busquedas[$cont] => [
                                    "fuzziness" => "AUTO",
                                    "operator" => "and",
                                    "query" => $town,
                                ],
                            ],
                        ],
                        'filter' => [
                            ['term' => ["codprov" => $cod_prov]],
                            ['terms' => ["codmun" => $cod_muns]]
                        ]
                    ]
                ]
            ];
//            Log::debug(print_r($query, true));
            $arr = self::elastica_search('address', 'towns', $query)->getResults();
//            Log::debug(print_r($arr, true));
            $cont++;
        } while (count($arr) == 0 && $cont < 4);

        if (count($arr) == 0 && $cont < 5) {
            return false;
        } else {
           return ['town' => $arr[0]->getSource()['municipio'],
                'provincia' => $provincia,
                'ine' => $arr[0]->getData()['codprov'] . $arr[0]->getData()['codmun']];
        }

    }

    public static function getDataAjax($ine, $rt, $street, $pnp, $letter, $km, $block, $stair, $floor, $door)
    {
        try {
            $tipo15 = app('db')->connection('catastro')->table('cat_15_' . $ine)->select('*')->where('tipodevia', $rt)
                ->where('primernumeropolicia', $pnp);

            if (!empty($letter)) {
                $tipo15 = $tipo15->where('primeraletra', $letter);
            }
            if (!empty($km)) {
                $tipo15 = $tipo15->where('kilometro', $km);
            }
            if (!empty($block)) {
                $tipo15 = $tipo15->where('bloque', $block);
            }
            if (!empty($stair)) {
                $tipo15 = $tipo15->where('escalera', $stair);
            }
            if (!empty($floor)) {
                $tipo15 = $tipo15->where('planta', $floor);
            }
            if (!empty($door)) {
                $tipo15 = $tipo15->where('puerta', $door);
            }
            $tipo15 = $tipo15->where('nombredevia', $street)->first();
//            Log::debug(print_r($tipo15, true));
            $tipo11 = app('db')->connection('catastro')->table('cat_11_' . $ine)->select('*')
                ->where('parcelacatastral', substr($tipo15->referenciacatastral, 0, 14))->first();
//            Log::debug(print_r($tipo11, true));
            return array(
                'parcela' => substr($tipo15->referenciacatastral, 0, 14),
                'm2' => $tipo15->supelementosconstructivos,
                'latitude' => $tipo11->latitude,
                'longitude' => $tipo11->longitude,
                'used_for' => $tipo15->usodebieninmueble,
                'level' => 1
            );
        } catch (\Exception $e) {
//            Log::debug(print_r($e, true));
            return false;

        }
    }

    public static function save($ter_codigo, $ip, $level, $direccion, $cliente, $origen)
    {
        $str_mr_geo_check = new stdClass();
        $str_mr_geo_check->ter_codigo = $ter_codigo;
        $str_mr_geo_check->fecha = date('c');
        $str_mr_geo_check->ip = $ip;
        if ($level == 3) $str_mr_geo_check->resultado = 'KO';//OK,KO
        else $str_mr_geo_check->resultado = 'OK';//OK,KO
        $str_mr_geo_check->usuario = '-';
        $str_mr_geo_check->geo_check = $level;
        $str_mr_geo_check->direccion = $direccion;
        $str_mr_geo_check->origen = $origen;
        $str_mr_geo_check->cliente = $cliente;

        $val_astr_mr_geo_check['str_mr_geo_check'] = $str_mr_geo_check;
        $params_calc_mirating = array(
            'val_as_base_url' => app('wsdl')->getBaseUrl(),
            'val_astr_mr_geo_check' => $val_astr_mr_geo_check,
            'ref_as_mensaje' => ''
        );
        try {
            return app('wsdl')->call_ws(app('wsdl')->getWsUrl('clientes'), 'mr_geo_check', $params_calc_mirating);
        } catch (\Exception $f) {
//            Log::error('Exception mr_geo_check ');

            return ['status' => 100, 'exception' => $f];
        }
    }

    public static function searchCatastro($province, $town, $rt, $street, $pnp, $bloque = '', $escalera = '', $planta = '', $puerta = '', $busqueda_repetida = false)
    {
        try {
            if (strlen($street) > 25 && !$busqueda_repetida)
                $street = substr($street, 0, 25);
            $start = microtime(true);
        Log::debug('LLAMO CATASTRO '.$start);
//        Log::debug(mb_detect_encoding($street));
            $url = "http://ovc.catastro.meh.es/ovcservweb/OVCSWLocalizacionRC/OVCCallejero.asmx/Consulta_DNPLOC?Provincia=" . urlencode($province) . "&Municipio=" . urlencode($town) . "&Sigla=" . $rt . "&Calle=" . str_replace('%26%23039%3', '%27', urlencode(mb_convert_encoding($street, 'UTF-8'))) . "&Numero=" . $pnp . "&Bloque=" . $bloque . "&Escalera=" . $escalera . "&Planta=" . $planta . "&Puerta=" . $puerta;
            $obj_proxy = new Proxies('catastro');
            $ip_real = false;
            do {
                $ip = $obj_proxy->get();
                $obj_proxy->setUsed($ip);
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => array(
                        "cache-control: no-cache"
                    ),
                ));
                if ($ip) {
                    curl_setopt($curl, CURLOPT_PROXY, $ip);
                    curl_setopt($curl, CURLOPT_PROXYUSERPWD, "mrhc:QwEr1234");
                } else $ip_real = true;

                $response = curl_exec($curl);
                $info = curl_getinfo($curl);
                curl_close($curl);
                if (!$ip_real) {
                    $obj_proxy->unlock($info["http_code"]);
                }
            } while ($info["http_code"] != 200 && !$ip_real);
            Log::debug('TERMINA CATASTRO  '. (microtime(true) - $start));
//            Log::debug('CATASTRO URL ' . $url);
//        Log::debug('CATASTRO RESULTADO ' . print_r($response, true));
            $xml = new \SimpleXMLElement($response);
            if (isset($xml->control->cuerr) && isset($xml->callejero->calle) && !$busqueda_repetida) {
                //Direccion Incorrecta Corregimos
                //TODO
                $max_percent = 0;
                $aux_street = 'none';
                foreach ($xml->callejero->calle as $callejero) {
                    similar_text($street, (string)$callejero->dir->nv, $percent);
                    if ($percent > $max_percent) $aux_street = (string)$callejero->dir->nv;
                }
                $street = $aux_street;
                unset($xml);
                if ($street == 'none') return false;
                return self::searchCatastro($province, $town, $rt, $street, $pnp, $bloque, $escalera, $planta, $puerta, true);
            } elseif (isset($xml->control->cuerr)) {
                unset($xml);
                return false;
            } else {
                unset($xml);
                return $response;
            }
        } catch (\Exception $e) {
            if (!isset($url)) $url = 'url vacia';
//            Log::debug('Error al buscar en catastro ' . $url);
            return false;
        }
    }

    public static function getDatosCatastro($ine, $referencia_catastral)
    {
        try {
            $tipo15 = app('db')->connection('catastro')->table('cat_15_' . $ine)->select('*')
                ->where('referenciacatastral', $referencia_catastral)->first();
            //Log::debug(print_r($tipo15, true));
            $tipo11 = app('db')->connection('catastro')->table('cat_11_' . $ine)->select('*')
                ->where('parcelacatastral', substr($tipo15->referenciacatastral, 0, 14))->first();
            //Log::debug(print_r($tipo11, true));
            return array(
                'm2' => $tipo15->supelementosconstructivos,
                'lat' => $tipo11->latitude,
                'long' => $tipo11->longitude,
                'uso' => $tipo15->usodebieninmueble
            );
        } catch (\Exception $e) {
//            Log::debug(print_r($e, true));
            return array(
                'm2' => 0,
                'lat' => '',
                'long' => '',
                'uso' => ''
            );

        }
    }

    public static function normalize_pob_town($ine)
    {
        try {
            $res = app('db')->connection('catastro')->table('codine_pob')->select('*')
                ->where('codine', $ine)->first();
            return array('town' => utf8_encode($res->poblacion), 'province' => utf8_encode($res->provincia));
        } catch (\Exception $e) {
//            Log::debug(print_r($e, true));
            return false;

        }
    }

    private static function generate_query_14($parcela, $floor, $door, $masdeunnumbifiscal)
    {
        //Buscamos
        $querybool = new \Elastica\Query\BoolQuery();
        //Añadimos filtro genérico (.raw antiguo .keyword)
        $filter_parcela = new \Elastica\Query\Term();
        $filter_parcela->setTerm('parcela.raw', $parcela);
        $querybool->addFilter($filter_parcela);

        //Filtramos que los inmuebles que vengan sean viviendas
//        $filter_uso = new \Elastica\Query\Match();
//        $filter_uso->setFieldOperator('codigodestino','OR');
//        $filter_uso->setFieldQuery('codigodestino', 'A');

        $filter_uso = new \Elastica\Query\Wildcard();
        $filter_uso->setValue('codigodestino.raw', 'A*');
        $querybool->addMustNot($filter_uso->toArray());
        //Filtramos para que no sean elemntos comunes
        $filter_uso = new \Elastica\Query\Match();
        $filter_uso->setFieldQuery('modalidadreparto.raw', '');
        $querybool->addMust($filter_uso->toArray());
//        if (!empty($floor)) {
//            $field = 'planta';
//            $operator = 'and';
//            if ($floor == '00') $floor = 0;
//            else
//                $floor = ltrim($floor, '0');
//
//            if ((strlen($floor) < 2 && preg_match("/^[0-9]+$/",$floor)) || $floor == 0) {
//                $operator = 'or';
//                $floor = '0' . $floor . ' ' . $floor;
//            }
//            similar_text($floor, 'BAJO', $percent);
//            if ($percent > 40.1) {
//                $operator = 'or';
//                $floor = '00 BJ B BP1 BP2 B1 B2 B3 BG OB B0 BB BA BC BE BI BD BX PB';
//            }
//
//            $fuzziness = "AUTO";
//            $planta_match = new \Elastica\Query\Match();
//            $planta_match->setFieldQuery($field, $floor);
//            $planta_match->setFieldOperator($field, $operator);
//            $planta_match->setFieldFuzziness($field, $fuzziness);
//            $querybool->addMust($planta_match->toArray());
//        }

        $array_abc = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'Ñ', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'Y', 'Z', 'X');

        if ($masdeunnumbifiscal) {
            if (!empty($door)) {
                if ($door == '00') $door = 0;
                else
                    $door = ltrim($door, '0');
                $array_abc = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'Ñ', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'Y', 'Z', 'X');
                $field = 'puerta';
                $operator = 'and';
                if (preg_match("/^[0-9]+$/", $door) || $door == 0) {
                    $aux = '';
                    if ($door < 25 || $door == 0) {
                        $operator = 'or';
                        $aux = $array_abc[$door - 1];
                    }
                    if (strlen($door) < 2 || $door == 0) {
                        $operator = 'or';
                        $aux = trim('0' . $door . ' ' . $door . ' ' . $aux);
                    }
                    if (!empty($aux))
                        $door = $aux;
                } else {
                    //si es solo nº o letras entro
                    if (!preg_match('/(([0-9])+([A-Za-z])+)|(([A-Za-z])+([0-9])+)/', $door) && in_array(strtoupper(substr($door, 0, 1)), $array_abc)) {
                        $operator = 'or';
                        $aux = 1 + array_search(strtoupper(substr($door, 0, 1)), $array_abc);
                        if (strlen($aux) < 2) {
                            $operator = 'or';
                            $aux = '0' . $aux . ' ' . $aux;
                        }
                        $door = $door . ' ' . $aux;
                    }
                }
                $fuzziness = "AUTO";
                $puerta_match = new \Elastica\Query\Match();
                $puerta_match->setFieldQuery($field, $door);
                $puerta_match->setFieldOperator($field, $operator);
                $puerta_match->setFieldFuzziness($field, $fuzziness);
                $querybool->addMust($puerta_match->toArray());
            }
        }
        return [
            'query' => $querybool->toArray()
        ];

    }

    public static function check_with_14($geo_check, $floor, $door)
    {
        try {
            //Miramos si tiene mr
            $res = app('db')->connection('catastro')->table('cat_14_' . $geo_check['ine'])->select('*')
                ->where('parcelacatastral', $geo_check['parcela'])->where('modalidadreparto', '<>', '')->count();
            $modalidad_reparto = ($res > 0);

            // Tiene Division en 15?
            $res = app('db')->connection('catastro')->table('cat_15_' . $geo_check['ine'])->select('*')
                ->where('referenciacatastral', 'like', $geo_check['parcela'] . '%')->count();
            $division_15 = ($res > 1);

            $res = app('db')->connection('catastro')->table('cat_14_' . $geo_check['ine'])->select('numbifiscal')->distinct()
                ->where('parcelacatastral', $geo_check['parcela'])->count();
            $num_bifiscal_distintos = ($res > 1);

            if (!$modalidad_reparto && !$division_15) {
                $inmueble = app('db')->connection('catastro')->table('cat_15_' . $geo_check['ine'])->select('*')
                    ->where('referenciacatastral', 'like', $geo_check['parcela'] . '%')->first();
                $m2_15 = $inmueble->supelementosconstructivos;
                $inmuebles = app('db')->connection('catastro')->table('cat_14_' . $geo_check['ine'])->select('*')
                    ->where('parcelacatastral', $geo_check['parcela'])->get();
//            $flat = $house =
                $m2 = 0;
                foreach ($inmuebles as $inmueble) {
                    if ($m2_15 == 0 || empty($m2_15))
                        $m2 += $inmueble->suptotalimputada;

//                if (substr($inmueble->tipologiaconstructiva, 0, 3) == '011') $flat++;
//                elseif (substr($inmueble->tipologiaconstructiva, 0, 3) == '012') $house++;
                }
//            if ($flat > $house)
//                $geo_check['type'] = 'flat';
//            else if ($flat < $house)
                $geo_check['type'] = 'house';
//            else $geo_check['type'] = 'flat';

                //Media
                if (empty($m2_15) || $m2_15 == 0)
                    $geo_check['m2'] = $m2;

//            if ($geo_check['type'] == 'flat') {
//                $geo_check['m2'] = 0;
//            }
            } else {
//
//        var_dump($query);
                $query = self::generate_query_14($geo_check['parcela'], $floor, $door, $num_bifiscal_distintos);
//                Log::debug(print_r($query, true));
                $arr = self::elastica_search('catastro14', $geo_check['ine'], $query)->getResults();//->getAggregation("street_names")["buckets"];
                if (count($arr) < 1) {
                    $query = self::generate_query_14($geo_check['parcela'], $floor, '', $num_bifiscal_distintos);
                    $arr = self::elastica_search('catastro14', $geo_check['ine'], $query)->getResults();//->getAggregation("street_names")["buckets"];
                }
                if (count($arr) < 1) {
                    $query = self::generate_query_14($geo_check['parcela'], '', '', $num_bifiscal_distintos);
                    $arr = self::elastica_search('catastro14', $geo_check['ine'], $query)->getResults();//->getAggregation("street_names")["buckets"];
                }
//                Log::debug('RES CAT14' . print_r($arr, true));

                if (count($arr) == 1) {
                    //Exacto Miramos si los m2 del tipo15 es == 0
                    //var_dump($arr);

                    $inmueble = $arr[0]->getData();
                    $sub_str = substr($inmueble['tipologiacons'], 0, 3);
                    if ($sub_str == '011' && $division_15 && $modalidad_reparto) {
                        $geo_check['type'] = 'flat';
                    } elseif ($sub_str == '012' && $division_15 && !$modalidad_reparto) {
                        $geo_check['type'] = 'house';
                    } elseif ($sub_str == '011' && !$division_15 && $modalidad_reparto) {
                        $geo_check['type'] = 'flat';
                        $geo_check['m2'] = $inmueble['m2'] + self::calculam2($inmueble['m2'], $geo_check['ine'], $geo_check['parcela']);
                    } elseif ($sub_str == '012') {
                        $geo_check['type'] = 'house';
                    } else $geo_check['type'] = 'flat';

                } elseif (count($arr) > 1) {
//                    Log::debug('Entro + 1 resultado');
                    $flat = $house = $house_dupx = $m2 = 0;
                    foreach ($arr as $inmueble) {
                        $inmueble = $inmueble->getData();
                        $sub_str = substr($inmueble['tipologiacons'], 0, 3);
                        if ($sub_str == '011' && $division_15 && $modalidad_reparto) {
                            $flat++;
                        } elseif ($sub_str == '012' && $division_15 && !$modalidad_reparto) {
                            if (substr($inmueble['tipologiacons'], 0, 4) == '0122') {
                                $m2 += $inmueble['m2'];
                                $house_dupx++;
                            } else $house++;
                        } elseif ($sub_str == '011' && !$division_15 && $modalidad_reparto) {
                            $flat++;
                        } else {
                            if (substr($inmueble['tipologiacons'], 0, 4) == '0122') {
                                $house_dupx++;
                                $m2 += $inmueble['m2'];
                            } else $house++;
                        }
                    }
                    if ($flat > $house && $flat > $house_dupx) {
                        if ($geo_check['type'] == 'house') {
                            if (empty($floor) && empty($door))
                                $geo_check['level'] = 4;
                            elseif (empty($floor) && !empty($door))
                                $geo_check['level'] = 2;
                            else $geo_check['level'] = 3;
                        }
                        $geo_check['type'] = 'flat';
                    } else if ($flat < $house && $house > $house_dupx) $geo_check['type'] = 'house';
                    else if ($house_dupx > $flat && $house_dupx > $flat) {
                        $geo_check['type'] = 'house-dpx';
                        $geo_check['m2'] = $m2;
                    }

//                    Log::debug('FLAT ' . $flat);
//                    Log::debug('HOUSE ' . $house);
                    // ANTES $geo_check['level'] != 1
                    if ($geo_check['level'] > 2 && $geo_check['type'] == 'flat') {
                        $count_inm_total = $desv_tipica = $sum = $media = $m2 = $m2_totales = 0;

                        foreach ($arr as $inmueble) {
                            $inmueble = $inmueble->getData();
                            $m2_totales += $inmueble['m2'] + self::calculam2($inmueble['m2'], $geo_check['ine'], $geo_check['parcela']);
                        }
                        $media = $m2_totales / count($arr);

                        foreach ($arr as $inmueble) {
                            $inmueble = $inmueble->getData();
                            $sum += pow($inmueble['m2'] - $media, 2);
                        }
                        $desv_tipica = sqrt($sum / count($arr));
                        $upper_limit = $media + $desv_tipica;
                        $low_limit = $media - $desv_tipica;
                        foreach ($arr as $inmueble) {
                            $inmueble = $inmueble->getData();
                            if ($inmueble['m2'] <= $upper_limit && $inmueble['m2'] >= $low_limit) {
                                $m2 += $inmueble['m2'];
                                $count_inm_total++;
                            }
                        }

                        $geo_check['m2'] = $m2 / $count_inm_total;
                    }


                }
            }
            return $geo_check;
        } catch (\Exception $e) {

            return $geo_check;
        }
    }

    //$ine,$parcela
    private static function calculam2($m2, $ine, $parcela)
    {
//        $ine = '08019';
//        $m2 = 156;
//        $parcela = '7039404DF2873G';
        $sql = app('db')->connection('catastro')->table('cat_14_' . $ine)->select('suptotalimputada')
            ->where('parcelacatastral', $parcela)->where('modalidadreparto', '')->get();
        $m2_todas_viviendas = 0;
        foreach ($sql as $res) {
            $m2_todas_viviendas += $res->suptotalimputada;
        }

        $sql = app('db')->connection('catastro')->table('cat_14_' . $ine)->select('suptotalimputada')
            ->where('parcelacatastral', $parcela)->where('modalidadreparto', '<>', '')->get();
        $m2_elm_comunes = 0;
        foreach ($sql as $res) {
            $m2_elm_comunes += $res->suptotalimputada;
        }
        $coef_propiedad = ($m2 * 100) / $m2_todas_viviendas;
        $m2_sup_elm_comunes = ($coef_propiedad * $m2_elm_comunes) / 100;
        return $m2_sup_elm_comunes;
    }

    public static function search_address_fenosa($cp, $street, $number, $geo_check)
    {
        $response = self::curl_fenosa("https://contrata.gasnaturalfenosa.es/mascaraxpress/api/findCP.json?text=" . $cp);
        if (isset($response->status) && $response->status) {
            foreach ($response->data as $data) {
                $result = self::curl_fenosa("https://contrata.gasnaturalfenosa.es/mascaraxpress/api/findCUPSByAddress.json?cp=" . $data->cp . "&town=" . urlencode($data->town) . "&energy=luz&text=" . urlencode($street . ' ' . $number));
                if (isset($result->status) && $result->status) {
                    if (count($result->data) > 1) {
                        // Más de una direccion 'flat'
                        $geo_check['m2'] = 0;
                        $geo_check['type'] = 'flat';
                        $geo_check['level'] = 4;
                        break;
                    } elseif (count($result->data) == 1) {
                        $geo_check['m2'] = 0;
                        $geo_check['type'] = 'house';
                        $geo_check['level'] = 4;
                        break;
                    }
                }
            }
        }
        return $geo_check;
    }

    private static function curl_fenosa($url)
    {

        $obj_proxy = new Proxies('fenosa');
        $ip_real = false;
        do {
            $ip = $obj_proxy->get();
            $obj_proxy->setUsed($ip);
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_SSL_VERIFYPEER => FALSE,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache"
                ),
            ));
            if ($ip) {
                curl_setopt($curl, CURLOPT_PROXY, $ip);
                curl_setopt($curl, CURLOPT_PROXYUSERPWD, "mrhc:QwEr1234");
            } else $ip_real = true;

            $response = curl_exec($curl);
            $info = curl_getinfo($curl);
            curl_close($curl);
            if (!$ip_real) {
                $obj_proxy->unlock($info["http_code"]);
            }
        } while ($info["http_code"] != 200 && !$ip_real);

        return json_decode($response);
    }


    public static function here_geo_check($search, $geo_check, $is_flat)
    {
        $curl = curl_init();
        //m527868@mvrht.com
        //Pass: 123456789Abcd
        //Gratis
//        $app_id = 'BnboVFMxgw7vEvfqnjueL';
//        $app_code = 'wKwo7LGdj-9SCnfQectjMw';
//Premium CAMBIADO R.Marcos 26/04/2017
        $app_id = 'pmOhSJJcCHE3rCMYbZpk';
        $app_code = 'Xstyn-gQk5Y3urHQy-KVoQ';

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://geocoder.cit.api.here.com/6.2/geocode.json?searchtext=" . urlencode($search) . "&gen=9&app_id=" . $app_id . "&app_code=" . $app_code,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
            ),
        ));

        $response = curl_exec($curl);
//        $err = curl_error($curl);

        curl_close($curl);

        $otro_numero_calle = false;
        $response = json_decode($response);
//        Log::debug('HERE RESPONSE: ' . print_r($response, true));
        if ((isset($response->Response->View[0]->Result[0]->MatchQuality->City) || isset($response->Response->View[0]->Result[0]->MatchQuality->State) || isset($response->Response->View[0]->Result[0]->MatchQuality->County)) &&
            isset($response->Response->View[0]->Result[0]->MatchQuality->Street) &&
            isset($response->Response->View[0]->Result[0]->MatchQuality->HouseNumber)
        ) {

            if (isset($response->Response->View[0]->Result[0]->MatchQuality->City) && $response->Response->View[0]->Result[0]->MatchQuality->City > 0.5 &&
                $response->Response->View[0]->Result[0]->MatchQuality->Street[0] > 0.5 &&
                $response->Response->View[0]->Result[0]->MatchQuality->HouseNumber > 0.5
            ) {
                //Nivel 1
                $otro_numero_calle = false;
            } elseif (isset($response->Response->View[0]->Result[0]->MatchQuality->State) && $response->Response->View[0]->Result[0]->MatchQuality->State > 0.5 &&
                $response->Response->View[0]->Result[0]->MatchQuality->Street[0] > 0.5 &&
                $response->Response->View[0]->Result[0]->MatchQuality->HouseNumber > 0.5
            ) {
                //Nivel 1
                $otro_numero_calle = false;
            } else if (isset($response->Response->View[0]->Result[0]->MatchQuality->County) && $response->Response->View[0]->Result[0]->MatchQuality->County > 0.5 &&
                $response->Response->View[0]->Result[0]->MatchQuality->Street[0] > 0.5 &&
                $response->Response->View[0]->Result[0]->MatchQuality->HouseNumber > 0.5
            ) {
                //Nivel 1
                $otro_numero_calle = false;
            } else {
                //Nivel 5
                $otro_numero_calle = true;
            }
            if (isset($response->Response->View[0]->Result[0]->Location->DisplayPosition->Latitude))
                $geo_check['latitude'] = $response->Response->View[0]->Result[0]->Location->DisplayPosition->Latitude;
            if (isset($response->Response->View[0]->Result[0]->Location->DisplayPosition->Longitude))
                $geo_check['longitude'] = $response->Response->View[0]->Result[0]->Location->DisplayPosition->Longitude;


        } else $otro_numero_calle = true;

        if (isset($response->Response->View[0]->Result[0]->Address->Label))
            $geo_check['address'] = $response->Response->View[0]->Result[0]->Address->Label;
        if (isset($response->Response->View[0]->Result[0]->Address->County))
            $country = $response->Response->View[0]->Result[0]->Address->County;
        if (isset($response->Response->View[0]->Result[0]->Address->City))
            $city = $response->Response->View[0]->Result[0]->Address->City;
        if (isset($response->Response->View[0]->Result[0]->Address->Street))
            $street = $response->Response->View[0]->Result[0]->Address->Street;
        if (isset($response->Response->View[0]->Result[0]->Address->HouseNumber))
            $house_number = $response->Response->View[0]->Result[0]->Address->HouseNumber;


        if (isset($response->Response->View[0]->Result[0]->AdditionalData)) {
//            Log::debug('Additional Data here: ' . print_r($response->Response->View[0]->Result[0]->AdditionalData, true));
            foreach ($response->Response->View[0]->Result[0]->AdditionalData as $additionalData) {
                if ($additionalData->key == "houseNumberFallbackDifference")
                    if ($additionalData->value > 5) $otro_numero_calle = true; //otro numero
            }
        }

        if (!$otro_numero_calle) {
            //Nivel 1?
            //if ($is_flat) 
            $geo_check['level'] = 4;
            //else $geo_check['level'] = 1;
        } else $geo_check['level'] = 5;

        return $geo_check;

    }
}

//    public static function search14($ine,$parcela,$floor,$door)
//    {
//        $res = app('db')->connection('catastro')->table('cat_14_' . $ine)->select('*')
//            ->where('parcelacatastral', $parcela)->where('modalidadreparto', '<>', '')->count();
//
//        if ($res == 0) return 'house';
//
//        $res = app('db')->connection('catastro')->table('cat_14_' . $ine)->select('*')
//            ->where('parcelacatastral', $parcela)->where('codigodestino', 'V')->groupby('planta')->get();
////        if(count($res) == 1){
////            if(substr($res[0]->tipologiaconstructiva,0,3) == '011'){
////                return 'flat';
////            }else return 'house';
////        }
//        $aux = $score = array();
//        foreach ($res as $resp) {
//            similar_text($resp->planta, $floor, $percent);
//            $score[] = array('id' => $resp->id, 'score' => $percent);
//        }
//        foreach ($aux as $key => $row) {
//            $aux[$key] = $row['score'];
//        }
//        array_multisort($aux, SORT_DESC, $score);
//        unset($aux);
//    }
