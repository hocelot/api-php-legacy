<?php
/**
 * Created by PhpStorm.
 * User: a.luis
 * Date: 13/07/2016
 * Time: 10:31
 */

namespace App\Http\Models;

use stdClass;
use Log;

class UserModel {
	public static function wsdlProfile($id){
		$params_datos_cl = array(
			'val_as_base_url' =>  app('wsdl')->getBaseUrl(),
			'val_al_ter_codigo' => $id,
			'ref_astr_tgtercero' => '',
			'ref_as_mensaje' => ''
		);
        try {
            $result= new StdClass;
            $result->data = app('wsdl')->call_ws(app('wsdl')->getWsUrl('clientes'), 'damedatosclientemirating', $params_datos_cl);
            $result->status = 200;
            return $result;
        } catch (\Exception $f) {
            Log::error('Exception listQueries ' . $f);
            return ['status' => 400, 'exception' => $f, 'data'=>''];
        }


		//return app('wsdl')->call_ws(app('wsdl')->getWsUrl('clientes'), 'damedatosclientemirating', $params_datos_cl);
	}
	public static function sqlProfile($id){
		$slug = app('db')->connection('mr_test')->table('mr_cliente')->select('validar_email')->where('id', $id)->first();
		return ($slug->validar_email== 0);
	}

	public static function getType($client_id){
		$res = app('db')->connection('mr_test')->table('mr_cliente')->select('tipo')->where('id', $client_id)->first();
		return $res->tipo;
	}
}