<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 1/08/16
 * Time: 17:23
 */

namespace App\Http\Models;


class SocialModel
{
    public static function isInfojobMail($email)
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://www.infojobs.net/candidate/profile/check-email-registered.xhtml');
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
//        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'email=' . $email);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
        $result = curl_exec($ch);
        curl_close($ch);
//TODO FINISH
        return $result;
    }

    public static function getLinkedinInfo()
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.linkedin.com/v1/people-search:(people:(id,first-name,last-name,headline,picture-url,industry,positions:(id,title,summary,start-date,end-date,is-current,company:(id,name,type,size,industry,ticker)),educations:(id,school-name,field-of-study,start-date,end-date,degree,activities,notes)),num-results)?first-name=Julia&last-name=Verdejo",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "accept: */*",
                "accept-language: es,en-GB;q=0.8,en;q=0.6",
                "cache-control: no-cache",
                "content-type: application/json",
                "oauth_token: UfYScT_xC1W7YJO_N1IE9uZz4qQDly_Ch6Ug",
                "referer: https://api.linkedin.com/uas/js/xdrpc.html?v=0.0.1191-RC8.56523-1429",
                "user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36",
                "x-cross-domain-origin: https://mail.google.com",
                "x-http-method-override: GET",
                "x-li-format: json",
                "x-requested-with: IN.XDCall"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            /* Access_Token caducado
             * {
                  "errorCode": 0,
                  "message": "[unauthorized]. token expired 4308 seconds ago",
                  "requestId": "TCBQ7DAQTS",
                  "status": 401,
                  "timestamp": 1470071512807
                }
             */
            echo $response;
        }
    }


}