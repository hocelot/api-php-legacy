<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 12/07/16
 * Time: 20:07
 */

namespace App\Http\Models;

use Log;

class AddressModel
{


    /*
     *
     * WSDL Part
     *
     */

    public static function getPoblation($zip_code)
    {
        $params = AddressModel::prepareParams($zip_code);
        Log::debug(print_r($params, true));
        try {

            return app('wsdl')->call_ws(app('wsdl')->getWsUrl('admin'), 'damepoblacion', $params);
        } catch (\Exception $f) {
            Log::error('Exception getPoblation ' . $f);

            return ['status' => 100, 'exception' => $f];
        }
    }

//    public static function getIneCode($params)
//    {
//        $params_ine = array(
//            'val_as_base_url' => app('wsdl')->getBaseUrl(),
//            'as_cod_postal' => $params->zip_code,
//            'ref_as_mensaje' => '',
//            'ref_as_poblacion' => $params->town
//        );
//        Log::debug('COD INE' . print_r($params_ine, true));
//        try {
//            return app('wsdl')->call_ws(app('wsdl')->getWsUrl('admin'), 'damecodine', $params_ine);
//        } catch (\Exception $f) {
//            Log::error('Exception getIneCode ' . $f);
//
//            return ['status' => 100, 'exception' => $f];
//        }
//    }

    public static function getIneCode($params)
    {

        $pob = self::getPoblation($params->zip_code);
        if (!isset($pob->ref_astr_codpost->str_adcodpost_nuevo)) {
            return false;
        }

        //Array de poblaciones
        $params->cod_muns = [];
        foreach ($pob->ref_astr_codpost->str_adcodpost_nuevo as $arr) {
            $params->cod_muns[] = $arr->cod_poblacion;
        }

      //  Log::debug('POB stdclass  '.print_r($pob,true));
       // Log::debug('Municipios '.print_r($params->cod_muns,true));

        $cod_prov = substr($params->zip_code, 0, 2);

        $client = new \Elastica\Client(array(
            'host' => '192.168.82.10',
            'port' => 9200
        ));
        $busquedas = array('municipio', 'entidadcolectiva', 'entidadsingular', 'nucleo');
        $cont = 0;
        $arr = [];
        do {
            $query = [
                'query' => [
                    'bool' => [
                        'must' => [
                            'match' => [
                                $busquedas[$cont] => [
                                    "fuzziness" => "AUTO",
                                    "operator" => "and",
                                    "query" => $params->town,
                                ],
                            ],
                        ],
                        'filter' => [
                            ['term' => ["codprov" => $cod_prov]],
                            ['terms' => ["codmun" => $params->cod_muns]]
                        ]
                    ]
                ]
            ];
         //   Log::debug('query '.print_r($query,true));

            $query = \Elastica\Query::create($query);
            $search = new \Elastica\Search($client);
            $search->addType('towns')->addIndex('address');
            $arr = $search->search($query)->getResults();
            $cont++;
        } while (count($arr) == 0 && $cont < 4);

       // Log::debug('INE array  '.print_r($arr,true));

        if (count($arr) == 0 && $cont < 5) {
            return false;
        } else {
            //TODO ltrim por la base de datos antigüa

            return $arr[0]->getData()['codprov'] . $arr[0]->getData()['codmun'];
        }

    }

    public static function densityTown($zip_code)
    {
        $params = AddressModel::prepareParams($zip_code);
        try {
            return app('wsdl')->call_ws(app('wsdl')->getWsUrl('clientes'), 'mr_tipo_municipio', $params);
        } catch (\Exception $f) {
            Log::error('Exception densityTown ' . $f);

            return ['status' => 100, 'exception' => $f];
        }
    }


    /**
     * @param $postalcode
     * @return array
     */
    private static function prepareParams($zip_code)
    {
        return array(
            'val_as_base_url' => app('wsdl')->getBaseUrl(),
            'val_as_codpostal' => $zip_code,
            'ref_as_mensaje' => '',
            'ref_astr_codpost' => 0
        );
    }

    /*
     *
     * SQL Part
     *
     */

    public static function getRoadType($inecode)
    {
        try {
            $results = app('db')->connection('catastro')->table('cat_15_' . $inecode)->distinct()->select('tipodevia')->get();
        } catch (\Exception $e) {
            return 'Ocurrio un error parámetros incorrecto';
        }
        $roadtypes = array();
        foreach ($results as $result) {
            $text = UtilClass::convertRoadType($result->tipodevia);
            $roadtypes[] = array('id' => trim($result->tipodevia), 'road_name' => $text);
        }

        return $roadtypes;
    }

    public static function getStreets($params)
    {
        try {
            $results = app('db')->connection('catastro')->table('cat_15_' . $params->code)->distinct()->select('nombredevia')->where('tipodevia', $params->rt_id)
                ->where('nombredevia', 'like', '%' . mb_strtoupper($params->street_name,'UTF-8') . '%')->get();
        } catch (\Exception $e) {
            Log::error(print_r('Exception getStreets[AddressModel]', true));
            return 'Ocurrio un error parámetros incorrecto';
        }
        $streets = array();
        foreach ($results as $result) {
            if (!empty(trim($result->nombredevia))) {
          //      Log::debug(($result->nombredevia));
                $streets[] = (trim($result->nombredevia));

            }
        }
        //Log::debug(print_r($streets,true));
        return $streets;
    }

    public static function getNumber($params)
    {
//        Log::debug(print_r('getNumber AddressModel',true));
//        Log::debug(print_r($params,true));
        try {
            //mb_convert_encoding(mb_strtoupper($params->street_name,'UTF-8'),'UTF-8', 'ISO-8859-1')
            /*$results = app('db')->connection('catastro')->table('cat_15_' . $params->code)->distinct()->select('primernumeropolicia')->where('tipodevia', $params->rt_id)
                ->where('primernumeropolicia', 'like', '%' . mb_convert_encoding(mb_strtoupper($params->number,'UTF-8'),'UTF-8', 'ISO-8859-1') . '%')->where('nombredevia', $params->street_name)->get();*/
            $results = app('db')->connection('catastro')->table('cat_15_' . $params->code)->distinct()->select('primernumeropolicia')->where('tipodevia', $params->rt_id)
                ->where('nombredevia',mb_strtoupper($params->street_name,'UTF-8'))->get();
        } catch (\Exception $e) {
            Log::error(print_r('Execption getNumber[AddressModel]', true));
            return 'Ocurrio un error parámetros incorrecto';
		   //return false;
        }
        $streetnumbers = array();
        foreach ($results as $result) {
            if (!empty(trim($result->primernumeropolicia))) {
//                Log::debug(($result->primernumeropolicia));
                $streetnumbers[] = trim($result->primernumeropolicia);
            }
        }
        if (isset($streetnumbers) && !empty($streetnumbers)) {
            sort($streetnumbers, 0);
        }
        return $streetnumbers;
    }

    public static function getLetter($params)
    {
        //Si solo hay letras vacias en ese numero..devolvemos el array en vacío
        $empty = true;
        $params->letter = self::isEmpty($params->letter);
        try {
            $results = app('db')->connection('catastro')->table('cat_15_' . $params->code)->distinct()->select('primeraletra')->where('tipodevia', $params->rt_id)
                ->where('primernumeropolicia', mb_convert_encoding(mb_strtoupper($params->number,'UTF-8'),'UTF-8', 'ISO-8859-1'))->where('nombredevia', mb_strtoupper($params->street_name,'UTF-8'))->get();
        } catch (\Exception $e) {
            Log::debug(print_r('OCURRIO UNA EXCEPCION [getLetter Model]', true));
            // return 'Ocurrio un error parámetros incorrecto';
		   return false;
        }
        $letters = array();
        foreach ($results as $result) {
            if (!empty($result->primeraletra)) {
                $empty = false;
            }
            $letters[] = trim($result->primeraletra);
        }
        if ($empty) {
            $letters = '';
        }
        if (isset($letters) && !empty($letters)) {
            sort($letters, 0);
        }
        return $letters;

    }

    public static function getKm($params)
    {
        //Si solo hay km vacio en ese numero..devolvemos el array en vacío
        $empty = true;
        $params->letter = self::isEmpty($params->letter);
        $params->km = self::isEmpty($params->km);
        try {
            $results = app('db')->connection('catastro')->table('cat_15_' . $params->code)->distinct()->select('kilometro')->where('tipodevia', $params->rt_id)
                ->where('primernumeropolicia', mb_convert_encoding(mb_strtoupper($params->number,'UTF-8'),'UTF-8', 'ISO-8859-1'))->where('nombredevia', mb_strtoupper($params->street_name,'UTF-8'));
            if (isset($params->letter) && !empty($params->letter)) {
                $results = $results->where('primeraletra',mb_convert_encoding(mb_strtoupper($params->letter,'UTF-8'),'UTF-8', 'ISO-8859-1'));
            }
            $results = $results->where('kilometro', 'like', '%' . mb_convert_encoding(mb_strtoupper($params->km,'UTF-8'),'UTF-8', 'ISO-8859-1') . '%')->get();
        } catch (\Exception $e) {
            // return 'Ocurrio un error parámetros incorrecto';
		   return false;
        }
        $kms = array();
        foreach ($results as $result) {
            if (!empty($result->kilometro)) {
                $empty = false;
            }
            $kms[] = trim($result->kilometro);
        }
        if ($empty) {
            $kms = '';
        }
        if (isset($kms) && !empty($kms)) {
            sort($kms, 0);
        }

        return $kms;

    }

    public static function getBlock($params)
    {
        //Si solo hay bloques en vacio en ese numero..devolvemos el array en vacío
        $empty = true;
        $params->letter = self::isEmpty($params->letter);
        $params->km = self::isEmpty($params->km);
        $params->block = self::isEmpty($params->block);
        try {
            $results = app('db')->connection('catastro')->table('cat_15_' . $params->code)->distinct()->select('bloque')->where('tipodevia', $params->rt_id)
                ->where('primernumeropolicia', mb_convert_encoding(mb_strtoupper($params->number,'UTF-8'),'UTF-8', 'ISO-8859-1'))->where('nombredevia', mb_strtoupper($params->street_name,'UTF-8'));
            if (isset($params->letter) && !empty($params->letter)) {
                $results = $results->where('primeraletra',mb_convert_encoding(mb_strtoupper($params->letter,'UTF-8'),'UTF-8', 'ISO-8859-1'));
            }
            if (isset($params->km) && !empty($params->km)) {
                $results = $results->where('kilometro', mb_convert_encoding(mb_strtoupper($params->km,'UTF-8'),'UTF-8', 'ISO-8859-1'));
            }
            $results = $results->where('bloque', 'like', '%' . mb_convert_encoding(mb_strtoupper($params->block,'UTF-8'),'UTF-8', 'ISO-8859-1') . '%')->get();
        } catch (\Exception $e) {
            // return 'Ocurrio un error parámetros incorrecto';
		   return false;
        }
        $blocks = array();
        foreach ($results as $result) {
            if (!empty($result->bloque)) {
                $empty = false;
            }
            $blocks[] = trim($result->bloque);
        }
        if ($empty) {
            $blocks = '';
        }
        if (isset($blocks) && !empty($blocks)) {
            sort($blocks, 0);
        }

        return $blocks;

    }

    public static function getStairs($params)
    {
        //Si solo hay escaleras en vacio en ese numero..devolvemos el array en vacío
        $empty = true;
        $params->letter = self::isEmpty($params->letter);
        $params->km = self::isEmpty($params->km);
        $params->block = self::isEmpty($params->block);
        $params->stairs = self::isEmpty($params->stairs);
        //Log::debug(print_r($params,true));
        try {
            $results = app('db')->connection('catastro')->table('cat_15_' . $params->code)->distinct()->select('escalera')->where('tipodevia', $params->rt_id)
                ->where('primernumeropolicia', mb_convert_encoding(mb_strtoupper($params->number,'UTF-8'),'UTF-8', 'ISO-8859-1'))->where('nombredevia', mb_strtoupper($params->street_name,'UTF-8'));
            if (isset($params->letter) && !empty($params->letter)) {
                $results = $results->where('primeraletra',mb_convert_encoding(mb_strtoupper($params->letter,'UTF-8'),'UTF-8', 'ISO-8859-1'));
            }
            if (isset($params->km) && !empty($params->km)) {
                $results = $results->where('kilometro', mb_convert_encoding(mb_strtoupper($params->km,'UTF-8'),'UTF-8', 'ISO-8859-1'));
            }
            if (isset($params->block) && !empty($params->block)) {
                $results = $results->where('bloque', mb_convert_encoding(mb_strtoupper($params->block,'UTF-8'),'UTF-8', 'ISO-8859-1'));
            }
            $results = $results->where('escalera', 'like', '%' . mb_convert_encoding(mb_strtoupper($params->stairs,'UTF-8'),'UTF-8', 'ISO-8859-1') . '%')->get();
        } catch (\Exception $e) {
            // return 'Ocurrio un error parámetros incorrecto';
		   return false;
        }
        $stairs = array();
        foreach ($results as $result) {
            if (!empty($result->escalera)) {
                $empty = false;
            }
            $stairs[] = trim($result->escalera);
        }
        if ($empty) {
            $stairs = '';
        }
        if (isset($stairs) && !empty($stairs)) {
            sort($stairs, 0);
        }
        //Log::debug(print_r($stairs,true));
        return $stairs;

    }

    public static function getFloor($params)
    {
        //Si solo hay plantas en vacio en ese numero..devolvemos el array en vacío
        $empty = true;
        $params->letter = self::isEmpty($params->letter);
        $params->km = self::isEmpty($params->km);
        $params->block = self::isEmpty($params->block);
        $params->stairs = self::isEmpty($params->stairs);
        $params->floor = self::isEmpty($params->floor);
        try {
			//Log::debug(print_r($params, true));
            $results = app('db')->connection('catastro')->table('cat_15_' . $params->code)->distinct()->select('planta')->where('tipodevia', $params->rt_id)
                ->where('primernumeropolicia', mb_convert_encoding(mb_strtoupper($params->number,'UTF-8'),'UTF-8', 'ISO-8859-1'))->where('nombredevia', mb_strtoupper($params->street_name,'UTF-8'));
            if (isset($params->letter) && !empty($params->letter)) {
                $results = $results->where('primeraletra',mb_convert_encoding(mb_strtoupper($params->letter,'UTF-8'),'UTF-8', 'ISO-8859-1'));
            }
            if (isset($params->km) && !empty($params->km)) {
                $results = $results->where('kilometro', mb_convert_encoding(mb_strtoupper($params->km,'UTF-8'),'UTF-8', 'ISO-8859-1'));
            }
            if (isset($params->block) && !empty($params->block)) {
                $results = $results->where('bloque', mb_convert_encoding(mb_strtoupper($params->block,'UTF-8'),'UTF-8', 'ISO-8859-1'));
            }
            if (isset($params->stairs) && !empty($params->stairs)) {
                $results = $results->where('escalera', mb_convert_encoding(mb_strtoupper($params->stairs,'UTF-8'),'UTF-8', 'ISO-8859-1'));
            }
            $results = $results->where('planta', 'like', '%' . mb_convert_encoding(mb_strtoupper($params->floor,'UTF-8'),'UTF-8', 'ISO-8859-1') . '%')->get();
        } catch (\Exception $e) {
            Log::error(print_r('Exception AddressModel[getFloor]', true));
			// return 'Ocurrio un error parámetros incorrecto';
		   return false;
        }
        $floors = array();
        foreach ($results as $result) {
            if (!empty($result->planta)) {
                $empty = false;
            }
            $floors[] = trim($result->planta);
        }
        if ($empty) {
            $floors = '';
        }
        if (isset($floors) && !empty($floors)) {
            sort($floors, 0);
        }
//		Log::debug(print_r('PLANTA ->', true));
//		Log::debug(print_r($floors, true));
        return $floors;
    }

    public static function getDoor($params)
    {
        //Si solo hay puertas en vacio en ese numero..devolvemos el array en vacío
        $empty = true;
        $params->letter = self::isEmpty($params->letter);
        $params->km = self::isEmpty($params->km);
        $params->block = self::isEmpty($params->block);
        $params->stairs = self::isEmpty($params->stairs);
        $params->floor = self::isEmpty($params->floor);
        $params->door = self::isEmpty($params->door);
        Log::debug(print_r($params,true));
        try {
            $results = app('db')->connection('catastro')->table('cat_15_' . $params->code)->distinct()->select('puerta')->where('tipodevia', mb_convert_encoding(mb_strtoupper($params->rt_id,'UTF-8'),'UTF-8', 'ISO-8859-1'))
                ->where('primernumeropolicia', mb_convert_encoding(mb_strtoupper($params->number,'UTF-8'),'UTF-8', 'ISO-8859-1'))->where('nombredevia', utf8_decode($params->street_name));
            if (isset($params->letter) && !empty($params->letter)) {
                $results = $results->where('primeraletra',mb_convert_encoding(mb_strtoupper($params->letter,'UTF-8'),'UTF-8', 'ISO-8859-1'));
            }
            if (isset($params->km) && !empty($params->km)) {
                $results = $results->where('kilometro', mb_convert_encoding(mb_strtoupper($params->km,'UTF-8'),'UTF-8', 'ISO-8859-1'));
            }
            if (isset($params->block) && !empty($params->block)) {
                $results = $results->where('bloque', mb_convert_encoding(mb_strtoupper($params->block,'UTF-8'),'UTF-8', 'ISO-8859-1'));
            }
            if (isset($params->stairs) && !empty($params->stairs)) {
                $results = $results->where('escalera', mb_convert_encoding(mb_strtoupper($params->stairs,'UTF-8'),'UTF-8', 'ISO-8859-1'));
            }
            if (isset($params->floor) && !empty($params->floor)) {
                $results = $results->where('planta', mb_convert_encoding(mb_strtoupper($params->floor,'UTF-8'),'UTF-8', 'ISO-8859-1'));
            }
            $results = $results->where('puerta', 'like', '%' .mb_convert_encoding(mb_strtoupper($params->door,'UTF-8'),'UTF-8', 'ISO-8859-1') . '%')->get();
//            $results = $results->where('puerta','like','% %')->get();
        } catch (\Exception $e) {
            // return 'Ocurrio un error parámetros incorrecto';
            Log::error(print_r('Exception AddressModel[getDoor]', true));
            //Log::error(print_r($e, true));
            return false;
        }
        $doors = array();
        foreach ($results as $result) {
            if (!empty($result->puerta)) {
                $empty = false;
            }
            $doors[] = trim($result->puerta);
        }
        if ($empty) {
            $doors = '';
        }
        if (isset($doors) && !empty($doors)) {
            sort($doors, 0);
        }

        return $doors;
    }

    public static function getTownId($id)
    {
        $result = app('db')->connection('apidb')->table('comunidades')->select('id_comunidad')->where('id', $id)->first();
        return $result->id_comunidad;
    }
    private static function isEmpty($param){
        if($param == 'EMPTY'){
            return '';
        }else{
            return ($param);
        }

    }
}
