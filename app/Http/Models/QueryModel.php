<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 19/06/16
 * Time: 19:43
 */

namespace App\Http\Models;

use Log;
use StdClass;

class QueryModel
{
    public static function listQueries($id)
    {
        $params = self::prepareParams($id);
        try {
            $result = new stdClass();
            $result->data = app('wsdl')->call_ws(app('wsdl')->getWsUrl('clientes'), 'hc_consultas', $params);
            $result->status = 200;
            if ($result->data->hc_consultasResult == 0){
                Log::debug(print_r('ERROR [listQueries Model] '. $result->data->ref_as_mensaje, true));
                return $result->status = 400;
            }
            Log::debug(print_r('RECIBO ANDRES CONSULTAS', true));
            Log::debug(print_r($result, true));
            return $result;
        } catch (\Exception $f) {
            Log::error('Exception [listQueries Model] ' . $f);
            return $result->status = 400;
        }
    }
    public static function getDataProfile($id)
    {
        $params = self::prepareParams($id);
        try {
            $result = new stdClass();
            $result->data = app('wsdl')->call_ws(app('wsdl')->getWsUrl('clientes'), 'damedatosclientemirating', $params);
            $result->status = 200;
            if ($result->data->damedatosclientemiratingResult == 0){
                Log::debug(print_r('ERROR [getDataProfile Model] '. $result->data->ref_as_mensaje, true));
                $result->status = 400;
                return $result;
            }
            return $result;
        } catch (\Exception $f) {
            Log::error('Exception [getDataProfile Model] ' . $f);
            $result->status = 400;
            return $result;
        }
    }

    public static function getDataQuery($id)
    {
        $params = QueryModel::prepareGetRate($id);
        try {
            $result = new stdClass();
            $result->data = app('wsdl')->call_ws(app('wsdl')->getWsUrl('clientes'), 'hc_consulta_evaluaciones', $params);
            $result->status = 200;
            if ($result->data->hc_consulta_evaluacionesResult == 0){
                Log::debug(print_r('ERROR [getDataQuery Model] '. $result->data->ref_as_mensaje, true));
                $result->status = 400;
                return $result;
            }
            return $result;
        } catch (\Exception $f) {
            Log::error('Exception [getDataQuery Model] ' . $f);
            $result->status = 400;
            return $result;
        }
    }
    public static function prepareGetRate($id)
    {
        $res['val_as_base_url'] = app('wsdl')->getBaseUrl();
        $res['val_ad_ter_codigo'] = $id;
        $res['val_astr_hc_id_check'] = array(1 => 'qwe');
        $res['val_astr_hc_id_fraud'] = array(1 => 'qwe');
        $res['val_astr_hc_geo_check'] = array(1 => 'qwe');
        $res['val_astr_hc_geo_fraud'] = array(1 => 'qwe');
        $res['val_asrt_hc_eval'] = array(1 => 'qwe');
        $res['ref_as_mensaje'] = '';
        return $res;
    }
    public static function prepareParams($id)
    {
        $res['val_as_base_url'] = app('wsdl')->getBaseUrl();
        $res['val_asrt_tgmiratingeval'] = array(1 => 'qwe');
        $res['val_ad_cliente'] = $id;
        return $res;
    }
}