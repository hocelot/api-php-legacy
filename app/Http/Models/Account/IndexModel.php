<?php
/**
 * Created by PhpStorm.
 * User: DC
 * Date: 05/01/17
 * Time: 16:08
 */

namespace App\Http\Models\Account;

use Log;
use StdClass;

class IndexModel
{

    public static function getIndex($id)
    {
        $params = self::prepareParams($id);
        try {
            $result = new stdClass();
            $result->data = app('wsdl')->call_ws(app('wsdl')->getWsUrl('clientes'), 'hc_historico_consultas', $params);
            $result->status = 200;
            if ($result->data->hc_historico_consultasResult == 0){
                Log::debug(print_r('ERROR [getIndex Model] '. $result->data->ref_as_mensaje, true));
                $result->status = 400;
                return $result;
            }
            //Log::debug(print_r('MODEL INDEX', true));
            //Log::debug(print_r($result, true));
            return $result;
        } catch (\Exception $f) {
            Log::error('Exception [getIndex Model] ' . $f);
            $result->status = 400;
            return $result;
        }
    }
    public static function getValMap($id)
    {
        $params = self::prepareParamsMap($id);
        try {
            $result = new stdClass();
            $result->data = app('wsdl')->call_ws(app('wsdl')->getWsUrl('clientes'), 'hc_mapa', $params);
            $result->status = 200;
            if ($result->data->hc_mapaResult == 0){
                Log::debug(print_r('ERROR [getValMap Model] '. $result->data->ref_as_mensaje, true));
                $result->status = 400;
                return $result;
            }
            return $result;
        } catch (\Exception $f) {
            Log::error('Exception [getValMap Model] ' . $f);
            $result->status = 400;
            return $result;
        }
    }
    public static function prepareParams($id)
    {
        $res['val_as_base_url'] = app('wsdl')->getBaseUrl();
        $res['val_al_cliente'] = $id;
        $res['ref_as_total'] = '';
        $res['ref_as_total_hoy'] = '';
        $res['ref_as_id'] = '';
        $res['ref_as_geo'] = '';
        $res['val_astr_historico_consultas'] = array();
        $res['val_astr_grafico_barras'] = array();
        $res['ref_as_mensaje'] = '';
        return $res;
    }
    public static function prepareParamsMap($id)
    {
        $res['val_as_base_url'] = app('wsdl')->getBaseUrl();
        $res['val_al_cliente'] = $id;
        $res['val_astr_historico_comunidades'] = array();
        $res['ref_as_mensaje'] = '';
        return $res;
    }
}