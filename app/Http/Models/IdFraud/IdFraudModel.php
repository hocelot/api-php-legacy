<?php

/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 4/11/16
 * Time: 10:46
 */
namespace App\Http\Models\IdFraud;
use stdClass;

class IdFraudModel
{
    public static function save($ter_codigo,$ip,$existemail,$alias,$score,$cliente,$origen)
    {
        $str_mr_id_fraud = new stdClass();
        $str_mr_id_fraud->ter_codigo = $ter_codigo;
        $str_mr_id_fraud->fecha = date('c');
        $str_mr_id_fraud->ip = $ip;
        $str_mr_id_fraud->resultado = $existemail;
        $str_mr_id_fraud->usuario = '-';
        $str_mr_id_fraud->id_fraud = $score;
        $str_mr_id_fraud->nick = $alias;
        $str_mr_id_fraud->cliente = $cliente;
        $str_mr_id_fraud->origen = $origen;

        $val_astr_mr_id_fraud['str_mr_id_fraud'] = $str_mr_id_fraud;
        $params_calc_mirating = array(
            'val_as_base_url' => app('wsdl')->getBaseUrl(),
            'val_astr_mr_id_fraud' => $val_astr_mr_id_fraud,
            'ref_as_mensaje' => ''
        );
        try {
//            Log::debug('Envio mr_id_check');
//            Log::debug(print_r($params_calc_mirating, true));
            return app('wsdl')->call_ws(app('wsdl')->getWsUrl('clientes'), 'mr_id_fraud', $params_calc_mirating);
        } catch (\Exception $f) {
            return ['status' => 100, 'exception' => $f];
        }
    }
}