<?php

/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 30/07/16
 * Time: 9:12
 */
namespace App\Http\Models\Admin;

use App\Http\Models\AuthModel;
use App\Http\Models\UtilClass;
use Log;
use PDOException;
use stdClass;

class ClientsModel
{
    public static function getClients($params)
    {
        $db_test = app('db')->connection('apidb')->table('clients')->select('*');
        $db_test2 = app('db')->connection('oauth')->table('oauth_clients')->select('*');
        $rows = 10;
        $current = 1;
        $limit_l = ($current * $rows) - ($rows);
        $limit_h = $limit_l + $rows;
        $nRows = count($db_test2->get());
        if (isset($params['rowCount'])) {
            $rows = $params['rowCount'];
        }
        if (isset($params['sort']) && is_array($params['sort'])) {
            foreach ($params['sort'] as $key => $value) {
                if($key == 'name'){
                    $db_test2= $db_test2->orderBy($key, $value);
                }else{
                    $db_test = $db_test->orderBy($key, $value);
                }
            }
        }
        if (isset($params['current'])) {
            $current = $params['current'];
            $limit_l = ($current * $rows) - ($rows);
            $limit_h = $rows;
        }
        if ($rows != -1) {
            // Al reves??
            $db_test = $db_test->take($limit_h)->skip($limit_l);
        }
        $clients = $db_test->get();
        $clients2 = $db_test2->get();
        //Log::debug(print_r($clients, true));
        $aux = array();
        foreach ($clients as $tabla1){
            foreach ($clients2 as $tabla2){
                $aux1 = new stdClass();
                if($tabla1->id == $tabla2->ter_codigo){
                    $aux1->id =$tabla1->id;
                    $aux1->email =$tabla1->email;
                    $aux1->name =$tabla2->name;
                    $aux1->site =$tabla1->site;
                    $aux1->joinDate =$tabla1->join_date;
//                    $aux1->lastLogin =$tabla2->updated_at;
                }else{
                    $aux1->id = false;
                }
                if($aux1->id) {
                    $aux[] = $aux1;
                }
            }
        }
        $json = $aux;

        if (isset($params['rowCount'])) //Means we're using bootgrid library
            return ['current' => $current, "rowCount" => $rows, "rows" => $json, "total" => $nRows];
        else
            return $json; //Just plain vanillat JSON output
    }
    public static function delClient($params){
        $aux = new stdClass();
        //$aux->clientPerm = array();
        try {
            app('db')->connection('apidb')->table('clients')->where('id', $params['id'])->delete();
            $aux->clientSecret = app('db')->connection('oauth')->table('oauth_clients')->select('*')->where('ter_codigo', $params['id'])->first();
            app('db')->connection('oauth')->table('oauth_clients_keys')->where('id', $aux->clientSecret->id)->delete();
            app('db')->connection('oauth')->table('oauth_clients')->where('ter_codigo', $params['id'])->delete();
            $aux->countPerm = count(app('db')->connection('oauth')->table('oauth_client_scopes')->select('scope_id')->where('client_id', $aux->clientSecret->id)->distinct()->get());
            app('db')->connection('apidb')->table('last_login')->where('id', $params['id'])->delete();
            for ($i=0; $aux->countPerm > $i; $i++){
                app('db')->connection('oauth')->table('oauth_client_scopes')->select('scope_id')->where('client_id', $aux->clientSecret->id)->delete();
            }
            return true;
        }catch (PDOException $e){
            Log::error(print_r('ERROR BORRAR CLIENTE ClientsModel[delClient]', true));
           //Log::error(print_r($e, true));
            return false;
        }
    }
    public static function editClient($params){


//        $aux = new stdClass();
//        try {
//            app('db')->connection('apidb')->table('clients')->where('id', $params['id'])->delete();
//            $aux->clientSecret = app('db')->connection('oauth')->table('oauth_clients')->select('*')->where('ter_codigo', $params['id'])->first();
//            app('db')->connection('oauth')->table('oauth_clients_keys')->where('id', $aux->clientSecret->id)->delete();
//            app('db')->connection('oauth')->table('oauth_clients')->where('ter_codigo', $params['id'])->delete();
//            $aux->countPerm = count(app('db')->connection('oauth')->table('oauth_client_scopes')->select('scope_id')->where('client_id', $aux->clientSecret->id)->distinct()->get());
//            app('db')->connection('apidb')->table('last_login')->where('id', $params['id'])->delete();
//            for ($i=0; $aux->countPerm > $i; $i++){
//                app('db')->connection('oauth')->table('oauth_client_scopes')->select('scope_id')->where('client_id', $aux->clientSecret->id)->delete();
//            }
//            return true;
//        }catch (PDOException $e){
//            Log::error(print_r('ERROR EDITAR CLIENTE ClientsModel[editClient]', true));
//            //Log::error(print_r($e, true));
//            return false;
//        }
    }
    public static function viewClient($params){
//        Log::debug(print_r($params, true));
        $aux = new stdClass();
        //$aux->clientPerm = array();
//        try {
            $aux->client = app('db')->connection('apidb')->table('clients')->select('*')->where('id', $params['id'])->first();
            $aux->clientSecret = app('db')->connection('oauth')->table('oauth_clients')->select('*')->where('ter_codigo', $params['id'])->first();
            $aux->clientPerm = app('db')->connection('oauth')->table('oauth_client_scopes')->select('scope_id')->where('client_id', $aux->clientSecret->id)->distinct()->get();
            $aux->clientKeys = app('db')->connection('oauth')->table('oauth_clients_keys')->select('*')->where('id', $aux->clientSecret->id)->first();
            $aux->login = app('db')->connection('apidb')->table('last_login')->where('id', $params['id'])->first();
//        }catch (\Exception $e){
//            Log::debug(print_r($e, true));
//            $aux->login = new stdClass();
//            $aux->login->ip = 'NC';
//            $aux->login->fecha = 'NC';
//            $aux->login->navegador = 'NC';
//        }
        if(empty($aux->login)){
            $aux->login = new stdClass();
            $aux->login->ip = 'NC';
            $aux->login->fecha = 'NC';
            $aux->login->navegador = 'NC';
        }
        return $aux;
    }
    public static function setHocelotClient($params, $scope, $keys)
    {
        $response = new stdClass();
        if (!empty($keys)) {
            $params = (object)$params;
            $params->user->client_id = UtilClass::randVar(39);
            $result = AuthModel::join($params);
            $response->status = 200;
            $apidb_clients = array(
                'id' => $result->val_astr_tgtercero->str_tgtercero[0]->ter_codigo,
                'email' => $params->user->email,
                'password' => $params->user->password,
                'site' => 'H',
                'join_date' => date('c')
            );
            try {
                app('db')->connection('apidb')->table('clients')->insert($apidb_clients);
            }catch (PDOException $e){
                $response->data = 'Usuario existente en la aplicación.';
                $response->status = 400;
                return $response;
            }
            $oauth_clients = array(
                'id' => $params->user->client_id,
                'secret' => UtilClass::randVar(39),
                'name' => $params->user->business_name,
                'created_at' => date('c'),
                'updated_at' => date('c'),
                'ter_codigo' => $result->val_astr_tgtercero->str_tgtercero[0]->ter_codigo
            );
            try {
                app('db')->connection('oauth')->table('oauth_clients')->insert($oauth_clients);
            }catch (PDOException $e){
                $response->data = 'Usuario ya registrado. Ter_cod existente';
                $response->status = 400;
                return $response;
            }
            if (!empty($scope)) {
                foreach ($scope as $sco) {
                    $oauth_client_scopes = array(
                        'client_id' => $oauth_clients['id'],
                        'scope_id' => $sco,
                        'created_at' => date('c'),
                        'updated_at' => date('c')
                    );
                    try {
                        app('db')->connection('oauth')->table('oauth_client_scopes')->insert($oauth_client_scopes);
                    } catch (PDOException $e) {
                        $response->data = 'Error al dar permisos al usuario en bbdd.';
                        $response->status = 400;
                        return $response;
                    }

                }
            }
            $oauth_clients_keys = array(
                'id' => $oauth_clients['id'],
                'private_key' => base64_encode($keys['private_key']),
                'public_key' => base64_encode($keys['public_key'])
            );
            try {
                app('db')->connection('oauth')->table('oauth_clients_keys')->insert($oauth_clients_keys);
            }catch (PDOException $e){
                $response->data = 'Error al introducir las claves (Pub/Priv) en bbdd.';
                $response->status = 400;
                return $response;
            }
        } else {
            $response->data = 'Error, No se han introducido permisos válidos en ClientsModel[setHocelotClient].';
            $response->status = 400;
        }
        return $response;
    }
}