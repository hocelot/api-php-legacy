<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 1/08/16
 * Time: 20:17
 */

namespace App\Http\Models;


class CatastroModel
{
    /*
     * README                 <---------------------------------------------------------------------
     * RESTRICCION DE 3600 peticiones por hora.
     *
     */




    /*
     * RC: Obligatorio. Referencia catastral. Debe tener 14 posiciones que corresponden a la finca.
     * http://ovc.catastro.meh.es/ovcservweb/OVCSWLocalizacionRC/OVCCoordenadas.asmx?op=Consulta_CPMRC
    */
    public static function coordRC(){
        //SRS = EPSG:4326 ...... SIEMPRE PARA EL FORMATO DE COORDENADAS
        //Provincia y Municipio optativos
        $page = 'http://ovc.catastro.meh.es/ovcservweb/OVCSWLocalizacionRC/OVCCoordenadas.asmx/Consulta_CPMRC?Provincia=string&Municipio=string&SRS=string&RC=string';
        $xml_response = self::callCatastro($page);
    }



    /*
     * Comprueba existencia de via
     *http://ovc.catastro.meh.es/ovcservweb/OVCSWLocalizacionRC/OVCCallejero.asmx?op=ConsultaVia
     */

    public static function isValidStreetName(){
        $page = 'http://ovc.catastro.meh.es/ovcservweb/OVCSWLocalizacionRC/OVCCallejero.asmx/ConsultaVia?Provincia=string&Municipio=string&TipoVia=string&NombreVia=string';
        $xml_response = self::callCatastro($page);

    }


    /*
     * Municipio Opcional
     * Comprueba si un municipio es correcto
     * http://ovc.catastro.meh.es/ovcservweb/OVCSWLocalizacionRC/OVCCallejero.asmx?op=ConsultaMunicipio
     */
    public static function validTown(){
        $page = 'http://ovc.catastro.meh.es/ovcservweb/OVCSWLocalizacionRC/OVCCallejero.asmx/ConsultaMunicipio?Provincia=string&Municipio=string';
        $xml_response = self::callCatastro($page);
    }


    /**
     * Get datos a partir de su referencia catastral
     * @return datos_vivienda si existe
     *http://ovc.catastro.meh.es/ovcservweb/OVCSWLocalizacionRC/OVCCallejero.asmx?op=Consulta_DNPRC
     */
    public static function getDataByRC(){
        // A partir de numero son opcionales
        $page = 'http://ovc.catastro.meh.es/ovcservweb/OVCSWLocalizacionRC/OVCCallejero.asmx/Consulta_DNPRC?Provincia=string&Municipio=string&RC=string';
        $xml_response = self::callCatastro($page);

    }


    /**
     * Hasta numero
     * @return referenciaCatastral si existe
     *
     */
    public static function existsNumber(){

        $page = 'http://ovc.catastro.meh.es/ovcservweb/OVCSWLocalizacionRC/OVCCallejero.asmx/ConsultaNumero?Provincia=string&Municipio=string&TipoVia=string&NomVia=string&Numero=string';
        $xml_response = self::callCatastro($page);

    }

    /**
     * Hasta numero Obligatorio
     * @return datos_vivienda si existe
     * http://ovc.catastro.meh.es/ovcservweb/OVCSWLocalizacionRC/OVCCallejero.asmx?op=Consulta_DNPLOC
     */
    public static function getM2Address(){
        // A partir de numero son opcionales
        $page = 'http://ovc.catastro.meh.es/ovcservweb/OVCSWLocalizacionRC/OVCCallejero.asmx/Consulta_DNPLOC?Provincia=string&Municipio=string&Sigla=string&Calle=string&Numero=string&Bloque=string&Escalera=string&Planta=string&Puerta=string';
        $xml_response = self::callCatastro($page);

    }
    private static function callCatastro($page){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $page);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        /*PROBAR*/
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml; charset=utf-8'));
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

}