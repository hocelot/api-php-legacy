<?php

/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 12/12/16
 * Time: 12:47
 */
namespace App\Http\NodeModel;
use NeoEloquent;

class Town extends NeoEloquent
{
    protected $label = 'Town'; // or array('User', 'Fan')

    protected $fillable = ['id', 'ine', 'name'];

    public function province()
    {
        return $this->belongsTo('Province');
    }

}