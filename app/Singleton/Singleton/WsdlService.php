<?php
/**
 * Created by PhpStorm.
 * User: a.luis
 * Date: 16/06/2016
 * Time: 9:09
 */

namespace App\Singleton;

//use Illuminate\Support\Facades\Log;
use SoapClient;
use Log;


class WsdlService
{
    private $val_as_base_url = 'http://www.bowbuy.com/';

    public function call_ws($target, $nombre_ws, $array_params)
    {
        $params = array(
            'cache_wsdl' => 0,
            'features' => 1,
            'keep_alive' => 'Connection: close',
            'ssl_method' => 2,
            'trace' => 1,
            'exceptions' => 1
        );
        // Si el target es una URL, lo utilizamos directamente, en caso contrario, devolvemos un error
        if ((strpos($target, 'http://') !== FALSE) || (strpos($target, 'https://') !== FALSE) || (strpos($target, 'www.') !== FALSE)) {
            $url = $target;
        } else {
            return FALSE;
        }

        $client = new SoapClient($url, $params);
        $result = $client->__soapCall($nombre_ws, array('parameters' => $array_params));
        unset($client);
        return $result;
    }

    public function getBaseUrl()
    {
        return $this->val_as_base_url;
    }

    public function getWsUrl($string)
    {
        return env("WSDL_".$string);
    }


}
