<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 30/07/16
 * Time: 21:21
 */

namespace App\Http\Middleware;

use Closure;
use League\OAuth2\Server\Exception\InvalidScopeException;
use LucaDegasperi\OAuth2Server\Authorizer;

class scopeMiddleware
{
    protected $authorizer;

    /**
     * Whether or not to check the http headers only for an access token.
     *
     * @var bool
     */
    protected $httpHeadersOnly = false;

    /**
     * Create a new oauth middleware instance.
     *
     * @param \LucaDegasperi\OAuth2Server\Authorizer $authorizer
     * @param bool $httpHeadersOnly
     */
    public function __construct(Authorizer $authorizer, $httpHeadersOnly = false)
    {
        $this->authorizer = $authorizer;
        $this->httpHeadersOnly = $httpHeadersOnly;
    }

    public function handle($request, Closure $next, $strscope)
    {
    //Ajax request con csrtoken
        
        if(($request->ajax() && $request->has('_token'))){// || $strscope == 'free_client_access_token_missmatch'){
            return $next($request);
        }else {
            $scopes = [];

            if (!is_null($strscope)) {
                $scopes = explode('+', $strscope);
            }

            $this->authorizer->setRequest($request);

            $this->authorizer->validateAccessToken($this->httpHeadersOnly);



            //Buscamos en db a ver los permisos del cliente
            $db_mr = app('db')->connection('oauth');
            $resp = $db_mr->table('oauth_client_scopes')->select('scope_id')->where('client_id', $this->authorizer->getClientId())->first();
            if (!empty($resp->scope_id) && $this->check_scope($resp->scope_id, $scopes)) {
                return $next($request);
            } else {
                throw new InvalidScopeException(implode(',', $scopes));
            }
        }
    }

    private function check_scope($client_scope, $str_scope)
    {
        foreach ($str_scope as $sc) {
            if ($sc == $client_scope) return true;
            else continue;
        }
        return false;
    }

}