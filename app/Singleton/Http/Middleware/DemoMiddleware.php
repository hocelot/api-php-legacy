<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 18/06/16
 * Time: 0:19
 */

namespace App\Http\Middleware;


use Closure;
use Log;
use App\Http\Models\Demo\DemoModel;

class DemoMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        Log::debug(print_r($request->input('params')['telefono'], true));
        if(!$request->has('params') || !isset($request->input('params')['telefono'])) return response()->json(['msg'=>'No te quedan consultas', 'status' => 400]);
        $phone = app('db')->connection('apidb')->table('demo_key_phone')->select('*')->where('phone', $request->input('params')['telefono'])->first();
        Log::debug(print_r($phone, true));
        Log::debug(print_r($phone->active, true));
        Log::debug(print_r('ACTIVO O NO', true));
        if ($phone->active) {
            //$phone->active == 1 &&
            if ( $phone->date > Date("Y-m-d H:i:s") && $phone->trys > 0 )
                DemoModel::quitActive($request->input('params')['telefono']);
                return $next($request);
        }
        return response()->json(['msg'=>'No te quedan consultas', 'status' => 400]);
    }


}
