<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
//$app->get('/dev/node', 'NodeTestController@meterProvincia');

// oAuth2
$app->post('/api/oauth/access_token', function () {
    return response()->json(Authorizer::issueAccessToken());
});

$app->group(['prefix' => 'admin','namespace' => 'App\Http\Controllers\Admin', 'middleware' => 'cPanel'], function ($app) {
//    $app->get('/', function (){
//echo phpinfo();
//                return view('cPanel.index');
//    });
    $app->get('/', function (){
//echo phpinfo();
//        return view('cPanel.index');
        return view('cPanel.viewClient');
    });
//    $app->get('/index', function (){return view('cPanel.index');});
    $app->get('/index', function (){return view('cPanel.viewClient');});
    $app->get('/clients', function (){return view('cPanel.viewClient');});
    $app->get('/addClient', function (){return view('cPanel.addClient');});
    $app->post('/clients/all', 'ClientsController@getClients');
    $app->post('/clients/add', 'ClientsController@setHocelotClient');
    $app->post('/clients/viewClient', 'ClientsController@viewClient');
    $app->post('/clients/delClient', 'ClientsController@delClient');
    $app->post('/clients/editClient', 'ClientsController@editClient');
});
$app->post('/api',['middleware'=> 'scope:mr_EhDnRifP3qZq75VUW5BsSnWh1sMk8Dkb0hKGh+mc_neoUmzccfNTAm1vZOaYvnr2XEMFAu7MSemCW+sc_IpqAf3mZZVVnUcMW56PBDfndae94DTIQOPbY+ic_Tl3h7b3HsEeAe2b0DtW8zmpQ9EnBc9XjCsM7+if_Fcz72MajCgEVDnf0s2ct8M52RffsQlI1tZM6+gc_pzP9O2TW9eOiCXn4Mo0gVUroCpg1wA5EuWWQ+gf_fjw6HcqXaXcJHkRUNSe2xbMygiiCbwkBltJ2','uses'=>'APIScopeController@initialize']);
$app->post('/demo',['middleware'=> 'scope:mr_EhDnRifP3qZq75VUW5BsSnWh1sMk8Dkb0hKGh+sc_IpqAf3mZZVVnUcMW56PBDfndae94DTIQOPbY+ic_Tl3h7b3HsEeAe2b0DtW8zmpQ9EnBc9XjCsM7+if_Fcz72MajCgEVDnf0s2ct8M52RffsQlI1tZM6+gc_pzP9O2TW9eOiCXn4Mo0gVUroCpg1wA5EuWWQ+gf_fjw6HcqXaXcJHkRUNSe2xbMygiiCbwkBltJ2','uses'=>'Demo\DemoController@initialize']);
$app->get('/', function ()
{
    return view('newApi.index');
});
$app->get('/demo', function ()
{
//    return view('newApi.demo');
     return view('errors.demo-off');
});
$app->get('/contrata', function ()
{
    return view('newApi.contacto');
});
$app->get('/response', function ()
{
    return view('newApi.demoResponse');
});
$app->group(['prefix' => 'smart-data'], function ($app) {
    $app->get('/', function () {
        return view('newApi.smart-data');
    });
    $app->get('/smooth-client-onboarding', function () {
        return view('newApi.smooth-client-onboarding');
    });
    $app->get('/standardization-enhacement', function () {
        return view('newApi.standardization-enhacement');
    });
    $app->get('/enrichment', function () {
        return view('newApi.enrichment');
    });
});
$app->group(['prefix' => 'smart-analytics'], function ($app) {
    $app->get('/', function () {
        return view('newApi.smart-analytics');
    });
    $app->get('/id-check-fraud', function () {
        return view('newApi.id-check-fraud');
    });
    $app->get('/geo-check-fraud', function () {
        return view('newApi.geo-check-fraud');
    });
    $app->get('/risk-score', function () {
        return view('newApi.risk-score');
    });
});
$app->get('/condiciones', function ()
{
    return view('newApi.condiciones');
});
$app->get('/politica-de-privacidad', function ()
{
    return view('newApi.politica-de-privacidad');
});
$app->get('/politica-de-cookies', function ()
{
    return view('newApi.politica-de-cookies');
});
$app->get('/aviso-legal', function ()
{
    return view('newApi.aviso-legal');
});
$app->get('/sobre-nosotros', function ()
{
    return view('newApi.about');
});
$app->get('sitemap.xml', function(){
    return view('newApi.sitemap');
});
$app->group(['prefix' => 'test',
    'namespace' => 'App\Http\Controllers\Demo',
], function ($app) {
    $app->get('DemoRes', 'DemoController@responseDemo');
});
//$app->get('/test/DemoRes', function ()
//{
//    return view('newApi.demoNewResponse');
//});
$app->group(['prefix' => 'check',
    'namespace' => 'App\Http\Controllers\Demo',
], function ($app) {
    $app->post('sendSms', 'DemoController@phoneValidation');
    $app->post('checkCode', 'DemoController@checkCodVal');
});

$app->group(['prefix' => 'newContact',
    'namespace' => 'App\Http\Controllers',
], function ($app) {
    $app->post('solicitar', 'ContactController@solicitarApiNew');
});

$app->group(['prefix' => 'pdf',
    'namespace' => 'App\Http\Controllers\Demo'
], function ($app) {
    $app->post('/getPdf', 'DemoController@getPdfFromJson');
    $app->post('/download', 'DemoController@downloadRate');
});
$app->group(['prefix' => 'demo',
    'namespace' => 'App\Http\Controllers\Demo',
], function ($app) {
    $app->post('/trueKey', 'DemoController@trueKey');
});
//FIN WEB NEW DC

//$app->get('/prueba_carr','CsvController@process');


$app->group(['prefix' => 'contact',
    'namespace' => 'App\Http\Controllers',
], function ($app) {
    $app->post('solicitar', 'ContactController@solicitarApi');
});

//,mp_HPDMhyzXlvGZoOUTXkmCZjIAXALILrKW71AD,ms_EZqxwa8A3XAt617ooT5hS0iWAonSvyMUeo9v
$app->group(['prefix' => 'address',
    'namespace' => 'App\Http\Controllers\Address',
    'middleware' => 'scope:mr_EhDnRifP3qZq75VUW5BsSnWh1sMk8Dkb0hKGh+mp_HPDMhyzXlvGZoOUTXkmCZjIAXALILrKW71AD+ms_EZqxwa8A3XAt617ooT5hS0iWAonSvyMUeo9v'
], function ($app) {

    $app->post('code', 'AddressController@getIneCode');
    $app->post('town', 'AddressController@getTown');
    $app->post('roadtype', 'AddressController@getRoadType');
    $app->post('streetname', 'AddressController@getStreetName');
    $app->post('number', 'AddressController@getNumber');
    $app->post('letter', 'AddressController@getLetter');
    $app->post('km', 'AddressController@getKm');
    $app->post('block', 'AddressController@getBlock');
    $app->post('stairs', 'AddressController@getStairs');
    $app->post('floor', 'AddressController@getFloor');
    $app->post('door', 'AddressController@getDoor');
});


$app->post('score',['middleware' => 'scope:mr_EhDnRifP3qZq75VUW5BsSnWh1sMk8Dkb0hKGh+ms_EZqxwa8A3XAt617ooT5hS0iWAonSvyMUeo9v+sc_IpqAf3mZZVVnUcMW56PBDfndae94DTIQOPbY',
    'uses' => 'ScoreController@calculateScore']);


$app->group(['prefix' => 'bill',
    'namespace' => 'App\Http\Controllers',
    //'middleware' => 'scope:mr_EhDnRifP3qZq75VUW5BsSnWh1sMk8Dkb0hKGh'
], function ($app) {
    $app->post('list', 'BillController@listBills');
});

//ROUTES APPLICATION DC
$app->group(['prefix' => 'auth',
    'namespace' => 'App\Http\Controllers\Account',
    'middleware' => 'scope:mr_EhDnRifP3qZq75VUW5BsSnWh1sMk8Dkb0hKGh'
], function ($app) {
    $app->post('login', 'AuthController@postLogin');
    $app->post('join', 'AuthController@postJoin');
});
$app->group(['prefix' => 'Account',
    'namespace' => 'App\Http\Controllers\Account',
    'middleware' => 'scope:mr_EhDnRifP3qZq75VUW5BsSnWh1sMk8Dkb0hKGh'
], function ($app) {
    $app->post('getRates', 'RatesController@getRates');
    $app->post('getTerRates', 'RatesController@getTerRates');
    $app->post('getProfile', 'UserController@getProfile');
    $app->post('getSidebarProfile', 'UserController@getSidebarProfile');
    $app->post('getBill', 'BillController@getBill');
    $app->post('getDataBill', 'BillController@getDataBill');
    $app->post('uploadBatch', 'BatchController@uploadBatch');
    $app->post('getIndex', 'IndexController@getIndex');
    $app->post('getValMap', 'IndexController@getValMap');
    $app->post('getApi', 'ApiController@getConsumo');
});
//REMEMORI ALEX
$app->get('/DemoRm', function ()
{
    return view('pruebas.rememori');
});
$app->group(['prefix' => 'Pruebas',
    'namespace' => 'App\Http\Controllers\Pruebas'
], function ($app) {
    $app->post('DemoRm', 'RememoriController@initialize');
});
