<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 6/09/16
 * Time: 12:56
 */

namespace App\Http\Controllers\IdCheck;


use App\Http\Controllers\Proxies\Proxies;
use App\Http\Models\UtilClass;
use Elastica\Util;
use Log;


class IDCheck
{
    public static function check_id($name, $first_name, $last_name, $dni)
    {
//        if (strlen($dni) < 9) {
//            return ['dni_encontrado' => $dni, 'hits' => ['name_surname' => $name . ' ' . $first_name . ' ' . $last_name, 'nif' => $dni, 'score_id_check' => 'KO', 'aeat_name' => '-', 'correlacion_id_check' => 0.0], 'status' => 400];
//        }

//        return ['dni_encontrado' => $dni, 'hits' => ['name_surname' => $name . ' ' . $first_name . ' ' . $last_name, 'nif' => $dni, 'score_id_check' => 'OK', 'aeat_name' =>$first_name . ' ' . $last_name.' '.$name, 'correlacion_id_check' => 100], 'status' => 200];

        if(preg_match('/[A-Za-z]/',substr($dni,0,1))) {
            $letter = substr($dni,0,1);
            $rest = substr($dni,1);
            $rest = str_pad($rest, 8, '0', STR_PAD_LEFT);
            $dni = $letter.$rest;
        }else $dni = str_pad($dni, 9, '0', STR_PAD_LEFT);

        $dni = strtoupper($dni);

        $letra = substr($dni, -1, 1);
        $numero = substr($dni, 0, 8);

        // Si es un NIE hay que cambiar la primera letra por 0, 1 ó 2 dependiendo de si es X, Y o Z.
        $numero = str_replace(array('X', 'Y', 'Z'), array(0, 1, 2), $numero);

        $modulo = $numero % 23;
        $letras_validas = "TRWAGMYFPDXBNJZSQVHLCKE";
        $letra_correcta = substr($letras_validas, $modulo, 1);

        if ($letra_correcta != $letra) {
            return ['dni_encontrado' => $dni, 'hits' => ['name_surname' => $name . ' ' . $first_name . ' ' . $last_name, 'nif' => $dni, 'score_id_check' => 'KO', 'aeat_name' => 'dni o nie mal formado', 'correlacion_id_check' => 0.0], 'status' => 400];
        }

//        $search = UtilClass::del_articulos(mb_strtoupper(UtilClass::elimina_acentos(trim($first_name . ' ' . $last_name . ' ' . $name))));
        $searchs = [
            UtilClass::del_articulos(mb_strtoupper(UtilClass::elimina_acentos(trim($first_name)))),
            UtilClass::del_articulos(mb_strtoupper(UtilClass::elimina_acentos(trim($first_name . ' ' . $last_name)))),
            UtilClass::del_articulos(mb_strtoupper(UtilClass::elimina_acentos(trim($first_name . ' ' . $last_name . ' ' . $name))))
        ];
        foreach ($searchs as $search) {
            $search = urlencode($search);
            $curl_results = self::searchAeat($dni, $search);
            if ($curl_results["aeat_name"]) break;
        }
        if (!isset($curl_results["aeat_name"]) || !$curl_results["aeat_name"]) {
            $aeat_name = "-";
            $dni_encontrado = $dni;
            $percent = 0.0;
        } else {
//            Log::debug('AEAT: '.$curl_results["aeat_name"]);
            $dni_encontrado = $curl_results["dni"];
            $aeat_name = mb_convert_encoding($curl_results["aeat_name"], 'UTF-8', 'HTML-ENTITIES');
            $cmp_name = mb_strtoupper(UtilClass::elimina_acentos($first_name . ' ' . $last_name . ' ' . $name), 'UTF-8');
            similar_text(trim(mb_strtoupper(UtilClass::elimina_acentos($aeat_name), 'UTF-8')), trim($cmp_name), $percent);
            $percent = round($percent, PHP_ROUND_HALF_EVEN);
        }
        $id_check = array(
            'nif' => $dni,
            'name_surname' => $name . ' ' . $first_name . ' ' . $last_name
        );
        if (!isset($percent)) {
            $aeat_name = '-';
            $percent = 0.0;
            $id_check['score_id_check'] = 'NC';
            $status = 402;
        } else if ($percent == 0.0) {
            //Nuevo del 28-03-2017
            $contenido = false;
            $array_names = explode(' ', $name);
            $cmp_f_n = trim(mb_strtoupper(UtilClass::elimina_acentos($first_name), 'UTF-8'));
            $cmp_l_n = trim(mb_strtoupper(UtilClass::elimina_acentos($last_name), 'UTF-8'));
            $cmp_aeat_name = trim(mb_strtoupper(UtilClass::elimina_acentos($aeat_name), 'UTF-8'));
            foreach ($array_names as $aux_name) {
                $cmp_name = trim(mb_strtoupper(UtilClass::elimina_acentos($aux_name), 'UTF-8'));
                if (!empty($cmp_name) && !empty($cmp_f_n) && !empty($cmp_l_n)) {
                    if (stristr($cmp_aeat_name, $cmp_name) && stristr($cmp_aeat_name, $cmp_f_n) && stristr($cmp_aeat_name, $cmp_l_n)) {
                        $contenido = true;
                        break;
                    }
                }
            }
            if (!$contenido) {
                $id_check['score_id_check'] = 'KO';
//                $id_check['score_id_check'] = 'OK';
                $status = 400;
            } else {
                $id_check['score_id_check'] = 'OK';
                $status = 200;
            }
        } else {
            $id_check['score_id_check'] = 'OK';
            $status = 200;
        }
        $id_check['aeat_name'] = $aeat_name;
        $id_check['correlacion_id_check'] = $percent;

        //Log::debug(print_r($save, true));
        return ['dni_encontrado' => $dni_encontrado, 'hits' => $id_check, 'status' => $status];

    }

    private static function searchAeat($dni, $ap)
    {
        $start = microtime(true);
        Log::debug('LLAMO AEAT  '.$start);
        $ip_real = false;
        $obj_proxy = new Proxies('aeat');
//        sleep(1);
        $ip = $obj_proxy->get();
        $obj_proxy->setUsed($ip);
        $cmd = escapeshellcmd('/usr/bin/python ' . __DIR__ . '/aeat.py ' . $ip . ' ' . $dni . ' ' . $ap);
//        var_dump($cmd);
        exec($cmd, $result);
        $status = 200;
        if (!isset($result[0])) {
            $obj_proxy->unlock(200);

        } elseif ($result[0] == 'NONE') {
            $obj_proxy->unlock(200);
        }elseif ($result[0] == 'TIMEOUT') {
            $obj_proxy->unlock(200);
            self::searchAeat($dni,$ap);
        } else {
            $result[0] = trim(str_replace('[', '', $result[0]));
            $result[0] = trim(str_replace(']', '', $result[0]));
            $result[0] = trim(str_replace('"', '', $result[0]));
            $result = explode(', ', $result[0]);
            $obj_proxy->unlock(200);
        }


//        Log::debug(print_r($array, true));
        if (empty($result[1]) || !isset($result[1])) {
            $res = false;
        } else $res = $result[1];
        if (empty($result[0]) || !isset($result[0])) {
            $dni = false;
        } else $dni = $result[0];
        Log::debug('Termina AEAT  '.(microtime(true)-$start));

        return ['dni' => $dni, 'aeat_name' => $res, 'status' => $status];
    }
}