<?php
/**
 * Created by PhpStorm.
 * User: a.luis
 * Date: 15/06/2016
 * Time: 13:54
 */
namespace App\Http\Controllers;


use App\Http\Models\AuthModel;
use Log;

class AuthController extends Controller {
	
    
	public function postLogin()
	{
		Log::debug(print_r($this->request_params,true));
		$user_id = AuthModel::login($this->request_params);
		if ( ! empty($user_id))
		{
			$status = 200;
		}
		else
		{
			$status = 400;
		}

		return response()->json([ 'user_id' => $user_id, 'status' => $status ]);
	}


	public function postJoin()
	{
		return response()->json(AuthModel::join($this->request_params));
	}


}