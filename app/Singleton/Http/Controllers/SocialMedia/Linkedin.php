<?php

/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 26/08/16
 * Time: 18:16
 */
namespace App\Http\Controllers\SocialMedia;

use App\Http\Controllers\Proxies\Proxies;
use stdClass;
use Symfony\Component\DomCrawler\Crawler;
use Log;
use Facebook\WebDriver\Interactions\Touch\WebDriverDownAction;
use Facebook\WebDriver\JavaScriptExecutor;
use Facebook\WebDriver\WebDriverAction;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverExpectedCondition;
use Facebook\WebDriver\WebDriverKeys;

class Linkedin
{

    private $first_name, $last_name, $name, $city;


    public function __construct($name, $first_name, $last_name, $city)
    {
        $this->name = $name;
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->city = $city;
        putenv("webdriver.chrome.driver=C:\\Users\\Administrador\\Desktop\\chromedriver.exe");
        set_time_limit(0);
        $host = 'http://192.168.1.4:4444/wd/hub';
        $this->driver = RemoteWebDriver::create($host, DesiredCapabilities::chrome());
        $this->driver->manage()->timeouts()->implicitlyWait = 50;
        $this->driver->manage()->window()->maximize();
        try {
            $this->driver->get('https://www.linkedin.com/');
            $this->driver->navigate()->to('https://www.linkedin.com/uas/login');
            $this->driver->wait(20, 100)->until(
                WebDriverExpectedCondition::presenceOfAllElementsLocatedBy(
                    WebDriverBy::cssSelector("input[name=\"session_key\"]")
                )
            );
            $this->driver->findElement(WebDriverBy::cssSelector("input[name=\"session_key\"]"))->sendKeys('pedro.garcia89@aol.com');
            $this->driver->findElement(WebDriverBy::cssSelector("input[name=\"session_password\"]"))->sendKeys('123456789Abcd')->submit();
            while (true) {
                try {
                    $this->driver->wait(50, 200)->until(
                        WebDriverExpectedCondition::presenceOfAllElementsLocatedBy(WebDriverBy::cssSelector('body.boot-complete'))
                    );
                    break;
                } catch (\Exception $e) {
                    $this->driver->navigate()->refresh();
                }
            }
        } catch (\Exception $e) {
//            $this->driver->manage()->deleteAllCookies();
//            $this->driver->close();
//            $this->driver->quit();

        }
    }

//             Cambiar esto a múltiples emails
    public function initialize()
    {

        $public_url = 'https://www.linkedin.com/in/oscar-mario-guillen-salguero-3646882/';

        $this->driver->navigate()->to($public_url);

        while (true) {
            try {
                $this->driver->wait(50, 200)->until(
                    WebDriverExpectedCondition::presenceOfAllElementsLocatedBy(WebDriverBy::cssSelector('body.boot-complete'))
                );
                break;
            } catch (\Exception $e) {
                $this->driver->navigate()->refresh();
            }
        }
//            foreach ($this->driver->findElements(WebDriverBy::cssSelector('section > button')) as $button) {
//                if ($button->findElement(WebDriverBy::tagName('li-icon'))->getAttribute('type') == 'chevron-down-icon') {
//                    do {
//                        $button->click();
//                    } while ($button->findElement(WebDriverBy::tagName('li-icon'))->getAttribute('type') != 'chevron-up-icon');
//                }
//            }
//            Log::debug(print_r($this->driver->findElements(WebDriverBy::cssSelector('section > button'))));

        $results = $this->normalizeData($this->driver->getPageSource());
//            $results = $this->driver->getPageSource();

//            $this->driver->navigate()->to('https://www.linkedin.com/mynetwork/contacts/imported/');
//            $this->driver->wait(50, 200)->until(
//                WebDriverExpectedCondition::presenceOfAllElementsLocatedBy(
//                    WebDriverBy::id('contact-select-checkbox')
//                )
//            );
//            $this->driver->findElement(WebDriverBy::id('contact-select-checkbox'))->click();
//            $this->driver->wait(50, 200)->until(
//                WebDriverExpectedCondition::elementToBeClickable(
//                    WebDriverBy::className('mn-contacts__delete-selected-btn')
//                )
//            );
//            $this->driver->findElement(WebDriverBy::className('mn-contacts__delete-selected-btn'))->click();
//            $this->driver->wait(50, 200)->until(
//                WebDriverExpectedCondition::presenceOfAllElementsLocatedBy(
//                    WebDriverBy::className('button-primary-medium')
//                )
//            );
//            $this->driver->wait(50, 200)->until(
//                WebDriverExpectedCondition::elementToBeClickable(
//                    WebDriverBy::className('button-primary-medium')
//                )
//            );
//            $this->driver->findElement(WebDriverBy::className('button-primary-medium'))->click();


        $this->driver->close();
        $this->driver->quit();
        return $results;


    }

    private function normalizeData($html)
    {
        $crawler = new \Symfony\Component\DomCrawler\Crawler();
        $crawler->addHtmlContent($html);

        $linkedin = new \stdClass();
        //Cabecera perfil
        $linkedin->name = preg_replace('!\s+!', ' ', $crawler->filter('section.pv-profile-section')->first()->filter('h1.pv-top-card-section__name')->text());
        $linkedin->city = trim($crawler->filter('section.pv-profile-section')->first()->filter('h3.pv-top-card-section__location')->text());
        $linkedin->contacts_num = $crawler->filter('section.pv-profile-section')->first()->filter('h3.pv-top-card-section__connections > span')->first()->text();

        if ($crawler->filter('section.pv-profile-section')->first()->filter('img.pv-top-card-section__image')->first()->count() > 0)
            $linkedin->img = $crawler->filter('section.pv-profile-section')->first()->filter('img.pv-top-card-section__image')->first()->attr('href');

        //Informacion básica
        if ($crawler->filter('section.ci-vanity-url')->first()->count() > 0) {
            $linkedin->url = preg_replace('!\s+!', ' ', $crawler->filter('section.ci-vanity-url > div')->last()->text());
        }
        if ($crawler->filter('section.ci-twitter')->first()->count() > 0) {
            $crawler->filter('section.ci-twitter')->first()->filter('ul > li')->each(function ($node) use ($linkedin) {
                $linkedin->twitter[] = $node->filter('a')->attr('href');
            });
        }
        if ($crawler->filter('section.ci-birthday')->first()->count() > 0) {
            $linkedin->birthday = preg_replace('!\s+!', ' ', $crawler->filter('section.ci-birthday')->first()->filter('div > span')->text());
        }
        if ($crawler->filter('section.ci-website')->first()->count() > 0) {
            $crawler->filter('section.ci-website')->first()->filter('ul > li')->each(function ($node) use ($linkedin) {
                $linkedin->websites[] = $node->filter('a')->attr('href');
            });
        }

        if ($crawler->filter('section.ci-email')->first()->count() > 0) {
            $linkedin->email = preg_replace('!\s+!', ' ', $crawler->filter('section.ci-email')->first()->filter('div > span')->text());

        }
        if ($crawler->filter('section.ci-phone')->first()->count() > 0) {
            $crawler->filter('section.ci-website')->first()->filter('ul > li')->each(function ($node) use ($linkedin) {
                $linkedin->phones[] = preg_replace('!\s+!', ' ', $node->text());
            });
        }
        if ($crawler->filter('section.ci-address')->first()->count() > 0) {
            $linkedin->address = preg_replace('!\s+!', ' ', $crawler->filter('section.ci-address')->first()->filter('div > span')->text());

        }

        //Experiencia
        $linkedin->jobs = [];
        $crawler->filter('section.experience-section')->first()->filter('ul > li')->each(function ($node) use ($linkedin) {
            $aux = new \stdClass();
            if ($node->filter('div.pv-entity__summary-info > h3')->first()->count() > 0)
                $aux->position = preg_replace('!\s+!', ' ', $node->filter('div.pv-entity__summary-info > h3')->first()->text());
            if ($node->filter('div.pv-entity__summary-info > h4')->first()->filter('span')->last()->count() > 0)
                $aux->company = preg_replace('!\s+!', ' ', $node->filter('div.pv-entity__summary-info > h4')->first()->filter('span')->last()->text());
            if ($node->filter('div.pv-entity__summary-info > div > h4.pv-entity__date-range > span')->last()->count() > 0) {
                $time_uf = preg_replace('!\s+!', ' ', $node->filter('div.pv-entity__summary-info > div > h4.pv-entity__date-range > span')->last()->text());
                $time_uf = mb_convert_encoding(utf8_decode($time_uf), 'ASCII');
                $time_u_f = preg_split("/ .{1,1} /", $time_uf);
                $aux->from = trim($time_u_f[0]);
                if (isset($time_u_f[1]))
                    $aux->until = trim($time_u_f[1]);
            }
            if ($node->filter('div.pv-entity__summary-info > div > h4.pv-entity__duration > span')->last()->count() > 0)
                $aux->duration = preg_replace('!\s+!', ' ', $node->filter('div.pv-entity__summary-info > div > h4.pv-entity__duration > span')->last()->text());
            if ($node->filter('div.pv-entity__summary-info > div > h4.pv-entity__location > span')->last()->count() > 0)
                $aux->city = preg_replace('!\s+!', ' ', $node->filter('div.pv-entity__summary-info > div > h4.pv-entity__location > span')->last()->text());

            if (!empty($aux))
                $linkedin->jobs[] = $aux;
        });

        //Education
        $linkedin->educations = [];
        $crawler->filter('section.education-section')->first()->filter('ul > li')->each(function ($node) use ($linkedin) {
            $aux = new \stdClass();
            if ($node->filter('div.pv-entity__summary-info > div.pv-entity__degree-info > h3')->first()->count() > 0)
                $aux->school = preg_replace('!\s+!', ' ', $node->filter('div.pv-entity__summary-info > div.pv-entity__degree-info > h3')->first()->text());
            if ($node->filter('div.pv-entity__summary-info > div.pv-entity__degree-info > h4.pv-entity__degree-name > span')->last()->count() > 0)
                $aux->degree = preg_replace('!\s+!', ' ', $node->filter('div.pv-entity__summary-info > div.pv-entity__degree-info > h4.pv-entity__degree-name > span')->last()->text());
            if ($node->filter('div.pv-entity__summary-info > div.pv-entity__degree-info > h4.pv-entity__fos > span')->last()->count() > 0)
                $aux->type = preg_replace('!\s+!', ' ', $node->filter('div.pv-entity__summary-info > div.pv-entity__degree-info > h4.pv-entity__fos > span')->last()->text());
            if ($node->filter('div.pv-entity__summary-info > h4.pv-entity__dates > span')->last()->count() > 0) {
                $time_uf = preg_replace('!\s+!', ' ', $node->filter('div.pv-entity__summary-info > h4.pv-entity__dates > span')->last()->text());
                $time_uf = mb_convert_encoding(utf8_decode($time_uf), 'ASCII');
                $time_u_f = preg_split("/ .{1,1} /", $time_uf);
                $aux->from = trim($time_u_f[0]);
                if (isset($time_u_f[1]))
                    $aux->until = trim($time_u_f[1]);
            }
            if (!empty($aux))
                $linkedin->educations[] = $aux;
        });


        //Skills
        $linkedin->skills = [];
        $crawler->filter('section.pv-featured-skills-section ')->first()->filter('ul > li')->each(function ($node) use ($linkedin) {
            $aux = new \stdClass();
            if ($node->filter('span.pv-skill-entity__skill-name')->count() > 0)
                $aux->title = preg_replace('!\s+!', ' ', $node->filter('span.pv-skill-entity__skill-name')->text());
            if ($node->filter('span.pv-skill-entity__endorsement-count')->count() > 0)
                $aux->count = preg_replace('!\s+!', ' ', $node->filter('span.pv-skill-entity__endorsement-count')->text());

            if (!empty($aux))
                $linkedin->skills[] = $aux;
        });

        //Certificates
        $linkedin->certifications = [];
        $crawler->filter('section.certifications')->first()->filter('ul > li')->each(function ($node) use ($linkedin) {
            $aux = new \stdClass();
            if ($node->filter('h4')->first()->count() > 0) {
                $remplazar_t = $node->filter('h4 > span')->first()->text();
                $aux->title = trim(preg_replace('!\s+!', ' ', str_replace($remplazar_t, '', $node->filter('h4')->first()->text())));
            }
            if ($node->filter('p.pv-accomplishment-entity__license')->first()->count() > 0) {
                $aux->license = trim(preg_replace('!\s+!', ' ', $node->filter('p.pv-accomplishment-entity__license')->first()->text()));
            }
            if ($node->filter('a[data-control-name="certification_detail_company"]')->first()->count() > 0) {
                $remplazar_c = $node->filter('a[data-control-name="certification_detail_company"] > p > span')->text();
                $aux->company = trim(preg_replace('!\s+!', ' ', str_replace($remplazar_c, '', $node->filter('a[data-control-name="certification_detail_company"]')->first()->text())));
            }
            if ($node->filter('p.pv-accomplishment-entity__date')->count() > 0) {
                $remplazar_fecha = $node->filter('p.pv-accomplishment-entity__date > span')->text();
                $time_uf = trim(preg_replace('!\s+!', ' ', str_replace($remplazar_fecha, '', $node->filter('p.pv-accomplishment-entity__date')->text())));
                $time_uf = mb_convert_encoding(utf8_decode($time_uf), 'ASCII');
                $time_u_f = preg_split("/ .{1,1} /", $time_uf);
                $aux->from = trim($time_u_f[0]);
                if (isset($time_u_f[1]))
                    $aux->until = trim($time_u_f[1]);
            }
            if (!empty($aux))
                $linkedin->certifications[] = $aux;
        });
        //Languages
        $linkedin->languages = [];
        $crawler->filter('section.languages')->first()->filter('ul > li')->each(function ($node) use ($linkedin) {
            $aux = new \stdClass();
            if ($node->filter('h4')->first()->count() > 0) {
                $remplazar_t = $node->filter('h4 > span')->first()->text();
                $aux->title = trim(preg_replace('!\s+!', ' ', str_replace($remplazar_t, '', $node->filter('h4')->first()->text())));
            }
            if ($node->filter('p')->first()->count() > 0) {
                $aux->level = trim(preg_replace('!\s+!', ' ', $node->filter('p')->first()->text()));
            }
            if (!empty($aux))
                $linkedin->languages[] = $aux;
        });

        //Courses
        $linkedin->courses = [];
        $crawler->filter('section.courses')->first()->filter('ul > li')->each(function ($node) use ($linkedin) {
            $aux = new \stdClass();
            if ($node->filter('h4')->first()->count() > 0) {
                $remplazar_t = $node->filter('h4 > span')->first()->text();
                $aux->title = trim(preg_replace('!\s+!', ' ', str_replace($remplazar_t, '', $node->filter('h4')->first()->text())));
            }
            if (!empty($aux))
                $linkedin->courses[] = $aux;
        });

        //Honors
        $linkedin->honors = [];
        $crawler->filter('section.courses')->first()->filter('ul > li')->each(function ($node) use ($linkedin) {
            $aux = new \stdClass();
            if ($node->filter('h4')->first()->count() > 0) {
                $remplazar_t = $node->filter('h4 > span')->first()->text();
                $aux->title = trim(preg_replace('!\s+!', ' ', str_replace($remplazar_t, '', $node->filter('h4')->first()->text())));
            }
            if ($node->filter('p.pv-accomplishment-entity__description')->first()->count() > 0) {
                $remplazar_d = $node->filter('p.pv-accomplishment-entity__description > span')->first()->text();
                $aux->description = trim(preg_replace('!\s+!', ' ', str_replace($remplazar_d, '', $node->filter('p.pv-accomplishment-entity__description')->first()->text())));
            }
            if ($node->filter('p.pv-accomplishment-entity__date')->count() > 0) {
                $remplazar_fecha = $node->filter('p.pv-accomplishment-entity__date > span')->text();
                $time_uf = trim(preg_replace('!\s+!', ' ', str_replace($remplazar_fecha, '', $node->filter('p.pv-accomplishment-entity__date')->text())));
                $time_uf = mb_convert_encoding(utf8_decode($time_uf), 'ASCII');
                $time_u_f = preg_split("/ .{1,1} /", $time_uf);
                $aux->from = trim($time_u_f[0]);
                if (isset($time_u_f[1]))
                    $aux->until = trim($time_u_f[1]);
            }
            if ($node->filter('p.pv-accomplishment-entity__issuer')->first()->count() > 0) {
                $remplazar_e = $node->filter('p.pv-accomplishment-entity__issuer > span')->first()->text();
                $aux->entity = trim(preg_replace('!\s+!', ' ', str_replace($remplazar_e, '', $node->filter('p.pv-accomplishment-entity__issuer')->first()->text())));
            }
            if (!empty($aux))
                $linkedin->honors[] = $aux;
        });

        //Organizations
        $linkedin->organizations = [];
        $crawler->filter('section.organizations')->first()->filter('ul > li')->each(function ($node) use ($linkedin) {
            $aux = new \stdClass();
            if ($node->filter('h4')->first()->count() > 0) {
                $remplazar_t = $node->filter('h4 > span')->first()->text();
                $aux->title = trim(preg_replace('!\s+!', ' ', str_replace($remplazar_t, '', $node->filter('h4')->first()->text())));
            }
            if ($node->filter('p.pv-accomplishment-entity__description')->first()->count() > 0) {
                $remplazar_d = $node->filter('p.pv-accomplishment-entity__description > span')->first()->text();
                $aux->description = trim(preg_replace('!\s+!', ' ', str_replace($remplazar_d, '', $node->filter('p.pv-accomplishment-entity__description')->first()->text())));
            }
            if ($node->filter('p.pv-accomplishment-entity__date')->count() > 0) {
                $time_uf = trim(preg_replace('!\s+!', ' ', $node->filter('p.pv-accomplishment-entity__date')->text()));
                $time_uf = mb_convert_encoding(utf8_decode($time_uf), 'ASCII');
                $time_u_f = preg_split("/ .{1,1} /", $time_uf);
                $aux->from = trim($time_u_f[0]);
                if (isset($time_u_f[1]))
                    $aux->until = trim($time_u_f[1]);
            }
            if (!empty($aux))
                $linkedin->organizations[] = $aux;
        });

        //Patents
        $linkedin->patents = [];
        $crawler->filter('section.patents')->first()->filter('ul > li')->each(function ($node) use ($linkedin) {
            $aux = new \stdClass();
            if ($node->filter('a')->first()->count() > 0) $aux->url = $node->filter('a')->first()->attr('href');
            if ($node->filter('h4')->first()->count() > 0) {
                $remplazar_t = $node->filter('h4 > span')->first()->text();
                $aux->title = trim(preg_replace('!\s+!', ' ', str_replace($remplazar_t, '', $node->filter('h4')->first()->text())));
            }
            if ($node->filter('p.pv-accomplishment-entity__description')->first()->count() > 0) {
                $remplazar_d = $node->filter('p.pv-accomplishment-entity__description > span')->first()->text();
                $aux->description = trim(preg_replace('!\s+!', ' ', str_replace($remplazar_d, '', $node->filter('p.pv-accomplishment-entity__description')->first()->text())));
            }
            if ($node->filter('p.pv-accomplishment-entity__issuer')->first()->count() > 0) {
                $remplazar_d = $node->filter('p.pv-accomplishment-entity__issuer > span')->first()->text();
                $aux->entity = trim(preg_replace('!\s+!', ' ', str_replace($remplazar_d, '', $node->filter('p.pv-accomplishment-entity__issuer')->first()->text())));
            }
            if ($node->filter('p.pv-accomplishment-entity__date')->count() > 0) {
                $time_uf = trim(preg_replace('!\s+!', ' ', $node->filter('p.pv-accomplishment-entity__date')->text()));
                $time_uf = mb_convert_encoding(utf8_decode($time_uf), 'ASCII');
                $time_u_f = preg_split("/ .{1,1} /", $time_uf);
                $aux->from = trim($time_u_f[0]);
                if (isset($time_u_f[1]))
                    $aux->until = trim($time_u_f[1]);
            }
            if ($node->filter('a')->last()->count() > 0) {
                $node->filter('a')->last()->filter('img')->each(function ($img) use ($aux) {
                    $aux->authors[] = $img->attr('alt');
                });
            }
            if (!empty($aux))
                $linkedin->patents[] = $aux;
        });

        //Proyectos
        $linkedin->projects = [];
        $crawler->filter('section.projects')->first()->filter('ul > li')->each(function ($node) use ($linkedin) {
            $aux = new \stdClass();
            if ($node->filter('a')->first()->count() > 0) $aux->url = $node->filter('a')->first()->attr('href');
            if ($node->filter('h4')->first()->count() > 0) {
                $remplazar_t = $node->filter('h4 > span')->first()->text();
                $aux->title = trim(preg_replace('!\s+!', ' ', str_replace($remplazar_t, '', $node->filter('h4')->first()->text())));
            }
            if ($node->filter('p.pv-accomplishment-entity__description')->first()->count() > 0) {
                $remplazar_d = '';
                if ($node->filter('p.pv-accomplishment-entity__description > div')->first()->count() > 0)
                    $remplazar_d = $node->filter('p.pv-accomplishment-entity__description > div')->first()->text();
                $aux->description = trim(preg_replace('!\s+!', ' ', str_replace($remplazar_d, '', $node->filter('p.pv-accomplishment-entity__description')->first()->text())));
            }
            if ($node->filter('p.pv-accomplishment-entity__date')->count() > 0) {
                $time_uf = trim(preg_replace('!\s+!', ' ', $node->filter('p.pv-accomplishment-entity__date')->text()));
                $time_uf = mb_convert_encoding(utf8_decode($time_uf), 'ASCII');
                $time_u_f = preg_split("/ .{1,1} /", $time_uf);
                $aux->from = trim($time_u_f[0]);
                if (isset($time_u_f[1]))
                    $aux->until = trim($time_u_f[1]);
            }
            if ($node->filter('a')->last()->count() > 0) {
                $node->filter('a')->last()->filter('img')->each(function ($img) use ($aux) {
                    $aux->authors[] = $img->attr('alt');
                });
            }
            if (!empty($aux))
                $linkedin->projects[] = $aux;
        });

        //Publications
        $linkedin->publications = [];
        $crawler->filter('section.publications')->first()->filter('ul > li')->each(function ($node) use ($linkedin) {
            $aux = new \stdClass();
            if ($node->filter('a')->first()->count() > 0) $aux->url = $node->filter('a')->first()->attr('href');
            if ($node->filter('h4')->first()->count() > 0) {
                $remplazar_t = $node->filter('h4 > span')->first()->text();
                $aux->title = trim(preg_replace('!\s+!', ' ', str_replace($remplazar_t, '', $node->filter('h4')->first()->text())));
            }
            if ($node->filter('p.pv-accomplishment-entity__description')->first()->count() > 0) {
                $remplazar_d = '';
                if ($node->filter('p.pv-accomplishment-entity__description > span')->first()->count() > 0)
                    $remplazar_d = $node->filter('p.pv-accomplishment-entity__description > span')->first()->text();
                $aux->description = trim(preg_replace('!\s+!', ' ', str_replace($remplazar_d, '', $node->filter('p.pv-accomplishment-entity__description')->first()->text())));
            }

            if ($node->filter('p.pv-accomplishment-entity__publisher')->first()->count() > 0) {
                $remplazar_d = '';
                if ($node->filter('p.pv-accomplishment-entity__publisher > span')->first()->count() > 0)
                    $remplazar_d = $node->filter('p.pv-accomplishment-entity__publisher > span')->first()->text();
                $aux->publisher = trim(preg_replace('!\s+!', ' ', str_replace($remplazar_d, '', $node->filter('p.pv-accomplishment-entity__publisher')->first()->text())));
            }
            if ($node->filter('p.pv-accomplishment-entity__date')->count() > 0) {
                $remplazar_fecha = $node->filter('p.pv-accomplishment-entity__date > span')->text();
                $time_uf = trim(preg_replace('!\s+!', ' ', str_replace($remplazar_fecha, '', $node->filter('p.pv-accomplishment-entity__date')->text())));
                $time_uf = mb_convert_encoding(utf8_decode($time_uf), 'ASCII');
                $time_u_f = preg_split("/ .{1,1} /", $time_uf);
                $aux->from = trim($time_u_f[0]);
                if (isset($time_u_f[1]))
                    $aux->until = trim($time_u_f[1]);
            }
            if ($node->filter('a')->last()->count() > 0) {
                $node->filter('a')->last()->filter('img')->each(function ($img) use ($aux) {
                    $aux->authors[] = $img->attr('alt');
                });
            }
            if (!empty($aux))
                $linkedin->publications[] = $aux;
        });

        //Calificaciones
        $linkedin->test_scores = [];
        $crawler->filter('section.test-scores')->first()->filter('ul > li')->each(function ($node) use ($linkedin) {
            $aux = new \stdClass();
            if ($node->filter('h4')->first()->count() > 0) {
                $remplazar_t = $node->filter('h4 > span')->first()->text();
                $aux->title = trim(preg_replace('!\s+!', ' ', str_replace($remplazar_t, '', $node->filter('h4')->first()->text())));
            }
            if ($node->filter('p.pv-accomplishment-entity__description')->first()->count() > 0) {
                $remplazar_d = '';
                if ($node->filter('p.pv-accomplishment-entity__description > span')->first()->count() > 0)
                    $remplazar_d = $node->filter('p.pv-accomplishment-entity__description > span')->first()->text();
                $aux->description = trim(preg_replace('!\s+!', ' ', str_replace($remplazar_d, '', $node->filter('p.pv-accomplishment-entity__description')->first()->text())));
            }

            if ($node->filter('p.pv-accomplishment-entity__score')->first()->count() > 0) {
                $aux->score = trim(preg_replace('!\s+!', ' ', $node->filter('p.pv-accomplishment-entity__score')->first()->text()));
            }
            if ($node->filter('p.pv-accomplishment-entity__date')->count() > 0) {
                $remplazar_fecha = $node->filter('p.pv-accomplishment-entity__date > span')->text();
                $time_uf = trim(preg_replace('!\s+!', ' ', str_replace($remplazar_fecha, '', $node->filter('p.pv-accomplishment-entity__date')->text())));
                $time_uf = mb_convert_encoding(utf8_decode($time_uf), 'ASCII');
                $time_u_f = preg_split("/ .{1,1} /", $time_uf);
                $aux->from = trim($time_u_f[0]);
                if (isset($time_u_f[1]))
                    $aux->until = trim($time_u_f[1]);
            }
            if (!empty($aux))
                $linkedin->test_scores[] = $aux;
        });

        //Intereses

        $linkedin->interests = [];
        $crawler->filter('section.interests-section')->first()->filter('ul > li')->each(function ($node) use ($linkedin) {
            if ($node->filter('h3')->first()->count() > 0) {
                $aux = trim(preg_replace('!\s+!', ' ', $node->filter('h3 > span')->first()->text()));
            }
            if (!empty($aux) && !in_array($aux, $linkedin->interests))
                $linkedin->interests[] = $aux;
        });

        return $linkedin;
    }
}