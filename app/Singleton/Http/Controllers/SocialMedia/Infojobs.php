<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 1/09/16
 * Time: 10:08
 */

namespace App\Http\Controllers\SocialMedia;

use Log;
use stdClass;
use Symfony\Component\DomCrawler\Crawler;
use Thread;

class Infojobs extends Thread
{
    private $first_name, $last_name, $name, $email;
    public $results;

    public function __construct($name, $first_name, $last_name, $email)
    {
        $this->name = $name;
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->email = $email;
    }

    public function run()
    {
//        echo ('Lanza infojobs '.microtime(true));
        $url = $this->searxurl();
        $prob_busqueda = $this->isReg();
        if ($url) {
            $this->results = array('prob_busqueda_empleo'=>$prob_busqueda,$this->getData($url));
        } else {
            $this->results = array('prob_busqueda_empleo'=>$prob_busqueda);
        }
    }

    private function searxurl()
    {
        $name_search = $this->name;
        if (strrpos($this->name, ' ')) {
            $name_search = str_replace(' ', '|', $this->name);
        }
        $str_search = urlencode('site:infojobs.net ("' . $name_search . ' ' . $this->first_name . ' ' . $this->last_name . '") intitle:perfil');
        $search = $this->curl('https://searx.laquadrature.net/?q=' . $str_search . '&categories=general&format=json');
        if (!empty($search->results)) {
            return $this->searxLogic($search);
        } else {
            return false;
        }

    }


//Paso los resultados de la busqueda
    private
    function searxLogic($search)
    {
        if (count($search->results) > 1) {
            $urls[] = false;
            //Se repite la busqueda para intentar afinar más
            foreach ($search->results as $result) {
                //(in_array('duckduckgo',$result->engines) || in_array('google',$result->engines) || in_array('bing',$result->engines)) &&
                if ((stristr($this->quitartildes($result->title), $this->quitartildes($this->name . ' ' . $this->first_name . ' ' . $this->last_name)) || stristr($this->quitartildes($result->title), $this->quitartildes($this->name . ' ' . $this->first_name))) && (in_array('duckduckgo', $result->engines) || in_array('google', $result->engines) || in_array('bing', $result->engines))) {
                    $urls[] = $result->url;
                }
            }
            //var_dump($urls,true);
            if (isset($urls) && count($urls) > 1) {
                $score_url = array();
                foreach ($urls as $url) {
                    $url_score = 0;
                    if (stristr($url, $this->quitartildes($this->name))) {
                        $url_score++;
                    }
                    if (stristr($url, $this->quitartildes($this->first_name))) {
                        $url_score++;
                    }
                    if (stristr($url, $this->quitartildes($this->last_name))) {
                        $url_score++;
                    }
                    $score_url[] = array(
                        'url' => $url,
                        'score' => $url_score
                    );
                }
                $max = 0;
                $url = false;
                foreach ($score_url as $arr) {
                    if ($arr['score'] > $max) {
                        $max = $arr['score'];
                        $url = $arr['url'];
                    }
                }
                return $url;
            } else {
                return $urls[0];
            }

        } else {
            if ((stristr($this->quitartildes($search->results[0]->title), $this->quitartildes($this->name . ' ' . $this->first_name))) && $search->results[0]->score > 0.5) {
                return $search->results[0]->url;
            }
            return false;
        }
    }

    private
    function getData($url)
    {
        $content = $this->curl($url, true);
        $crawler = new Crawler();
        $crawler->addHtmlContent($content);

        $info = new stdClass();
        $info->ultima_actualizacion = trim($crawler->filter('.cv-update')->text());

            //Estudios
        $crawler->filter('#perfilPublicoEstudios > ul > li')->each(function ($node) use ($info) {
            $studie = new stdClass();
            // Fecha
            if ($node->filter('.vevent > span')->count() > 1) {
                $studie->start = $node->filter('.vevent > .dtstart')->text();
                $studie->end = $node->filter('.vevent > .dtend')->text();
            } else if ($node->filter('.vevent > span')->count() == 1) {
                $studie->date = $node->filter('.vevent > .dtend')->text();
            } else {
                $studie->date = '-';
            }
            //Titulo
            if ($node->filter('.clearfix > abbr')->count() > 0) {
                $studie->title = $node->filter('.clearfix > abbr')->attr('title');
            } else {
                $studie->title = '-';
            }
            // Institucion
            if ($node->filter('.clearfix > .education')->count() > 0) {
                $studie->institucion = $node->filter('.clearfix > .education')->text();
            } else {
                $studie->institucion = '-';
            }
            $info->studies[] = $studie;
        });


        //Experiencias Laborales
        $last_experience = new stdClass();
        if ($crawler->filter('#perfilPublicoExperiencias > .unit > .child > .title')->first()->filter('.summary')->count() > 0) {
            $last_experience->title = $crawler->filter('#perfilPublicoExperiencias > .unit > .child > .title')->first()->filter('.summary')->text();
        } else {
            $last_experience->title = '-';
        }
        if ($crawler->filter('#perfilPublicoExperiencias > .unit > .child > .title')->first()->filter('span > .location')->count() > 0) {
            $last_experience->company = $crawler->filter('#perfilPublicoExperiencias > .unit > .child > .title')->first()->filter('span > .location')->text();
        } else {
            $last_experience->company = '-';
        }
        if ($crawler->filter('#perfilPublicoExperiencias > .unit > .child > ul > li.first')->count() > 0) {
            $last_experience->sector = $crawler->filter('#perfilPublicoExperiencias > .unit > .child > ul > li.first')->text();
        } else {
            $last_experience->sector = '-';
        }
        if ($crawler->filter('#perfilPublicoExperiencias > .unit > .child > ul.time > li.first')->count() > 0) {
            $fecha = $crawler->filter('#perfilPublicoExperiencias > .unit > .child > ul.time > li.first')->text();
            if ($pos = strpos($fecha, '-')) {
                $last_experience->inicio = trim(substr($fecha, 0, $pos));
                $last_experience->hasta = trim(str_replace('-', '', substr($fecha, $pos)));
            } else {
                $last_experience->inicio = $last_experience->hasta = $fecha;
            }
        } else {
            $last_experience->inicio = $last_experience->hasta = '-';
        }
        $info->jobs[] = $last_experience;
        unset($last_experience);
        $crawler->filter('#perfilPublicoExperiencias > .unit > .child > .more-experience > .unit')->each(function ($node) use ($info) {
            $last_experience = new stdClass();
            if ($node->filter('.child > .title')->first()->filter('.summary')->count() > 0) {
                $last_experience->title = $node->filter('.child > .title')->first()->filter('.summary')->text();
            } else {
                $last_experience->title = '-';
            }
            if ($node->filter('.child > .title')->first()->filter('span > .location')->count() > 0) {

                $last_experience->company = $node->filter('.child > .title')->first()->filter('span > .location')->text();
            } else {
                $last_experience->company = '-';
            }
            if ($node->filter('.child > ul > li.first')->count() > 0) {

                $last_experience->sector = $node->filter('.child > ul > li.first')->text();
            } else {
                $last_experience->sector = '-';
            }

            if ($node->filter('.child > ul > li.time')->count() > 0) {
                $fecha = $node->filter('.child > ul > li.time')->text();
                if ($pos = strpos($fecha, '-')) {
                    $last_experience->inicio = trim(substr($fecha, 0, $pos));
                    $last_experience->hasta = trim(str_replace('-', '', substr($fecha, $pos)));
                } else {
                    $last_experience->inicio = $last_experience->hasta = $fecha;
                }
            } else {
                $last_experience->inicio = $last_experience->hasta = '-';
            }

            $info->jobs[] = $last_experience;

        });
        return $info;
    }

    private
    function curl($page, $info = false)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $page);
     //   curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
//        $result = curl_exec($ch);
//        curl_close($ch);
        if ($info) {
            return $result;
        }
        return json_decode($result);

    }

    private
    function quitartildes($cadena)
    {
        $no_permitidas = array("á", "é", "í", "ó", "ú", "Á", "É", "Í", "Ó", "Ú", "ñ", "À", "Ã", "Ì", "Ò", "Ù", "Ã™", "Ã ", "Ã¨", "Ã¬", "Ã²", "Ã¹", "ç", "Ç", "Ã¢", "ê", "Ã®", "Ã´", "Ã»", "Ã‚", "ÃŠ", "ÃŽ", "Ã”", "Ã›", "ü", "Ã¶", "Ã–", "Ã¯", "Ã¤", "«", "Ò", "Ã", "Ã„", "Ã‹");
        $permitidas = array("a", "e", "i", "o", "u", "A", "E", "I", "O", "U", "n", "N", "A", "E", "I", "O", "U", "a", "e", "i", "o", "u", "c", "C", "a", "e", "i", "o", "u", "A", "E", "I", "O", "U", "u", "o", "O", "i", "a", "e", "U", "I", "A", "E");
        return str_replace($no_permitidas, $permitidas, $cadena);

    }
    private function isReg(){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://www.infojobs.net/candidate/profile/check-email-registered.xhtml');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'email=' . $this->email);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
        $result = curl_exec($ch);
        curl_close($ch);
        $res = json_decode($result);
        if($res->email){
            return 'Media-Alta';
        }else {
            return 'Media-Baja';
        }
    }
}