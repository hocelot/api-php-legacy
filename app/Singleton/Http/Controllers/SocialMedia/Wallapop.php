<?php
/**
 * Created by PhpStorm.
 * User: DC
 * Date: 23/02/2017
 * Time: 16:33
 */

namespace App\Http\Controllers\SocialMedia;

use Illuminate\Http\Request;
use App\Http\Controllers\Proxies\Proxies;
use Log;
use stdClass;
use Goutte\Client;

class Wallapop
{
    public function getWallapop(){
        $telf = '665143412';
        $resp = $this->getWallapopAnuncio($telf);
        if($resp == '') {
            $telfFormat = $this->formatTelWalla($telf, 'A');
            $resp = $this->getWallapopAnuncio($telfFormat);
            if ($resp == '') {
                $telfFormat = $this->formatTelWalla($telf, 'B');
                $resp = $this->getWallapopAnuncio($telfFormat);
                if ($resp == '') {
                    return 'Ningún dato encontrado';
                } else {
                    $resp->user = $this->getUserWalla($resp);
                    return json_encode($resp);
                }
            } else {
                $resp->user = $this->getUserWalla($resp);
                return json_encode($resp);
            }
        }else{
            $resp->user = $this->getUserWalla($resp);
            return json_encode($resp);
        }
    }
    private function formatTelWalla($telf, $type){
        //TIPOS DE MOVIL-> A = Agrupado de 3 en 3; -> B = 3,2,2,2
        if($type == 'A') {
            $res1 = substr($telf, 0,3);
            $res2 = substr($telf, 3,3);
            $res3 = substr($telf, 6,3);
            $telf = $res1.' '.$res2.' '.$res3;
            return $telf;
        }elseif ($type == 'B'){
            $res1 = substr($telf, 0,3);
            $res2 = substr($telf, 3,2);
            $res3 = substr($telf, 5,2);
            $res4 = substr($telf, 7,2);
            $telf = $res1.' '.$res2.' '.$res3.' '.$res4;
            return $telf;
        }
    }
    public function getUserWalla($perfil){
        $client = new Client();
        $guzzleClient = new \GuzzleHttp\Client([
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 6.1; rv:49.0) Gecko/20100101 Firefox/49.0',
            ],
            'curl' => array(
                CURLOPT_SSL_VERIFYPEER => false,
            ),
            'proxy' => 'http://mrhc:QwEr1234@62.4.15.48:80'
        ]);
        $client->setClient($guzzleClient);
        foreach ($perfil->user as $usu) {
            $url = 'https://es.wallapop.com' . $usu->url;
            $html_search = $client->request('GET', $url);
            $img = $html_search->filter('.card-profile-avatar')->attr('style');
            $img1 = str_replace('background-image: url(\'', '', $img);
            $img = str_replace('\')', '', $img1);
            if($img != '/images/icons/man.png'){
                $usu->img = $img;
            }
            $usu->geo = new stdClass();
            $usu->geo->lat = $html_search->filter('#profile-map')->attr('data-lat');
            $usu->geo->lon = $html_search->filter('#profile-map')->attr('data-lng');
            if(count($html_search->filter('#js-profile-buttons > .js-profile-products')->eq(1)) > 0){
                $usu->sale = $html_search->filter('#js-profile-buttons > .js-profile-products')->eq(1)->text();
            }
            if(count($html_search->filter('#js-profile-buttons > .js-profile-products')->eq(2)) > 0) {
                $usu->ratings = $html_search->filter('#js-profile-buttons > .js-profile-products')->eq(2)->text();
            }
        }
        return $perfil->user;
    }
    public function getWallapopAnuncio($telf){
        $walla = new stdClass();
        $client = new Client();
        $guzzleClient = new \GuzzleHttp\Client([
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 6.1; rv:49.0) Gecko/20100101 Firefox/49.0',
            ],
            'curl' => array(
                CURLOPT_SSL_VERIFYPEER => false,
            ),
            'proxy' => 'http://mrhc:QwEr1234@62.4.15.48:80'
        ]);
        $client->setClient($guzzleClient);
        $url = 'http://es.wallapop.com/search?kws='.$telf.'&lat=&lng=';
        $html_search = $client->request('GET', $url);
        if (count($html_search->filter('.card-product')) > 0){
            //NUMERO DE PRODUCTOS SACADOS POR BUSQUEDA, NO INFLUYE QUE SEAN DISTINTOS PERFILES, AQUI SACAMOS EL TOTAL
            $walla->hits = new stdClass();
            $walla->hits->total = count($html_search->filter('.card-product'));
            $walla->hits->type = array();
            $html_search->filter('.card-product')->each(function ($node) use ($walla) {
                if(empty($walla->hits->type)) {
                    $walla->hits->type[0] = $node->filter('.product-info-category')->text();
                }
                else{
                    $correlacion = 0;
                    $prodCount = count($walla->hits->type);
                    foreach ($walla->hits->type as $usu) {
                        if ($usu == $node->filter('.product-info-category')->text()) {
                            $correlacion = 1;
                        }
                    }
                    if ($correlacion == 0) {
                        $walla->hits->type[$prodCount] = $node->filter('.product-info-category')->text();
                    }
                }
            });
            $walla->user = array();
            $html_search->filter('.card-product')->each(function ($node) use ($walla) {
                $correlacion = 0;
                if(!empty($walla->user)) {
                    $userCount = count($walla->user);
                    foreach ($walla->user as $usu) {
                        if ($usu->url == $node->filter('a')->last()->attr('href')) {
                            $correlacion = 1;
                        }
                    }
                    if ($correlacion == 0) {
                        $walla->user[$userCount]= new stdClass();
                        $walla->user[$userCount]->alias = $node->filter('.seller-name')->text();
                        $walla->user[$userCount]->prod = $node->filter('.seller-products')->text();
                        $walla->user[$userCount]->url = $node->filter('a')->last()->attr('href');
                    }
                }else{
                    $walla->user[0]= new stdClass();
                    $walla->user[0]->alias = $node->filter('.seller-name')->text();
                    $walla->user[0]->productos = $node->filter('.seller-products')->text();
                    $walla->user[0]->url = $node->filter('a')->last()->attr('href');
                }

            });
        }else{
            return '';
        }
        return $walla;
    }
}