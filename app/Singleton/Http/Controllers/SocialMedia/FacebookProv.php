<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 25/10/16
 * Time: 22:48
 */

namespace App\Http\Controllers\SocialMedia;

use Log;

class FacebookProv
{
    private $email;


    public function __construct($email)
    {
        $this->email = $email;
    }

    private function getOAuthToken()
    {
        $c = 0;
        do {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'dev2.vinculando.net/api/oauth/access_token');
            curl_setopt($ch, CURLOPT_VERBOSE, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, 'grant_type=client_credentials&client_id=mr_UdyAtQkqcKtw8yoRFe7WlJwp9HuGoyRZUcAHj&client_secret=OfjbZsHGcVRajirXj3fxjOYLRqNO4IAKksZ9MKHk');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $result = curl_exec($ch);
            $info = curl_getinfo($ch);
            curl_close($ch);
            sleep(2);
            $c++;
        } while ($info['http_code'] != 200 && $c < 3);
        if (empty($result)) return false;
        $result = json_decode($result);
        if (!isset($result->access_token)) return false;
        return $result->access_token;
    }

    public function search()
    {
        $access_token = $this->getOAuthToken();
        if(!$access_token) return false;
        $params = array(
            'params' => array(
                'email' => $this->email
            )
        );
//        $params = new stdClass();
//        $params->name = 'ALEJANDRO';
//        $params->first_name = 'SAN LUIS';
//        $params->last_name = 'MOURE';
//        $params->city = 'MADRID';
//Encriptamos
        $pubKey = 'LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0KTUlJQ0lqQU5CZ2txaGtpRzl3MEJBUUVGQUFPQ0FnOEFNSUlDQ2dLQ0FnRUF0TEovdHFKVEFLWGFvNnNHeXhEeQpIT1JQckhWc1lOckNrdXZqRXB0OFJNLzc0ZmtqRGNMaVh1NGlOQVh2TTNCWEUzbGZrQU9xeStJcThYbjdyV2ZnCis3aGU1VGp5NElDVTdDUHV3WjNtRXV6bUh1eUg4bG1DNHVrRTJuYUcwNmVXYkoyNkV5bzU4UVVOZk00Slk1WEkKalRLenAwU2VFK0tkWWhzMTJiWk4rdndybTExQ1FsM3VsZFpvTDdHL1M4UFNKOXJmZUhsUEU2MEl3Z0srWjNSQQoxaUJ0QzBVNGFZVjJzS2NFYjcxMXArTFlLVlJxSDNnUkJHL1BCclZqZUY4ZEJsQWptdG4zZ2o0TDhjRStvREFRCi9iOGN4R25jMEVZT05TaVJSaTRNWHhaNWVzT0FLMnNIKzJtTDdnT1ZZRGtUWU1oTWlGbUdOUjNqNG45L2dVcmgKS1o3NjNpOW8zc1VzV2htZ3ZMQ1VIVkloSFEwY2pudndZd21MMGk4TGVsaGFNSzQ4T3M4blFNMGliUDc0UE12VApGS0o4eCtIVzRBckM0V2pMMndRbVg5STJNemtla3lSU0lzZCtMM1pmWWhaT0N4QXpCVkVNZEpTMU9jUG10K3g4ClpIN09QRFFvZ0tBbzZyS1I0V1VxNlFjZVphZmp3VWRsS05YRUpnZ0RXeVV5L0lyTVdQS080SVhWb08vdlpWZlMKdU92OWYrOHBBN2JmMmxPQjhnSS8yaTdSQ1BTM09rZW5BOFhkcnNabUV6N0hqSDB6eTIxQ3RBT3hiUDhNZWZjUwphcHVjWjNXdnY0WnVRSkU0dW9JM1daUGdJUFo0NHd6dCtsOE9Uc3FhTWpYRUtmUUNkdEZjRXdreUlHNkVLRzlTClI4YW5NdW1uclhTUlRpVDNBMGluV1ZVQ0F3RUFBUT09Ci0tLS0tRU5EIFBVQkxJQyBLRVktLS0tLQo=';
        openssl_public_encrypt(json_encode($params['params']), $params['params'], base64_decode($pubKey));

        $page = 'social/fb';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'dev2.vinculando.net/' . $page);
//        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        $headers = array();
        $headers[] = 'Authorization: Bearer ' . $access_token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        $priv_key = 'LS0tLS1CRUdJTiBQUklWQVRFIEtFWS0tLS0tCk1JSUpRZ0lCQURBTkJna3Foa2lHOXcwQkFRRUZBQVNDQ1N3d2dna29BZ0VBQW9JQ0FRQ3FhUWg2c0FhRnBqZnIKdDN6d21pS3gxZmpxZVM0L2YwWjRMNHNDRSszZy9uUWVpZzBmSFkwdW5UUFdzY2VsNkVYOFJhU2wvVEwrOUR2OApzZloyQlpGL05IM2NTWFl1U2FoSEx1SjFwRVV4Yjd2YzUvbVV0NWx5Wm8xTlp6aWJVVkhMWXYwY2l4dk0xNDFCClhyRnpkQmE0U0d0VCtvZFErd25CUzRiWWU0MU9OWVExa3ZWUDJGRldpa1laenJRNjB2VFphb3l4TGZtUzgyanUKaDZSc3lPaytyWThGMXlLbWZQWU1QOEg3T0Y5aS9zQkZYREFFZXBEY0QrRVJyVktVUWl6bHlPZDZzb1V4WG1iQwp5UXQrYkVnUnRLaTZUUFRmaFlJRDNHM0ROOVF0RU9sdnhWblZHa3B4a3pRdmJwVm56UU8zSWhodm8weDc2RGJlCisvS3IxNlUxcEZEM0JwWHlTcEFWdTB1NGJIaXQ0aCszTXZLTjVFWDZUa2FjWmZWT0loZkdwNVBhK2JhemVGMzEKVC9vR0hIVzgySGI2QVVrZ0l6ekRBdHVCMUphNnNHTHo4ankwdHIrbVRkRmRLS25KOFF6MzdETFljMFgxZndCSwo5WStjMHByZHhWUHNUb25ZZko5b0pXVjloVzhiVTVZSkxwQUFQSWFxdk52eXVrdmVJeVVWMElGOFE2eksybDRICmh3cXZIY25JYVd2d0RLaGlWcmZEZXdLSDRFeWVYdGdtUGlvQXdWdEtpYkl5dXhnUE5QM0IrclowR0FyS2NDZHgKeDM2b2ZFY3o3N0wyNGdPbzlTUDZQUUNUdVJwQkJoWGJRcENMa0tSazNzbmJEb1pIeWt5em9xTlNSRmlHTVBYNwo5R2RvaWs4OURhS1VKSENjV0ZWMndWR1ZYcHN1SndJREFRQUJBb0lDQUVXTm15MWdQVGs5ajZPQS85akFRSnJ2Cko2cWtEVkZUWkhMWGpOZ3JSRE5LQ0NFUzNhb3VpSnBYQlhxQ2syZE85NG13N3pQa3RYVEVRTnV4MDFOMmtGMG8KRXByZnpRRzFoSjZSTDFNdTJpTUVaTndVUHBVSm52TEhrckxlQjRlMVdEbzRTL0RqZ2xSSVFscEZDUTZEZW1aNwpBSHVGVE1lcnZ0NkxNaDJYeUNQOHlkcUlBZUkxKzNUaitScTllR2h1QkF2ZFpsRkczNCt3RThzUHgvZXl4U2lVCjlFUkErOTZTTUdidGV3SkxqdnM3MlVRMkRVODJxM3QyYTdtb1VZVTd3VkdFZjU1ZjBPZEdqWkRCT0JUQW8yVysKcEFOdU42QW53TnBNbE94U0ZDTFV2VUttZlRUeWh5SDJJQjJmK0JFbTUzWk96MFI1bFZVWmdaUGZwN3VseDhNRApSRHJWZjNCNlMzaTlEZk1lcTZ0MmNudGdycUdOa1RjM1lxZEJwelh2Q2NjcE1vV0dPT1FLVWhIeXJDNks2eVJkCjdEMkxrVEVGT3hRcXk2c3lRVlFzb01JVkdIVW9hMTFFN21ZYU8wYUVxUjRkWHVQRGU3a3FxYTBZam4wV0xKOUgKSWJ2bUJFMjJHeEFhTnlTdHhkTEdsY3FDQ3FhSWZHVWhxOWNYYmQyYloydXF3dmhSSTZ3SEF5R20vVk9oOFFWSApVT3ZoTkMwTSt6aS9rQUdyS094VmNvakdscnA1eC84S2I4OHZyNU12U3VXYnUvSC8zeXBITm5YK2lTMXRHb29WCnc4T3Zsbi9OSVkvY00yZ0svQUJYZ1ZXeEF0OE5tWnN3WXRpNmdhWWVqb0VmRG5hQ0g0OHlINlVSZExMSHFJcFEKOWcwVm5ERDRlY1NULzhXYjlIbXhBb0lCQVFEZi9ycXZqYWtRdDc1dFhDTVJEZEhqNlRGRitBNnNNUHA3NG5CaAo4R3VGNGdyc29mSWE1aktzV0ZOK3FPY09oREVvbENJQjA5SGJkcnNUbkZ0bGYweWxZMkRUOHdDWWhPaDU1cFpICnlxYS9YV1JLYlgrNEVQK3VwSmVOOEFuT21hUWpEN1RPRWE0Tjh6U1pqTjRHdjl0OHhlbExtWDljNXZ3a2dmSE4KSEJtNVJBTnppZ3N2ajF0MkZsbEM4bUgyQlBBSWJIdFZ0Tmc5YmFkMXZrQkpRc1dueDR5K1VjRmczaGl5eUNTdgpDRjdHVG5uTlpnanZLbWF3cGZJUTVQNnVMMUs5SXFRQWhBNTJnTUNyeU1aZURFM1pSVW4xZHRYNmRET3J6d2hLClF3SmZ6SE9KYk83YTJILzZ2KytTZVdPS0lGQkFaeStVTjV5OVBSUE03SjFnVTJjSkFvSUJBUURDd2trY0RBd20KWHFTU084MERUZVhiNmJ0RkV3MXZIN2hrVGJ1UFdjN0prNXVrQ1Uxd2FsT1RGaG1ydmZiSDhzSWtiZU5xMG5NSgpVMDVpUkJRNDNEMzRrMnY2QUV1NWl4YWpoY0tyd3ZibkUvMzBlQmUzTTZjRVJSUWNxWGJHeHB5Q0w2c3pqeTl2CkNkc1ZmRzZQMjlVMTkzQm5oWTZZby9ITVZxcHlBWGNObE55T2E2b2VEd3lVYjk3SmVuOXlER3VueFBxRWwxRHUKMTRqYm1XejlYcUJlekQ3TWpuOCt4d05tUi9SRTV3ai9vMUhnR3FBcG54YkoyQzB0Z0h2YXlMWUttOHRlbFIwNApsQk11dE83NUVtQytCNys1WUFoS1BZdUQrOVl3Z3JLMzJsdjR5cS9IV09naWhyZERFVG9ham5BMzVDTjZYSEVkClZSeE1XbU0yc29ldkFvSUJBRUZQdjlWU1E5cUpDUEJRTGpWWnh6cGlFeWpvcERhVzdXSlQ2eHJUaERaOGExMEYKOTFiYWlpbndjeC82VmF6STVLQ0ZXTWtabnhZVHI0YVBUNkx2bEVTaERtbWVjQzBJaTlTSFpJZzRVWmg0dmJacAo2UlNkZUQvTzVCWFJ5THBCMkRjVTdFNHRuNGVYdzEyR1VuSzdYRjhzUWRMMlJjbXIzQmMxQUhFaTRXbEFxSlQyCnlqNHEyYXM3NXZqN1BZOERiNDIxTzkwSXB3aDZRWnEyUjM1VkJ0YmQrMjYrM2RqRnlZUFBrY3FyY1I3M1lOS0EKRHRqVmp5MndwVDFCMS9Tbi9jTUNKN215eDVqTC8rSFczK2dxUkVTMnpGTFRQSjhUWk1HUzAwTDcrT1EvOCt3OQpKQVFSbm5kWm9ad2U0dVF5ZWRQZGdtVUFOaWFEZVpPRjVubGd1Q2tDZ2dFQWZRVm5GU0FPdEpyUW5pYlJSaXU5ClFGeGNJTTFJTkd0eVhNRWhnN1lzN20xWTNnWXl0ZFVyYlNJa3ZJQ25hRmRIbFVjTlU4ZGduVDNpOVdXcWJZM0UKSWt2UVpwM0Yvb2R3c3V2c3VZSGdLUnBHVGFiQjRwSy94aGZwL3pReDBRd3g3OGhWNkRVa1IrMVZ2QWN5bWhxTwpWYVJQZmNUWWN0ZmJJa2NJQjBNMWYzeHQwT2NBUDJkOXRkUFNlaGV1dERHUThoT2FsWDdsTDAwZ3g0ZEgxYVhtCjRMUFFKRlBkRHhtRjd5U0ZnNjJqc3liT0trUXRTRXFkd0ZpTE44aGd1V05hcUppMkJqTkduTWxCUHh4YXczeXUKcVk0ajN3WHNqR3ZSb1pvS052M1BhQUkrUkFrb3FabEMrdGJyMFp1L1pzakU5THY0b2xXeEx4a3huem51c25BTQpkUUtDQVFFQWw3ZDlMUTNSKzRlTlJoSDNEVy9DSThhb3llSDVjTks5aVV2OWQrZVAwTm1ES1AxVVE3SXJWbWIxCnhRUW9XazBJSG05N1MyQ1pGNWpWbnpuUWNrN09NNUpjamZrS2FCZjllNmhMU0dIc0VoUkF6UXB1T3BKNVhDVDMKVTdISS9ZWGhqM1JsSWRDOFE5alZuT0pkTW92VDhQNFVhTTdjaGlCL1ZsTWJnb2lYdWNBMk1lZlJFdVlTWnQ4TgpaRXpsNk9LYUpHc283dXIyK2FNTUJKeTlkNHRpRWJwRXlnN0l3emdocTNaVVdFbWFIcWRENk9aeE9HYWQzMGRSCmlXVmZRd0R2WjloWTlSQ1NvMHNGY1hiRWVDZkhWL29NaWZNcFNWd202SVo2Rk1PRC9SR09WaW8zcmE4SUFINVgKbGhyNitubC9pbUlMRWNadURaaXBGL2szS0FaSE5nPT0KLS0tLS1FTkQgUFJJVkFURSBLRVktLS0tLQo=';
        $res = json_decode($result);
        Log::debug(print_r($res, true));
//
        if ($res->status == 400) {
            return false;
        } else {
//            openssl_private_decrypt(base64_decode($res->linkedin), $response, base64_decode($priv_key));
            return (json_decode($res->results));
        }
    }
}