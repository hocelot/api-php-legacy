<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 20/10/16
 * Time: 12:25
 */

namespace App\Http\Controllers\SocialMedia;


use App\Http\Controllers\Proxies\Proxies;
use Goutte\Client;
use stdClass;
use Log;
use Symfony\Component\DomCrawler\Crawler;

class Facebook
{
    private $email, $phone;
    public $results;

    public function __construct($mail, $phone)
    {
        $this->email = $mail;
        $this->phone = $phone;
    }

    public function search()
    {
        if ($alias = $this->searchAlias($this->email)) {
//            $this->results = $this->normalizeData($alias);
//            $this->results = $alias;
            return $this->normalizeData($alias);
        } elseif ($alias = $this->searchAlias($this->phone)) {
//            $this->results = $this->normalizeData($alias);
            return $this->normalizeData($alias);
        } else {
            return false;
        }
    }

    private function searchAlias($query)
    {
        $data = $this->curl('http://www.facebook.com/search/people/?q=' . $query, true);
        if (empty($data)) return false;
        preg_match_all('/href=\"https:\/\/www.facebook.com\/(([^\"\/]+)|people\/([^\"]+\/\d+))[\/]?\"/', $data, $matches);
        $only_url = true;
        foreach ($matches as $match) {
            foreach ($match as $m)
                if (!stristr($m, 'https%3A%2F%2Finstagram.com')) $only_url = false;
        }
        if ($only_url) return false;
        if ($matches[3][0] != FALSE) {                // facebook.com/people/name/id
            $pages = array_map(function ($el) {
                return explode('/', $el)[0];
            }, $matches[3]);
        } else                                      // facebook.com/name
            $pages = $matches[2];
        $result = array_filter(array_unique($pages));
        if (isset($result[1]) && !empty($result[1])) {
            return $result[1];
        }
        //*[@id="all_search_results"]/div/div/div/div/a
//        $crawler = new Crawler();
//        $crawler->addHtmlContent($data);
//        if($crawler->filterXPath("//*[@id=\"all_search_results\"]/div/div/div/div/a")->count() > 0)
//        return $crawler->filterXPath("//*[@id=\"all_search_results\"]/div/div/div/div/a")->first()->href();
//        else
        return false;
    }

    private function normalizeData($alias)
    {
        $crawler = new Crawler();
        $crawler->addHtmlContent($this->curl('http://www.facebook.com/' . $alias));
        $facebook = new stdClass();

        if ($crawler->filterXPath('//html/body/div/div[@id=\'globalContainer\']/div[@id=\'content\']/div/div[@id=\'mainContainer\']/div[@id=\'contentCol\']/div[@id=\'contentArea\']/div/div[1]/div[1]/div[3]/div/div[@id=\'fbTimelineHeadline\']/div[3]/div/div/div/img')->count())
            $facebook->cover_image = $crawler->filterXPath('//html/body/div/div[@id=\'globalContainer\']/div[@id=\'content\']/div/div[@id=\'mainContainer\']/div[@id=\'contentCol\']/div[@id=\'contentArea\']/div/div[1]/div[1]/div[3]/div/div[@id=\'fbTimelineHeadline\']/div[3]/div/div/div/img')->attr('src');

        if ($crawler->filterXPath('//html/body/div/div[@id=\'globalContainer\']/div[@id=\'content\']/div/div[@id=\'mainContainer\']/div[@id=\'contentCol\']/div[@id=\'contentArea\']/div/div[1]/div[1]/div[3]/div/div[@id=\'u_0_5\']/div/h1/a/span')->count())
            $facebook->name = $crawler->filterXPath('//html/body/div/div[@id=\'globalContainer\']/div[@id=\'content\']/div/div[@id=\'mainContainer\']/div[@id=\'contentCol\']/div[@id=\'contentArea\']/div/div[1]/div[1]/div[3]/div/div[@id=\'u_0_5\']/div/h1/a/span')->text();

        if ($crawler->filterXPath('//html/body/div/div[@id=\'globalContainer\']/div[@id=\'content\']/div/div[@id=\'mainContainer\']/div[@id=\'contentCol\']/div[@id=\'contentArea\']/div/div[1]/div[1]/div[3]/div/div[@id=\'fbTimelineHeadline\']/div[3]/div/div/div/img')->count())
            $facebook->profile_pic = $crawler->filterXPath('//html/body/div/div[@id=\'globalContainer\']/div[@id=\'content\']/div/div[@id=\'mainContainer\']/div[@id=\'contentCol\']/div[@id=\'contentArea\']/div/div[1]/div[1]/div[3]/div/div[@id=\'fbTimelineHeadline\']/div[3]/div/div/div/img')->attr('src');

        $facebook->alias[] = $alias;
        // Si acaba con ) hay un alias
        if (substr(trim($facebook->name), -1) == ')') {
            $searx_ap = strpos($facebook->name, '(');
            $facebook->alias[] = trim(str_replace(')', '', substr(trim($facebook->name), $searx_ap + 1)));
            $facebook->name = trim(str_replace(substr(trim($facebook->name), $searx_ap), '', trim($facebook->name)));
        }

        //Empleo
        $crawler->filterXPath('//html/body/div/div[@id=\'globalContainer\']/div[@id=\'content\']/div/div[@id=\'mainContainer\']/div[@id=\'contentCol\']/div[@id=\'contentArea\']/div/div[2]/div/div[1]/div[@id=\'pagelet_timeline_medley_about\']/div[@id=\'collection_wrapper_2327158227\']/div/div/div[@id=\'pagelet_eduwork\']/div/div[1]/ul/li')
            ->each(function ($node) use ($facebook) {
                $job = new stdClass();
                $nom = $node->filter('div > div > div > div > div')->eq(1)->filter('div > a');

                if ($nom->count() > 0)
                    $job->name = $nom->text();
                else $job->name = false;

                $fech = $node->filter('div > div > div > div > div')->eq(1)->filter('div')->eq(2)->filter('div');
                if ($fech->count() > 0)
                    $job->date = $fech->text();
                else $job->date = false;


                $facebook->jobs[] = $job;
            });

        //Studies
        $crawler->filterXPath('//html/body/div/div[@id=\'globalContainer\']/div[@id=\'content\']/div/div[@id=\'mainContainer\']/div[@id=\'contentCol\']/div[@id=\'contentArea\']/div/div[2]/div/div[1]/div[@id=\'pagelet_timeline_medley_about\']/div[@id=\'collection_wrapper_2327158227\']/div/div/div[@id=\'pagelet_eduwork\']/div/div[2]/ul/li')
            ->each(function ($node) use ($facebook) {
                $study = new stdClass();
                $li_s = $node->filter('div > div > div > div > div')->eq(1);
                $ins = $li_s->filter('div > a');
                $sub = $li_s->filter('div')->eq(2);

                if ($ins->count() > 0)
                    $study->institution = $ins->text();
                else  $study->institution = false;

                if ($sub->count() > 0)
                    $study->sub = $sub->text();
                else  $study->sub = false;

                $facebook->studies[] = $study;
            });

        //Lugares
        $crawler->filterXPath('//html/body/div/div[@id=\'globalContainer\']/div[@id=\'content\']/div/div[@id=\'mainContainer\']/div[@id=\'contentCol\']/div[@id=\'contentArea\']/div/div[2]/div/div[1]/div[@id=\'pagelet_timeline_medley_about\']/div[@id=\'collection_wrapper_2327158227\']/div/div/div[@id=\'pagelet_hometown\']/div/div/ul')
            ->each(function ($node) use ($facebook) {
                $node->filter('li')->each(function ($n) use ($facebook) {
                    $place = new stdClass();
                    $li_s = $n->filter('div > div > div > div > div')->eq(1);
                    $name = $li_s->filter('span > a');
                    $status = $li_s->filter('div > div');

                    if ($name->count() > 0)
                        $place->name = $name->text();
                    if ($status->count() > 0)
                        $place->status = $status->text();

                    if (!isset($place->name) && empty($place->name)) {
                        $li_s = $n->filter('div > div > div > div > div > div')->eq(1);
                        $name = $li_s->filter('span > a');
                        $status = $li_s->filter('div > div');

                        if ($name->count() > 0)
                            $place->name = $name->text();
                        if ($status->count() > 0)
                            $place->status = $status->text();
                    }

                    $facebook->places[] = $place;

                });

            });

        $crawler->filterXPath('//html/body/div/div[@id=\'globalContainer\']/div[@id=\'content\']/div/div[@id=\'mainContainer\']/div[@id=\'contentCol\']/div[@id=\'contentArea\']/div/div[2]/div/div[1]/div[@id=\'pagelet_all_favorites\']/div/div/div[2]/table/tbody')
            ->each(function ($node) use ($facebook) {
                $fav = new stdClass();
                $fav->titulo = $node->filter('tr > th')->text();
                $node->filter('td > div > ul > li')->each(function ($n) use ($fav, $facebook) {
                    $facebook->details[$fav->titulo] = $n->filter('div')->text();
                });
                $node->filter('td > div > span')->each(function ($n) use ($fav, $facebook) {
                    $n->filter('a')->each(function ($a) use ($fav, $facebook) {
                        $facebook->details[$fav->titulo][] = $a->text();
                    });
                });
            });
        return $facebook;
    }

    private function curl($url, $alias = false)
    {
        $user_agent = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.109 Safari/537.36';
        $count = 0;
        if (!$alias) {
            $c = curl_init();
            curl_setopt_array($c, array(
                CURLOPT_URL => $url,
                CURLOPT_USERAGENT => $user_agent,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_FOLLOWLOCATION => TRUE,
                CURLOPT_SSL_VERIFYPEER => FALSE
            ));

            $data = curl_exec($c);
            curl_close($c);
        } else {
            $data = '';
            $obj_proxy = new Proxies('fb');
            $ip_real = false;
            do {
                $ip = $obj_proxy->get();
                $obj_proxy->setUsed($ip);
                $c = curl_init();
                curl_setopt_array($c, array(
                    CURLOPT_URL => $url,
                    CURLOPT_USERAGENT => $user_agent,
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_FOLLOWLOCATION => TRUE,
                    CURLOPT_SSL_VERIFYPEER => FALSE
                ));
                if ($ip) {
                    curl_setopt($c, CURLOPT_PROXY, $ip);
                    curl_setopt($c, CURLOPT_PROXYUSERPWD, "mrhc:QwEr1234");
                } else  $ip_real = true;
                $data = curl_exec($c);
                $info = curl_getinfo($c);
                curl_close($c);
                if (isset($crawler)) unset($crawler);
                $crawler = new Crawler();
                $crawler->addHtmlContent($data);
                $count++;
                if (!$ip_real) {
                    if ($crawler->filter('#captcha_persist_data')->count() < 1)
                        $obj_proxy->unlock(200);
                    else $obj_proxy->unlock(999);
                }
            } while ($crawler->filter('#captcha_persist_data')->count() > 0 && !$ip_real);
            if ($crawler->filter('#captcha_persist_data')->count() > 0 && $ip_real) $data = false;
        }

        return $data;
    }

}