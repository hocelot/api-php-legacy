<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 6/09/16
 * Time: 19:23
 */

namespace App\Http\Controllers\SocialMedia;

use Log;

class eTools extends \Threaded
{
// Busqueda1 = ("52889883A" AND "JULIA" AND "VERDEJO" AND "SINTAS")
//Busqueda2 solo primer resultado= site:facebook.com ("EDMUNDO LUJAN OCEJO") intitle:"EDMUNDO LUJAN" -inurl:"public" -inurl:"people"
//http://gplus.slfeed.net/?id=ENRIQUE+MU%C3%91OZ+HURTADO
    public $results;
    private $first_name, $last_name, $name, $dni;
    public function __construct($name, $first_name, $last_name)
    {
        $this->name = $name;
        $this->first_name = $first_name;
        $this->last_name = $last_name;
//        $this->dni = $dni;
    }

    public function run(){
        require_once(__DIR__ . '/../../../../vendor/autoload.php');
        require_once(__DIR__ . '/../../../../bootstrap/app.php');
        $this->worker->addData('search', $this->getSearch());
//        $this->results = $this->getSearch();
    }
    private function getSearch(){
        $site = 'http://www.etools.ch';
//        $str_search = urlencode('("'.$this->dni.'" AND "'.$this->name.'" AND "'.$this->first_name.'" AND "'.$this->last_name.'")');
        $str_search = urlencode('"'.$this->name.' '.$this->first_name.' '.$this->last_name.'"');
        $search = $this->curl($site . '/partnerSearch.do?partner=TestAdvanced&query=' . $str_search . '&language=es&country=web&includeClusters=true');//country=web para todos //&maxRecords=200
        $res = new \SimpleXMLElement($search);
        $etool = new \stdClass();
        $etool->hits = array();
        foreach ($res->mergedRecords->record as $record){
            $r = new \stdClass();
            $r->title = (string)$record->title;
            $r->text = (string)$record->text;
            $r->url = (string)$record->url;
            $etool->hits[] = $r;

        }
            return $etool;
    }
    private
    function curl($page)
    {
        $url = $page;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }


}