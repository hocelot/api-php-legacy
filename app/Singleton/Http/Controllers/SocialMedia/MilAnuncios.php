<?php
/**
 * Created by PhpStorm.
 * User: rmarcos
 * Date: 7/10/16
 * Time: 11:47
 */

namespace App\Http\Controllers\SocialMedia;

use App\Http\Controllers\Proxies\Proxies;
use Symfony\Component\DomCrawler\Crawler;
use Log;

class MilAnuncios extends \Threaded
{
    // http://www.milanuncios.com/anuncios/tlf.htm
    private $nombre, $ape1, $tlf;

    public function __construct($nombre, $ape1, $mobile)
    {
        $this->nombre = trim($nombre);
        $this->ape1 = trim($ape1);
        $this->tlf = trim($mobile);
    }

    public function run()
    {
        require_once(__DIR__ . '/../../../../vendor/autoload.php');
        require_once(__DIR__ . '/../../../../bootstrap/app.php');
        $this->worker->addData('milanuncios', $this->search());
    }

    private function search()
    {
        if (!($result = $this->search_norm())) {
            $status = 400;
            $result = 'No encontrado';
        } else {
            $status = 200;
        }
        return ['hits' => $result, 'status' => $status];
    }

    private function search_norm()
    {
        $html_web = 'http://www.milanuncios.com/anuncios/' . $this->tlf . ".htm";
        Log::debug($html_web);
        $milanuncio = new \stdClass();
        $milanuncio->anuncio = array();
        $html_search = new Crawler();
        if (!($web_url = $this->curl($html_web))) return false;
        $html_search->addHtmlContent($web_url);
        $titulos = array();
        $html_search->filter('#cuerpo > .aditem')->each(function ($node) use ($milanuncio, $titulos) {
            $titulo = $node->filter('.aditem-detail > .aditem-detail-title')->first()->text();
            $url = $node->filter('.aditem-detail > .aditem-detail-title')->first()->attr('href');
            if (!in_array(trim($titulo), $titulos)) {
                $titulos[] = trim($titulo);
                $anuncio = new \stdClass();
                $anuncio->titulo = trim($titulo);
                $anuncio->url = trim('http://www.milanuncios.com' . $url);
                $milanuncio->anuncio[] = $anuncio;
            }
        });
        if (count($milanuncio->anuncio) > 0) {
            foreach ($milanuncio->anuncio as $anuncio) {
                unset($crawler);
                $crawler = new Crawler();
                if (!($anuncio_url = $this->curl($anuncio->url))) continue;
                $crawler->addHtmlContent($anuncio_url);
                //Tipo PROFESIONAL O PARTICULAR y Nombre

                if ($crawler->filter('.contenido > .contentInnerBoxPagAnu > #cuerpo > .pagAnuRightColumn > .pagAnuContact >.pagAnuContactNombre')->count() > 0) {
                    $anuncio->contact_name = $crawler->filter('.contenido > .contentInnerBoxPagAnu > #cuerpo > .pagAnuRightColumn > .pagAnuContact >.pagAnuContactNombre')->first()->text();
                    $cmp_name = $this->nombre . ' ' . $this->ape1;
                    similar_text(mb_strtoupper($anuncio->contact_name, 'UTF-8'), $cmp_name, $percent);
                    $anuncio->correlacion = round($percent, PHP_ROUND_HALF_EVEN) . '%';

                }
//                if($crawler->filter('.contenido > .contentInnerBoxPagAnu > #cuerpo > .pagAnuRightColumn >.pagAnuContact > div > .pagAnuContactSellerType')->count() > 0) {
//                    $anuncio->contact_type = $crawler->filter('.contenido > .contentInnerBoxPagAnu > #cuerpo > .pagAnuRightColumn >.pagAnuContact > div > .pagAnuContactSellerType')->first()->text();
//                }

                //Quitamos urls
                unset($anuncio->url);

                $cat_sub = $crawler->filter('.contenido > .cab > .beacrumb-container > .beacrumb > a');
                if ($cat_sub->count() >= 3) {
                    $anuncio->categoria = $cat_sub->eq(1)->text();
                    $anuncio->subcategoria = $cat_sub->eq(2)->text();
                } elseif ($cat_sub->count() == 2) {
                    $anuncio->categoria = $cat_sub->eq(1)->text();
                } else {
                    continue;
                }
            }
            return $milanuncio;
        } else {
            //No se encontraron anuncios
            return false;
        }
    }

    private function curl($page)
    {
        $obj_proxy = new Proxies('milanuncios');
        $ip_real = false;
        do {
            $ip = $obj_proxy->get();
            $obj_proxy->setUsed($ip);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $page);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            if ($ip) {
                curl_setopt($ch, CURLOPT_PROXY, $ip);
                curl_setopt($ch, CURLOPT_PROXYUSERPWD, "mrhc:QwEr1234");
            } else  $ip_real = true;
            $result = curl_exec($ch);
            $info = curl_getinfo($ch);
            curl_close($ch);
            if (!$ip_real) {
                $obj_proxy->unlock($info["http_code"]);
            }
        } while ($info["http_code"] != 200 && !$ip_real);
        if ($info["http_code"] != 200 && $ip_real) return false;
        return $result;
    }
}