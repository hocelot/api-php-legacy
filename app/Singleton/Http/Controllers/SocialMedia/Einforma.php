<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 6/09/16
 * Time: 13:55
 */

namespace App\Http\Controllers\SocialMedia;

use Goutte\Client;
use Thread;
use Log;

class Einforma
{
    private $client;
    public function __construct()
    {
        $this->client = new Client();
        $guzzleClient = new \GuzzleHttp\Client(array(
            'request.options' => array(
                'proxy' => '134.58.64.158:80'
            ),
            'curl' => array(
                CURLOPT_SSL_VERIFYPEER => false,
            ),
        ));
        $this->client->setClient($guzzleClient);
        $this->client->request('POST', 'https://www.einforma.com/servlet/app/screen/SProductoAJAX/prod/LOGIN_XML/', array('username' => 'presidencia@vinoloa.com', 'password' => '00001962', 'recordar' =>'false'));
        // Log::debug(print_r($auth->html()));
    }

    public function parseEinforma($name)
    {
        $client = $this->client;
        //$name = 'cyprea';
        $name = str_replace(' ', '+', $name);
        $html_web = 'https://www.einforma.com/servlet/app/prod/LISTA_EMPRESAS/razonsocial/' . $name;
        $html_search = $client->request('GET', $html_web);
        if($html_search->filter('#nacional > tbody > tr')->count() > 0) {
            $html = $html_search->filter('#nacional > tbody > tr')->attr('url');
        }else{
            return 'No encontrado';
        }
        $html_web_search = $client->request('GET', 'https://www.einforma.com/' . $html);
        $info = new \stdClass();
        $html_web_search->filter('#datos > tr')->each(function ($node) use ($info) {
            if (stristr($node->text(), 'CIF:')) {
                $info->cif = trim(str_replace('CIF:', ' ', $node->text()));
            }
            if (stristr($node->text(), 'Localidad:')) {
                $info->localidad = trim(str_replace('Localidad:', ' ', $node->text()));
            }
            if (stristr($node->text(), 'Domicilio social actual:')) {
                $info->domicilio_social = trim(str_replace('Ver Mapa', '', str_replace('Domicilio social actual:', ' ', $node->text())));
            }
            if (stristr($node->text(), 'Tel�fono:')) {
                $info->telefono = trim(str_replace('Tel�fono:', ' ', $node->text()));
            }
            if (stristr($node->text(), 'Administrador �nico:')) {
                $info->adm_unico = trim(str_replace('Administrador �nico:', ' ', $node->text()));
            }

            if (stristr($node->text(), 'Accionistas:')) {
                $info->num_accionistas = trim(str_replace('Accionistas', ' ', str_replace('Constan', ' ', str_replace('Accionistas:', ' ', $node->text()))));
            }
            if (stristr($node->text(), 'Capital Social:')) {
                $info->capital = trim(str_replace('Capital Social:', ' ',$node->text()));
            }
            if (stristr($node->text(), 'Resultado �ltimo A�o:')) {
                $info->resultados_ultimo_a = trim(str_replace('Resultado �ltimo A�o:', ' ', $node->text()));
            }

            if (stristr($node->text(), 'Rango Ventas:')) {
                $info->rango_ventas = trim(str_replace('Rango Ventas:', ' ', $node->text()));
            }

            if (stristr($node->text(), 'N�mero de Empleados:')) {
                $info->num_empleados = trim(str_replace('N�mero de Empleados:', ' ', $node->text()));
            }

            if (stristr($node->text(), 'Fecha Constituci�n:')) {
                $info->fech_constitucion = trim(str_replace('Fecha Constituci�n:', ' ', $node->text()));
            }

            if (stristr($node->text(), 'CNAE 2009:')) {
                $info->cnae = trim(str_replace('CNAE 2009:', ' ', $node->text()));
            }
//            if(stristr($node->text(),'�ltimo Balance cargado:')) {
//                $info->ultimo_balance_cargado = trim(str_replace('�ltimo Balance cargado:',' ',$node->text()));
//            }
            if (stristr($node->text(), 'Forma Jur�dica:')) {
                $info->forma_juridica = trim(str_replace('Forma Jur�dica:', ' ', $node->text()));
            }
            if (stristr($node->text(), 'Actividad Informa:')) {
                $info->actividad_1 = trim(str_replace('Actividad Informa:', ' ', $node->text()));
            }
//            if(stristr($node->text(),'Balances disponibles:')) {
//                $info->balances_disp = trim(str_replace('Balances disponibles:',' ',$node->text()));
//            }
            if (stristr($node->text(), 'Objeto Social:')) {
                $info->obj_social = trim(str_replace('Objeto Social:', ' ', $node->text()));
            }
            if (stristr($node->text(), 'Dep�sito en R. Mercantil:')) {
                $info->deposito_r_mercantil = trim(str_replace('Dep�sito en R. Mercantil:', ' ', $node->text()));
            }
            if (stristr($node->text(), 'Popularidad:')) {
                $info->trafico = trim(str_replace(' y ', ' / ', str_replace('veces en total', '', str_replace('Popularidad:Esta empresa ha sido consultada por �ltima vez el ', ' ', $node->text()))));
            }
        });
        if (empty($info->cif)) {
            return 'No encontrado';
        } else {
            return json_encode($info);
        }
    }
    private function parseEinformacif($url){
        $client = new Client();
        $guzzleClient = new \GuzzleHttp\Client(array(
            'request.options' => array(
               // 'proxy' => '217.61.0.175:1718'
            ),
            'curl' => array(
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_COOKIEJAR => 'cookie_einforma.txt',
                CURLOPT_COOKIEFILE => 'cookie_einforma.txt',
            ),
        ));
        $client->setClient($guzzleClient);
        $html_web_search = $client->request('GET', $url);
        $info = new \stdClass();
        $html_web_search->filter('#datos > tr')->each(function ($node) use ($info) {
            //echo $node->text(), PHP_EOL;
            if (stristr($node->text(), 'CIF:')) {
                $info->cif = trim(str_replace('CIF:', ' ', $node->text()));
            }

            if (stristr($node->text(), 'Localidad:')) {
                $info->localidad = trim(str_replace('Localidad:', ' ', $node->text()));
            }
            if (stristr($node->text(), 'Domicilio social actual:')) {
                $info->domicilio_social = trim(str_replace('Ver Mapa', '', str_replace('Domicilio social actual:', ' ', $node->text())));
            }
            if (stristr($node->text(), 'Teléfono:')) {
                $info->telefono = trim(str_replace('Teléfono:', ' ', $node->text()));
            }
            if (stristr($node->text(), 'Administrador Único:')) {
                $info->adm_unico = trim(str_replace('Administrador Único:', ' ', $node->text()));
            }

            if (stristr($node->text(), 'Accionistas:')) {
                $info->num_accionistas = trim(str_replace('Accionistas', ' ', str_replace('Constan', ' ', str_replace('Accionistas:', ' ', $node->text()))));
            }

            if (stristr($node->text(), 'Resultado Último Año:')) {
                $info->resultados_ultimo_a = trim(str_replace('Resultado Último Año:', ' ', $node->text()));
            }

            if (stristr($node->text(), 'Rango Ventas:')) {
                $info->rango_ventas = trim(str_replace('Rango Ventas:', ' ', $node->text()));
            }

            if (stristr($node->text(), 'Número de Empleados:')) {
                $info->num_empleados = trim(str_replace('Número de Empleados:', ' ', $node->text()));
            }

            if (stristr($node->text(), 'Fecha Constitución:')) {
                $info->fech_constitucion = trim(str_replace('Fecha Constitución:', ' ', $node->text()));
            }

            if (stristr($node->text(), 'CNAE 2009:')) {
                $info->cnae = trim(str_replace('CNAE 2009:', ' ', $node->text()));
            }
//            if(stristr($node->text(),'Último Balance cargado:')) {
//                $info->ultimo_balance_cargado = trim(str_replace('Último Balance cargado:',' ',$node->text()));
//            }
            if (stristr($node->text(), 'Forma Jurídica:')) {
                $info->forma_juridica = trim(str_replace('Forma Jurídica:', ' ', $node->text()));
            }
            if (stristr($node->text(), 'Actividad Informa:')) {
                $info->actividad_1 = trim(str_replace('Actividad Informa:', ' ', $node->text()));
            }
//            if(stristr($node->text(),'Balances disponibles:')) {
//                $info->balances_disp = trim(str_replace('Balances disponibles:',' ',$node->text()));
//            }
            if (stristr($node->text(), 'Objeto Social:')) {
                $info->obj_social = trim(str_replace('Objeto Social:', ' ', $node->text()));
            }
            if (stristr($node->text(), 'Depósito en R. Mercantil:')) {
                $info->deposito_r_mercantil = trim(str_replace('Depósito en R. Mercantil:', ' ', $node->text()));
            }
            if (stristr($node->text(), 'Popularidad:')) {
                $info->trafico = trim(str_replace(' y ', ' / ', str_replace('veces en total', '', str_replace('Popularidad:Esta empresa ha sido consultada por última vez el ', ' ', $node->text()))));
            }

        });
        if (empty($info)) {
            return 'No encontrado';
        } else {
            return $info;
        }
    }

}