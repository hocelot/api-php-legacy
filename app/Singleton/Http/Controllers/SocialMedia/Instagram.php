<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 2/12/16
 * Time: 10:27
 */

namespace App\Http\Controllers\SocialMedia;


class Instagram extends \Threaded
{
    private $email, $full_name;

    public function __construct($full_name, $email)
    {
        $arroba = strpos($email, '@');
        $this->email = substr($email, 0, $arroba);
        $this->full_name = $full_name;
    }

    public function run()
    {
        require_once(__DIR__ . '/../../../../vendor/autoload.php');
        require_once(__DIR__ . '/../../../../bootstrap/app.php');
        $this->worker->addData('instagram', $this->searchUser());

    }

    private function searchUser()
    {
        $instagram =array();
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://www.instagram.com/web/search/topsearch/?context=blended&query=" . $this->email . "&rank_token=0.4",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "accept: */*",
                "accept-encoding: gzip, deflate, sdch, br",
                "accept-language: es-ES,es;q=0.8",
                "authority: www.instagram.com",
                "cache-control: no-cache",
                "referer: https://www.instagram.com/gorda/",
                "user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36",
                "x-requested-with: XMLHttpRequest"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if (!$err) {
            $instagram['email']=$response;
        }
        return $instagram;
    }
}