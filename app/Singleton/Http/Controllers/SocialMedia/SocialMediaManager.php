<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 26/11/16
 * Time: 17:25
 */

namespace App\Http\Controllers\SocialMedia;


use App\Http\Controllers\Worker\ThreadsManager;

class SocialMediaManager extends \Threaded
{
    private $name, $first_name, $last_name, $email, $phone, $city, $facebook;

    public function __construct($facebook_results, $name, $first_name, $last_name, $email, $phone, $city)
    {
        $this->name = $name;
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->email = $email;
        $this->phone = $phone;
        $this->city = $city;
        $this->facebook = $facebook_results;
    }

    public function run()
    {
        //Aqui lanzaria los hilos
        require_once(__DIR__ . '/../../../../vendor/autoload.php');
        require_once(__DIR__ . '/../../../../bootstrap/app.php');
//        $linkedin = new Linkedin($this->name, $this->first_name, $this->last_name, $this->city);
//        $result_link = $linkedin->initialize();
        $worker = new ThreadsManager();


        $milanuncios = new MilAnuncios($this->name, $this->first_name, $this->phone);
        $worker->stack($milanuncios);


        $libreborme = new Libreborme($this->name, $this->first_name, $this->last_name);
        $worker->stack($libreborme);


//        $relaciones = new FindRelations($this->name, $this->first_name, $this->last_name);
//        $worker->stack($relaciones);

//        $instagram = new Instagram($this->name.' '.$this->first_name.' '.$this->last_name,$this->email);
//        $worker->stack($instagram);

//        $etools = new eTools($this->name,$this->first_name,$this->last_name);
//        $worker->stack($etools);

        // Start all jobs
        $worker->start();
        // Join all jobs and close worker
        $worker->shutdown();
        $result = [];

//        $result = ['linkedin'=>$result_link];
        foreach ($worker->data as $key => $value) {
            $result = array_merge($result, [$key => $value]);
        }
        //Worker heredado
        $this->worker->addData('social2', $result);

    }

}