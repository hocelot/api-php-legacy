<?php
/**
 * Created by PhpStorm.
 * User: a.luis
 * Date: 15/06/2016
 * Time: 13:54
 */

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use App\Http\Models\Account\BillModel;
use Illuminate\Http\Request;
use Log;

class BillController extends Controller
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
    }

    public function getBill()
    {
        $resp = BillModel::getBills(json_decode($this->request_params->client_id));
        if ($resp->status == 200) {
            if (!isset($resp->data->val_astr_mr_compras->str_facturas)) {
                $resp->empty = true;
                return response()->json(['bills' => $resp, 'status' => $resp->status]);
                //return response()->json(['bills' => $resp->data, 'status' => $resp->status, "empty" => true]);
            } else {
                return response()->json(['bills' => $resp, 'status' => $resp->status]);
            }
        } else {
            return response()->json(['bills' => $resp, 'status' => $resp->status]);
        }
    }

    public function getDataBill()
    {
        $resp = BillModel::getDataBill(json_decode($this->request_params->numFact));
        if ($resp->status == 200) {
            return response()->json(['bill' => $resp, 'status' => $resp->status]);
        } else {
            return response()->json(['bill' => $resp, 'status' => $resp->status]);
        }
    }
}