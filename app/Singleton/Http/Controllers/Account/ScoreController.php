<?php
/**
 * Created by PhpStorm.
 * User: DC
 * Date: 25/01/2017
 * Time: 16:11
 */
namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use App\Http\Models\Account\ApiModel;
use Log;

class ScoreController extends Controller {
    public function getConsumo()
    {
        $resp = ApiModel::getConsumo(json_decode($this->request_params->client_id));
        if ($resp->status == 200) {
            if(!isset($resp->data)){
                $resp->empty = true;
                return response()->json(['data' => $resp, 'status' => $resp->status]);
            }else{
                return response()->json(['data' => $resp, 'status' => $resp->status]);

            }
        }else{
            return response()->json(['msg' => $resp, 'status' => $resp->status]);
        }
    }
}