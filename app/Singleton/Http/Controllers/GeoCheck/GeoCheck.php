<?php

namespace App\Http\Controllers\GeoCheck;

use App\Http\Controllers\Proxies\Proxies;
use App\Http\Models\GeoCheck\GeoCheckModel;
use App\Http\Models\UtilClass;
use Log;

/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 29/10/16
 * Time: 10:49
 */
class GeoCheck extends \Threaded {

    private $type, $used_for, $m2, $lat, $long, $town, $province, $ajax, $letterinpnp, $zip_code, $level, $road_type, $street, $number, $block, $km, $letter, $stair, $floor, $door, $ine_code, $parcela;

    /*
                         * Niveles de exactitud:
                         *  1 - Exacto
                         *  2 - Hasta Puerta, pero falta KM o BLOQUE o ESCALERA o LETRA
                         *  3 - Hasta Planta
                         *  4 - Hasta Numero
                         *  5 - Inexistente
                          */
    public function __construct($params, $ajax = false)
    {
        $this->ajax = $ajax;
        $arr_s = explode('(', $params->street);
        $params->street = $arr_s[0];
        $this->road_type = mb_strtoupper(UtilClass::elimina_acentos(trim($params->road_type)), 'UTF-8');
        $this->street = mb_strtoupper(UtilClass::elimina_acentos(trim($params->street)), 'UTF-8');
//        Log::debug('NOMBRE VIA  ' . $this->street);
        $this->number = intval(mb_strtoupper(trim($params->number), 'UTF-8'));
        $this->letter = mb_strtoupper(trim($params->letter), 'UTF-8');
        // Quitamos letras del primer numero policia
        if (preg_match('/([A-Z]|[a-z])+/', $this->number))
        {
            $prob_letter = $this->number;
            $prob_pnp = preg_replace('/([A-Z]|[a-z])+/', '', $this->number);
            foreach (str_split($prob_pnp) as $number)
            {
                $prob_letter = str_replace($number, '', $prob_letter);
            }
            if (empty($this->letter))
            {
                $this->letter = $prob_letter;
            }
            else
            {
                $this->letterinpnp = $prob_letter;
            }
            $this->number = $prob_pnp;
        }
        $this->block = mb_strtoupper(trim($params->block), 'UTF-8');
        $this->stair = mb_strtoupper(trim($params->stair), 'UTF-8');
        $this->km = mb_strtoupper(trim($params->km), 'UTF-8');
        $this->floor = mb_strtoupper(trim($params->floor), 'UTF-8');
        $this->door = mb_strtoupper(trim($params->door), 'UTF-8');
        $this->zip_code = str_pad(trim($params->zip_code), 5, '0', STR_PAD_LEFT);
        $this->province = mb_strtoupper(UtilClass::elimina_acentos(trim($params->province)), 'UTF-8');
        $this->town = mb_strtoupper(UtilClass::elimina_acentos(trim($params->town)), 'UTF-8');
        $this->ine_code = '';
        if ( ! empty($this->floor))
            $this->type = 'flat';
        else $this->type = 'house';
        $this->m2 = $this->lat = $this->long = 0;
        $this->used_for = 'n/c';
        $this->parcela = '';
        $this->level = 5;

    }

    public function run()
    {
        require_once(__DIR__ . '/../../../../vendor/autoload.php');
        require_once(__DIR__ . '/../../../../bootstrap/app.php');
        $this->worker->addData('geo_check', $this->normalizeAddress());
    }

//    public function test()
//    {
//        $res = $this->normalizeAddress();
//        var_dump($res);
//        return $res;
//    }

    private function normalizeAddress()
    {
        /* Paso a paso:
         *  1º- Buscamos codigo ine y poblacion
         *  2º- Normalizamos tipo de via
         *  3º- Buscamos el nombre de la via
         *
        */

//        $fail = true;
        $otro_catastro = false;
        $resp = array();
        if (empty($this->street) || empty($this->road_type))
        {
            $resp = array(
                'address'   => trim(preg_replace('!\s+!', ' ', $this->road_type . ' ' . html_entity_decode($this->street) . ' ' . $this->number . ' ' . $this->letter . ' ' . $this->km . ' ' . $this->block . ' ' . $this->stair . ' ' . $this->floor . ' ' . $this->door . ', ' . $this->zip_code . ', ' . $this->town . ', ' . $this->province)),
                'm2'        => 0,
                'latitude'  => 0,
                'longitude' => 0,
                'used_for'  => 'n/c',
                'type'      => 'none',
                'level'     => 5,
                'ine'       => $this->ine_code
            );

            return ['hits' => $resp, 'status' => 400];
        }
        if ($this->normalizeTown())
        {
//            if ($this->ajax) {
//                $resp = GeoCheckModel::getDataAjax($this->ine_code, $this->road_type, $this->street, $this->number, $this->letter, $this->km, $this->block, $this->stair, $this->floor, $this->door);
////                if ($resp) {
////                    $fail = false;
//                $resp['address'] = trim(str_replace('  ', '', $this->road_type . ' ' . $this->street . ' ' . $this->number . ' ' . $this->letter . ' ' . $this->km . ' ' . $this->block . ' ' . $this->stair . ' ' . $this->floor . ' ' . $this->door . ', ' . $this->zip_code . ', ' . $this->town . ', ' . $this->province));
//                $resp['type'] = $this->type;
//                $resp['ine'] = $this->ine_code;
////                }
//            } else {

            if (substr($this->ine_code, 0, 2) != '01' && substr($this->ine_code, 0, 2) != '20'
                || substr($this->ine_code, 0, 2) != '31' && substr($this->ine_code, 0, 2) != '48'
            )
            {

                if ($this->normalizeRoadType())
                {
                    $resp = $this->searchAddress();
//                    if ($resp) $fail = false;
                }

            }
            else $otro_catastro = true;
//            }
        }
        else
        {
            $resp = array(
                'address'   => $this->road_type . ' ' . html_entity_decode($this->street) . ' ' . $this->number . ' ' . $this->letter . ' ' . $this->km . ' ' . $this->block . ' ' . $this->stair . ' ' . $this->floor . ' ' . $this->door . ', ' . $this->zip_code . ', ' . $this->town . ', ' . $this->province,
                'm2'        => 0,
                'latitude'  => 0,
                'longitude' => 0,
                'used_for'  => 'n/c',
                'type'      => 'none',
                'level'     => 7,
                'ine'       => '00000'
            );
        }

//        Log::debug('ANTES DEL ISSET '.print_r($resp));
        if ( ! isset($resp['m2']))
        {
            if ( ! $otro_catastro)
                $resp = $this->findAddress_Catastro();
            if ( ! isset($resp['m2']))
            {
                $resp = array(
                    'address'   => $this->road_type . ' ' . html_entity_decode($this->street) . ' ' . $this->number . ' ' . $this->letter . ' ' . $this->km . ' ' . $this->block . ' ' . $this->stair . ' ' . $this->floor . ' ' . $this->door . ', ' . $this->zip_code . ', ' . $this->town . ', ' . $this->province,
                    'm2'        => 0,
                    'latitude'  => 0,
                    'longitude' => 0,
                    'used_for'  => 'n/c',
                    'type'      => 'none',
                    'level'     => 5,
                    'ine'       => $this->ine_code
                );
            }

        }
        if (isset($resp['used_for']) && $resp['used_for'] == 'M')
        {
            //Falseado
            $resp['type'] = 'none';
            $resp['level'] = 4;
        }

        if (isset($resp['parcela']) && $resp['used_for'] != 'RU' && $resp['type'] != 'suelo')
        {
            $resp = GeoCheckModel::check_with_14($resp, $this->floor, $this->door);
            if ($resp['type'] == 'house' && ! empty($this->floor))
            {
                $resp['type'] = 'flat';
                $resp['level'] = 4;
                $resp['m2'] = 0;
                $resp['address'] = (html_entity_decode($resp['address']));
            }
        }

        if ($resp['level'] == 5 && $resp['used_for'] != 'A')
        {
            $resp = $this->findinmueble_otros($resp);
        }
        if (isset($resp['address']))
        {
            $resp['address'] = mb_strtoupper(html_entity_decode(mb_strtolower($resp['address']), ENT_QUOTES, "UTF-8"));
            $resp['address'] = str_replace(';', ',', $resp['address']);
        }
        if (isset($resp['level']) && $resp['level'] == 6)
        {
            $resp['level'] = 4;
        }
        if ($resp['m2'] > 600 && $resp['type'] = 'flat') $resp['m2'] = 0;

        if (isset($resp['level']) && (($resp['level'] == 5 && ($resp['used_for'] != 'A' && $resp['used_for'] != 'M' && $resp['used_for'] != 'RU')) || ($resp['level'] < 5 && $resp['latitude'] == 0 && $resp['longitude'] == 0)))
        {
            $is_flat = false;
            $search = $this->road_type . ' ' . $this->street . ' ' . $this->number . ', ' . $this->town . ', ' . $this->province;
            if ( ! empty($this->floor))
            {
                $is_flat = true;
            }
            $resp = GeoCheckModel::here_geo_check(trim($search), $resp, $is_flat);
        }

        if ($resp['type'] == 'house-dpx') $resp['type'] = 'house';

        if ($resp['level'] < 5)
        {
            $status = 200;
            if ($resp['type'] == 'none')
            {
                if ( ! empty($this->floor)) $resp['type'] = 'flat';
                else $resp['type'] = 'house';
            }
        }
        else
        {
            $resp['latitude'] = $resp['longitude'] = 0;
            $status = 400;
        }
        $resp['address'] = trim(preg_replace('!\s+!', ' ', $resp['address']));
	Log::debug(print_r($resp,true));
        return ['hits' => $resp, 'status' => $status];
//var_dump($this->searchAddress());
//        $result = $this->normalizeStreet();
//        // Si hay resultado, cambiamos el atributo por el normalizado
//        // Si no puede que el numero este mal metido
//        $posibles_dirrecciones = array();
//        foreach ($result as $nombrevia){
//            $posibles_dirrecciones=GeoCheckModel::searchAddress($nombrevia['key'],$this->normalizeNumber());
//        }
    }

    private function normalizeRoadType()
    {
        //TODO QUITAR TILDES
        $result = false;
        if (strlen($this->road_type) > 2)
        {
            // Si length > 2 p.e.-> AVENIDA... aplicamos elastic y si hay mas de un resultado, aplicamos similar text
            $result = GeoCheckModel::normalize_roadType($this->road_type);
        }
        else
        {
            // Nos aseguramos que este normalizada
            $road_types = array("AG", "AL", "AR", "AU", "AV", "AY", "BJ", "BO", "BR", "CA", "CG", "CH", "CI", "CJ", "CL", "CM", "CN", "CO", "CP", "CR", "CS", "CT", "CU", "DE", "DP", "DS", "ED", "EM", "EN", "ER", "ES", "EX", "FC", "FN", "GL", "GR", "GV", "HT", "JR", "LD", "LG", "MC", "ML", "MN", "MS", "MT", "MZ", "PB", "PD", "PJ", "PL", "PM", "PQ", "PR", "PS", "PT", "PZ", "QT", "RB", "RC", "RD", "RM", "RP", "RR", "RU", "SA", "SD", "SL", "SN", "SU", "TN", "TO", "TR", "UR", "VR", "CY");
            $max_percent = 0;
            foreach ($road_types as $rt)
            {
                similar_text($rt, $this->road_type, $percent);
                if ($percent > $max_percent)
                {
                    $max_percent = $percent;
                    $result = $rt;
                }
            }
        }
        // Si hay resultado, cambiamos el atributo por el normalizado
        if ($result)
            $this->road_type = $result;

        return $result;
    }


    private function normalizeTown()
    {
        $norm = GeoCheckModel::normalize_Town($this->zip_code, $this->town);
        if (is_array($norm))
        {
            $this->ine_code = $norm['ine'];
            $this->town = $norm['town'];
            $this->province = $norm['provincia'];

            return $this->ine_code;

        }
        else return false;
    }

    private function searchAddress()
    {
        /*CL Nombre Numero Letra Km Bloque Esc Planta Puerta*/
        $result = GeoCheckModel::search($this->ine_code, $this->road_type, $this->street, $this->number, $this->letter, $this->km, $this->block, $this->stair, $this->floor, $this->door, true);
        if ($result)
        {
            if (count($result) > 1)
            {
                $inmueble_data = $result[0]->getData();
                $esc = $inmueble_data['escalera'];
                $km = $inmueble_data['kilometro'];
                $bloque = $inmueble_data['bloque'];
                $letra = $inmueble_data['letra'];
                $this->level = 3;
                foreach ($result as $inmueble)
                {
                    $inmueble_data = $inmueble->getData();
                    if ($inmueble_data['escalera'] != $esc || $inmueble_data['kilometro'] != $km ||
                        $inmueble_data['bloque'] != $bloque || $inmueble_data['letra'] != $letra
                    )
                        $this->level = 2;
                }
            }
            else $this->level = 1;
        }
        if ( ! $result)
        {
            $street = GeoCheckModel::del_articulos($this->street);
            $result = GeoCheckModel::search($this->ine_code, $this->road_type, $street, $this->number, $this->letter, $this->km, $this->block, $this->stair, $this->floor, $this->door);
            if ($result)
            {
                if (count($result) > 1)
                {
                    $inmueble_data = $result[0]->getData();
                    $esc = $inmueble_data['escalera'];
                    $km = $inmueble_data['kilometro'];
                    $bloque = $inmueble_data['bloque'];
                    $letra = $inmueble_data['letra'];
                    $this->level = 3;
                    foreach ($result as $inmueble)
                    {
                        $inmueble_data = $inmueble->getData();
                        if ($inmueble_data['escalera'] != $esc || $inmueble_data['kilometro'] != $km ||
                            $inmueble_data['bloque'] != $bloque || $inmueble_data['letra'] != $letra
                        )
                            $this->level = 2;
                    }
                }
                else $this->level = 1;
            }
//            if (!$result) {
//                $array_abc = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M');
//                if (in_array(strtoupper(substr($this->door, 0, 1)), $array_abc)) {
//                    $result = GeoCheckModel::search_wildcard($this->ine_code, $this->road_type, $street, $this->number, $this->letter, $this->km, $this->block, $this->stair, $this->floor, substr($this->door, 0, 1));
//                    if (!$result) {
//                        //Si es A,B o C convertimos a numero, si es BAJO = 0
//                        $door = 1 + array_search(strtoupper(substr($this->door, 0, 1)), $array_abc);
//                        $result = GeoCheckModel::search($this->ine_code, $this->road_type, $street, $this->number, $this->letter, $this->km, $this->block, $this->stair, $this->floor, $door);
//                    } else $this->level = 1;
//                }
//            } else $this->level = 1;

            if ($result)
            {
                if (count($result) > 1)
                {
                    $inmueble_data = $result[0]->getData();
                    $esc = $inmueble_data['escalera'];
                    $km = $inmueble_data['kilometro'];
                    $bloque = $inmueble_data['bloque'];
                    $letra = $inmueble_data['letra'];
                    $this->level = 3;
                    foreach ($result as $inmueble)
                    {
                        $inmueble_data = $inmueble->getData();
                        if ($inmueble_data['escalera'] != $esc || $inmueble_data['kilometro'] != $km ||
                            $inmueble_data['bloque'] != $bloque || $inmueble_data['letra'] != $letra
                        )
                            $this->level = 2;
                    }
                }
                else $this->level = 1;
            }
            else
            {
                $searchs = array();
                if ( ! empty($this->door) || strlen((string) $this->door) > 0)
                {
                    $searchs[] = 'door';
                }
                if ( ! empty($this->floor) || strlen((string) $this->floor) > 0)
                {
                    $searchs[] = 'floor';
                }
                if ( ! empty($this->stair) || strlen((string) $this->stair) > 0)
                {
                    $searchs[] = 'stair';
                }
                if ( ! empty($this->block) || strlen((string) $this->block) > 0)
                {
                    $searchs[] = 'block';
                }
                if ( ! empty($this->km) || strlen((string) $this->km) > 0)
                {
                    $searchs[] = 'km';
                }
                if ( ! empty($this->letter) || strlen((string) $this->letter) > 0)
                {
                    $searchs[] = 'letter';
                }
                $search_i = 0;
                $floor_check = true;
                // False entonces si tiene puerta lo ponemos en vacio
                do
                {
                    /*
                     * Niveles de exactitud:
                     *  1 - Exacto
                     *  2 - Hasta Puerta, pero falta KM o BLOQUE o ESCALERA o LETRA
                     *  3 - Hasta Planta
                     *  4 - Hasta Numero
                     *  5 - Inexistente
                      */

                    if (isset($searchs[$search_i]) && $searchs[$search_i] == 'door')
                    {
                        $result = GeoCheckModel::search($this->ine_code, $this->road_type, $street, $this->number, $this->letter, $this->km, $this->block, $this->stair, $this->floor);
                        if ($result)
                        {
                            if (count($result) > 0)
                                $this->level = 3;
                        }
                    }
                    if (isset($searchs[$search_i]) && $searchs[$search_i] == 'floor')
                    {
                        $result = GeoCheckModel::search($this->ine_code, $this->road_type, $street, $this->number, $this->letter, $this->km, $this->block, $this->stair);
                        if ($result)
                        {
                            if (count($result) > 0)
                            {
                                $this->level = 4;
                                $floor_check = true;
                            }
                        }
                    }
                    if (isset($searchs[$search_i]) && $searchs[$search_i] == 'stair')
                    {
                        $result = GeoCheckModel::search($this->ine_code, $this->road_type, $street, $this->number, $this->letter, $this->km, $this->block);
                        if ($result)
                        {
                            if (count($result) > 0)
                            {
                                $this->level = 4;
                                $floor_check = false;
                            }
                        }
                    }
                    if (isset($searchs[$search_i]) && $searchs[$search_i] == 'block')
                    {
                        $result = GeoCheckModel::search($this->ine_code, $this->road_type, $street, $this->number, $this->letter, $this->km);
                        if ($result)
                        {
                            if (count($result) > 0)
                            {
                                $this->level = 4;
                                $floor_check = false;
                            }
                        }
                    }
                    if (isset($searchs[$search_i]) && $searchs[$search_i] == 'km')
                    {
                        $result = GeoCheckModel::search($this->ine_code, $this->road_type, $street, $this->number, $this->letter);
                        if ($result)
                        {
                            if (count($result) > 0)
                            {
                                $this->level = 4;
                                $floor_check = false;
                            }
                        }
                    }
                    if (isset($searchs[$search_i]) && $searchs[$search_i] == 'letter')
                    {
                        $result = GeoCheckModel::search($this->ine_code, $this->road_type, $street, $this->number);
                        if ($result)
                        {
                            if (count($result) > 0)
                            {
                                $this->level = 4;
                                $floor_check = false;
                            }
                        }
                    }
                    $search_i++;
                } while ($search_i < count($searchs) && ! $result);
                if ( ! $result)
                {   //Direccion inexistente
                    $this->level = 5;

                    return false;
                }
                // Si hay resultado es nivel 4 pero ni la planta ni la puerta estan mal
                if ($result && ! $floor_check && $this->level == 4)
                {
                    $result_aux = GeoCheckModel::search($this->ine_code, $this->road_type, $street, $this->number, '', '', '', '', $this->floor, $this->door);
                    if ($result_aux)
                    {
                        $this->level = 2;
                        $result = $result_aux;
                        unset($result_aux);
                    }
                }
            }
        }
//        Log::debug('Search Address'. print_r($result,true));
        //TODO quitar este fix
        //**************Miramos si son iguales(Hay valores duplicados)
//        if (count($result) > 1) {
//            $aux[] = $result[0];
//            $i = 0;
//            foreach ($result as $inmueble) {
//                if (in_array($inmueble, $aux)) continue;
//                if ($inmueble->getData() != $aux[$i]->getData()) {
//                    $aux[] = $inmueble;
//                    $i++;
//                }
//            }
//            $result = $aux;
//            unset($aux);
//        }

        //Nueva Lógica 24/01/2017
        $aux = $this->checkResults($result);
        if (count($aux) > 1) $aux = $this->checkResults($result, true);
        $result = $aux;
        unset($aux);

        $parcela = '';
        if (count($result) < 1 || empty($result))
        {   //Direccion inexistente
            $this->level = 5;

            return false;
        }
        elseif (count($result) > 1)
        {

            //Mas de una dirección y no son almacenes(A)
            $first = true;
            $count_inm_total = $desv_tipica = $sum = $media = $m2 = $m2_totales = 0;
            $inmueble = $result[0]->getData();
            $this->street = $inmueble['nombredevia'];
            $this->road_type = $inmueble['tipodevia'];
            foreach ($result as $inmueble)
            {
                $inmueble = $inmueble->getData();
                $m2_totales += $inmueble['m2'];
            }
            $media = $m2_totales / count($result);

            foreach ($result as $inmueble)
            {
                $inmueble = $inmueble->getData();
                $sum += pow($inmueble['m2'] - $media, 2);
            }
            $desv_tipica = sqrt($sum / count($result));
            $upper_limit = $media + $desv_tipica;
            $low_limit = $media - $desv_tipica;
            foreach ($result as $inmueble)
            {
                $inmueble = $inmueble->getData();
                if ($first)
                {
                    $parcela = $inmueble['parcela'];
                    $this->street = $inmueble['nombredevia'];
                    $this->lat = $inmueble['latitud'];
                    $this->long = $inmueble['longitud'];
                    $this->used_for = $inmueble['uso'];
                    $first = false;
                }
                else
                {
                    if ( ! stristr($this->used_for, $inmueble['uso']))
                        $this->used_for .= ', ' . $inmueble['uso'];
                }
                if ($inmueble['m2'] <= $upper_limit && $inmueble['m2'] >= $low_limit)
                {
                    $m2 += $inmueble['m2'];
                    $count_inm_total++;
                }
            }

            $this->m2 = $m2 / $count_inm_total;

        }
        else
        {
            //Una dirección

            $this->level = 1;

            $inmueble = $result[0]->getData();
//            GeoCheckModel::search14($this->ine_code, $inmueble['parcela'], $this->floor, $this->door);

            similar_text('TODOS', trim($inmueble['escalera'] . $inmueble['planta'] . $inmueble['puerta']), $percent);
            if ($percent > 90) $this->type = 'house';

            similar_text('SUELO', trim($inmueble['escalera'] . $inmueble['planta'] . $inmueble['puerta']), $percent);
            if ($percent > 90)
            {
                $this->type = 'suelo';
                $this->level = 5;
                $this->m2 = 0;
            }

            $this->street = $inmueble['nombredevia'];
            $this->lat = $inmueble['latitud'];
            $this->long = $inmueble['longitud'];
            $this->used_for = $inmueble['uso'];
            $parcela = $inmueble['parcela'];
            if ($this->type == 'house' && ( ! empty($this->floor) && ! empty($this->door)))
            {
                $this->m2 = 0;
//                $this->type = 'flat';
            }
            else
                $this->m2 = $inmueble['m2'];

        }
        $direccion = $this->road_type . ' ' . $this->street . ' ' . $this->number . ' ' . $this->letter . ' ' . $this->km . ' ' . $this->block . ' ' . $this->stair . ' ' . $this->floor . ' ' . $this->door . ', ' . $this->zip_code . ', ' . $this->town . ', ' . $this->province;


        //  ['direccion', 'm2', 'lat', 'long', 'uso', 'level', 'tipo'];
        return array(
            'parcela'   => $parcela,
            'address'   => $direccion,
            'm2'        => $this->m2,
            'latitude'  => $this->lat,
            'longitude' => $this->long,
            'used_for'  => $this->used_for,
            'type'      => $this->type,
            'level'     => $this->level,
            'ine'       => $this->ine_code
        );
    }

    private
    function googleMapsLatLong($direccion)
    {
        $resultado = json_decode($this->curl(sprintf('https://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=%s', urlencode($direccion))));
//        Log::debug('GOOGLE :' . print_r($resultado, true));
        $estado = $resultado->status;
        if ($estado == 'OK')
        {
            $this->lat = $resultado->results[0]->geometry->location->lat;
            $this->long = $resultado->results[0]->geometry->location->lng;

            return true;
        }
        else
        {
            return false;
        }
    }

    private
    function cartoCiudadLatLong($address)
    {
//        $address = str_replace(' ', '%20', $address);
//        Log::debug('CARTOCIUDAD');
        $start = microtime(true);
        Log::debug('LLAMO CARTOCIUDAD  ' . ($start));

        $page = 'http://www.cartociudad.es/CartoGeocoder/Geocode?address=' . urlencode($address);
//        Log::debug($page);
        $json = $this->curl($page);
        Log::debug('TERMINO CARTOCIUDAD  ' . (microtime(true) - $start));

        //$var = utf8_decode($json);
        $var = json_decode(utf8_encode($json));
//        Log::debug('VAR :'.print_r($var, true));
//        Log::debug('JSON: '.print_r($json, true));
//        Log::debug('ENCODE: '.print_r(json_decode(utf8_encode($json)), true));
        if (isset($var->success) && $var->success != false)
        {
            if ($var->result[0]->status == 1)
            {
                $this->road_type = $var->result[0]->road_type;
                $this->street = $var->result[0]->road_name;
                $this->lat = $var->result[0]->latitude;
                $this->long = $var->result[0]->longitude;

                return true;
            }
            else if ($var->result[0]->status == 2)
            {
                $this->road_type = $var->result[0]->road_type;
                $this->street = $var->result[0]->road_name;
                $this->lat = $var->result[0]->latitude;
                $this->long = $var->result[0]->longitude;

                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }

    }

    private
    function findAddress_Catastro()
    {
        /*CL Nombre Numero Letra Km Bloque Esc Planta Puerta*/
        //$street = GeoCheckModel::del_articulos($this->street);
        $norm = GeoCheckModel::normalize_pob_town($this->ine_code);
        if ( ! $norm) return false;
        $this->town = $norm['town'];
        $this->province = $norm['province'];
//        Log::debug('BUSQUEDA CON TODO');
        $result = GeoCheckModel::searchCatastro($this->province, $this->town, $this->road_type, $this->street, $this->number, $this->block, $this->stair, $this->floor, $this->door);
//        if ($result) {
//            $xml = new \SimpleXMLElement($result);
//            if ((int)$xml->control->cudnp == 1)
//                $this->level = 1;
//            else $this->level = 2;
//            unset($xml);
//        }

        if ($result)
        {
//            Log::debug('HAY RESULTADO');
            $xml = new \SimpleXMLElement($result);

            if ((int) $xml->control->cudnp > 1)
            {
                $esc = $bloque = $letra = '';
                if (isset($xml->lrcdnp->rcdnp[0]->dt->locs->lous->lourb->loint->es))
                    $esc = (string) $xml->lrcdnp->rcdnp[0]->dt->locs->lous->lourb->loint->es;
                if (isset($xml->lrcdnp->rcdnp[0]->dt->locs->lous->lourb->loint->bq))
                    $bloque = (string) $xml->lrcdnp->rcdnp[0]->dt->locs->lous->lourb->loint->bq;
                if (isset($xml->lrcdnp->rcdnp[0]->dt->locs->lous->lourb->dir->plp))
                    $letra = (string) $xml->lrcdnp->rcdnp[0]->dt->locs->lous->lourb->dir->plp;
                $this->level = 3;
                foreach ($xml->lrcdnp->rcdnp as $inmueble)
                {
                    if ((isset($inmueble->dt->locs->lous->lourb->loint->es) && $inmueble->dt->locs->lous->lourb->loint->es != $esc)
                        || (isset($inmueble->dt->locs->lous->lourb->loint->bq) && $inmueble->dt->locs->lous->lourb->loint->bq != $bloque)
                        || (isset($inmueble->dt->locs->lous->lourb->dir->plp) && $inmueble->dt->locs->lous->lourb->dir->plp != $letra)
                    ) $this->level = 2;
                }

            }
            else $this->level = 1;
            unset($xml);
        }
        else
        {
//            Log::debug('ENTRO PORQUE NO ENCUENTRO NADA DE PRIMERAS');
            $searchs = array();
            if ( ! empty($this->door) || strlen((string) $this->door) > 0)
            {
//                Log::debug('TIENE PUERTA');
                $searchs[] = 'door';
            }
            if ( ! empty($this->floor) || strlen((string) $this->floor) > 0)
            {
//                Log::debug('TIENE PLANTA');
                $searchs[] = 'floor';
            }
            if ( ! empty($this->stair) || strlen((string) $this->stair) > 0)
            {
//                Log::debug('TIENE ESCALERA');
                $searchs[] = 'stair';
            }
            if ( ! empty($this->block) || strlen((string) $this->block) > 0)
            {
//                Log::debug('TIENE BLOQUE');
                $searchs[] = 'block';
            }
            $search_i = 0;
            // False entonces si tiene puerta lo ponemos en vacio
            do
            {
                /*
                 * Niveles de exactitud:
                 *  1 - Exacto
                 *  2 - Hasta Puerta, pero falta KM o BLOQUE o ESCALERA o LETRA
                 *  3 - Hasta Planta
                 *  4 - Hasta Numero
                 *  5 - Inexistente
                  */

                if (isset($searchs[$search_i]) && $searchs[$search_i] == 'door')
                {
//                    Log::debug('BUSQUEDA SIN PUERTA');
                    $result = GeoCheckModel::searchCatastro($this->province, $this->town, $this->road_type, $this->street, $this->number, $this->block, $this->stair, $this->floor);
                    if ($result)
                    {
                        $xml = new \SimpleXMLElement($result);
                        if ((int) $xml->control->cudnp > 0)
                            $this->level = 3;
                        unset($xml);
                    }
                }
                if (isset($searchs[$search_i]) && $searchs[$search_i] == 'floor')
                {
//                    Log::debug('BUSQUEDA SIN PISO');
                    $result = GeoCheckModel::searchCatastro($this->province, $this->town, $this->road_type, $this->street, $this->number, $this->block, $this->stair);
                    if ($result)
                    {
                        $xml = new \SimpleXMLElement($result);
                        if ((int) $xml->control->cudnp > 0)
                            $this->level = 4;
                        unset($xml);
                    }
                }
                if (isset($searchs[$search_i]) && $searchs[$search_i] == 'stair')
                {
//                    Log::debug('BUSQUEDA SIN ESCALERA');
                    $result = GeoCheckModel::searchCatastro($this->province, $this->town, $this->road_type, $this->street, $this->number, $this->block);
                    if ($result)
                    {
                        $xml = new \SimpleXMLElement($result);
                        if ((int) $xml->control->cudnp > 0)
                            $this->level = 4;
                        unset($xml);
                    }
                }
                if (isset($searchs[$search_i]) && $searchs[$search_i] == 'block')
                {
//                    Log::debug('BUSQUEDA SIN BLOQUE');
                    $result = GeoCheckModel::searchCatastro($this->province, $this->town, $this->road_type, $this->street, $this->number);
                    if ($result)
                    {
                        $xml = new \SimpleXMLElement($result);
                        if ((int) $xml->control->cudnp > 0)
                            $this->level = 4;
                        unset($xml);
                    }
                }
                $search_i++;
            } while ($search_i < count($searchs) && ! $result);
            if ( ! $result)
            {   //Direccion inexistente
                $this->level = 5;

                return false;
            }
        }
        $xml = new \SimpleXMLElement($result);
        //Aqui podemos decir que hay varios pisos falta algún dato
        $parcela = '';
//        if (!isset($xml->bico->bi->ldt) || !isset($xml->bico->bi->debi->luso)) {
        if (isset($xml->lrcdnp->rcdnp))
        {
            $dir = $this->road_type . ' ' . $this->street . ' ' . $this->number . ' ' . $this->letter . ' ' . $this->km . ' ' . $this->block . ' ' . $this->stair . ' ' . $this->floor . ' ' . $this->door . ', ' . $this->zip_code . ', ' . $this->town . ', ' . $this->province;
//            $this->m2 = 0;
            //Mas de un inmueble buscamos montamos las refencias y buscamos los m2 de cada inmueble
            $inmuebles = array();
            foreach ($xml->lrcdnp->rcdnp as $inmueble)
            {
                if (empty($parcela)) $parcela = (string) $inmueble->rc->pc1 . (string) $inmueble->rc->pc2;
                $referencia_catastral = (string) $inmueble->rc->pc1 . (string) $inmueble->rc->pc2 . (string) $inmueble->rc->car . (string) $inmueble->rc->cc1 . (string) $inmueble->rc->cc2;
                $inm = GeoCheckModel::getDatosCatastro($this->ine_code, $referencia_catastral);
                if ($inm['uso'] != 'A')
                    $inmuebles[] = $inm;
            }
            if (count($inmuebles) < 1)
            {
                $this->m2 = 0;
                $this->long = 0;
                $this->lat = 0;
                $this->used_for = 'A';
                $this->level = 5;
                $parcela = '';
            }
            elseif (count($inmuebles) > 1)
            {
                $first = true;
                $count_inm_total = $desv_tipica = $sum = $media = $m2 = $m2_totales = 0;

                foreach ($inmuebles as $inmueble)
                {
                    $m2_totales += $inmueble['m2'];
                }
                $media = $m2_totales / count($result);

                foreach ($inmuebles as $inmueble)
                {
                    $sum += pow($inmueble['m2'] - $media, 2);
                }
                $desv_tipica = sqrt($sum / count($result));
                $upper_limit = $media + $desv_tipica;
                $low_limit = $media - $desv_tipica;
                foreach ($inmuebles as $inmueble)
                {
                    if ($first)
                    {
                        $this->lat = $inmueble['lat'];
                        $this->long = $inmueble['long'];

                        $this->used_for = $inmueble['uso'];
                        if ( ! empty($this->lat) && ! empty($this->long) && ! empty($this->used_for))
                            $first = false;
                    }
                    else
                    {
                        if ( ! stristr($this->used_for, $inmueble['uso']))
                            $this->used_for .= ', ' . $inmueble['uso'];
                    }
                    if ($inmueble['m2'] <= $upper_limit && $inmueble['m2'] >= $low_limit)
                    {
                        $m2 += $inmueble['m2'];
                        $count_inm_total++;
                    }
                }

                $this->m2 = $m2 / $count_inm_total;
            }
            else
            {
                $this->m2 = $inmuebles[0]['m2'];
                $this->long = $inmuebles[0]['long'];
                $this->lat = $inmuebles[0]['lat'];
                $this->used_for = $inmuebles[0]['uso'];
            }
            // Latitud y longitud por google maps
        }
        else
        {

            if ($this->level == 4)
                $dir = $this->road_type . ' ' . $this->street . ' ' . $this->number . ' ' . $this->letter . ' ' . $this->km . ' ' . $this->block . ' ' . $this->stair . ' ' . $this->floor . ' ' . $this->door . ', ' . $this->zip_code . ', ' . $this->town . ', ' . $this->province;
            else $dir = (string) $xml->bico->bi->ldt;

            if (isset($xml->bico->bi->dt->locs->lous->lourb->loint->es) && isset($xml->bico->bi->dt->locs->lous->lourb->loint->pt) && isset($xml->bico->bi->dt->locs->lous->lourb->loint->pu))
            {
                similar_text('TODOS', trim((string) $xml->bico->bi->dt->locs->lous->lourb->loint->es . (string) $xml->bico->bi->dt->locs->lous->lourb->loint->pt . (string) $xml->bico->bi->dt->locs->lous->lourb->loint->pu), $percent);
                if ($percent > 90) $this->type = 'house';

            }

            $this->used_for = substr((string) $xml->bico->bi->debi->luso, 0, 1);
            $this->m2 = 0;
            if ($this->type == 'house' && ! empty($this->floor))
            {
                $this->type = 'flat';
            }
            else
            {
                if (isset($xml->bico->bi->debi->sfc))
                    $this->m2 = (int) $xml->bico->bi->debi->sfc;

            }
            $parcela = (string) $xml->bico->bi->idbi->rc->pc1 . (string) $xml->bico->bi->idbi->rc->pc2;
            $inm = GeoCheckModel::getDatosCatastro($this->ine_code, (string) $xml->bico->bi->idbi->rc->pc1 . (string) $xml->bico->bi->idbi->rc->pc2 . (string) $xml->bico->bi->idbi->rc->car . (string) $xml->bico->bi->idbi->rc->cc1 . (string) $xml->bico->bi->idbi->rc->cc2);
            if ((string) $xml->bico->bi->idbi->cn == 'RU')
            {
                $this->used_for = 'RU';
                $this->m2 = 0;
                $this->lat = 0;
                $this->long = 0;
                $this->type = 'RU';
                $this->level = 5;
            }

            $this->lat = $inm['lat'];
            $this->long = $inm['long'];
            //Lat y Long google maps
        }
        if ((empty($this->lat) || empty($this->long)) && $this->used_for != 'RU')
        {
            $direccion = trim($this->road_type) . ' ' . trim($this->street) . ',' . trim($this->number) . ', ';
            $pos = strpos($this->town, ',');
            if ($pos !== false)
            {
                $nombre_pob = substr($this->town, 0, $pos);
                $pronombre_pob = substr($this->town, $pos + 1);
                $poblacion = $pronombre_pob . ' ' . $nombre_pob;
                $direccion .= $poblacion;
            }
            else
            {
                $direccion .= $this->town;
            }
            $this->cartoCiudadLatLong($direccion);
            //GOOGLE CORTE
            if (empty($this->lat) || empty($this->long))
            {
                $rt = $this->road_type;
                if ($this->road_type == 'UR') $rt = 'URB';
                $direccion = $rt . ' ' . $this->street . ' ,' . $this->number . ', ' . $this->zip_code . ' ' . $this->town;
                $this->googleMapsLatLong($direccion);
            }
            if (empty($this->lat) || empty($this->long))
            {
                return false;
            }
        }

        return array(
            'parcela'   => $parcela,
            'address'   => $dir,
            'm2'        => $this->m2,
            'latitude'  => $this->lat,
            'longitude' => $this->long,
            'used_for'  => $this->used_for,
            'type'      => $this->type,
            'level'     => $this->level,
            'ine'       => $this->ine_code
        );

    }

    private
    function findinmueble_otros($resp)
    {
        //GUIPUZCOA ALAVA NAVARRA VIZCAYA O TIPO 5
        $resp = GeoCheckModel::search_address_fenosa($this->zip_code, $this->street, $this->number, $resp);
        if ($resp['level'] != 5)
        {
            $this->lat = $this->long = '';
            if (substr($this->ine_code, 0, 2) != '01' && substr($this->ine_code, 0, 2) != '20'
                || substr($this->ine_code, 0, 2) != '31' && substr($this->ine_code, 0, 2) != '48'
            )
            {
                $res = GeoCheckModel::search_street_lat_long($this->ine_code, GeoCheckModel::del_articulos($this->street));
                $aux = $this->checkResults($res);
                if (count($aux) > 1) $aux = $this->checkResults($res, true);
                $res = $aux;
                unset($aux);
                if (count($res) > 1)
                {
                    $inmueble_data = $res[0]->getData();
                    $this->lat = $inmueble_data['latitud'];
                    $this->long = $inmueble_data['longitud'];
                }
            }
            if ((empty($this->lat) || empty($this->long)))
            {
                $direccion = trim($this->road_type) . ' ' . trim($this->street) . ',' . trim($this->number) . ', ';
                $pos = strpos($this->town, ',');
                if ($pos !== false)
                {
                    $nombre_pob = substr($this->town, 0, $pos);
                    $pronombre_pob = substr($this->town, $pos + 1);
                    $poblacion = $pronombre_pob . ' ' . $nombre_pob;
                    $direccion .= $poblacion;
                }
                else
                {
                    $direccion .= $this->town;
                }
                $this->cartoCiudadLatLong($direccion);
                //GOOGLE CORTE
                if (empty($this->lat) || empty($this->long))
                {
                    $rt = $this->road_type;
                    if ($this->road_type == 'UR') $rt = 'URB';
                    $direccion = $rt . ' ' . $this->street . ' ,' . $this->number . ', ' . $this->zip_code . ' ' . $this->town;
                    $this->googleMapsLatLong($direccion);
                }
                if (empty($this->lat) || empty($this->long))
                {
                    $this->here_maps();
                }
            }
        }
        if ((empty($this->lat) || empty($this->long)) && $resp['level'] == 4)
        {
            $resp['latitude'] = 0;
            $resp['longitude'] = 0;
            $resp['level'] = 6;
        }
        elseif ($resp['level'] == 4)
        {
            $resp['latitude'] = $this->lat;
            $resp['longitude'] = $this->long;
        }

        return $resp;
    }

    private
    function curl($page, $google = false)
    {
        if ($google)
        {
            $obj_proxy = new Proxies('google');
        }
        else
        {
            $obj_proxy = new Proxies('cartociudad');
        }

        $ip_real = false;
        do
        {
            $ip = $obj_proxy->get();
            $obj_proxy->setUsed($ip);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $page);
            curl_setopt($ch, CURLOPT_VERBOSE, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Accept-Charset: utf-8'));
            if ($ip)
            {
                curl_setopt($ch, CURLOPT_PROXY, $ip);
                curl_setopt($ch, CURLOPT_PROXYUSERPWD, "mrhc:QwEr1234");
            }
            else $ip_real = true;
            $info = curl_getinfo($ch);
            $result = curl_exec($ch);
            curl_close($ch);
            if ( ! $ip_real)
            {
                $obj_proxy->unlock($info["http_code"]);
            }
        } while ($info["http_code"] != 200 && ! $ip_real);

        return $result;
    }

    private function here_maps()
    {
        $start = microtime(true);
        Log::debug('LLAMO HERE  ' . $start);
        $obj_proxy = new Proxies('here');

        $ip_real = false;
        do
        {
            $ip = $obj_proxy->get();
            $obj_proxy->setUsed($ip);
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL            => "https://places.api.here.com/places/v1/autosuggest?X-Map-Viewport=1.6372%2C41.5689%2C1.6389%2C41.5707&X-Political-View=ESP&app_code=djPZyynKsbTjIUDOBcHZ2g&app_id=xWVIueSv6JL0aJ5xqTxb&hlEnd=%3C%2Fspan%3E&hlStart=%3Cspan%20class%3D%22mark%20bold%22%3E&q=" . str_replace(' ', '%2B', $this->road_type . ' ' . $this->street . ' ' . $this->number . ' ' . $this->town) . "&result_types=address%2Cplace&size=5",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "",
                CURLOPT_MAXREDIRS      => 10,
                CURLOPT_TIMEOUT        => 30,
                CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST  => "GET",
                CURLOPT_HTTPHEADER     => array(
                    "accept: application/json",
                    "accept-encoding: gzip, deflate, sdch, br",
                    "accept-language: es-ES",
                    "cache-control: no-cache",
                    "connection: keep-alive",
                    "origin: https://wego.here.com",
                    "referer: https://wego.here.com/?map=41.56977,1.63803,19,normal",
                    "user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36"
                ),
            ));
            if ($ip)
            {
                curl_setopt($curl, CURLOPT_PROXY, $ip);
                curl_setopt($curl, CURLOPT_PROXYUSERPWD, "mrhc:QwEr1234");
            }
            else $ip_real = true;
            $response = curl_exec($curl);
            $info = curl_getinfo($curl);
            curl_close($curl);
            if ( ! $ip_real)
            {
                $obj_proxy->unlock($info["http_code"]);
            }
        } while ($info["http_code"] != 200 && ! $ip_real);
        $response = json_decode($response);
        if (isset($response->results) && count($response->results) > 0)
        {
            $this->lat = $response->results[0]->position[0];
            $this->long = $response->results[0]->position[1];
        }
        Log::debug('TERMINO HERE  ' . (microtime(true) - $start));
    }

    private function checkResults($result, $segunda_busqueda = false)
    {
        if (count($result) > 1)
        {
            $max_percent = 60;
            $this->type = 'house';
            $score = $aux = [];
            foreach ($result as $inmueble)
            {
                if (in_array($inmueble, $aux)) continue;
                $inmueble_data = $inmueble->getData();
                $arr_s = explode('(', $inmueble_data['nombredevia']);
                $str = trim($arr_s[0]);
                $str = GeoCheckModel::del_articulos($str);
                $str_intro = GeoCheckModel::del_articulos($this->street);
                if ($segunda_busqueda)
                {
                    if ($pos_coma = strpos($str, ','))
                    {
                        $final_via = trim(substr($str, 0, $pos_coma));
                        $p_via = trim(substr($str, $pos_coma + 1));
                        $str = $p_via . ' ' . $final_via;
                    }
                    unset($pos_coma);
                    if ($pos_coma = strpos($str_intro, ','))
                    {
                        $final_via = trim(substr($str_intro, 0, $pos_coma));
                        $p_via = trim(substr($str_intro, $pos_coma + 1));
                        $str_intro = $p_via . ' ' . $final_via;
                    }
                }
                similar_text($str_intro, $str, $percent);
                if ($percent >= $max_percent)
                {
//                    Log::debug('CALLE: ' . $this->street . 'Percent : ' . $percent . 'Max percent: ' . $max_percent);
                    $score[] = array('score' => $percent, 'id' => $inmueble->getId());
//                    $aux[] = $inmueble;
                    if (trim($inmueble_data['escalera'] . $inmueble_data['planta'] . $inmueble_data['puerta']) != 'TODOS')
                        $this->type = 'flat';
//                    $max_percent = $percent;
                }
            }
            foreach ($score as $key => $row)
            {
                $aux[$key] = $row['score'];
            }
            array_multisort($aux, SORT_DESC, $score);
            unset($aux);
            $aux = [];
//            Log::debug('VAR SCORE ' . print_r($score, true));

            if ( ! isset($score[0]))
            {
                $this->level = 5;

                return false;
            }
            else
            {
                $max_score = $score[0]['score'];
                foreach ($score as $row)
                {
                    foreach ($result as $inmueble)
                    {
                        if ($row['id'] == $inmueble->getId() && $row['score'] >= ($max_score - 10) && $row['score'] >= 70.0)
                        {
                            $aux[] = $inmueble;
                        }
                    }
                }

//                Log::debug('VAR AUX ' . print_r($aux, true));
                return $aux;
            }
        }

        return $result;
    }

}

// TEST
//    public function __construct()
//    {
//        $params = new stdClass();
//        $params->road_type = 'CL';
//        $params->street = 'CASTILLO';
//        $params->number = '8';
//        $params->floor = '';
//        $params->door = '';
//        $params->stair = '';
//        $params->km = '';
//        $params->block = '';
//        $params->letter = '';
//        $params->province = 'MADRID';
//        $params->town = 'MADRID';
//        $params->zip_code = str_pad(28008, 5, '0', STR_PAD_LEFT);
//        $this->ine_code = "28079";
//
//        $this->road_type = mb_strtoupper(UtilClass::elimina_acentos(trim($params->road_type)), 'UTF-8');
//        $this->street = mb_strtoupper(UtilClass::elimina_acentos(trim($params->street)), 'UTF-8');
//        $this->number = intval(mb_strtoupper(trim($params->number), 'UTF-8'));
//        $this->letter = mb_strtoupper(trim($params->letter), 'UTF-8');
//        // Quitamos letras del primer numero policia
//        if (preg_match('/([A-Z]|[a-z])+/', $this->number)) {
//            $prob_letter = $this->number;
//            $prob_pnp = preg_replace('/([A-Z]|[a-z])+/', '', $this->number);
//            foreach (str_split($prob_pnp) as $number) {
//                $prob_letter = str_replace($number, '', $prob_letter);
//            }
//            if (empty($this->letter)) {
//                $this->letter = $prob_letter;
//            } else {
//                $this->letterinpnp = $prob_letter;
//            }
//            $this->number = $prob_pnp;
//        }
//        $this->block = mb_strtoupper(trim($params->block), 'UTF-8');
//        $this->stair = mb_strtoupper(trim($params->stair), 'UTF-8');
//        $this->km = mb_strtoupper(trim($params->km), 'UTF-8');
//        $this->floor = mb_strtoupper(trim($params->floor), 'UTF-8');
//        $this->door = mb_strtoupper(trim($params->door), 'UTF-8');
//        $this->zip_code = str_pad(trim($params->zip_code), 5, '0', STR_PAD_LEFT);
//        $this->province = mb_strtoupper(UtilClass::elimina_acentos(trim($params->province)), 'UTF-8');
//        $this->town = mb_strtoupper(UtilClass::elimina_acentos(trim($params->town)), 'UTF-8');
//
//        if (!empty($this->floor))
//            $this->type = 'flat';
//        else $this->type = 'house';
//        $this->m2 = $this->lat = $this->long = 0;
//        $this->used_for = 'n/c';
//
//    }
