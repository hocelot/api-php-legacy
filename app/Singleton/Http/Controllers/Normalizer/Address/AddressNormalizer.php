<?php

/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 5/05/17
 * Time: 12:42
 */

namespace App\Http\Controllers\Normalizer\Address;


class AddressNormalizer {

    private $input_street, $i_prov, $town, $zip_code;
    private $output = array(
        'roadtype' => '',
        'street'   => '',
        'number'   => '',
        'letter'   => '',
        'km'       => '',
        'block'    => '',
        'stair'    => '',
        'floor'    => '',
        'door'     => '',
        'zip'      => '',
        'town'     => '',
        'province' => '',
        'ine'      => '',
        'verified' => 0
    );

    public function __construct($input_street, $town, $zip_code, $provincia)
    {
        $this->input_street = mb_strtoupper(trim($input_street));
        $this->town = mb_strtoupper(trim($town));
        $this->output['zip_code'] = $this->zip_code = str_pad(trim($zip_code), 5, '0', STR_PAD_LEFT);
        $this->i_prov = mb_strtoupper(trim($provincia));
    }

    public function normalize(){
//        $this->find__street();
//        $this->find__number();
//        $this->find__letter();
//        $this->find__km();
//        $this->find__block();
//        $this->find__stairs();
//        $this->find__floor();
//        $this->find__door();
//        $this->find_others();
//        return $this->output;
        $parser = file_get_contents(env("ZUUL_URL").'/bs-address-normalizer/address-normalizer/'.rawurlencode($this->input_street));
        return json_decode($parser);
    }

    //Busca street AKA nombre de via
    public function find__street($limit_tv = -1)
    {
        $input_clean_street = $this->cleanStreet();

        //Nos cargamos los tv
        $has_tv = false;
        $general_count_tv = 0;
        if ($limit_tv !== 0)
            foreach (Regex::$roadtypes__exp as $roadtypes_exp_key => $roadtypes_exp_value)
            {
                $input_clean_street = trim(preg_replace('@^((' . $roadtypes_exp_value . '|' . $roadtypes_exp_key . ')( ))+@', '', $input_clean_street, $limit_tv, $count_tv));
                $general_count_tv += $count_tv;
                if($general_count_tv === 1 && $count_tv > 0){
                    $this->output['roadtype'] = $roadtypes_exp_key;
                }
                if ($count_tv > 0)
                {
                    $has_tv = true;
                }
                if ($general_count_tv === 1 && $limit_tv === 1)
                {
                    break;
                }
            }
//        Nos cargamos numero ejemplo Nº 22
        foreach (Regex::$ab_number__exp as $number_exp_key => $number_exp_value)
        {
            $input_clean_street = trim(preg_replace('@( )(' . $number_exp_value . '|' . $number_exp_key . ')( |[0-9]+)+( |[A-Za-zÇçÑñ]+|[.]+)*@', '', $input_clean_street));
        }

        //Quitar todo despues de un numero
        $input_clean_street = trim(preg_replace('@( )*[0-9]+( |[A-Za-zÇçÑñ]+|[.]+)*@', '', $input_clean_street));

        //TODO: Mirar si tiene mas de dos calles, si la tiene me quedo con la última
        // Si input_clean_street es vacio, puede ser por:
        //  1- Hay dos tipos de vias
        //  2- La calle es un nº o fecha
        if (empty($input_clean_street) && $limit_tv === -1)
        {
            $street = $this->cleanStreet();
            //el nombre de via es fecha o numero
            foreach (Regex::get__dates__exp() as $date__exp)
            {
                if (preg_match_all('@(' . $date__exp . ')@', $street, $match))
                {
                    if (isset($match[0][0]))
                        $input_clean_street = trim($match[0][0]);
                }
            }

            $street = $this->cleanStreet();
            preg_match_all("/\d+/", $street, $match);
            if ($has_tv && empty($input_clean_street) && count($match[0]) < 4 && count($match[0]) != 2)
            {
                $input_clean_street = trim(preg_replace('@( )*[0-9]+( |[A-Za-zÇçÑñ]+|[.]+)*@', '', $street));
            }

            //Si sigue vacio hay dos tipos de via
            if (empty($input_clean_street))
            {
                if ($general_count_tv === 1)
                {
                    //El tipo de via es el nombre de via
                    $this->find__street(0);
                    if (empty($this->output['street'])){
                        $one = preg_split("/\D+/", $street);
                        $two = array_filter($one);
                        $input_clean_street = reset($two);
                }
                }
                elseif ($general_count_tv > 1)
                    $this->find__street(1);
            }
        }


        $street_explode = explode(' ', $input_clean_street);
        //Eliminamos letras sueltas
        for ($i = 0; $i < count($street_explode); $i++)
        {
            $street_explode[$i] = trim(preg_replace("/^([^IVX0-9])$/", "", $street_explode[$i]));
            foreach (Regex::$ab_level__exp as $key__level_exp => $level_exp)
            {
                $street_explode[$i] = trim(preg_replace('@^((SIN|S)?(\\\| |/)?(N(U)?(M)?(E)?(R)?(O)?))?(' . $level_exp . ')$@', '', $street_explode[$i]));
            }
            foreach (Regex::$ab_number__exp as $key_level_exp => $levek_exp)
            {
                $street_explode[$i] = trim(preg_replace('@^(' . $key_level_exp . ')$@', '', $street_explode[$i]));
            }
            foreach (Regex::$ab_street__exp as $expresion => $value)
            {
                $street_explode[$i] = trim(preg_replace('@^(' . $expresion . ')$@', ' ' . $value . ' ', $street_explode[$i]));

            }
        }

        $input_clean_street = implode(' ', $street_explode);

        $input_clean_street = preg_replace('@(NU(M)?(E)?(R)?(O)?)$@', '', $input_clean_street);
        //Si es vacio busco el numero y cojo todo lo que haya antes
        $this->output['street'] = trim($input_clean_street);
        $this->output['street'] = str_replace('  ', ' ', $this->output['street']);

        return true;
    }

    public function find__number()
    {
//        $this->output['number'] = 8;
        $input_clean_street = $this->cleanStreet();
        //Eliminamos el nombre de via encontrado
//        $street = str_replace( $input_clean_street,$count);
        $street = trim(preg_replace('@' . $this->output['street'] . '@', ' ', $input_clean_street, 1));

        //Eliminamos tipos de via
        foreach (Regex::$roadtypes__exp as $roadtypes_exp_key => $roadtypes_exp_value)
        {
            $street = trim(preg_replace('@((' . $roadtypes_exp_value . '|' . $roadtypes_exp_key . ')( ))+@', '', $street));
        }

        $street = str_replace('  ', ' ', $street);
        $number = '';
        // Puede ser sin numero
        $c = 0;
        foreach (Regex::$ab_number__exp as $expresion => $value)
        {
            if ($c === 0 && preg_match('@(' . $expresion . ')@', $street))
            {
                $number = 'SN';
            }
            if ($c === 1)
            {
                preg_match_all('@(' . $expresion . '( )+[0-9]+)@', $street, $match);
                if (isset($match[0][0]))
                    $number = trim(preg_replace('@(' . $expresion . '( )+)@', '', $match[0][0]));
            }
            $c++;
        }

        if (empty($number))
        {
            $one = preg_split("/\D+/", $street);
            $two = array_filter($one);
            $number = reset($two);

        }

        $this->output['number'] = $number;

        if (empty($this->output['street']))
        {
            $this->find__street_after_number();
        }
        else return true;

        if (preg_match('@(([A-ZÑÇ ])+' . $this->output['number'] . ')@', $this->output['street']))
        {
            $this->output['street'] = str_replace($this->output['number'], '', $this->output['street']);
        }

        return true;
    }

    public function find__letter()
    {
        $input_clean_street = $this->cleanStreet();
        //Eliminamos el nombre de via encontrado
        $street = str_replace($this->output['street'], ' ', $input_clean_street);
        if ($pos = strpos($street, $this->output['number']))
        {
            $street = substr($street, $pos);
        }
        $street = preg_replace('@' . $this->output['number'] . '@', ' ', $street, 1);

        //Eliminamos tipos de via
        foreach (Regex::$roadtypes__exp as $roadtypes_exp_key => $roadtypes_exp_value)
        {
            $street = trim(preg_replace('@^((' . $roadtypes_exp_value . ')( ))+@', '', $street));
        }
        foreach (Regex::$ab_number__exp as $expresion => $value)
        {
            $street = trim(preg_replace('@(' . $expresion . ')@', '', $street));
        }
        $street = trim(str_replace('  ', ' ', $street));
        $letter = '';
        $street_explode = explode(' ', $street);
        if (preg_match('@^[A-Za-z]$@', $street_explode[0], $match))
        {
            $letter = $street_explode[0];
        }

        $this->output['letter'] = $letter;
    }

    public function find__km()
    {
        $input_clean_street = $this->cleanStreet(true);
        $input_clean_street = str_replace('-', ' ', $input_clean_street);
        $input_clean_street = str_replace('$', ' ', $input_clean_street);
        $input_clean_street = str_replace('*', ' ', $input_clean_street);
        $input_clean_street = str_replace('  ', ' ', $input_clean_street);
        //Eliminamos el nombre de via encontrado
        $street = str_replace($this->output['street'], ' ', $input_clean_street);
        $street = trim($street);
        //Eliminamos tipos de via
        foreach (Regex::$roadtypes__exp as $roadtypes_exp_key => $roadtypes_exp_value)
        {
            $street = trim(preg_replace('@^((' . $roadtypes_exp_value . ')( ))+@', '', $street));
        }
        $street = trim(str_replace('  ', ' ', $street));
        $km = '';
        if (preg_match('@' . Regex::$ab_level__exp['km'] . '( )([0-9,.])+@', $street, $match))
        {
            $match[0] = trim(preg_replace('@' . Regex::$ab_level__exp['km'] . '@', '', $match[0]));
            $max_match = 0;
            $km = $match[0];
            $str_explode = explode(' ', $street);
            foreach ($str_explode as $str)
            {
                $str = preg_replace('@([.]([A-ZÑÇ])+)@', ' ', $str);
                $str = trim(str_replace('  ', ' ', $str));

                similar_text($str, $match[0], $percent);
                if ($percent > $max_match)
                {
                    $max_match = $percent;
                    $km = $str;
                }
            }
        }
        $this->output['km'] = $km;
        if ($this->check__number(str_replace(',', '', str_replace('.', '', $km))))
            $this->output['number'] = 'SN';

    }

    public function find__block()
    {
        $input_clean_street = $this->cleanStreet();
        //Eliminamos el nombre de via encontrado
        $street = str_replace($this->output['street'], ' ', $input_clean_street);
        //Eliminamos tipos de via
        foreach (Regex::$roadtypes__exp as $roadtypes_exp_key => $roadtypes_exp_value)
        {
            $street = trim(preg_replace('@^((' . $roadtypes_exp_value . ')( ))+@', '', $street));
        }
        $street = trim(str_replace('  ', ' ', $street));
        $block = '';
        if (preg_match('@(' . Regex::$ab_level__exp['block'] . ')( )([0-9]|[A-Z])+@', $street, $match))
        {
            $block = trim(preg_replace('@(' . Regex::$ab_level__exp['block'] . ')@', '', $match[0]));
        }
        $this->output['block'] = $block;
        $this->check__number($block);

    }

    public function find__stairs()
    {
        $input_clean_street = $this->cleanStreet();
        //Eliminamos el nombre de via encontrado
        $street = str_replace($this->output['street'], ' ', $input_clean_street);
        //Eliminamos tipos de via
        foreach (Regex::$roadtypes__exp as $roadtypes_exp_key => $roadtypes_exp_value)
        {
            $street = trim(preg_replace('@^((' . $roadtypes_exp_value . ')( ))+@', '', $street));
        }
        $street = trim(str_replace('  ', ' ', $street));
        $stair = '';
        if (preg_match('@( )(' . Regex::$ab_level__exp['stairs'] . ')( )([0-9]|[A-Z])+@', $street, $match))
        {
            $stair = trim(preg_replace('@( )(' . Regex::$ab_level__exp['stairs'] . ')@', '', $match[0]));
        }
        $this->output['stair'] = $stair;
        $this->check__number($stair);

    }

    public function find__floor()
    {
        $input_clean_street = $this->cleanStreet();
        //Eliminamos el nombre de via encontrado
        $street = str_replace($this->output['street'], ' ', $input_clean_street);
        //Eliminamos tipos de via
        foreach (Regex::$roadtypes__exp as $roadtypes_exp_key => $roadtypes_exp_value)
        {
            $street = trim(preg_replace('@^((' . $roadtypes_exp_value . ')( ))+@', '', $street));
        }
        $street = trim(str_replace('  ', ' ', $street));
        $floor = '';
        if (preg_match('@( )(' . Regex::$ab_level__exp['floor'] . ')( )([0-9]|[A-Z])+@', $street, $match))
        {
            $floor = trim(preg_replace('@( )(' . Regex::$ab_level__exp['floor'] . ')@', '', $match[0]));
        }
        $this->output['floor'] = $floor;
        $this->check__number($floor);

    }

    public function find__door()
    {
        $input_clean_street = $this->cleanStreet();
        //Eliminamos el nombre de via encontrado
        $street = str_replace($this->output['street'], ' ', $input_clean_street);
        //Eliminamos tipos de via
        foreach (Regex::$roadtypes__exp as $roadtypes_exp_key => $roadtypes_exp_value)
        {
            $street = trim(preg_replace('@^((' . $roadtypes_exp_value . ')( ))+@', '', $street));
        }
        $street = trim(str_replace('  ', ' ', $street));
        $door = '';
        if (preg_match('@( )(' . Regex::$ab_level__exp['door'] . ')( )([0-9]|[A-Z])+@', $street, $match))
        {
            $door = trim(preg_replace('@( )(' . Regex::$ab_level__exp['door'] . ')@', '', $match[0]));
        }
        $this->output['door'] = $door;
        $this->check__number($door);
    }

    private function cleanStreet($km = false)
    {
        if ( ! $km)
        {
            $street = UtilClass::cleanStr($this->input_street);

        }
        else
        {
            $street = $this->input_street;
        }

        //Separamos numeros y letras, es decir, 18BJO pasaria a 18 BJO
        $st = explode(' ', $street);
        foreach ($st as $n => $streete)
        {
            $st[$n] = implode(' ', preg_split("/(,?\s+)|((?<=[a-zA-ZÑñ'Çç])(?=\d))|((?<=\d)(?=[a-zA-ZÑñ'Çç]))/i", $streete));
            foreach (Regex::$ab_street__exp as $expresion => $value)
            {
                $st[$n] = trim(preg_replace('@^(' . $expresion . ')$@', ' ' . $value . ' ', $st[$n]));

            }
            foreach (Regex::$numbers_str__exp as $number => $str)
                $st[$n] = trim(preg_replace('@(' . $str . ')@', $number, $st[$n]));

        }
        $street = implode(' ', $st);
//QUITAR CP

        $street = trim(preg_replace('@[0-9]{4,5}(.)*@', '', $street));

        return mb_strtoupper($street);
    }

    public function __toString()
    {
        return $this->output['roadtype'] . ';' . $this->output['street'] . ';' . $this->output['number'] . ';' . $this->output['letter'] . ';' . $this->output['km'] . ';' . $this->output['block']
            . ';' . $this->output['stair'] . ';' . $this->output['floor'] . ';' . $this->output['door'] . ';' . $this->output['zip_code'] . ';' . $this->output['town'] . ';' . $this->output['province']
            . ';' . $this->output['ine'];
    }

    public function check__number($out)
    {

        if ( ! empty($out) && trim($this->output['number']) === trim($out))
        {
            $street = $this->cleanStreet();
            $count = substr_count($street, $out);
            if ($count === 1)
            {
                $this->output['number'] = '';
                if ( ! empty($this->output['km'])) $this->output['number'] = 'SN';

                return true;
            }
        }


        return false;

    }

    public function find_others()
    {
        $input_clean_street = $this->cleanStreet();

        //Eliminamos el nombre de via encontrado
        $street = trim(str_replace($this->output['street'], ' ', $input_clean_street));
        $street = trim(preg_replace('@' . $this->output['door'] . '@', '', $street, 1));
        //Eliminamos tipos de via
        foreach (Regex::$roadtypes__exp as $roadtypes_exp_key => $roadtypes_exp_value)
        {
            $street = trim(preg_replace('@((' . $roadtypes_exp_value . '|' . $roadtypes_exp_key . ')( ))+@', '', $street));
        }
        foreach (Regex::$ab_number__exp as $num_exp_key => $num_exp_value)
        {
            $street = trim(preg_replace('@((' . $num_exp_value . '|' . $num_exp_key . ')( ))+@', '', $street));
        }

        foreach (Regex::$ab_level__exp as $level_exp_key => $level_exp_value)
        {
            if ($level_exp_key !== 'DR' && $level_exp_key !== 'IZ')
                $street = trim(preg_replace('@((' . $level_exp_value . '|' . $level_exp_key . ')( ))+@', '', $street));
        }

        if ( ! empty($this->output['number']))
            $street = trim(preg_replace('@' . $this->output['number'] . '@', '', $street, 1));
        if ( ! empty($this->output['letter']))
            $street = trim(preg_replace('@' . $this->output['letter'] . '@', '', $street, 1));
        if ( ! empty($this->output['km']))
            $street = trim(preg_replace('@' . $this->output['km'] . '@', '', $street, 1));
        if ( ! empty($this->output['block']))
            $street = trim(preg_replace('@' . $this->output['block'] . '@', '', $street, 1));
        if ( ! empty($this->output['stair']))
            $street = trim(preg_replace('@' . $this->output['stair'] . '@', '', $street, 1));
        if ( ! empty($this->output['floor']))
            $street = trim(preg_replace('@' . $this->output['floor'] . '@', '', $street, 1));
        if ( ! empty($this->output['door']))
            $street = trim(str_replace('  ', ' ', $street));
        //EMPEZAMOS POR LA DERECHA
        $explode_str = explode(' ', $street);
        $aux = [];
        $c = 0;
        for ($i = count($explode_str) - 1; $i >= 0; $i--)
        {
            if ($c > 1) break;
            if ($c === 0 && empty($this->output['door']))
                $this->output['door'] = $explode_str[$i];
            elseif (empty($this->output['floor'])) $this->output['floor'] = $explode_str[$i];
            if ($c === 1 && empty($this->output['floor']))
                $this->output['floor'] = $explode_str[$i];
            $c++;
        }
        $communs = false;
        if ( ! empty($this->output['door']) && empty($this->output['floor']))
        {
            if (preg_match('@B(A)?(I)?(X|J)(O)?(S)?@', $this->output['door']))
            {
                $this->output['floor'] = 'BAJO';
                $this->output['door'] = '';
                $communs = true;
            }
            else
                if (preg_match('@AT(I)?(C)?(O)?@', $this->output['door']))
                {
                    $this->output['floor'] = 'ATICO';
                    $this->output['door'] = '';
                    $communs = true;

                }
                else
                    if (preg_match('@ENT(R)?(S)?(U)?(E)?(L)?(O)?@', $this->output['door']))
                    {
                        $this->output['floor'] = 'ENT';
                        $this->output['door'] = '';
                        $communs = true;

                    }
                    else if (strlen($this->output['door']) > 3) $this->output['door'] = '';
        }
        if ( ! empty($this->output['door']) && preg_match('@(\w)+@', $this->output['door']))
        {
            if (preg_match('@B(A)?(I)?(X|J)(O)?(S)?@', $this->output['door']))
            {
                $this->output['door'] = 'BAJO';
            }
            else
                if (preg_match('@AT(I)?(C)?(O)?@', $this->output['door']))
                {
                    $this->output['door'] = 'ATICO';

                }
                else
                    if (preg_match('@ENT(R)?(S)?(U)?(E)?(L)?(O)?@', $this->output['door']))
                    {
                        $this->output['door'] = 'ENT';
                    }
                    else
                        if (preg_match('@IZ(Q)?(U)?(I)?(E)?(R)?(D)?(A)?(S)?@', $this->output['door']))
                        {
                            $this->output['door'] = 'IZ';
                        }
                        else
                            if (preg_match('@D(E)?(R)?(E)?(I)?(T|X|CH)?(A)?@', $this->output['door']))
                            {
                                $this->output['door'] = 'DR';
                            }
                            elseif (strlen($this->output['door']) > 3) $this->output['door'] = '';
        }
        if ( ! empty($this->output['floor']) && preg_match('@(\w)+@', $this->output['floor']))
        {
            if (preg_match('@B(A)?(I)?(X|J)(O)?(S)?@', $this->output['floor']))
            {
                $this->output['floor'] = 'BAJO';
            }
            else
                if (preg_match('@AT(I)?(C)?(O)?@', $this->output['floor']))
                {
                    $this->output['floor'] = 'ATICO';

                }
                else
                    if (preg_match('@ENT(R)?(S)?(U)?(E)?(L)?(O)?@', $this->output['floor']))
                    {
                        $this->output['floor'] = 'ENT';
                    }
                    else
                        if (preg_match('@IZ(Q)?(U)?(I)?(E)?(R)?(D)?(A)?(S)?@', $this->output['floor']))
                        {
                            $this->output['floor'] = 'IZ';
                        }
                        else
                            if (preg_match('@D(E)?(R)?(E)?(I)?(T|X|CH)?(A)?@', $this->output['floor']))
                            {
                                $this->output['floor'] = 'DR';
                            }
                            elseif (strlen($this->output['floor']) > 3) $this->output['floor'] = '';
        }

    }

    public
    function find__street_after_number($del_tv = true)
    {
        $street = $this->cleanStreet();
        if ($pos = strpos($street, $this->output['number']))
        {
            $input_clean_street = trim(substr($street, 0, $pos + strlen($this->output['number'])));

            foreach (Regex::$ab_number__exp as $number_exp_key => $number_exp_value)
            {
                $input_clean_street = trim(preg_replace('@( )(' . $number_exp_value . '|' . $number_exp_key . ')( |[0-9]+)+( |[A-Za-zÇçÑñ]+|[.]+)*@', '', $input_clean_street));
            }

            if ($del_tv)
            {
                foreach (Regex::$roadtypes__exp as $roadtypes_exp_key => $roadtypes_exp_value)
                {
                    $input_clean_street = trim(preg_replace('@^((' . $roadtypes_exp_value . ')( ))+@', '', $input_clean_street, -1, $count_tv));
                    if ($count_tv > 0)
                    {
                        break;
                    }
                }
            }
            $this->output['street'] = trim($input_clean_street);
        }
        if (empty($this->output['street']) && $del_tv) $this->find__street_after_number(false);
    }
}
