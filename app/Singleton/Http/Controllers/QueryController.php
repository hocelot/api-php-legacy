<?php
/**
 * Created by PhpStorm.
 * User: Alex San Luis
 * Date: 16/06/2016
 * Time: 21:47
 */

namespace App\Http\Controllers;


use App\Http\Models\QueryModel;
use Illuminate\Http\Request;

class QueryController extends Controller
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
    }

    public function listQueries()
    {
        $result = QueryModel::listQueries($this->request_params->client_id);
        $rows = 0;
        //TODO count¿?
        foreach ($result->val_asrt_tgmiratingeval->str_listado_evaluaciones as $pers) {
            $rows++;
        }
        //$json = json_encode($result->val_asrt_tgmiratingeval->str_listado_evaluaciones);

        return response()->json(["status"=> 200,"draw" => 1, "recordsTotal" => $rows, "recordsFiltered" => $rows, "data" => $result->val_asrt_tgmiratingeval->str_listado_evaluaciones]);
    }

    public function getQuery(){
        $result = QueryModel::getDataQuery($this->request_params['client_id'],$this->request_params['query_num']);
        return $result->ref_str_tasaparocom->str_tasaparocom[0];
    }
}