<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Mail;
use Mail;

class ContactController extends Controller
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
    }

    public function solicitarApi(Request $request)
    {
        $data = $request->all();
        $email = $data['params']['email'];
        $nombre = $data['params']['nombre'];
        $mensaje = $data['params']['mensaje'];
        $cons_year = $data['params']['cons_year'];
        $producto = $data['params']['producto'];
        $telefono = $data['params']['tel'];
        $c = false;
        $prod = '';
        foreach ($producto as $item) {
            if (!$c) {
                $prod = $item;
                $c = true;
            } else {
                $prod .= ', ' . $item;
            }
        }
        $msj = 'Email: ' . $email . PHP_EOL . 'Nombre: ' . $nombre . PHP_EOL . 'Teléfono: ' . $telefono . PHP_EOL .'Num consultas: ' . $cons_year . PHP_EOL . 'Producto: ' . $prod . PHP_EOL . 'Mensaje: ' . $mensaje;

        Mail::raw($msj, function ($msg) {
            $msg->subject('Solicitud API');
            $msg->to(['r.marcos@hocelot.com','j.verdejo@hocelot.com','a.camacho@hocelot.com','j.cuervo@hocelot.com']);
            $msg->from(['no-reply@hocelot.com']);
        });
        return response()->json(['status' => '200']);
        //return array('status' => '200');
    }
    public function solicitarApiNew(Request $request)
    {
        $data = $request->all();
        $email = $data['params']['email'];
        $nombre = $data['params']['nombre'];
        $apellidos = $data['params']['apellidos'];
        $cargo = $data['params']['cargo'];
        $telefono = $data['params']['tel'];
        $nombreEmp = $data['params']['nom_emp'];
        $origen = $data['params']['origen'];
        $msj = 'Email: ' . $email . PHP_EOL . 'Nombre: ' . $nombre . PHP_EOL .'Apellidos: ' . $apellidos . PHP_EOL . 'Teléfono: ' . $telefono  . PHP_EOL . 'Cargo: ' . $cargo . PHP_EOL . 'Nombre de la empresa: ' . $nombreEmp. PHP_EOL . 'Origen: ' . $origen;
        Mail::raw($msj, function ($msg) {
            $msg->subject('Solicitud API');
            $msg->to(['a.olmedo@hocelot.com', 'd.cabezas@hocelot.com']);
//            $msg->to(['r.marcos@hocelot.com','j.verdejo@hocelot.com','a.camacho@hocelot.com','j.cuervo@hocelot.com']);
            $msg->from(['no-reply@hocelot.com']);
        });
        return response()->json(['status' => '200']);
        //return array('status' => '200');
    }
}

?>