<?php
/**
 * Created by PhpStorm.
 * User: DC
 * Date: 16/02/2017
 * Time: 16:09
 */

namespace App\Http\Controllers\MediaCheck;

use App\Http\Controllers\Proxies\Proxies;
use App\Http\Models\UtilClass;
use GuzzleHttp\Exception\ConnectException;
use stdClass;
use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;
use Log;

class Vibbo extends \Threaded
{
    private $tlf, $nombre_via;

    public function __construct($phone, $nombre_via)
    {
        $this->tlf = trim($phone);
        $this->nombre_via = $nombre_via;
    }

    public function run()
    {
        require_once(__DIR__ . '/../../../../vendor/autoload.php');
        require_once(__DIR__ . '/../../../../bootstrap/app.php');
        $this->worker->addData('vibbo', $this->search());
    }


    private function curl($page)
    {
        $obj_proxy = new Proxies('vibbo');
        $ip_real = false;
        do {
            $ip = $obj_proxy->get();
            $obj_proxy->setUsed($ip);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $page);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            if ($ip) {
                curl_setopt($ch, CURLOPT_PROXY, $ip);
                curl_setopt($ch, CURLOPT_PROXYUSERPWD, "mrhc:QwEr1234");
            } else  $ip_real = true;
            $result = curl_exec($ch);
            $info = curl_getinfo($ch);
            curl_close($ch);
            if (!$ip_real) {
                $obj_proxy->unlock($info["http_code"]);
            }
        } while ($info["http_code"] == 400 && !$ip_real);
        if ($info["http_code"] == 400 && $ip_real) return false;
//        var_dump($result);
        return $result;
    }

    public function search()
    {
        $vibbo = new stdClass();
        $vibbo->score_vivienda = 'OK';
        $vibbo->score_trabajo = 'OK';
        $vibbo->hits = [];
        $vibbo->anuncio_urls = [];

        $crawler = new Crawler();
        $page = 1;

        do {
            $html_web = 'https://www.vibbo.com/anuncios-toda-espana/' . $this->tlf . '.htm?ca=0_s&x=3&w=3&c=0&o=' . $page;
            $crawler->clear();
            if ($resp = $this->curl($html_web)) {
                $crawler->addHtmlContent($resp);

                if ($crawler->filter('div[class="basicList flip-container list_ads_row  "]')->count() > 0) {
                    $crawler->filter('div[class="basicList flip-container list_ads_row  "]')->each(function ($node) use ($vibbo) {
                        $url = preg_replace('|//|', 'https://', $node->filter('div > div > div > div > div > a')->attr('href'));
                        if (!in_array($url, $vibbo->anuncio_urls)) {
                            $vibbo->anuncio_urls[] = $url;
                        }
                    });
                }
                $page++;
            } else break;
        } while ($crawler->filter('a[class="box-icon-filter box-icon-pagination-top"] > span[class="icon-ics_navigation_ic_Forward"]')->count() > 0);

        if (count($vibbo->anuncio_urls) > 0) {
            foreach ($vibbo->anuncio_urls as $anuncio_url) {
                $crawler->clear();
                $crawler->addHtmlContent($this->curl($anuncio_url));
                $anuncio = new stdClass();
                $anuncio->url = $anuncio_url;
                if ($crawler->filter('.sellerBox__info__name')->count() > 0) {
                    $anuncio->alias = trim(mb_strtolower($crawler->filter('.sellerBox__info__name')->text()));
                }
                if ($crawler->filter('#breadcrumbsContainer > p > a')->eq(4)->count() > 0) {
                    $anuncio->type = trim(mb_strtolower($crawler->filter('#breadcrumbsContainer > p > a')->eq(4)->text()));
                    if ($anuncio->type != 'venta' || $anuncio->type != 'compra') {
                        $anuncio->type = 'venta';
                    }
                }
                if ($crawler->filter('#breadcrumbsContainer > p > a')->eq(2)->count() > 0) {
                    $anuncio->category = trim(mb_strtolower($crawler->filter('#breadcrumbsContainer > p > a')->eq(2)->text()));
                }
                if ($crawler->filter('h1.productTitle')->count() > 0) {
                    $anuncio->titulo = trim($crawler->filter('h1.productTitle')->text());
                }

                if ($crawler->filter('#descriptionText')->count() > 0) {
                    $anuncio->cuerpo = '';
                    $eq_max = $crawler->filter('#descriptionText')->count();
                    $eq = 0;
                    do {
                        $anuncio->cuerpo .= ' ' . $crawler->filter('#descriptionText')->eq($eq)->text();
                        $eq++;
                    } while ($eq < $eq_max);
                    $anuncio->cuerpo = trim($anuncio->cuerpo);
                }

                if ($crawler->filter('.priceTags > .priceLevel1')->count() > 0) {
                    $anuncio->price = str_replace('.', '', str_replace('€', '', trim($crawler->filter('.priceTags > .priceLevel1')->text())));
                }

                if ((isset($anuncio->price) && $anuncio->price > 3000) &&
                    ((isset($anuncio->cuerpo) && stristr($this->nombre_via, mb_strtoupper(UtilClass::elimina_acentos($anuncio->cuerpo)))) ||
                        (isset($anuncio->titulo) && stristr($this->nombre_via, mb_strtoupper(UtilClass::elimina_acentos($anuncio->titulo)))))
                ) {
                    if (isset($anuncio->type) && $anuncio->type == 'venta')
                        $vibbo->score_vivienda = 'KO';
                    elseif (!isset($anuncio->type)) $vibbo->score_vivienda = 'KO';
                }

                if ((isset($anuncio->type) && $anuncio->type == 'venta') &&
                    (isset($anuncio->category) && $anuncio->category == 'trabajo y formacin')) $vibbo->score_trabajo = 'KO';

                $vibbo->hits[] = $anuncio;
            }



        }
        unset($vibbo->anuncio_urls);

        return $vibbo;
    }
}