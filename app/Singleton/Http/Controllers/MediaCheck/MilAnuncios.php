<?php
/**
 * Created by PhpStorm.
 * User: rmarcos
 * Date: 7/10/16
 * Time: 11:47
 */

namespace App\Http\Controllers\MediaCheck;

use App\Http\Controllers\Proxies\Proxies;
use App\Http\Models\UtilClass;
use Symfony\Component\DomCrawler\Crawler;
use Log;
use stdClass;

class MilAnuncios extends \Threaded
{
    // http://www.milanuncios.com/anuncios/tlf.htm
    private $tlf, $nombre_via;

    public function __construct($mobile, $nombre_via)
    {
        $this->tlf = trim($mobile);
        $this->nombre_via = $nombre_via;
    }

    public function run()
    {
        require_once(__DIR__ . '/../../../../vendor/autoload.php');
        require_once(__DIR__ . '/../../../../bootstrap/app.php');
        $this->worker->addData('milanuncios', $this->search());
    }

    public function search()
    {
        $milanuncio = new stdClass();
        $milanuncio->score_vivienda = 'OK';
        $milanuncio->score_trabajo = 'OK';
        $milanuncio->score_finanziacion = 'OK';

        $milanuncio->hits = [];
        $milanuncio->anuncio = array();
        $crawler = new Crawler();
        $page = 1;
        do {
            $crawler->clear();
            $html_web = 'http://www.milanuncios.com/anuncios/' . $this->tlf . ".htm?pagina=" . $page;
            if ($resp = $this->curl($html_web)) {
                $crawler->addHtmlContent($resp);
                $titulos = array();
                if($crawler->filter('#cuerpo > .aditem')->count() > 0) {
                    $crawler->filter('#cuerpo > .aditem')->each(function ($node) use ($milanuncio, $titulos) {
                        $titulo = $node->filter('.aditem-detail > .aditem-detail-title')->first()->text();
                        $url = $node->filter('.aditem-detail > .aditem-detail-title')->first()->attr('href');
                        if (!in_array(trim($titulo), $titulos)) {
                            $titulos[] = trim($titulo);
                            $anuncio = new \stdClass();
                            $anuncio->titulo = trim($titulo);
                            $anuncio->url = trim('http://www.milanuncios.com' . $url);
                            $milanuncio->anuncio[] = $anuncio;
                        }
                    });
                }else break;
                $page++;
            }
        } while ($crawler->filter('#cuerpo > .aditem')->count() > 0);
        if (count($milanuncio->anuncio) > 0) {
            foreach ($milanuncio->anuncio as $anuncio) {
                //vaciamos crawler
                $crawler->clear();
                if ($resp = $this->curl($anuncio->url)) {
                    $crawler->addHtmlContent($resp);
                    if ($crawler->filter('.contenido > .contentInnerBoxPagAnu > #cuerpo > .pagAnuRightColumn > .pagAnuContact >.pagAnuContactNombre')->count() > 0) {
                        $anuncio->alias = trim($crawler->filter('.contenido > .contentInnerBoxPagAnu > #cuerpo > .pagAnuRightColumn > .pagAnuContact >.pagAnuContactNombre')->first()->text());
                    }
//                    unset($anuncio->url);
                    if ($crawler->filter('.pagAnuSubtitle > .pagAnuAdType')->count() > 0) {
                        $anuncio->type = trim($crawler->filter('.pagAnuSubtitle > .pagAnuAdType')->text());
                    }
                    $cat_sub = $crawler->filter('.contenido > .cab > .beacrumb-container > .beacrumb > a');
                    if ($cat_sub->count() >= 3) {
                        $anuncio->category = $cat_sub->eq(1)->text();
                        $anuncio->subcategory = $cat_sub->eq(2)->text();
                    } elseif ($cat_sub->count() == 2) {
                        $anuncio->category = $cat_sub->eq(1)->text();
                    } else {
                        continue;
                    }
                    if ($crawler->filter('.pagAnuCuerpoAnu')->count() > 0) {
                        $anuncio->cuerpo = trim($crawler->filter('.pagAnuCuerpoAnu')->text());
                    }
                    if ($crawler->filter('.pagAnuPrecioTexto')->count() > 0) {
                        $anuncio->price = str_replace('.', '', trim(str_replace($crawler->filter('.pagAnuPrecioTexto > sup')->text(), '', $crawler->filter('.pagAnuPrecioTexto')->text())));
                    }

                    if ((isset($anuncio->price) && $anuncio->price > 3000) &&
                        ((isset($anuncio->cuerpo) && (stristr($anuncio->cuerpo, $this->nombre_via) ||
                                    stristr($anuncio->cuerpo, mb_strtolower($this->nombre_via)))) ||
                            (isset($anuncio->titulo) && stristr($anuncio->titulo, mb_strtolower($this->nombre_via)) ||
                                stristr($anuncio->titulo, mb_strtolower($this->nombre_via))))
                    ) {
                        $milanuncio->score_vivienda = 'KO';
                    }
                    if((isset($anuncio->type) && mb_strtoupper($anuncio->type) == 'DEMANDA') &&
                        (isset($anuncio->category) && mb_strtoupper($anuncio->category) == 'EMPLEO')){
                        $milanuncio->score_trabajo = 'KO';
                    }
                    if((isset($anuncio->type) && mb_strtoupper($anuncio->type) == 'DEMANDA') &&
                        (isset($anuncio->category) && mb_strtoupper($anuncio->category) == 'NEGOCIOS') &&
                        (isset($anuncio->subcategory) && mb_strtoupper($anuncio->subcategory) == 'FINANCIACIN')){
                        $milanuncio->score_finanziacion = 'KO';
                    }

                    if (isset($anuncio->cuerpo)) unset($anuncio->cuerpo);
                    $milanuncio->hits = $milanuncio->anuncio;
                }
            }
        }
        unset($milanuncio->anuncio);
//        Log::debug('RESULTADO MILANUNCIO   '.print_r($milanuncio,true));

        return $milanuncio;
    }


    private
    function curl($page)
    {
        $obj_proxy = new Proxies('milanuncios');
        $ip_real = false;
//        Log::debug('Milanuncios pagina   '.$page);
            $ip = $obj_proxy->get();
            $obj_proxy->setUsed($ip);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $page);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36');
            if ($ip) {
                curl_setopt($ch, CURLOPT_PROXY, $ip);
                curl_setopt($ch, CURLOPT_PROXYUSERPWD, "mrhc:QwEr1234");
            } else  $ip_real = true;
            $result = curl_exec($ch);
            $info = curl_getinfo($ch);
            curl_close($ch);
            if (!$ip_real) {
                if($info["http_code"] === 301) $info["http_code"] = 200;
                $obj_proxy->unlock($info["http_code"]);
            }
//            Log::debug('INFO MA  '.print_r($info,true));
        if ($info["http_code"] == 400 && $ip_real) return false;
        return $result;
    }
}