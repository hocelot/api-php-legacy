<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 6/09/16
 * Time: 19:23
 */

namespace App\Http\Controllers\MediaCheck;
use App\Http\Models\UtilClass;

use Log;

class eTools extends \Threaded
{
public $results;
    private $first_name, $last_name, $name, $dni;
    public function __construct($name, $first_name, $last_name)
    {
        $this->name =  UtilClass::del_articulos(mb_strtoupper(UtilClass::elimina_acentos(trim($name))));
        $this->first_name =  UtilClass::del_articulos(mb_strtoupper(UtilClass::elimina_acentos(trim($first_name))));
        $this->last_name =  UtilClass::del_articulos(mb_strtoupper(UtilClass::elimina_acentos(trim($last_name))));
//        $this->dni = $dni;
    }

    public function run(){
        require_once(__DIR__ . '/../../../../vendor/autoload.php');
        require_once(__DIR__ . '/../../../../bootstrap/app.php');
        $this->worker->addData('search', $this->getSearch());
//        $this->results = $this->getSearch();
    }
    private function getSearch(){
        $site = 'http://www.etools.ch';
//        $str_search = urlencode('("'.$this->dni.'" AND "'.$this->name.'" AND "'.$this->first_name.'" AND "'.$this->last_name.'")');
        $str_search = urlencode(trim('"'.$this->name.' '.$this->first_name.' '.$this->last_name.'"'));
        $search = $this->curl($site . '/partnerSearch.do?partner=TestAdvanced&query=' . $str_search . '&language=ES&country=es&includeClusters=true');//country=web para todos //&maxRecords=200
        $res = new \SimpleXMLElement($search);
        $etool = new \stdClass();
        $etool->hits = array();
        foreach ($res->mergedRecords->record as $record){
            $r = new \stdClass();

            $r->title = mb_convert_encoding((string)$record->title,'UTF-8');
            $r->text = mb_convert_encoding((string)$record->text,'UTF-8');
            $r->url = mb_convert_encoding((string)$record->url,'UTF-8');
            $etool->hits[] = $r;

        }
            return $etool;
    }
    private
    function curl($page)
    {
        $url = $page;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }


}