<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 13/01/17
 * Time: 14:06
 */

namespace App\Http\Controllers;


use App\Http\Controllers\GeoCheck\GeoCheck;
use App\Http\Controllers\GeoFraud\GeoFraud;
use App\Http\Controllers\IdCheck\IDCheck;
use App\Http\Controllers\IdFraud\IdFraud;
use App\Http\Controllers\Worker\ThreadsManager;
use App\Http\Controllers\RiskScore\ScoreController;
use App\Http\Models\AuthModel;
use App\Http\Models\UtilClass;
use App\Jobs\WsdlJob;
use Carbon\Carbon;
use Log;

class BatchController {

    private static $scope = array(
        'id_check'    => 'ic_Tl3h7b3HsEeAe2b0DtW8zmpQ9EnBc9XjCsM7',
        'id_fraud'    => 'if_Fcz72MajCgEVDnf0s2ct8M52RffsQlI1tZM6',
        'geo_check'   => 'gc_pzP9O2TW9eOiCXn4Mo0gVUroCpg1wA5EuWWQ',
        'geo_fraud'   => 'gf_fjw6HcqXaXcJHkRUNSe2xbMygiiCbwkBltJ2',
        //       'bureau' => 'br_W3C5b4A0giMfbZC8b6ayjIEvihhubYsf0TsJ ',
        'risk_score'  => 'sc_IpqAf3mZZVVnUcMW56PBDfndae94DTIQOPbY',
        'media_check' => 'mc_neoUmzccfNTAm1vZOaYvnr2XEMFAu7MSemCW'
    );

    public static function process($files_name)
    {
        set_time_limit(0);
        $start = microtime(true);
        Log::debug('Empieza FICHERO: ' . $start);
        foreach ($files_name as $file)
        {
            $cabeceras = true;
            $filename = substr($file, strrpos($file, '/') + 1);
            $client_id = substr($filename, 0, strpos($filename, '-'));
            $clientscope = new BatchScopeController();
            $ip_id_oauth['ip'] = '192.168.1.0';
            $ip_id_oauth['oauth_id'] = app('db')->connection('oauth')->table('oauth_clients')->select('id')->where('ter_codigo', $client_id)->first()->id;
            $ip_id_oauth['id'] = $client_id;
            $client_name = app('db')->connection('oauth')->table('oauth_clients')->where('ter_codigo', $client_id)->first()->name;
            $filename_admin = '1-' . $client_name . substr($filename, 0, -4) . date('Y-m-d-H.m.s') . '.csv';
            $client_scope = app('db')->connection('oauth')->table('oauth_client_scopes')->where('client_id', $ip_id_oauth['oauth_id'])->lists('scope_id');
            Log::debug(print_r($file, true));
            Log::debug(print_r($ip_id_oauth, true));
            Log::debug(print_r($client_scope, true));
            if (($archivo = fopen($file, 'r')) !== false)
            {
                while (($datos = fgetcsv($archivo, 1000, ";")) !== false)
                {
                    $id_check = $id_fraud = $geo_check = $geo_fraud = $media_check = $risk_score = '';
                    if ( ! in_array(self::$scope['id_check'], $client_scope))
                    {
                        for ($i = 0; $i < 5; $i++)
                            $id_check .= 'No tiene contratado ID CHECK;';
                    }

                    if ( ! in_array(self::$scope['id_fraud'], $client_scope))
                    {
                        $id_fraud = 'No tiene contratado ID FRAUD;';
                    }
                    if ( ! in_array(self::$scope['geo_check'], $client_scope))
                    {
                        for ($i = 0; $i < 6; $i++)
                            $geo_check .= 'No tiene contratado GEO CHECK;';
                    }
                    if ( ! in_array(self::$scope['geo_fraud'], $client_scope))
                    {
                        for ($i = 0; $i < 3; $i++)
                            $geo_fraud .= 'No tiene contratado GEO FRAUD;';
                    }
                    if ( ! in_array(self::$scope['risk_score'], $client_scope))
                    {
                        for ($i = 0; $i < 53; $i++)
                            $risk_score .= 'No tiene contratado RISK SCORE;';
                    }
                    if ( ! in_array(self::$scope['media_check'], $client_scope))
                    {
                        for ($i = 0; $i < 5; $i++)
                            $media_check .= 'No tiene contratado MEDIA CHECK;';
                    }
                    // Aqui estructura CSV
                    Log::debug(print_r($datos, true));
                    if ( ! (isset($datos[0])) || ! (isset($datos[1])) || ! (isset($datos[2])) || ! (isset($datos[3]))
                        || ! (isset($datos[4])) || ! (isset($datos[5])) || ! (isset($datos[6])) || ! (isset($datos[7]))
                        || ! (isset($datos[8])) || ! (isset($datos[9])) || ! (isset($datos[10])) || ! (isset($datos[11]))
                        || ! (isset($datos[12])) || ! (isset($datos[13])) || ! (isset($datos[14])) || ! (isset($datos[15]))
                        || ! (isset($datos[16])) || ! (isset($datos[17])) || ! (isset($datos[18])) || ! (isset($datos[19]))
                    )
                    {
                        file_put_contents('/var/www/hocelot/files/processing/res-' . $filename, 'ESTA LINEA CONTIENE UN ERROR;' . PHP_EOL, FILE_APPEND);
                        continue;
                    }
                    $request_params = new \stdClass();
                    $request_params->user = new \stdClass();
                    $request_params->user->name = self::encoding_type(trim($datos[0]));
                    $request_params->user->first_name = self::encoding_type(trim($datos[1]));
                    $request_params->user->last_name = self::encoding_type(trim($datos[2]));
                    $request_params->user->cif_dni = trim($datos[4]);
                    $request_params->user->phone = trim($datos[6]);
                    $request_params->user->gender = mb_strtoupper(trim($datos[5]));
                    $request_params->user->email = trim($datos[7]);
//                    $date = date_create(trim($datos[3]));
                    $date_parse = date_parse(trim($datos[3]));
                    $date = new \Carbon\Carbon();
                    $request_params->user->birthday = $date->setDate($date_parse['year'], $date_parse['month'], $date_parse['day'])->format('Y-m-d');


                    $request_params->address = new \stdClass();
                    $request_params->address->road_type = trim($datos[8]);
                    $request_params->address->street = self::encoding_type(trim($datos[9]));
                    $request_params->address->number = self::encoding_type(trim($datos[10]));
                    $request_params->address->floor = self::encoding_type(trim($datos[15]));
                    $request_params->address->door = self::encoding_type(trim($datos[16]));
                    $request_params->address->stair = self::encoding_type(trim($datos[14]));
                    $request_params->address->km = self::encoding_type(trim($datos[12]));
                    $request_params->address->block = self::encoding_type(trim($datos[13]));
                    $request_params->address->letter = self::encoding_type(trim($datos[11]));
                    $request_params->address->province = self::encoding_type(trim($datos[19]));
                    $request_params->address->town = self::encoding_type(trim($datos[18]));
                    $request_params->address->zip_code = str_pad(trim($datos[17]), 5, '0', STR_PAD_LEFT);
                    $request_params->user->client_id = $client_id;
                    $request_params->user->from = 'B';
                    if ($ip_id_oauth['id'] == '52301' || $ip_id_oauth['id'] == '6')
                    {

                        if (( ! isset($datos[26]) || empty($datos[26])) || ( ! isset($datos[24]) || empty($datos[24])) || ( ! isset($datos[22]) || empty($datos[22])) || ( ! isset($datos[23]) || empty($datos[23])))
                        {
                            file_put_contents('/var/www/hocelot/files/processing/res-' . $filename, 'FALTAN DATOS TIN IMPORTE O PLAZO;' . PHP_EOL, FILE_APPEND);
                            continue;
                        }
                        else
                        {
                            $request_params->economics = new \stdClass();
                            $request_params->economics->tin = self::encoding_type(trim($datos[24]));
                            $request_params->economics->tin = UtilClass::tofloat(ltrim($request_params->economics->tin));

                            if (isset($datos[25]))
                            {
                                $request_params->economics->tae = self::encoding_type(trim($datos[25]));
                                $request_params->economics->tae = UtilClass::tofloat(ltrim($request_params->economics->tae));

                            }

//                            if (preg_match('/[0-9]*/', $request_params->economics->tae)) {
//                                $request_params->economics->tae = $request_params->economics->tae / 100;
//                            }
                            $request_params->economics->plazo = trim($datos[23]);
                            $request_params->economics->importe = intval(round(UtilClass::tofloat(ltrim($datos[22])), 0, PHP_ROUND_HALF_UP));


                            $request_params->economics->producto = self::encoding_type(trim($datos[20]));
                            $request_params->economics->finalidad = self::encoding_type(trim($datos[21]));
                            $request_params->economics->tipo_cliente = trim(mb_strtoupper(self::encoding_type(trim($datos[26]))));

                            if (isset($datos[25]))
                                $request_params->economics->tae = trim($datos[25]);
                            else $request_params->economics->tae = '';

                        }
                    }


                    $clientscope->setParams($ip_id_oauth, $request_params);
                    $json = $clientscope->initialize();
//                    Log::debug('RESULTADO: ' . print_r($json->getContent(), true));
//                    if($ip_id_oauth['id'] == '5'){
//                        file_put_contents('/var/www/hocelot/files/processing/res-' . $request_params->user->cif_dni.'.json' , $json, FILE_APPEND);
//                    }
                    $json = json_decode($json->getContent());

                    for ($i = 0; $i < 83; $i++)
                        $result[$i] = '';
                    //DNI
                    $result[0] = $request_params->user->cif_dni;
                    //NOMBRE
                    $result[1] = mb_strtoupper($request_params->user->name . ' ' . $request_params->user->first_name . ' ' . $request_params->user->last_name);
                    //MOVIL
                    $result[2] = $request_params->user->phone;
                    //EMAIL
                    $result[3] = $request_params->user->email;
                    //DIRECCION
                    if (isset($json->geo_check->hits->address))
                    {
                        $result[4] = $json->geo_check->hits->address;
                        $result[12] = $json->geo_check->hits->level;
                        $result[35] = $json->geo_check->hits->latitude;
                        $result[36] = $json->geo_check->hits->longitude;
                        $result[37] = $json->geo_check->hits->used_for;
                        $result[38] = $json->geo_check->hits->type;
                        $result[39] = $json->geo_check->hits->m2;

                        if (isset($json->geo_check->hits->parcela))
                            $result[82] = $json->geo_check->hits->parcela;
                        if (isset($json->geo_check->hits->ine))
                            $result[83] = $json->geo_check->hits->ine;


                    }
                    else
                    {
                        $result[4] = trim(trim($request_params->address->road_type . ' ') . trim($request_params->address->street . ' ') .
                            trim($request_params->address->number . ' ') .
                            trim($request_params->address->letter . ' ') .
                            trim($request_params->address->km . ' ') .
                            trim($request_params->address->block . ' ') .
                            trim($request_params->address->stair . ' ') .
                            trim($request_params->address->floor . ' ') .
                            trim($request_params->address->door));
                        $result[12] = 5;
                        $result[35] = ' ';
                        $result[36] = ' ';
                        $result[37] = ' ';
                        $result[38] = ' ';
                        $result[39] = ' ';

                    }
                    //TOWN
                    $result[5] = $request_params->address->town;
                    //PROVINCIA
                    $result[6] = $request_params->address->province;
                    //FECHA NACIMIENTP
                    $result[7] = $request_params->user->birthday;
                    //GENERO
                    $result[8] = $request_params->user->gender;

                    if (isset($json->id_check->hits))
                    {
                        if ($json->id_check->hits->alert == true) $json->id_check->hits->alert = 'true';
                        $json->id_check->hits->alert = 'false';
                        $result[9] = $json->id_check->hits->score_id_check;
                        $result[10] = $json->id_check->hits->correlacion_id_check;
                        $result[11] = $json->id_check->hits->alert;
                    }
                    else
                    {
                        $result[9] = ' ';
                        $result[10] = ' ';
                        $result[11] = ' ';
                    }

                    if (isset($json->geo_fraud->hits->name))
                    {
                        $result[13] = $json->geo_fraud->hits->name;
                        $result[14] = $json->geo_fraud->hits->correlacion;
                        $result[15] = $json->geo_fraud->hits->phone;
                        $result[16] = $json->geo_fraud->hits->address;
                    }
                    else
                    {
                        $result[13] = 'Sin datos';
                        $result[14] = 0;
                        $result[15] = 'Sin datos';
                        $result[16] = 'Sin datos';
                    }

                    if (isset($json->risk_score->hits))
                    {
                        if ( ! empty($json->risk_score->hits->edad))
                            $result[17] = $json->risk_score->hits->edad;
                        else
                        {
                            if ( ! empty($request_params->user->birthday))
                            {
                                $date_user = \Carbon\Carbon::parse($request_params->user->birthday);
                                $result[17] = $date_user->diffInYears(\Carbon\Carbon::now());
                            }
                            else $result[17] = '';
                        }
                        $result[18] = $json->risk_score->hits->nivel_estudios;
                        $result[19] = $json->risk_score->hits->trabajo;
                        $result[20] = $json->risk_score->hits->dependiente;
                        $result[21] = $json->risk_score->hits->tenencia;
                        $result[22] = $json->risk_score->hits->tipo_moroso;
                        $result[23] = $json->risk_score->hits->ingreso_mensual_hogar;
                        $result[24] = $json->risk_score->hits->ingreso_mensual_individual;
                        $result[25] = $json->risk_score->hits->renta_vivienda_hogar;
                        $result[26] = $json->risk_score->hits->renta_vivienda_individual;
                        $result[27] = $json->risk_score->hits->capacidad_ahorro;
                        $result[28] = $json->risk_score->hits->tasa_esfuerzo;
                        $result[29] = $json->risk_score->hits->tasa_paro_correg;
                        $result[30] = $json->risk_score->hits->desviacion_renta;
                        $result[31] = $json->risk_score->hits->periodo_medio_pago;
                        //falta 32
                        $result[32] = $json->risk_score->hits->fiabilidad_pago;
                        $result[33] = $json->risk_score->hits->probabilidad_impago;
                        $result[34] = $json->risk_score->hits->umbral_pobreza;
                        $result[40] = $json->risk_score->hits->precio;
                        $result[41] = $json->risk_score->hits->grupo_gastos_individual->alimentacion;
                        $result[42] = $json->risk_score->hits->grupo_gastos_individual->alcohol_tabaco;
                        $result[43] = $json->risk_score->hits->grupo_gastos_individual->ropa_calzado;
                        $result[44] = $json->risk_score->hits->grupo_gastos_individual->vivienda_suministros;
                        $result[45] = $json->risk_score->hits->grupo_gastos_individual->mobiliario;
                        $result[46] = $json->risk_score->hits->grupo_gastos_individual->salud;
                        $result[47] = $json->risk_score->hits->grupo_gastos_individual->transporte;
                        $result[48] = $json->risk_score->hits->grupo_gastos_individual->comunicaciones;
                        $result[49] = $json->risk_score->hits->grupo_gastos_individual->ocio;
                        $result[50] = $json->risk_score->hits->grupo_gastos_individual->educacion;
                        $result[51] = $json->risk_score->hits->grupo_gastos_individual->restaurantes;
                        $result[52] = $json->risk_score->hits->grupo_gastos_individual->otros;

                        $result[53] = $json->risk_score->hits->grupo_gastos_hogar->alimentacion;
                        $result[54] = $json->risk_score->hits->grupo_gastos_hogar->alcohol_tabaco;
                        $result[55] = $json->risk_score->hits->grupo_gastos_hogar->ropa_calzado;
                        $result[56] = $json->risk_score->hits->grupo_gastos_hogar->vivienda_suministros;
                        $result[57] = $json->risk_score->hits->grupo_gastos_hogar->mobiliario;
                        $result[58] = $json->risk_score->hits->grupo_gastos_hogar->salud;
                        $result[59] = $json->risk_score->hits->grupo_gastos_hogar->transporte;
                        $result[60] = $json->risk_score->hits->grupo_gastos_hogar->comunicaciones;
                        $result[61] = $json->risk_score->hits->grupo_gastos_hogar->ocio;
                        $result[62] = $json->risk_score->hits->grupo_gastos_hogar->educacion;
                        $result[63] = $json->risk_score->hits->grupo_gastos_hogar->restaurantes;
                        $result[64] = $json->risk_score->hits->grupo_gastos_hogar->otros;
                        $result[65] = $json->risk_score->hits->score;

                        if ($ip_id_oauth['id'] == '52301' || $ip_id_oauth['id'] == '6')
                        {
                            if ( ! empty($json->risk_score->hits->respuesta))
                                $result[66] = $json->risk_score->hits->respuesta;
                            else $result[66] = 'D';
                        }
                    }
                    else
                    {
                        for ($i = 17; $i < 65; $i++)
                        {
                            if ($i != 35 && $i != 36 && $i != 37 && $i != 38 && $i != 39)
                                $result[$i] = ' ';
                        }
                        if ($ip_id_oauth['id'] == '52301' || $ip_id_oauth['id'] == '6')
                        {
                            $result[66] = 'D';
                        }
                        if ( ! empty($request_params->user->birthday))
                        {
                            $date_user = \Carbon\Carbon::parse($request_params->user->birthday);
                            $result[17] = $date_user->diffInYears(\Carbon\Carbon::now());
                        }
                        else $result[17] = '';
                    }


                    if ($cabeceras)
                    {
                        $str_cabeceras = 'ID;NAME;MOBILE;EMAIL;ADDRESS;CITY;REGION;BIRTH DATE;GENDER;OK/KO;CORR;ALERT;SCORE;NAME;CORR;PHONE;PHONE ADDRESS' .
                            ';AGE;EDUCATION;WORK;DEPENDENCY;POSSESSION;TYPE OF DELINQUENCY;NET INCOME (H);NET INCOME (I);' .
                            'HOUSE EXPENSE (HOME);HOUSE EXPENSE (IND.);SAVINGS CAPABILITY;EFFORT RATE;UNEMPLOYMENT RATE;' .
                            'INCOME VARIATION;AVERAGE PAYMENT PERIOD;PAYMENT RELIABILITY;PROBABILITY OF DEFAULT;' .
                            '% POVERTY THRESHOLD;LATITUDE;LONGITUDE;USAGE;TYPE;SQM;ESTIMATED PRICE;FOOD (EXP I);' .
                            'ALCOHOL & TOBACCO (EXP I);CLOTHES & FOOTWEAR (EXP I);' .
                            'HOUSE & SUPPLIES (EXP I);FURNITURE (EXP I);HEALTHCARE (EXP I);TRANSPORT (EXP I);TELCO (EXP I);LEISURE (EXP I);' .
                            'EDUCATION (EXP I);RESTAURANTS (EXP I);OTHERS (EXP I);FOOD (EXP H);ALCOHOL & TOBACCO (EXP H);' .
                            'CLOTHES & FOOTWEAR (EXP H);HOUSE & SUPPLIES (EXP H);FURNITURE (EXP H);HEALTHCARE (EXP H);' .
                            'TRANSPORT (EXP H);TELCO (EXP H);LEISURE (EXP H);EDUCATION (EXP H);RESTAURANTS (EXP H);OTHERS (EXP H);SCORE;';
                        if ($ip_id_oauth['id'] == '52301' || $ip_id_oauth['id'] == '6')
                        {
                            $str_cabeceras .= 'DECISSION';
                        }
                        //Fichero admin con todo
                        $str_cabeceras_admin = $str_cabeceras . ';;NOMBRE REAL;IMPORTE;PLAZO;PRODUCTO;FINALIDAD;FINALIDAD_ANDRES;TIN;TAE;EMPLEADO/GENERICO;OK/KO;MILANUNCIOS;VIBBO;IDEALISTA;ETOOLS';
                        file_put_contents('/var/www/hocelot/files/processing/res-' . $filename_admin, $str_cabeceras_admin . PHP_EOL, FILE_APPEND);

                        file_put_contents('/var/www/hocelot/files/processing/res-' . $filename, $str_cabeceras . PHP_EOL, FILE_APPEND);

                        $cabeceras = false;
                    }
                    file_put_contents('/var/www/hocelot/files/processing/res-' . $filename, implode(';', $result) . PHP_EOL, FILE_APPEND);

                    $result[67] = ' ';
                    if (isset($json->id_check->hits->aeat_name))
                        $result[68] = $json->id_check->hits->aeat_name;
                    else $result[68] = '';
                    if (isset($request_params->economics->importe))
                        $result[69] = number_format($request_params->economics->importe, 2, ',', '.');//importe
                    else $result[69] = ' ';
                    if (isset($request_params->economics->plazo))
                        $result[70] = $request_params->economics->plazo;//plazo
                    else $result[70] = ' ';
                    if (isset($request_params->economics->producto))
                        $result[71] = $request_params->economics->producto;//producto
                    else $result[71] = ' ';
                    if (isset($request_params->economics->finalidad))
                        $result[72] = $request_params->economics->finalidad;//finalidad
                    else $result[72] = ' ';
                    if (isset($json->risk_score->hits->finalidad))
                        $result[73] = $json->risk_score->hits->finalidad;//finalidad_andres
                    else $result[73] = ' ';
                    if (isset($request_params->economics->tin))
                        $result[74] = number_format($request_params->economics->tin, 2, ',', '.');//tin
                    else $result[74] = ' ';
                    if (isset($request_params->economics->tae) && ! empty($request_params->economics->tae))
                        $result[75] = number_format($request_params->economics->tae, 2, ',', '.');//tae
                    else $result[75] = ' ';
                    if (isset($request_params->economics->tipo_cliente))
                        $result[76] = $request_params->economics->tipo_cliente;//empleado_carrefour
                    else $result[76] = ' ';

                    if (isset($json->media_check->score))
                        $result[77] = $json->media_check->score;
                    else $result[77] = ' ';
                    if (isset($json->media_check->hits->milanuncios))
                        $result[78] = str_replace(';', ',', json_encode($json->media_check->hits->milanuncios));
                    else $result[78] = ' ';
                    if (isset($json->media_check->hits->vibbo))
                        $result[79] = str_replace(';', ',', json_encode($json->media_check->hits->vibbo));
                    else $result[79] = ' ';
                    if (isset($json->media_check->hits->idealista))
                        $result[80] = str_replace(';', ',', json_encode($json->media_check->hits->idealista));
                    else $result[80] = ' ';
                    if (isset($json->media_check->hits->search))
                        $result[81] = str_replace(';', ',', json_encode($json->media_check->hits->search));
                    else $result[81] = ' ';

                    file_put_contents('/var/www/hocelot/files/processing/res-' . $filename_admin, implode(';', $result) . PHP_EOL, FILE_APPEND);

                }
                rename('/var/www/hocelot/files/processing/res-' . $filename, '/var/www/hocelot/files/processed/' . $filename);
                rename('/var/www/hocelot/files/processing/res-' . $filename_admin, '/var/www/hocelot/files/processed/' . $filename_admin);

            }
            $end = microtime(true) - $start;
            Log::debug('Termina FICHERO: ' . $end);
        }
    }

    private static function encoding_type($str)
    {
//       if(mb_detect_encoding($str, 'UTF-8', true))
//          return utf8_decode($str);
//        else return utf8_encode($str);
        $text = trim(mb_convert_encoding($str, 'UTF-8'));

        return str_replace('  ', ' ', $text);
    }

}