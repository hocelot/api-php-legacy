<?php
/**
 * Created by PhpStorm.
 * User: a.luis
 * Date: 18/07/2016
 * Time: 12:39
 */

namespace App\Http\Controllers;


use App\Http\Models\OfferModel;
use App\Http\Models\UserModel;
use Illuminate\Http\Request;

class OfferController extends Controller {

	protected $client_type;
    public function __construct(Request $request)
    {
        parent::__construct($request);
		$this->client_type = UserModel::getType($this->request_params['client_id']);
	}

	public function getOffers()
	{
		$result = OfferModel::getOffers($this->client_type);
		return response()->json(['offers'=>$result,'status'=>200]);
	}

	public function getOfferPrice()
	{
		$result = OfferModel::getOfferPrice($this->client_type,$this->request_params['credits_num']);
		return response()->json(['offer_price'=>$result,'status'=>200]);
	}

}