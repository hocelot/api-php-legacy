<?php

/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 30/11/16
 * Time: 12:29
 */
namespace App\Http\Controllers\Worker;
use Worker;
class ThreadsManager extends Worker
{
    public $data = [];

    public function start($options = PTHREADS_INHERIT_ALL) { return parent::start(PTHREADS_INHERIT_NONE); }

    public function addData($key,$data)
    {
        $this->data=array_merge($this->data,[$key => $data]);
    }
}