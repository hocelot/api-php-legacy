<?php

/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 9/11/16
 * Time: 12:41
 */
namespace App\Http\Controllers\Proxies;

use App\Http\Models\Proxies\ProxiesModel;
use Log;
class Proxies
{
    private $service, $used_proxies, $actual ,$debug = false;

    public function __construct($service)
    {
        $this->service = $service;
        $this->used_proxies = array();
    }

    public function get()
    {
        $count = 0;
        if($this->debug)return false;
        do {
            $proxy = ProxiesModel::get($this->service, $this->used_proxies);
            $count++;
            if (!$proxy) break;

        } while (in_array($proxy, $this->used_proxies) && $count < ($this->okcount()+1));

        if (!$proxy) return false;
        $this->actual = $proxy;
        ProxiesModel::lock($this->actual);

        return $proxy;
    }

    public function unlock($status)
    {
        ProxiesModel::unlock($this->actual, $this->service, $status);
    }

    public function setUsed($proxy)
    {
        if(!$this->debug)
        $this->used_proxies[] = $proxy;
    }

    private function okcount()
    {
        if($this->debug)return 1;
        return ProxiesModel::okcount($this->service);
    }
}