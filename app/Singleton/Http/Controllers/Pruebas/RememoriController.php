<?php

/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 7/03/17
 * Time: 9:05
 */
namespace App\Http\Controllers\Pruebas;

use App\Http\Controllers\Proxies\Proxies;
use Goutte\Client;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use stdClass;

class RememoriController extends BaseController
{
    public function initialize(Request $request)
    {
        $obj_proxy = new Proxies('rememori');
        $ip_real = false;
        do {
            $ip = $obj_proxy->get();
            $obj_proxy->setUsed($ip);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'http://www.rememori.com');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            if ($ip) {
                curl_setopt($ch, CURLOPT_PROXY, $ip);
                curl_setopt($ch, CURLOPT_PROXYUSERPWD, "mrhc:QwEr1234");
            } else  $ip_real = true;
            $result = curl_exec($ch);
            $info = curl_getinfo($ch);
            curl_close($ch);
            if (!$ip_real) {
                $obj_proxy->unlock($info["http_code"]);
            }
        } while ($info["http_code"] != 200 && !$ip_real);
        $client = new Client();
        if(!$ip_real) {
            $guzzleClient = new \GuzzleHttp\Client([
                'headers' => [
                    'User-Agent' => 'Mozilla/5.0 (Windows NT 6.1; rv:49.0) Gecko/20100101 Firefox/49.0',
                ],
                'curl' => array(
                    CURLOPT_SSL_VERIFYPEER => false,
                ),
                'proxy' => 'http://mrhc:QwEr1234@' . $ip
            ]);
        }else{
            $guzzleClient = new \GuzzleHttp\Client([
                'headers' => [
                    'User-Agent' => 'Mozilla/5.0 (Windows NT 6.1; rv:49.0) Gecko/20100101 Firefox/49.0',
                ],
                'curl' => array(
                    CURLOPT_SSL_VERIFYPEER => false,
                )
            ]);
        }
        $client->setClient($guzzleClient);
        //Nombre ISABEL%20GARCIA
        $url = 'http://www.rememori.com/buscar/';
        if ($request->has('name') && !empty($request->input('name'))) {
            $url .= 'que:' . str_replace(' ', '%20', $request->input('name')) . '/';
        }
        if ($request->has('place') && !empty($request->input('place'))) {
            $url .= 'donde:' . str_replace(' ', '%20', $request->input('place')) . '/';
        }
        if ($request->has('fecha_ini') && !empty($request->input('fecha_ini'))) {
            $date = date_create($request->input('fecha_ini'));
            $url .= 'desde:' . date_format($date, "d-m-Y") . '/';
        }
        if ($request->has('fecha_hasta') && !empty($request->input('fecha_hasta'))) {
            $date = date_create($request->input('fecha_hasta'));
            $url .= 'hasta:' . date_format($date, "d-m-Y");
        }
        if (!empty($request->input('fecha_ini')) && empty($request->input('fecha_hasta'))) {
            $url .= 'hasta:' . date('d-m-Y');
        }
        if (empty($request->input('fecha_ini')) && empty($request->input('fecha_hasta'))) {
            $url .= 'desde:' . date('d-m-Y').'/hasta:' . date('d-m-Y');
        }
        if ($request->input('pass') != 'Esteban2017') {
            return response()->json(['results' => 'Contraseña incorrecta', 'status' => 400]);
        }
        $crawler = $client->request('GET', $url);
        $results = new stdClass();
        $results->people = [];
        $status = 200;
        $maxpage = 2;
        $pages = 0;
        do {
            $crawler->filter('ol > li')->each(function ($node) use ($results) {
                $aux = new stdClass();
                $aux->name = preg_replace('!\s+!', ' ', $node->filter('a')->text());
                $aux->description = preg_replace('!\s+!', ' ', $node->filter('div')->text());
                $results->people[] = $aux;
            });
            if ($crawler->filter('div.pagination > span > a.next')->count() > 0) {
                $crawler = $client->request('GET', 'http://www.rememori.com' . $crawler->filter('div.pagination > span > a.next')->attr('href'));
            }
            $pages++;
        } while ($crawler->filter('div.pagination > span > a.next')->count() > 0 && $pages < $maxpage);
        if (empty($results->people)) $status = 400;
        //$results->people
        return response()->json(['results' => $results->people, 'status' => $status]);
    }
}