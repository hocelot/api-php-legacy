<?php
/**
 * Created by PhpStorm.
 * User: rmarcos
 * Date: 5/10/16
 * Time: 13:31
 */

namespace App\Http\Controllers;

use App\Http\Models\AddressModel;
use App\Http\Models\AuthModel;
use App\Http\Models\ScoreModel;
use App\Http\Models\UtilClass;
use Laravel\Lumen\Routing\Controller as BaseController;

use App\Http\Controllers\SocialMedia\Einforma;
use App\Http\Controllers\SocialMedia\FindRelations;
use App\Http\Controllers\SocialMedia\IDCheck;
use App\Http\Controllers\SocialMedia\Infojobs;
use App\Http\Controllers\SocialMedia\Libreborme;
use App\Http\Controllers\SocialMedia\Linkedin;
use App\Http\Controllers\SocialMedia\MilAnuncios;
use App\Http\Controllers\SocialMedia\Searxme;
use Log;
class CsvController extends BaseController
{
    private $csvparams;
//Request $request
    public function process()
    {
        set_time_limit(0);
        // El archivo CSV deberá estar en ...
//        if (!$request->has('id')) {
//            return response()->json(['msg' => 'id del cliente ? ', 'status' => 400]);
//        }
        // . $request->input('id') .
        $csv_file = 'C:/nginx/www/api/storage/logs/'.date('Ymd').'.csv';

        if (!file_exists($csv_file)) {
            return response()->json(['msg' => 'No existe el fichero ' . $csv_file, 'status' => 400]);
        }

        $this->csvparams = new \stdClass();
        $this->csvparams->user = new \stdClass();
        $this->csvparams->address = new \stdClass();
        $this->csvparams->user->doc_type = 'NIF';
        $this->csvparams->user->client_id = 6;
        $this->csvparams->address->level = 1;

        if (($archivo = fopen($csv_file, 'r')) !== FALSE) {
            while (($datos = fgetcsv($archivo, 1000, ";")) !== FALSE) {
                $this->csvparams->user->name = mb_strtoupper(utf8_encode(trim($datos[0])), 'UTF-8');
                $this->csvparams->user->first_name = mb_strtoupper(utf8_encode(trim($datos[1])), 'UTF-8');
                $this->csvparams->user->last_name = mb_strtoupper(utf8_encode(trim($datos[2])), 'UTF-8');
                $this->csvparams->user->cif_dni = strtoupper(trim($datos[4]));
                $this->csvparams->user->phone = trim($datos[6]);
                $gender = strtoupper(trim($datos[5]));
                if ($gender == 'MASCULINO' || $gender == 'HOMBRE' || $gender == 'M') {
                    $gender = 'M';
                } else {
                    $gender = 'F';
                }
                $this->csvparams->user->gender = $gender;
                $this->csvparams->user->email = trim($datos[7]);
                $this->csvparams->user->birthday = date('Y-m-d', strtotime(str_replace('/','-',trim($datos[3]))));

//                if ($this->getRoadType(trim($datos[8]))) {
//                    //...
//                }
                $this->csvparams->address->road_type = trim($datos[8]);
                $this->csvparams->address->street = trim(utf8_encode($datos[9]));
//                if($datos[10] < 10){
//                    $datos[10] = '0'.$datos[10];
//                }
                $this->csvparams->address->number = trim($datos[10]);
                $this->csvparams->address->letter = trim($datos[11]);
                //$datos[11] letra
                $this->csvparams->address->floor = trim($datos[15]);
                $this->csvparams->address->door = trim($datos[16]);
                $this->csvparams->address->stair = trim($datos[14]);
                $this->csvparams->address->km = trim($datos[12]);
                $this->csvparams->address->block = trim($datos[13]);
                $this->csvparams->address->province = trim($datos[19]);
                $this->csvparams->address->town = trim(utf8_encode($datos[18]));
                $this->csvparams->address->zip_code = $datos[17];
                $this->csvparams->address->type = 'house';
                if (!empty($this->csvparams->address->floor)) {
                    $this->csvparams->address->type = 'flat';
                }

                //FALSEADO
                if( $this->csvparams->user->cif_dni == '50084265V'){
                    $this->csvparams->address->type = 'flat';

                }

//                $id_check = new IDCheck($this->csvparams->user->name, $this->csvparams->user->first_name, $this->csvparams->user->last_name, $this->csvparams->user->cif_dni);
//                $id_check = $id_check->check_id();
//                if ($id_check == false) {
//                    file_put_contents($this->csvparams->user->cif_dni . '.json', json_encode(['idcheck'=>$id_check]) . PHP_EOL, FILE_APPEND);
//                } else {
                    $score = $this->score();
//                    $milanuncio = new MilAnuncios($this->csvparams->user->phone);
//                    $linkedin = new Linkedin($this->csvparams->user->name, $this->csvparams->user->first_name, $this->csvparams->user->last_name, $this->csvparams->address->province);
//                    if(!empty($this->csvparams->user->email)) {
//                        $infojobs = new Infojobs($this->csvparams->user->name, $this->csvparams->user->first_name, $this->csvparams->user->last_name, $this->csvparams->user->email);
//                    }else{
//                        $infojobs = new \stdClass();
//                        $infojobs->results = 'No encontrado';
//                    }
//                    $libreborme = new Libreborme($this->csvparams->user->name, $this->csvparams->user->first_name, $this->csvparams->user->last_name, $id_check);
//                    $searx = new Searxme($this->csvparams->user->name, $this->csvparams->user->first_name, $this->csvparams->user->last_name, strtoupper($this->csvparams->user->cif_dni));
//                    $relat = new FindRelations($this->csvparams->user->name, $this->csvparams->user->first_name, $this->csvparams->user->last_name);
//                    $linkedin->start() && $linkedin->join();
//                    if(!empty($this->csvparams->user->email)) {
//
//                        $infojobs->start() && $infojobs->join();
//                    }
//                    $searx->start() && $searx->join();
//                    $libreborme->start() && $libreborme->join();
//                    $einforma = new \stdClass();
//                    $curl = curl_init();
//                    curl_setopt_array($curl, array(
//                        CURLOPT_URL => "https://www.einforma.com/servlet/app/screen/SProductoAJAX/prod/LOGIN_XML/",
//                        CURLOPT_RETURNTRANSFER => true,
//                        CURLOPT_ENCODING => "",
//                        CURLOPT_MAXREDIRS => 10,
//                        CURLOPT_TIMEOUT => 30,
//                        CURLOPT_COOKIESESSION => true,
//                        CURLOPT_COOKIEJAR => 'C:/nginx/www/api/storage/logs/cookie.txt',
//                        CURLOPT_COOKIEFILE => 'C:/nginx/www/api/storage/logs/cookie.txt',
//                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//                        CURLOPT_CUSTOMREQUEST => "POST",
//                        CURLOPT_POSTFIELDS => "username=presidencia@vinoloa.com&password=00001962&recordar=false",
//                        CURLOPT_HTTPHEADER => array(
//                            "accept: application/xml, text/xml, */*; q=0.01",
//                            "accept-encoding: gzip, deflate, br",
//                            "accept-language: es,en-GB;q=0.8,en;q=0.6",
//                            "cache-control: no-cache",
//                            "connection: keep-alive",
//                            "content-type: application/x-www-form-urlencoded; charset=UTF-8",
//                            "origin: https://www.einforma.com",
//                            "referer: https://www.einforma.com/",
//                            "user-agent: Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36",
//                            "x-requested-with: XMLHttpRequest"
//                        ),
//                    ));
//                    $response = curl_exec($curl);
//                    curl_close($curl);
//                    if (isset($libreborme->results['borme']->persona) && is_array($libreborme->results['borme']->persona)) {
//                        foreach ($libreborme->results['borme']->persona as $per) {
//                            foreach ($per->companies as $comp) {
//                                sleep(1);
//                                $einforma_thread = new Einforma(trim($comp));
//                                $einforma->companies[] = array($comp => $einforma_thread->parseEinforma());
//                                unset($einforma_thread);
//                            }
//                        }
//                    } else {
//                        $einforma->companies = 'No se han encontrado datos';
//
//                    }
                        $datos = json_encode([
//                            'id_check' => $id_check,
                            'score' => $score,
//                            'social' => [
//                                $libreborme->results,
//                                $einforma->companies,
//                                ['uno' => $linkedin->results],
//                                ['dos' => $infojobs->results],
//                                ['tres' => $milanuncio->search()],
//                                ['publico' => $searx->results],
//
//                                'relations' => $relat->findRelations(),
//                                ],
                                'status' => 200]);


                        file_put_contents($this->csvparams->user->cif_dni . '-postgresql.json', $datos, FILE_APPEND);
                    }
                }
        echo 'FIN';
            }


//        }


        private
        function score()
        {
            $resp = AuthModel::wsdljoin($this->csvparams);
//            Log::debug($resp->val_astr_tgtercero->str_tgtercero[0]->ter_codigo);
            if (!empty($resp->val_astr_tgtercero->str_tgtercero[0]->ter_codigo)) {
                //calc Eval
//            Log::debug($resp->val_astr_tgtercero->str_tgtercero[0]->ter_codigo);
                return $this->calcEval($resp->val_astr_tgtercero->str_tgtercero[0]->ter_codigo);

            } else {

                return ['msg' => 'No se ha podido realizar la consulta', 'status' => 400];
            }
        }


        private
        function calcEval($client_id)
        {
            // Exactitud 1
            $params = $this->getAddressData();
            //Log::debug('PARAMS ' . print_r($params, TRUE));
            if (is_array($params)) {
                $params['zip_code'] = $this->csvparams->address->zip_code;
                $params['lp_type'] = $this->csvparams->address->type;

//                    $m2 = ScoreModel::getM2Level1or2($this->csvparams->address);
 // TODO FALSEADO
                $m2 = 102;
                    // NUEVO PARA CSV SI NO ENCONTRAMOS NIVEL 1, DAMOS NIVEL 2 Y SI TAMPOCO HAY NIVEL 2 VAMOS AL 3
                    if($m2 == 0 && $this->csvparams->address->level == 1){
                        $this->csvparams->address->level = 2;
                        $m2 = ScoreModel::getM2Level1or2($this->csvparams->address);
                    }

                    if($m2 == 0 && $this->csvparams->address->level == 2){
                        $params['id_com'] = AddressModel::getTownId(substr($this->csvparams->address->zip_code, 0, 2));
                        $params['radio'] = UtilClass::densityRadio(AddressModel::densityTown($this->csvparams->address->zip_code)->ref_as_tipo_municipio);
                        $d=ScoreModel::getM2forLevel3($params);
                        Log::debug(print_r($d,true));
                        $m2 = $d->ref_as_metros;
                        $this->csvparams->address->level = 3;

                    }


                //Equifax
                //TODO EQUIFAX AHORA MISMO FALSEADO
                $f_nac = str_replace('-', '', $client_id);
                $datos['scofecna'] = $f_nac;
                $datos['incorazo'] = '-';
                $datos['incotipv'] = $this->csvparams->address->road_type;
                $datos['inconumv'] = $this->csvparams->address->number;
                $datos['incoresv'] = trim($this->csvparams->address->floor . ' ' . $this->csvparams->address->door);
                $datos['incoprov'] = $this->csvparams->address->town;
                $datos['inconomv'] = $this->csvparams->address->street;
                $datos['scomunic'] = $this->csvparams->address->province;
                $datos['ident'] = $this->csvparams->user->cif_dni;
                $datos['codpos'] = $this->csvparams->address->zip_code;
                $datos['sconom'] = $this->csvparams->user->name;
                $datos['scoape1'] = $this->csvparams->user->first_name;
                $datos['scoape2'] = $this->csvparams->user->last_name;
                if (empty($datos['incoresv'])) {
                    $datos['incoresv'] = '-';
                }


                $str_tgclientesequifax = array(
                    'ter_codigo' => $client_id,
                    'cldel_codigo' => 1,
                    'cexq_id' => 0,
                    'ejercicio' => 0,
                    'ser_serie' => '',
                    'cex_numero' => 0,
                    'cexq_fechacons' => date('c'),
                    'cexq_e_ident' => 0,
                    'cexq_e_sconom' => 0,
                    'cexq_e_scoape1' => 0,
                    'cexq_e_scoape2' => 0,
                    'cexq_e_scofecna' => 0,
                    'cexq_e_codpos' => 0,
                    'cexq_e_incorazo' => 0,
                    'cexq_e_incotipv' => 0,
                    'cexq_e_inconumv' => 0,
                    'cexq_e_incoresv' => 0,
                    'cexq_e_incoprov' => 0,
                    'cexq_e_inconomv' => 0,
                    'cexq_e_scomunic' => 0,
                    'cexq_s_incide' => '--', //(string) $res_equifax['INCIDE'],
                    'cexq_s_presenci' => '01',
                    'cexq_s_notaadm' => 0,
                    'cexq_s_sevnumop' => 0,
                    'cexq_s_sevnum02' => 0,
                    'cexq_s_sevnum06' => 0,
                    'cexq_s_sevnum07' => 0,
                    'cexq_s_sevnum09' => 0,
                    'cexq_s_sevnum14' => 0,
                    'cexq_s_sevnumre' => 0,
                    'cexq_s_sevimtot' => 0,
                    'cexq_s_seviment' => 0,
                    'cexq_s_sevimres' => 0,
                    'cexq_s_sevimp02' => 0,
                    'cexq_s_sevimp06' => 0,
                    'cexq_s_sevimp07' => 0,
                    'cexq_s_sevimp09' => 0,
                    'cexq_s_sevimp14' => 0,
                    'cexq_s_sevimprp' => 0,
                    'cexq_s_sevpeori' => 0,
                    'cexq_s_sevpeors' => 0,
                    'cexq_s_sevnumdi' => 0,
                    'cexq_s_sevnumac' => 0,
                    'cexq_s_sevnumdm' => 0,
                    'cexq_s_preseniv' => 000, //presencia en id verifier 001 confirmado y 000 no confirmado
                    'cexq_s_vfletpre' => 100, //porcentaje de similitud del registro verificado
                    'cexq_s_increnmn' => 0,
                    'cexq_s_increnmx' => 0,
                    'cexq_s_increnmd' => 0
                );

                ScoreModel::wsdlsetEquifaxWS($client_id, $str_tgclientesequifax);


                $params['client_id'] = $client_id;
                $params['m2'] = $m2;
                $resp = ScoreModel::calculateScore($params);
                if (isset($resp->ref_as_mensaje)) {
                    Log::debug(print_r($resp->ref_as_mensaje, true));
                }
                if (isset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval)) {
                    $status = 200;
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->asnef);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevnumop);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevnum02);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevnum06);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevnum07);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevnum09);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevnum14);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevnumre);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevimtot);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevniment);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevimp02);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevimp06);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevimp07);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevimp09);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevimp14);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevimprp);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevpeori);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevpeors);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevnumdi);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevnumac);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevnumdm);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->income);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ref_catastral);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ter_codigo);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->coeficiente);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->renta_minima);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->renta_mensual);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->renta_media);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->rating_eq);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->cliente);
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual = new \stdClass();
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->alimentacion=$resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo1 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_individual;
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->alcohol_tabaco=$resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo2 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_individual;
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->ropa_calzado=$resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo3 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_individual;
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->vivienda_suministros=$resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo4 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_individual;
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->mobiliario =$resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo5 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_individual;
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->salud =$resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo6 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_individual;
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->transporte =$resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo7 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_individual;
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->comunicaciones=$resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo8 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_individual;
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->ocio = $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo9 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_individual;
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->educacion =$resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo10 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_individual;
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->restaurantes=$resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo11 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_individual;
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_individual->otros=$resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo12 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_individual;

                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar= new \stdClass();
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->alimentacion=$resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo1 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_hogar;
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->alcohol_tabaco=$resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo2 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_hogar;
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->ropa_calzado=$resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo3 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_hogar;
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->vivienda_suministros=$resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo4 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_hogar;
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->mobiliario =$resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo5 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_hogar;
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->salud =$resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo6 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_hogar;
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->transporte =$resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo7 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_hogar;
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->comunicaciones=$resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo8 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_hogar;
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->ocio = $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo9 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_hogar;
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->educacion =$resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo10 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_hogar;
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->restaurantes=$resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo11 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_hogar;
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo_gastos_hogar->otros=$resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo12 * $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->ingreso_mensual_hogar;


                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->nivel1);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->nivel2);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->nivel3);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo1nac);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo2nac);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo3nac);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo4nac);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo5nac);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo6nac);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo7nac);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo8nac);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo9nac);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo10nac);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo11nac);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo12nac);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo1);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo2);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo3);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo4);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo5);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo6);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo7);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo8);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo9);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo10);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo11);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->grupo12);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->propia);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->alquilada);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->cedida);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->otra);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->fecha);
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->periodo_medio_pago= $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->plazo_gen_renta;
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->plazo_gen_renta);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->renta);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->equivalencia_asnef);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->tasa_nac_esfuerzo);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->tasa_paro);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->seviment);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->sevimres);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->comunidad);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->paro_hombre_total);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->paro_mujer_total);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->paro_indust_porcent);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->paro_agric_porcent);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->paro_const_porcent);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->paro_serv_porcent);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->saldo_trabajadores);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->morosidad_media);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->morosidad_nacional);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->compra_tot);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->compra_pagos);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->propia_edad);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->pagos_edad);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->herencia_edad);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->alquilada_edad);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->cedida_edad);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->otra_edad);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->tasaesfuerzo_ccaa);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->tasaesfuerzo_nacional);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->fortuito);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->incompetente);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->negligente);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->recurrente);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->intencional);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->id_comunidad);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->umbral_pobreza_ind);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->porcent_umbral_pobreza);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->porcent_umbral_pobreza_ind);
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->tasa_esfuerzo = $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->tasa_esfuerzo_hogar;
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->tasa_esfuerzo_hogar);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->importe_financiable_bowbuy);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->porcentaje_financiable_bowbuy);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->evolucion_patrimonial);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->porcent_agricultura);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->porcent_industria);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->porcent_construccion);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->porcent_servicios);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->hipoteca);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->coche);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->credito365);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->total_nacional);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->posicion_personal_nacional);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->total_comunidad);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->total_ciudad);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->posicion_personal_comunidad);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->posicion_personal_ciudad);
                    unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->renta_libre);


                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->nivel_estudios = '';
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->tenencia = '';
                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->tipo_moroso = '';

                    $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->nivel_exactitud = $this->csvparams->address->level;

//                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->tasa_nac_esfuerzo);
//                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->renta_media);
//                unset($resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0]->renta_minima);

                    $resp = $resp->ref_as_tgmiratingeval->srt_tgmiratingeval[0];
                } else {
                    $resp = 'Ha ocurrido un error intentelo más tarde';
                    $status = '400';
                }
//            return response()->json(['score' => ScoreModel::calculateScore($params), 'status' => 200]);
            } else {
                $resp = 'Direccion inexistente';
                $status = '400';
//            return response()->json(['error' => 'Direccion inexistente', 'status' => 400]);
            }

            return ['score' => $resp, 'status' => $status];

        }


        private
        function getAddressData()
        {
            $direccion = $this->csvparams->address->road_type . ' ' . $this->csvparams->address->street . ' ,' . $this->csvparams->address->number . ', ';
            $pos = strpos($this->csvparams->address->town, ',');
            if ($pos !== false) {
                $nombre_pob = substr($this->csvparams->address->town, 0, $pos);
                $pronombre_pob = substr($this->csvparams->address->town, $pos + 1);
                $poblacion = $pronombre_pob . ' ' . $nombre_pob;
                $direccion .= $poblacion;
            } else {
                $direccion .= $this->csvparams->address->town;
            }
            $lat_long = $this->cartoCiudadLatLong($direccion);
            //GOOGLE CORTE
            if (!$lat_long) {
                $direccion = $this->csvparams->address->road_type . ' ' . $this->csvparams->address->street . ' ,' . $this->csvparams->address->number . ', ' . $this->csvparams->address->zip_code . ' ' . $this->csvparams->address->town;
                $lat_long = $this->googleMapsLatLong($direccion);
            }
            //No existe direccion
            if (!$lat_long) {
                return false;
            }

            // VECINDARIO GOOGLE
            $nbhood = $this->checkNeighborhood($this->csvparams->address->zip_code, $lat_long['lat'], $lat_long['long']);

            return array(
                'latitude' => $lat_long['lat'],
                'longitude' => $lat_long['long'],
                'nbhood' => $nbhood
            );

        }


        private
        function curl($page)
        {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $page);
            curl_setopt($ch, CURLOPT_VERBOSE, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
            $result = curl_exec($ch);
            curl_close($ch);

            return $result;
        }


        private
        function googleMapsLatLong($direccion)
        {
            $resultado = json_decode(file_get_contents(sprintf('https://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=%s', urlencode($direccion))));

            $estado = $resultado->status;
            if ($estado == 'OK') {
                $lat = $resultado->results[0]->geometry->location->lat;
                $long = $resultado->results[0]->geometry->location->lng;

                return array('lat' => $lat, 'long' => $long);
            } else {
                return false;
            }
        }

        private
        function cartoCiudadLatLong($address)
        {
            $address = str_replace(' ', '%20', $address);
            $page = 'http://www.cartociudad.es/CartoGeocoder/Geocode?address=' . $address;
            $json = $this->curl($page);
            //$var = utf8_decode($json);
            $var = json_decode($json);
            if (isset($var->success) && $var->success != false) {
                if ($var->result[0]->status == 1) {
                    return array('lat' => $var->result[0]->latitude, 'long' => $var->result[0]->longitude);

                } else if ($var->result[0]->status == 2) {
                    return array('lat' => $var->result[0]->latitude, 'long' => $var->result[0]->longitude);
                } else {
                    return false;
                }
            } else {
                return false;
            }

        }

        private
        function googleMapsNeighborhood($lat, $long)
        {
            $resultnbhood = json_decode(file_get_contents(sprintf('https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $lat . ',' . $long . '&result_type=neighborhood&key=AIzaSyD0O7jxiQQ7b2XHShygw-eFvEXMPvqfFPo')));
            $estado = $resultnbhood->status;

            if ($estado == 'OK') {
                for ($i = 0; $i < count($resultnbhood->results); $i++) {
                    if ($resultnbhood->results[$i]->address_components[0]->types[0] == 'neighborhood') {
                        //Erro utf8_decode utf8_decode($resultnbhood->results[$i]->address_components[0]->long_name)
                        return $resultnbhood->results[$i]->address_components[0]->long_name;
                    }
                }
            }
        }

        private
        function checkNeighborhood($cp, $lat, $long)
        {
            $cp_mad = array(
                "28001",
                "28002",
                "28003",
                "28004",
                "28005",
                "28006",
                "28007",
                "28008",
                "28009",
                "28010",
                "28011",
                "28012",
                "28013",
                "28014",
                "28015",
                "28016",
                "28017",
                "28018",
                "28019",
                "28020",
                "28021",
                "28022",
                "28023",
                "28024",
                "28025",
                "28026",
                "28027",
                "28028",
                "28029",
                "28030",
                "28031",
                "28032",
                "28033",
                "28034",
                "28035",
                "28036",
                "28037",
                "28038",
                "28039",
                "28040",
                "28041",
                "28042",
                "28043",
                "28044",
                "28045",
                "28046",
                "28047",
                "28048",
                "28049",
                "28050",
                "28051",
                "28052",
                "28053",
                "28054",
                "28055",
                "28056",
                "28057",
                "28058",
                "28059",
                "28060"
            );
            $cp_bcn = array(
                "08001",
                "08002",
                "08003",
                "08004",
                "08005",
                "08006",
                "08007",
                "08008",
                "08009",
                "08010",
                "08011",
                "08012",
                "08013",
                "08014",
                "08015",
                "08016",
                "08017",
                "08018",
                "08019",
                "08020",
                "08021",
                "08022",
                "08023",
                "08024",
                "08025",
                "08026",
                "08027",
                "08028",
                "08029",
                "08030",
                "08031",
                "08032",
                "08033",
                "08034",
                "08035",
                "08036",
                "08037",
                "08038",
                "08039",
                "08040",
                "08041",
                "08042",
                "08043",
                "08044",
                "08045",
                "08046",
                "08047",
                "08048",
                "08049",
                "08050",
                "08051",
                "08052",
                "08053",
                "08054",
                "08055",
                "08056",
                "08057",
                "08058",
                "08059",
                "08060"
            );
            $nbhood = '';
            if (in_array($cp, $cp_mad) || in_array($cp, $cp_bcn)) {
                $nbhood = $this->googleMapsNeighborhood($lat, $long);
            } else {
                $cp_m = substr($cp, 0, 2);
                if ($cp_m == 28) {
                    $nbhood = 'Total Madrid';
                }
                if ($cp_bcn == '08') {
                    $nbhood = 'Total Barcelona';
                }
            }
            unset($cp_mad);
            unset($cp_bcn);

            return $nbhood;
        }

        private
        function getRoadType($rt)
        {
            if (strpos($rt, '.')) {
                $rt = str_replace('.', '', $rt);
            }

            if (strlen($rt) < 2) {
                //Error ni via ni ostias
                return false;
            } else if (strlen($rt) < 3) {
                $busqueda = array('alias', 'text');
            } else {
                $busqueda = array('text', 'alias');
            }

            $client = new \Elastica\Client(array(
                'host' => 'localhost',
                'port' => 9200
            ));

            $cont = 0;
            do {
                $query = [
                    'query' => [
                        'bool' => [
                            'must' => [
                                'match' => [
                                    $busqueda[$cont] => [
                                        "fuzziness" => "AUTO",
                                        "operator" => "and",
                                        "query" => $rt,
                                    ],
                                ],
                            ],
                        ]
                    ]
                ];
                //   Log::debug('query '.print_r($query,true));

                $query = \Elastica\Query::create($query);
                $search = new \Elastica\Search($client);
                $search->addType('roadtype')->addIndex('towns');
                $arr = $search->search($query)->getResults();
                $cont++;
            } while (count($arr) == 0 && $cont < 2);

            if (count($arr) == 0 && $cont < 3) {
                return false;
            } else {

                return $arr[0]->getData()['alias'];
            }
        }


    }