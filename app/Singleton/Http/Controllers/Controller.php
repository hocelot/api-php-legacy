<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use Log;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;


class Controller extends BaseController {

    protected $request_params, $client;
    protected $ajax = false;
    protected $error__params = false;
    protected $params_alvaro;
    protected $url;

    public function __construct(Request $request)
    {
        $this->url = $request->url();
        $this->error__params = false;
        $this->client['ip'] = $request->ip();
//        Log::debug(print_r($request->all(),true));
//    if($this->client['ip'] === '192.')
        if ($request->has('params'))
        {
            $this->request_params = json_decode(json_encode($request->input('params'), false));
//        $this->request_params = json_decode($request->input('params'));


//            Log::debug(print_r($this->request_params, true));
            if ($request->ajax() && $request->has('_token'))
            {
                $this->client['id'] = '1';
                $this->ajax = true;
            }
            else
            {
                $this->client['oauth_id'] = Authorizer::getResourceOwnerId();
                $this->client['id'] = app('db')->connection('oauth')->table('oauth_clients')->select('ter_codigo')->where('id', $this->client['oauth_id'])->first()->ter_codigo;

                if (isset($this->request_params->user->from) && $this->request_params->user->from == 'W')
                {
                    // Via web oauth_id propio
                    $this->client['ip'] = $this->request_params->user->ip;
                    $this->client['id'] = $this->request_params->user->client_id;
                    $this->client['oauth_id'] = app('db')->connection('oauth')->table('oauth_clients')->select('id')->where('ter_codigo', $this->client['id'])->first()->id;

                }

            }
        }else $this->error__params = true;
    }

}
