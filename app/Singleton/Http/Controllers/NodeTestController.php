<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 14/12/16
 * Time: 8:23
 */

namespace App\Http\Controllers;

use App\Http\NodeModel\Province;
use Laravel\Lumen\Routing\Controller as BaseController;

class NodeTestController extends BaseController
{
    public function meterProvincia(){
        if (($archivo = fopen('/var/www/api/app/Http/Controllers/provincias.csv', 'r')) !== FALSE) {
            while (($dato = fgetcsv($archivo, 1000, ';')) !== FALSE) {
                $post = new Province(['id' => $dato[0], 'name' => $dato[1]]);
                $post->save();

            }

        }
    }

}