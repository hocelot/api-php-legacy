<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 30/11/16
 * Time: 10:03
 */

namespace App\Http\Controllers\IdFraud;
use App\Http\Controllers\SocialMedia\Facebook;
use Thread;
use Log;

class TestThreads extends Thread
{
    private $email, $full_name, $phone;
    private $facebook;
    public function __construct($email, $phone, $name, $primer_apellido, $segundo_apellido)
    {
        $this->full_name = $name . ' ' . $primer_apellido . ' ' . $segundo_apellido;
        $this->email = $email;
        $this->phone = $phone;
    }
    public function run(){
        require_once(__DIR__.'/../../../../vendor/autoload.php');
        require_once(__DIR__.'/../../../../bootstrap/app.php');
        Log::debug('FUNCIONA');
        $this->worker->addData('id_check',$this->fraud_id());
        $this->worker->addData('fb',$this->facebook);
    }


    public function fraud_id()
    {
//        Log::debug('Aqui en el id fraud');
//        return 'OK';
        if (EmailCheck::verify($this->email)) {
            $facebook = new Facebook($this->email, $this->phone);
            $this->facebook = $facebook->search();
            if (!$this->facebook) {
                $id_fraud['email'] = ['estado' => 'OK', 'alias' => 'No encontrado', 'score' => 2];
            } else {
                $cmp_name = mb_strtoupper($this->full_name, 'UTF-8');
                similar_text(mb_strtoupper($this->facebook->name, 'UTF-8'), $cmp_name, $percent);
                $corr = round($percent, PHP_ROUND_HALF_EVEN);
                if ($corr < 40.0)
                    $id_fraud['email'] = ['estado' => 'Existe email', 'alias' => $this->facebook->alias, 'correlacion' => $corr . '%', 'score' => 2];
                else
                    $id_fraud['email'] = ['estado' => 'Existe email', 'alias' => $this->facebook->alias, 'correlacion' => $corr . '%', 'score' => 1];
            }
        } else {
            $id_fraud['email'] = ['estado' => 'KO', 'alias' => 'No encontrado', 'score' => 0];
        }
        if (is_array($id_fraud['email']['alias'])) {
            $alias = '';
            $c_alias = true;
            foreach ($id_fraud['email']['alias'] as $nick) {
                if ($c_alias) {
                    $alias .= $nick;
                    $c_alias = false;
                } else {
                    $alias .= ',' . $nick;
                }
            }
        } else {
            $alias = '';
        }
        // Guardamos string
        $id_fraud['email']['alias'] = $alias;

        $phone = PhoneCheck::verify($this->phone);
        if ($phone) {
            $id_fraud['phone'] = $phone;
        } else {
            $id_fraud['phone'] = "No existe";
        }

        return ['hits' => $id_fraud, 'status' => 200];
    }

}