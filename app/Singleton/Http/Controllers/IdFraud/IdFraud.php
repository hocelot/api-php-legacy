<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 4/11/16
 * Time: 9:57
 */

namespace App\Http\Controllers\IdFraud;

//use Thread;
use App\Http\Controllers\Worker\ThreadsManager;
use Log;

class IdFraud extends \Threaded
{
    private $email, $name, $first, $last, $phone;

    public function __construct($email, $phone, $name, $primer_apellido, $segundo_apellido)
    {
        $this->name = $name;
        $this->first = $primer_apellido;
        $this->last = $segundo_apellido;
//        $this->full_name = $name . ' ' . $primer_apellido . ' ' . $segundo_apellido;
        $this->email = $email;
        $this->phone = $phone;
    }

    public function run()
    {
        require_once(__DIR__ . '/../../../../vendor/autoload.php');
        require_once(__DIR__ . '/../../../../bootstrap/app.php');
        $this->worker->addData('id_fraud', $this->fraud_id());
//        $this->worker->addData('fb', $this->facebook);
    }

    private function fraud_id()
    {
        $full_name = $this->name . ' ' . $this->first . ' ' . $this->last;
        if(empty($this->phone) && empty($this->email)){
            return ['fb_email' => ['email'=>'','phone'=>'','img'=>'','name'=>''], 'fb_phone' => ['email'=>'','phone'=>'','img'=>'','name'=>''], 'hits' => ['score' => 0, 'phone_info'=>''], 'status' => 200];
        }else {
            $fb_obj = new FacebookCheck($this->email, $this->phone, mb_strtoupper($full_name, 'UTF-8'));
            $worker = new ThreadsManager();
            $email = new EmailCheck($this->email, $this->phone, $fb_obj);
            $worker->stack($email);

            $phone = new PhoneCheck($this->phone, $fb_obj);
            $worker->stack($phone);

//        $carrot = new Carrot($this->dni,$this->name,$this->first,$this->last);
//        $worker->stack($carrot);
            $worker->start();
            $worker->shutdown();
            Log::debug(print_r($worker->data['email'], true));
            Log::debug(print_r($worker->data['phone'], true));

            if ($worker->data['email']['hits']['score'] > $worker->data['phone']['hits']['score']) {
                if ($worker->data['phone']['hits']['score'] != 0)
                    $id_fraud = $worker->data['email']['hits']['score'] + 1;
                else $id_fraud = $worker->data['email']['hits']['score'];
            } elseif ($worker->data['email']['hits']['score'] < $worker->data['phone']['hits']['score']) {
                if ($worker->data['email']['hits']['score'] != 0)
                    $id_fraud = $worker->data['phone']['hits']['score'] + 1;
                else $id_fraud = $worker->data['phone']['hits']['score'];
            } elseif ($worker->data['email']['hits']['score'] == 0 && $worker->data['phone']['hits']['score'] == 0) {
                $id_fraud = 0;
            } elseif ($worker->data['email']['hits']['score'] == $worker->data['phone']['hits']['score']) {
                $id_fraud = $worker->data['email']['hits']['score'] + 1;
            }
            return ['fb_email' => $worker->data['email']['hits']['fb'], 'fb_phone' => $worker->data['phone']['hits']['fb'], 'hits' => ['score' => $id_fraud, 'phone_info'=>$worker->data['phone']['hits']['phone_info']], 'status' => 200];

        }

//        foreach ($worker->data as $key => $value){
//            //if($key == 'fb') $this->facebook = $value;
//            //else
//                $id_fraud[$key]=$value;
//        }

    }
}