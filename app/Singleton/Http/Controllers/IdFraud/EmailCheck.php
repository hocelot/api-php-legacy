<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 24/10/16
 * Time: 21:42
 */

namespace App\Http\Controllers\IdFraud;

use App\Http\Controllers\SocialMedia\Facebook;
use Goutte\Client;
use Log;

class EmailCheck extends \Threaded
{
    private $email, $phone, $facebook;

    public function __construct($email, $phone, FacebookCheck $fb)
    {
        $this->email = $email;
        $this->phone = $phone;
        $this->facebook = $fb;
    }

    public function run()
    {
        require_once(__DIR__ . '/../../../../vendor/autoload.php');
        require_once(__DIR__ . '/../../../../bootstrap/app.php');
        if ($this->verify()) {
            $id_fraud = ['hits' => $this->facebook->searchByEmail()];
        } else {
            $id_fraud = ['hits' => ['fb' => ['email' => '', 'phone' => '', 'img' => '', 'name' => ''], 'score' => 0]];
        }

        $this->worker->addData('email', $id_fraud);
        //$this->worker->addData('fb',$this->facebook);

    }

    private function verify()
    {
        //Falseado para pueba
        // return true;
        if(empty($this->email))return false;
        //Sintaxis correcta? Cumple los estándares?
        if (!$this->isvalid()) return false;

        //Cuenta pass 123456789Aa
        $fromemail = 'khfgydfdfdfdsafdsadadasddsfsdfsdf@hotmail.com';

        $email_arr = explode("@", $this->email);
        $domain = array_slice($email_arr, -1);
        $domain = $domain[0];

        // Trim [ and ] from beginning and end of domain string, respectively
        $domain = ltrim($domain, "[");
        $domain = rtrim($domain, "]");

        if ("IPv6:" == substr($domain, 0, strlen("IPv6:"))) {
            $domain = substr($domain, strlen("IPv6") + 1);
        }

        $mxhosts = array();
        if (filter_var($domain, FILTER_VALIDATE_IP))
            $mx_ip = $domain;
        else
            getmxrr($domain, $mxhosts, $mxweight);

        if (!empty($mxhosts))
            $mx_ip = $mxhosts[array_search(min($mxweight), $mxhosts)];
        else {
            if (filter_var($domain, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
                $record_a = dns_get_record($domain, DNS_A);
            } elseif (filter_var($domain, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
                $record_a = dns_get_record($domain, DNS_AAAA);
            }

            if (!empty($record_a))
                $mx_ip = $record_a[0]['ip'];
            else {
                return false;
            }
        }

        $connect = @fsockopen($mx_ip, 25);
        if ($connect) {
            if (preg_match("/^220/i", $out = fgets($connect, 1024))) {
                fputs($connect, "HELO $mx_ip\r\n");
                $out = fgets($connect, 1024);

                fputs($connect, "MAIL FROM: <$fromemail>\r\n");
                $from = fgets($connect, 1024);

                fputs($connect, "RCPT TO: <$this->email>\r\n");
                $to = fgets($connect, 1024);

                fputs($connect, "QUIT");
                fclose($connect);

                if (!preg_match("/^250/i", $from) || !preg_match("/^250/i", $to)) {
                    return false;
                } else {
                    return true;
                }
            }
        }
        return false;

    }

    private function isvalid()
    {
        return !!filter_var($this->email, FILTER_VALIDATE_EMAIL);
    }
}

//public function run()
//{
//    require_once(__DIR__ . '/../../../../vendor/autoload.php');
//    require_once(__DIR__ . '/../../../../bootstrap/app.php');
////        if ($this->verify()) {
////            $facebook = new Facebook($this->email, $this->phone);
////            $this->facebook = $facebook->search();
////            if (!$this->facebook) {
////                $id_fraud = ['estado' => 'OK', 'alias' => 'No encontrado', 'score' => 2];
////            } else {
////                similar_text(mb_strtoupper($this->facebook->name, 'UTF-8'), $this->full_name, $percent);
////                $corr = round($percent, PHP_ROUND_HALF_EVEN);
////                if ($corr < 50.0)
////                    $id_fraud = ['estado' => 'Existe email', 'alias' => $this->facebook->alias, 'correlacion' => $corr . '%', 'score' => 2];
////                else
////                    $id_fraud = ['estado' => 'Existe email', 'alias' => $this->facebook->alias, 'correlacion' => $corr . '%', 'score' => 1];
////            }
////        } else {
////            $id_fraud = ['estado' => 'KO', 'alias' => 'No encontrado', 'score' => 0];
////        }
////        if (is_array($id_fraud['alias'])) {
////            $alias = '';
////            $c_alias = true;
////            foreach ($id_fraud['alias'] as $nick) {
////                if ($c_alias) {
////                    $alias .= $nick;
////                    $c_alias = false;
////                } else {
////                    $alias .= ',' . $nick;
////                }
////            }
////            // Guardamos string
////            $id_fraud['alias'] = $alias;
////        }
//
//    $this->worker->addData('email_check',$this->verify());//$id_fraud);
//    //$this->worker->addData('fb',$this->facebook);
//
//}