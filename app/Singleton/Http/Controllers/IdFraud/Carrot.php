<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 21/12/16
 * Time: 11:52
 */

namespace App\Http\Controllers\IdFraud;

use Log;
use Symfony\Component\DomCrawler\Crawler;

class Carrot extends \Threaded
{
    private $dni, $name, $first_name, $last_name, $client;

    public function __construct($dni, $name, $first, $last)
    {
        $this->dni = $dni;
        $this->name = $name;
        $this->first_name = $first;
        $this->last_name = $last;
    }
    public function run(){
        require_once(__DIR__ . '/../../../../vendor/autoload.php');
        require_once(__DIR__ . '/../../../../bootstrap/app.php');
        $this->search();
        $this->worker->addData('carrot','falseado');

    }

    private function search()
    {
        $searxme = $this->searxme_search();
        $etools = $this->etools_search();

        if (!$searxme && !$etools) return false;
        if($searxme && $etools) {
            foreach ($searxme['hits'] as $hit) {
                if (!in_array($hit['url'], $etools['urls'])) {
                    $etools['hits'][] = $hit;
                }
            }
        }else if($searxme && !$etools){
            foreach ($searxme['hits'] as $hit) {
                    $etools['hits'][] = $hit;
            }
        }
        unset($etools['urls']);
        unset($searxme);
        // var_dump($etools);
        $xml = new \SimpleXMLElement('<?xml version=\'1.0\' encoding=\'utf-8\'?><searchresult/>');

//        $xml->addChild('query', '("' . $this->dni . '" AND "' . $this->name . '" AND "' . $this->first_name . '" AND "' . $this->last_name . '")');
        $xml->addChild('query', '' . $this->dni . ' ' . $this->name . ' ' . $this->first_name . ' ' . $this->last_name . '');

        foreach ($etools['hits'] as $hit) {
            $document = $xml->addChild('document');
            $document->addChild('title', htmlspecialchars($hit['title']));
            $document->addChild('snippet', htmlspecialchars($hit['text']));
            $document->addChild('url', htmlspecialchars($hit['url']));
        }

        $ch = curl_init();
//$fields['query']
        $fields['dcs.c2stream'] = $xml->asXML();
        curl_setopt_array($ch,
            array(
                CURLOPT_URL => "http://localhost:8080/dcs/rest",
                CURLOPT_POST => true,
                CURLOPT_HTTPHEADER => array('Content-Type: multipart/formdata'),
                CURLOPT_HEADER => false,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POSTFIELDS => $fields
            )
        );

        $result = curl_exec($ch);
        curl_close($ch);
        Log::debug('Carrot'.print_r($result,true));
        return true;
    }

    private function etools_search()
    {
//        $site = 'http://www.etools.ch';
//        $str_search = urlencode('("' . $this->dni . '" "' . $this->name . '" "' . $this->first_name . '" "' . $this->last_name . '")');
        $str_search = urlencode('"' . $this->dni .'"');

        // 9d9624eced4adaa97bd37e9b2991c304
        $search = 'http://www.etools.ch/partnerSearch.do?partner=TestAdvanced&query=' . $str_search . '&language=es&country=web&includeClusters=true';//country=web para todos //&maxRecords=200
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $search);
        $search = $response->getBody()->getContents();
        unset($client);
        $results = new \stdClass();
        $crawler = new Crawler();
        $crawler->addXmlContent($search);
        $crawler->filterXPath('//result/mergedRecords/record')
            ->each(function ($node) use ($results) {
                $aux['title'] = $node->filter('title')->text();
                $aux['text'] = $node->filter('text')->text();
                $aux['url'] = $node->filter('url')->text();
                $results->urls[] = $aux['url'];
                $results->hits[] = $aux;
            });
        if (isset($results->hits))
            return array('hits' => $results->hits, 'urls' => $results->urls);
        else return false;

    }

    private function searxme_search()
    {
//        $str_search = urlencode('("' . $this->dni . '" AND "' . $this->name . '" AND "' . $this->first_name . '" AND "' . $this->last_name . '")');
        $str_search = urlencode('"' . $this->dni .'"');

        //85.214.29.216/searx

        //tu instancia??? ricardomarcos.es/searx
        $search = 'http://ricardomarcos.es/searx/?q=' . $str_search . '&categories=general&format=json';
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $search);
        $search = json_decode($response->getBody()->getContents());
        unset($client);
        if (!empty($search->results)) {
            $res = new \stdClass();
            foreach ($search->results as $resultados) {
                $res->hits[] = array('title' => $resultados->title, 'text' => $resultados->content, 'url' => $resultados->url);
            }
            return array('hits' => $res->hits);
        } else {
            return false;
        }
    }
}