<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 20/12/16
 * Time: 11:12
 */

namespace App\Http\Controllers\IdFraud;


use App\Http\Controllers\Proxies\Proxies;
use Goutte\Client;
use Log;

class FacebookCheck
{
    private $mail, $number, $fullname;

    public function __construct($mail, $number, $full_name)
    {
        $this->mail = $mail;
        if (substr($number, 0, 2) == '34') {
            $number = substr($number, 2);
        } else if (substr($number, 0, 3) == '+34') {
            $number = substr($number, 3);
        }
        $this->number = '+34' . $number;
        $this->fullname = $full_name;
    }

    private function getData($search)
    {
        $client = new Client();
        $client->setHeader('User-Agent', "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36");
        $crawler = $client->request('GET', 'https://www.facebook.com/login/identify?ctx=recover');
        $var = strstr($crawler->html(), '["LSD",');
        $array = explode('"', $var);
        $lsd = $array[5];
        $var = strstr($crawler->html(), '["_js_datr",');
        $array = explode('"', $var);
        $datr = $array[3];
        unset($array);
        unset($client);
        unset($crawler);
        $obj_proxy = new Proxies('fb');
        $ip_real = false;
        Log::debug('BUSCO ' . $search);
        $count_crawler = 0;
//        do {
//            $repetir = false;
            $ip = $obj_proxy->get();
            $obj_proxy->setUsed($ip);
            $cmd = escapeshellcmd('/usr/bin/python /var/www/api/storage/logs/facebook.py ' . $lsd . ' ' . $datr . ' ' . $search . ' ' . $ip);
//            Log::debug('Comando '.$cmd);
//            $python = shell_exec($cmd);
            exec($cmd, $result);
            //Log::debug('EXEC FACEBOOK ' . print_r($result, true));
            //Separamos las cuentas encontradas
//            $result = preg_split("/ACCOUNT:/", $python, 0, PREG_SPLIT_NO_EMPTY);
//            Log::debug('RESULT FACEBOOK '.print_r($result));
            //Puede ser una cuenta o bien nada
            if (!isset($result[0])) {
                $obj_proxy->unlock(999);
            } elseif ($result[0] == 'NONE') {
                $obj_proxy->unlock(200);
//                break;
            }
            if(isset($result[0])) {
                $crawler = new \Symfony\Component\DomCrawler\Crawler();
                $crawler->addHtmlContent(json_decode($result[0]));
                if (!$ip_real) {
                    if ($crawler->filter('#captcha_persist_data')->count() < 1)
                        $obj_proxy->unlock(200);
                    else $obj_proxy->unlock(999);
                }
                if ($crawler->filter('#captcha_persist_data')->count() > 0) {
//                    $repetir = true;
//                    $count_crawler++;
//                    if ($count_crawler > 5) {
                        Log::debug('FALLO ID FRAUD EN ' . $search);
                        $result[0] = 'NONE';
//                        break;
//                    }
                }
            }
//            else $repetir = true;
//        } while ($repetir);
        if ($result[0] == 'NONE') return false;
        return $result;
    }

    public function searchByEmail()
    {
        $email = $phone = $img = $name = false;
        $score = 1;
        $results = $this->getData($this->mail);
        if (!$results) // No encontrado
            return ['fb' => ['email' => '', 'phone' => '', 'img' => '', 'name' => ''], 'score' => $score];
        $fb_email = array();
        $r = new \stdClass();
        foreach ($results as $result) {
            $crawler = new \Symfony\Component\DomCrawler\Crawler();
            $crawler->addHtmlContent(json_decode($result));
            if ($crawler->filter('label[for="send_email"] > div > div > div > div')->count() > 0) {
                $email = trim($crawler->filter('label[for="send_email"] > div > div > div > div')->text());
                if (stristr($email, "Email me a link to reset my password")) {
                    $email = trim(str_replace("Email me a link to reset my password", "", $email));
                }
                if (stristr($email, $this->mail)) {
                    $score += 2;
                }
            } else $email = 'none';

            //No es el email exacto
            if (trim($email) != $this->mail)
                return ['fb' => ['email' => '', 'phone' => '', 'img' => '', 'name' => ''], 'score' => 1];

            $r->email = $email;


            if ($crawler->filter('tr:last-of-type > td > div > label > div > div > div > div')->count() > 0) {
                $phone = trim($crawler->filter('tr:last-of-type > td > div > label > div > div > div > div')->text());
                if ($phone == $email || $phone == $this->mail) $phone = 'none';
                else {
                    if (stristr($phone, substr($this->number, -2))) {
                        $score += 2;
                    }
                }
            } else $phone = 'none';

            $r->phone = $phone;

            if ($crawler->filterXPath("//html/body/div/div[@id='globalContainer']/div[@id='content']/div/form/div/div[2]/table/tr/td[2]/div/div/div[1]/img")->count() > 0) {
                $img = $crawler->filterXPath("//html/body/div/div[@id='globalContainer']/div[@id='content']/div/form/div/div[2]/table/tr/td[2]/div/div/div[1]/img")->attr('src');
            } else $img = 'none';
            $r->img = $img;

            if ($crawler->filterXPath("//html/body/div/div[@id='globalContainer']/div[@id='content']/div/form/div/div[2]/table/tr/td[2]/div/div/div[2]/div")->count() > 0) {
                $name = trim($crawler->filterXPath("//html/body/div/div[@id='globalContainer']/div[@id='content']/div/form/div/div[2]/table/tr/td[2]/div/div/div[2]/div")->text());
                $name = trim(str_replace('Facebook User', '', trim($name)));
                if ($name == $email || $name == $this->mail) $name = 'none';
                else {
                    similar_text(mb_strtoupper($name, 'UTF-8'), $this->fullname, $percent);
                    if ($percent > 40.0) {
                        $score += 4;
                    }
                }
            } else $name = 'none';
            $r->name = $name;
            unset($crawler);
            $fb_email[] = $r;
        }
        $c = true;
        foreach ($fb_email as $fb) {
            if ($c) {
                $email = $fb->email;
                $name = $fb->name;
                $phone = $fb->phone;
                $img = $fb->img;
                $c = false;
            } else {
                $email .= ';' . $fb->email;
                $name .= ';' . $fb->name;
                $phone .= ';' . $fb->phone;
                $img .= ';' . $fb->img;
            }
        }

        return ['fb' => ['email' => $email, 'phone' => $phone, 'img' => $img, 'name' => $name], 'score' => $score];
    }

    public function searchByPhone()
    {
        $score = 1;
        $email = $phone = $img = $name = false;
        $results = $this->getData($this->number);
        if (!$results) // No encontrado
            return ['fb' => ['email' => '', 'phone' => '', 'img' => '', 'name' => ''], 'score' => $score];
        $fb_email = array();
        $r = new \stdClass();
        foreach ($results as $result) {
            $crawler = new \Symfony\Component\DomCrawler\Crawler();
            $crawler->addHtmlContent(json_decode($result));
            if ($crawler->filter('label[for="send_email"] > div > div > div > div')->count() > 0) {
                $email = trim($crawler->filter('label[for="send_email"] > div > div > div > div')->text());
                if (stristr($email, "Email me a link to reset my password")) {
                    $email = trim(str_replace("Email me a link to reset my password", "", $email));
                }
                if (stristr($email, substr($this->mail, -2))) {
                    $score += 2;
                }
            } else $email = 'none';
            $r->email = $email;


            if ($crawler->filter('tr:last-of-type > td > div > label > div > div > div > div')->count() > 0) {
                $phone = trim($crawler->filter('tr:last-of-type > td > div > label > div > div > div > div')->text());
                if (stristr($phone, $this->number)) {
                    $score += 2;
                }
            } else $phone = 'none';
            $r->phone = $phone;


            if ($crawler->filterXPath("//html/body/div/div[@id='globalContainer']/div[@id='content']/div/form/div/div[2]/table/tr/td[2]/div/div/div[1]/img")->count() > 0) {
                $img = $crawler->filterXPath("//html/body/div/div[@id='globalContainer']/div[@id='content']/div/form/div/div[2]/table/tr/td[2]/div/div/div[1]/img")->attr('src');
            } else $img = 'none';
            $r->img = $img;

            if ($crawler->filterXPath("//html/body/div/div[@id='globalContainer']/div[@id='content']/div/form/div/div[2]/table/tr/td[2]/div/div/div[2]/div")->count() > 0) {
                $name = trim($crawler->filterXPath("//html/body/div/div[@id='globalContainer']/div[@id='content']/div/form/div/div[2]/table/tr/td[2]/div/div/div[2]/div")->text());
                $name = trim(str_replace('Facebook User', '', trim($name)));
                if ($name == $phone || $name == $this->number) $name = 'none';
                else {
                    similar_text(mb_strtoupper($name, 'UTF-8'), $this->fullname, $percent);
                    if ($percent > 40.0) {
                        $score += 4;
                    }
                }
            } else $name = 'none';
            $r->name = $name;
            unset($crawler);
            $fb_email[] = $r;
        }
        $c = true;
        foreach ($fb_email as $fb) {
            if ($c) {
                $email = $fb->email;
                $name = $fb->name;
                $phone = $fb->phone;
                $img = $fb->img;
                $c = false;
            } else {
                $email .= ';' . $fb->email;
                $name .= ';' . $fb->name;
                $phone .= ';' . $fb->phone;
                $img .= ';' . $fb->img;
            }
        }
        Log::debug('SCORE ' . $score);

        return ['fb' => ['email' => $email, 'phone' => $phone, 'img' => $img, 'name' => $name], 'score' => $score];

    }

}