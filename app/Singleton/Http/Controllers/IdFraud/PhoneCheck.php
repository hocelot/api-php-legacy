<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 18/11/16
 * Time: 17:41
 */

namespace App\Http\Controllers\IdFraud;


use App\Http\Controllers\Proxies\Proxies;
use SebastianBergmann\CodeCoverage\Report\PHP;
use Symfony\Component\DomCrawler\Crawler;
use Log;

class PhoneCheck extends \Threaded
{
    private $phone, $facebook;

    public function __construct($phone, FacebookCheck $fb)
    {
        $this->phone = $phone;
        $this->facebook = $fb;
    }

    public function run()
    {
        require_once(__DIR__ . '/../../../../vendor/autoload.php');
        require_once(__DIR__ . '/../../../../bootstrap/app.php');
        $phone_check = $this->verify();
//        if (is_array($phone_check)) {
        if ($phone_check) {
            $result = $this->facebook->searchByPhone();
            $result['phone_info'] = $phone_check;
//            $result['phone_info'] = 'Re-Falseado';

            $id_fraud = ['hits' => $result];
        } else {
            $id_fraud = ['hits' => ['fb' => ['email' => '', 'phone' => '', 'img' => '', 'name' => ''], 'phone_info' => 'No existe el telefono introducido', 'score' => 0]];
        }
        $this->worker->addData('phone', $id_fraud);
    }

    private function verify()
    {
//        return true;
        if(empty($this->phone)) return false;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://numclass-api.nubefone.com/v2/numbers/' . $this->phone,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "x-api-key:6944Z4jD7vFZjAnJGzmE",
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
//        echo $response;

        $var = json_decode($response);
//        file_put_contents('/var/www/api/storage/logs/peoplecall.csv',$response.'---'.PHP_EOL,FILE_APPEND);
        if (!isset($var->typeDescription)) {
            return false;
        } else {
            unset($var->queriesLeft);
            return $var;
        }

    }

}