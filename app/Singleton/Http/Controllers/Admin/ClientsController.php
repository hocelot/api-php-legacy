<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Models\Admin\ClientsModel;
use App\Http\Models\AuthModel;
use App\Http\Models\UtilClass;
use Illuminate\Http\Request;
use stdClass;
use Log;
use Symfony\Component\CssSelector\Tests\Parser\ReaderTest;

/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 30/07/16
 * Time: 9:06
 */
class ClientsController extends Controller
{
    public function getClients(Request $request){
        $params = array(
            'rowCount'=>$request->input('rowCount'),
            'sort'=>$request->input('sort'),
            'current'=>$request->input('current'),
        );
        //Log::debug(print_r(ClientsModel::getClients($params), true));
        return response()->json(ClientsModel::getClients($params));
    }
    public function viewClient(Request $request){
        $response = clientsModel::viewClient($request->input('params'));
        $permisos = '';
        foreach ($response->clientPerm as $perm){
           // Log::debug(print_r($perm, true));
            if($perm->scope_id == 'mp_HPDMhyzXlvGZoOUTXkmCZjIAXALILrKW71AD'){
                //ADDRESS
                $permisos.=' Address';
            }elseif ($perm->scope_id == 'sc_IpqAf3mZZVVnUcMW56PBDfndae94DTIQOPbY'){
                $permisos.=' RiskScore';
            }elseif ($perm->scope_id == 'ic_Tl3h7b3HsEeAe2b0DtW8zmpQ9EnBc9XjCsM7'){
                $permisos.=' IdCheck';
            }elseif ($perm->scope_id == 'if_Fcz72MajCgEVDnf0s2ct8M52RffsQlI1tZM6'){
                $permisos.=' IdFraud';
            }elseif ($perm->scope_id == 'gc_pzP9O2TW9eOiCXn4Mo0gVUroCpg1wA5EuWWQ'){
                $permisos.=' GeoCheck';
            }elseif ($perm->scope_id == 'gf_fjw6HcqXaXcJHkRUNSe2xbMygiiCbwkBltJ2'){
                $permisos.=' GeoFraud';
            }
        }
        unset($response->clientPerm);
        $response->clientPerm = $permisos;
       // Log::debug(print_r($response, true));
        return response()->json($response);
    }
    public function delClient(Request $request){
        $input = $request->input('params');
        $response = clientsModel::delClient($request->input('params'));
        if($response){
            return response()->json(array('msg' => 'Cliente con tercod: '.$input['id'].' eliminado correctamente de la aplicación.', 'status' => 200));
        }else{
            return response()->json(array('msg' => 'Error al eliminar el cliente con tercod: '.$input['id'].'.', 'status' => 400));
        }
    }
    public function editClient(Request $request){
        $input = $request->input('params');
        $change = clientsModel::viewClient($request->input('params'));
        Log::debug(print_r($input, true));
        Log::debug(print_r($change, true));
        $newParams = new stdClass();
        $newParams->scopeDel = array();
        $newParams->scopeNew = array();
        foreach ($input['clientPerm'] as $prod){
            if($prod == 'IdCheck'){
                $scopesOld[] = 'ic_Tl3h7b3HsEeAe2b0DtW8zmpQ9EnBc9XjCsM7';
            }
            if($prod == 'IdFraud'){
                $scopesOld[] = 'if_Fcz72MajCgEVDnf0s2ct8M52RffsQlI1tZM6';
            }
            if($prod == 'GeoCheck'){
                $scopesOld[] = 'gc_pzP9O2TW9eOiCXn4Mo0gVUroCpg1wA5EuWWQ';
            }
            if($prod == 'GeoFraud'){
                $scopesOld[] = 'gf_fjw6HcqXaXcJHkRUNSe2xbMygiiCbwkBltJ2';
            }
            if($prod == 'RiskScore'){
                $scopesOld[] = 'sc_IpqAf3mZZVVnUcMW56PBDfndae94DTIQOPbY';
            }
            if($prod == 'Address'){
                $scopesOld[] = 'mp_HPDMhyzXlvGZoOUTXkmCZjIAXALILrKW71AD';
            }
        }

//        foreach ($change->clientPerm as $perm){
//            if($perm->scope_id == 'mp_HPDMhyzXlvGZoOUTXkmCZjIAXALILrKW71AD' && $scopesOld['add'] = 0){
//            //BORRAMOS DE BD, NO ESTA EN LOS NUEVOS PERMISOS Y ANTES SI
//                $newParams->scopeDel[] = 'mp_HPDMhyzXlvGZoOUTXkmCZjIAXALILrKW71AD';
//            }elseif ($perm->scope_id != 'mp_HPDMhyzXlvGZoOUTXkmCZjIAXALILrKW71AD' && $scopesOld['add'] = 1){
//            //NO ESTABA ANTES Y AHORA SI, METEMOS EN LA BD
//                $newParams->scopeNew[] = 'mp_HPDMhyzXlvGZoOUTXkmCZjIAXALILrKW71AD';
//            }
//            if($perm->scope_id == 'sc_IpqAf3mZZVVnUcMW56PBDfndae94DTIQOPbY' && $scopesOld['sc'] = 0){
//                $newParams->scopeDel[] = 'sc_IpqAf3mZZVVnUcMW56PBDfndae94DTIQOPbY';
//            }elseif ($perm->scope_id != 'sc_IpqAf3mZZVVnUcMW56PBDfndae94DTIQOPbY' && $scopesOld['sc'] = 1){
//                $newParams->scopeNew[] = 'sc_IpqAf3mZZVVnUcMW56PBDfndae94DTIQOPbY';
//            }
//            if($perm->scope_id == 'ic_Tl3h7b3HsEeAe2b0DtW8zmpQ9EnBc9XjCsM7' && $scopesOld['ic'] = 0){
//                $newParams->scopeDel[] = 'ic_Tl3h7b3HsEeAe2b0DtW8zmpQ9EnBc9XjCsM7';
//            }elseif ($perm->scope_id != 'ic_Tl3h7b3HsEeAe2b0DtW8zmpQ9EnBc9XjCsM7' && $scopesOld['ic'] = 1){
//                $newParams->scopeNew[] = 'ic_Tl3h7b3HsEeAe2b0DtW8zmpQ9EnBc9XjCsM7';
//            }
//            if($perm->scope_id == 'if_Fcz72MajCgEVDnf0s2ct8M52RffsQlI1tZM6' && $scopesOld['if'] = 0){
//                $newParams->scopeDel[] = 'if_Fcz72MajCgEVDnf0s2ct8M52RffsQlI1tZM6';
//            }elseif ($perm->scope_id != 'if_Fcz72MajCgEVDnf0s2ct8M52RffsQlI1tZM6' && $scopesOld['if'] = 1){
//                $newParams->scopeNew[] = 'if_Fcz72MajCgEVDnf0s2ct8M52RffsQlI1tZM6';
//            }
//            if($perm->scope_id == 'gc_pzP9O2TW9eOiCXn4Mo0gVUroCpg1wA5EuWWQ' && $scopesOld['gc'] = 0){
//                $newParams->scopeDel[] = 'gc_pzP9O2TW9eOiCXn4Mo0gVUroCpg1wA5EuWWQ';
//            }elseif ($perm->scope_id != 'gc_pzP9O2TW9eOiCXn4Mo0gVUroCpg1wA5EuWWQ' && $scopesOld['gc'] = 1){
//                $newParams->scopeNew[] = 'gc_pzP9O2TW9eOiCXn4Mo0gVUroCpg1wA5EuWWQ';
//            }
//            if($perm->scope_id == 'gf_fjw6HcqXaXcJHkRUNSe2xbMygiiCbwkBltJ2' && $scopesOld['gf'] = 0){
//                $newParams->scopeDel[] = 'gf_fjw6HcqXaXcJHkRUNSe2xbMygiiCbwkBltJ2';
//            }elseif ($perm->scope_id != 'gf_fjw6HcqXaXcJHkRUNSe2xbMygiiCbwkBltJ2' && $scopesOld['gf'] = 1){
//                $newParams->scopeNew[] = 'gf_fjw6HcqXaXcJHkRUNSe2xbMygiiCbwkBltJ2';
//            }
//        }
        unset($input['clientPerm']);

        Log::debug(print_r($input, true));

        if($input['email'] != $change->client->email){
            $newParams->email = $input['email'];
        }
        if($input['name'] != $change->clientSecret->name){
            $newParams->name = $input['name'];
        }
        if($input['site'] != $change->client->site){
            $newParams->site = $input['site'];
        }
        if($input['password'] != $change->client->password){
            if(hash('sha256', $input['password']) != $change->client->password){
                $newParams->password = hash('sha256',$input['password']);
            }
        }
        Log::debug(print_r($newParams, true));


        //$input['password'] = hash('sha256', $input['password']);
        //$response = clientsModel::editClient($request->input('params'));
        //if($response){
            return response()->json(array('msg' => 'Cliente con tercod: '.$input['id'].' modificado correctamente en la aplicación.', 'status' => 200));
//        }else{
//            return response()->json(array('msg' => 'Error al modificar el cliente con tercod: '.$input['id'].'.', 'status' => 400));
//        }

    }
    public function setHocelotClient(Request $request){
        $params = array(
                'user' => new stdClass(),
                'address' => new stdClass()
            );
        $input = $request->input('params');
        if(!isset($input['productos'])) $input['productos'] = array();
        $params['user']->business_name = $input['nombre_emp'];
        $params['user']->name = $input['nombre'];
        $params['user']->first_name = $input['apellidos'];
        $params['user']->last_name = "";
        $params['user']->doc_type = "CIF";
        $params['user']->cif_dni = $input['doc'];
        $params['user']->phone = $input['telefono'];
        $params['user']->gender = "M";
        $params['user']->birthday = '1980-01-01';
        $params['user']->email = $input['email'];
        $params['user']->ter_type = "J";
        $params['user']->scope = $input['productos'];
        $params['user']->site = "";
        $scope = array();
        $keys = UtilClass::generateKeys();
        foreach ($input['productos'] as $prod){
            if($prod == 'ic'){
                $scope[] = 'ic_Tl3h7b3HsEeAe2b0DtW8zmpQ9EnBc9XjCsM7';
            }
            if($prod == 'if'){
                $scope[] = 'if_Fcz72MajCgEVDnf0s2ct8M52RffsQlI1tZM6';
            }
            if($prod == 'gc'){
                $scope[] = 'gc_pzP9O2TW9eOiCXn4Mo0gVUroCpg1wA5EuWWQ';
            }
            if($prod == 'gf'){
                $scope[] = 'gf_fjw6HcqXaXcJHkRUNSe2xbMygiiCbwkBltJ2';
            }
            if($prod == 'rs'){
                $scope[] = 'sc_IpqAf3mZZVVnUcMW56PBDfndae94DTIQOPbY';
            }
            if($prod == 'ad'){
                $scope[] = 'mp_HPDMhyzXlvGZoOUTXkmCZjIAXALILrKW71AD';
            }
        }
        $params['user']->password = hash('sha256', $input['password']);
        $params['address']->road_type = '';
        $params['address']->street = $input['direccion'];
        $params['address']->number = '';
        $params['address']->stair = '';
        $params['address']->floor = '';
        $params['address']->door = '';
        $params['address']->block = '';
        $params['address']->province = '';
        $params['address']->town = '';
        $params['address']->zip_code = '';
        $params['address']->km = '';
        $params['address']->letter = '';
        $response = ClientsModel::setHocelotClient($params, $scope, $keys);
        if(isset($response->status)) {
            if ($response->status == 200) {
                return response()->json(['status' => 200]);
            } else {
                return response()->json(['status' => 400, 'data' => $response->data]);
            }
        }else{
            return response()->json(['status' => 400, 'data' => $response->data]);
        }
    }
}