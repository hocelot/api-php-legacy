<?php

/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 12/12/16
 * Time: 12:47
 */
namespace App\Http\NodeModel;
use NeoEloquent;

class Province extends NeoEloquent
{
    protected $connection = 'neo4j';
    protected $label = 'Province'; // or array('User', 'Fan')

    protected $fillable = ['id', 'name'];

    public function town()
    {
        return $this->hasMany('Town');
    }

}