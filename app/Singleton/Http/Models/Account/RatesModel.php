<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 19/06/16
 * Time: 19:43
 */

namespace App\Http\Models\Account;

use Log;
use stdClass;
class RatesModel
{
    public static function getRates($id)
    {
        $params = self::prepareParams($id);
        //Log::debug(print_r($params, true));
        try {
            $result = new stdClass();
            $result->data = app('wsdl')->call_ws(app('wsdl')->getWsUrl('clientes'), 'hc_consultas', $params);
            $result->status = 200;
            //Log::debug(print_r($result, true));
            if ($result->data->hc_consultasResult == 0){
                Log::debug(print_r('ERROR [getRates Model] '. $result->data->ref_as_mensaje, true));
                $result->status = 400;
                return $result;
            }
            return $result;
        } catch (\Exception $f) {
            Log::error(print_r('Exception [getRates Model] ' . $f, true));
            $result->status = 400;
            return $result;
        }
    }

    public static function getDataRates($id)
    {
        $params = self::prepareGetRate($id);
        try {
            $result = new stdClass();
            $result->data = app('wsdl')->call_ws(app('wsdl')->getWsUrl('clientes'), 'hc_consulta_evaluaciones', $params);
            $result->status = 200;
            if ($result->data->hc_consulta_evaluacionesResult == 0){
                Log::debug(print_r('ERROR [getDataRates Model] '. $result->data->ref_as_mensaje, true));
                $result->status = 400;
                return $result;
            }
            return $result;
        } catch (\Exception $f) {
            Log::error(print_r('Exception [getDataRates Model] ' . $f,true));
            $result->status = 400;
            return $result;
        }
    }
    public static function prepareGetRate($id)
    {
        $res['val_as_base_url'] = app('wsdl')->getBaseUrl();
        $res['val_ad_ter_codigo'] = $id;
        $res['val_astr_hc_id_check'] = array(1 => 'qwe');
        $res['val_astr_hc_id_fraud'] = array(1 => 'qwe');
        $res['val_astr_hc_geo_check'] = array(1 => 'qwe');
        $res['val_astr_hc_geo_fraud'] = array(1 => 'qwe');
        $res['val_asrt_hc_eval'] = array(1 => 'qwe');
        $res['ref_as_mensaje'] = '';
        return $res;
    }
    public static function prepareParams($id)
    {
        $res['val_as_base_url'] = app('wsdl')->getBaseUrl();
        $res['val_asrt_tgmiratingeval'] = array();
        $res['val_ad_cliente'] = $id;
        return $res;
    }
}