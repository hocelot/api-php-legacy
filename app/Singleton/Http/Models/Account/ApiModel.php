<?php
/**
 * Created by PhpStorm.
 * User: DC
 * Date: 25/01/2017
 * Time: 16:25
 */

namespace App\Http\Models\Account;
use Log;
use StdClass;

class ApiModel {
    public static function prepareParams($id)
    {
        $res['val_as_base_url'] = app('wsdl')->getBaseUrl();
        $res['ref_as_mensaje'] = '';
        $res['val_astr_hc_consumos_totales'] = array();
        $res['val_astr_hc_consumos_meses'] = array();
        $res['val_as_estado_api'] = '';
        $res['val_as_api_key'] = '';
        $res['val_ad_ter_codigo'] = $id;
        $res['val_astr_hc_desglose_consumos'] = array();
        return $res;
    }
    public static function getConsumo($id)
    {
        $params = self::prepareParams($id);
        try {
            $result = new stdClass();
            $result->data = app('wsdl')->call_ws(app('wsdl')->getWsUrl('clientes'), 'hc_consumos_totales', $params);
            $result->status = 200;
            return $result;
        } catch (\Exception $f) {
            Log::error('Exception [getConsumo Model] ' . $f);
            $result = new stdClass();
            $result->data = $f;
            $result->status = 400;
            return $result;
        }
    }
}