<?php
/**
 * Created by PhpStorm.
 * User: a.luis
 * Date: 13/07/2016
 * Time: 10:31
 */

namespace App\Http\Models\Account;

use stdClass;
use Log;

class UserModel {
	public static function wsdlSidebarProfile($id){
		$params_datos_cl = array(
            'val_as_base_url' =>  app('wsdl')->getBaseUrl(),
            'val_ad_ter_codigo' => $id,
            'val_as_cif' => '',
            'val_as_nombre' => '',
            'val_as_fecha'=> '',
            'val_astr_hc_productos'=> array(),
            'ref_as_mensaje' => ''
		);
        try {
            $result= new StdClass;
            $result->data = app('wsdl')->call_ws(app('wsdl')->getWsUrl('clientes'), 'hc_productos', $params_datos_cl);
            $result->status = 200;

            //Log::debug(print_r($result, true));
            return $result;
        } catch (\Exception $f) {
            Log::error('Exception [wsdlProfile Model] ' . $f);
            return ['status' => 400, 'exception' => $f, 'data'=>''];
        }
	}
    public static function wsdlProfile($id){
        $params_datos_cl = array(
            'val_as_base_url' =>  app('wsdl')->getBaseUrl(),
            'val_al_ter_codigo' => $id,
            'ref_astr_tgtercero' => '',
            'ref_as_mensaje' => ''
        );
        try {
            $result= new StdClass;
            $result->data = app('wsdl')->call_ws(app('wsdl')->getWsUrl('clientes'), 'damedatosclientemirating', $params_datos_cl);
            $result->status = 200;

            Log::debug(print_r($result, true));
            return $result;
        } catch (\Exception $f) {
            Log::error('Exception [wsdlProfile Model] ' . $f);
            return ['status' => 400, 'exception' => $f, 'data'=>''];
        }
    }
	public static function sqlProfile($id){
		$slug = app('db')->connection('mr_test')->table('mr_cliente')->select('validar_email')->where('id', $id)->first();
		return ($slug->validar_email== 0);
	}

	public static function getType($client_id){
		$res = app('db')->connection('mr_test')->table('mr_cliente')->select('tipo')->where('id', $client_id)->first();
		return $res->tipo;
	}
}