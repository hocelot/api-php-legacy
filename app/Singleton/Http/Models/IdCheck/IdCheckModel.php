<?php

/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 3/11/16
 * Time: 13:12
 */
namespace App\Http\Models\IdCheck;
use Log;
use stdClass;

class IdCheckModel
{
    public static function save($ter_codigo,$ip,$aeat_name,$correlacion,$score,$cliente,$origen)
    {
        $str_mr_id_check = new stdClass();
        $str_mr_id_check->ter_codigo = $ter_codigo;
        $str_mr_id_check->fecha = date('c');
        $str_mr_id_check->ip = $ip;
        $str_mr_id_check->resultado = $score;//$aeat_name;
        $str_mr_id_check->usuario = '-';
        $str_mr_id_check->id_check = $correlacion;
        $str_mr_id_check->nombre = $aeat_name;
        $str_mr_id_check->origen = $origen;
        $str_mr_id_check->cliente = $cliente;

        $val_astr_mr_id_check['str_mr_id_check'] = $str_mr_id_check;
        $params_calc_mirating = array(
            'val_as_base_url' => app('wsdl')->getBaseUrl(),
            'val_astr_mr_id_check' => $val_astr_mr_id_check,
            'ref_as_mensaje' => ''
        );
        try {
            Log::debug('Envio mr_id_check');
            Log::debug(print_r($params_calc_mirating, true));
            return app('wsdl')->call_ws(app('wsdl')->getWsUrl('clientes'), 'mr_id_check', $params_calc_mirating);
        } catch (\Exception $f) {
            Log::error('Exception calculateScore ' . $f);

            return ['status' => 100, 'exception' => $f];
        }
    }
}