<?php

/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 4/11/16
 * Time: 10:46
 */
namespace App\Http\Models\Proxies;

use Log;
use stdClass;
use Mail;
class ProxiesModel
{
    public static function get($service,$proxies_usados)
    {
        try {
            // Cogemos el proxy que no tenga conexiones y el ultimo que ha sido utilizado
            $proxy = app('db')->connection('apidb')->table('proxies')->select('ip')->whereNotIn('ip', $proxies_usados)->where($service.'_status', 'OK')->
                orderBy($service.'_last_connection', 'asc')->first();
            return $proxy->ip;
        } catch (\Exception $e) {
//            Mail::raw('Todos los proxies caidos', function ($msg) {
//                $msg->subject('Proxies');
//                $msg->to(['a.sanluis@bowbuy.com','r.marcos@bowbuy.com']);
//                $msg->from(['no-reply@hocelot.com']);
//            });
            return false;
        }
    }
    public static function lock($ip){
        app('db')->connection('apidb')->table('proxies')->where('ip', $ip)->increment('currents_connections', 1);

    }

    public static function unlock($ip,$service,$status)
    {
        try {
            if($status == 200) $status = 'OK';
            else $status = 'KO';
            //Si ha fallado ya se ha restado solo se actualiza el status;
            $proxy = app('db')->connection('apidb')->table('proxies')->select('currents_connections')->where('ip', $ip)->first();
            if($proxy->currents_connections > 0)
                app('db')->connection('apidb')->table('proxies')->where('ip', $ip)->decrement('currents_connections', 1);

            app('db')->connection('apidb')->table('proxies')->where('ip', $ip)->update([$service.'_last_connection' => date('c'),$service.'_status' => $status]);
            return true;
        } catch (\Exception $e) {
//            $msj ='Error al hacer unlock:'.PHP_EOL.'IP: '.$ip.PHP_EOL.'Servicio: '.$service.PHP_EOL.'Status: '.$status.PHP_EOL.'Decrement: '.$decrement;
//            Mail::raw($msj, function ($msg) {
//                $msg->subject('Proxies');
//                $msg->to(['a.sanluis@bowbuy.com','r.marcos@bowbuy.com']);
//                $msg->from(['no-reply@hocelot.com']);
//            });

            return false;
        }
    }

    public static function okcount($service)
    {
        return app('db')->connection('apidb')->table('proxies')->where('currents_connections', '<' ,'51')
            ->where($service.'_status', 'OK')->count();
    }
}