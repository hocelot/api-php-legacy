<?php
/**
 * Created by PhpStorm.
 * User: a.luis
 * Date: 13/07/2016
 * Time: 9:21
 */

namespace App\Http\Models;


use Log;
use stdClass;

class AuthModel
{

    public static function login($params)
    {
        Log::debug(print_r($params, true));

        $user = app('db')->connection('apidb')->table('clients')->where('email', $params->email)->where('password', $params->password)->first();
        $user_id = '';
        if (!empty($user->id)) {

            $user_id = $user->id;
//            app('db')->connection('apidb')->table('clients')->where('ter_cod', $user_id)->update(['updated_at' => date('Y-m-d H:i:s')]);
        }

//        app('db')->connection('mr_test')->table('mr_logs_login')->insert(
//            [
//                'idCliente' => $user_id,
//                'ip' => $params['ip'],
//                'fecha' => date('Y-m-d H:i:s'),
//                'navegador' => $params['nav'],
//                'pass' => $params['password'],
//                'email' => $params['email']
//            ]
//        );

        return $user_id;
    }

    public static function join($params)
    {
        $resp = AuthModel::wsdljoin($params);
        unset($params);
        //Excepcion en el webService
        if (is_array($resp)) {
            return $resp;
        }
        return $resp;

        //Log::debug(print_r($params, true));
        //$password = $params['user']->password;




//        if (!empty($resp->val_astr_tgtercero->str_tgtercero[0]->ter_codigo)) {
//            $slug = hash('sha256', date('Y-m-d H:i:s') . $resp->val_astr_tgterdir->str_tgterdir[0]->dir_email);
//            $user = array(
//                'id' => $resp->val_astr_tgtercero->str_tgtercero[0]->ter_codigo,
//                'slug' => $slug
//            );
//            $status = 200;
//            if (!empty($resp->ref_as_mensaje)) {
//                app('db')->connection('mr_test')->table('mr_cliente')->insert(
//                    [
//                        'id' => $user['id'],
//                        'email' => $resp->val_astr_tgterdir->str_tgterdir[0]->dir_email,
//                        'password' => $password,
//                        'num_creditos_consumidos' => 0,
//                        'last_login' => date('Y-m-d H:i:s'),
//                        'estado' => 1,
//                        'fecha_registro' => date('Y-m-d H:i:s'),
//                        'num_creditos_totales' => 0,
//                        'tipo' => $resp->val_astr_tgtercero->str_tgtercero[0]->ter_tipotercero,
//                        'validar_email' => $slug
//
//                    ]
//                );
//            } else {
//                $user = 'Usuario ya registrado';
//                $status = 401;
//            }
//        } else {
//            $user = $resp->ref_as_mensaje;
//            $status = 400;
//        }
//        unset($resp);

//        return ['user' => $user, 'status' => $status];
    }

    public static function wsdljoin($params)
    {
        //Log::debug('WSDLJOIN params '.print_r($params,true));
        $str_tgtercero = new stdClass();
        $str_tgtercontactos = new stdClass();

        // J(EMPRESA), F(Particular), S(Score)
        if (!isset($params->user->ter_type)) {
            $params->user->doc_type='NIF';
            $str_tgtercero->ter_tipotercero = 'S';
        } else {
            $str_tgtercero->ter_tipotercero = $params->user->ter_type;
        }

        if (!isset($params->user->business_name)) {
            $str_tgtercero->ter_nomsoc = $params->user->name. ' ' . $params->user->first_name . ' ' . $params->user->last_name;
            $str_tgtercontactos->cnt_apellidos = $params->user->first_name . '/' . $params->user->last_name;
            $str_tgtercero->ter_nomcom = $params->user->name . ' ' . $params->user->first_name . ' ' . $params->user->last_name;

        } else {
            $str_tgtercontactos->cnt_apellidos = $params->user->first_name;
            $str_tgtercero->ter_nomsoc = $params->user->business_name;
            $str_tgtercero->ter_nomcom = $params->user->business_name;
        }
        $str_tgtercero->ter_codigo = 0;
        $str_tgtercero->cliente = $params->user->client_id;
        $str_tgtercero->ter_fecha_activo = date('c');
        $str_tgtercero->ter_fecha_baja = date('c');
        $str_tgtercero->ter_fechareg = date('c');
        $str_tgtercero->ter_activo = '';
        $str_tgtercero->ter_tipoidentif = $params->user->doc_type;
        $str_tgtercero->ter_numidentif = $params->user->cif_dni;
        $str_tgtercero->ter_link_activacion = '';//$num_ident, // Los clientes no tienen link de activación, así que almacenamos el número de identificador porque en BD no se admiten nulos
        $str_tgtercero->ter_url = '';
        $str_tgtercero->ter_sector = '';
        $str_tgtercero->creditos_disponibles = 0;
        $str_tgtercero->creditos_consumidos = 0;
        $str_tgtercero->estado = 1;
        $str_tgtercero->tipo = 0;//Premium o gratis


        $str_tgterdir = new stdClass();
        $str_tgterdir->ter_codigo = 0;
        $str_tgterdir->dir_codigo = 0;
        $str_tgterdir->dir_sigla = $params->address->road_type;
        $str_tgterdir->dir_viapublica = $params->address->street;
        $str_tgterdir->dir_casakm = $params->address->number;
        $str_tgterdir->dir_escalera = $params->address->stair;
        $str_tgterdir->dir_piso = $params->address->floor;
        $str_tgterdir->dir_puerta = $params->address->door;
        $str_tgterdir->dir_bloque = $params->address->block;
        $str_tgterdir->dir_pais = 'ES';
        $str_tgterdir->provincia = substr($params->address->zip_code,0,2);//$params->address->province
        $str_tgterdir->control = '';
        $str_tgterdir->poblacion = $params->address->town;
        $str_tgterdir->codpostal = $params->address->zip_code;
        $str_tgterdir->dir_telefono1 = $params->user->phone;
        $str_tgterdir->dir_telefono2 = '';
        $str_tgterdir->dir_fax = '';
        $str_tgterdir->dir_email = $params->user->email;
        $str_tgterdir->dir_dirppal = '';
        $str_tgterdir->dir_activa = '';


        $str_tgtercontactos->ter_codigo = 0;
        $str_tgtercontactos->cnt_numeroid = 1;
        $str_tgtercontactos->cnt_cargo = '';
        $str_tgtercontactos->cnt_nombre = $params->user->name;
        $str_tgtercontactos->cnt_tipoindentif = $params->user->doc_type;
        $str_tgtercontactos->cnt_numidentif = $params->user->cif_dni;
        $str_tgtercontactos->cnt_telefono1 = $params->user->phone;
        $str_tgtercontactos->cnt_telefono2 = '';
        $str_tgtercontactos->cnt_movil1 = $params->user->phone;
        $str_tgtercontactos->cnt_movil2 = '';
        $str_tgtercontactos->cnt_email1 = $params->user->email;
        $str_tgtercontactos->cnt_email2 = '';
        $str_tgtercontactos->cnt_acceso_web = '';
        $str_tgtercontactos->cnt_fechanac = $params->user->birthday;
        $str_tgtercontactos->cnt_sexo = $params->user->gender;


        $val_astr_tgtercero['str_tgtercero'] = $str_tgtercero;
        $val_astr_tgterdir['str_tgterdir'] = $str_tgterdir;
        $val_astr_tgtercontactos['str_tgtercontactos'] = $str_tgtercontactos;
        $params_alta_cl = array(
            'val_as_base_url' => app('wsdl')->getBaseUrl(),
            'val_astr_tgtercero' => $val_astr_tgtercero,
            'val_astr_tgterdir' => $val_astr_tgterdir,
            'val_astr_tgtercontactos' => $val_astr_tgtercontactos,
            'ref_srt_miratingeval' => array(),
            'ref_ab_evaluar' => '',
            'ref_ll_precio' => '0',
            'ref_ll_metros' => '0',
            'ref_as_cif' => $params->user->cif_dni,
            'ref_as_nbhood' => '',
            'ref_al_cliente' => $params->user->client_id,// Cambiar para score
            'ref_as_mensaje' => ''
        );
        try {
//           Log::debug('Envio crearclientemir...');
//            $response = new stdClass();
            $response = app('wsdl')->call_ws(app('wsdl')->getWsUrl('clientes'), 'crearclientemirating_nuevo', $params_alta_cl);
//            Log::debug('Envio crearclientemir...');
//            Log::debug(print_r($response->data,true));

            //$response->status = 200;
            return $response;

        } catch (\Exception $f) {
//            ini_set('memory_limit',-1);
           Log::debug('Error WSDL ');
            $response = new stdClass();
            $response->status = 400;
            return $response;
        }
    }

}