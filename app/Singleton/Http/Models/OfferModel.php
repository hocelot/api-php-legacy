<?php
/**
 * Created by PhpStorm.
 * User: a.luis
 * Date: 18/07/2016
 * Time: 12:39
 */

namespace App\Http\Models;


class OfferModel {
	public static function getOffers($tipo){
		return app('db')->connection('mr_test')->select('nombre,precio,num_creditos')->table('mr_oferta')->where('tipo', $tipo)->get();
	}

	public static function getOfferPrice($tipo,$num_credits){
		$result = app('db')->connection('mr_test')->select('precio')->table('mr_oferta')->where('tipo', $tipo)
			->where('num_creditos',$num_credits)->first();
        return $result->precio;
	}

}