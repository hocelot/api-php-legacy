<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 2/11/16
 * Time: 8:52
 */
namespace App\Http\Models\GeoCheck;
class RoadTypeModel
{
    public static function normalize($rt_search)
    {
        GeoCheckModel::del_articulos($rt_search);
        $result = false;
        $query = [
            'query' => [
                'bool' => [
                    'must' => [
                        'match' => [
                            'full_name' => [
                                "fuzziness" => "AUTO",
                                "operator" => "and",
                                "query" => $rt_search,
                            ],
                        ],
                    ]
                ]
            ]
        ];

        $arr = GeoCheckModel::elastica_search('address','roadtypes',$query);
        if (count($arr) == 1) {
            $result = $arr[0]->getData()['name'];
        } elseif (count($arr) > 1){
            $max_percent = 0;
            foreach ($arr as $res) {
                similar_text($res->getData()['full_name'], $rt_search, $percent);
                if ($percent > $max_percent) {
                    $max_percent = $percent;
                    $result = $res->getData()['name'];
                }
            }
        }
        return $result;
    }
}