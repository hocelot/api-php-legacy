<?php
/**
 * Created by PhpStorm.
 * User: DC
 * Date: 04/05/17
 * Time: 17:43
 */

namespace App\Http\Models\Demo;

use App\Libraries\SmsAltiria;
use Log;
use PDOException;
use StdClass;

class DemoModel
{
    /*FUNCION PARA VER SI EXISTE EL MOVIL*/
    public static function getStatusMobile($params){
        Log::debug(print_r($params, true));
        $exist = self::existMobile($params);
        if ($exist) {
            if ($exist->trys > 0 && $exist->date > Date("Y-m-d H:i:s") && $exist->active == 1) {
                self::sendSms($params['phone'], $exist->key);
                $perm = self::quitPerm($params);
                if ($perm) {
                    return array('status' => 200, 'msg' => 'Reenviar', 'key' => $exist->key);
                } else {
                    return array('status' => 400, 'msg' => 'Error al actualizar BBDD');
                }
            } else {
                if ($exist->trys == 0){
                    return array('status' => 401, 'msg' => 'Has agotado los intentos');
                }else return array('status' => 400, 'msg' => 'Has agotado las consultas');
            }
        }
        else {
            $sms = self::sendSms($params['phone']);
            if($sms['status'] == 200) {
                $params['key'] = $sms['key'];
            }else return $sms;
            $putKey = self::putKeyMobile($params);
            if ($putKey) {
                return array('status' => 200, 'msg' => 'Código enviado.', 'key' => $params['key']);
            } else {
                return array('status' => 400, 'msg' => 'Error al grabar en BBDD');
            }
        }
    }
    public static function successCons($telf){
        try {
            app('db')->connection('apidb')->table('demo_key_phone')->where('phone', $telf) ->update(['active' => 0]);
            return array('status' => 200, 'msg' => 'Intentos agotados');
        }catch (PDOException $e){
            Log::error(print_r('existMobile [DemoModel] Exception', true));
            return false;
        }
    }
    private static function sendSms($telf , $verif_code = false){
        if(!$verif_code) {
            $verif_code = rand(10000, 99999);
        }
        $movil = '34' . $telf;
        $val_obj = new  SmsAltiria();
        $cod=$val_obj->sendMessage($movil, $verif_code);
        if($cod) {
            return array('status' => 200, 'key' => $cod);
        }else{
            return array('status' => 400, 'msg' => 'Error al enviar SMS.');
        }
    }
    private static function existMobile($params){
        try {
            $phone = app('db')->connection('apidb')->table('demo_key_phone')->select('*')->where('phone', $params['phone'])->first();
            return $phone;
        }catch (PDOException $e){
            Log::error(print_r('existMobile [DemoModel] Exception', true));
            return false;
        }
    }
    private static function quitPerm($params){
        try {
            //Log::debug(print_r(app('db')->connection('apidb')->table('demo_key_phone')->select('*')->where('phone', $params['phone'])->first(), true));
            return app('db')->connection('apidb')->table('demo_key_phone')->where('phone', $params['phone'])->decrement('trys', 1);
        }catch (PDOException $e){
            Log::error(print_r('quitPerm [DemoModel] Exception', true));
            return false;
        }
    }
    public static function quitActive($params){
        try {
            $quit = app('db')->connection('apidb')->table('demo_key_phone')->where('phone', $params)->decrement('active', 1);
            Log::debug(print_r(app('db')->connection('apidb')->table('demo_key_phone')->select('*')->where('phone', $params)->first(), true));
            return $quit;
        }catch (PDOException $e){
            Log::error(print_r('quitActive [DemoModel] Exception', true));
            return false;
        }
    }
    private static function putKeyMobile($params){
        try {
            $date = Date("Y-m-d H:i:s");
            return app('db')->connection('apidb')->table('demo_key_phone')->insert([
                'phone' => $params['phone'],
                'key' => $params['key'],
                'trys' => 2,
                'active' => 1,
                'date' => date('Y-m-d H:i:s', strtotime($date . ' +1 day'))
            ]);
        }catch (PDOException $e){
            Log::error(print_r('putKeyMobile [DemoModel] Exception', true));
            return false;
        }
    }
}