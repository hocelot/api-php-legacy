<?php
/**
 * Created by PhpStorm.
 * User: Alex San Luis
 * Date: 16/06/2016
 * Time: 19:42
 */

namespace App\Http\Models;


class UtilClass
{
    public static function convertRoadType($param)
    {
        switch (trim($param)) {
            case 'AG':
                $tipovia = 'AGREGADO';
                break;
            case 'AL':
                $tipovia = 'ALDEA, ALAMEDA';
                break;
            case 'AR':
                $tipovia = 'AREA, ARRABAL';
                break;
            case 'AU':
                $tipovia = 'AUTOPISTA';
                break;
            case 'AV':
                $tipovia = 'AVENIDA';
                break;
            case 'AY':
                $tipovia = 'ARROYO';
                break;
            case 'BJ':
                $tipovia = 'BAJADA';
                break;
            case 'BO':
                $tipovia = 'BARRIO';
                break;
            case 'BR':
                $tipovia = 'BARRANCO';
                break;
            case 'CA':
                $tipovia = 'CAÑADA';
                break;
            case 'CG':
                $tipovia = 'COLEGIO, CIGARRAL';
                break;
            case 'CH':
                $tipovia = 'CHALET';
                break;
            case 'CI':
                $tipovia = 'CINTURON';
                break;
            case 'CJ':
                $tipovia = 'CALLEJA, CALLEJON';
                break;
            case 'CL':
                $tipovia = 'CALLE';
                break;
            case 'CM':
                $tipovia = 'CAMINO, CARMEN';
                break;
            case 'CN':
                $tipovia = 'COLONIA';
                break;
            case 'CO':
                $tipovia = 'CONCEJO, COLEGIO';
                break;
            case 'CP':
                $tipovia = 'CAMPA, CAMPO';
                break;
            case 'CR':
                $tipovia = 'CARRETERA, CARRERA';
                break;
            case 'CS':
                $tipovia = 'CASERIO';
                break;
            case 'CT':
                $tipovia = 'CUESTA, COSTANILLA';
                break;
            case 'CU':
                $tipovia = 'CONJUNTO';
                break;
            case 'DE':
                $tipovia = 'DETRÁS';
                break;
            case 'DP':
                $tipovia = 'DIPUTACION';
                break;
            case 'DS':
                $tipovia = 'DISEMINADOS';
                break;
            case 'ED':
                $tipovia = 'EDIFICIOS';
                break;
            case 'EM':
                $tipovia = 'EXTRAMUROS';
                break;
            case 'EN':
                $tipovia = 'ENTRADA, ENSANCHE';
                break;
            case 'ER':
                $tipovia = 'EXTRARRADIO';
                break;
            case 'ES':
                $tipovia = 'ESCALINATA';
                break;
            case 'EX':
                $tipovia = 'EXPLANADA';
                break;
            case 'FC':
                $tipovia = 'FERROCARRIL';
                break;
            case 'FN':
                $tipovia = 'FINCA';
                break;
            case 'GL':
                $tipovia = 'GLORIETA';
                break;
            case 'GR':
                $tipovia = 'GRUPO';
                break;
            case 'GV':
                $tipovia = 'GRAN VIA';
                break;
            case 'HT':
                $tipovia = 'HUERTA, HUERTO';
                break;
            case 'JR':
                $tipovia = 'JARDINES';
                break;
            case 'LD':
                $tipovia = 'LADO, LADERA';
                break;
            case 'LG':
                $tipovia = 'LUGAR';
                break;
            case 'MC':
                $tipovia = 'MERCADO';
                break;
            case 'ML':
                $tipovia = 'MUELLE';
                break;
            case 'MN':
                $tipovia = 'MUNICIPIO';
                break;
            case 'MS':
                $tipovia = 'MASIAS';
                break;
            case 'MT':
                $tipovia = 'MONTE';
                break;
            case 'MZ':
                $tipovia = 'MANZANA';
                break;
            case 'PB':
                $tipovia = 'POBLADO';
                break;
            case 'PD':
                $tipovia = 'PARTIDA';
                break;
            case 'PJ':
                $tipovia = 'PASAJE, PASADIZO';
                break;
            case 'PL':
                $tipovia = 'POLIGONO';
                break;
            case 'PM':
                $tipovia = 'PARAMO';
                break;
            case 'PQ':
                $tipovia = 'PARROQUIA, PARQUE';
                break;
            case 'PR':
                $tipovia = 'PROLONGACION, CONTINUAC.';
                break;
            case 'PS':
                $tipovia = 'PASEO';
                break;
            case 'PT':
                $tipovia = 'PUENTE';
                break;
            case 'PZ':
                $tipovia = 'PLAZA';
                break;
            case 'QT':
                $tipovia = 'QUINTA';
                break;
            case 'RB':
                $tipovia = 'RAMBLA';
                break;
            case 'RC':
                $tipovia = 'RINCON, RINCONA';
                break;
            case 'RD':
                $tipovia = 'RONDA';
                break;
            case 'RM':
                $tipovia = 'RAMAL';
                break;
            case 'RP':
                $tipovia = 'RAMPA';
                break;
            case 'RR':
                $tipovia = 'RIERA';
                break;
            case 'RU':
                $tipovia = 'RUA';
                break;
            case 'SA':
                $tipovia = 'SALIDA';
                break;
            case 'SD':
                $tipovia = 'SENDA';
                break;
            case 'SL':
                $tipovia = 'SOLAR';
                break;
            case 'SN':
                $tipovia = 'SALON';
                break;
            case 'SU':
                $tipovia = 'SUBIDA';
                break;
            case 'TN':
                $tipovia = 'TERRENOS';
                break;
            case 'TO':
                $tipovia = 'TORRENTE';
                break;
            case 'TR':
                $tipovia = 'TRAVESIA';
                break;
            case 'UR':
                $tipovia = 'URBANIZACION';
                break;
            case 'VR':
                $tipovia = 'VEREDA';
                break;
            case 'CY':
                $tipovia = 'CALEYA';
                break;
            // TIPO DE VIA EUSKADI
            case 'EP':
                $tipovia = 'PLAZA';
                break;
            case 'BL':
                $tipovia = 'BLOQUE';
                break;
            case 'NU':
                $tipovia = 'NUCLEO';
                break;
            case 'BA':
                $tipovia = 'BARRIADA';
                break;
            default:
                $tipovia = ' ';
                break;
        }
        return $tipovia;
    }

    public static function densityRadio($type_town)
    {
        $radio = 0;
        // PREPARO EL RADIO DE BUSQUEDA SEGUN DENSIDAD DE POBLACION  --> $res_catastro[ref_as_tipo_municipio]
        switch ($type_town) {
            case 1:
                $radio = 15000;
                break;
            case 2:
                $radio = 15000;
                break;
            case 3:
                $radio = 10000;
                break;
            case 4:
                $radio = 450;
                break;
            case 5:
                $radio = 5000;
                break;
            case 6:
                $radio = 500;// R. Marcos 18-04-2016 Cambiado a 1000m por el corte de idealista, antes 500
                break;
            case 7:
                $radio = 200; // R. Marcos 18-04-2016 Cambiado a 1000m por el corte de idealista, antes 200
                break;
            case 8:
                $radio = 150;// R. Marcos 18-04-2016 Cambiado a 1000m por el corte de idealista, antes 150
                break;
            case 9:
                $radio = 150; // R. Marcos 18-04-2016 Cambiado a 1000m por el corte de idealista, antes 150
                break;
            case 10:
                $radio = 100; // R. Marcos 18-04-2016 Cambiado a 1000m por el corte de idealista, antes 100
                break;
        }
        return $radio;
    }

    public static function randVar($length)
    {
        $chars = 'QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm0123456789';
        $length_chars = strlen($chars);
        $result = '';
        for ($x = 0; $x <= $length; $x++) {
            $pos = rand(0, $length_chars);
            $result .= substr($chars, $pos, 1);
        }
        return $result;
    }

    public static function generateKeys()
    {
        $config = array(
            "digest_alg" => "sha512",
            "private_key_bits" => 4096,
            "private_key_type" => OPENSSL_KEYTYPE_RSA,
        );

        // Create the private and public key
        $res = openssl_pkey_new($config);

        // Extract the private key from $res to $privKey
        openssl_pkey_export($res, $privKey);

        // Extract the public key from $res to $pubKey
        $pubKey = openssl_pkey_get_details($res);
        $pubKey = $pubKey["key"];
        return array('public_key' => $pubKey, 'private_key' => $privKey);
//        $data = 'plaintext data goes here';
//
//        // Encrypt the data to $encrypted using the public key
//        openssl_public_encrypt($data, $encrypted, $pubKey);
//
//        // Decrypt the data using the private key and store the results in $decrypted
//        openssl_private_decrypt($encrypted, $decrypted, $privKey);
    }

    public static function elimina_acentos($cadena)
    {
        $text = htmlentities($cadena, ENT_SUBSTITUTE, 'UTF-8');
        $text = strtolower($text);
        $patron = array(
            '/\+/' => '',
            '/&agrave;/' => 'a',
            '/&egrave;/' => 'e',
            '/&igrave;/' => 'i',
            '/&ograve;/' => 'o',
            '/&ugrave;/' => 'u',
            '/&aacute;/' => 'a',
            '/&eacute;/' => 'e',
            '/&iacute;/' => 'i',
            '/&oacute;/' => 'o',
            '/&uacute;/' => 'u',
            '/&acirc;/' => 'a',
            '/&ecirc;/' => 'e',
            '/&icirc;/' => 'i',
            '/&ocirc;/' => 'o',
            '/&ucirc;/' => 'u',
            '/&atilde;/' => 'a',
            '/&etilde;/' => 'e',
            '/&itilde;/' => 'i',
            '/&otilde;/' => 'o',
            '/&utilde;/' => 'u',
            '/&auml;/' => 'a',
            '/&euml;/' => 'e',
            '/&iuml;/' => 'i',
            '/&ouml;/' => 'o',
            '/&uuml;/' => 'u',
            '/&auml;/' => 'a',
            '/&euml;/' => 'e',
            '/&iuml;/' => 'i',
            '/&ouml;/' => 'o',
            '/&uuml;/' => 'u',
            '/&aring;/' => 'a',
            '/&ntilde;/' => 'ñ',
            '/&Ccedil;/' => 'Ç',
            '/&ccedil;/' => 'ç',
            "/&#039;/" => "'"
        );

        $text = preg_replace(array_keys($patron), array_values($patron), $text);
        return $text;
    }

    public static function del_articulos($str)
    {
        //Quitamos determinantes del nombre de la calle
        $articulos = array('DEL', 'DE', 'LA', 'EL', 'LO', 'LAS', 'LOS', "L'", "N'", "A", "VAN");

        $str_aux = explode(" ", $str);

        $resultado = array_diff($str_aux, $articulos);

        $str = implode(" ", $resultado);
        return $str;
    }

    public static function del_puntoycoma($str)
    {
        //Quitamos determinantes del nombre de la calle
        $str = str_replace('.', '', $str);
        $str = str_replace(',', '', $str);

        return $str;
    }

    public static function tofloat($num) {
        $dotPos = strrpos($num, '.');
        $commaPos = strrpos($num, ',');
        $sep = (($dotPos > $commaPos) && $dotPos) ? $dotPos :
            ((($commaPos > $dotPos) && $commaPos) ? $commaPos : false);

        if (!$sep) {
            return floatval(preg_replace("/[^0-9]/", "", $num));
        }

        return floatval(
            preg_replace("/[^0-9]/", "", substr($num, 0, $sep)) . '.' .
            preg_replace("/[^0-9]/", "", substr($num, $sep+1, strlen($num)))
        );
    }

}