<?php
/**
 * Created by PhpStorm.
 * User: asanluis
 * Date: 30/11/16
 * Time: 17:54
 */

namespace App\Jobs;
use Log;

class WsdlJob extends Job
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $params, $wsdl, $wsdl_url;

    public function __construct($params, $wsdl, $wsdl_url)
    {
        $this->params = $params;
        $this->wsdl = $wsdl;
        $this->wsdl_url = $wsdl_url;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $this->params['val_as_base_url'] = app('wsdl')->getBaseUrl();
            $this->params['ref_as_mensaje'] = '';
            Log::debug('Respuesta WSDLJOB'.print_r(app('wsdl')->call_ws(app('wsdl')->getWsUrl($this->wsdl_url), $this->wsdl, $this->params),true));

        } catch (\Exception $e) {
//            Log::error('Error en WSDLJOB '.print_r($e,true));
//            if(isset($resp))Log::error('Respuesta error WSDLJOB '.print_r($resp,true));
        }

    }
}