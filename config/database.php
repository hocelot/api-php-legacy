<?php
/**
 * Created by PhpStorm.
 * User: a.luis
 * Date: 15/06/2016
 * Time: 9:53
 */

return [

    'default' => 'apidb',
    'fetch' => PDO::FETCH_CLASS,
    'connections' => [
        'oauth' => [
            'driver' => 'pgsql',
            'host' => '192.168.79.24',
            'database' => 'apioauth',
            'username' => 'postgres',
            'password' => '#QwEr1234',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'schema' => 'public',
            'strict' => false,
        ],
        'apidb' => [
            'driver' => 'pgsql',
            'host' => '192.168.79.24',
            'database' => 'apidb',
            'username' => 'postgres',
            'password' => '#QwEr1234',
            'charset' => 'utf8',
            'collation' => 'utf8_general_ci',
            'prefix' => '',
            'schema' => 'public',
            'strict' => false,
        ],
        'catastro' => [
            'driver' => 'pgsql',
            'host' => '192.168.79.24',
            'database' => 'catastro',
            'username' => 'postgres',
            'password' => '#QwEr1234',
            'prefix' => '',
            'charset' => 'utf8',
            'collation' => 'utf8_general_ci',
            'schema' => 'public',
            'strict' => false,
        ]
    ],
];
