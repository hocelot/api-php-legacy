@extends('newApi.templates.landing')
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/css/bootstrap-select.min.css">
    <link href="/assets/css/new-demo.css" rel="stylesheet"/>
@endsection
@section('content')
    <section class="col-md-12 col-xs-12 section_padding">
        <div class="container">
            <div class="col-md-12 col-xs-12">
                <div class="col-md-2">
                </div>
                <div class="col-md-8">
                    <ol class="breadcrumb">
                        <li><a href="/">Home</a></li>
                        <li><a href="/demo">Demo</a></li>
                        <li class="active"><a href="#">Resultados</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="col-md-12 col-xs-12 section_padding">
        <div class="container">
            <div class="col-md-12 col-xs-12">
                <h2>ID Check & Fraud</h2>
                <div class="section_line_div">
                    <hr class="section_line">
                </div>
                <p class="section_p">Determina la exactitud de los datos de cabecera de un individuo, variable fundamental para la detección del usuario. Id Check de Hocelot identifica la correlación DNI/NIE con el nombre y apellidos del sujeto evaluado.</p>
                <p class="section_p">Detección de anomalías entre la identidad real y la identidad digital de un usuario online. Es un sistema multi-variable que hace único al usuario por más de una variable para conseguir una descripción digital más robusta.</p>
            </div>
            <div class="col-md-12 col-xs-12 back_grey_resp">
                <div class="col-md-3 col-xs-12 result_div">
                    <div class="white_div_resp">
                        <p class="p_titulo_res">Resultado</p>
                        <p class="p_res">OK</p>
                    </div>
                </div>
                <div class="col-md-9 col-xs-12 result_div">
                    <div class="white_div_resp">
                        <p class="p_titulo_res">Resultado</p>
                        <p class="p_res">Daniel Cabezas</p>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-xs-12">
                <p class="section_p">Los valores obtenidos mayores de 70 puntos pueden ser considerados como correctos. Puede ser que haya alguna pequeña variación en los datos aportados que no coinciden 100% pero la identificación es confiable. </p>
            </div>
        </div>
    </section>
    <section class="col-md-12 col-xs-12 section_padding">
        <div class="container">
            <div class="col-md-12 col-xs-12">
                <h2>GEO Check & Fraud</h2>
                <div class="section_line_div">
                    <hr class="section_line">
                </div>
                <p class="section_p">Determina la exactitud de los datos de cabecera de un individuo, variable fundamental para la detección del usuario. Id Check de Hocelot identifica la correlación DNI/NIE con el nombre y apellidos del sujeto evaluado.</p>
            </div>
            <div class="col-md-12 col-xs-12 back_grey_resp">
                <div class="col-md-3 col-xs-12 result_div">
                    <div class="white_div_resp">
                        <p class="p_titulo_res">Resultado</p>
                        <p class="p_res">OK</p>
                    </div>
                </div>
                <div class="col-md-9 col-xs-12 result_div">
                    <div class="white_div_resp">
                        <p class="p_titulo_res">Resultado</p>
                        <p class="p_res">Daniel Cabezas</p>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-xs-12">
                <p class="section_p">Los valores obtenidos mayores de 70 puntos pueden ser considerados como correctos. Puede ser que haya alguna pequeña variación en los datos aportados que no coinciden 100% pero la identificación es confiable. </p>
            </div>
        </div>
    </section>

    <section class="col-md-12 col-xs-12 text-center section_padding">
        <h1 class="section_big_title section_padding"> ¿Alguna duda?</h1>
        <button class="link_style" id="contactaSmooth" data-toggle="modal" data-target="#contactModal">Contacta con nosotros</button>
        <div class="section_padding_bottom"></div>
    </section>

    <div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg">
            <div class="modal-header modal-contact">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <section class="col-md-12 col-xs-12 section_padding_bottom">
                {{--<div class="col-md-1 visible-md-block visible-lg-block height_input"></div>--}}
                <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                    <h1>
                        Confirma tus datos de contacto
                    </h1>
                    <p class="error_contact" id="error_text"></p>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="placeMin" style="height:5px"><text id="placenombre">Nombre</text></div>
                    <input type="text" class="form-control input_label" value="{{$form['name']}}" placeholder="Nombre" id="nombre" onBlur="checkInput(this)" onFocus="infoInput(this)" onkeyup="getPlaceMin(this)">
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="placeMin" style="height:5px"><text id="placeapellidos">Primer apellido</text></div>
                    <input type="text" class="form-control input_label" value="{{$form['lastName']}}" placeholder="Apellidos" id="apellidos" onBlur="checkInput(this)" onFocus="infoInput(this)" onkeyup="getPlaceMin(this)">
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="placeMin" style="height:5px"><text id="placecargo" style="display:none">Cargo</text></div>
                    <input type="text" class="form-control input_label" placeholder="Cargo" id="cargo" onBlur="checkInput(this)" onFocus="infoInput(this)" onkeyup="getPlaceMin(this)">
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="placeMin" style="height:5px"><text id="placenombre_emp" style="display:none">Nombre de la empresa</text></div>
                    <input type="text" class="form-control input_label" placeholder="Nombre de la empresa" id="nombre_emp" onBlur="checkInput(this)" onFocus="infoInput(this)" onkeyup="getPlaceMin(this)">
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="placeMin" style="height:5px"><text id="placeemail">Email</text></div>
                    <input type="text" class="form-control input_label" value="{{$form['email']}}" placeholder="Email" id="email" onBlur="checkEmail(this)" onFocus="infoEmail(this)" onkeyup="getPlaceMin(this)">
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="placeMin" style="height:5px"><text id="placetelefono">Teléfono</text></div>
                    <input type="text" class="form-control input_label" value="{{$form['phone']}}" placeholder="Teléfono" id="telefono" maxlength="9" onBlur="checkTelf(this)" onFocus="infoTelf(this)" onkeyup="getPlaceMin(this)">
                </div>
                {{--<div class="col-md-4 visible-md-block visible-lg-block height_input"></div>--}}
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="margin_auto_cons section_margin_top button_margin_bottom">
                        <input type="button" class="boton_consultar" id="submit_button" value="Enviar">
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    Disclaimer que dice que al darle a enviar nos autoriza a a guardar sus datos y que dice que sólo vamos a guardar los datos que aparecen en esta pantalla para incluirlos en nuestro CRM y ponernos en contacto.
                </div>
            </section>
        </div>
    </div>

    {{--    {{dd(get_defined_vars())}}--}}

    @if($id_check['status'] == 200)
        {{$id_check['hits']['nif']}}
        {{$id_check['hits']['name_surname']}}
        {{$id_check['hits']['score_id_check']}}
        {{$id_check['hits']['correlacion_id_check']}}
        {{$id_check['hits']['alert']}}
        {{$id_check['status']}}
        @if($id_fraud['status'] == 200)
            @if ($id_fraud['hits']['score'] == 0 ||$id_fraud['hits']['score'] == 1 ||$id_fraud['hits']['score'] == 2)
                Riesgo Muy Alto
            @elseif ($id_fraud['hits']['score'] == 3 ||$id_fraud['hits']['score'] == 4 ||$id_fraud['hits']['score'] == 5)
                Riesgo Alto
            @elseif ($id_fraud['hits']['score'] == 6 ||$id_fraud['hits']['score'] == 7 ||$id_fraud['hits']['score'] == 8)
                Riesgo Medio
            @elseif ($id_fraud['hits']['score'] == 9 ||$id_fraud['hits']['score'] == 10)
                Riesgo Bajo
            @endif
            {{$id_fraud['hits']['phone_info']->originalOperator}}
            {{$id_fraud['hits']['phone_info']->currentOperator}}
            {{$id_fraud['hits']['phone_info']->number}}
            {{$id_fraud['hits']['phone_info']->prefix}}
            {{$id_fraud['hits']['phone_info']->type}}
            {{$id_fraud['hits']['phone_info']->typeDescription}}
            {{--ESTOS NO ESTAN EN TODOS LOS MOVILES --}}
            {{--        {{$id_fraud['hits']['phone_info']->lastPortability->when}}--}}
            {{--        {{$id_fraud['hits']['phone_info']->lastPortability->from}}--}}
            {{--        {{$id_fraud['hits']['phone_info']->lastPortability->to}}--}}
            {{--{{$id_fraud['hits']['phone_info']->lastPortability->when}}--}}
            {{--{{$id_fraud['hits']['phone_info']->lastPortability->from}}--}}
            {{--{{$id_fraud['hits']['phone_info']->lastPortability->to}}--}}
            {{$id_fraud['status']}}
        @else
            {{'No encontrado'}}
        @endif

        {{$geo_check['hits']['parcela']}}
        {{$geo_check['hits']['address']}}
        {{$geo_check['hits']['m2']}}
        {{$geo_check['hits']['latitude']}}
        {{$geo_check['hits']['longitude']}}
        {{$geo_check['hits']['used_for']}}
        {{$geo_check['hits']['type']}}
        {{$geo_check['hits']['level']}}
        {{$geo_check['status']}}

        @if($geo_check['status'] == 200)
            {{$media_check['score']}}
            @if(gettype($media_check['hits']['media1']) == "array")
                @for($ind= 0; count($media_check['hits']['media1']) > $ind; $ind++)
                    <p class="">{{$media_check['hits']['media1'][$ind]->alias}}</p>
                    <p class="">{{$media_check['hits']['media1'][$ind]->type}}</p>
                    <p class="">{{$media_check['hits']['media1'][$ind]->price}}</p>
                    <p class="">{{$media_check['hits']['media1'][$ind]->category}}</p>
                @endfor
            @else
                {{$media_check['hits']['media1']}}
            @endif
            @if(gettype($media_check['hits']['media2']) == "array")
                @for($ind= 0; count($media_check['hits']['media2']) > $ind; $ind++)
                    <p class="">{{$media_check['hits']['media2'][$ind]->alias}}</p>
                    <p class="">{{$media_check['hits']['media2'][$ind]->type}}</p>
                    <p class="">{{$media_check['hits']['media2'][$ind]->price}}</p>
                    <p class="">{{$media_check['hits']['media2'][$ind]->category}}</p>
                @endfor
            @else
                {{$media_check['hits']['media2']}}
            @endif
            @if(gettype($media_check['hits']['media3']) == "array")
                @for($ind = 0; count($media_check['hits']['media3']) > $ind; $ind++)
                    <p class="">{{$media_check['hits']['media3'][$ind]->alias}}</p>
                    <p class="">{{$media_check['hits']['media3'][$ind]->type}}</p>
                    <p class="">{{$media_check['hits']['media3'][$ind]->price}}</p>
                    <p class="">{{$media_check['hits']['media3'][$ind]->category}}</p>
                @endfor
            @else
                {{$media_check['hits']['media3']}}
            @endif
            {{$media_check['status']}}

            @if($geo_fraud['status'] == 200)
                <p class="result_title">Dirección: <text class="result_subtitle">{{$geo_fraud['hits']['address']}}</text></p><p class="result_info"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Dirección encontrada.</p>
                <p class="result_title">Correlación: <text class="result_subtitle">{{$geo_fraud['hits']['correlacion']}}</text></p><p class="result_info"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Relacion entre sujeto.</p>
                <p class="result_title">Nombre: <text class="result_subtitle">{{$geo_fraud['hits']['name']}}</text></p><p class="result_info"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Nombre del sujeto encontrado.</p>
                <p class="result_title">Teléfono: <text class="result_subtitle">{{$geo_fraud['hits']['phone']}}</text></p><p class="result_info"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Teléfono del sujeto encontrado.</p>
            @else
                <p class="result_title">Dirección: <text class="result_subtitle"> No encontrado</text></p><p class="result_info"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Dirección encontrada.</p>
                <p class="result_title">Correlacion: <text class="result_subtitle"> 0</text></p><p class="result_info"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/>  Relacion entre sujeto y dirección encontrada.</p>
            @endif

            @if($risk_score['status'] ==200)
                <p class="result_title">Score: <text class="result_subtitle"> {{ $risk_score['hits']->score }} </text></p><p class="result_info"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Calificación de Hocelot Risk Score entre 300 y 850.</p>
                <p class="result_title">Fiabilidad de pago: <text class="result_subtitle">{{number_format($risk_score['hits']->fiabilidad_pago, 2, ",", ".")}}%</text></p><p class="result_info"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Porcentaje de éxito en los pagos realizados por el sujeto evaluado.</p>
                <p class="result_title">Probabilidad impago: <text class="result_subtitle">{{number_format($risk_score['hits']->probabilidad_impago, 2, ",", ".")}}%</text></p><p class="result_info"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Probabilidad impago del sujeto evaluado.</p>
                <p class="result_title">Periodo medio de pago: <text class="result_subtitle">{{$risk_score['hits']->periodo_medio_pago}}días.</text></p><p class="result_info"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Tasa de esfuerzo del evaluado referente a vivienda/suministros expresado en días <br> Cuanto menor es el número de días, mayor es la capacidad económica del sujeto evaluado para hacer frente a los gastos estructurales.</p>
                <p class="result_title">Régimen de tenencia: <text class="result_subtitle">{{$risk_score['hits']->tenencia}}</text></p><p class="result_info"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Régimen de tenencia más probable del domicilio/sujeto evaluado. <br> 1 = Compra con pagos pendientes. <br> 2 = Compra sin pagos pendientes. <br> 3 = Alquilada. <br> 4 = Cedida. <br> 5 = Otra.</p>
                <p class="result_title">Dependiente: <text class="result_subtitle">{{$risk_score['hits']->dependiente}}</text></p><p class="result_info"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Determina si el sujero evaluado es dependiente o no de la unidad económica familiar. <br> N = No dependiente de la unidad económica. <br> S = Dependiente de la unidad económica.</p>
                {{--<p class="result_title">Nivel de exactitud: <text class="result_subtitle">{{$risk_score['hits']->nivel_exactitud}}</text></p><p class="result_info"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Exactitud de la dirección facilitada por el usuario. <br> 0 = No existe, errónea. <br> 1 = Dirección exacta verificada. <br> 2 = Calle, número y planta verificados.<br> 3 = calle y número verificado .</p>--}}
                <p class="result_title">Ingreso mensual hogar: <text class="result_subtitle">{{number_format($risk_score['hits']->ingreso_mensual_hogar, 2, ",", ".")}}€</text></p><p class="result_info"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Importe neto mínimo mensual ingresado por la unidad familiar.</p>
                <p class="result_title">Ingreso mensual individual: <text class="result_subtitle">{{number_format($risk_score['hits']->ingreso_mensual_individual, 2, ",", ".")}}€</text></p><p class="result_info"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Importe neto mínimo mensual ingresado por el evaluado.</p>
                <p class="result_title">Desviación Renta: <text class="result_subtitle">{{number_format($risk_score['hits']->desviacion_renta, 2, ",", ".")}}%</text></p><p class="result_info"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Comparación entre la renta del evaluado y la renta media de la zona geográfica estudiada.<br> Cuando es mayor de 100, significa que está por encima de la media.</p>
                <p class="result_title">Umbral pobreza: <text class="result_subtitle">{{number_format($risk_score['hits']->umbral_pobreza, 2, ",", ".")}}%</text></p><p class="result_info"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Desviación porcentual sobre el umbral de la pobreza de la zona geográfica del evaluado, según fuentes oficiales.</p>
                <p class="result_title">Nivel estudios: <text class="result_subtitle">{{$risk_score['hits']->nivel_estudios}}</text></p><p class="result_info"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Nivel de estudios alcanzados por el evaluado.<br>1 = Estudios Primarios.<br>2 = Estudios secundarios. <br> 3 = Estudios superiores</p>
                <p class="result_title">Trabajo: <text class="result_subtitle">{{$risk_score['hits']->trabajo}}%</text></p><p class="result_info"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Probabilidad de la situación laboral del sujeto evaluado.</p>
                <p class="result_title">Metros cuadrados: <text class="result_subtitle">{{number_format($risk_score['hits']->metros_cuadrados, 0, ",", ".")}}m2</text></p><p class="result_info"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Metros cuadrados del domicilio evaluado.</p>
                <p class="result_title">Precio de mercado: <text class="result_subtitle">{{number_format($risk_score['hits']->precio, 2, ",", ".")}}€</text></p><p class="result_info"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Valor del inmueble actualizado por comparativa de mercado.</p>
                <p class="result_title">Tasa de Esfuerzo: <text class="result_subtitle">{{number_format($risk_score['hits']->tasa_esfuerzo, 2, ",", ".")}}%</text></p><p class="result_info"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Esfuerzo económico desarrollado por el evaluado para hacer frente a la “Renta vivienda individual” expresado en porcentaje. Siendo 1 el 100%. </p>
                <p class="result_title">Renta vivienda hogar: <text class="result_subtitle">{{number_format($risk_score['hits']->renta_vivienda_hogar, 2, ",", ".")}}€</text></p><p class="result_info"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Importe neto mensual destinado por la unidad familiar a hipoteca/Alquiler</p>
                <p class="result_title">Renta vivienda individual: <text class="result_subtitle">{{number_format($risk_score['hits']->renta_vivienda_individual, 2, ",", ".")}}€</text></p><p class="result_info"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Importe neto mensual destinado por el evaluado a hipoteca/Alquiler.</p>
                <p class="result_title">Morosidad: <text class="result_subtitle">{{number_format($risk_score['hits']->morosidad, 2, ",", ".")}}%</text></p><p class="result_info"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Porcentaje de la morosidad local de la zona geográfica evaluada.</p>
                <p class="result_title">Tipo de potencial morosidad: <text class="result_subtitle">{{$risk_score['hits']->tipo_moroso}}</text></p><p class="result_info"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Potencial de morosidad del evaluado por variables cualitativas.<br>1 = Fortuito.<br>2 = Negligente.<br>3 = Incompetente.<br>4 = Recurrente.<br>5 = Intencional.<br></p>
                <p class="result_title">Tasa de paro corregida: <text class="result_subtitle">{{number_format($risk_score['hits']->tasa_paro_correg, 2, ",", ".")}}%</text></p><p class="result_info"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Porcentaje de paro corregido por fuentes propias en la zona geográfica del domicilio evaluado.</p>
                <p class="result_title">Capacidad Ahorro: <text class="result_subtitle">{{number_format($risk_score['hits']->capacidad_ahorro, 2, ",", ".")}}€</text></p><p class="result_info"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Estimación de la capacidad de ahorro mensual del evaluado.</p>
                <p class="result_title">Grupo de gasto individual:</p><p class="result_info"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Potencial por grupos delgasto del evaluado.</p>
                <p class="result_title result_left">Alimentación: <text class="result_subtitle">{{number_format($risk_score['hits']->grupo_gastos_individual->alimentacion, 2, ",", ".")}}€</text></p><p class="result_info result_left"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Potencial de la capacidad de gasto en alimentación mensual neta del evaluado.</p>
                <p class="result_title result_left">Alcohol y tabaco: <text class="result_subtitle">{{number_format($risk_score['hits']->grupo_gastos_individual->alcohol_tabaco, 2, ",", ".")}}€</text></p><p class="result_info result_left"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Potencial de la capacidad de gasto en alcohol y tabaco mensual neta del evaluado.</p>
                <p class="result_title result_left">Ropa y calzado: <text class="result_subtitle">{{number_format($risk_score['hits']->grupo_gastos_individual->ropa_calzado, 2, ",", ".")}}€</text></p><p class="result_info result_left"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Potencial de la capacidad de gasto en ropa y calzado mensual neta del evaluado.</p>
                <p class="result_title result_left">Suministros vivienda: <text class="result_subtitle">{{number_format($risk_score['hits']->grupo_gastos_individual->vivienda_suministros, 2, ",", ".")}}€</text></p><p class="result_info result_left"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Potencial de la capacidad de gasto en suministros de vivienda mensual neta del evaluado.</p>
                <p class="result_title result_left">Mobiliario: <text class="result_subtitle">{{number_format($risk_score['hits']->grupo_gastos_individual->mobiliario, 2, ",", ".")}}€</text></p><p class="result_info result_left"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Potencial de la capacidad de gasto en mobiliario mensual neta del evaluado.</p>
                <p class="result_title result_left">Salud: <text class="result_subtitle">{{number_format($risk_score['hits']->grupo_gastos_individual->salud, 2, ",", ".")}}€</text></p><p class="result_info result_left"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Potencial de la capacidad de gasto en salud mensual neta del evaluado.</p>
                <p class="result_title result_left">Transporte: <text class="result_subtitle">{{number_format($risk_score['hits']->grupo_gastos_individual->transporte, 2, ",", ".")}}€</text></p><p class="result_info result_left"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Potencial de la capacidad de gasto en transporte mensual neta del evaluado.</p>
                <p class="result_title result_left">Comunicaciones: <text class="result_subtitle">{{number_format($risk_score['hits']->grupo_gastos_individual->comunicaciones, 2, ",", ".")}}€</text></p><p class="result_info result_left"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Potencial de la capacidad de gasto en comunicaciones mensual neta del evaluado.</p>
                <p class="result_title result_left">Ocio: <text class="result_subtitle">{{number_format($risk_score['hits']->grupo_gastos_individual->ocio, 2, ",", ".")}}€</text></p><p class="result_info result_left"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Potencial de la capacidad de gasto en ocio mensual neta del evaluado.</p>
                <p class="result_title result_left">Educación: <text class="result_subtitle">{{number_format($risk_score['hits']->grupo_gastos_individual->educacion, 2, ",", ".")}}€</text></p><p class="result_info result_left"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Potencial de la capacidad de gasto en educación mensual neta del evaluado.</p>
                <p class="result_title result_left">Restaurantes: <text class="result_subtitle">{{number_format($risk_score['hits']->grupo_gastos_individual->restaurantes, 2, ",", ".")}}€</text></p><p class="result_info result_left"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Potencial de la capacidad de gasto en restaurantes mensual neta del evaluado.</p>
                <p class="result_title result_left">otros: <text class="result_subtitle">{{number_format($risk_score['hits']->grupo_gastos_individual->otros, 2, ",", ".")}}€</text></p><p class="result_info"><p class="result_info result_left"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Potencial de la capacidad de gasto en otros mensual neta del evaluado.</p>
                <p class="result_title">Grupo de gasto hogar:</p><p class="result_info"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Potencial por grupos de gasto de la unidad familiar.</p>
                <p class="result_title result_left">Alimentación: <text class="result_subtitle">{{number_format($risk_score['hits']->grupo_gastos_hogar->alimentacion, 2, ",", ".")}}€</text></p><p class="result_info result_left"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Potencial de la capacidad de gasto en alimentación mensual neta del hogar evaluado.</p>
                <p class="result_title result_left">Alcohol y tabaco: <text class="result_subtitle">{{number_format($risk_score['hits']->grupo_gastos_hogar->alcohol_tabaco, 2, ",", ".")}}€</text></p><p class="result_info result_left"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Potencial de la capacidad de gasto en alcohol y tabaco mensual neta del hogar evaluado.</p>
                <p class="result_title result_left">Ropa y calzado: <text class="result_subtitle">{{number_format($risk_score['hits']->grupo_gastos_hogar->ropa_calzado, 2, ",", ".")}}€</text></p><p class="result_info result_left"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Potencial de la capacidad de gasto en ropa y calzado mensual neta del hogar evaluado.</p>
                <p class="result_title result_left">Suministros vivienda: <text class="result_subtitle">{{number_format($risk_score['hits']->grupo_gastos_hogar->vivienda_suministros, 2, ",", ".")}}€</text></p><p class="result_info result_left"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Potencial de la capacidad de gasto en suministros de la vivienda mensual neta del hogar evaluado.</p>
                <p class="result_title result_left">Mobiliario: <text class="result_subtitle">{{number_format($risk_score['hits']->grupo_gastos_hogar->mobiliario, 2, ",", ".")}}€</text></p><p class="result_info result_left"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Potencial de la capacidad de gasto en mobiliario mensual neta del hogar evaluado.</p>
                <p class="result_title result_left">Salud: <text class="result_subtitle">{{number_format($risk_score['hits']->grupo_gastos_hogar->salud, 2, ",", ".")}}€</text></p><p class="result_info result_left"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Potencial de la capacidad de gasto en salud mensual neta del hogar evaluado.</p>
                <p class="result_title result_left">Transporte: <text class="result_subtitle">{{number_format($risk_score['hits']->grupo_gastos_hogar->transporte, 2, ",", ".")}}€</text></p><p class="result_info result_left"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Potencial de la capacidad de gasto en transporte mensual neta del hogar evaluado.</p>
                <p class="result_title result_left">Comunicaciones: <text class="result_subtitle">{{number_format($risk_score['hits']->grupo_gastos_hogar->comunicaciones, 2, ",", ".")}}€</text></p><p class="result_info result_left"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Potencial de la capacidad de gasto en comunicaciones mensual neta del hogar evaluado.</p>
                <p class="result_title result_left">Ocio: <text class="result_subtitle">{{number_format($risk_score['hits']->grupo_gastos_hogar->ocio, 2, ",", ".")}}€</text></p><p class="result_info result_left"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Potencial de la capacidad de gasto en ocio mensual neta del hogar evaluado.</p>
                <p class="result_title result_left">Educación: <text class="result_subtitle">{{number_format($risk_score['hits']->grupo_gastos_hogar->educacion, 2, ",", ".")}}€</text></p><p class="result_info result_left"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Potencial de la capacidad de gasto en educación mensual neta del hogar evaluado.</p>
                <p class="result_title result_left">Restaurantes: <text class="result_subtitle">{{number_format($risk_score['hits']->grupo_gastos_hogar->restaurantes, 2, ",", ".")}}€</text></p><p class="result_info result_left"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Potencial de la capacidad de gasto en restaurantes mensual neta del hogar evaluado.</p>
                <p class="result_title result_left">otros: <text class="result_subtitle">{{number_format($risk_score['hits']->grupo_gastos_hogar->otros, 2, ",", ".")}}€</text></p><p class="result_info result_left"><img class="img_inf_result" src="/assets/img/demo/icono_info_azul_trans.png"/> Potencial de la capacidad de gasto en otros mensual neta del hogar evaluado.</p>
            @else
                {{--STATUS 400 RISK--}}
                <p class="result_title"> Status: <text class="result_subtitle"> 400 </text>  </p>
            @endif

        @else
            {{--STATUS 400 GEOCHECK--}}
            <p class="result_title"> Status: <text class="result_subtitle"> 400 </text>  </p>
        @endif

        {{--STATUS 400 IDCHECK--}}
    @else
        <p class="result_title"> Status: <text class="result_subtitle"> 400 </text>  </p>

    @endif
@endsection
@section('js')
    <script src="/assets/js/new-validar.js"></script>
    <script src="/assets/js/new-demoResponse.js"></script>
    <script src="/assets/js/bootstrap-select.js"></script>
@endsection