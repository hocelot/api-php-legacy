@extends('newApi.templates.landing')
@section('meta')
    <title>Aviso legal | Hocelot</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
@endsection
@section('css')
    <link href="/assets/css/hocelot.css" rel="stylesheet"/>
@endsection
@section('content')
<section class="col-md-12 col-xs-12">
    <div class="container">
        <div class="col-md-12 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li class="active"><a href="#">Aviso Legal</a></li>
            </ol>
            <div><h1 id="legal_msg_aviso_title"></h1></div>
            <div class="section_line_div">
                <hr class="section_line"/>
            </div>
        </div>
    </div>
</section>
<!-- Section 2-->
<section class="col-md-12 col-xs-12 section_padding_bottom">
    <div class="container">
        <div class="col-md-12 col-xs-12">
            <p id="legal_msg_aviso_text" class="section_p"></p>
        </div>
    </div>
</section>
@endsection
@section('js')
    <script src="/assets/js/new-validar.js"></script>
    <script src="/assets/js/new-contacto.js"></script>
    <script>
        jQuery.i18n.properties({
            name: 'Messages',
            path: '/assets/',
            mode: 'both',
//            async: true,
            callback: function () {
                $("[id^='legal_msg_aviso']").each(function () {
                    $(this).text(jQuery.i18n.prop($(this).attr('id')));
                });

                $("[id^='legal_img_aviso']").each(function () {
                    $(this).attr('src', '/assets/img/new-api/' + jQuery.i18n.prop($(this).attr('id')));
                });
            }
        });
    </script>
@endsection