@extends('newApi.templates.landing')
@section('meta')
    <title>Política de privacidad | Hocelot</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
@endsection
@section('css')
    <link href="/assets/css/hocelot.css" rel="stylesheet"/>
    <link href="/assets/css/new-demo.css" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">
@endsection
@section('content')
<section class="col-md-12 col-xs-12">
    <div class="container">
        <div class="col-md-12 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li class="active"><a href="#">Política de privacidad</a></li>
            </ol>
            <div><h1 id="legal_msg_priv_title"></h1></div>
            <div class="section_line_div">
                <hr class="section_line"/>
            </div>
        </div>
    </div>
</section>
<!-- Section 2-->
<section class="col-md-12 col-xs-12 section_padding_bottom">
    <div class="container">
        <div class="col-md-12 col-xs-12">
            <div><h2 id="legal_msg_priv_subtitle1" class="section_subtitle"></h2></div>
            <p id="legal_msg_priv_text1" class="section_p"></p>
            <p id="legal_msg_priv_text2" class="section_p"></p>
            <div><h2 id="legal_msg_priv_subtitle2" class="section_subtitle"></h2></div>
            <p id="legal_msg_priv_text3" class="section_p"></p>
            <p id="legal_msg_priv_text4" class="section_p"></p>
            <p id="legal_msg_priv_text5" class="section_p"></p>
            <p id="legal_msg_priv_text6" class="section_p"></p>
            <div><h2 id="legal_msg_priv_subtitle3" class="section_subtitle"></h2></div>
            <p id="legal_msg_priv_text7" class="section_p"></p>
            <div><h2 id="legal_msg_priv_subtitle4" class="section_subtitle"></h2></div>
            <p id="legal_msg_priv_text8" class="section_p"></p>
            <p id="legal_msg_priv_text9" class="section_p"></p>
            <p id="legal_msg_priv_text10" class="section_p"></p>
            <div><h2 id="legal_msg_priv_subtitle5" class="section_subtitle"></h2></div>
            <p id="legal_msg_priv_text11" class="section_p"></p>
            <div><h2 id="legal_msg_priv_subtitle6" class="section_subtitle"></h2></div>
            <p id="legal_msg_priv_text12" class="section_p"></p>
        </div>
    </div>
</section>
@endsection
@section('js')
    <script src="/assets/js/new-validar.js"></script>
    <script src="/assets/js/new-contacto.js"></script>
    <script>
        jQuery.i18n.properties({
            name: 'Messages',
            path: '/assets/',
            mode: 'both',
//            async: true,
            callback: function () {
                $("[id^='legal_msg']").each(function () {
                    $(this).text(jQuery.i18n.prop($(this).attr('id')));
                });

                $("[id^='legal_img']").each(function () {
                    $(this).attr('src', '/assets/img/new-api/' + jQuery.i18n.prop($(this).attr('id')));
                });
            }
        });
    </script>
@endsection