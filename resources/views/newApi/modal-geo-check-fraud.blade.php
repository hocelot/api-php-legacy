@if(Request::path() == 'smart-analytics')
    <div style="display: none;">{{$cont = ''}}</div>
@else
    <div style="display: none;">{{$cont = 'container'}}</div>
@endif
<section class="col-md-12 col-xs-12 section_padding_bottom80">
    <div class="{{$cont}}">
        <div class="col-md-1 visible-md-block visible-lg-block"></div>
        <div class="col-md-8 ">
            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li><a href="/smart-data">Smart Analytics</a></li>
                <li class="active"><a href="#">Geo Check & Fraud</a></li>
            </ol>
            <h1>Geo Check & Fraud</h1>
            <div class="section_line_div">
                <hr class="section_line"/>
            </div>
            <p id="analytics_msg_services2_modal_inlet_description" class="title_section1 text-left"></p>
            <div class="link_services section_margin_top">
                <button id="analytics_msg_services2_modal_inlet_btn" class="link_style" onclick="window.location.href='/demo'"></button>
            </div>
        </div>
        <div class="col-md-3 visible-md-block visible-lg-block"></div>
    </div>
</section>
<section class="col-md-12 col-xs-12 section_padding_bottom80">
    <div class="{{$cont}}">
        <div class="col-md-1 visible-md-block visible-lg-block"></div>
        <div class="col-md-8">
            <div class="col-md-12 col-xs-12 section_padding no_padding">
                <div class="col-md-12 col-xs-12 no_padding">
                    <div>
                        <h4 id="analytics_msg_services2_modal_section1_title" class="section_subtitle"></h4>
                    </div>
                    <div class="section_line_div">
                        <hr class="section_line">
                    </div>
                    <div>
                        <p id="analytics_msg_services2_modal_section1_description" class="section_p"></p>
                    </div>
                </div>
                <div class="col-md-12 col-xs-12 no_padding">
                    <div>
                        <h4 id="analytics_msg_services2_modal_section2_title" class="section_subtitle"></h4>
                    </div>
                    <div class="section_line_div">
                        <hr class="section_line">
                    </div>
                    <div>
                        <p id="analytics_msg_services2_modal_section2_description" class="section_p"></p>
                    </div>
                </div>
                <div class="col-md-12 col-xs-12 no_padding">
                    <div>
                        <h4 id="analytics_msg_services2_modal_section3_title" class="section_subtitle"></h4>
                    </div>
                    <div class="section_line_div">
                        <hr class="section_line">
                    </div>
                    <div>
                        <p id="analytics_msg_services2_modal_section3_description" class="section_p"></p>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-xs-12 no_padding">
                <p class="section_margin section6_text" id="analytics_msg_services2_modal_paragraph"></p>
            </div>
            <div class="col-md-12 col-xs-12">
                <button id="analytics_msg_services2_modal_btn" class="btn button_section6" onclick="window.location.href='/contrata'"></button>
            </div>
        </div>
        <div class="col-md-3 visible-md-block visible-lg-block"></div>
    </div>
</section>