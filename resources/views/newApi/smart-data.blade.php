@extends('newApi.templates.landing')
@section('meta')
    <title>Smart Data Soluciones | Hocelot</title>
    <meta name="description" content="Usa nuestros servicios de Smart Data para agilizar tus formularios, evitar abandonos en tus registros, normalizar y mejorar la información que posees de tus clientes y enriquecer con nuevas variables las que ya tienes.">
    <meta name="keywords" content="Smart Data, evitar abandonos, normalizar, enriquecer">
@endsection
@section('content')
    <section class="col-md-12 col-xs-12 section_padding_bottom80">
        <div class="container">
            <div class="col-md-1 visible-md-block visible-lg-block"></div>
            <div class="col-md-8">
                <ol class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    <li class="active"><a href="#">Smart Data</a></li>
                </ol>
                <h1>Smart Data</h1>
                <div class="section_line_div">
                    <hr class="section_line"/>
                </div>
                <p id="smartdata_msg_inlet" class="title_section1 text-left"></p>
                <div class="link_services">
                    <button id="smartdata_msg_inlet_button" class="link_style" onclick="window.location.href='/demo'"></button>
                </div>
            </div>
            <div class="col-md-3 visible-md-block visible-lg-block"></div>
        </div>
    </section>
    <!-- Section 2-->
    <section class="col-md-12 col-xs-12 section_background section_padding">
        <div class="col-md-12 col-xs-12">
            <h4 class="section_title text-center">Servicios</h4>
        </div>
        <div class="container">
            <div class="col-md-12 col-xs-12">
                <div class="col-md-4 col-xs-12 section_background_white section_margin_right section_responsive_margin_bottom no_padding section_responsive_no_margins margin-left20 similar_height_data">
                    <div class="col-md-12 col-xs-12 no_padding">
                        <img id="smartdata_img_services1_src" alt="Smooth Client Onboarding icon" class="full_width" src="">
                    </div>
                    <div class="col-md-12 col-xs-12 section_padding_all">
                        <div><h2 id="smartdata_msg_services1_title" class="section_subtitle"></h2></div>
                        <div class="section_line_div">
                            <hr class="section_line"/>
                        </div>
                        <p id="smartdata_msg_services1_description" class="section_p"></p>
                        <div class="link_services section_margin_top">
                            {{--<button id="smartdata_msg_services1_button" class="link_style" onclick="window.location.href='/smart-data/smooth-client-onboarding'"/>--}}
                            <button id="smartdata_msg_services1_button" class="link_style" data-toggle="modal" data-target="#smooth-client-onboarding-modal"></button>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12 section_background_white section_margin_right section_margin_left section_responsive_margin_bottom no_padding section_responsive_no_margins margin-left10 similar_height_data">
                    <div class="col-md-12 col-xs-12 no_padding">
                        <img id="smartdata_img_services2_src" alt="Standardization & Enhacement icon" class="full_width" src="">
                    </div>
                    <div class="col-md-12 col-xs-12 section_padding_all ">
                        <div><h2 id="smartdata_msg_services2_title" class="section_subtitle"></h2></div>
                        <div class="section_line_div">
                            <hr class="section_line"/>
                        </div>
                        <p id="smartdata_msg_services2_description" class="section_p"></p>
                        <div class="link_services section_margin_top">
                            {{--<button id="smartdata_msg_services2_button" class="link_style" onclick="window.location.href='/smart-data/standardization-enhacement'"/>--}}
                            <button id="smartdata_msg_services2_button" class="link_style" data-toggle="modal" data-target="#standardization-enhacement-modal"></button>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12 section_background_white section_margin_left section_responsive_margin_bottom no_padding section_responsive_no_margins margin-right20 similar_height_data" style="margin-right: -20px;">
                    <div class="col-md-12 col-xs-12 no_padding">
                        <img id="smartdata_img_services3_src" alt="Enrichment icon" class="full_width" src="">
                    </div>
                    <div class="col-md-12 col-xs-12 section_padding_all">
                        <div><h2 id="smartdata_msg_services3_title" class="section_subtitle"></h2></div>
                        <div class="section_line_div">
                            <hr class="section_line"/>
                        </div>
                        <p id="smartdata_msg_services3_description" class="section_p"></p>
                        <div class="link_services section_margin_top">
                            {{--<button id="smartdata_msg_services3_button" class="link_style" onclick="window.location.href='/smart-data/enrichment'"/>--}}
                            <button id="smartdata_msg_services3_button" class="link_style" data-toggle="modal" data-target="#enrichment-modal"></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Section 6-->
    <section class="col-md-12 col-xs-12 text-center section_double_padding abstract_background">
        <div class="col-md-12 col-xs-12">
            <div><h3 id="smartdata_msg_highlighted1_title" class="section_padding_bottom section_big_title"></h3></div>
            <div>
                <button id="smartdata_msg_highlighted1_button" class="btn button_section6 btn_transparent" onclick="window.location.href='/contrata'"></button>
            </div>
        </div>
    </section>
    <section class="col-md-12 col-xs-12 text-center section_double_padding">
        <div class="col-md-12 col-xs-12">
            <div><h4 id="smartdata_msg_highlighted2_title" class="section_big_title section_padding_bottom"></h4></div>
            <div>
                <button id="smartdata_msg_highlighted2_button" class="link_style section_bottom" onclick="window.location.href='/smart-analytics'"></button>
            </div>
        </div>
    </section>
<!-- Modals -->
    <div id="smooth-client-onboarding-modal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    @include('newApi.modal-smooth-client-onboarding')
                </div>
            </div>
        </div>
    </div>
    <div id="standardization-enhacement-modal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    @include('newApi.modal-standardization-enhacement')
                </div>
            </div>
        </div>
    </div>
    <div id="enrichment-modal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    @include('newApi.modal-enrichment')
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        jQuery.i18n.properties({
            name: 'Messages',
            path: '/assets/',
            mode: 'both',
//            async: true,
            callback: function () {
                $("[id^='smartdata_msg']").each(function () {
                    $(this).text(jQuery.i18n.prop($(this).attr('id')));
                });

                $("[id^='smartdata_img']").each(function () {
                    if($(window).width() < '780') {
                        $(this).attr('src', '/assets/img/new-api/' + jQuery.i18n.prop($(this).attr('id')+'_1x'));
                    }else if($(window).width() > '780' && $(window).width() < '1024'){
                        $(this).attr('src', '/assets/img/new-api/' + jQuery.i18n.prop($(this).attr('id')+'_2x'));
                    }else{
                        $(this).attr('src', '/assets/img/new-api/' + jQuery.i18n.prop($(this).attr('id')+'_3x'));
                    }
                });
            }
        });
    </script>
@endsection
@yield('js-modal')