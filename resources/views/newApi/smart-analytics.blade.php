@extends('newApi.templates.landing')
@section('meta')
    <title>Smart Analytics Soluciones | Hocelot</title>
    <meta name="description" content="Usa nuestras soluciones de Smart Analytics para reducir costes, apoyar los procesos de decisión y analizar toda la información en tiempo real. Si no analizas la información, no sirve de nada.">
    <meta name="keywords" content="Smart Analytics, reducir costes, analizar informacion, tiempo real">
@endsection
@section('content')
    <section class="col-md-12 col-xs-12 section_padding_bottom80">
        <div class="container">
            <div class="col-md-1 visible-md-block visible-lg-block"></div>
            <div class="col-md-8">
                <ol class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    <li class="active"><a href="#">Smart Analytics</a></li>
                </ol>
                <h1>Smart Analytics</h1>
                <div class="section_line_div">
                    <hr class="section_line"/>
                </div>
                <p id="analytics_msg_inlet" class="title_section1 text-left"></p>
                <div class="link_services">
                    <button id="analytics_msg_inlet_button" class="link_style" onclick="window.location.href='/demo'"></button>
                </div>
            </div>
            <div class="col-md-3 visible-md-block visible-lg-block"></div>
        </div>
    </section>
    <!-- Section 2-->
    <section class="col-md-12 col-xs-12 section_background section_padding">
        <div class="col-md-12 col-xs-12">
            <h4 class="section_title text-center">Servicios</h4>
        </div>
        <div class="container">
            <div class="col-md-12 col-xs-12">
                <div class="col-md-4 col-xs-12 section_background_white section_margin_right section_responsive_margin_bottom no_padding section_responsive_no_margins margin-left20 similar_height_analy">
                    <div class="col-md-12 col-xs-12 no_padding">
                        <img id="analytics_img_services1_src" src="" alt="id check & fraud icon" class="full_width">
                    </div>
                    <div class="col-md-12 col-xs-12 section_padding_all">
                        <div><h2 id="analytics_msg_services1_title" class="section_subtitle"></h2></div>
                        <div class="section_line_div">
                            <hr class="section_line"/>
                        </div>
                        <p id="analytics_msg_services1_description" class="section_p"></p>
                        <div class="link_services section_margin_top">
                            <button id="analytics_msg_services1_button" class="link_style" data-toggle="modal" data-target="#id-check-fraud-modal"></button>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12 section_background_white section_margin_right section_margin_left section_responsive_margin_bottom no_padding section_responsive_no_margins margin-left10 similar_height_analy">
                    <div class="col-md-12 col-xs-12 no_padding">
                        <img id="analytics_img_services2_src" src="" alt="geo check & fraud icon" class="full_width">
                    </div>
                    <div class="col-md-12 col-xs-12 section_padding_all">
                        <div><h2 id="analytics_msg_services2_title" class="section_subtitle"></h2></div>
                        <div class="section_line_div">
                            <hr class="section_line"/>
                        </div>
                        <p id="analytics_msg_services2_description" class="section_p"></p>
                        <div class="link_services section_margin_top">
                            <button id="analytics_msg_services2_button" class="link_style" data-toggle="modal" data-target="#geo-check-fraud-modal"></button>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12 section_background_white section_margin_left section_responsive_margin_bottom no_padding section_responsive_no_margins margin-right20 similar_height_analy">
                    <div class="col-md-12 col-xs-12 no_padding">
                        <img id="analytics_img_services3_src" src="" alt="risk score icon" class="full_width">
                    </div>
                    <div class="col-md-12 col-xs-12 section_padding_all">
                        <div><h2 id="analytics_msg_services3_title" class="section_subtitle"></h2></div>
                        <div class="section_line_div">
                            <hr class="section_line"/>
                        </div>
                        <p id="analytics_msg_services3_description" class="section_p"></p>
                        <div class="link_services section_margin_top">
                            <button id="analytics_msg_services3_button" class="link_style" data-toggle="modal" data-target="#risk-score-modal"></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="col-md-12 col-xs-12 text-center section_double_padding abstract_background">
        <div class="col-md-12 col-xs-12">
            <div><h3 id="analytics_msg_highlighted1_title" class="section_padding_bottom section_big_title"></h3></div>
            <div>
                <button id="analytics_msg_highlighted1_button" class="btn button_section6 btn_transparent" onclick="window.location.href='/contrata'"></button>
            </div>
        </div>
    </section>
    <section class="col-md-12 col-xs-12 text-center section_double_padding">
        <div class="col-md-12 col-xs-12">
            <div><h4 id="analytics_msg_highlighted2_title" class="section_big_title section_padding_bottom"></h4></div>
            <div>
                <button id="analytics_msg_highlighted2_button" class="link_style section_bottom" onclick="window.location.href='/smart-data'"></button>
            </div>
        </div>
    </section>
    <!-- Modals -->
    <div id="id-check-fraud-modal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    @include('newApi.modal-id-check-fraud')
                </div>
            </div>
        </div>
    </div>
    <div id="geo-check-fraud-modal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    @include('newApi.modal-geo-check-fraud')
                </div>
            </div>
        </div>
    </div>
    <div id="risk-score-modal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    @include('newApi.modal-risk-score')
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        jQuery.i18n.properties({
            name: 'Messages',
            path: '/assets/',
            mode: 'both',
//            async: true,
            callback: function () {
                $("[id^='analytics_msg']").each(function () {
                    $(this).text(jQuery.i18n.prop($(this).attr('id')));
                });

                $("[id^='analytics_img']").each(function () {
                    if($(window).width() < '780') {
                        $(this).attr('src', '/assets/img/new-api/' + jQuery.i18n.prop($(this).attr('id')+'_1x'));
                    }else if($(window).width() > '780' && $(window).width() < '1024'){
                        $(this).attr('src', '/assets/img/new-api/' + jQuery.i18n.prop($(this).attr('id')+'_2x'));
                    }else{
                        $(this).attr('src', '/assets/img/new-api/' + jQuery.i18n.prop($(this).attr('id')+'_3x'));
                    }
                });
            }
        });
    </script>
@endsection