@extends('newApi.templates.landing')
@section('meta')
    <title>Demo | Hocelot</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
@endsection
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/css/bootstrap-select.min.css">
    <link href="/assets/css/new-demo.css" rel="stylesheet"/>
@endsection
@section('content')
<section class="col-md-12 col-xs-12 section_padding_bottom80">
    <div class="container">
        <div class="col-md-1 visible-md-block visible-lg-block"></div>
        <div class="col-md-8">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    <li class="active"><a href="#">Demo</a></li>
                </ol>
                <p class="title_section1 text-left" id="demo_msg_title"></p>
                <p class="landing_desc no_margin_desc" id="demo_msg_subtitle1"></p>
                <p class="landing_desc no_margin_desc" id="demo_msg_subtitle2"></p>
                <p class="landing_desc no_margin_desc" id="demo_msg_subtitle3"></p>
                <div class="section_line_div">
                    <hr class="section_line">
                </div>
            </div>
        </div>
        <div class="col-md-3 visible-md-block visible-lg-block min_height_demo"></div>
        <div class="col-md-1 visible-md-block visible-lg-block"></div>
        <div class="col-md-10">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <p class="landing_desc no_margin_desc" id="demo_msg_sexo_title"></p>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <input name="sexo" id="sexoH" value="H" type="radio" checked> <label class="label_sex" for="sexoH" id="demo_msg_sexo_label_H"></label>
                <input name="sexo" id="sexoM" value="M" type="radio"> <label class="label_sex" for="sexoM" id="demo_msg_sexo_label_M"></label>
            </div>
            <input type="hidden" id="code" value="">
            <input type="hidden" id="zip_type" value="0">
            <input type="hidden" id="rt_id" value="">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <p class="landing_desc" id="demo_msg_name_title"></p>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="placeMin" style="height:5px"><label for="nombre" id="placenombre" style="display:none">Nombre</label></div>
                <input tabindex="1" type="text" class="form-control input_label" placeholder="Nombre" id="nombre" onBlur="checkInput(this)" onFocus="infoInput(this)" onkeyup="getPlaceMin(this)">
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="placeMin" style="height:5px"><label for="primer_apellido" id="placeprimer_apellido" style="display:none">Primer apellido</label></div>
                <input tabindex="2" type="text" class="form-control input_label" placeholder="Primer apellido" id="primer_apellido" onBlur="checkInput(this)" onFocus="infoInput(this)" onkeyup="getPlaceMin(this)">
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="placeMin" style="height:5px"><label for="segundo_apellido" id="placesegundo_apellido" style="display:none">Segundo apellido</label></div>
                <input tabindex="3" type="text" class="form-control input_label" placeholder="Segundo apellido" id="segundo_apellido" onBlur="checkInput(this)" onFocus="infoInput(this)" onkeyup="getPlaceMin(this)">
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <p class="landing_desc" id="demo_msg_fecha_title"></p>
                <div class="placeMin" style="height:5px"><label for="date_nac" id="placedate_nac" style="display:none">Fecha de nacimiento</label></div>
                <input tabindex="4" type="text" class="form-control input_label" placeholder="DD / MM / AAAA" id="date_nac" maxlength="10" onBlur="checkFech(this); checkMsg(this, 'demo_msg_fecha_desc')" onFocus="infoFech(this)" onkeyup="getPlaceMin(this);">
            </div>
            <div class="visible-md-block visible-lg-block col-md-6 height_info">
                <p class="desc_form" id="demo_msg_fecha_desc"></p>
                <p class="desc_form" id="demo_msg_fecha_desc_error" style="display:none"></p>
            </div>
            <div class="visible-sm-block visible-xs-block col-sm-12 col-xs-12 height_info_mobile">
                <p class="desc_form" id="demo_msg_fecha_desc_mov"></p>
                <p class="desc_form" id="demo_msg_fecha_desc_mov_error" style="display:none"></p>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <p class="landing_desc" id="demo_msg_doc_title"></p>
                <div class="placeMin" style="height:5px"><label for="NIF" id="placeNIF" style="display:none">NIF/NIE</label></div>
                <input tabindex="5" type="text" class="form-control input_label" placeholder="NIF/NIE" id="NIF" onBlur="checkDoc(this);checkMsg(this,'demo_msg_doc_desc') " onFocus="infoDoc(this)" onkeyup="getPlaceMin(this)">
            </div>
            <div class="visible-md-block visible-lg-block col-md-6 height_info">
                <p class="desc_form" id="demo_msg_doc_desc"></p>
                <p class="desc_form" id="demo_msg_doc_desc_error" style="display:none"></p>
            </div>
            <div class="visible-sm-block visible-xs-block col-sm-12 col-xs-12 height_info_mobile">
                <p class="desc_form" id="demo_msg_doc_desc_mov"></p>
                <p class="desc_form" id="demo_msg_doc_desc_mov_error" style="display:none"></p>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <p class="landing_desc" id="demo_msg_dir_title"></p>
            </div>
             <div class="col-md-4 col-sm-6 col-xs-12">
                 <div class="placeMin" style="height:5px"><label for="del_codpos" id="placedel_codpos" style="display:none">Código postal</label></div>
                <input tabindex="6" type="text" class="form-control input_label" id="del_codpos" placeholder="Código postal" onBlur="checkInt(this)" onFocus="infoInt(this)" maxlength="5" onkeyup="getPlaceMin(this)">
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="placeMin" style="height:5px"><label for="poblacion" id="placepoblacion" style="display:none">Población</label></div>
                <select class="selectpicker form-control input_label" id="poblacion" onChange="checkSelSex(this)" data-live-search="true" title="Población" disabled>
                </select>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="placeMin" style="height:5px"><label for="provincia" id="placeprovincia" style="display:none">Provincia</label></div>
                <input type="text" class="form-control input_label" id="provincia" placeholder="Provincia" onChange="checkInput(this); getPlaceMin(this)" disabled>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="placeMin" style="height:5px"><label for="tipo_via" id="placetipo_via" style="display:none">Tipo de vía</label></div>
                <select tabindex="7" class="selectpicker form-control input_label" id="tipo_via" onChange="checkSelSex(this)" data-live-search="true">
                    <option value="" disabled selected> Tipo de vía</option>
                </select>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="placeMin" style="height:5px"><label for="via" id="placevia" style="display:none">Nombre de vía</label></div>
                <select tabindex="8" class="selectpicker form-control input_label" id="via" onChange="checkSelSex(this)" data-live-search-normalize="true" data-min="minLenght" data-live-search="true" data-live-Search-Placeholder="Introduce al menos 3 caracteres">
                    <option value="" disabled selected> Nombre de vía</option>
                </select>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="placeMin" style="height:5px"><label for="numero" id="placenumero" style="display:none">Numero</label></div>
                <select tabindex="9" class="selectpicker form-control input_label" id="numero" onChange="checkSelSex(this)" data-live-search="true">
                    <option value="" disabled selected> Número</option>
                </select>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12" id="div_sel_letra" style="display:none">
                <div class="placeMin" style="height:5px"><label for="letra" id="placeletra" style="display:none">Letra</label></div>
                <select tabindex="10" class="selectpicker form-control input_label" id="letra" onChange="checkSelSex(this)" data-live-search="true" disabled>
                    <option value="" disabled selected> Letra</option>
                </select>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12" id="div_sel_km" style="display:none">
                <div class="placeMin" style="height:5px"><label for="km" id="placekm" style="display:none">Kilómetro</label></div>
                <select tabindex="11" class="selectpicker form-control input_label" id="km" onChange="checkSelSex(this)" data-live-search="true" disabled>
                    <option value="" disabled selected> Kilómetro</option>
                </select>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12" id="div_sel_bloque" style="display:none">
                <div class="placeMin" style="height:5px"><label for="bloque" id="placebloque" style="display:none">Bloque</label></div>
                 <select tabindex="12" class="selectpicker form-control input_label" id="bloque" onChange="checkSelSex(this)" data-live-search="true" disabled>
                    <option value="" disabled selected> Bloque</option>
                </select>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12" id="div_sel_escalera" style="display:none">
                <div class="placeMin" style="height:5px"><label for="escalera" id="placeescalera" style="display:none">Escalera</label></div>
                 <select tabindex="13" class="selectpicker form-control input_label" id="escalera" onChange="checkSelSex(this)" data-live-search="true" disabled>
                    <option value="" disabled selected> Escalera</option>
                </select>
            </div>
             <div class="col-md-4 col-sm-4 col-xs-12" id="div_sel_piso" style="display:none">
                 <div class="placeMin" style="height:5px"><label for="piso" id="placepiso" style="display:none">Piso</label></div>
                 <select tabindex="14" class="selectpicker form-control input_label" id="piso" onChange="checkSelSex(this)" data-live-search="true" disabled>
                    <option value="" disabled selected> Piso</option>
                </select>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 " id="div_sel_puerta" style="display:none">
                <div class="placeMin" style="height:5px"><label for="puerta" id="placepuerta" style="display:none">Puerta</label></div>
                 <select tabindex="15" class="selectpicker form-control input_label" id="puerta" onChange="checkSelSex(this)" data-live-search="true" disabled>
                    <option value="" disabled selected> Puerta</option>
                </select>
            </div>
            <input type="hidden" id="api_key_true" value="">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <p class="landing_desc" id="demo_msg_contact_title"></p>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 height_divkey">
                <input type="text" class="form-control input_label" placeholder="Número de teléfono móvil" id="telefono" maxlength="9" onBlur="checkTelf(this)" onFocus="infoTelf(this)">
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 height_divkey">
                <input type="text" class="form-control input_label" placeholder="Email" id="email" onBlur="checkEmail(this)" onFocus="infoEmail(this)" onkeyup="getPlaceMin(this)">
            </div>
            <div class="col-md-4 visible-md-block visible-lg-block height_input"></div>
            <div class="col-md-12 col-sm-12 col-xs-12 section_margin_top section_margin_bottom20">
                <p class="p-destacado" id="demo_msg_clave_title"></p>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 section_margin_bottom20">
                <button class="disabled_button button_small float-left" id="demo_msg_button_key" disabled></button>
                <div class="div_sms_left float-left" id="sms_div_info"></div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <input type="password" class="form-control input_label" onblur="checkInt(this)" id="api_key" maxlength="5" placeholder="Clave de 5 cifras">
            </div>
            <input type="hidden" id="secureApiKey">
            <div class="col-md-8 col-sm-6 col-xs-12">
                <p id="text_cod_sms" class="landing_desc_text text_padding_top">
                    <text id="demo_msg_clave_desc"></text>
                    <button class="resend_key" id="demo_msg_resendKey"></button>
                </p>
                <p id="text_cod_sms_error" class="landing_desc_text text_padding_top" style="color:#f9315b; display:none">
                    <text id="demo_msg_clave_desc_error"></text>
                    <button class="resend_key" id="demo_msg_resendKeyError" style="color:#f9315b"></button>
                </p>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 div_cond">
                <p class="error_contact text-left" id="error_acepto_cond"></p>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 div_cond">
                <input class="float_right" type="checkbox" name="acepto_cond" tabindex="20" id="acepto_cond" onClick="checkCheckbox(this)"/>
                <label class="condiciones" id="label_check" for="checkbox" > Acepto los <a href="/condiciones" target="_blank">Términos y Condiciones de Uso</a> y la <a href="/politica-de-privacidad" target="_blank">Política de Privacidad de Datos </a>.</label>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="margin_auto">
                <input type="button" class="disabled_button button_bigest section_margin_top" id="submit_button" value="Ver resultados" disabled>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="col-md-12 col-xs-12 section_padding_bottom">
    <div class="container">
        <h1 class="section_big_title section_padding_bottom text-center" id="demo_msg_final"></h1>
    </div>
</section>
<div class="modal fade" id="loadingModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-content">
        <div id="loading">
            <img src="/assets/img/new-api/loading.gif"/>
        </div>
    </div>
</div>
<div class="modal fade" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-header modal-contact">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <section class="col-md-12 col-xs-12 modal-contact section_padding_bottom">
            <div class="col-md-1 visible-md-block visible-lg-block height_input"></div>
            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                <h1 id="demo_msg_modal_error_title" style="max-width:474px; margin: 0 auto 35px;"></h1>
                <p class="p-destacado" id="ModalErrorBody" style="max-width: 621px; margin: 0 auto;"></p>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="margin_auto_cons section_margin_top div_boton_seguridad">
                    <input id="getSmoothContact" class="boton_consultar boton_seguridad" value="Contacta con nosotros" type="button">
                </div>
            </div>
        </section>
    </div>
</div>
<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-header modal-contact">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <section class="col-md-12 col-xs-12 modal-contact section_padding_bottom" id="ModalInfoBody">
            <div class="col-md-1 visible-md-block visible-lg-block height_input"></div>
            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                <h1 id="demo_msg_modal_secure_title" style="max-width:474px; margin: 0 auto 35px;"></h1>
                <p id="demo_msg_modal_secure_desc" class="p-destacado" style="max-width: 621px; margin: 0 auto;"></p>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="margin_auto_cons section_margin_top div_boton_seguridad">
                    <input id="getSmoothContact" class="boton_consultar boton_seguridad" value="Contacta con nosotros" type="button">
                </div>
            </div>
        </section>
    </div>
</div>
<div class="modal fade" id="infoModalMovil" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-header modal-contact">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <section class="col-md-12 col-xs-12 modal-contact section_padding_bottom" id="ModalInfoBody">
            <div class="col-md-1 visible-md-block visible-lg-block height_input"></div>
            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                <h1 id="demo_msg_modal_secure_title" style="max-width:474px; margin: 0 auto 35px;"></h1>
                <p id="demo_msg_modal_secure_desc" class="p-destacado" style="max-width: 621px; margin: 0 auto;"></p>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="margin_auto_cons section_margin_top div_boton_seguridad">
                    <input id="getSmoothContact2" class="boton_consultar boton_seguridad" value="Contacta con nosotros" type="button">
                </div>
            </div>
        </section>
    </div>
</div>
<div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-header modal-contact">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <section class="col-md-12 col-xs-12 modal-contact section_padding_bottom" id="ModalContactBody">
            <div class="col-md-1 visible-md-block visible-lg-block height_input"></div>
            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                <h1 id="demo_msg_modal_smsError_title"></h1>
                <p id="demo_msg_modal_smsError_desc" class="p-destacado"></p>
                <p class="error_contact" id="error_text"></p>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="placeMin" style="height:5px"><text id="placenombreCon">Nombre</text></div>
                <input type="text" class="form-control input_label" value="" placeholder="Nombre" id="nombreCon" onBlur="checkInput(this)" onFocus="infoInput(this)" onkeyup="getPlaceMin(this)">
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="placeMin" style="height:5px"><text id="placeapellidosCon">Apellidos</text></div>
                <input type="text" class="form-control input_label" value="" placeholder="Apellidos" id="apellidosCon" onBlur="checkInput(this)" onFocus="infoInput(this)" onkeyup="getPlaceMin(this)">
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="placeMin" style="height:5px"><text id="placecargoCon" style="display:none">Cargo</text></div>
                <input type="text" class="form-control input_label" placeholder="Cargo" id="cargoCon" onBlur="checkInput(this)" onFocus="infoInput(this)" onkeyup="getPlaceMin(this)">
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="placeMin" style="height:5px"><text id="placenombre_empCon" style="display:none">Nombre de la empresa</text></div>
                <input type="text" class="form-control input_label" placeholder="Nombre de la empresa" id="nombre_empCon" onBlur="checkInput(this)" onFocus="infoInput(this)" onkeyup="getPlaceMin(this)">
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="placeMin" style="height:5px"><text id="placeemailCon">Email</text></div>
                <input type="text" class="form-control input_label" value="" placeholder="Email" id="emailCon" onBlur="checkEmail(this)" onFocus="infoEmail(this)" onkeyup="getPlaceMin(this)">
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="placeMin" style="height:5px"><text id="placetelefonoCon">Teléfono</text></div>
                <input type="text" class="form-control input_label" value="" placeholder="Teléfono" id="telefonoCon" maxlength="9" onBlur="checkTelf(this)" onFocus="infoTelf(this)" onkeyup="getPlaceMin(this)">
            </div>
            <div class="col-md-3 visible-md-block visible-lg-block height_input"></div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="margin_auto_cons section_margin_top button_margin_bottom">
                    <input type="button" class="boton_consultar" id="return_button" value="Volver" data-dismiss="modal">
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="margin_auto_cons section_margin_top button_margin_bottom">
                    <input type="button" class="boton_consultar" id="submit_contact" value="Enviar">
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <p class="" id="demo_msg_modal_disclaimer"></p>
            </div>
        </section>
    </div>
</div>
@endsection
@section('js')
    <script src="/assets/js/new-validar.js"></script>
    <script src="/assets/js/new-demo.js"></script>
    <script src="/assets/js/bootstrap-select.js"></script>
    <script>
        jQuery.i18n.properties({
            name: 'Messages',
            path: '/assets/',
            mode: 'both',
            async: true,
            callback: function () {
                $("[id^='demo_msg']").each(function () {
                    $(this).text(jQuery.i18n.prop($(this).attr('id')));
                });
            }
        });
    </script>
@endsection