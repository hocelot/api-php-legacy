@extends('newApi.templates.landing')
@section('meta')
    <title>Sobre Nosotros | Hocelot</title>
    <meta name="description" content="Hocelot surge de la búsqueda de soluciones fiables a problemas que nos hemos ido encontrando. Nuestra propia experiencia es la que nos lleva a crear un servicio que creemos útil y necesario para mejorar los negocios de nuestros clientes.">
    <meta name="keywords" content="soluciones fiables, util, clientes">
@endsection
@section('css')
    <link href="/assets/css/about.css" rel="stylesheet"/>
@endsection
@section('content')
    <section class="col-md-12 col-xs-12 section_padding_bottom80">
        <div class="container">
            <div class="col-md-1 visible-md-block visible-lg-block"></div>
            <div class="col-md-8">
                <ol class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    <li class="active"><a href="#">Sobre nosotros</a></li>
                </ol>
                <h1>Sobre nosotros</h1>
                <div class="section_line_div">
                    <hr class="section_line"/>
                </div>
                <p id="about_msg_inlet" class="title_section1 text-left"></p>
            </div>
            <div class="col-md-3 visible-md-block visible-lg-block"></div>
        </div>
    </section>
    <section class="col-md-12 col-xs-12 section_background section_padding">
        <div class="col-md-12 col-xs-12">
            <h3 class="section_title text-center">Equipo directivo</h3>
        </div>
        <div class="container">
            <div class="col-md-12 col-xs-12">
                <div class="col-md-4 col-xs-12 section_background_white section_margin_right section_responsive_margin_bottom no_padding section_responsive_no_margins margin-left20 similar_height_about">
                    <div class="col-md-12 col-xs-12 no_padding">
                        <img id="about_img_col1_src" class="full_width" src="">
                    </div>
                    <div class="col-md-12 col-xs-12 section_padding_all">
                        <div><h4 id="about_msg_col1_title" class="section_subtitle"></h4></div>
                        <div class="section_line_div">
                            <hr class="section_line"/>
                        </div>
                        <p id="about_msg_col1_subtitle" class="section_p"></p>
                        <p id="about_msg_col1_description" class="section_p"></p>
                    </div>
                </div>

                <div class="col-md-4 col-xs-12 section_background_white section_margin_right section_margin_left section_responsive_margin_bottom no_padding section_responsive_no_margins margin-left10 similar_height_about">
                    <div class="col-md-12 col-xs-12 no_padding">
                        <img id="about_img_col2_src" class="full_width" src="">
                    </div>
                    <div class="col-md-12 col-xs-12 section_padding_all">
                        <div><h4 id="about_msg_col2_title" class="section_subtitle"></h4></div>
                        <div class="section_line_div">
                            <hr class="section_line"/>
                        </div>
                        <p id="about_msg_col2_subtitle" class="section_p"></p>
                        <p id="about_msg_col2_description" class="section_p"></p>
                    </div>
                </div>

                <div class="col-md-4 col-xs-12 section_background_white section_margin_left section_responsive_margin_bottom no_padding section_responsive_no_margins margin-right20 similar_height_about" style="margin-right: -20px;">
                    <div class="col-md-12 col-xs-12 no_padding">
                        <img id="about_img_col3_src" class="full_width" src="">
                    </div>
                    <div class="col-md-12 col-xs-12 section_padding_all">
                        <div><h4 id="about_msg_col3_title" class="section_subtitle"></h4></div>
                        <div class="section_line_div">
                            <hr class="section_line"/>
                        </div>
                        <p id="about_msg_col3_subtitle" class="section_p"></p>
                        <p id="about_msg_col3_description" class="section_p"></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="col-md-12 col-xs-12 text-center section_double_padding abstract_background">
        <div class="col-md-12 col-xs-12">
            <div><h3 id="about_msg_highlighted2_title" class="section_big_title section_padding_bottom"></h3></div>
            <div>
                <a class="btn button_section6 btn_transparent" href="mailto:info@hocelot.com?Subject=Contacto%20Hocelot" target="_top">CONTACTA CON NOSOTROS</a>
            </div>
        </div>
    </section>
@endsection
@section('js')
    <script>
        jQuery.i18n.properties({
            name: 'Messages',
            path: '/assets/',
            mode: 'both',
//            async: true,
            callback: function () {
                $("[id^='about_msg']").each(function () {
                    $(this).text(jQuery.i18n.prop($(this).attr('id')));
                });

                $("[id^='about_img']").each(function () {
                    if($(window).width() < '780') {
                        $(this).attr('src', '/assets/img/new-api/' + jQuery.i18n.prop($(this).attr('id')+'_1x'));
                    }else if($(window).width() > '780' && $(window).width() < '1024'){
                        $(this).attr('src', '/assets/img/new-api/' + jQuery.i18n.prop($(this).attr('id')+'_2x'));
                    }else{
                        $(this).attr('src', '/assets/img/new-api/' + jQuery.i18n.prop($(this).attr('id')+'_3x'));
                    }
                });
            }
        });
    </script>
@endsection