@section('meta')
    <title>Smooth Client Onboarding | Hocelot</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
@endsection
@extends('newApi.templates.landing')
@section('content')
@include('newApi.modal-smooth-client-onboarding')
@endsection
@section('js')
<script>
    jQuery.i18n.properties({
        name: 'Messages',
        path: '/assets/',
        mode: 'both',
//        async: true,
        callback: function () {
            $("[id^='smartdata_msg']").each(function () {
                $(this).text(jQuery.i18n.prop($(this).attr('id')));
            });
            $("[id^='smartdata_img']").each(function () {
                if($(window).width() < '780') {
                    $(this).attr('src', '/assets/img/new-api/' + jQuery.i18n.prop($(this).attr('id')+'_1x'));
                }else if($(window).width() > '780' && $(window).width() < '1024'){
                    $(this).attr('src', '/assets/img/new-api/' + jQuery.i18n.prop($(this).attr('id')+'_2x'));
                }else{
                    $(this).attr('src', '/assets/img/new-api/' + jQuery.i18n.prop($(this).attr('id')+'_3x'));
                }
            });
        }
    });
</script>
@endsection