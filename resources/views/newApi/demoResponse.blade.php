@extends('newApi.templates.landing')
@section('meta')
    <title>Demo | Hocelot</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
@endsection
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/css/bootstrap-select.min.css">
    <link href="/assets/css/new-demo.css" rel="stylesheet"/>
@endsection
@section('content')
    <section class="col-md-12 col-xs-12 section_padding_bottom80">
        <div class="container">
            <div class="col-md-1 visible-md-block visible-lg-block"></div>
            <div class="col-md-8">
                <ol class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    <li class="active"><a href="#">Demo</a></li>
                </ol>
            </div>
            <div class="col-md-3 visible-md-block visible-lg-block"></div>
        </div>
    </section>
<section class="col-md-12 col-xs-12 section_padding_bottom">
    <div class="container" id="id_cf_demo">
        <div class="col-md-12 col-xs-12">
            <h2>ID Check & Fraud</h2>
            <div class="section_line_div">
                <hr class="section_line">
            </div>
            <p class="section_p">
                Identificamos la correlación DNI/NIE con el nombre y apellidos y comprobamos la posibilidad de fraude a través de la identidad digital del sujeto.
            </p>
        </div>
        <div class="col-md-12 col-xs-12 back_grey_resp">
            <div class="col-md-12 col-xs-12 result_div white_div_resp">
                @if($id_check['status'] == 200)
                    <div class="col-md-6 col-xs-12">
                        <p class="p_titulo_res">Documento</p>
                        <p class="p_res">{{$id_check['hits']['nif']}}</p>
                        <p class="p_titulo_res">Nombre y apellidos</p>
                        <p class="p_res">{{$id_check['hits']['name_surname']}}</p>
                        <p class="p_titulo_res">Score ID Check</p>
                        <p class="p_res">{{$id_check['hits']['score_id_check']}}</p>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <p class="p_titulo_res">Correlación</p>
                        <p class="p_res">{{$id_check['hits']['correlacion_id_check']}} %</p>
                        <p class="p_titulo_res">Alerta</p>
                        <p class="p_res">
                        @if(empty($id_check['hits']['alert']))
                            False
                        @else
                            True
                        @endif
                        </p>
                        @if($id_fraud['status'] == 200)
                            <p class="p_titulo_res">Score ID Fraud</p>
                            <p class="p_res">{{$id_fraud['hits']['score']}}</p>
                        @else
                            <p class="p_titulo_res">Score ID Fraud</p>
                            <p class="p_res">No encontrado</p>
                        @endif
                    </div>
                @else
                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-6 col-xs-12">
                            <p class="p_titulo_res">Documento</p>
                            <p class="p_res">{{$id_check['hits']['nif']}}</p>
                            <p class="p_titulo_res">Nombre y apellidos</p>
                            <p class="p_res">{{$id_check['hits']['name_surname']}}</p>
                            <p class="p_titulo_res">Score ID Check</p>
                            <p class="p_res">{{$id_check['hits']['score_id_check']}}</p>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <p class="p_titulo_res">Correlación</p>
                            <p class="p_res">{{$id_check['hits']['correlacion_id_check']}} %</p>
                            <p class="p_titulo_res">Alerta</p>
                            <p class="p_res">
                                @if(empty($id_check['hits']['alert']))
                                    False
                                @else
                                    True
                                @endif
                            </p>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</section>
<section class="col-md-12 col-xs-12 section_padding_bottom">
    <div class="container" id="geo_cf_demo">
        <div class="col-md-12 col-xs-12">
            <h2>Geo Check & Fraud</h2>
            <div class="section_line_div">
                <hr class="section_line">
            </div>
            <p class="section_p">
                Identificamos la correlación DNI/NIE con el nombre y apellidos y comprobamos la posibilidad de fraude a través de la identidad digital del sujeto.
            </p>
        </div>
        <div class="col-md-12 col-xs-12 back_grey_resp">
            <div class="col-md-12 col-xs-12 result_div white_div_resp">
                @if($id_check['status'] == 200 && $id_fraud['status'] == 200 && $geo_check['status'] == 200)
                    <div class="col-md-6 col-xs-12">
                        <p class="p_titulo_res">Parcela</p>
                        <p class="p_res">{{$geo_check['hits']['parcela']}}</p>
                        <p class="p_titulo_res">Dirección</p>
                        <p class="p_res">{{$geo_check['hits']['address']}}</p>
                        <p class="p_titulo_res">Metros cuadrados</p>
                        <p class="p_res">{{$geo_check['hits']['m2']}}</p>
                        <p class="p_titulo_res">Usado para</p>
                        <p class="p_res">{{$geo_check['hits']['used_for']}}</p>
                        <p class="p_titulo_res">Nivel de exactitud</p>
                        <p class="p_res">{{$geo_check['hits']['level']}}</p>
                        @if($geo_fraud['status'] == 200)
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <p class="p_titulo_res">Tipo</p>
                            <p class="p_res">{{$geo_check['hits']['type']}}</p>
                            <p class="p_titulo_res">Latitud</p>
                            <p class="p_res">{{$geo_check['hits']['latitude']}}</p>
                            <p class="p_titulo_res">Longitud</p>
                            <p class="p_res">{{$geo_check['hits']['longitude']}}</p>
                            <p class="p_titulo_res">Dirección Geo Fraud</p>
                            <p class="p_res">{{$geo_fraud['hits']->address}}</p>
                            <p class="p_titulo_res">Correlación Geo Fraud</p>
                            <p class="p_res">{{$geo_fraud['hits']->correlacion}}</p>
                        </div>
                        @else
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <p class="p_titulo_res">Latitud</p>
                            <p class="p_res">{{$geo_check['hits']['latitude']}}</p>
                            <p class="p_titulo_res">Longitud</p>
                            <p class="p_res">{{$geo_check['hits']['longitude']}}</p>
                            <p class="p_titulo_res">Nivel de exactitud</p>
                            <p class="p_res">{{$geo_check['hits']['level']}}</p>
                            <p class="p_titulo_res">Tipo</p>
                            <p class="p_res">{{$geo_check['hits']['type']}}</p>
                            <p class="p_titulo_res">Correlación Geo Fraud</p>
                            <p class="p_res">No encontrado</p>
                        </div>
                        @endif
                @else
                    <div class="col-md-6 col-xs-12">
                        <p class="p_titulo_res">Parcela</p>
                        <p class="p_res">{{$geo_check['hits']['parcela']}}</p>
                        <p class="p_titulo_res">Dirección</p>
                        <p class="p_res">{{$geo_check['hits']['address']}}</p>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <p class="p_titulo_res">Nivel de exactitud</p>
                        <p class="p_res">{{$geo_check['hits']['level']}}</p>
                        <p class="p_titulo_res">Tipo</p>
                        <p class="p_res">{{$geo_check['hits']['type']}}</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</section>
<section class="col-md-12 col-xs-12 section_padding_bottom">
    <div class="container" id="risk_demo">
        <div class="col-md-12 col-xs-12">
            <h2>Risk Score</h2>
            <div class="section_line_div">
                <hr class="section_line">
            </div>
            <p class="section_p">
                Hocelot Risk Score mediante su motor de búsquela localiza, discrimina, clasifica y analiza información cualitativa y cuantitativa del individuo buscada en la web. El rastro digital de un sujeto estudiado se parametriza para determinar con nuestros algoritmos un informe con variables objetivas.
            </p>
        </div>
        <div class="col-md-12 col-xs-12 back_grey_resp">
            @if($id_check['status'] == 200 && $id_fraud['status'] == 200 && $geo_check['status'] == 200 && $risk_score['status'] ==200)
            <div class="col-md-12 col-xs-12 result_div white_div_resp">
                <div class="col-md-6 col-xs-12">
                    <p class="p_titulo_res">Score</p>
                    <p class="p_res"> {{ $risk_score['hits']->score }}</p>
                    <p class="p_titulo_res">Fiabilidad de pago</p>
                    <p class="p_res">{{number_format($risk_score['hits']->fiabilidad_pago, 2, ",", ".")}}%</p>
                    <p class="p_titulo_res">Probabilidad impago</p>
                    <p class="p_res">{{number_format($risk_score['hits']->probabilidad_impago, 2, ",", ".")}}%</p>
                    <p class="p_titulo_res">Periodo medio de pago</p>
                    <p class="p_res">{{$risk_score['hits']->periodo_medio_pago}} días.</p>
                    <p class="p_titulo_res">Régimen de tenencia</p>
                    <p class="p_res">{{$risk_score['hits']->tenencia}}</p>
                    <p class="p_titulo_res">Dependiente</p>
                    <p class="p_res">{{$risk_score['hits']->dependiente}}</p>
                    <p class="p_titulo_res">Ingreso mensual hogar</p>
                    <p class="p_res">{{number_format($risk_score['hits']->ingreso_mensual_hogar, 2, ",", ".")}}€</p>
                    <p class="p_titulo_res">Ingreso mensual individual</p>
                    <p class="p_res">{{number_format($risk_score['hits']->ingreso_mensual_individual, 2, ",", ".")}}€</p>
                    <p class="p_titulo_res">Desviación Renta</p>
                    <p class="p_res">{{number_format($risk_score['hits']->desviacion_renta, 2, ",", ".")}}%</p>
                    <p class="p_titulo_res">Umbral pobreza</p>
                    <p class="p_res">{{number_format($risk_score['hits']->umbral_pobreza, 2, ",", ".")}}%</p>
                    <p class="p_titulo_res">Nivel estudios</p>
                    <p class="p_res">{{$risk_score['hits']->nivel_estudios}}</p>
                </div>
                <div class="col-md-6 col-xs-12">
                    <p class="p_titulo_res">Trabajo</p>
                    <p class="p_res">{{$risk_score['hits']->trabajo}}%</p>
                    <p class="p_titulo_res">Metros cuadrados</p>
                    <p class="p_res">{{number_format($risk_score['hits']->metros_cuadrados, 0, ",", ".")}}m2</p>
                    <p class="p_titulo_res">Precio de mercado</p>
                    <p class="p_res">{{number_format($risk_score['hits']->precio, 2, ",", ".")}}€</p>
                    <p class="p_titulo_res">Tasa de Esfuerzo</p>
                    <p class="p_res">{{number_format($risk_score['hits']->tasa_esfuerzo, 2, ",", ".")}}%</p>
                    <p class="p_titulo_res">Renta vivienda hogar</p>
                    <p class="p_res">{{number_format($risk_score['hits']->renta_vivienda_hogar, 2, ",", ".")}}€</p>
                    <p class="p_titulo_res">Renta vivienda individual</p>
                    <p class="p_res">{{number_format($risk_score['hits']->renta_vivienda_individual, 2, ",", ".")}}€</p>
                    <p class="p_titulo_res">Morosidad</p>
                    <p class="p_res">{{number_format($risk_score['hits']->morosidad, 2, ",", ".")}}%</p>
                    <p class="p_titulo_res">Tipo de potencial morosidad</p>
                    <p class="p_res">{{$risk_score['hits']->tipo_moroso}}</p>
                    <p class="p_titulo_res">Tasa de paro corregida</p>
                    <p class="p_res">{{number_format($risk_score['hits']->tasa_paro_correg, 2, ",", ".")}}%</p>
                    <p class="p_titulo_res">Capacidad Ahorro</p>
                    <p class="p_res">{{number_format($risk_score['hits']->capacidad_ahorro, 2, ",", ".")}}€</p>
                </div>
            </div>
            <div class="col-md-12 col-xs-12 result_div white_div_resp">
                <div class="col-md-6 col-xs-12">
                    <p class="p_titulo_res">Grupo de gasto</p>
                    <p class="p_res">Individual</p>
                    <p class="p_titulo_res">Alimentación</p>
                    <p class="p_res">{{number_format($risk_score['hits']->grupo_gastos_individual->alimentacion, 2, ",", ".")}}€
                    <p class="p_titulo_res">Alcohol y tabaco</p>
                    <p class="p_res">{{number_format($risk_score['hits']->grupo_gastos_individual->alcohol_tabaco, 2, ",", ".")}}€
                    <p class="p_titulo_res">Ropa y calzado</p>
                    <p class="p_res">{{number_format($risk_score['hits']->grupo_gastos_individual->ropa_calzado, 2, ",", ".")}}€
                    <p class="p_titulo_res">Suministros vivienda</p>
                    <p class="p_res">{{number_format($risk_score['hits']->grupo_gastos_individual->vivienda_suministros, 2, ",", ".")}}€
                    <p class="p_titulo_res">Mobiliario</p>
                    <p class="p_res">{{number_format($risk_score['hits']->grupo_gastos_individual->mobiliario, 2, ",", ".")}}€
                    <p class="p_titulo_res">Salud</p>
                    <p class="p_res">{{number_format($risk_score['hits']->grupo_gastos_individual->salud, 2, ",", ".")}}€
                </div>
                <div class="col-md-6 col-xs-12">
                    <p class="p_titulo_res">Transporte</p>
                    <p class="p_res">{{number_format($risk_score['hits']->grupo_gastos_individual->transporte, 2, ",", ".")}}€
                    <p class="p_titulo_res">Comunicaciones</p>
                    <p class="p_res">{{number_format($risk_score['hits']->grupo_gastos_individual->comunicaciones, 2, ",", ".")}}€
                    <p class="p_titulo_res">Ocio</p>
                    <p class="p_res">{{number_format($risk_score['hits']->grupo_gastos_individual->ocio, 2, ",", ".")}}€
                    <p class="p_titulo_res">Educación</p>
                    <p class="p_res">{{number_format($risk_score['hits']->grupo_gastos_individual->educacion, 2, ",", ".")}}€
                    <p class="p_titulo_res">Restaurantes</p>
                    <p class="p_res">{{number_format($risk_score['hits']->grupo_gastos_individual->restaurantes, 2, ",", ".")}}€
                    <p class="p_titulo_res">otros</p>
                    <p class="p_res">{{number_format($risk_score['hits']->grupo_gastos_individual->otros, 2, ",", ".")}}€
                </div>
            </div>
                <div class="col-md-12 col-xs-12 result_div white_div_resp">
                    <div class="col-md-6 col-xs-12">
                        <p class="p_titulo_res">Grupo de gasto</p>
                        <p class="p_res">Hogar</p>
                        <p class="p_titulo_res">Alimentación</p>
                        <p class="p_res">{{number_format($risk_score['hits']->grupo_gastos_hogar->alimentacion, 2, ",", ".")}}€
                        <p class="p_titulo_res">Alcohol y tabaco</p>
                        <p class="p_res">{{number_format($risk_score['hits']->grupo_gastos_hogar->alcohol_tabaco, 2, ",", ".")}}€
                        <p class="p_titulo_res">Ropa y calzado</p>
                        <p class="p_res">{{number_format($risk_score['hits']->grupo_gastos_hogar->ropa_calzado, 2, ",", ".")}}€
                        <p class="p_titulo_res">Suministros vivienda</p>
                        <p class="p_res">{{number_format($risk_score['hits']->grupo_gastos_hogar->vivienda_suministros, 2, ",", ".")}}€
                        <p class="p_titulo_res">Mobiliario</p>
                        <p class="p_res">{{number_format($risk_score['hits']->grupo_gastos_hogar->mobiliario, 2, ",", ".")}}€
                        <p class="p_titulo_res">Salud</p>
                        <p class="p_res">{{number_format($risk_score['hits']->grupo_gastos_hogar->salud, 2, ",", ".")}}€
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <p class="p_titulo_res">Transporte</p>
                        <p class="p_res">{{number_format($risk_score['hits']->grupo_gastos_hogar->transporte, 2, ",", ".")}}€
                        <p class="p_titulo_res">Comunicaciones</p>
                        <p class="p_res">{{number_format($risk_score['hits']->grupo_gastos_hogar->comunicaciones, 2, ",", ".")}}€
                        <p class="p_titulo_res">Ocio</p>
                        <p class="p_res">{{number_format($risk_score['hits']->grupo_gastos_hogar->ocio, 2, ",", ".")}}€
                        <p class="p_titulo_res">Educación</p>
                        <p class="p_res">{{number_format($risk_score['hits']->grupo_gastos_hogar->educacion, 2, ",", ".")}}€
                        <p class="p_titulo_res">Restaurantes</p>
                        <p class="p_res">{{number_format($risk_score['hits']->grupo_gastos_hogar->restaurantes, 2, ",", ".")}}€
                        <p class="p_titulo_res">otros</p>
                        <p class="p_res">{{number_format($risk_score['hits']->grupo_gastos_hogar->otros, 2, ",", ".")}}€
                    </div>
                </div>
            @else
                <div class="col-md-12 col-xs-12 white_div_resp">
                    <div class="col-md-12 col-xs-12">
                        <p class="p_titulo_res">Status 400</p>
                        <p class="p_res">No encontrado</p>
                    </div>
                </div>
            @endif
        </div>
    </div>
</section>
<section class="col-md-12 col-xs-12 text-center section_padding">
    <h1 class="section_big_title section_padding"> ¿Alguna duda?</h1>
    <button class="link_style" id="contactaSmooth" data-toggle="modal" data-target="#contactModal">Contacta con nosotros</button>
    <div class="section_padding_bottom"></div>
</section>
<div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-header modal-contact">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <section class="col-md-12 col-xs-12 modal-contact section_padding_bottom" id="ModalContactBody">
            <div class="col-md-1 visible-md-block visible-lg-block height_input"></div>
            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                <h1>
                    Confirma tus datos de contacto
                </h1>
                <p class="error_contact" id="error_text"></p>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="placeMin" style="height:5px"><text id="placenombre">Nombre</text></div>
                <input type="text" class="form-control input_label" value="{{$form['name']}}" placeholder="Nombre" id="nombre" onBlur="checkInput(this)" onFocus="infoInput(this)" onkeyup="getPlaceMin(this)">
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="placeMin" style="height:5px"><text id="placeapellidos">Primer apellido</text></div>
                <input type="text" class="form-control input_label" value="{{$form['lastName']}}" placeholder="Apellidos" id="apellidos" onBlur="checkInput(this)" onFocus="infoInput(this)" onkeyup="getPlaceMin(this)">
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="placeMin" style="height:5px"><text id="placecargo" style="display:none">Cargo</text></div>
                <input type="text" class="form-control input_label" placeholder="Cargo" id="cargo" onBlur="checkInput(this)" onFocus="infoInput(this)" onkeyup="getPlaceMin(this)">
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="placeMin" style="height:5px"><text id="placenombre_emp" style="display:none">Nombre de la empresa</text></div>
                <input type="text" class="form-control input_label" placeholder="Nombre de la empresa" id="nombre_emp" onBlur="checkInput(this)" onFocus="infoInput(this)" onkeyup="getPlaceMin(this)">
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="placeMin" style="height:5px"><text id="placeemail">Email</text></div>
                <input type="text" class="form-control input_label" value="{{$form['email']}}" placeholder="Email" id="email" onBlur="checkEmail(this)" onFocus="infoEmail(this)" onkeyup="getPlaceMin(this)">
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="placeMin" style="height:5px"><text id="placetelefono">Teléfono</text></div>
                <input type="text" class="form-control input_label" value="{{$form['phone']}}" placeholder="Teléfono" id="telefono" maxlength="9" onBlur="checkTelf(this)" onFocus="infoTelf(this)" onkeyup="getPlaceMin(this)">
            </div>
            <div class="col-md-4 visible-md-block visible-lg-block height_input"></div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="margin_auto_cons section_margin_top button_margin_bottom">
                    <input type="button" class="boton_consultar" id="submit_button" value="Enviar">
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                En cumplimiento de la L.O. 15/1999 y del REGLAMENTO (UE) 2016/679 usted consiente en el tratamiento de sus datos con la finalidad de poder atender su petición y poder contactar con usted. En caso de que quiera retirar su consentimiento o ejercitar cualquiera de sus derechos le rogamos que lo haga de la forma indicada en nuestra Política de Privacidad.            </div>
        </section>
    </div>
</div>
@endsection
@section('js')
    <script src="/assets/js/new-validar.js"></script>
    <script src="/assets/js/new-demoResponse.js"></script>
    <script src="/assets/js/bootstrap-select.js"></script>
@endsection