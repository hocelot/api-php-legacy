@extends('newApi.templates.landing')
@section('meta')
    <title>Real Data Real Time | Hocelot</title>
    <meta name="description" content="Hocelot es una compañía especialista en la obtención de información útil y objetiva de personas físicas en tiempo real. ¡Prueba nuestra demo!">
    <meta name="keywords" content="información útil, personas físicas, Hocelot">
@endsection
@section('css')

@endsection
@section('content')
    <!-- Section 1-->
    <section class="col-md-12 col-xs-12 text-center bottom_padding">
        <div class="container">
            <div class="col-md-1"></div>
            <div class="col-md-8">
                <p id="home_msg_inlet" class="title_section1 text-left"></p>
            </div>
        </div>
        <div class="container section_padding">
            <div class="col-md-12">
                <img id="home_img_inlet_src" class="section_img1" alt="Simulación hocelot" src="">
            </div>
        </div>
    </section>
    <!-- section 2 -->
    <section class="col-md-12 col-xs-12 section_background section_padding">
        <div class="container">
            <h4 id="home_msg_technologies_header" class="section_title text-center"></h4>
        </div>
        <div class="container_home">
            <div class="col-md-12 section_padding section_background_white section_margin_bottom">
                <div class="col-md-4 text-center">
                    <img id="home_img_technologies1_src" class="icon_home_tec" alt="Fiabilidad icono" src="">
                </div>
                <div class="col-md-8">
                    <div><h1 id="home_msg_technologies1_title" class="section_no_margin_bottom"></h1></div>
                    <div class="section_line_div">
                        <hr class="section_line"/>
                    </div>
                    <p id="home_msg_technologies1_description" class="section_p"></p>
                </div>
            </div>
            <div class="col-md-12 section_padding section_background_white section_margin_bottom">
                <div class="col-md-4 text-center">
                    <img id="home_img_technologies2_src" class="icon_home_tec" alt="Tiempo real icono" src="">
                </div>
                <div class="col-md-8">
                    <div><h1 id="home_msg_technologies2_title" class="section_no_margin_bottom"></h1></div>
                    <div class="section_line_div">
                        <hr class="section_line"/>
                    </div>
                    <p id="home_msg_technologies2_description" class="section_p"></p>
                </div>
            </div>
            <div class="col-md-12 section_padding section_background_white section_margin_bottom">
                <div class="col-md-4 text-center">
                    <img id="home_img_technologies3_src" class="icon_home_tec" alt="Utilidad icono" src="">
                </div>
                <div class="col-md-8">
                    <div><h1 id="home_msg_technologies3_title" class="section_no_margin_bottom"></h1></div>
                    <div class="section_line_div">
                        <hr class="section_line"/>
                    </div>
                    <p id="home_msg_technologies3_description" class="section_p"></p>
                </div>
            </div>
        </div>
    </section>
    <!-- Section 3-->
    <section class="col-md-12 col-xs-12 text-center section_padding">
        <div><h4 id="home_msg_highlighted1_title" class="section_big_title section_padding"></h4></div>
        <div>
            <button id="home_msg_highlighted1_btn" class="link_style" onclick="window.location.href='/demo'"></button>
        </div>
        <div class="section_padding_bottom"></div>
    </section>
    <!-- Section 4-->
    <section class="col-md-12 col-xs-12 section_background section_padding">
        <div class="container">
            <div class="col-md-12 col-xs-12">
                <h4 id="home_msg_offers" class="section_title text-center"></h4>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 section_background_white section_margin_right_2 section_padding_all section_responsive_margin_bottom section_padding_left80 section_padding_bottom70 section_padding_top70 similar_height_home">
                <div><h2 id="home_msg_offers1_title" class="section_subtitle"></h2></div>
                <div class="section_line_div">
                    <hr class="section_line"/>
                </div>
                <div><p id="home_msg_offers1_description" class="section_p"></p></div>
                <div><h3 id="home_msg_offers1_service1" class="p_margin p_margin_top"></h3></div>
                <div><h3 id="home_msg_offers1_service2" class="p_margin"></h3></div>
                <div><h3 id="home_msg_offers1_service3" class="p_margin"></h3></div>
                <div class="link_services section_margin_top">
                    <button id="home_msg_offers1_btn" class="link_style" onclick="window.location.href='/smart-data'"></button>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 section_background_white section_margin_left_2 section_padding_all section_padding_left80 section_padding_bottom70 section_padding_top70 similar_height_home">
                <div><h2 id="home_msg_offers2_title" class="section_subtitle"></h2></div>
                <div class="section_line_div">
                    <hr class="section_line"/>
                </div>
                <div><p id="home_msg_offers2_description" class="section_p"></p></div>
                <div><h3 id="home_msg_offers2_service1" class="p_margin p_margin_top"></h3></div>
                <div><h3 id="home_msg_offers2_service2" class="p_margin"></h3></div>
                <div><h3 id="home_msg_offers2_service3" class="p_margin"></h3></div>
                <div class="link_services section_margin_top">
                    <button id="home_msg_offers2_btn" class="link_style" onclick="window.location.href='/smart-analytics'"></button>
                </div>
            </div>
        </div>
    </section>
    <!-- Section 5-->
    <section class="col-md-12 col-xs-12 text-center section_double_padding abstract_background">
        <div class="col-md-12 col-xs-12">
            <div><h4 id="home_msg_highlighted2_title" class="section_big_title section_big_title_2 section_padding_bottom30"></h4></div>
            <div>
                <button id="home_msg_highlighted2_button" class="btn button_section6 btn_transparent" onclick="window.location.href='/contrata'"></button>
            </div>
        </div>
    </section>
    <!-- Section 6-->
    <section class="col-md-12 col-xs-12 text-center section_double_padding">
        <div class="col-md-12 col-xs-12">
            <div><h4 id="home_msg_highlighted3_title" class="section_big_title section_padding_bottom30"></h4></div>
            <div>
                <button id="home_msg_highlighted3_button" class="link_style section_bottom" onclick="window.location.href='/demo'"></button>
            </div>
        </div>
    </section>
@endsection
@section('js')
    <script>
        jQuery.i18n.properties({
            name: 'Messages',
            path: '/assets/',
            mode: 'both',
//            async: true,
            callback: function () {
                $("[id^='home_msg']").each(function () {
                    $(this).text(jQuery.i18n.prop($(this).attr('id')));
                });
                $("[id^='home_img']").each(function () {
                    if($(window).width() < '780') {
                        $(this).attr('src', '/assets/img/new-api/' + jQuery.i18n.prop($(this).attr('id')+'_1x'));
                    }else if($(window).width() > '780' && $(window).width() < '1024'){
                        $(this).attr('src', '/assets/img/new-api/' + jQuery.i18n.prop($(this).attr('id')+'_2x'));
                    }else{
                        $(this).attr('src', '/assets/img/new-api/' + jQuery.i18n.prop($(this).attr('id')+'_3x'));
                    }
                });
            }
        });
    </script>
@endsection