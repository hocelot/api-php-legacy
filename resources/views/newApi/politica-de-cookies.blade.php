@extends('newApi.templates.landing')
@section('meta')
    <title>Política de cookies | Hocelot</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
@endsection
@section('css')
    <link href="/assets/css/hocelot.css" rel="stylesheet"/>
    <link href="/assets/css/new-demo.css" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">
@endsection
@section('content')
<section class="col-md-12 col-xs-12">
    <div class="container">
        <div class="col-md-12 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li class="active"><a href="#">Política de cookies</a></li>
            </ol>
            <div><h1 id="legal_msg_cook_title"></h1></div>
            <div class="section_line_div">
                <hr class="section_line"/>
            </div>
        </div>
    </div>
</section>
<!-- Section 2-->
<section class="col-md-12 col-xs-12 section_padding_bottom">
    <div class="container">
        <div class="col-md-12 col-xs-12">
            <p id="legal_msg_cook_subtext1" class="section_p"></p>
            <p id="legal_msg_cook_subtext2" class="section_p"></p>
            <p id="legal_msg_cook_title1" class="section_p"></p>
            <div><h2 id="legal_msg_cook_title2" class="section_subtitle"></h2></div>
            <p id="legal_msg_cook_text2" class="section_p"></p>
            <div><h2 id="legal_msg_cook_title3" class="section_subtitle"></h2></div>
            <p id="legal_msg_cook_text3" class="section_p"></p>
            <div><h2 id="legal_msg_cook_title4" class="section_subtitle"></h2></div>
            <p id="legal_msg_cook_text4" class="section_p"></p>
            <p id="legal_msg_cook_text5" class="section_p"></p>
        </div>
    </div>
</section>
@endsection
@section('js')
    <script src="/assets/js/new-validar.js"></script>
    <script src="/assets/js/new-contacto.js"></script>
    <script>
        jQuery.i18n.properties({
            name: 'Messages',
            path: '/assets/',
            mode: 'both',
//            async: true,
            callback: function () {
                $("[id^='legal_msg_cook']").each(function () {
                    $(this).text(jQuery.i18n.prop($(this).attr('id')));
                });

                $("[id^='legal_img_cook']").each(function () {
                    $(this).attr('src', '/assets/img/new-api/' + jQuery.i18n.prop($(this).attr('id')));
                });
            }
        });
    </script>
@endsection