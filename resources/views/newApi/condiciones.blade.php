@extends('newApi.templates.landing')
@section('meta')
    <title>Condiciones de uso | Hocelot</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
@endsection
@section('css')
    <link href="/assets/css/hocelot.css" rel="stylesheet"/>
@endsection
@section('content')
<section class="col-md-12 col-xs-12">
    <div class="container">
        <div class="col-md-12 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="/">Home</a></li>
                <li class="active"><a href="#">Términos y Condiciones de Uso</a></li>
            </ol>
            <div><h1 id="legal_msg_cond_title"></h1></div>
            <div class="section_line_div">
                <hr class="section_line"/>
            </div>
        </div>
    </div>
</section>
<!-- Section 2-->
<section class="col-md-12 col-xs-12 section_padding_bottom">
    <div class="container">
        <div class="col-md-12 col-xs-12">
            <p id="legal_msg_cond_text" class="section_p"></p>
            <div><h2 id="legal_msg_cond_title1" class="section_subtitle"></h2></div>
            <p id="legal_msg_cond_text1" class="section_p"></p>
            <div><h2 id="legal_msg_cond_title2" class="section_subtitle"></h2></div>
            <p id="legal_msg_cond_text2" class="section_p"></p>
            <div><h2 id="legal_msg_cond_title3" class="section_subtitle"></h2></div>
            <p id="legal_msg_cond_text3" class="section_p"></p>
            <div><h2 id="legal_msg_cond_title4" class="section_subtitle"></h2></div>
            <p id="legal_msg_cond_text4" class="section_p"></p>
            <div><h2 id="legal_msg_cond_title5" class="section_subtitle"></h2></div>
            <p id="legal_msg_cond_text5" class="section_p"></p>
            <div><h2 id="legal_msg_cond_title6" class="section_subtitle"></h2></div>
            <p id="legal_msg_cond_text6" class="section_p"></p>
            <div><h2 id="legal_msg_cond_title7" class="section_subtitle"></h2></div>
            <p id="legal_msg_cond_text7" class="section_p"></p>
        </div>
    </div>
</section>
@endsection
@section('js')
    <script>
        jQuery.i18n.properties({
            name: 'Messages',
            path: '/assets/',
//            mode: 'both',
            async: true,
            callback: function () {
                $("[id^='legal_msg_cond']").each(function () {
                    $(this).text(jQuery.i18n.prop($(this).attr('id')));
                });

                $("[id^='legal_img_cond']").each(function () {
                    $(this).attr('src', '/assets/img/new-api/' + jQuery.i18n.prop($(this).attr('id')));
                });
            }
        });
    </script>
@endsection