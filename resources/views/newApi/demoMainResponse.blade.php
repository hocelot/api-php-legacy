@if(Request::path() == 'pdf/getPdf')
    <link href="https://fonts.googleapis.com/css?family=Roboto:400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">
    <link href="/var/www/api/public/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="/var/www/api/public/assets/css/hocelot.css" rel="stylesheet"/>
    <link href="/var/www/api/public/assets/css/new-DemoResponse.css" rel="stylesheet"/>
    <meta charset="UTF-8" />
@else

@endif

<section style="display:none">
    {{-- TIPO DE MOROSO VARS --}}
    @foreach(explode(',', $risk_score['hits']->tipo_moroso) as $mor)
        @if($mor == 1)
            @if(isset($tipo_moroso))
                {!! $tipo_moroso .= ', Fortuito' !!}
            @else
                {!! $tipo_moroso = 'Fortuito' !!}
            @endif
        @endif
        @if($mor == 2)
            @if(isset($tipo_moroso))
                {!! $tipo_moroso .= ', Negligente' !!}
            @else
                {!! $tipo_moroso = 'Negligente' !!}
            @endif
        @endif
        @if($mor == 3)
            @if(isset($tipo_moroso))
                {!! $tipo_moroso .= ', Incompetente' !!}
            @else
                {!! $tipo_moroso = 'Incompetente' !!}
            @endif
        @endif
        @if($mor == 4)
            @if(isset($tipo_moroso))
                {!! $tipo_moroso .= ', Recurrente' !!}
            @else
                {!! $tipo_moroso = 'Recurrente' !!}
            @endif
        @endif
        @if($mor == 5)
            @if(isset($tipo_moroso))
                {!! $tipo_moroso .= ', Intencional' !!}
            @else
                {!! $tipo_moroso = 'Intencional' !!}
            @endif
        @endif
    @endforeach
    {{-- GEO CHECK VARS --}}
    @if($geo_check['hits']['level'] == 1)
        {!! $tipo='Exacta' !!}
        {!! $left='85%' !!}
        {!! $id_text = 'demoResp_msg_geo_c_lvl1' !!}
    @elseif($geo_check['hits']['level'] == 2)
        {!! $tipo='Casi exacta' !!}
        {!! $left='75%' !!}
        {!! $id_text = 'demoResp_msg_geo_c_lvl2' !!}
    @elseif($geo_check['hits']['level'] == 3)
        {!! $tipo='Completa' !!}
        {!! $left='50%' !!}
        {!! $id_text = 'demoResp_msg_geo_c_lvl3' !!}
    @elseif($geo_check['hits']['level'] == 4)
        {!! $tipo='Aproximada' !!}
        {!! $left='15%' !!}
        {!! $id_text = 'demoResp_msg_geo_c_lvl4' !!}
    @else
        {!! $tipo='No existe' !!}
        {!! $left='0%' !!}
        {!! $id_text = 'demoResp_msg_geo_c_lvl5' !!}
    @endif
    @if($geo_fraud['status'] == 200)
        {!! $id_text .= '_f_ko' !!}
    @else
        {!! $id_text .= '_f_ok' !!}
    @endif
    @if ($risk_score['hits']->score > 300 && $risk_score['hits']->score < 330){
    {!! $text_risk = 'muy malo' !!}
    {!! $left_risk = '0%' !!}
    }
    @elseif ($risk_score['hits']->score >= 331 && $risk_score['hits']->score <= 370){
    {!! $text_risk = 'malo' !!}
    {!! $left_risk = '25%' !!}
    }
    @elseif ($risk_score['hits']->score >= 371 && $risk_score['hits']->score <= 450){
    {!! $text_risk = 'regular' !!}
    {!! $left_risk = '30%' !!}
    }
    @elseif ($risk_score['hits']->score >= 451 && $risk_score['hits']->score <= 580){
    {!! $text_risk = 'bueno' !!}
    {!! $left_risk = '50%' !!}
    }
    @elseif ($risk_score['hits']->score >= 581 && $risk_score['hits']->score <= 700){
    {!! $text_risk = 'muy bueno' !!}
    {!! $left_risk = '70%' !!}
    }
    @elseif ($risk_score['hits']->score >= 701 && $risk_score['hits']->score <= 850){
    {!! $text_risk = 'excelente' !!}
    {!! $left_risk = '80%' !!}
    }
    {{--if ($rating['score'] > 300 && $rating['score'] < 310){--}}
    {{--echo '--}}
    {{--<div class="div_result_numero_abajo">--}}
    {{--<p class="result_rating_d_menos">'.$rating['score'].'</p>--}}
    {{--</div>--}}
    {{--<div class="div_img_rating">--}}
    {{--<img class="img_rating" src="'.base_url().'assets/img/mirating/fiabilidad_pago/d-.png">--}}
    {{--</div>--}}
    {{--';--}}
    {{--} elseif ($rating['score'] >= 311 && $rating['score'] <= 320){--}}
    {{--echo '--}}
    {{--<div class="div_result_numero_abajo">--}}
    {{--<p class="result_rating_d">'.$rating['score'].'</p>--}}
    {{--</div>--}}
    {{--<div class="div_img_rating">--}}
    {{--<img class="img_rating" src="'.base_url().'assets/img/mirating/fiabilidad_pago/d.png">--}}
    {{--</div>--}}
    {{--';--}}
    {{--} elseif ($rating['score'] >= 321 && $rating['score'] <= 330){--}}
    {{--echo '--}}
    {{--<div class="div_result_numero_abajo">--}}
    {{--<p class="result_rating_d_mas">'.$rating['score'].'</p>--}}
    {{--</div>--}}
    {{--<div class="div_img_rating">--}}
    {{--<img class="img_rating" src="'.base_url().'assets/img/mirating/fiabilidad_pago/d+.png">--}}
    {{--</div>--}}
    {{--';--}}
    {{--} elseif ($rating['score'] >= 331 && $rating['score'] <= 340){--}}
    {{--echo '--}}
    {{--<div class="div_result_numero_abajo">--}}
    {{--<p class="result_rating_c_menos">'.$rating['score'].'</p>--}}
    {{--</div>--}}
    {{--<div class="div_img_rating">--}}
    {{--<img class="img_rating" src="'.base_url().'assets/img/mirating/fiabilidad_pago/c-.png">--}}
    {{--</div>--}}
    {{--';--}}
    {{--} elseif ($rating['score'] >= 341 && $rating['score'] <= 360){--}}
    {{--echo '--}}
    {{--<div class="div_result_numero_abajo">--}}
    {{--<p class="result_rating_c">'.$rating['score'].'</p>--}}
    {{--</div>--}}
    {{--<div class="div_img_rating">--}}
    {{--<img class="img_rating" src="'.base_url().'assets/img/mirating/fiabilidad_pago/c.png">--}}
    {{--</div>--}}
    {{--';--}}
    {{--} elseif ($rating['score'] >= 361 && $rating['score'] <= 370){--}}
    {{--echo '--}}
    {{--<div class="div_result_numero_abajo">--}}
    {{--<p class="result_rating_c_mas">'.$rating['score'].'</p>--}}
    {{--</div>--}}
    {{--<div class="div_img_rating">--}}
    {{--<img class="img_rating" src="'.base_url().'assets/img/mirating/fiabilidad_pago/c+.png">--}}
    {{--</div>--}}
    {{--';--}}
    {{--}elseif ($rating['score'] >= 371 && $rating['score'] <= 390){--}}
    {{--echo '--}}
    {{--<div class="div_result_numero_abajo">--}}
    {{--<p class="result_rating_b_menos">'.$rating['score'].'</p>--}}
    {{--</div>--}}
    {{--<div class="div_img_rating">--}}
    {{--<img class="img_rating" src="'.base_url().'assets/img/mirating/fiabilidad_pago/b-.png">--}}
    {{--</div>--}}
    {{--';--}}
    {{--} elseif ($rating['score'] >= 391 && $rating['score'] <= 420){--}}
    {{--echo '--}}
    {{--<div class="div_result_numero_abajo">--}}
    {{--<p class="result_rating_b">'.$rating['score'].'</p>--}}
    {{--</div>--}}
    {{--<div class="div_img_rating">--}}
    {{--<img class="img_rating" src="'.base_url().'assets/img/mirating/fiabilidad_pago/b.png">--}}
    {{--</div>--}}
    {{--';--}}
    {{--} elseif ($rating['score'] >= 421 && $rating['score'] <= 450){--}}
    {{--echo '--}}
    {{--<div class="div_result_numero_abajo">--}}
    {{--<p class="result_rating_b_mas">'.$rating['score'].'</p>--}}
    {{--</div>--}}
    {{--<div class="div_img_rating">--}}
    {{--<img class="img_rating" src="'.base_url().'assets/img/mirating/fiabilidad_pago/b+.png">--}}
    {{--</div>--}}
    {{--';--}}
    {{--} elseif ($rating['score'] >= 451 && $rating['score'] <= 500){--}}
    {{--echo '--}}
    {{--<div class="div_result_numero_abajo">--}}
    {{--<p class="result_rating_muy_a_menos">'.$rating['score'].'</p>--}}
    {{--</div>--}}
    {{--<div class="div_img_rating">--}}
    {{--<img class="img_rating" src="'.base_url().'assets/img/mirating/fiabilidad_pago/a-.png">--}}
    {{--</div>--}}
    {{--';--}}
    {{--} elseif ($rating['score'] >= 501 && $rating['score'] <= 540){--}}
    {{--echo '--}}
    {{--<div class="div_result_numero_abajo">--}}
    {{--<p class="result_rating_a">'.$rating['score'].'</p>--}}
    {{--</div>--}}
    {{--<div class="div_img_rating">--}}
    {{--<img class="img_rating" src="'.base_url().'assets/img/mirating/fiabilidad_pago/a.png">--}}
    {{--</div>--}}
    {{--';--}}
    {{--}elseif ($rating['score'] >= 541 && $rating['score'] <= 580){--}}
    {{--echo '--}}
    {{--<div class="div_result_numero_abajo">--}}
    {{--<p class="result_rating_a_mas">'.$rating['score'].'</p>--}}
    {{--</div>--}}
    {{--<div class="div_img_rating">--}}
    {{--<img class="img_rating" src="'.base_url().'assets/img/mirating/fiabilidad_pago/a+.png">--}}
    {{--</div>--}}
    {{--';--}}
    {{--} elseif ($rating['score'] >= 581 && $rating['score'] <= 620){--}}
    {{--echo '--}}
    {{--<div class="div_result_numero_abajo">--}}
    {{--<p class="result_rating_aa_menos">'.$rating['score'].'</p>--}}
    {{--</div>--}}
    {{--<div class="div_img_rating">--}}
    {{--<img class="img_rating" src="'.base_url().'assets/img/mirating/fiabilidad_pago/aa-.png">--}}
    {{--</div>--}}
    {{--';--}}
    {{--} elseif ($rating['score'] >= 621 && $rating['score'] <= 660){--}}
    {{--echo '--}}
    {{--<div class="div_result_numero_abajo">--}}
    {{--<p class="result_rating_aa">'.$rating['score'].'</p>--}}
    {{--</div>--}}
    {{--<div class="div_img_rating">--}}
    {{--<img class="img_rating" src="'.base_url().'assets/img/mirating/fiabilidad_pago/aa.png">--}}
    {{--</div>--}}
    {{--';--}}
    {{--} elseif ($rating['score'] >= 661 && $rating['score'] <= 700){--}}
    {{--echo '--}}
    {{--<div class="div_result_numero_abajo">--}}
    {{--<p class="result_rating_muy_aa_mas">'.$rating['score'].'</p>--}}
    {{--</div>--}}
    {{--<div class="div_img_rating">--}}
    {{--<img class="img_rating" src="'.base_url().'assets/img/mirating/fiabilidad_pago/aa+.png">--}}
    {{--</div>--}}
    {{--';--}}
    {{--} elseif ($rating['score'] >= 701 && $rating['score'] <= 750){--}}
    {{--echo '--}}
    {{--<div class="div_result_numero_abajo">--}}
    {{--<p class="result_rating_aaa_menos">'.$rating['score'].'</p>--}}
    {{--</div>--}}
    {{--<div class="div_img_rating">--}}
    {{--<img class="img_rating" src="'.base_url().'assets/img/mirating/fiabilidad_pago/aaa-.png">--}}
    {{--</div>--}}
    {{--';--}}
    {{--} elseif ($rating['score'] >= 751 && $rating['score'] <= 800){--}}
    {{--echo '--}}
    {{--<div class="div_result_numero_abajo">--}}
    {{--<p class="result_rating_aaa">'.$rating['score'].'</p>--}}
    {{--</div>--}}
    {{--<div class="div_img_rating">--}}
    {{--<img class="img_rating" src="'.base_url().'assets/img/mirating/fiabilidad_pago/aaa.png">--}}
    {{--</div>--}}
    {{--';--}}
    {{--} elseif ($rating['score'] >= 801 && $rating['score'] <= 850){--}}
    {{--echo '--}}
    {{--<div class="div_result_numero_abajo">--}}
    {{--<p class="result_rating_aaa_mas">'.$rating['score'].'</p>--}}
    {{--</div>--}}
    {{--<div class="div_img_rating">--}}
    {{--<img class="img_rating" src="'.base_url().'assets/img/mirating/fiabilidad_pago/aaa+.png">--}}
    {{--</div>--}}
    {{--';--}}
    {{--}--}}
    @endif
</section>
<div id="responseHocelot">
    {{--ID CHECK E ID FRAUD--}}
    <section class="col-md-12 col-xs-12 section_padding_bottom">
        <div class="container">
            <div class="col-md-12 col-xs-12">
                <h2>ID Check & Fraud</h2>
                @if($id_check['status'] == 200 && $id_fraud['status'] == 200)
                    <p class="section_p" id="demoResp_msg_id_cf_ok">La identidad del sujeto está validada. No hay patrón de fraude asociado.</p>
                @elseif($id_check['status'] == 200 && $id_fraud['status'] == 400)
                    <p class="section_p" id="demoResp_msg_id_cf_ok_ko">La identidad del sujeto está validada. Hay patrón de fraude asociado.</p>
                @elseif($id_check['status'] == 400)
                    <p class="section_p" id="demoResp_msg_id_cf_ko">La identidad del sujeto no está validada.</p>
                @endif
                <div class="text_graf_first section_p">0%</div>
                <div class="text_graf_mid section_p">50%</div>
                <div class="text_graf_first section_p">100%</div>
                <div class="col-md-1 col-sm-1 col-xs-1 first_graf"></div>
                <div class="col-md-1 col-sm-1 col-xs-1 med_graf"></div>
                <div class="col-md-1 col-sm-1 col-xs-1 med_graf"></div>
                <div class="col-md-1 col-sm-1 col-xs-1 med_graf"></div>
                <div class="col-md-1 col-sm-1 col-xs-1 med_graf"></div>
                <div class="col-md-1 col-sm-1 col-xs-1 med_graf"></div>
                <div class="col-md-1 col-sm-1 col-xs-1 med_graf"></div>
                <div class="col-md-1 col-sm-1 col-xs-1 med_graf"></div>
                <div class="col-md-1 col-sm-1 col-xs-1 med_graf"></div>
                <div class="col-md-1 col-sm-1 col-xs-1 med_graf"></div>
                <div class="col-md-1 col-sm-1 col-xs-1 med_graf"></div>
                <div class="col-md-1 col-sm-1 col-xs-1 last_graf"></div>
                <div class="col-md-12 col-sm-12 col-xs-12 section_no_padding_all">
                    <div class="div_response" style="left:{{(85*$id_check['hits']['correlacion_id_check'])/100}}%;">
                        <div class="triangulo_pop_response"></div>
                        <div class="pop_response">
                            <div class="col-md-7 col-sm-7 col-xs-7">
                                {{$id_check['hits']['correlacion_id_check']}}%
                            </div>
                            <div class="col-md-5 col-sm-5 col-xs-5">
                                @if(Request::path() == 'pdf/getPdf')
                                    <img src="/var/www/api/public/assets/img/new-api/path-2-copy-4.png">
                                @else
                                    <img src="/assets/img/new-api/path-2-copy-4.png">
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-xs-12">
                <div class="section_line_div">
                    <hr class="section_line">
                </div>
            </div>
        </div>
    </section>
</div>
@if($id_check['status'] == 200)
    {{--GEO CHECK Y GEO FRAUD--}}
    <section class="col-md-12 col-xs-12 section_padding_bottom">
        <div class="container">
            <div class="col-md-12 col-xs-12">
                <h2>Geo Check & Fraud</h2>
                <p class="section_p" id="{{$id_text}}">La dirección es completa y está verificada por nombre de vía, número, bloque, escalera, planta y puerta.
                    No se ha encontrado fraude geográfico ni domicilio comprometido.</p>
                <div class="col-md-1 col-sm-1 col-xs-1 graf_geo_first"><div class="graf_geo_in graf_geo1"></div></div>
                <div class="col-md-1 col-sm-1 col-xs-1 graf_geo_all"><div class="graf_geo_in graf_geo2"></div></div>
                <div class="col-md-1 col-sm-1 col-xs-1 graf_geo_all"><div class="graf_geo_in graf_geo3"></div></div>
                <div class="col-md-1 col-sm-1 col-xs-1 graf_geo_all"><div class="graf_geo_in graf_geo4"></div></div>
                <div class="col-md-1 col-sm-1 col-xs-1 graf_geo_all"><div class="graf_geo_in graf_geo5"></div></div>
                <div class="col-md-1 col-sm-1 col-xs-1 graf_geo_all"><div class="graf_geo_in graf_geo6"></div></div>
                <div class="col-md-1 col-sm-1 col-xs-1 graf_geo_all"><div class="graf_geo_in graf_geo7"></div></div>
                <div class="col-md-1 col-sm-1 col-xs-1 graf_geo_all"><div class="graf_geo_in graf_geo8"></div></div>
                <div class="col-md-1 col-sm-1 col-xs-1 graf_geo_all"><div class="graf_geo_in graf_geo9"></div></div>
                <div class="col-md-1 col-sm-1 col-xs-1 graf_geo_last"><div class="graf_geo_in graf_geo10"></div></div>
                <div class="div_response div_response_margin" style="left:{{$left}}">
                    <div class="triangulo_pop_response"></div>
                    <div class="pop_response">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            {{$tipo}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-xs-12">
                <div class="section_line_div">
                    <hr class="section_line">
                </div>
            </div>
        </div>
    </section>
    {{--@if($geo_check['status'] == 200)--}}
    {{--MEDIA CHECK--}}
    <section class="col-md-12 col-xs-12 section_padding_bottom">
        <div class="container">
            <div class="col-md-12 col-xs-12">
                <h2>Media check</h2>
                @if($media_check['score'] == 'OK')
                    <p class="section_p" id="demoResp_msg_geo_cf">No hay alertas relativas a venta de hogar ni por búsqueda de trabajo.</p>
                @elseif($media_check['hits']['milanuncios']->score_vivienda == 'KO' || $media_check['hits']['vibbo']->score_vivienda == 'KO' || $media_check['hits']['idealista']->score_vivienda == 'KO')
                    @if($media_check['hits']['milanuncios']->score_trabajo == 'KO' || $media_check['hits']['vibbo']->score_trabajo == 'KO')
                        <p class="section_p" id="demoResp_msg_geo_cf">El cliente está vendiendo su casa y en búsqueda activa de trabajo.
                    @else
                        <p class="section_p" id="demoResp_msg_geo_cf">El cliente está vendiendo su vivienda.</p>
                    @endif
                @else

                @endif
            </div>
            <div class="col-md-12 col-xs-12">
                <div class="section_line_div">
                    <hr class="section_line">
                </div>
            </div>
        </div>
    </section>

    {{--Risk Score--}}
    <section class="col-md-12 col-xs-12 section_padding_bottom">
        <div class="container">
            <div class="col-md-12 col-xs-12">
                <h2>Risk score</h2>
                <div class="text_graf_first">300</div>
                <div class="text_graf_mid">500</div>
                <div class="text_graf_first">800</div>
                <div class="col-md-1 col-sm-1 col-xs-1 first_graf"></div>
                <div class="col-md-1 col-sm-1 col-xs-1 med_graf"></div>
                <div class="col-md-1 col-sm-1 col-xs-1 med_graf"></div>
                <div class="col-md-1 col-sm-1 col-xs-1 med_graf"></div>
                <div class="col-md-1 col-sm-1 col-xs-1 med_graf"></div>
                <div class="col-md-1 col-sm-1 col-xs-1 med_graf"></div>
                <div class="col-md-1 col-sm-1 col-xs-1 med_graf"></div>
                <div class="col-md-1 col-sm-1 col-xs-1 med_graf"></div>
                <div class="col-md-1 col-sm-1 col-xs-1 med_graf"></div>
                <div class="col-md-1 col-sm-1 col-xs-1 med_graf"></div>
                <div class="col-md-1 col-sm-1 col-xs-1 med_graf"></div>
                <div class="col-md-1 col-sm-1 col-xs-1 last_graf"></div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="div_response div_cc div_cc_{{str_replace('+','_plus',str_replace('-','_minus',$risk_score['hits']->rating_eq))}}" style="left:{{(86.5*$risk_score['hits']->score)/850}}%">
                        <div class="triangulo_pop_response"></div>
                        <div class="pop_response pop_response_risk">
                            <div class="col-md-6 col-sm-6 col-xs-6" style="text-align: right">
                                {{$risk_score['hits']->score}}
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6" style="text-align: left">
                                {{$risk_score['hits']->rating_eq}}
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                {{$text_risk}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-xs-12">
                    <div class="section_line_div">
                        <hr class="section_line">
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{--VARIABLES PERSONALES--}}
    <section class="col-md-12 col-xs-12 section_padding_bottom">
        <div class="container">
            <div class="col-md-12 col-xs-12">
                <h2>Variables personales</h2>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
                <p class="p_titulo_res" id="demoResp_msg_vp_edad">Edad</p>
                <p class="p_res">{{$risk_score['hits']->edad}}</p>
                <p class="p_titulo_res" id="demoResp_msg_vp_estudios">Nivel de estudios</p>
                <p class="p_res">
                    @if($risk_score['hits']->nivel_estudios == 1)
                        Estudios primarios
                    @elseif($risk_score['hits']->nivel_estudios == 2)
                        Estudios secundarios
                    @else
                        Estudios superiores
                    @endif
                </p>
                <p class="p_titulo_res" id="demoResp_msg_vp_dependencia">Dependencia económica</p>
                <p class="p_res">
                    @if($risk_score['hits']->dependiente == 'S')
                        Dependiente
                    @else
                        Independiente
                    @endif
                </p>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
                <p class="p_titulo_res" id="demoResp_msg_vp_morosidad">Tipo probable de morosidad</p>
                <p class="p_res">{{$tipo_moroso}}</p>
                <p class="p_titulo_res" id="demoResp_msg_vp_empleo">Probabilidad de empleo</p>
                <p class="p_res">{{$risk_score['hits']->trabajo}}%</p>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
                <p class="p_titulo_res" id="demoResp_msg_vp_movil">Teléfono movil</p>
                <p class="p_res">{{$id_fraud['hits']['phone_info']['number']}}</p>
                <p class="p_titulo_res" id="demoResp_msg_vp_porta">Última portabilidad</p>
                {{--AÑADIR CONTROL POR SI NO HAY ULTIMA PORTABILIDAD--}}
                <p class="p_res">{{date('d/m/Y', strtotime($id_fraud['hits']['phone_info']['lastPortability']['when']))}}</p>
                <p class="p_titulo_res" id="demoResp_msg_vp_compañia">Compañia telefónica</p>
                <p class="p_res">{{$id_fraud['hits']['phone_info']['lastPortability']['from']}} > {{$id_fraud['hits']['phone_info']['lastPortability']['to']}}</p>
            </div>
            <div class="col-md-12 col-xs-12">
                <div class="section_line_div">
                    <hr class="section_line">
                </div>
            </div>
        </div>
    </section>
    {{--Datos de la vivienda--}}
    <section class="col-md-12 col-xs-12 section_padding_bottom">
        <div class="container">
            <div class="col-md-12 col-xs-12">
                <h2>Datos de la vivienda</h2>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
                <p class="p_titulo_res" id="demoResp_msg_house_title">Dirección completa</p>
                <p class="p_res">{{$geo_check['hits']['address']}}</p>
                <div class="col-md-6 col-sm-8 col-xs-6 section_no_padding_left">
                    <p class="p_titulo_res" id="demoResp_msg_house_tipo">Tipo de inmueble</p>
                    <p class="p_res">
                        @if($geo_check['hits']['type'] == 'flat')
                            Piso
                        @else
                            Casa
                        @endif
                    </p>
                    <p class="p_titulo_res" id="demoResp_msg_house_superficie">Superficie</p>
                    <p class="p_res">{{$geo_check['hits']['m2']}}</p>
                    <p class="p_titulo_res" id="demoResp_msg_house_precio">Precio de venta de mercado</p>
                    <p class="p_res">{{number_format($risk_score['hits']->precio, 2, ",", ".")}}€</p>
                </div>
                <div class="col-md-6 col-sm-8 col-xs-6 section_no_padding_right">
                    <p class="p_titulo_res" id="demoResp_msg_house_uso">Uso destinado a</p>
                    <p class="p_res">
                        @if($geo_check['hits']['used_for'] == 'A')
                            Almacén -Estacionamiento
                        @elseif($geo_check['hits']['used_for'] == 'V')
                            Residencial
                        @elseif($geo_check['hits']['used_for'] == 'I')
                            Industrial
                        @elseif($geo_check['hits']['used_for'] == 'O')
                            Oficinas
                        @elseif($geo_check['hits']['used_for'] == 'C')
                            Comercial
                        @elseif($geo_check['hits']['used_for'] == 'K')
                            Deportivo
                        @elseif($geo_check['hits']['used_for'] == 'T')
                            Espectáculos
                        @elseif($geo_check['hits']['used_for'] == 'G')
                            Ocio y Hostelería
                        @elseif($geo_check['hits']['used_for'] == 'Y')
                            Sanidad y Beneficiencia
                        @elseif($geo_check['hits']['used_for'] == 'E')
                            Cultural
                        @elseif($geo_check['hits']['used_for'] == 'R')
                            Religioso
                        @elseif($geo_check['hits']['used_for'] == 'M')
                            Obras en urbanización y jardinería, suelos sin edificar
                        @elseif($geo_check['hits']['used_for'] == 'P')
                            Edificio singular
                        @elseif($geo_check['hits']['used_for'] == 'B')
                            Almacén agrario
                        @elseif($geo_check['hits']['used_for'] == 'J')
                            Industrial agrario
                        @elseif($geo_check['hits']['used_for'] == 'Z')
                            Agrario
                        @else
                            No definido
                        @endif
                    </p>
                    <p class="p_titulo_res" id="demoResp_msg_house_ref">Referencia catastral</p>
                    <p class="p_res">{{$geo_check['hits']['parcela']}}</p>
                    <p class="p_titulo_res" id="demoResp_msg_house_tenencia">Régimen de tenencia</p>
                    <p class="p_res">
                        @if($risk_score['hits']->tenencia == 1)
                            Compra con pagos pendientes
                        @elseif($risk_score['hits']->tenencia == 2)
                            Compra sin pagos pendientes
                        @elseif($risk_score['hits']->tenencia == 3)
                            Alquilada
                        @elseif($risk_score['hits']->tenencia == 4)
                            Cedida
                        @else
                            Otra
                        @endif
                    </p>
                </div>
            </div>
            <div class="col-md-4 col-sm-8 col-xs-4">
                @if(Request::path() == 'pdf/getPdf')
                    <img src="https://maps.googleapis.com/maps/api/staticmap?center={{$geo_check['hits']['latitude']}},{{$geo_check['hits']['longitude']}}&zoom=15&size=300x200&maptype=roadmap&markers=color:red|{{$geo_check['hits']['latitude']}},{{$geo_check['hits']['longitude']}}&key=AIzaSyCypnAH84wyzexRIQDCm9ADQDlgnsAxkz0">
                @else
                    <div id="map" style="height: 200px; width:302px"></div>
                @endif
                {{--<div id="static_map" style="height: 200px; width:100%"></div>--}}
                {{--<img src="https://maps.googleapis.com/maps/api/staticmap?center={{$geo_check['hits']['latitude']}},{{$geo_check['hits']['longitude']}}&zoom=8&size=400x200&key=AIzaSyCFabxYkX1IKE0JWKbgGYfQguBNPXYFBNw">--}}
                <p class="p_titulo_res" id="demoResp_msg_house_datos_adi">Datos adicionales de la vivienda</p>
            </div>
            <div class="col-md-12 col-xs-12">
                <div class="section_line_div">
                    <hr class="section_line">
                </div>
            </div>
        </div>
    </section>

    {{--Variables económicas--}}
    <section class="col-md-12 col-xs-12 section_padding_bottom">
        <div class="container">
            <div class="col-md-12 col-xs-12">
                <h2>Variables económicas</h2>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
                <p class="p_titulo_res" id="demoResp_msg_eco_ingresoshogar">Ingresos netos hogar</p>
                <p class="p_res">{{number_format($risk_score['hits']->ingreso_mensual_hogar, 2, ",", ".")}}€</p>
                <p class="p_titulo_res" id="demoResp_msg_eco_ingrasosindividual">Ingresos netos individuales</p>
                <p class="p_res">{{number_format($risk_score['hits']->ingreso_mensual_individual, 2, ",", ".")}}€</p>
                <p class="p_titulo_res" id="demoResp_msg_eco_impago">Probabilidad de impago</p>
                <p class="p_res">{{number_format($risk_score['hits']->probabilidad_impago, 2, ",", ".")}}%</p>
                <p class="p_titulo_res" id="demoResp_msg_mediopago">Periodo medio de pago</p>
                <p class="p_res">{{$risk_score['hits']->periodo_medio_pago}} días.</p>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
                <p class="p_titulo_res" id="demoResp_msg_eco_rentavivienda">Renta vivienda</p>
                <p class="p_res">{{number_format($risk_score['hits']->renta_vivienda_hogar, 2, ",", ".")}}€</p>
                <p class="p_titulo_res" id="demoResp_msg_eco_rentaindividual">Renta vivienda individual</p>
                <p class="p_res">{{number_format($risk_score['hits']->renta_vivienda_individual, 2, ",", ".")}}€</p>
                <p class="p_titulo_res" id="demoResp_msg_eco_esfuerza">Tasa de esfuerzo</p>
                <p class="p_res">{{number_format($risk_score['hits']->tasa_esfuerzo, 2, ",", ".")}}%</p>
                <p class="p_titulo_res" id="demoResp_msg_eco_desviacion">Desviación de la renta</p>
                <p class="p_res">{{number_format($risk_score['hits']->desviacion_renta, 2, ",", ".")}}%</p>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
                <p class="p_titulo_res" id="demoResp_msg_eco_ahorro">Capacidad de ahorro</p>
                <p class="p_res">{{number_format($risk_score['hits']->capacidad_ahorro, 2, ",", ".")}}€</p>
                <p class="p_titulo_res" id="demoResp_msg_eco_fiapago">Fiabilidad de pago</p>
                <p class="p_res">{{number_format($risk_score['hits']->fiabilidad_pago, 2, ",", ".")}}%</p>
                <p class="p_titulo_res" id="demoResp_msg_eco_desempleo">Probabilidad de desempleo</p>
                <p class="p_res">{{$risk_score['hits']->trabajo}}%</p>
                <p class="p_titulo_res" id="demoResp_msg_eco_pobreza">Umbral de pobreza</p>
                <p class="p_res">{{number_format($risk_score['hits']->umbral_pobreza, 2, ",", ".")}}%</p>
            </div>
            <div class="col-md-12 col-xs-12">
                <div class="section_line_div">
                    <hr class="section_line">
                </div>
            </div>
        </div>
    </section>
    {{--Gastos medios estimados por individuo y gastos por hogar--}}
    <section class="col-md-12 col-xs-12 section_padding_bottom">
        <div class="container">
            <div class="col-md-8 col-xs-8">
                <div class="col-md-12 col-xs-12 section_no_padding_right">
                    <h2>Gastos medios estimados por individuo</h2>
                    <div id="pieChart" style="width:400px;height:400px;"></div>
                </div>
            </div>
            <div class="col-md-4 col-xs-4">
                <div class="col-md-12 col-xs-12 section_no_padding_left">
                    <h2>Gastos por hogar</h2>
                    <p class="p_titulo_res" id="demoResp_msg_gh_alimen">Alimentación: {{number_format($risk_score['hits']->grupo_gastos_hogar->alimentacion, 2, ",", ".")}}€</p>
                    <p class="p_titulo_res" id="demoResp_msg_gh_alcohol">Alcohol y tabaco: {{number_format($risk_score['hits']->grupo_gastos_hogar->alcohol_tabaco, 2, ",", ".")}}€</p>
                    <p class="p_titulo_res" id="demoResp_msg_gh_ropa">Ropa y calzado: {{number_format($risk_score['hits']->grupo_gastos_hogar->ropa_calzado, 2, ",", ".")}}€</p>
                    <p class="p_titulo_res" id="demoResp_msg_gh_renta">Renta y suministros: {{number_format($risk_score['hits']->grupo_gastos_hogar->vivienda_suministros, 2, ",", ".")}}€</p>
                    <p class="p_titulo_res" id="demoResp_msg_gh_mobiliario">Mobiliario: {{number_format($risk_score['hits']->grupo_gastos_hogar->mobiliario, 2, ",", ".")}}€</p>
                    <p class="p_titulo_res" id="demoResp_msg_gh_salud">Salud: {{number_format($risk_score['hits']->grupo_gastos_hogar->salud, 2, ",", ".")}}€</p>
                    <p class="p_titulo_res" id="demoResp_msg_gh_trans">Transporte: {{number_format($risk_score['hits']->grupo_gastos_hogar->transporte, 2, ",", ".")}}€</p>
                    <p class="p_titulo_res" id="demoResp_msg_gh_comunicaciones">Comunicaciones: {{number_format($risk_score['hits']->grupo_gastos_hogar->comunicaciones, 2, ",", ".")}}€</p>
                    <p class="p_titulo_res" id="demoResp_msg_gh_ocio">Ocio: {{number_format($risk_score['hits']->grupo_gastos_hogar->ocio, 2, ",", ".")}}€</p>
                    <p class="p_titulo_res" id="demoResp_msg_gh_edu">Educación: {{number_format($risk_score['hits']->grupo_gastos_hogar->educacion, 2, ",", ".")}}€</p>
                    <p class="p_titulo_res" id="demoResp_msg_gh_res">Restaurantes: {{number_format($risk_score['hits']->grupo_gastos_hogar->restaurantes, 2, ",", ".")}}€</p>
                    <p class="p_titulo_res" id="demoResp_msg_gh_otros">Otros: {{number_format($risk_score['hits']->grupo_gastos_hogar->otros, 2, ",", ".")}}€</p>
                </div>
            </div>
        </div>
    </section>
@endif


@if(Request::path() == 'pdf/getPdf')
    <script src="/var/www/api/public/assets/js/d3.min.js"></script>
    <script src="/var/www/api/public/assets/js/d3pie.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var pie = new d3pie("pieChart", {
            size: {
                canvasHeight: 450,
                canvasWidth: 600
            },
            data: {
                content: [
                    { label: 'Alimentacion: {{number_format($risk_score['hits']->grupo_gastos_individual->alimentacion, 2, ",", ".")}} €', value: {{$risk_score['hits']->grupo_gastos_individual->alimentacion}}},
                    { label: 'Alcohol y tabaco: {{number_format($risk_score['hits']->grupo_gastos_individual->alcohol_tabaco, 2, ",", ".")}} €', value: {{$risk_score['hits']->grupo_gastos_individual->alcohol_tabaco}}},
                    { label: 'Ropa y calzado: {{number_format($risk_score['hits']->grupo_gastos_individual->ropa_calzado, 2, ",", ".")}} €', value: {{$risk_score['hits']->grupo_gastos_individual->ropa_calzado}}},
                    { label: 'Vivienda y suministros: {{number_format($risk_score['hits']->grupo_gastos_individual->vivienda_suministros, 2, ",", ".")}} €', value: {{$risk_score['hits']->grupo_gastos_individual->vivienda_suministros}}},
                    { label: 'Mobiliario: {{number_format($risk_score['hits']->grupo_gastos_individual->mobiliario, 2, ",", ".")}} €', value: {{$risk_score['hits']->grupo_gastos_individual->mobiliario}}},
                    { label: 'Salud: {{number_format($risk_score['hits']->grupo_gastos_individual->salud, 2, ",", ".")}} €', value: {{$risk_score['hits']->grupo_gastos_individual->salud}}},
                    { label: 'Otros: {{number_format($risk_score['hits']->grupo_gastos_individual->otros, 2, ",", ".")}} €', value: {{$risk_score['hits']->grupo_gastos_individual->otros}}},
                    { label: 'Ocio: {{number_format($risk_score['hits']->grupo_gastos_individual->ocio, 2, ",", ".")}} €', value: {{$risk_score['hits']->grupo_gastos_individual->ocio}}},
                    { label: 'Educación: {{number_format($risk_score['hits']->grupo_gastos_individual->educacion, 2, ",", ".")}} €', value: {{$risk_score['hits']->grupo_gastos_individual->educacion}}},
                    { label: 'Restaurantes: {{number_format($risk_score['hits']->grupo_gastos_individual->restaurantes, 2, ",", ".")}} €', value: {{$risk_score['hits']->grupo_gastos_individual->restaurantes}}},
                    { label: 'Comunicaciones: {{number_format($risk_score['hits']->grupo_gastos_individual->comunicaciones, 2, ",", ".")}} €', value: {{$risk_score['hits']->grupo_gastos_individual->comunicaciones}}},
                    { label: 'Transporte: {{number_format($risk_score['hits']->grupo_gastos_individual->transporte, 2, ",", ".")}} €', value: {{$risk_score['hits']->grupo_gastos_individual->transporte}}}
                ]
            },
            effects:{
                load: {
                    effect: "none" // none / default
                }
            },
            labels: {
                outer: {
                    pieDistance: 30 //Distancia del label con el grafico
                },
                inner: {
                    format: "none"
                },
                lines: {
                    enabled: true,
                    style: "straight",
                    color: "#4a4a4a"
                },
                mainLabel: {
                    color: "#4a4a4a",
                    font: "Montserrat",
                    fontSize: 10
                }
            },
            misc: {
                colors: {
                    segmentStroke: 'none'
                }
            }
        });
    });
</script>
<script type="text/javascript">
//    window.onload = function() {
//        var svg = $('#pieChart').clone().wrap("<div>").parent().html(),
//            dataURL = ("data:image/svg+xml;utf8," + encodeURI(svg));
//        $('#pieChart').html('<img/>').attr('src',dataURL);
//
//    };
</script>
@endif