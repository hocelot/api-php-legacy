<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="shortcut icon" href="/assets/img/new-api/favicon.ico">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    @yield('meta')
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/css/bootstrap-select.min.css">
    {{--<link href="/assets/css/new-main.css" rel="stylesheet"/>--}}
    <link rel="stylesheet" type="text/css" href="/assets/css/jquery.cookiebar.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">
    <link href="/assets/css/hocelot.css" rel="stylesheet">
    <script src="/assets/js/jquery.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/jquery.i18n.properties.min.js"></script>
    <script type="text/javascript" src="/assets/js/jquery.cookiebar.js"></script>
    @yield('css')
    <script>
        (function (b, o, i, l, e, r) {
            b.GoogleAnalyticsObject = l;
            b[l] || (b[l] =
                function () {
                    (b[l].q = b[l].q || []).push(arguments)
                });
            b[l].l = +new Date;
            e = o.createElement(i);
            r = o.getElementsByTagName(i)[0];
            e.src = 'https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e, r)
        }(window, document, 'script', 'ga'));
        ga('create', 'UA-79365623-1');
        ga('send', 'pageview');
    </script>
</head>
<body>
<!--[if IE]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
    @if(Request::path() === '/')
    <script src="/assets/js/carousel.js"></script>
    <div id="mycarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <div class="item">
                <img src="/assets/img/new-api/IMG_Home_Hocelot_desktopHD_2000.png" data-color="white" alt="First Image">
                {{--<img id="home_img_carousel" data-color="white" alt="First Image">--}}
                <div class="carousel-caption">
                    <div class="carousel_div">
                        <h4 class="carousel_title">Real Data.<br>
                            Real Time.</h4>
                        <p>
                            <a class="btn button_section6 carousel_button" href="/demo" role="button">USA NUESTRA DEMO</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
<!-- Menú escritorio DC-->
<section class="hocelot-header col-md-12">
    <nav class="navbar navbar-hocelot" role="navigation">
        <div class="nav-hocelot-center">
            <div class="collapse navbar-collapse" id="MenuHocelot">
                <ul class="nav navbar-nav">
                    <li>
                    @if(Request::path() === 'smart-data')
                        <a class="text-center main_menu active" href="/smart-data">Smart Data</a>
                    @else
                        <a class="text-center main_menu" href="/smart-data">Smart Data</a>
                    @endif
                    </li>
                    <li>
                    @if(Request::path() === 'smart-analytics')
                        <a class="text-center main_menu active" href="/smart-analytics">Smart Analytics</a>
                    @else
                        <a class="text-center main_menu" href="/smart-analytics">Smart Analytics</a>
                    @endif
                    </li>
                    <li>
                        <a class="text-center navbar-brand" href="/"><img src="/assets/img/new-api/hocelot-logotipo.png"/></a>
                    </li>
                    <li>
                    @if(Request::path() === 'sobre-nosotros')
                        <a class="text-center main_menu active" href="/sobre-nosotros">Sobre nosotros</a>
                    @else
                        <a class="text-center main_menu" href="/sobre-nosotros">Sobre nosotros</a>
                    @endif
                    </li>
                    <li>
                    @if(Request::path() === 'contrata')
                        <a class="text-center main_menu active" href="/contrata">Contrata</a>
                    @else
                        <a class="text-center main_menu" href="/contrata">Contrata</a>
                    @endif
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
</section>
<!-- Menu scroll up -->
<section class="header-fixed nav-up">
    <nav class="navbar navbar-hocelot" role="navigation">
        <div class="nav-hocelot-center">
            <div class="collapse navbar-collapse" id="MenuHocelotFixed">
                <ul class="nav navbar-nav">
                    <li>
                        @if(Request::path() === 'smart-data')
                            <a class="text-center main_menu active" href="/smart-data">Smart Data</a>
                        @else
                            <a class="text-center main_menu" href="/smart-data">Smart Data</a>
                        @endif
                    </li>
                    <li>
                        @if(Request::path() === 'smart-analytics')
                            <a class="text-center main_menu active" href="/smart-analytics">Smart Analytics</a>
                        @else
                            <a class="text-center main_menu" href="/smart-analytics">Smart Analytics</a>
                        @endif
                    </li>
                    <li>
                        <a class="text-center navbar-brand" href="/"><img src="/assets/img/new-api/hocelot-logotipo.png"/></a>
                    </li>
                    <li>
                        @if(Request::path() === 'sobre-nosotros')
                            <a class="text-center main_menu active" href="/sobre-nosotros">Sobre nosotros</a>
                        @else
                            <a class="text-center main_menu" href="/sobre-nosotros">Sobre nosotros</a>
                        @endif
                    </li>
                    <li>
                        @if(Request::path() === 'contrata')
                            <a class="text-center main_menu active" href="/contrata">Contrata</a>
                        @else
                            <a class="text-center main_menu" href="/contrata">Contrata</a>
                        @endif
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</section>
<!--Menú movil -->
<section class="hocelot-header-mobile col-xs-12">
    <nav class="navbar navbar-hocelot-mobile navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle button-navbar-mobile glyphicon glyphicon-menu-hamburger" id="buttonRespNav" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" ></button>
                <div class="img_menu">
                    <a class="navbar-brand" href="/"><img src="/assets/img/new-api/hocelot-logotipo.png"/></a>
                </div>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a class="text-center main_menu_mobile" href="/smart-data">Smart Data</a>
                    </li>
                    <li>
                        <a class="text-center main_menu_mobile" href="/smart-analytics">Smart Analytics</a>
                    </li>
                    <li>
                        <a class="text-center main_menu_mobile" href="/sobre-nosotros">Sobre nosotros</a>
                    </li>
                    <li>
                        <a class="text-center main_menu_mobile" href="/contrata">Contrata</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
</section>
@yield('content')
<footer class="footer-hocelot col-md-12 col-xs-12 text-center">
    <div class="col-md-12 col-xs-12 padding_footer">
        <img src="/assets/img/hocelot-logotipo-tagline-negativo.png" class="logo_footer">
    </div>
    <div class="col-md-12 col-xs-12 padding_footer">
        <div class="container_footer">
            <div class="col-md-12 visible-md-block visible-lg-block space_footer_top"></div>
            <div class="col-md-4 col-xs-12">
                <p class="title_footer">Smart Data</p>
                <p class="p_footer">
                    <a href="/smart-data/smooth-client-onboarding" class="a_footer">Smooth Client Onboarding</a>
                </p>
                <p class="p_footer">
                    <a href="/smart-data/standardization-enhacement" class="a_footer">Standardization & Enhancement</a>
                </p>
                <p class="p_footer">
                    <a href="/smart-data/enrichment" class="a_footer">Enrichment</a>
                </p>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="col-md-2 visible-md-block visible-lg-block"  style="margin-left: -15px"></div>
                <div class="col-md-10">
                    <p class="title_footer">Smart Analytics</p>
                    <p class="p_footer">
                        <a href="/smart-analytics/id-check-fraud" class="a_footer">ID Check & Fraud</a>
                    </p>
                    <p class="p_footer">
                        <a href="/smart-analytics/geo-check-fraud" class="a_footer">Geo Check & Fraud</a>
                    </p>
                    <p class="p_footer">
                        <a href="/smart-analytics/risk-score" class="a_footer">Risk Score</a>
                    </p>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="col-md-2 visible-md-block visible-lg-block"></div>
                <div class="col-md-10">
                    <p class="title_footer">¿Te interesa?</p>
                    <p class="p_footer">
                        <a href="/demo" class="a_footer">Usa nuestra Demo</a>
                    </p>
                    <p class="p_footer">
                        <a href="/contrata" class="a_footer">Contrata Hocelot</a>
                    </p>
                    <p class="p_footer">
                        <a href="/sobre-nosotros" class="a_footer">Sobre nosotros</a>
                    </p>
                    <p class="p_footer">
                        Tel: <a href="tel:+34917377765" class="a_footer">91 7377765</a>
                    </p>
                </div>
            </div>
            <div class="col-md-12 visible-md-block visible-lg-block space_footer_bottom"></div>
        </div>
    </div>
    <div class="col-md-12 col-xs-12 padding_footer">
        <div class="col-md-6 col-xs-6">
            <a target="_blank" href="https://www.linkedin.com/company/hocelot?trk=biz-companies-cym"><img class="rs_left_footer" alt="Icon linkedin" src="/assets/img/new-api/in-white-34-px-tm.png"/> </a>
        </div>
        <div class="col-md-6 col-xs-6">
            <a target="_blank" href="https://twitter.com/hocelot_spain"><img class="rs_right_footer" alt="Icon twitter" src="/assets/img/new-api/logo-x-2014-fixed.png"/> </a>
            <a id="gotTop" href="javascript:void(0)"><img class="rs_left_footer" alt="Icon twitter" src="/assets/img/new-api/arrow-bold-down.png"/></a>
        </div>
    </div>
    <div class="col-md-12 col-xs-12 footer-white">
        <div class="col-md-7 col-xs-12 footer_legal_div">
            <div class="col-md-3 col-xs-12 footer_link_div">
                <a class="footer_link" href="/condiciones">Condiciones de uso</a>
            </div>
            <div class="col-md-3 col-xs-12 footer_link_div">
                <a class="footer_link" href="/politica-de-privacidad">Política de privacidad</a>
            </div>
            <div class="col-md-3 col-xs-12 footer_link_div">
                <a class="footer_link" href="/politica-de-cookies">Política de cookies</a>
            </div>
            <div class="col-md-3 col-xs-12 footer_link_div">
                <a class="footer_link" href="/aviso-legal">Aviso legal</a>
            </div>
        </div>
        <div class="col-md-5 col-xs-12 footer_link_div">
            <p class="footer_text">Hocelot Finacial Technologies, SL © Copyright 2017</p>
        </div>
    </div>
</footer>
@yield('js')
<script src="/assets/js/new-main.js"></script>
<script>
    $(document).ready(function(){
        $.cookieBar();
    });
    <!-- Hotjar Tracking Code for www.hocelot.com -->
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:505145,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>
</body>
</html>