@extends('newApi.templates.landing')
@section('meta')
    <title>Contrata | Hocelot</title>
    <meta name="description" content="Nuestro servicio se configura según tus necesidades. Contacta con nosotros y te ayudamos a encontrar la mejor fórmula para tu negocio.">
    <meta name="keywords" content="contacta, te ayudamos">
@endsection
@section('css')
    <link href="/assets/css/new-contrata.css" rel="stylesheet"/>
@endsection
@section('content')
    <section class="col-md-12 col-xs-12 section_padding_bottom80">
        <div class="container">
            <div class="col-md-1 visible-md-block visible-lg-block"></div>
            <div class="col-md-8">
                <ol class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    <li class="active"><a href="#">Contrata</a></li>
                </ol>
                <h1>Contrata</h1>
                <div class="section_line_div">
                    <hr class="section_line"/>
                </div>
                <p id="contact_msg_inlet_description" class="title_section1 text-left"></p>

            </div>
            <div class="col-md-3 visible-md-block visible-lg-block"></div>
        </div>
    </section>
    <section class="col-md-12 col-xs-12 section_background section_padding">
        <div class="container">
            <div class="col-md-12 col-xs-12">
                <div class="col-md-4 col-xs-12 section_background_white section_margin_right section_padding_all section_responsive_margin_bottom section_responsive_no_margins margin-left20 similar_height_con">
                    <div class="col-md-12 col-xs-12">
                        <div class="div_full_width_contrata">
                            <img class="full_width_contrata" id="contact_img_services1_src" alt="Icono actualiza" src=""/>
                        </div>
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <div><h2 id="contact_msg_services1_title" class="section_subtitle"></h2></div>
                        <div class="section_line_div">
                            <hr class="section_line"/>
                        </div>
                        <div><p id="contact_msg_services1_description" class="section_p"></p></div>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12 section_background_white section_margin_right section_padding_all section_margin_left section_responsive_margin_bottom section_responsive_no_margins margin-left10 similar_height_con">
                    <div class="col-md-12 col-xs-12">
                        <div class="div_full_width_contrata">
                            <img class="full_width_contrata" id="contact_img_services2_src" alt="Icono fraude" src=""/>
                        </div>
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <div><h2 id="contact_msg_services2_title" class="section_subtitle"></h2></div>
                        <div class="section_line_div">
                            <hr class="section_line"/>
                        </div>
                        <div><p id="contact_msg_services2_description" class="section_p"></p></div>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12 section_background_white section_margin_left section_padding_all section_responsive_margin_bottom section_responsive_no_margins margin-right20 similar_height_con"  style="margin-right: -20px;">
                    <div class="col-md-12 col-xs-12">
                        <div class="div_full_width_contrata">
                            <img class="full_width_contrata" id="contact_img_services3_src" alt="Icono gana" src=""/>
                        </div>
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <div><h2 id="contact_msg_services3_title" class="section_subtitle"></h2></div>
                        <div class="section_line_div">
                            <hr class="section_line"/>
                        </div>
                        <div><p id="contact_msg_services3_description" class="section_p"></p></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="col-md-12 col-xs-12 section_padding">
        <div class="container">
            <div class="col-md-1 visible-md-block visible-lg-block height_input"></div>
            <div class="col-md-10 col-sm-10 col-xs-12">
                <p class="p-destacado">
                    Déjanos tus datos y nos pondremos en contacto contigo
                </p>
            </div>
            <div class="col-md-1 visible-md-block visible-lg-block height_input"></div>
            <div class="col-md-1 visible-md-block visible-lg-block height_input"></div>
            <div class="col-md-5 col-sm-6 col-xs-12">
                <div class="placeMin" style="height:5px">
                    <text id="placenombre" style="display:none">Nombre</text>
                </div>
                <input type="text" class="form-control input_label" placeholder="Nombre" id="nombre" onBlur="checkInput(this)" onFocus="infoInput(this)" onkeyup="getPlaceMin(this)">
            </div>
            <div class="col-md-5 col-sm-6 col-xs-12">
                <div class="placeMin" style="height:5px">
                    <text id="placeapellidos" style="display:none">Apellidos</text>
                </div>
                <input type="text" class="form-control input_label" placeholder="Apellidos" id="apellidos" onBlur="checkInput(this)" onFocus="infoInput(this)" onkeyup="getPlaceMin(this)">
            </div>
            <div class="col-md-1 visible-md-block visible-lg-block height_input"></div>
            <div class="col-md-1 visible-md-block visible-lg-block height_input"></div>
            <div class="col-md-5 col-sm-6 col-xs-12">
                <div class="placeMin" style="height:5px">
                    <text id="placecargo" style="display:none">Cargo</text>
                </div>
                <input type="text" class="form-control input_label" placeholder="Cargo" id="cargo" onBlur="checkInput(this)" onFocus="infoInput(this)" onkeyup="getPlaceMin(this)">
            </div>
            <div class="col-md-5 col-sm-6 col-xs-12">
                <div class="placeMin" style="height:5px">
                    <text id="placenombre_emp" style="display:none">Nombre de la empresa</text>
                </div>
                <input type="text" class="form-control input_label" placeholder="Nombre de la empresa" id="nombre_emp" onBlur="checkInput(this)" onFocus="infoInput(this)" onkeyup="getPlaceMin(this)">
            </div>
            <div class="col-md-1 visible-md-block visible-lg-block height_input"></div>
            <div class="col-md-1 visible-md-block visible-lg-block height_input"></div>
            <div class="col-md-5 col-sm-6 col-xs-12">
                <div class="placeMin" style="height:5px">
                    <text id="placeemail" style="display:none">Email</text>
                </div>
                <input type="text" class="form-control input_label" placeholder="Email" id="email" onBlur="checkEmail(this)" onFocus="infoEmail(this)" onkeyup="getPlaceMin(this)">
            </div>
            <div class="col-md-5 col-sm-6 col-xs-12">
                <div class="placeMin" style="height:5px">
                    <text id="placetelefono" style="display:none">Teléfono</text>
                </div>
                <input type="text" class="form-control input_label" placeholder="Teléfono" id="telefono" maxlength="9" onBlur="checkTelf(this)" onFocus="infoTelf(this)" onkeyup="getPlaceMin(this)">
            </div>
            <div class="col-md-1 visible-md-block visible-lg-block height_input"></div>
            <div class="col-md-1 visible-md-block visible-lg-block height_input"></div>
            <div class="col-md-10 col-sm-12 col-xs-12 div_cond">
                <p class="error_contact" id="error_acepto_contacto" style="text-align: left"></p>
                <input type="checkbox" name="acepto_contacto" tabindex="20" id="acepto_contacto" onClick="checkCheckbox(this)"/>
                <label class="condiciones" id="label_check" for="checkbox"> Acepto los <a href="/condiciones" target="_blank">Términos y Condiciones de Uso</a> y la <a href="/politica-de-privacidad" target="_blank">Política de Privacidad de Datos </a>.</label>
            </div>
            <div class="col-md-1 visible-md-block visible-lg-block height_input"></div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="margin_auto_cons section_margin_top">
                    <input type="button" class="button_bigest button_hocelot" id="submit_button" value="Enviar">
                </div>
            </div>
        </div>

    </section>
    <!-- Section 8-->
    <section class="col-md-12 col-xs-12 text-center abstract_background">
        <div class="col-md-12 col-xs-12">
            <h3 id="contact_msg_highlighted_title" class="section_big_title section_padding"></h3>
        </div>
    </section>
    <div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg">
            <div class="modal-header modal-contact">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <section class="col-md-12 col-xs-12 modal-contact section_padding_bottom" id="ModalContactBody">
            </section>
        </div>
    </div>
@endsection
@section('js')
    <script src="/assets/js/new-validar.js"></script>
    <script src="/assets/js/new-contacto.js"></script>
    <script>
        jQuery.i18n.properties({
            name: 'Messages',
            path: '/assets/',
            mode: 'both',
//            async: true,
            callback: function () {
                $("[id^='contact_msg']").each(function () {
                    $(this).text(jQuery.i18n.prop($(this).attr('id')));
                });
                $("[id^='contact_img']").each(function () {
                    if($(window).width() < '780') {
                        $(this).attr('src', '/assets/img/new-api/' + jQuery.i18n.prop($(this).attr('id')+'_1x'));
                    }else if($(window).width() > '780' && $(window).width() < '1024'){
                        $(this).attr('src', '/assets/img/new-api/' + jQuery.i18n.prop($(this).attr('id')+'_2x'));
                    }else{
                        $(this).attr('src', '/assets/img/new-api/' + jQuery.i18n.prop($(this).attr('id')+'_3x'));
                    }
                });
            }
        });
    </script>
@endsection