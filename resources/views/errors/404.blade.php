<?php
/**
 * Created by PhpStorm.
 * User: DC
 * Date: 22/05/17
 * Time: 15:20
 */
?>
        <!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="shortcut icon" href="/assets/img/new-api/favicon.ico">
    <title>Error 404 | Hocelot</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/css/bootstrap-select.min.css">
    <link href="/assets/css/new-main.css" rel="stylesheet"/>
    <link href="/assets/css/404.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/jquery.cookiebar.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">
    <link href="/assets/css/hocelot.css" rel="stylesheet">
    <script src="/assets/js/jquery.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/carousel.js"></script>
</head>
<body>
<section class="hocelot-header col-md-12">
    <nav class="navbar navbar-hocelot" role="navigation">
        <div class="nav-hocelot-center" style="top: 10px; position: relative;">
            <div class="collapse navbar-collapse" id="MenuHocelot">
                <ul class="nav navbar-nav">
                    <li>
                        @if(Request::path() === 'smart-data')
                            <a class="text-center main_menu active" href="/smart-data">Smart Data</a>
                        @else
                            <a class="text-center main_menu" href="/smart-data">Smart Data</a>
                        @endif
                    </li>
                    <li>
                        @if(Request::path() === 'smart-analytics')
                            <a class="text-center main_menu active" href="/smart-analytics">Smart Analytics</a>
                        @else
                            <a class="text-center main_menu" href="/smart-analytics">Smart Analytics</a>
                        @endif
                    </li>
                    <li>
                        <a class="text-center navbar-brand" href="/"><img src="/assets/img/new-api/hocelot-logotipo.png"/></a>
                    </li>
                    <li>
                        @if(Request::path() === 'sobre-nosotros')
                            <a class="text-center main_menu active" href="/sobre-nosotros">Sobre nosotros</a>
                        @else
                            <a class="text-center main_menu" href="/sobre-nosotros">Sobre nosotros</a>
                        @endif
                    </li>
                    <li>
                        @if(Request::path() === 'contrata')
                            <a class="text-center main_menu active" href="/contrata">Contrata</a>
                        @else
                            <a class="text-center main_menu" href="/contrata">Contrata</a>
                        @endif
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
</section>
<section class="header-fixed nav-up">
    <nav class="navbar navbar-hocelot" role="navigation">
        <div class="nav-hocelot-center">
            <div class="collapse navbar-collapse" id="MenuHocelotFixed">
                <ul class="nav navbar-nav">
                    <li>
                        @if(Request::path() === 'smart-data')
                            <a class="text-center main_menu active" href="/smart-data">Smart Data</a>
                        @else
                            <a class="text-center main_menu" href="/smart-data">Smart Data</a>
                        @endif
                    </li>
                    <li>
                        @if(Request::path() === 'smart-analytics')
                            <a class="text-center main_menu active" href="/smart-analytics">Smart Analytics</a>
                        @else
                            <a class="text-center main_menu" href="/smart-analytics">Smart Analytics</a>
                        @endif
                    </li>
                    <li>
                        <a class="text-center navbar-brand" href="/"><img src="/assets/img/new-api/hocelot-logotipo.png"/></a>
                    </li>
                    <li>
                        @if(Request::path() === 'sobre-nosotros')
                            <a class="text-center main_menu active" href="/sobre-nosotros">Sobre nosotros</a>
                        @else
                            <a class="text-center main_menu" href="/sobre-nosotros">Sobre nosotros</a>
                        @endif
                    </li>
                    <li>
                        @if(Request::path() === 'contrata')
                            <a class="text-center main_menu active" href="/contrata">Contrata</a>
                        @else
                            <a class="text-center main_menu" href="/contrata">Contrata</a>
                        @endif
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</section>
<!--Menú movil DC-->
<section class="hocelot-header-mobile col-xs-12">
    <nav class="navbar navbar-hocelot-mobile navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle button-navbar-mobile glyphicon glyphicon-menu-hamburger" id="buttonRespNav" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" ></button>
                <div class="img_menu">
                    <a class="navbar-brand" href="/"><img src="/assets/img/new-api/hocelot-logotipo.png"/></a>
                </div>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a class="text-center main_menu_mobile" href="/smart-data">Smart Data</a>
                    </li>
                    <li>
                        <a class="text-center main_menu_mobile" href="/smart-analytics">Smart Analytics</a>
                    </li>
                    <li>
                        <a class="text-center main_menu_mobile" href="/sobre-nosotros">Sobre nosotros</a>
                    </li>
                    <li>
                        <a class="text-center main_menu_mobile" href="/contrata">Contrata</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
</section>
<div id="mycarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
        <div class="item">
            <img src="/assets/img/new-api/IMG_404_Hocelot_desktopHD.png" data-color="white" alt="First Image">
            {{--<img id="home_img_carousel" data-color="white" alt="First Image">--}}
            <div class="carousel-caption">
                <div class="carousel_div">
                    <h4 class="carousel_title">
                        El error humano a
                        veces te lleva a
                        descubrir lugares hermosos.
                        Este no es el caso.</h4>
                    <p>
                        <a class="btn button_section6 carousel_button" href="/" role="button">Vuelve al inicio</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="footer-hocelot col-md-12 col-xs-12 text-center" style="margin-top: -20px">
    <div class="col-md-12 col-xs-12 padding_footer">
        <img src="/assets/img/hocelot-logotipo-tagline-negativo.png" class="logo_footer">
    </div>
    <div class="col-md-12 col-xs-12 padding_footer">
        <div class="col-md-6 col-xs-6">
            <a target="_blank" href="https://www.linkedin.com/company/hocelot?trk=biz-companies-cym"><img class="rs_left_footer" alt="Icon linkedin" src="/assets/img/new-api/in-white-34-px-tm.png"/> </a>
        </div>
        <div class="col-md-6 col-xs-6">
            <a target="_blank" href="https://twitter.com/hocelot_spain"><img class="rs_right_footer" alt="Icon twitter" src="/assets/img/new-api/logo-x-2014-fixed.png"/> </a>
            <a id="gotTop" href="#"><img class="rs_left_footer" alt="Icon twitter" src="/assets/img/new-api/arrow-bold-down.png"/></a>
        </div>
    </div>
    <div class="col-md-12 col-xs-12 footer-white">
        <div class="col-md-7 col-xs-12 footer_legal_div">
            <div class="col-md-3 col-xs-12 footer_link_div">
                <a class="footer_link" href="/condiciones">Condiciones de uso</a>
            </div>
            <div class="col-md-3 col-xs-12 footer_link_div">
                <a class="footer_link" href="/politica-de-privacidad">Política de privacidad</a>
            </div>
            <div class="col-md-3 col-xs-12 footer_link_div">
                <a class="footer_link" href="/politica-de-cookies">Política de cookies</a>
            </div>
            <div class="col-md-3 col-xs-12 footer_link_div">
                <a class="footer_link" href="/aviso-legal">Aviso legal</a>
            </div>
        </div>
        <div class="col-md-5 col-xs-12 footer_link_div">
            <p class="footer_text">Hocelot Finacial Technologies, SL © Copyright 2017</p>
        </div>
    </div>
</footer>
<script src="/assets/js/new-main.js"></script>
<script>
    $(document).ready(function(){
        $.cookieBar();
    });
</script>
</body>
</html>