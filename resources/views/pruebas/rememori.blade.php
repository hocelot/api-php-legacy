@extends('api.templates.landing')
<link rel="shortcut icon" href="/assets/img/favicon.ico">
@section('css')
    <link href="/assets/css/contacto.css" rel="stylesheet"/>
    <style>
        .modal-dialog.modal-content {
            position: relative;
            top: 300px;
            width: 340px;
        }
    </style>
@endsection
<meta name="csrf-token" content="{{ csrf_token() }}" />
@section('content')
    <div class="container" id="send_mens">
        <div class="">
            <div class="col-md-12">
                <div class="container col-md-12">
                    <div class="row">
                        <div class="col-lg-12" style="margin-top: 130px">
                            <div class="landing_titulo solicitar_api">
                                DEMO ESQUELAS
                                <br>ESTEBAN
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2"></div>
                            <div class="col-md-8 col-sm-12 col-xs-12">
                                * Cómo máximo se mostrarán 30 resultados.<br>
                                ** Si sólo se rellena la contraseña se buscará en todas las provincias a día de hoy.
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                        <div class="col-md-2"></div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon input_img"><img src="/assets/img/contacto/icono_usuario.png"/></span>
                                <input type="text" class="form-control input_label" placeholder="Nombre" id="name">
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon input_img"><img src="/assets/img/demo/icono_ciudad.png"/></span>
                                <input type="text" class="form-control input_label" placeholder="Ciudad" id="place">
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon input_img"><img style="height: 20px;" src="http://app.hocelot.com/img/login/icono_password.png"/></span>
                                <input type="password" class="form-control input_label" placeholder="Contraseña" id="pass" required>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon input_img"><img src="/assets/img/demo/icono_fecha.png"/></span>
                                <input type="text" class="form-control input_label" placeholder="Fecha inicial" onfocus="infoFech(this)" onblur="delInfoFech(this)" id="fecha_ini">
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon input_img"><img src="/assets/img/demo/icono_fecha.png"/></span>
                                <input type="text" class="form-control input_label" placeholder="Fecha Final" onfocus="infoFech(this)" onblur="delInfoFech(this)" id="fecha_hasta">
                            </div>
                            <div class="input-group" style="width: 100%; margin: 0 auto;">
                                <input id="submit" type="button" value="Enviar" style="margin-top: 5px;" class="boton_demo boton_mostrar">
                            </div>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                    <div class="col-md-2"></div>
                    <div id="response_body" class="col-md-8" style="margin-top: 15px; max-height: 200px; overflow: auto;">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="loadingModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-content">
            <div id="loading">
                <img src="/assets/img/demo/gif_procesando_peticion.gif"/>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="/assets/js/pruebas/DemoRm.js"></script>
@endsection