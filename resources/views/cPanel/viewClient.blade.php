@extends('cPanel.templates.header')
@section('css')
    <link rel="stylesheet" href="/assets/css/addClient.css">
@endsection
@section('content')
    <section class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            {{--<h1 class="page-header"><p class="titulo_cons_head">Crear Cliente</p></h1>--}}
            <div class="demo_titulo">
                Clientes registrados
            </div>
            {{--<h1 class="page-header"><p class="titulo_cons_head">Clientes</p></h1>--}}
            <div class="col-md-12">
                <table id="grid-data-api" class="table table-condensed table-hover table-striped tabla_admin">
                    <thead>
                    <tr>
                        <th data-column-id="id" data-type="text" data-identifier="true" data-order="asc">ID</th>
                        <th data-column-id="name" data-order="desc">Nombre</th>
                        <th data-column-id="email" data-formatter="email" data-order="desc">Usuario</th>
                        <th data-column-id="site" data-formatter="site" data-order="desc">Site</th>
                        <th data-column-id="joinDate" data-order="desc">Fecha registro</th>
                        {{--<th data-column-id="lastLogin" data-order="desc">último acceso</th>--}}
                        <th data-column-id="commands" data-formatter="commands" data-sortable="false"></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </section>
    </section>
    <div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title" id="tit_client">Vista detallada cliente</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="show" style="overflow-x: auto">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="/assets/js/jquery.bootgrid.js"></script>
    <script src="/assets/js/viewClient.js"></script>
@endsection