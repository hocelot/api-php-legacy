<?php
/**
 * Created by PhpStorm.
 * User: DC
 * Date: 31/10/2016
 * Time: 11:00
 */
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Hocelot-Administrator</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    {{--<link rel="stylesheet" href="/assets/css/bootstrap.min.css">--}}
    {{--<link rel="stylesheet" href="/assets/css/font-awesome.css">--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

    <link rel="stylesheet" href="/assets/css/account.css">
    <link rel="stylesheet" href="/assets/css/skins/skin-black.css">

    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/cpanel_clients.css">
    <link rel="stylesheet" href="/assets/css/jquery-ui.css">
    <link rel="stylesheet" href="/assets/css/jquery.bootgrid.css">
    <link rel="stylesheet" href="/assets/css/font-awesome.css">



    <link rel="stylesheet" href="/assets/css/AdminLTE.min.css">

    <link rel="shortcut icon" href="/assets/img/favicon.ico">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
@yield('css')
</head>
<body class="hold-transition skin-black sidebar-mini">
<div class="wrapper">
    <header class="main-header">
        <a href="#" class="logo">
            <span class="logo-mini"><img src="/assets/img/logo_hocelot_menu.png"></span>
            <span class="logo-lg"><img src="/assets/img/logo_hocelot_menu.png"></span>
        </a>
        <nav class="navbar navbar-static-top">
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="cursor:default;">
                            <img src="/assets/img/hocelot.png" class="user-image" alt="User Image">
                            <span class="hidden-xs">Administrador</span>
                        </a>
                        {{--<ul class="dropdown-menu">--}}
                            {{--<!-- User image -->--}}
                            {{--<li class="user-header">--}}
                                {{--<img src="/assets/img/hocelot.png" class="img-circle" alt="User Image">--}}

                                {{--<p>--}}

                                    {{--<small></small>--}}
                                {{--</p>--}}
                            {{--</li>--}}
                            {{--<li class="user-footer">--}}
                                {{--<div class="pull-left">--}}
                                    {{--<a href="/account/profile" class="btn btn-default btn-flat">Perfil</a>--}}
                                {{--</div>--}}
                                {{--<div class="pull-right">--}}
                                    {{--<a id="button_logout" href="#" class="btn btn-default btn-flat">Cerrar sesión</a>--}}
                                {{--</div>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <aside class="main-sidebar">
        <section class="sidebar">
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="/assets/img/hocelot.png" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>Administrador</p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <div style="height: 20px"></div>
            <ul class="sidebar-menu">
                @if (Request::path() == 'admin/clients' || Request::path() == 'admin/addClient')
                    <li class="active treeview">
                @else
                    <li class="treeview">
                @endif
                    <a href="javascript:void(0)">
                        <i class="fa fa-users"></i> <span>Clientes</span>
                    </a>
                        <ul class="treeview-menu">
                            <li><a href="/admin/clients"><i class="fa fa-circle-o"></i> Ver clientes</a></li>
                            <li><a href="/admin/addClient"><i class="fa fa-circle-o"></i> Nuevo cliente</a></li>
                            {{--<li><a href="/admin/msg/send"><i class="fa fa-circle-o"></i> Redactar</a></li>--}}
                        </ul>
                </li>
                {{--@if (Request::path() == 'account/profile')--}}
                    {{--<li class="active treeview">--}}
                {{--@else--}}
                    {{--<li class="treeview">--}}
                {{--@endif--}}
                    {{--<a href="/account/profile">--}}
                        {{--<i class="fa fa-suitcase"></i>--}}
                        {{--<span>Datos de la empresa</span>--}}
                    {{--</a>--}}
                {{--</li>--}}

            </ul>
        </section>
    </aside>
    @yield('content')
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> beta 1
        </div>
        Hocelot Administrator panel &copy; 2017<!--<p id="power">0</p>-->
    </footer>
</div>
<script src="/assets/js/jquery-2.1.1.js"></script>
{{--<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>--}}
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
//    $.widget.bridge('uibutton', $.ui.button);
</script>
{{--<script src="/assets/js/jquery.js"></script>--}}
<script src="/assets/js/jquery-ui.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>

{{--<script src="/assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>--}}
{{--<script src="/assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>--}}
{{--<script src="/assets/plugins/fastclick/fastclick.js"></script>--}}
<script src="/assets/js/app.js"></script>
{{--<script src="/assets/js/account.js"></script>--}}
@yield('js')
</body>
</html>