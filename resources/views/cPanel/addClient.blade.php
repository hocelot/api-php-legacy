@extends('cPanel.templates.header')
@section('css')
    <link rel="stylesheet" href="/assets/css/addClient.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/css/bootstrap-select.min.css">
@endsection
@section('content')
    <section class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            {{--<h1 class="page-header"><p class="titulo_cons_head">Crear Cliente</p></h1>--}}
            <div class="demo_titulo">
                Crear Cliente
            </div>
            <div class="col-md-12">
                <form action="javascript:void(0)" method="post">
                    <div id="container-particular">
                        <div class="caja_1">
                            <p class="session_text">Usuario, contraseña y permisos</p>
                            <div class="input-group">
                                <input class="form-control input_label" id="email" name="email" type="email" placeholder="Correo electrónico" onBlur="checkEmail(this)" onFocus="infoEmail(this);">
                                <span class="input-group-addon input_img"><img src="/assets/img/demo/icono_email.png"/></span>
                            </div>
                            <div class="input-group">
                                <input class="form-control input_label" id="password" name="password" type="password" placeholder="Contraseña" onBlur="checkPass(this)" onFocus="infoPass(this);">
                                <span class="input-group-addon input_img"><img src="/assets/img/demo/icono_password.png"/></span>
                            </div>
                            <div class="input-group">
                                <select class="selectpicker form-control input_label" id="producto" onChange="checkSelSex(this)" multiple placeholder="Producto">
                                    <!--<option value="" disabled> Producto</option>-->
                                    <option value="" disabled selected hidden>Producto</option>
                                    <option value="ic">ID Check</option>
                                    <option value="if">ID Fraud</option>
                                    <option value="gc">Geo Check</option>
                                    <option value="gf">Geo Fraud</option>
                                    <option value="rs">Risk Score</option>
                                    <option value="ad">Address</option>
                                </select>
                                <span class="input-group-addon input_img"><img src="/assets/img/contacto/icono_producto_contacto.png"/></span>
                            </div>
                        </div>
                        <div class="caja_2">
                            <p class="session_text">Datos de la empresa</p>
                            <div class="input-group">
                                <input class="form-control input_label" id="nombre_emp" name="nombre_emp" type="text" placeholder="Nombre de la empresa" onBlur="checkInput(this)" onFocus="infoInput(this);">
                                <span class="input-group-addon input_img"><img src="/assets/img/demo/icono_ciudad.png"/></span>
                            </div>
                            <div class="input-group">
                                <input class="form-control input_label" id="nombre" name="nombre" type="text" placeholder="Nombre de la persona de contacto" onBlur="checkInput(this)" onFocus="infoInput(this);">
                                <span class="input-group-addon input_img"><img src="/assets/img/demo/icono_usuario.png"/></span>
                            </div>
                            <div class="input-group">
                                <input class="form-control input_label" id="apellidos" name="apellidos" type="text" placeholder="Apellidos" onBlur="checkInput(this)" onFocus="infoInput(this);">
                                <span class="input-group-addon input_img"><img src="/assets/img/demo/icono_usuario.png"/></span>
                            </div>
                            <div class="input-group">
                                <input class="form-control input_label" id="doc" name="doc" type="text" placeholder="CIF" onBlur="checkDoc(this)" onFocus="infoDoc(this);">
                                <span class="input-group-addon input_img"><img src="/assets/img/demo/icono_NIE.png"/></span>
                            </div>
                            <div class="input-group">
                                <input class="form-control input_label" id="direccion" name="direccion" type="text" placeholder="Dirección" onBlur="checkDir(this)" onFocus="infoDir(this);">
                                <span class="input-group-addon input_img"><img src="/assets/img/demo/icono_gps.png"/></span>
                            </div>
                            <div class="input-group">
                                <input class="form-control input_label" id="telefono" name="telefono" type="text" placeholder="Teléfono" maxlength="9" onBlur="checkTelf(this)" onFocus="infoTelf(this)">
                                <span class="input-group-addon input_img"><img src="/assets/img/demo/icono_telefono.png"/></span>
                            </div>
                        </div>
                        <div class="div_validar_boton">
                            <input type="submit" id="boton_join" name="submit" class="validar_boton" value="Continuar">
                        </div>
                    </div>
                </form>
                <div class="modal fade" id="loadingModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-content">
                        <div id="loading">
                            <img src="/assets/img/demo/gif_procesando_peticion.gif"/>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
@endsection
@section('js')
    <script src="/assets/js/bootstrap-select.js"></script>
    <script src="/assets/js/addClient.js"></script>
    <script src="/assets/js/validar.js"></script>
@endsection