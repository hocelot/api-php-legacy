<?
//Sacamos últimos 6 meses desde la fecha actual para mostrar el gráfico de histórico DC
$mes_actual = date('d-m-2016');
$mes_1 = date('Y-m',strtotime('-7 months', strtotime($mes_actual)));
$mes_2 = date('Y-m',strtotime('-6 months', strtotime($mes_actual)));
$mes_3 = date('Y-m',strtotime('-5 months', strtotime($mes_actual)));
$mes_4 = date('Y-m',strtotime('-4 months', strtotime($mes_actual)));
$mes_5 = date('Y-m',strtotime('-3 months', strtotime($mes_actual)));
$mes_6 = date('Y-m',strtotime('-2 months', strtotime($mes_actual)));
$mes_7 = date('Y-m',strtotime('-1 months', strtotime($mes_actual)));
$mes_actual = date('Y-m');
?>
 <div id="page-wrapper">
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Inicio <small>Estadísticas generales</small>
                </h1>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-comments fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge" id="consultas">0</div>
                                <div>Nuevas consultas</div>
                            </div>
                        </div>
                    </div>
                    <a href="consultas">
                        <div class="panel-footer">
                            <span class="pull-left">Ver detalles</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-tasks fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge" id="total_cons">0</div>
                                <div>Total de consultas </div>
                            </div>
                        </div>
                    </div>
                    <a href="consultas">
                        <div class="panel-footer">
                            <span class="pull-left">Ver detalles</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-shopping-cart fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge" id="clientes">0</div>
                                <div>Nuevos clientes</div>
                            </div>
                        </div>
                    </div>
                    <a href="cliente">
                        <div class="panel-footer">
                            <span class="pull-left">Ver detalles</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel panel-red">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-support fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge" id="total_clientes">0</div>
                                <div>Total de clientes</div>
                            </div>
                        </div>
                    </div>
                    <a href="cliente">
                        <div class="panel-footer">
                            <span class="pull-left">Ver detalles</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-bar-chart-o fa-fw"></i> Area Chart</h3>
                    </div>
                    <div class="panel-body">
                        <div id="morris-area-chart"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

<script>
//MOVER A ADMIN.js DC 
$(window).load(function(){
	//FUNCION PARA SACAR DIA ACTUAL
	var d = new Date();
	var month = d.getMonth()+1;
	var day = d.getDate();
	var out = d.getFullYear() + '-' +((''+month).length<2 ? '0' : '') + month + '-' +((''+day).length<2 ? '0' : '') + day;
	
//	DECLARO VARS PARA LOS 7 MESES ANTERIORES EN CLIENTES Y CONSULTAS
	var mes1_cons = 0;
	var mes2_cons = 0;
	var mes3_cons = 0;
	var mes4_cons = 0;
	var mes5_cons = 0;
	var mes6_cons = 0;
	var mes7_cons = 0;
	var mes_act_cons = 0;
	
	var mes1_cli = 0;
	var mes2_cli = 0;
	var mes3_cli = 0;
	var mes4_cli = 0;
	var mes5_cli = 0;
	var mes6_cli = 0;
	var mes7_cli = 0;
	var mes_act_cli = 0;
	
//	labels : ["","<? echo $mes_2; ?>","<? echo $mes_3; ?>","<? echo $mes_4; ?>","<? echo $mes_5; ?>","<? echo $mes_6; ?>","<? echo $mes_7; ?>", "<? echo $mes_actual; ?>"],
	
	//FUNCION PARA SACAR LAS CONSULTAS
	$.ajax({
		url: 'http://hocelot.com/app/Account/getMisConsultas',
		type: 'POST',
		dataType: 'json',
		success: function (row, data){
			var output = out + "T00:00:00";
			var total_consultas = row["recordsTotal"];// total consultas
	//SACAMOS FECHA CONSULTA PARA SACAR LA FECHA DE LAS CONSULTAS DE HOY
			var datos = row["data"];
			var consultas = 0;
			for (var i = 0, l = datos.length; i < l; i++ ) {
				var dat = (datos[i]);
				var fecha = dat["fecha"];
				var mes_graf = fecha.substr(0,7);
				//CONSULTAS POR DIA
				//console.log (mes_graf +'asdasd'+ <? echo "'".$mes_1."'"; ?>);
				if (fecha == output){
					consultas ++;
				}
				if (mes_graf == <? echo "'".$mes_1."'"; ?>){
					mes1_cons ++;
				}
				if (mes_graf == <? echo "'".$mes_2."'"; ?>){
					mes2_cons ++;
				}
				if (mes_graf == <? echo "'".$mes_3."'"; ?>){
					mes3_cons ++;
				}
				if (mes_graf == <? echo "'".$mes_4."'"; ?>){
					mes4_cons ++;
				}
				if (mes_graf == <? echo "'".$mes_5."'"; ?>){
					mes5_cons ++;
				}
				if (mes_graf == <? echo "'".$mes_6."'"; ?>){
					mes6_cons ++;
				}
				if (mes_graf == <? echo "'".$mes_7."'"; ?>){
					mes7_cons ++;
				}
				if (mes_graf == <? echo "'".$mes_actual."'"; ?>){
					mes_act_cons ++;
				}
			}
			$('#consultas').html(consultas);
			$('#total_cons').html(total_consultas);
			
			
		}
	})
	//FUNCION PARA SACAR LOS CLIENTES
	$.ajax({
		url: 'http://hocelot.com/app/Admin/getClients',
		type: 'POST',
		dataType: 'json',
		success: function (column, row){
			var total_clientes = column.length;
			var clientes = 0;
			for (var i = 0, l = column.length; i < l; i++ ) {
				var dat = (column[i]);
				var fecha_cli = dat["fecha_registro"];
				var fecha_cli_graf = fecha_cli.substr(0,7);
				//console.log (fecha_cli +' '+ <? echo "'".$mes_1."'"; ?>);
				if (fecha_cli == out){
					clientes ++;
				}
				if (fecha_cli_graf == <? echo "'".$mes_1."'"; ?>){
					mes1_cli ++;
				}
				if (fecha_cli_graf == <? echo "'".$mes_2."'"; ?>){
					mes2_cli ++;
				}
				if (fecha_cli_graf == <? echo "'".$mes_3."'"; ?>){
					mes3_cli ++;
				}
				if (fecha_cli_graf == <? echo "'".$mes_4."'"; ?>){
					mes4_cli ++;
				}
				if (fecha_cli_graf == <? echo "'".$mes_5."'"; ?>){
					mes5_cli ++;
				}
				if (fecha_cli_graf == <? echo "'".$mes_6."'"; ?>){
					mes6_cli ++;
				}
				if (fecha_cli_graf == <? echo "'".$mes_7."'"; ?>){
					mes7_cli ++;
				}
				if (fecha_cli_graf == <? echo "'".$mes_actual."'"; ?>){
					mes_act_cli ++;
				}
			}
		$('#clientes').html(clientes);
		$('#total_clientes').html(total_clientes);
		}
	});	
	setTimeout(function(){
		//AÑADIMOS TIMEOUT PARA DAR TIEMPO A QUE SE RELLENEN LAS VARIABLES
		//PINTAMOS LOS RESULTADOS EN LA GRÁFICA
		//SACAMOS RESULTADOS POR MESES, 8 meses total:
		var grafAdmin = Morris.Area({
			element: 'morris-area-chart',
			data: [{
				period: '<? echo $mes_1; ?>',
				Consultas: mes1_cons,
				Clientes: mes1_cli,
			}, {
				period: '<? echo $mes_2; ?>',
				Consultas: mes2_cons,
				Clientes: mes2_cli,
			}, {
				 period: '<? echo $mes_3; ?>',
				Consultas: mes3_cons,
				Clientes: mes3_cli,
			}, {
				 period: '<? echo $mes_4; ?>',
				Consultas: mes4_cons,
				Clientes: mes4_cli,
			}, {
				period: '<? echo $mes_5; ?>',
				Consultas: mes5_cons,
				Clientes: mes5_cli,
			}, {
				period: '<? echo $mes_6; ?>',
				Consultas: mes6_cons,
				Clientes: mes6_cli,
			}, {
				 period: '<? echo $mes_7; ?>',
				Consultas: mes7_cons,
				Clientes: mes7_cli,
			}, {
				 period: '<? echo $mes_actual; ?>',
				Consultas: mes_act_cons,
				Clientes: mes_act_cli,
			}],
			xkey: 'period',
			ykeys: ['Consultas', 'Clientes'],
			labels: ['Consultas', 'Clientes'],
			pointSize: 2,
			hideHover: 'auto',
			resize: true
		});
	},2000);
});
</script>