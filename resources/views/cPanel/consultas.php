<div id="page-wrapper">
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <p class="titulo_cons_head">Consultas realizadas</p>
                </h1>
                <script src="<?= base_url() ?>assets/js/bootstrap-datepicker.min.js"></script>
                <div class="col-md-12 subtitulo_factura">
                    <text class="texto_bold">Buscar</text> por:
                </div>
                <div class="col-md-12 subtitulo_factura ordenar_factura">
                    <text class="texto_bold">Ordenar</text> por:
                </div>
                <div class="col-md-12 actionBar">
                    <div class="col-md-3 col-xs-3">
                        <div class="cuadro_factura label_factura">Nombre</div>
                        <input class="cuadro_factura" type="text" id="search-nombre"/>
                    </div>
                    <div class="col-md-3 col-xs-3">
                        <div class="cuadro_factura label_factura">DNI</div>
                        <input  class="cuadro_factura" type="text" id="search-nif"/>
                    </div>
                    <div class="col-md-3 col-xs-3">
                        <div class="cuadro_factura label_factura">Fecha</div>
                        <div class="cuadro_factura date" data-provide="datepicker">
                            <input id="search_fecha" class="cuadro_input_fecha" type="text">
                            <div class="input-group-addon fecha_factura"> </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-3 actionBar noActionBar">
                        <div class="cuadro_factura label_factura">Mostrar</div>
                        <select id="num_campos">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50" selected>50</option>
                            <option value="all">Todos</option>
                        </select>
                    </div>
                </div>
                <table id="mis_consultas" class="display tabla_facturas tabla_mis_consulta">
                    <thead>
                    <tr class="menu_rates">
                        <th>Rating</th>
                        <th>NIF</th>
                        <th>Nombre</th>
                        <th>Apellidos</th>
                        <th>Fecha</th>
                        <th>Calificación</th>
                        <th>Renta dispo.</th>
                    </tr>
                    </thead>
                    <tbody class="tabla_rates">
                    </tbody>
                </table>
                <nav>
                    <ul class="pagination">
        
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
<div id="loadingModal" class="modal fade" aria-labelledby="myModalLabel" role="dialog" tabindex="-1">
	<div class="modal-dialog modal-content">
		<div id="loading">
			<img src="<?= base_url() ?>assets/img/cargando_espera.gif"/>
		</div>
	</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Consulta</h4>
			</div>
			<div class="modal-body">

			</div>
		</div>
	</div>
</div>

<!-- CIERRO ROW PROFILE Y CONTAINER DC -->
</div>
</div>
<!--<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.css">-->
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>

<script>
$(window).load(function(){
	$('#mis_consultas_processing').html('Cargando...');
});
	var count=0;
	var pages=1;
	var actual_page=1;
function number_format(number, decimals, dec_point, thousands_sep) {
  number = (number + '')
    .replace(/[^0-9+\-Ee.]/g, '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function(n, prec) {
      var k = Math.pow(10, prec);
      return '' + (Math.round(n * k) / k)
        .toFixed(prec);
    };
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
    .split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '')
    .length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1)
      .join('0');
  }
  return s.join(dec);
}


	$.fn.datepicker.defaults.format = "yyyy-mm-dd";

	jQuery.fn.search = function (x) {
		$('.pagination').html('');
		var value = $(this).val().toUpperCase();
		var $rows = $("table tr");
		if (value === '') {
			$rows.show();
			return false;
		}
		$rows.each(function (index) {
			if (index !== 0) {
				$row = $(this);
				var column2 = $row.find("td").eq(x).text().toUpperCase();
				if ((column2.indexOf(value) > -1)) {
					$row.show();
				}
				else {
					$row.hide();
				}
			}
		});
	};
	$(document).ready(function () {
		$.extend($.fn.dataTable.defaults, {
			searching: false,
			ordering: false,
			paging: false,
			info: false
		});
		$('#mis_consultas').DataTable({
			serverSide: true,
			processing: true,
			ajax: {
				url: 'http://hocelot.com/app/Account/getMisConsultas',
				type: 'POST',
				dataType: 'json'
			},
			"createdRow": function (row, data) {
				count++;
				if(count > 50){
					$(row).hide();
				}
				$(row).attr('id', data['dt_rowid']);
				$(row).attr('ev', data['numero_evaluaciones']);
				$(row).find("td").eq(6).append('&euro;');

			//Sacamos la clase correspondiente a cada score DC
				if(data['score'] <= 310)
					$(row).find("td").eq(0).addClass('rat_d_menos'),
					$(row).addClass('row_d_menos'),
					$(row).find('td').eq(6).addClass('row_d_menos_final');
					
				else if (data['score'] > 310 && data['score'] <= 320)
					$(row).find("td").eq(0).addClass('rat_d'),
					$(row).addClass('row_d'),
					$(row).find('td').eq(6).addClass('row_d_final');
					
				else if (data['score'] > 320 && data['score'] <= 330)
					$(row).find("td").eq(0).addClass('rat_d_mas'),
					$(row).addClass('row_d_mas'),
					$(row).find('td').eq(6).addClass('row_d_mas_final');
					
				else if (data['score'] > 330 &&  data['score'] <= 340)
					$(row).find("td").eq(0).addClass('rat_c_menos'),
					$(row).addClass('row_c_menos'),
					$(row).find('td').eq(6).addClass('row_c_menos_final');
					
				else if (data['score'] > 340 && data['score'] <= 360)
					$(row).find("td").eq(0).addClass('rat_c'),
					$(row).addClass('row_c'),
					$(row).find('td').eq(6).addClass('row_c_final');
					
				else if (data['score'] > 360 && data['score'] <= 370)
					$(row).find("td").eq(0).addClass('rat_c_mas'),
					$(row).addClass('row_c_mas'),
					$(row).find('td').eq(6).addClass('row_c_mas_final');
					
				else if (data['score'] > 370 && data['score'] <= 390)
					$(row).find("td").eq(0).addClass('rat_b_menos'),
					$(row).addClass('row_b_menos'),
					$(row).find('td').eq(6).addClass('row_b_menos_final');
					
				else if (data['score'] > 390 &&  data['score'] <= 420)
					$(row).find("td").eq(0).addClass('rat_b'),
					$(row).addClass('row_b'),
					$(row).find('td').eq(6).addClass('row_b_final');
					
				else if (data['score'] > 420 && data['score'] <= 450)
					$(row).find("td").eq(0).addClass('rat_b_mas'),
					$(row).addClass('row_b_mas'),
					$(row).find('td').eq(6).addClass('row_b_mas_final');
					
				else if (data['score'] > 450 && data['score'] <= 500)
					$(row).find("td").eq(0).addClass('rat_a_menos'),
					$(row).addClass('row_a_menos'),
					$(row).find('td').eq(6).addClass('row_a_menos_final');
					
				else if (data['score'] > 500 && data['score'] <= 540)
					$(row).find("td").eq(0).addClass('rat_a'),
					$(row).addClass('row_a'),
					$(row).find('td').eq(6).addClass('row_a_final');
					
				else if (data['score'] > 540 &&  data['score'] <= 580)
					$(row).find("td").eq(0).addClass('rat_a_mas'),
					$(row).addClass('row_a_mas'),
					$(row).find('td').eq(6).addClass('row_a_mas_final');
					
				else if (data['score'] > 580 && data['score'] <= 620)
					$(row).find("td").eq(0).addClass('rat_aa_menos'),
					$(row).addClass('row_aa_menos'),
					$(row).find('td').eq(6).addClass('row_aa_menos_final');
					
				else if (data['score'] > 620 && data['score'] <= 660)
					$(row).find("td").eq(0).addClass('rat_aa'),
					$(row).addClass('row_aa'),
					$(row).find('td').eq(6).addClass('row_aa_final');
					
				else if (data['score'] > 660 && data['score'] <= 700)
					$(row).find("td").eq(0).addClass('rat_aa_mas'),
					$(row).addClass('row_aa_mas'),
					$(row).find('td').eq(6).addClass('row_aa_mas_final');
					
				else if (data['score'] > 700 &&  data['score'] <= 750)
					$(row).find("td").eq(0).addClass('rat_aaa_menos'),
					$(row).addClass('row_aaa_menos'),
					$(row).find('td').eq(6).addClass('row_aaa_menos_final');
					
				else if (data['score'] > 750 && data['score'] <= 800)
					$(row).find("td").eq(0).addClass('rat_aaa'),
					$(row).addClass('row_aaa'),
					$(row).find('td').eq(6).addClass('row_aaa_final');
					
				else if (data['score'] > 800 && data['score'] <= 850)
					$(row).find("td").eq(0).addClass('rat_aaa_mas'),
					$(row).addClass('row_aaa_mas'),
					$(row).find('td').eq(6).addClass('row_aaa_mas_final');
			},
			columns: [
				{data: 'score', type: "number-range"},
				{data: 'dni', type: "text"},
				{data: 'nombre', type: "text"},
				{
					"data": "ape",
					"render": function (data) {
						var ape1 = data.substr(0, data.indexOf('/'));
						var ape2 = data.substr(data.indexOf('/')+1, data.length);
						return ape1+" "+ape2;
					}
				},
				{
					"data": "fecha",
					"render": function (data) {
						var date = new Date(data);
						var month = date.getMonth() + 1;
						var day =date.getDate();
						return date.getFullYear() + "-" + (month.length > 1 ? month : "0" + month) + "-" + (day > 10 ? day : "0" + day);
					}
				},
				{data: 'rating_eq', type: "text"},
				//{data: 'renta_libre', type: "number-range"}
				{
					'data': 'renta_libre',
					"render": function (data) {
						var num = number_format(data, 2, ',', '.');
						return num;
					}
				}
			],
			"initComplete": function() {

				cambiarNumPaginas(10);
			}
		});
		$('#search-nif').on('keyup', function () {
			$(this).search(1);
		});
		$('#search-nombre').on('keyup', function () {
			$(this).search(2);
		});
		$('#search_fecha').on('change', function () {
			$(this).search(3);
		});
		$("#mis_consultas tbody").delegate("tr", "click", function (e) {
			//rest of the code here

			$.ajax({
				url: 'http://hocelot.com/app/Account/generatePDFRating/'+$(this).attr('id')+'/'+$(this).attr('ev'),
				type: 'POST',
				dataType: 'json',
				success: function (result) {
					$('.modal-body').append('<iframe id="iframe" src="'+result+'" style="width:100%; height:85%;" frameborder="0"></iframe>');
					$('#myModal').modal('show');
				},
				error: function () {
					alert('ERROR');
				},
				beforeSend: function () {
					$('#loadingModal').modal('show');
				},
				complete: function () {
					$('#loadingModal').modal('hide');
				}
			});

		});

		$('#myModal').on('hidden.bs.modal', function (e) {
			$('#iframe').remove();
			$.ajax({
				url: 'http://hocelot.com/app/Account/deletePDFRating',
				type: 'POST'
			});
			$('body').css('padding-right', '0px');
		});
//		$.ajax({
//			url: 'http://test.mirating.es/Account/getMisConsultas',
//			type: 'POST',
//			dataType: 'json',
//			success: function (result) {
//				console.log(result);
//			},
//			error: function () {
//				alert('ERROR');
//			}
//		});

		/*PAGINACION*/
		$('#num_campos').on('change', function () {
			cambiarNumPaginas($(this).val());
			pagination(1,$(this).val());
		});
	});

	function pagination(page,num_reg) {

		var reg_vistos=1;
		var numregmost=0;
		var saltar_tr=(page-1)*num_reg;
		$('#mis_consultas > tbody  > tr').each(function() {
			$(this).hide();
			if(num_reg=='all'){
				$(this).show();

			}else if(saltar_tr >=0 && reg_vistos>saltar_tr && numregmost<=num_reg){
				$(this).show();
				numregmost++;
			}
			reg_vistos++;
		});
	}
	function cambiarNumPaginas(num_reg_a_mostrar) {
		var ul_pag=$('.pagination');
		ul_pag.html('');
		if(num_reg_a_mostrar!='all') {
			pages = Math.ceil(count / num_reg_a_mostrar);
			
			ul_pag.append('<li class="disabled" id="prev_li"><a href="javascript:void(0)" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>');
			var c = 1;
			ul_pag.append('<li class="active"><a href="javascript:void(0)" onclick="cambiopag('+c+')">' + c + '<span class="sr-only">(current)</span></a></li>');
			c++;
			do {
				ul_pag.append('<li><a href="javascript:void(0)" onclick="cambiopag('+c+')">' + c + '<span class="sr-only">' + c + '</span></a></li>');
				c++;
			} while (c <= pages);
			ul_pag.append('<li id="next_li"><a href="javascript:void(0)" onclick="cambiopag(2)" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>');
		}
	}
	function cambiopag(num){
		actual_page=num;
		if(actual_page>1) {
			$("#prev_li").removeClass("disabled");
			$("#prev_li a").attr('onclick', 'cambiopag('+(actual_page-1)+')');
		}else{
			$("#prev_li").addClass("disabled");
		}
		if(actual_page==pages) {
			$("#next_li").addClass("disabled");
		}else{
			$("#next_li").removeClass("disabled");
			$("#next_li a").attr('onclick', 'cambiopag('+(actual_page+1)+')');
		}
		$("ul.pagination li.active").removeClass("active");
		$("ul.pagination li:nth-child("+(num+1)+")").addClass('active');
		pagination(num,$('#num_campos').val());
	}
</script>