 <div id="page-wrapper">
    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
            <h1 class="page-header">
              <p class="titulo_cons_head">Clientes</p>
            </h1>
    	<link type="text/css" href="<?= base_url()?>assets/css/datagrid_facturas.css">
        <link type="text/css" href="<?= base_url()?>assets/css/cpanel.css">
		<script src="<?= base_url()?>assets/js/jquery.bootgrid.js"></script>
		<script src="<?= base_url()?>assets/js/bootstrap-datepicker.min.js"></script>
		<!--<p class="titulo_cons_head">Clientes</p>-->
		<div class="col-md-12 subtitulo_factura">
			buscar<text class="texto_bold">por</text>
		</div>
		<div class="col-md-12 subtitulo_factura ordenar_factura">
			Ordenar<text class="texto_bold">por</text>
		</div>
        <div class="col-md-12">
            <table id="grid-data-api" class="table table-condensed table-hover table-striped tabla_admin" data-toggle="bootgrid" data-ajax="true" data-url="<?=base_url('Account/')?>">
                <thead>
                <tr>
                    <th data-column-id="id" data-type="text" data-identifier="true" data-order="desc">ID</th>
                    <th data-column-id="email" data-order="desc">Usuario</th>
                    <th data-column-id="fecha_registro" data-order="desc">Fecha registro</th>
                    <th data-column-id="last_login" data-order="desc">último acceso</th>
                    <th data-column-id="tipo" data-formatter="tipo" data-order="desc">Tipo</th>
                    <th data-column-id="num_creditos_consumidos" data-order="desc">Número de consultas</th>
                    <th data-column-id="estado" data-formatter="estado" data-order="desc">Estado</th>
                    <th data-column-id="cuenta" data-formatter="cuenta" data-order="desc">Cuenta</th>
                    <th data-column-id="validar_email" data-formatter="validar_email" data-order="desc">Email_validado</th>
                    <th data-column-id="commands" data-formatter="commands" data-sortable="false"></th>
                </tr>
                </thead>
            </table>
        </div>
	</div>
</div>
			</div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
<script>
	var urlBase = "http://hocelot.com/app/";
    var grid = $("#grid-data-api").bootgrid({
        caseSensitive:false,
        ajax: true,
        url: urlBase + 'Admin/getClients',
	    formatters: {
			"tipo": function(column, row){
				if(row.tipo == 0){
					return 'Banco';
				}
				else if(row.tipo == 1){
					return 'Empresa';
				}
				else if(row.tipo == 2){
					return 'Particular';
				}
				else if(row.tipo == 3){
					return 'Administrador';
				}
				else{
					return 'No disponible';
				}
			},
			"estado": function(column, row){
				if(row.estado == 0){
					return 'Baja';
				}
				else if(row.estado == 1){
					return 'Activo';
				}
				else{
					return 'Suspendido';
				}	
			},
			"cuenta": function(column, row){
				if(row.gratis == 0){
					return 'Premium';
				}
				else{
					return 'Gratis';
				}
			},
			"validar_email": function(column, row){
				if(row.validar_email == 0){
					return 'Sí';
				}
				else{
					return 'No';
				}
			},
			"commands": function(column, row){
				return "<button type=\"button\" class=\"boton_pres command-view\" data-row-id=\"" + row.id + "\"><img src=\"../../assets/img/icono_ver.png \"></button> ";
			}
   		},
        requestHandler: function (request) {
			var searchFecha=$('#fecha_registro_search').val();
            var searchNFactura=$('#fecha_registro').val();
            if (searchFecha != "") {
                request.searchFecha = searchFecha;
            }
            if (searchNFactura != "") {
               request.searchNFactura = searchNFactura;
            }
            return request;
		}
    });
	$("#grid-data-api").on("loaded.rs.jquery.bootgrid", function()	{
		grid.find(".command-view").on("click", function(e){
			$.ajax({
				url: urlBase + 'Account/viewCliente/'+$(this).data("row-id"),
				type: 'POST',
				success:function(result){
					window.location= urlBase + "account/cpanel/cliente"+$(this).data("row-id");
				}
			});
		});
	});
$(document).ready(function(){
		//$('.actions').addClass('col-md-3');
        //Falta on key up request
		var search = $('.search');
        search.append('<div class="col-md-2 col-xs-2"><div class="cuadro_factura label_factura" id="basic-addon1">ID</div><input type="text" id="id_search" class="cuadro_factura"></div>');
		search.append('<div class="col-md-2 col-xs-2"><div class="cuadro_factura label_factura" id="basic-addon1">Usuario</div><input type="text" id="email_search" class="cuadro_factura"></div>');
        search.append('<div class="col-md-2 col-xs-2"><div class="cuadro_factura label_factura" id="basic-addon1">Fecha de alta</div><div class="cuadro_factura date" data-provide="datepicker"><input id="fecha_registro_search" type="text" class="cuadro_input_fecha"><div class="input-group-addon fecha_factura"> </div></div></div>');
		search.append('<div class="col-md-2 col-xs-2"><div class="cuadro_factura label_factura" id="basic-addon1">Tipo</div><input type="text" id="tipo_search" class="cuadro_factura"></div>');
		$.fn.datepicker.defaults.format = "yyyy-mm-dd";
		$( "#id_search" ).keyup(function() {
			$("#grid-data-api").bootgrid("reload")
		});
		$( "#email_search" ).keyup(function() {
			$("#grid-data-api").bootgrid("reload")
		});
		$( "#tipo_search" ).keyup(function() {
			$("#grid-data-api").bootgrid("reload")
		});
		$( "#fecha_registro_search" ).change(function() {
			$("#grid-data-api").bootgrid("reload")
		});
	});
</script>