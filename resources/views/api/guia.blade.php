@extends('api.templates.landing')
<link rel="shortcut icon" href="/assets/img/favicon.ico">
@section('css')
    <link href="/assets/css/guia.css" rel="stylesheet"/>
@endsection
@section('js')
    <script src="/assets/js/guia.js"></script>
@endsection
@section('content')
<div class="container container_landing">
<div class="full">
    <div class="col-md-12 col-sm-12 vertical-center">
        <div class="container col-md-12 col-sm-12 text-center">
            <h1 class="landing_titulo">Próximamente</h1>
        </div>
    </div>
</div>
    <!--<div class="row" style="display: none">-->
        <div class="col-md-3" style="display: none">
        <!-- SIDEBAR LATERAL DC-->
       		<div class="landing_titulo landing_titulo_left">
                Guia
            </div>
            <ul class="nav nav-pills nav-stacked border_sidebar">
                <li role="presentation titulo_sidebar" class="titulo_sidebar div_titulo_sidebar_first"><div class="div_titulo_sidebar">Integración</div></li>
                	<ul class="nav nav-pills nav-stacked">
                		<li role="presentation" class="active"><a href="#">Home</a></li>
                    </ul>
                <li role="presentation titulo_sidebar"><a href="#">Profile</a></li>
                <li role="presentation titulo_sidebar"><a href="#">Messages</a></li>
                <li role="presentation titulo_sidebar" class="titulo_sidebar"><div class="div_titulo_sidebar">Datos</div></li>
                <li role="presentation titulo_sidebar"><a href="#">Profile</a></li>
                <li role="presentation titulo_sidebar"><a href="#">Messages</a></li>
            </ul>
        </div>
        <div class="col-md-9">
        <!-- PRIMERA LANDING DC-->
        	<div class="landing_titulo">
            	Getting Started
            </div>
            <ul class="nav nav-tabs nav-justified">
                <li role="presentation" class="active"><a href="#">Home</a></li>
                <li role="presentation"><a href="#">Profile</a></li>
                <li role="presentation"><a href="#">Messages</a></li>
            </ul>
        </div>
	</div>
<!--</div>-->
@endsection
