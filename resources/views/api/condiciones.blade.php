@extends('api.templates.landing')
<link rel="shortcut icon" href="/assets/img/favicon.ico">
@section('css')
    <link href="/assets/css/demo.css" rel="stylesheet"/>
@endsection
@section('content')
    <div class="container container_landing">
        <div class="row">
            <div class="col-lg-12">
                <div class="landing_titulo">
                    Condiciones generales Demo.<br>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="condiciones_text">
                    El presente acuerdo regula las condiciones generales de uso de la Demo ofertada por Hocelot Financial Technologies S.L., en adelante Hocelot, con CIF B87546164, ubicada en Calle Perú nº 6, CP 28290, Las Rozas de Madrid y cuyos datos registrales son: Tomo: 34.626, Libro: 0, Folio: 110, Sección: 8, Hoja: M-622956, Inscripción: 1, Fecha: 22/04/2016.
                </div>
                <div class="demo_titulo">
                    1.	Condiciones del servicio
                </div>
                <div class="condiciones_text">
                    El USUARIO, en el momento que completa el formulario de registro y utiliza nuestra Demo, acepta y se somete expresamente a las cláusulas especificadas a continuación, así como a las condiciones de acceso a nuestro portal y su política de privacidad, accesibles a través del enlace “Avisos Legales”.
                    El acceso y navegación en www.hocelot.com (en adelante “esta web”) implica aceptar y conocer las advertencias legales, condiciones y términos de uso contenidos en ella. El mero acceso no implica el establecimiento de ningún tipo de relación comercial entre la empresa y el usuario. Los contenidos y servicios de esta web van dirigidos únicamente a aquellas personas que no tienen ninguna restricción jurisdiccional o legal para acceder a las mismas. Los servicios de esta web están a disposición de personas físicas mayores de edad (18 años y en adelante) y/o jurídicas debidamente representadas.
                    La Demo de HOCELOT es gratuito, e independiente y no vinculada al resto de servicios ofertados por HOCELOT.
                </div>
                <div class="demo_titulo">
                    2.	Características de la DEMO
                </div>
                <div class="condiciones_text">
                    HOCELOT se reserva el derecho a modificar de cualquier forma las características y condiciones de la Demo.
                    El informe de la Demo está destinado únicamente para comprobar la eficacia de nuestro software que permite conocer la reputación financiera de personas físicas. Hocelot no se hace responsable del uso que haga USTED de los datos obtenidos, ni de su divulgación, ni de ningún perjuicio que esto pueda ocasionarle a USTED o a terceras personas. El informe derivado de la Demo solo puede ser consultado por la propia persona física que utilice la Demo. Si USTED cuanta con el consentimiento firmado y expreso de un tercero para obtener su informe, USTED es el único responsable sobre dicha consulta, eximiendo de toda responsabilidad a HOCELOT.
                    HOCELOT se nutre de fuentes públicas y privadas, propias y externas y obtiene la información de fuentes de información cuyos datos se presumen correctos. HOCELOT realiza todos los esfuerzos posibles para asegurar la exactitud de la información, pero debido a que existe una inmensa cantidad de datos detallados al compilar las Bases de Datos, informes y otros soportes de información, y que los datos son obtenidos de fuentes no controladas por HOCELOT, que no asume responsabilidad alguna por errores, inexactitudes u omisiones de la información facilitada.
                    El usuario asume toda la responsabilidad derivada del uso que realice del resultado de HOCELOT, siendo el único responsable de todo efecto directo o indirecto que sobre el mismo se derive, incluyendo, de forma enunciativa y no limitativa, todo resultado económico, técnico y/o jurídico adverso, así como la defraudación de las expectativas generadas, obligándose el usuario a mantener indemne a Hocelot por cualesquiera reclamaciones derivadas, directa o indirectamente de tales hechos.
                </div>
                <div class="demo_titulo">
                    3.	Obligaciones de las partes
                </div>
            <div class="condiciones_text">
                La Demo de HOCELOT contempla facilitar al Usuario un cálculo de su posicionamiento financiero en base a los datos obtenidos de diversas fuentes por HOCELOT, en base a la información recibida del Usuario en el formulario de solicitud. El Usuario asume toda la responsabilidad derivada de la correcta utilización del resultado de HOCELOT, siendo el Usuario el único responsable de todo efecto directo o indirecto que pudiese originar obligándose el usuario de la Demo a mantener indemne a Hocelot.
                HOCELOT no garantiza que la disponibilidad de la Demo sea continua e ininterrumpida, por circunstancias originadas por problemas en la red de Internet, averías en los dispositivos informáticos y otras circunstancias imprevisibles.
                HOCELOT tiene la potestad de interrumpir la Demo cuando lo desee, sin necesidad de aviso a los usuarios y sin que éstos tengan derecho a compensación alguna por el cese del servicio.
                En Usuario en ningún caso, podrá trasladar, transferir, subarrendar, sublicenciar, vender o realizar cualquier otro acto de disposición sobre la información ya sea gratuita u onerosa, ni extraer o reutilizar la totalidad o parte del contenido de los informes, sin perjuicio de lo dispuesto en la legislación vigente en materia de propiedad intelectual. Queda expresamente prohibido reproducir, copiar, transformar, modificar o bajo cualquier procedimiento alterar la información contenida en los Productos y Servicios, ya sea de forma parcial o totalmente, gratuita u onerosamente. USTED se abstendrá de pedir información a sabiendas de que va a ser utilizada por terceros, y de permitir voluntaria o negligentemente que terceros hagan semejantes solicitudes.
            </div>
            <div class="demo_titulo">
                4.	Autorización
            </div>
            <div class="condiciones_text">
                El usuario autoriza expresamente a Hocelot, a solicitar en su nombre información sobre su solvencia patrimonial a las entidades dedicadas a ello, así como a acceder a todos los sitios y fuentes en Internet susceptibles de proporcionarle información financiera del Usuario.
                5. Compromiso, aceptación y validez del contrato
                Tal como indica en la política de privacidad de nuestra página web, el Usuario es el responsable de facilitar sus datos de forma exacta y veraz, así como de la actualización de los mismos.
                El Usuario reconoce haber leído y aceptado las condiciones legales de uso y la política de privacidad de la página web.
                El Usuario reconoce que ha entendido toda la información respecto a la utilización de la Demo ofrecida en nuestra página web, así como todas las condiciones y estipulaciones recogidas en las presentes condiciones de uso, por lo que afirma que son suficientes para la exclusión del error en el consentimiento de las condiciones y, por lo tanto, las acepta integra y expresamente.
                El usuario desde el mismo momento en que haya rellenado el formulario y haya hecho clic en la casilla correspondiente para obtener la Demo, manifiesta su conformidad con las presentes Condiciones Generales, sin que sea necesario confirmar la recepción de la aceptación de la demo cuando este se haya celebrado exclusivamente mediante medios electrónicos.
            </div>
            <div class="demo_titulo">
                6. Normativa Aplicable
            </div>
            <div class="condiciones_text">
                Las presentes condiciones de uso se regirán e interpretarán de acuerdo con las leyes españolas.
                En el caso de que se produzca cualquier tipo de discrepancia o diferencia entre las partes en relación con la interpretación, contenido o ejecución de las presentes condiciones, que no sea solucionada de mutuo acuerdo, ambas partes podrán optar por acudir, con renuncia expresa a cualquier otro fuero que pudiera corresponderles, a los Juzgados y Tribunales de Madrid.
            </div>
            <div class="demo_titulo">
                7. Datos personales
            </div>
            <div class="condiciones_text">
                En cumplimiento de la Ley Orgánica 15/1999 de Protección de Datos de Carácter Personal, le informamos que los datos personales que nos facilite para la utilización de nuestra Demo, son confidenciales y que se incorporarán a un fichero de “Clientes”, así como remitirle información comercial de nuestros productos y servicios propios y de colaboradores de la web.
                No obstante, le recordamos que la aceptación de nuestros servicios implica la aceptación y sometimiento expreso a la política de privacidad y avisos legales accesibles en nuestra web.
                En cualquier momento oponerse a la recepción de nuestros envíos comerciales, así como ejercitar los derechos de acceso, rectificación, cancelación y oposición de sus datos en Hocelot Financial Technologies S.L., C/ Perú, 6, Edificio B, 1º, Of. 1, 28290 de Las Rozas (Madrid).
            </div>
            <div class="condiciones_text">
                <b>Atención:</b> Las presentes Condiciones Generales de Uso han sido actualizadas con fecha 19/12/2016. En cualquier momento podemos proceder a su modificación, así como variar la relación a los precios y productos ofertados. Por favor, compruebe la fecha de emisión en cada ocasión en que se conecte a nuestra página Web y así tendrá la certeza de que no se ha producido modificación alguna que le afecte.
            </div>
            </div>
        </div>
    </div>
@endsection