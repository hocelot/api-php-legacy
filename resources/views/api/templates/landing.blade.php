<!DOCTYPE html>
<html lang="en">
<!-- Make sure the <html> tag is set to the .full CSS class. Change the background image in the full.css file. -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Hocelot</title>
    <!-- Bootstrap Core CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="/assets/css/landing.css" rel="stylesheet"/>
    @yield('css')
</head>
<body>
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top nav_landing" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><img src="/assets/img/logo_hocelot_menu_blanco.png"/></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
					<li>
                       <a class="main_menu_landing" href="http://hocelot.com"> <img class="img_landing_hocelot" src="/assets/img/icono_home_hocelot.png"/>Volver</a>
                    </li>
                    <li>
                        <a class="main_menu_landing" href="/guia">Guia</a>
                    </li>
                    <li>
                        <a class="main_menu_landing" href="/demo">Demo</a>
                    </li>
                    <!--<li>
                        <a class="main_menu_landing" href="/precio">Precio</a>
                    </li>-->
                    <li>
                        <a class="main_menu_landing" href="/contacto">Solicitar API</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    <!-- Put your page content here! -->
@yield('content')
<div class="footer_landing">
	<div class="container">
        <div class="col-md-4 col-sm-6">
    		<p class="text_footer_landing">&copy; 2016 Hocelot </p>
        </div>
        <div class="col-md-8 col-sm-6">
        	<div class="col-md-3">
            	<a class="link_footer_landing" target="_blank" href="http://hocelot.com/aviso-legal">Aviso legal</a>
            </div>
            <div class="col-md-3">
            	<a class="link_footer_landing" target="_blank" href="http://hocelot.com/politica-privacidad">Política de privacidad</a>
            </div>
            <div class="col-md-3">
            	<a class="link_footer_landing" target="_blank" href="http://hocelot.com/politica-de-cookies">Política de cookies</a>
            </div>
            <div class="col-md-3">
            	<a class="link_footer_landing" target="_blank" href="/condiciones_generales">Condiciones generales</a>
            </div>
        </div>
    </div>
</div>
<!-- jQuery -->
<script src="/assets/js/jquery.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="/assets/js/bootstrap.min.js"></script>
@yield('js')
</body>
</html>