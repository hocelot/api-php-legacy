<!DOCTYPE html>
<html class="full" lang="en">
<!-- Make sure the <html> tag is set to the .full CSS class. Change the background image in the full.css file. -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Hocelot</title>
    <!-- Bootstrap Core CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="/assets/css/full.css" rel="stylesheet">
    <link href="/assets/css/home.css" rel="stylesheet"/>
    @yield('css')
</head>
<body>
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
				<div class="img_menu">
                <a class="navbar-brand" href="/"><img src="/assets/img/logo_hocelot_menu.png"/></a>
				</div>
			</div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
					<li>
                       <a class="main_menu" href="http://hocelot.com"> <img class="img_home_hocelot" src="/assets/img/icono_home_hocelot_naranja.png"/>Volver</a>
                    </li>
                    <li>
                        <a class="main_menu" href="/guia">Guia</a>
                    </li>
                    <li>
                        <a class="main_menu" href="/demo">Demo</a>
                    </li>
                    <!--<li>
                        <a class="main_menu" href="/precio">Precio</a>
                    </li>-->
                    <li>
                        <a class="main_menu" href="/contacto">Solicitar API</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    <!-- Put your page content here! -->
@yield('content')
<div class="footer_home">
	<div class="container">
        <div class="col-md-4">
    		<p class="text_footer_home">&copy; 2016 Hocelot </p>
        </div>
        <div class="col-md-8">
        	<div class="col-md-3">
            	<a class="link_footer_home" target="_blank" href="http://hocelot.com/aviso-legal">Aviso legal</a>
            </div>
            <div class="col-md-3">
            	<a class="link_footer_home" target="_blank" href="http://hocelot.com/politica-privacidad">Política de privacidad</a>
            </div>
            <div class="col-md-3">
            	<a class="link_footer_home" target="_blank" href="http://hocelot.com/politica-de-cookies">Política de cookies</a>
            </div>
            <div class="col-md-3">
            	<a class="link_footer_home" target="_blank" href="/condiciones_generales">Condiciones generales</a>
            </div>
        </div>
    </div>
</div>
<!-- jQuery -->
<script src="/assets/js/jquery.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="/assets/js/bootstrap.min.js"></script>
@yield('js')
</body>
</html>