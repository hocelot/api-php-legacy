@extends('api.templates.landing')
<link rel="shortcut icon" href="/assets/img/favicon.ico">
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/css/bootstrap-select.min.css">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <link href="/assets/css/contacto.css" rel="stylesheet"/>
@endsection
@section('js')
    <script src="/assets/js/contacto.js"></script>
    <script src="/assets/js/validar.js"></script>
	<script src="/assets/js/bootstrap-select.js"></script>
@endsection
<meta name="csrf-token" content="{{ csrf_token() }}" />
@section('content')
<div class="container container_landing" id="send_mens">
    <div class="full">
    	<div class="col-md-12 vertical-center">
   			 <div class="container col-md-12">
    			<div class="row">
                    <div class="col-lg-12">
                        <div class="landing_titulo solicitar_api">
                            Solicitar API
                        </div>
                       <!-- <div class="landing_subtitulo">
                            Texto subtitulo prueba.
                        </div>-->
                        <div class="col-md-12">
                            <div class="col-md-2"></div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon input_img"><img src="/assets/img/contacto/icono_usuario.png"/></span>
                                    <input type="text" class="form-control input_label" placeholder="Nombre" id="nombre" onBlur="checkInput(this)" onFocus="infoInput(this)">
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon input_img"><img src="/assets/img/demo/icono_telefono.png"/></span>
                                    <input type="text" class="form-control input_label" placeholder="Teléfono" id="telefono" onBlur="checkTelf(this)" onFocus="infoTelf(this)">
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon input_img"><img src="/assets/img/contacto/icono_mail.png"/></span>
                                    <input type="text" class="form-control input_label" placeholder="Email" id="email"  onBlur="checkEmail(this)" onFocus="infoEmail(this)">
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon input_img"><img src="/assets/img/contacto/icono_producto_contacto.png"/></span>
                                    <select class="selectpicker form-control input_label" id="producto" onChange="checkSelSex(this)" multiple placeholder="Producto">
                                        <!--<option value="" disabled> Producto</option>-->
                                        <option value="" disabled selected hidden>Producto</option>
                                        <option value="idcheck">ID Check</option>
                                        <option value="idfraud">ID Fraud</option>
                                        <option value="geocheck">Geo Check</option>
                                        <option value="geofraud">Geo Fraud</option>
                                        <option value="riskscore">Risk Score</option>
                                        <option value="asnef">Incidencias</option>
                                    </select>
                               </div>
                            </div>
                            <div class="col-md-2"></div>
                         </div>
                        <div class="col-md-12">
                            <div class="col-md-2"></div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon input_img"><img src="/assets/img/contacto/icono_consultas_contacto.png"/></span>
                                    <input type="text" class="form-control input_label" placeholder="Nº Consultas Anuales" id="cons_year" maxlength="10" onBlur="checkInt(this)" onFocus="infoInt(this)">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2"></div>
                            <div class="col-md-8 col-sm-12 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon input_img">
                                        <div class="div_img_mensaje">
                                            <img class="icono_mensaje" src="/assets/img/contacto/icono_mensaje_contacto.png"/>
                                        </div>
                                    </span>
                                     <textarea class="form-control input_label" placeholder="Mensaje" id="mensaje" onBlur="checkTextArea(this)" onFocus="infoInput(this)" rows="10"></textarea>
                                </div>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2"></div>
                            <div class="col-md-2 col-sm-3 col-xs-12 input_colum_demo boton_responsive input_colum_button_pri">
                                <input id="submit_button" class="boton_demo boton_mostrar" type="button" value="Contactar">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>     
</div>
<div class="container container_landing" id="ok_mens" style="display:none">
    <div class="full">
    	<div class="col-md-12 vertical-center">
   			 <div class="container col-md-12">
    			<div class="row">
                    <div class="col-lg-12">
                        <div class="landing_titulo">
                            Solicitar API
                        </div>
                     <!--   <div class="landing_subtitulo">
                            Texto subtitulo prueba.
                        </div> -->
                        <div id="img_ok_mens" style="display:none">
                        	<img src="/assets/img/contacto/icono_mensaje_enviado.png"/>
                        </div>
                         <h1 class="landing_titulo" id="ok_mens_text"></h1>
                         <h1 class="landing_titulo_2" id="ok_mens_text_sub"></h1>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection