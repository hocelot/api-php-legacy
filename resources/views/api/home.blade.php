@extends('api.templates.index')
<link rel="shortcut icon" href="/assets/img/favicon.ico">
@section('content')
@section('js')
<script src="/assets/js/TweenLite.min.js"></script>
@endsection
<div class="full">
    <div class="col-xs-12 vertical-center">
        <div class="container col-xs-12 text-center">
			<!--<div id="large-header" class="large-header" style="height: auto;">
			<canvas id="demo-canvas" height="auto" width="1920"></canvas>-->
				<h1 class="h1_home">Instala ahora la API de Hocelot.</h1>
				<div class="row">
					<div class="col-md-12">
					<ul class="top-features">
						<li><h3 class="h3_home">Y evita desde hoy el fraude <br>y la mora en tu negocio.</h3></li>
						</ul>
						<div class=""> 
						<input type="button" class="button_home demo" value="Demo" onClick="window.location.href = '/demo'"/>
						<input type="button" class="button_home" value="Solicitar API" onClick="window.location.href = '/contacto'"/>
					</div>
				</div>
			</div>
			<!--</div>-->
		</div>
</div>
</div>
<script src="/assets/js/demo-1.js"></script>
@endsection

