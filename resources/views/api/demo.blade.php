@extends('api.templates.landing')
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/css/bootstrap-select.min.css">
    <link href="/assets/css/demo.css" rel="stylesheet"/>
	<link rel="shortcut icon" href="/assets/img/favicon.ico">
@endsection
<meta name="csrf-token" content="{{ csrf_token() }}" />
@section('content')
<div class="container container_landing">
    <div class="row">
        <div class="col-lg-12">
        	<div class="landing_titulo">
            	Haz una DEMO gratis.
            </div>
            <div class="landing_subtitulo">
            	Introduce tus datos para conocer los resultados que facilita nuestra API en tiempo real.<br>
                Es necesario disponer de un Api Key para realizar la consulta.
            </div>
        </div>
        <div class="col-md-2">
        </div>
       	<div class="col-md-4 col-sm-6 col-xs-12">
        	<div class="demo_titulo">
            	Tus datos
            </div>
           <input type="hidden" id="code" value="">
            <input type="hidden" id="zip_type" value="0">
            <div class="input-group">
               <span class="input-group-addon input_img"><img src="/assets/img/demo/icono_sexo.png"/></span>
               <select class="selectpicker form-control input_label" id="sexo" onChange="checkSelSex(this)">
               		<option value="" disabled selected style="opacity: 0.5"> Sexo</option>
               		<option value="H"> Hombre</option>
                    <option value="M"> Mujer</option>
               </select>
            </div>
            <div class="input-group">
                <span class="input-group-addon input_img"><img src="/assets/img/demo/icono_usuario.png"/></span>
                <input type="text" class="form-control input_label" placeholder="Nombre" id="nombre" onBlur="checkInput(this)" onFocus="infoInput(this)">
            </div>
            <div class="input-group">
                <span class="input-group-addon input_img"><img src="/assets/img/demo/icono_usuario.png"/></span>
                <input type="text" class="form-control input_label" placeholder="Primer apellido" id="primer_apellido" onBlur="checkInput(this)" onFocus="infoInput(this)">
            </div>
            <div class="input-group">
                <span class="input-group-addon input_img"><img src="/assets/img/demo/icono_usuario.png"/></span>
                <input type="text" class="form-control input_label" placeholder="Segundo apellido" id="segundo_apellido" onBlur="checkInput(this)" onFocus="infoInput(this)">
            </div>
            <div class="input-group">
                <span class="input-group-addon input_img"><img src="/assets/img/demo/icono_NIE.png"/></span>
                <input type="text" class="form-control input_label" placeholder="NIF/NIE" id="NIF" onBlur="checkDoc(this)" onFocus="infoDoc(this)">
            </div>
            <div class="input-group">
                <span class="input-group-addon input_img"><img src="/assets/img/demo/icono_mail.png"/></span>
                <input type="text" class="form-control input_label" placeholder="Email" id="email" onBlur="checkEmail(this)" onFocus="infoEmail(this)">
            </div>
            <div class="input-group">
                <span class="input-group-addon input_img"><img src="/assets/img/demo/icono_telefono.png"/></span>
                <input type="text" class="form-control input_label" placeholder="Teléfono" id="telefono" maxlength="9" onBlur="checkTelf(this)" onFocus="infoTelf(this)">
            </div>
            <div class="input-group">
                <span class="input-group-addon input_img"><img src="/assets/img/demo/icono_fecha.png"/></span>
                <input type="text" class="form-control input_label" placeholder="Fecha de nacimiento" id="date_nac" maxlength="10" onBlur="checkFech(this)" onFocus="infoFech(this)">
            </div>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12 ">
        	<div class="demo_titulo">
            	Tu dirección
            </div>
             <div class="input-group">
                <span class="input-group-addon input_img"><img src="/assets/img/demo/icono_cp.png"/></span>
                <input type="text" class="form-control input_label" id="del_codpos" placeholder="Código postal" onBlur="checkInt(this)" onFocus="infoInt(this)" maxlength="5">
            </div>
            <div class="input-group">
                <span class="input-group-addon input_img"><img src="/assets/img/demo/icono_ciudad.png"/></span>
                <select class="selectpicker form-control input_label" id="poblacion" onChange="checkSelSex(this)" data-live-search="true" title="Población">
                <!--	<option value="" disabled selected> Provincia</option>-->
                </select>
               <!-- <input type="text" class="form-control input_label" placeholder="Provincia" onBlur="checkInput(this)" onFocus="infoInput(this)">-->
            </div>
            <div class="input-group">
                <span class="input-group-addon input_img"><img src="/assets/img/demo/icono_ciudad.png"/></span>
                <input type="text" class="form-control input_label" id="provincia" placeholder="Provincia" onChange="checkInput(this)" disabled>
            </div>
            <div class="input-group">
                <span class="input-group-addon input_img"><img src="/assets/img/demo/icono_via.png"/></span>
                <select class="selectpicker form-control input_label" id="tipo_via" onChange="checkSelSex(this)" data-live-search="true">
                	<option value="" disabled selected> Tipo de vía</option>
                </select>
                
                <!--<input type="text" class="form-control input_label" id="tipo_via" placeholder="Tipo de vía" onBlur="checkInput(this)" onFocus="infoInput(this)">-->
            </div>
            <input type="hidden" id="rt_id" value="">
            <div class="input-group">
                <span class="input-group-addon input_img"><img src="/assets/img/demo/icono_gps.png"/></span>
                <select class="selectpicker form-control input_label" id="via" onChange="checkSelSex(this)" data-min="minLenght" data-live-search="true" data-live-Search-Placeholder="Introduce al menos 3 caracteres"> 
                	<option value="" disabled selected> Nombre de vía</option>
                </select>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 input_colum_demo input_colum_demo_pri">
                <div class="input-group">
                    <span class="input-group-addon input_img"><img src="/assets/img/demo/icono_gps.png"/></span>
                    <select class="selectpicker form-control input_label" id="numero" onChange="checkSelSex(this)" data-live-search="true">
                        <option value="" disabled selected> Número</option>
                    </select>
                    <!-- <select class="selectpicker form-control input_label" onBlur="checkSelSex(this)" id="numero" data-live-search="true" title="Número"></select>-->
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 input_colum_demo input_colum_demo_ult">
                <div class="input-group">
                    <span class="input-group-addon input_img img_disabled" id="km_img"><img src="/assets/img/demo/icono_gps.png"/></span>
                    <select class="selectpicker form-control input_label" id="km" onChange="checkSelSex(this)" data-live-search="true" disabled>
                        <option value="" disabled selected> Kilómetro</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 input_colum_demo input_colum_demo_pri">
                <div class="input-group">
                    <span class="input-group-addon input_img img_disabled" id="bloque_img"><img src="/assets/img/demo/icono_gps.png"/></span>
                     <select class="selectpicker form-control input_label" id="bloque" onChange="checkSelSex(this)" data-live-search="true" disabled>
                        <option value="" disabled selected> Bloque</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 input_colum_demo input_colum_demo_ult">
                <div class="input-group">
                    <span class="input-group-addon input_img img_disabled" id="escalera_img"><img src="/assets/img/demo/icono_gps.png"/></span>
                     <select class="selectpicker form-control input_label" id="escalera" onChange="checkSelSex(this)" data-live-search="true" disabled>
                        <option value="" disabled selected> Escalera</option>
                    </select>
                </div>
            </div>
             <div class="col-md-4 col-sm-4 col-xs-12 input_colum_demo">
                <div class="input-group">
                    <span class="input-group-addon input_img img_disabled" id="piso_img"><img src="/assets/img/demo/icono_gps.png"/></span>
                     <select class="selectpicker form-control input_label" id="piso" onChange="checkSelSex(this)" data-live-search="true" disabled>
                        <option value="" disabled selected> Piso</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 input_colum_demo input_colum_demo_medio">
                <div class="input-group">
                    <span class="input-group-addon input_img img_disabled" id="puerta_img"><img src="/assets/img/demo/icono_gps.png"/></span>
                     <select class="selectpicker form-control input_label" id="puerta" onChange="checkSelSex(this)" data-live-search="true" disabled>
                        <option value="" disabled selected> Puerta</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 input_colum_demo">
                <div class="input-group">
                    <span class="input-group-addon input_img img_disabled" id="letra_img"><img src="/assets/img/demo/icono_gps.png"/></span>
                     <select class="selectpicker form-control input_label" id="letra" onChange="checkSelSex(this)" data-live-search="true" disabled>
                        <option value="" disabled selected> Letra</option>
                    </select>
                </div>
            </div>
            <div class="input-group key_input">
                <span class="input-group-addon input_img input_key"></span>
                <input type="password" class="form-control input_label input_key" onblur="checkDir(this)" id="api_key" placeholder="API Key">
            </div>
        </div>
        <div class="col-md-2">
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 ">
            <div class="col-md-2">
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="col-md-12">
                    <input type="checkbox" name="acepto_cond" tabindex="20" id="acepto_cond" onClick="checkCheckbox(this)"/>
                    <label class="condiciones" id="label_check" for="checkbox" > He leído y acepto las <a href="/condiciones_generales" target="_blank">condiciones generales</a>.</label>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
            </div>
            <div class="col-md-2">
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 ">
            <div class="col-md-2">
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="col-md-6 col-sm-6 col-xs-6 input_colum_demo boton_responsive input_colum_button_pri">
                    <input type="button" class="boton_demo boton_mostrar" id="submit_button" value="Mostrar">
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 input_colum_demo boton_responsive input_colum_demo_pri">
                    <input type="button" class="boton_demo boton_limpiar" id="reset_button" value="Limpiar"/>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
            </div>
            
            <div class="col-md-2 col-sm-2">
            </div>
         </div>
		<div class="col-md-12 col-sm-12 col-xs-12">
	        <hr class="hr_demo">
        </div>
        <div class="col-md-12" id="pre_api">
        </div>
<!--        <div class="col-md-12 col-sm-12 col-xs-12" id="response_api" style="display:none">-->
        <div class="col-md-12 col-sm-12 col-xs-12" id="response_api" style="display:none">
		<ul class="nav nav-tabs nav-justified">
                <li role="presentation" class="active" id="resp_idCheck"><a href="javascript:changeActive('resp_idCheck','div_idCheck')">ID Check</a></li>
                <li role="presentation" id="resp_idFraud"><a href="javascript:changeActive('resp_idFraud','div_idFraud')">ID Fraud</a></li>
                <li role="presentation" id="resp_gCheck"><a href="javascript:changeActive('resp_gCheck','div_gCheck')">Geo Check</a></li>
                <li role="presentation" id="resp_gFraud"><a href="javascript:changeActive('resp_gFraud','div_gFraud')">Geo Fraud</a></li>
                <li role="presentation" id="resp_social" style="display: none"><a href="javascript:changeActive('resp_social','div_social')">Social</a></li>
                <li role="presentation" id="resp_score"><a href="javascript:changeActive('resp_score','div_score')">Risk Score</a></li>
                <li role="presentation" id="resp_json"><a href="javascript:changeActive('resp_json','div_json')">JSON Response</a></li>
                <!--ALEX ESTO ES LO TUYO-->
                <li role="presentation" id="resp_foam" style="display:none"><a href="javascript:changeActive('resp_foam','FoamTree')">FoamTree</a></li>
          </ul>
          <!-- <div class="response_fuera col-md-12" id="div_graf">
                <div class="col-md-12">
                	<p class="response_titulo response_titulo_abajo">Resultado tratado</p>
                </div>
                <div class="col-md-12">
                	<div class="response_dentro" id="response_graf"></div>
                </div>
	        </div>-->
            <div class="response_fuera col-md-12" id="div_idCheck">
                <div class="col-md-12">
                	<p class="response_titulo response_titulo_abajo">Resultado tratado</p>
                    <p class="response_info_titulo"><img class="img_info_response" src="/assets/img/demo/icono_info_naranja.png"/>
                    Determina la exactitud de los datos de cabecera de un individuo, variable fundamental para la detección del usuario. Id Check de Hocelot identifica la correlación DNI/NIE con el nombre y apellidos del sujeto evaluado.
                    </p>
                </div>
                <div class="col-md-12">
                	<div class="response_dentro" id="response_idCheck">
                        <canvas id="idCheckChart" width="400" height="200"></canvas>
                        <!--<canvas id="myChart" width="400" height="400"></canvas>-->
                        <!--<canvas id="idCheckChart"></canvas>-->
                    </div>
                </div>
	        </div>
             <div class="response_fuera col-md-12" id="div_idFraud" style="display:none">
                <div class="col-md-12">
                	<p class="response_titulo response_titulo_abajo">Resultado tratado</p>
                    <p class="response_info_titulo"><img class="img_info_response" src="/assets/img/demo/icono_info_naranja.png"/>
                    Detección de anomalías entre la identidad real y la identidad digital de un usuario online. Es un sistema multi-variable que hace único al usuario por más de una variable para conseguir una descripción digital más robusta.
                    </p>
                </div>
                <div class="col-md-12">
                	<div class="response_dentro" id="response_idFraud"></div>
                </div>
	        </div>
             <div class="response_fuera col-md-12" id="div_gCheck" style="display:none">
                <div class="col-md-12">
                	<p class="response_titulo response_titulo_abajo">Resultado tratado</p>
                    <p class="response_info_titulo"><img class="img_info_response" src="/assets/img/demo/icono_info_naranja.png"/>
                   Identificación y verificación de la dirección facilitada por el usuario, determinando en tiempo real de la inexistencia/existencia y la exactitud de la dirección facilitada por el cliente.
                    </p>
                </div>
                <div class="col-md-12">
                	<div class="response_dentro" id="response_gCheck"></div>
                </div>
	        </div>
             <div class="response_fuera col-md-12" id="div_gFraud" style="display:none">
                <div class="col-md-12">
                	<p class="response_titulo response_titulo_abajo">Resultado tratado</p>
                    <p class="response_info_titulo"><img class="img_info_response" src="/assets/img/demo/icono_info_naranja.png"/>
                   Comprobación de la correlación entre el sujeto evaluado y el domicilio facilitado, detección del fraude geográfico y domicilios comprometidos. <br>
                   Sólo se mostrará cando Geo Check sea distinto de 0.
                    </p>
                </div>
                <div class="col-md-12">
                	<div class="response_dentro" id="response_gFraud"></div>
                </div>
	        </div>
            <div class="response_fuera col-md-12" id="div_social" style="display:none">
                <div class="col-md-12">
                    <p class="response_titulo response_titulo_abajo">Resultado tratado</p>
                    <p class="response_info_titulo"><img class="img_info_response" src="/assets/img/demo/icono_info_naranja.png"/>
                        SOCIAL
                    </p>
                </div>
                <div class="col-md-12">
                    <div class="response_dentro" id="response_social"></div>
                </div>
            </div>
             <div class="response_fuera col-md-12" id="div_score" style="display:none">
                <div class="col-md-12">
                	<p class="response_titulo response_titulo_abajo">Resultado tratado</p>
                    <p class="response_info_titulo"><img class="img_info_response" src="/assets/img/demo/icono_info_naranja.png"/>
                   	Hocelot Risk Score mediante su motor de búsquela localiza, discrimina, clasifica y analiza información cualitativa y cuantitativa del individuo buscada en la web.
                    El rastro digital de un sujeto estudiado se parametriza para determinar con nuestros algoritmos un informe con variables objetivas.
                    </p>
                </div>
                <div class="col-md-12">
                	<div class="response_dentro" id="response_score"></div>
                </div>
	        </div>
        	<div class="response_fuera col-md-12" id="div_json" style="display:none">
            	 <!--<div class="col-md-12">
                	<p class="response_titulo">Resultado encriptado</p>
                </div>
                <div class="col-md-12">
                	<div class="response_dentro" id="response_encript"></div>
                </div>-->
               
                <div class="col-md-12">
                	<p class="response_titulo response_titulo_abajo">Resultado tratado</p>
                </div>
                <div class="col-md-12">
                	<div class="response_dentro" id="response_body"></div>
                </div>
	        </div>
             <!--ALEX ESTO ES LO TUYO-->
            <div class="response_fuera col-md-12" id="FoamTree" style="display:none">               
                <div class="col-md-12">
                	<p class="response_titulo response_titulo_abajo">Resultado tratado</p>
                </div>
                <div class="col-md-12">
                	<div class="response_dentro" id="response_FoamTree"></div>
                </div>
	        </div>
        </div>
         <div class="modal fade" id="loadingModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-content">
                <div id="loading">
                    <img src="/assets/img/demo/gif_procesando_peticion.gif"/>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
    <script src="/assets/js/validar.js"></script>
    <script src="/assets/js/demo.js"></script>
    <script src="/assets/js/bootstrap-select.js"></script>
    <script src="/assets/js/chart.js"></script>
    {{--<script src="/assets/js/foamtree/carrotsearch.foamtree.js"></script>--}}
    <script>
        var ctx = document.getElementById("idCheckChart");
        var data = {
            labels: [
                "Red",
                "Blue",
                "Yellow"
            ],
            datasets: [
                {
                    data: [300, 50, 100],
                    backgroundColor: [
                        "#FF6384",
                        "#36A2EB",
                        "#FFCE56"
                    ],
                    hoverBackgroundColor: [
                        "#FF6384",
                        "#36A2EB",
                        "#FFCE56"
                    ]
                }]
        };
        var myDoughnutChart = new Chart(ctx, {
            type: 'doughnut',
            data: data,
            options: {
                animation:{
                    animateScale:true
                }
            }
        });
//       var ctx = document.getElementById("idCheckChart");
//       var myChart = new Chart(ctx, {
//           type: 'bar',
//           data: {
//               labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
//               datasets: [{
//                   label: '# of Votes',
//                   data: [12, 19, 3, 5, 2, 3],
//                   backgroundColor: [
//                       'rgba(255, 99, 132, 0.2)',
//                       'rgba(54, 162, 235, 0.2)',
//                       'rgba(255, 206, 86, 0.2)',
//                       'rgba(75, 192, 192, 0.2)',
//                       'rgba(153, 102, 255, 0.2)',
//                       'rgba(255, 159, 64, 0.2)'
//                   ],
//                   borderColor: [
//                       'rgba(255,99,132,1)',
//                       'rgba(54, 162, 235, 1)',
//                       'rgba(255, 206, 86, 1)',
//                       'rgba(75, 192, 192, 1)',
//                       'rgba(153, 102, 255, 1)',
//                       'rgba(255, 159, 64, 1)'
//                   ],
//                   borderWidth: 1
//               }]
//           },
//           options: {
//               scales: {
//                   yAxes: [{
//                       ticks: {
//                           beginAtZero:true
//                       }
//                   }]
//               }
//           }
//       });
    </script>
@endsection
