@extends('api.templates.landing')
<link rel="shortcut icon" href="/assets/img/favicon.ico">
@section('css')
    <link href="/assets/css/precio.css" rel="stylesheet"/>
@endsection
@section('js')
    <script src="/assets/js/precio.js"></script>
@endsection
@section('content')
<div class="container container_landing">
    <div class="row">
        <div class="col-lg-12">
        	<div class="landing_titulo">
            	Título precio
            </div>
            <div class="landing_subtitulo">
            	Texto subtitulo prueba.
            </div>
		</div>
  
        <div class="col-md-3 col-sm-6 col-xs-12">
			<div class="caja_1">
				<div class="arriba_caja_1">
					<p class="p_titulo">ID <font id="bold">Check</font></p>
						<div class="div_p_subt">
							<p class="p_desde">Desde</p><br>
							<p class="precio">10<font id="eurito">€</font></p>
						</div>
				</div>
				<a id="a_azul" href="http://api.hocelot.com/contacto"><div class="abajo_caja_1">
					<div class="boton_solicitar">
						<p class="solicita_azul">Solicitar</p>
					</div></a>
				</div>
			</div>
		
        </div>
		<div class="col-md-3 col-sm-6 col-xs-12">
			<div class="caja_1 caja_2">
				<div class="arriba_caja_1 arriba_caja_2">
					<p class="p_titulo">ID <font id="bold">Fraud</font></p>
						<div class="div_p_subt">
							<p class="p_desde">Desde</p><br>
							<p class="precio">10<font id="eurito">€</font></p>
						</div>
				</div>
				<div class="abajo_caja_1 abajo_caja_2">
					<a id="a_azul" href="http://api.hocelot.com/contacto"><div class="boton_solicitar boton_solicitar_2">
						<p class="solicita_azul">Solicitar</p>
					</div></a>
				</div>
			</div>
		
        </div>
        
		<div class="col-md-3 col-sm-6 col-xs-12">
			<div class="caja_1 caja_3">
				<div class="arriba_caja_1 arriba_caja_3">
					<p class="p_titulo">Geo <font id="bold">Check</font></p>
						<div class="div_p_subt">
							<p class="p_desde">Desde</p><br>
							<p class="precio">10<font id="eurito">€</font></p>
						</div>
				</div>
				<div class="abajo_caja_1 abajo_caja_3">
					<a id="a_azul" href="http://api.hocelot.com/contacto"><div class="boton_solicitar boton_solicitar_3">
						<p class="solicita_azul">Solicitar</p>
					</div></a>
				</div>
			</div>
		
        </div>
		<div class="col-md-3 col-sm-6 col-xs-12">
			<div class="caja_1 caja_4">
				<div class="arriba_caja_1 arriba_caja_4">
					<p class="p_titulo">Geo <font id="bold">Fraud</font></p>
						<div class="div_p_subt">
							<p class="p_desde">Desde</p><br>
							<p class="precio">10<font id="eurito">€</font></p>
						</div>
				</div>
				<a id="a_azul" href="http://api.hocelot.com/contacto"><div class="abajo_caja_1 abajo_caja_4">
					<div class="boton_solicitar boton_solicitar_4">
						<p class="solicita_azul">Solicitar</p>
					</div></a>
				</div>
			</div>
		
        </div>
		<div class="col-md-3">
        </div>
		
		<div class="col-md-3 col-sm-6 col-xs-12">
			<div class="caja_1 caja_5">
				<div class="arriba_caja_1 arriba_caja_5">
					<p class="p_titulo">Risk <font id="bold">Score</font></p>
						<div class="div_p_subt">
							<p class="p_desde">Desde</p><br>
							<p class="precio">10<font id="eurito">€</font></p>
						</div>
				</div>
				<div class="abajo_caja_1 abajo_caja_5">
					<a id="a_naranja" href="http://api.hocelot.com/contacto"><div class="boton_solicitar boton_solicitar_5">
						<p class="solicita_naranja">Solicitar</p>
					</div></a>
				</div>
			</div>
		
        </div>
		<div class="col-md-3 col-sm-6 col-xs-12">
			<div class="caja_1 caja_6">
				<div class="arriba_caja_1 arriba_caja_6">
					<p class="p_titulo">Risk + <font id="bold">Deuda</font></p>
						<div class="div_p_subt">
							<p class="p_desde">Desde</p><br>
							<p class="precio">10<font id="eurito">€</font></p>
						</div>
				</div>
				<a id="a_naranja" href="http://api.hocelot.com/contacto"><div class="abajo_caja_1 abajo_caja_6">
					<div class="boton_solicitar boton_solicitar_6">
						<p class="solicita_naranja">Solicitar</p>
					</div></a>
				</div>
			</div>
		
        </div>
		<div class="col-md-3">
        </div>
		
	</div>     
</div>
@endsection
