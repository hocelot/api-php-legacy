var token = $('meta[name="csrf-token"]').attr('content');
$(document).ready(function (e) {
    $('#submit').on('click', function () {
        var send = {
            '_token': token,
            'name': $("#name").val(),
            'place': $("#place").val(),
            'pass': $("#pass").val(),
            'fecha_ini': $("#fecha_ini").val(),
            'fecha_hasta': $("#fecha_hasta").val(),
        }
        $.ajax({
            type: "POST",
            url: '/Pruebas/DemoRm',
            data: send,
            dataType: "json",
            beforeSend: function () {
                $('#loadingModal').modal('show');
            },
            success: function (result) {
                $('#loadingModal').modal('hide');
                var str = JSON.stringify(result, undefined, 4);
                $('#response_body').html('<pre>' + syntaxHighlight(str) + '</pre>');
            },
            error: function () {
                $('#loadingModal').modal('hide');
            }
        });
    });
});

// function valReme() {
//     checkFech('fecha_ini');
//     checkFech('fecha_hasta');
// }
function infoFech(id){
    var idMens = 'mens_info_' + $(id).attr("id");
    if ($('#'+idMens).length > 0) {
        $('#'+idMens).remove();
        $(id).removeClass('caja_error');
    }
    $(id).parent(this).after('<div id="'+idMens+'" class="mens_info">La fecha de nacimiento debe tener el formato dd-mm-aaaa.</div>');
}
function delInfoFech(id){
    var idMens = 'mens_info_' + $(id).attr("id");
    if ($('#'+idMens).length > 0) {
        $('#'+idMens).remove();
        $(id).removeClass('caja_error');
    }
}
function syntaxHighlight(json) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}