var token = $('meta[name="csrf-token"]').attr('content');
var type = 'flat';
var house = 0;
function setZipType(val) {
    zip_type = $('#zip_type').val(val);
}
$(window).on('load', function () {
    setTimeout(function () {
        $('.filter-option .pull-left').addClass('select_disable');
    }, 10);
});
function getBarsNac(id, key) {
    // console.log(key)
    // console.log($(id).val().length);
    if(key == 8) {
        if ($(id).val().length == 2 || $(id).val().length == 5) {
            $(id).val($(id).val().substr(0, $(id).val().length-1));
        }else if($(id).val().slice(-1) == '/'){
            $(id).val($(id).val().substr(0, $(id).val().length-1));
        }
    }else{
        if ($(id).val().length == 2 || $(id).val().length == 5) {
            if($(id).val().slice(-1) == '/'){
                $(id).val($(id).val().substr(0, $(id).val().length-1));
            }else {
                $(id).val($(id).val() + '/');
            }
        }else if($(id).val().slice(-1) == '/'){
            $(id).val($(id).val().substr(0, $(id).val().length-1));
        }
    }
    $(id).val($(id).val().replace(/^(\d\d)(\d)$/g,'$1/$2').replace(/^(\d\d\/\d\d)(\d+)$/g,'$1/$2').replace(/[^\d\/]/g,''));
    // console.log($(id).val().replace(/^(\d\d)(\d)$/g,'$1/$2').replace(/^(\d\d\/\d\d)(\d+)$/g,'$1/$2').replace(/[^\d\/]/g,''));
}
function controlGetBarsNac(id) {
    $(id).val($(id).val().replace(/^(\d\d)(\d)$/g,'$1/$2').replace(/^(\d\d\/\d\d)(\d+)$/g,'$1/$2').replace(/[^\d\/]/g,''));
}
var format = "mm/dd/yyyy";
var match = new RegExp(format.replace(/(\w+)\W(\w+)\W(\w+)/, "^\\s*($1)\\W*($2)?\\W*($3)?([0-9]*).*").replace(/m|d|y/g, "\\d"));
var replace = "$1/$2/$3$4".replace(/\//g, format.match(/\W/));
function doFormat(target){
    target.value = target.value
        .replace(/(^|\W)(?=\d\W)/g, "$10")   // padding
        .replace(match, replace)             // fields
        .replace(/(\W)+/g, "$1");            // remove repeats
    if(target.value.length == 12){
        target.value = target.value.substr(0,target.value.length-2);
    }else if(target.value.length == 11){
        target.value = target.value.substr(0,target.value.length-1);
    }
}
$('#date_nac').keyup(function(e) {
    if(!e.ctrlKey && !e.metaKey && (e.keyCode == 32 || e.keyCode > 46))
        doFormat(e.target)
});

function getKey(){
    valForm = []; //Vaciamos array de validación
    var validateForm = 1; // 1 = Valido, 0 = No Valido DC
    validateSmsForm();
    for (var i = 0; i < valForm.length; i++) {
        //console.log('Dentro ' + valForm[i][0]);
        if (valForm[i][1] == 0) {
            validateForm = 0;
            $('html, body').animate({
                scrollTop: ($("#"+valForm[i][0]).offset().top -90)
            }, 1000);
            break
        }
        else {
        }
    }
    if (validateForm != 0) {
        var send = {
            '_token': token,
            'params': {
                'telf': $("#telefono").val()
            }
        };
        $.ajax({
            type: "POST",
            url: '/check/sendSms',
            data: send,
            dataType: "json",
            beforeSend: function () {
                $('#sms_div_info').html('<img class="img_width100" src="/assets/img/new-api/loading.gif"/>');
            },
            success: function (resp) {
                $('#demo_msg_button_key').addClass('disabled_button');
                $('#demo_msg_button_key').removeClass('button_hocelot');
                $('#demo_msg_button_key').attr('disabled', true);
                if(resp['status']== 200){
                    //$('#infoModal').modal('show');
                    $('#contactaSmooth').addClass('disabledButton');
                    $('#contactaSmooth').attr('disabled');
                    $('#sms_div_info').html('<img class="icono_ok_sms" src="/assets/img/new-api/path-2-copy-4.png">');
                }else if(resp['status']== 401){
                    //console.log(resp)
                    $('#sms_div_info').html('');
                    $('#contactModal').modal('show');
                    $('#mens_info_telefono').empty();
                    $('#telefono').addClass('caja_error');
                }else{
                    $('#sms_div_info').html('');
                    $('#infoModalMovil').modal('show');
                    $('#mens_info_telefono').empty();
                    $('#telefono').addClass('caja_error');
                    //console.log(resp)
                }
            },
            error: function () {
                $('#mens_info_telefono').empty();
                $('#telefono').addClass('caja_error');
            }
        });
    }else{
        // $('#mens_info_telefono').empty();
        // $('#telefono').addClass('caja_error');
    }
}
function validateGetKey() {
    checkTelf("#telefono");
}
function trueKey() {
    var send = {
        '_token': token,
        'params': {
            'key': $("#api_key").val()
        }
    };
    $.ajax({
        type: "POST",
        url: '/check/checkCode',
        data: send,
        dataType: "json",
        success: function (resp) {
            if(resp['status']== 200){
                // console.log(resp)
                $('#secureApiKey').val($('#api_key').val());
                $('#submit_button').removeClass('disabled_button');
                $('#submit_button').addClass('button_hocelot');
                $('#submit_button').removeAttr('disabled');
                $('#text_cod_sms').css('display', 'block');
                $('#text_cod_sms_error').css('display', 'none');
            }else{
                $('#submit_button').addClass('disabled_button');
                $('#submit_button').removeClass('button_hocelot');
                $('#submit_button').attr('disabled', true);
                $('#mens_info_api_key').empty();
                $('#api_key').addClass('caja_error');
                $('#text_cod_sms').css('display', 'none');
                $('#text_cod_sms_error').css('display', 'block');
                $("#api_key").val('')
            }
        },
        error: function () {
            $("#api_key").val('')
        }
    });
}
function validateContactForm(){
    checkInput('#nombreCon');
    checkInput('#apellidosCon');
    checkInput('#nombre_empCon');
    checkInput('#cargoCon');
    checkEmail('#emailCon');
    checkTelf("#telefonoCon");
}
function getSmooth() {
    $('#infoModal').modal('hide');
    $('#errorModal').modal('hide');
    $('#infoModalMovil').modal('hide');
    $("#nombreCon").val($("#nombre").val());
    $("#apellidosCon").val($("#primer_apellido").val()+' '+$("#segundo_apellido").val());
    $("#emailCon").val($("#email").val());
    $("#telefonoCon").val($("#telefono").val());
    $('#contactModal').modal('show');
}
function checkMsg(id, idDiv){
    if($(id).hasClass('caja_error')){
        $('#'+idDiv).css('display','none');
        $('#'+idDiv+'_mov').css('display', 'none');
        $('#'+idDiv+'_error').css('display', 'block');
        $('#'+idDiv+'_mov_error').css('display', 'block');
        $('#'+idDiv+'_error').css('color','#f9315b');
        $('#'+idDiv+'_mov_error').css('color','#f9315b');
    }else{
        $('#'+idDiv).css('display','block');
        $('#'+idDiv+'_mov').css('display', 'block');
        $('#'+idDiv+'_error').css('display', 'none');
        $('#'+idDiv+'_mov_error').css('display', 'none');
        $('#'+idDiv+'_error').css('color','#4a4a4a');
        $('#'+idDiv+'_mov_error').css('color','#4a4a4a');
    }
}
$(document).ajaxError(function(request, event, settings) {
    $('#errorModal').modal('show');
    if(event.status == 510){
        $('#ModalErrorBody').html(event.responseText);
    }else{
        $('#ModalErrorBody').html('Error Interno, póngase en contacto con nuestro equipo técnico.');
    }
});
$(document).ready(function (e) {
    $('#telefono').on('change',function () {
        valForm = []; //Vaciamos array de validación
        var validateForm = 1; // 1 = Valido, 0 = No Valido DC
        validateGetKey();
        for (var i = 0; i < valForm.length; i++) {
            //console.log('Dentro ' + valForm[i][0]);
            if (valForm[i][1] == 0) {
                validateForm = 0;
            }
            else {
            }
        }
        if (validateForm != 0) {
            $('#demo_msg_button_key').removeClass('disabled_button');
            $('#demo_msg_button_key').addClass('button_hocelot');
            $('#demo_msg_button_key').removeAttr('disabled');
        }else{
            $('#demo_msg_button_key').addClass('disabled_button');
            $('#demo_msg_button_key').removeClass('button_hocelot');
            $('#demo_msg_button_key').attr('disabled', true);
        }
    })

    $('#getSmoothContact').on('click', function (){
        getSmooth();
    });
    $('#getSmoothContact2').on('click', function (){
        getSmooth();
    });
    $('#getSmoothContact3').on('click', function (){
        getSmooth();
    });
    $('#submit_contact').on('click', function (){
        valForm = [];
        var validateForm = 1;
        validateContactForm();
        for (var i = 0; i < valForm.length; i++){
            if(valForm[i][1] == 0){
                validateForm = 0;
            }
            else{
            }
        }
        if(validateForm == 0){
            $('#error_text').html('Revisa los campos resaltados puede ser que el formato sea incorrecto o sea un campo obligatorio.');
        }
        if(validateForm != 0){
            $('#error_text').empty();
            var send = {
                '_token': token,
                'params': {
                    'nombre': $("#nombreCon").val(),
                    'apellidos': $("#apellidosCon").val(),
                    'email': $("#emailCon").val(),
                    'cargo': $("#cargoCon").val(),
                    'nom_emp': $("#nombre_empCon").val(),
                    'tel' : $("#telefonoCon").val(),
                    'origen': 'Contrata Smooth'
                }
            }
            $.ajax({
                type: "POST",
                url: '/newContact/solicitar',
                data: send,
                dataType: "json",
                success: function (result) {
                    if (result['status'] == 200){
                        $('#ModalContactBody').html('<div class="col-md-12 col-sm-12 col-xs-12 text-center"><h1>¡Oído cocina! Gracias por tu interés en nuestros servicios.</h1></div>');
                    }else if(result['status'] == 400) {
                        $('#ModalContactBody').html('<div class="col-md-12 col-sm-12 col-xs-12 text-center"><h1>Ha ocurrido un error, no hemos podido realizar tu petición.</h1></div>');
                    }else {
                        $('#ModalContactBody').html('<div class="col-md-12 col-sm-12 col-xs-12 text-center"><h1>Ha ocurrido un error, no hemos podido realizar tu petición.</h1></div>');
                    }
                    $('#contactaSmooth').addClass('disabledButton');
                    $('#contactaSmooth').attr('disabled');
                },
                error: function (result) {
                    $('#ModalContactBody').html('<div class="col-md-12 col-sm-12 col-xs-12 text-center"><h1>Ha ocurrido un error, no hemos podido realizar tu petición.</h1></div>');
                    $('#contactaSmooth').addClass('disabledButton');
                    $('#contactaSmooth').attr('disabled');
                }
            });
        }
    });
    $("#api_key").on('change', function () {
        trueKey();
    });
    $("#demo_msg_button_key").on('click',function () {
        getKey();
    });
    $("#demo_msg_resendKey").on('click',function () {
        getKey();
    });
    $("#demo_msg_resendKeyError").on('click',function () {
        getKey();
    });
    $("#del_codpos").change(function () {
        var codpos = $("#del_codpos").val();
        var send = {
            '_token': token,
            'params': {
                'zip_code': codpos
            }
        };
        if (codpos != '') {
            changeInput('codpos');
            var patt = /^(?=\d{5}$)(01|20|31|48)\d+/
            if (patt.test($("#del_codpos").val())){
                setZipType(1);
                zip_type_funct(zip_type.val());
            }else {
                setZipType(0);
                zip_type_funct(zip_type.val())
            }
            $.ajax({
                type: "POST",
                url: '/address/town',
                data: send,
                dataType: "json",
                success: function (resp) {
                    if(resp['status'] == 400){
                        if ($('#mens_info_del_codpos').length > 0) {
                            $('#mens_info_del_codpos').remove();
                        }
                        $('#del_codpos').addClass('caja_error');
                    }else{
                        var option_list = resp.towns;
                        $("#poblacion").empty();
                        if(option_list.length == 1){
                            $("#poblacion").append(
                                $("<option selected></option>").attr("value", option_list[0].poblacion).text(option_list[0].poblacion)
                            );
                            $("#poblacion").attr('disabled', true);
                            checkSelSex("#poblacion");
                            $("#placepoblacion").css('display', 'block');
                            provincia = option_list[0]['descripcion_provincia'];
                            $('#provincia').val(provincia);
                            checkInput("#provincia");
                            $("#placeprovincia").css('display', 'block');
                            getTipoVia();
                        }else {
                            for (var i = 0; i < option_list.length; i++) {
                                $("#poblacion").removeAttr('disabled');
                                $("#poblacion").append(
                                    $("<option></option>").attr("value", option_list[i]['poblacion']).text(option_list[i]['poblacion'])
                                );
                            }
                            $("#poblacion option:first").attr('selected','selected');
                            provincia = option_list[0]['descripcion_provincia'];
                            $('#provincia').val(provincia);
                            checkInput("#provincia");
                            $("#placeprovincia").css('display', 'block');
                            getTipoVia();
                        }
                        $('#poblacion').selectpicker('refresh');
                    }
                }
            });
        }
        else {
            if ($('#mens_info_del_codpos').length > 0) {
                $('#mens_info_del_codpos').remove();
            }
            $('#del_codpos').parent(this).after('<div id="mens_info_del_codpos" class="mens_error"><img src="/assets/img/demo/icono_ERROR.png">Ese código postal no existe.</div>');
        }
    });
    $("#tipo_via").change(function () {
        var send = {
            '_token': token,
            'params': {
                'code': $('#code').val()
            }
        };
        $.ajax({
            type: "POST",
            url: '/address/roadtype',
            data: send,
            dataType: "json",
            success: function (resp) {
                var option_list = resp['street']['road_types'];
                for (var i = 0; i < option_list.length; i++) {
                    if ($('#tipo_via').val() == option_list[i]['road_name']) {
                        $('#rt_id').val(option_list[i]['id']);
                    }
                }
                var rt_id = $('#rt_id').val();
                var send = {
                    '_token': token,
                    'params': {
                        'code': code,
                        'rt_id': rt_id,
                        'street_name': ''
                    }
                };
            }
        })
    });
    $("#via").change(function () {
        var send = {
            '_token': token,
            'params': {
                'code': $('#code').val(),
                'rt_id': $('#rt_id').val(),
                'street_name': $('#via').val()
            }
        };
        $.ajax({
            type: "POST",
            url: '/address/number',
            data: send,
            dataType: "json",
            success: function (resp) {
                var option_list = resp['street']['numbers'];
                $("#numero").empty();
                $("#numero").append($("<option></option>").attr("value", '').text('Número').attr('disabled', 'true').attr('selected', 'true'));
                for (var i = 0; i < option_list.length; i++) {
                    $("#numero").append(
                        $("<option></option>").attr("value", option_list[i]).text(option_list[i])
                    )
                }
                $('#numero').selectpicker('refresh');
            },
        })
    });
    //PARA LOS ZIP_TYPE = 0
    $("#numero").change(function () {
        if(zip_type.val() == 0) {
            getLetter();
        }
    })
    $('#letra').change(function () {
        if(zip_type.val() == 0) {
            getBloque();
        }
    })
    $('#bloque').change(function () {
        if(zip_type.val() == 0) {
            getEsc();
        }
    })
    $('#escalera').change(function () {
        if(zip_type.val() == 0) {
            getPiso();
        }
    })
    $('#piso').change(function () {
        if(zip_type.val() == 0) {
            getPuerta();
        }
    })
    $('#submit_button').on('click', function () {
        valForm = []; //Vaciamos array de validación
        var validateForm = 1; // 1 = Valido, 0 = No Valido DC
        validateApiForm();
        for (var i = 0; i < valForm.length; i++) {
            //console.log('Dentro ' + valForm[i][0]);
            if (valForm[i][1] == 0) {
                validateForm = 0;
                $('html, body').animate({
                    scrollTop: ($("#"+valForm[i][0]).offset().top -90)
                }, 1000);
                break
            }
            else {
            }
        }
        if (validateForm != 0) {
            var dia = $("#date_nac").val().substr(0, 2);
            var mes = $("#date_nac").val().substr(3, 2);
            var anio = $("#date_nac").val().substr(6, 4);
            var date = anio + '-' + mes + '-' + dia;
            //$('#modal_api').fadeIn();
            var sexo = '';
            if ($("#sexo").attr('title') == 'Mujer') {
                sexo = 'F'
            }
            else {
                sexo = 'M'
            }
            if (house == 3) {
                type = 'house';
            }
            var send = {};
            var send = {
                '_token': token,
                'params': {
                    'nombre': $("#nombre").val(),
                    'primer_apellido': $("#primer_apellido").val(),
                    'segundo_apellido': $("#segundo_apellido").val(),
                    'sexo': sexo,
                    'NIF': $("#NIF").val(),
                    'email': $("#email").val(),
                    'telefono': $("#telefono").val(),
                    'date_nac': date,
                    'via': $("#rt_id").val(),
                    //'assoc_id': '1',
                    'direccion': $("#via").val(),
                    'numero': $("#numero").val(),
                    'km': $("#km").val(),
                    'letra': $("#letra").val(),
                    'bloque': $("#bloque").val(),
                    'escalera': $("#escalera").val(),
                    'puerta': $("#puerta").val(),
                    'piso': $("#piso").val(),
                    'del_codpos': $("#del_codpos").val(),
                    'poblacion': $("#poblacion").val(),
                    'provincia': $("#provincia").val(),
                    'type': type,
                    'key': $('#api_key').val(),
                    'secureApiKey': $('#secureApiKey').val(),
                    //'casapiso': 0,
                    //'consistencia' : globalCons
                }
            };
            $.ajax({
                type: "POST",
                url: '/demo',
                data: send,
                dataType: "json",
                beforeSend: function () {
                    $('#loadingModal').modal('show');
                },
                success: function (result) {
                    $('body').html(result.html);
                },
                complete: function () {
                    $("#loadingModal").modal('hide');
                    $("body").removeClass("modal-open");
                    $(".modal-backdrop").remove();
                    $("body").css('padding-right', '0px');
                },
            });
        }
        else {}
    });
});
var a = 0;
// DC params entrada = 00, 01 -> búsqueda normal, búsqueda libre;
function zip_type_funct(zip) {
    if (zip == 1){
        $('#km').removeAttr('disabled');
        $("#km_img").removeClass('img_disabled');
        $('#km').selectpicker('destroy');
        $('#km').remove();
        $('#km_img').after('<input type="text" id="km" placeholder="Kilómetro" class="selectpicker form-control input_label bs-select-hidden"/>');
        $('#bloque').removeAttr('disabled');
        $("#bloque_img").removeClass('img_disabled');
        $('#bloque').empty();
        $('#bloque').selectpicker('destroy');
        $('#bloque').remove();
        $('#bloque_img').after('<input type="text" id="bloque" placeholder="Bloque" class="selectpicker form-control input_label bs-select-hidden"/>');
        $('#escalera').removeAttr('disabled');
        $("#escalera_img").removeClass('img_disabled');
        $('#escalera').selectpicker('destroy');
        $('#escalera').remove();
        $('#escalera_img').after('<input type="text" id="escalera" placeholder="Escalera" class="selectpicker form-control input_label bs-select-hidden"/>');
        $('#piso').removeAttr('disabled');
        $("#piso_img").removeClass('img_disabled');
        $('#piso').selectpicker('destroy');
        $('#piso').remove();
        $('#piso_img').after('<input type="text" id="piso" placeholder="Piso" class="selectpicker form-control input_label bs-select-hidden"/>');
        $('#puerta').removeAttr('disabled');
        $("#puerta_img").removeClass('img_disabled');
        $('#puerta').selectpicker('destroy');
        $('#puerta').remove();
        $('#puerta_img').after('<input type="text" id="puerta" placeholder="Puerta" class="selectpicker form-control input_label bs-select-hidden"/>');
        $('#letra').removeAttr('disabled');
        $("#letra_img").removeClass('img_disabled');
        $('#letra').selectpicker('destroy');
        $('#letra').remove();
        $('#letra_img').after('<input type="text" id="letra" placeholder="Letra" class="selectpicker form-control input_label bs-select-hidden"/>');
        a = 1;
    }
    else if(zip == 0){
        if (a == 1) {
            removeValidation('km');
            $('#km').remove();
            $('#km_img').after('<select class="selectpicker form-control input_label" id="km" onChange="checkSelSex(this)" data-live-search="true" disabled>' +
                '<option value="" disabled selected> Kilómetro</option>' +
                '</select>');
            $('#km').selectpicker('refresh');
            removeValidation('bloque');
            $('#bloque').remove();
            $('#bloque_img').after('<select class="selectpicker form-control input_label" id="bloque" onChange="checkSelSex(this)" data-live-search="true" disabled>' +
                '<option value="" disabled selected> Bloque</option>' +
                '</select>');
            $('#bloque').selectpicker('refresh');
            removeValidation('escalera');
            $('#escalera').remove();
            $('#escalera_img').after('<select class="selectpicker form-control input_label" id="escalera" onChange="checkSelSex(this)" data-live-search="true" disabled>' +
                '<option value="" disabled selected> Escalera</option>' +
                '</select>');
            $('#escalera').selectpicker('refresh');
            removeValidation('piso');
            $('#piso').remove();
            $('#piso_img').after('<select class="selectpicker form-control input_label" id="piso" onChange="checkSelSex(this)" data-live-search="true" disabled>' +
                '<option value="" disabled selected> Piso</option>' +
                '</select>');
            $('#piso').selectpicker('refresh');
            removeValidation('puerta');
            $('#puerta').remove();
            $('#puerta_img').after('<select class="selectpicker form-control input_label" id="puerta" onChange="checkSelSex(this)" data-live-search="true" disabled>' +
                '<option value="" disabled selected> Puerta</option>' +
                '</select>');
            $('#puerta').selectpicker('refresh');
            removeValidation('letra');
            $('#letra').remove();
            $('#letra_img').after('<select class="selectpicker form-control input_label" id="letra" onChange="checkSelSex(this)" data-live-search="true" disabled>' +
                '<option value="" disabled selected> Letra</option>' +
                '</select>');
            $('#letra').selectpicker('refresh');
            a = 0;
        }
    }
}
function getSearch(type) {
    if (type == 0) {
        setTimeout(function () {
            $('.no-results').empty();
            $('.no-results').html('Buscando..' + $('.no-results').html());
        }, 10);
    }
    else {
        setTimeout(function () {
            $('.no-results').empty();
            $('.no-results').html('No hay resultados.');
        }, 10);
    }
};
// $("#poblacion").change(function () {
function getTipoVia(){
    var codpos = $("#del_codpos").val();
    var send = {
        '_token': token,
        'params': {
            'zip_code': codpos,
            'town': $("#poblacion").val()
        }
    };
    $.ajax({
        type: "POST",
        url: '/address/code',
        data: send,
        dataType: "json",
        success: function (resp) {
            $('#code').val(resp['code']);
            var send = {
                '_token': token,
                'params': {
                    'code': $('#code').val(),
                }
            };
            $.ajax({
                type: "POST",
                url: '/address/roadtype',
                data: send,
                dataType: "json",
                success: function (resp) {
                    var option_list = resp['street']['road_types'];
                    $("#tipo_via").empty();
                    $("#tipo_via").append($("<option></option>").attr("value", '').text('Tipo de vía').attr('disabled', 'true').attr('selected', 'true'));
                    for (var i = 0; i < option_list.length; i++) {
                        $("#tipo_via").append(
                            $("<option></option>").attr("value", option_list[i]['road_name']).text(option_list[i]['road_name'])
                        );
                    }
                    $('#tipo_via').selectpicker('refresh');
                },
            })
        }
    });
};
function getVia(via) {
    var send = {
        '_token': token,
        'params': {
            'code': $('#code').val(),
            'rt_id': $('#rt_id').val(),
            'street_name': normalize(via)
        }
    };
    $.ajax({
        type: "POST",
        url: '/address/streetname',
        data: send,
        dataType: "json",
        beforeSend: function () {
            getSearch(0)
        },
        success: function (resp) {
            var option_list = resp['street']['names'];
            getSearch(1)
            $("#via").empty();
            $("#via").append($("<option></option>").attr("value", '').text('Nombre de vía').attr('disabled', 'true').attr('selected', 'true'));
            for (var i = 0; i < option_list.length; i++) {
                $("#via").append(
                    $("<option></option>").attr("value", option_list[i]).text(option_list[i])
                );
            }
            $('#via').selectpicker('refresh');
        },
    })
}
function getLetter() {
    var send = {
        '_token': token,
        'params': {
            'code': $('#code').val(),
            'rt_id': $('#rt_id').val(),
            'street_name': $('#via').val(),
            'number': $('#numero').val(),
            'letter': ''
        }
    };
    $.ajax({
        type: "POST",
        url: '/address/letter',
        data: send,
        dataType: "json",
        success: function (resp) {
            var option_list = resp['street']['letters'];
            $("#letra").empty();
            $("#letra").append($("<option></option>").attr("value", '').text('Letra').attr('disabled', 'true').attr('selected', 'true'));
            if (resp['status'] == 200) {
                if(option_list.length == 1){
                    $("#letra").append(
                        $("<option selected></option>").attr("value", option_list[0]).text(option_list[0])
                    )
                    getKm();
                }else {
                    for (var i = 0; i < option_list.length; i++) {
                        if (option_list[i] == '') {
                            $("#letra").append(
                                $("<option></option>").attr("value", 'EMPTY').text('Sin letra')
                            )
                            addValidation("letra");
                        }
                        else {
                            $("#letra").append(
                                $("<option></option>").attr("value", option_list[i]).text(option_list[i])
                            )
                            addValidation("letra");
                        }
                    }
                }
                $('#letra').selectpicker('refresh');
                //getKm();
            }
            else if(resp['status'] == 401){
                removeValidation("letra");
                $('#letra').selectpicker('refresh');
                getKm();
            }
            else {
                // console.log(resp);
                //ERROR 400, PONERSE EN CONTACTO CON EL EQUIPO TECNICO
            }
        }
    });
}
function getKm(){
    var send = {
        '_token': token,
        'params': {
            'code': $('#code').val(),
            'rt_id': $('#rt_id').val(),
            'street_name': $('#via').val(),
            'number': $('#numero').val(),
            'letter': $('#letra').val(),
            'km': ''
        }
    };
    $.ajax({
        type: "POST",
        url: '/address/km',
        data: send,
        dataType: "json",
        success: function (resp) {
            var option_list = resp['street']['kms'];
            $("#km").empty();
            $("#km").append($("<option></option>").attr("value", '').text('Kilómetro').attr('disabled', 'true').attr('selected', 'true'));
            if (resp['status'] == 200) {
                if(option_list.length == 1){
                    $("#km").append(
                        $("<option selected></option>").attr("value", option_list[0]).text(option_list[0])
                    )
                    getBloque();
                }else {
                    for (var i = 0; i < option_list.length; i++) {
                        if (option_list[i] == '') {
                            $("#km").append(
                                $("<option></option>").attr("value", 'EMPTY').text('Sin kilómetro')
                            )
                            addValidation("km");
                        }
                        else if (option_list[i] == '00000') {
                            removeValidation("km");
                            $('#km').selectpicker('refresh');
                            getBloque();
                        }
                        else {
                            $("#km").append(
                                $("<option></option>").attr("value", option_list[i]).text(option_list[i])
                            )
                            addValidation("km");
                        }
                    }
                }
                $('#km').selectpicker('refresh');
                //getBloque();
            }
            else if(resp['status'] == 401){
                removeValidation("km");
                $('#km').selectpicker('refresh');
                getBloque();
            }
            else {
                //ERROR 400, PONERSE EN CONTACTO CON EL EQUIPO TECNICO
            }
        }
    })
}

function getBloque() {
    var send = {
        '_token': token,
        'params': {
            'code': $('#code').val(),
            'rt_id': $('#rt_id').val(),
            'street_name': $('#via').val(),
            'number': $('#numero').val(),
            'letter': $('#letra').val(),
            'km': $('#km').val(),
            'block': ''
        }
    };
    $.ajax({
        type: "POST",
        url: '/address/block',
        data: send,
        dataType: "json",
        success: function (resp) {
            var option_list = resp['street']['blocks'];
            $("#bloque").empty();
            $("#bloque").append($("<option></option>").attr("value", '').text('Bloque').attr('disabled', 'true').attr('selected', 'true'));
            if (resp['status'] == 200) {
                if(option_list.length == 1){
                    $("#bloque").append(
                        $("<option selected></option>").attr("value", option_list[0]).text(option_list[0])
                    )
                    getEsc();
                }else {
                    for (var i = 0; i < option_list.length; i++) {
                        if (option_list[i] == '') {
                            $("#bloque").append(
                                $("<option></option>").attr("value", 'EMPTY').text('Sin bloque')
                            )
                            addValidation("bloque");
                        }
                        else {
                            $("#bloque").append(
                                $("<option></option>").attr("value", option_list[i]).text(option_list[i])
                            )
                            addValidation("bloque");
                        }
                    }
                }
                $('#bloque').selectpicker('refresh');
                //getEsc();
            }
            else if(resp['status'] == 401){
                removeValidation("bloque");
                $('#bloque').selectpicker('refresh');
                getEsc();
            }
            else {
                //ERROR 400, PONERSE EN CONTACTO CON EL EQUIPO TECNICO
            }
        }
    })
}
function getEsc() {
    var send = {
        '_token': token,
        'params': {
            'code': $('#code').val(),
            'rt_id': $('#rt_id').val(),
            'street_name': $('#via').val(),
            'number': $('#numero').val(),
            'letter': $('#letra').val(),
            'km': $('#km').val(),
            'block': $('#bloque').val(),
            'stairs': ''
        }
    };
    $.ajax({
        type: "POST",
        url: '/address/stairs',
        data: send,
        dataType: "json",
        success: function (resp) {
            var option_list = resp['street']['stairs'];
            $("#escalera").empty();
            $("#escalera").append($("<option></option>").attr("value", '').text('Escalera').attr('disabled', 'true').attr('selected', 'true'));
            if (resp['status'] == 200) {
                if(option_list.length == 1){
                    $("#escalera").append(
                        $("<option selected></option>").attr("value", option_list[0]).text(option_list[0])
                    )
                    getPiso();
                }else {
                    for (var i = 0; i < option_list.length; i++) {
                        if (option_list[i] == '') {
                            $("#escalera").append(
                                $("<option></option>").attr("value", 'EMPTY').text('Sin escalera')
                            )
                            addValidation("escalera");
                        }
                        else if (option_list[i] == 'T') {
                            house = house + 1;
                            if (option_list.length == 1) {
                                removeValidation("escalera");
                                getPiso();
                            }
                        }
                        else if (option_list[i] == 'S') {
                            if (option_list.length == 1) {
                                removeValidation("escalera");
                                getPiso();
                            }
                        }
                        else {
                            house = house - 1;
                            $("#escalera").append(
                                $("<option></option>").attr("value", option_list[i]).text(option_list[i])
                            )
                            addValidation("escalera");
                        }
                    }
                }
                $('#escalera').selectpicker('refresh');
                //getPiso();
            }
            else if(resp['status'] == 401){
                removeValidation("escalera");
                $('#escalera').selectpicker('refresh');
                getPiso();
            }
            else {
                //ERROR 400, PONERSE EN CONTACTO CON EL EQUIPO TECNICO
            }
        }
    })
}
function getPiso() {
    var send = {
        '_token': token,
        'params': {
            'code': $('#code').val(),
            'rt_id': $('#rt_id').val(),
            'street_name': $('#via').val(),
            'number': $('#numero').val(),
            'letter': $('#letra').val(),
            'km': $('#km').val(),
            'block': $('#bloque').val(),
            'stairs': $('#escalera').val(),
            'floor': ''
        }
    };
    $.ajax({
        type: "POST",
        url: '/address/floor',
        data: send,
        dataType: "json",
        success: function (resp) {
            var option_list = resp['street']['floors'];
            $("#piso").empty();
            $("#piso").append($("<option></option>").attr("value", '').text('Piso').attr('disabled', 'true').attr('selected', 'true'));
            if (resp['status'] == 200) {
                if(option_list.length == 1){
                    $("#piso").append(
                        $("<option selected></option>").attr("value", option_list[0]).text(option_list[0])
                    )
                    getPuerta();
                }else {
                    for (var i = 0; i < option_list.length; i++) {
                        if (option_list[i] == '') {
                            $("#piso").append(
                                $("<option></option>").attr("value", 'EMPTY').text('Sin piso')
                            )
                            addValidation("piso");
                        } else if (option_list[i] == 'OD') {
                            if (option_list.length == 1) {
                                house = house + 1;
                                removeValidation("piso");
                                getPuerta();
                            }
                        } else if (option_list[i] == 'UE') {
                            if (option_list.length == 1) {
                                removeValidation("piso");
                                getPuerta();
                            }
                        } else {
                            house = house - 1;
                            $("#piso").append(
                                $("<option></option>").attr("value", option_list[i]).text(option_list[i])
                            )
                            addValidation("piso");
                        }
                    }
                }
                $('#piso').selectpicker('refresh');
                //getPuerta();
            }
            else if(resp['status'] == 401){
                removeValidation("piso");
                $('#piso').selectpicker('refresh');
                getPuerta();
            }
            else {
                //ERROR 400, PONERSE EN CONTACTO CON EL EQUIPO TECNICO
            }
        }
    })
}
function getPuerta() {
    var send = {
        '_token': token,
        'params': {
            'code': $('#code').val(),
            'rt_id': $('#rt_id').val(),
            'street_name': $('#via').val(),
            'number': $('#numero').val(),
            'letter': $('#letra').val(),
            'km': $('#km').val(),
            'block': $('#bloque').val(),
            'stairs': $('#escalera').val(),
            'floor': $('#piso').val(),
            'door': ''
        }
    };
    $.ajax({
        type: "POST",
        url: '/address/door',
        data: send,
        dataType: "json",
        success: function (resp) {
            var option_list = resp['street']['doors'];
            $("#puerta").empty();
            $("#puerta").append($("<option></option>").attr("value", '').text('Puerta').attr('disabled', 'true').attr('selected', 'true'));
            if (resp['status'] == 200) {
                if(option_list.length == 1){
                    $("#puerta").append(
                        $("<option selected></option>").attr("value", option_list[0]).text(option_list[0])
                    )
                }else {
                    for (var i = 0; i < option_list.length; i++) {
                        if (option_list[i] == '') {
                            $("#puerta").append(
                                $("<option></option>").attr("value", 'EMPTY').text('Sin puerta')
                            )
                            addValidation("puerta");
                        } else if (option_list[i] == 'OS') {
                            house = house + 1;
                            if (option_list.length == 1) {
                                removeValidation("puerta");
                                // getPuerta();
                            }
                        } else if (option_list[i] == 'LO') {
                            if (option_list.length == 1) {
                                removeValidation("puerta");
                                // getPuerta();
                            }
                        } else {
                            house = house - 1;
                            $("#puerta").append(
                                $("<option></option>").attr("value", option_list[i]).text(option_list[i])
                            )
                            addValidation("puerta");
                        }
                    }
                }
                $('#puerta').selectpicker('refresh');
                //getPuerta();
            }
            else if(resp['status'] == 401){
                removeValidation("puerta");
                $('#puerta').selectpicker('refresh');
            }
            else {
                //ERROR 400, PONERSE EN CONTACTO CON EL EQUIPO TECNICO
            }
        }
    })
}
function removeValidation(id) {
    // console.log(id);
    $('#'+id).attr('disabled', 'true');
    $('#'+id).css('display', 'none');
    $('#div_sel_'+id).selectpicker('refresh');
    $('#'+id + "_img").addClass('img_disabled');
}
function addValidation(id) {
    $('#'+id).selectpicker('refresh');
    $('#'+id).removeAttr('disabled');
    $('#div_sel_'+id).css('display', 'block');
    $('#'+id + "_img").removeClass('img_disabled');
}
//FUNCIONES PARA OCULTAR CAMPOS DC
function hidePoblacion() {
    $("#poblacion").empty();
    $("#poblacion").append($("<option></option>").attr("value", '').text('Población').attr('disabled', 'true').attr('selected', 'true'));
    $("#poblacion").selectpicker('refresh');
    $("#provincia").val('');
    $("#provincia").attr('placeholder', 'Provincia');
}
function hideTipoVia() {
    $("#tipo_via").empty();
    $("#tipo_via").append($("<option></option>").attr("value", '').text('Tipo de vía').attr('disabled', 'true').attr('selected', 'true'));
    $("#tipo_via").selectpicker('refresh');
}
function hideVia() {
    $("#via").empty();
    $("#via").append($("<option></option>").attr("value", '').text('Nombre de vía').attr('disabled', 'true').attr('selected', 'true'));
    $("#via").selectpicker('refresh');
}
function hideNumero() {
    $("#numero").empty();
    $("#numero").append($("<option></option>").attr("value", '').text('Número').attr('disabled', 'true').attr('selected', 'true'));
    $("#numero").selectpicker('refresh');
}
function hideOptions() {
    $("#div_sel_km").css('display', 'none');
    $("#km").empty();
    $("#km").append($("<option></option>").attr("value", '').text('Kilómetro').attr('disabled', 'true').attr('selected', 'true'));
    removeValidation("km");
    $("#div_sel_letra").css('display', 'none');
    $("#letra").empty();
    $("#letra").append($("<option></option>").attr("value", '').text('Letra').attr('disabled', 'true').attr('selected', 'true'));
    removeValidation("letra");
    $("#div_sel_bloque").css('display', 'none');
    $("#bloque").empty();
    $("#bloque").append($("<option></option>").attr("value", '').text('Bloque').attr('disabled', 'true').attr('selected', 'true'));
    removeValidation("bloque");
    $("#div_sel_escalera").css('display', 'none');
    $("#escalera").empty();
    $("#escalera").append($("<option></option>").attr("value", '').text('Escalera').attr('disabled', 'true').attr('selected', 'true'));
    removeValidation("escalera");
    $("#div_sel_puerta").css('display', 'none');
    $("#puerta").empty();
    $("#puerta").append($("<option></option>").attr("value", '').text('Puerta').attr('disabled', 'true').attr('selected', 'true'));
    removeValidation("puerta");
    $("#div_sel_piso").css('display', 'none');
    $("#piso").empty();
    $("#piso").append($("<option></option>").attr("value", '').text('Piso').attr('disabled', 'true').attr('selected', 'true'));
    removeValidation("piso");
}
function changeInput(value) {
    if (value == 'numero') {
        hideOptions();
    }
    else if (value == 'via') {
        hideOptions();
        hideNumero();
    }
    else if (value == 'tipoVia') {
        hideOptions();
        hideNumero();
        hideVia();
    }
    else if (value == 'poblacion') {
        hideOptions();
        hideNumero();
        hideVia();
        hideTipoVia();
    }
    else if (value == 'codpos') {
        hideOptions();
        hideNumero();
        hideVia();
        hideTipoVia();
        hidePoblacion();
    }
};
function validateSmsForm() {
    checkInput('#nombre');
    checkInput('#primer_apellido');
    checkInput('#segundo_apellido');
    checkDoc("#NIF");
    checkEmail("#email");
    checkTelf("#telefono")
    checkFech("#date_nac");
    checkInt("#del_codpos");
    checkSelSex("#poblacion");
    checkInput("#provincia");
    checkSelSex("#tipo_via");
    checkSelSex("#via");
    checkSelSex("#numero");
    if ($('#km').prop('placeholder') == 'Kilómetro'){}
    else if (!$('#km').prop('disabled')) {
        checkSelSex("#km");
    }
    if ($('#letra').prop('placeholder') == 'Letra'){}
    else if (!$('#letra').prop('disabled')) {
        checkSelSex("#letra");
    }
    if ($('#bloque').prop('placeholder') == 'Bloque'){}
    else if (!$('#bloque').prop('disabled')) {
        checkSelSex("#bloque");
    }
    if ($('#escalera').prop('placeholder') == 'Escalera'){}
    else if (!$('#escalera').prop('disabled')) {
        checkSelSex("#escalera");
    }
    if ($('#puerta').prop('placeholder') == 'Puerta'){}
    else if (!$('#puerta').prop('disabled')) {
        checkSelSex("#puerta");
    }
    if ($('#piso').prop('placeholder') == 'Piso'){}
    else if (!$('#piso').prop('disabled')) {
        checkSelSex("#piso");
    }
}
function validateApiForm() {
    checkInput('#nombre');
    checkInput('#primer_apellido');
    checkInput('#segundo_apellido');
    //checkSelSex('#sexo');
    checkDoc("#NIF");
    checkEmail("#email");
    checkTelf("#telefono");
    // checkTelf("#telefonoCod");
    checkFech("#date_nac");
    checkInt("#del_codpos");
    checkSelSex("#poblacion");
    checkInput("#provincia");
    checkSelSex("#tipo_via");
    checkSelSex("#via");
    checkSelSex("#numero");
    checkInt('#api_key');
    checkInt('#secureApiKey');
    checkCheckbox("#acepto_cond");
    if ($('#km').prop('placeholder') == 'Kilómetro'){}
    else if (!$('#km').prop('disabled')) {
        checkSelSex("#km");
    }
    if ($('#letra').prop('placeholder') == 'Letra'){}
    else if (!$('#letra').prop('disabled')) {
        checkSelSex("#letra");
    }
    if ($('#bloque').prop('placeholder') == 'Bloque'){}
    else if (!$('#bloque').prop('disabled')) {
        checkSelSex("#bloque");
    }
    if ($('#escalera').prop('placeholder') == 'Escalera'){}
    else if (!$('#escalera').prop('disabled')) {
        checkSelSex("#escalera");
    }
    if ($('#puerta').prop('placeholder') == 'Puerta'){}
    else if (!$('#puerta').prop('disabled')) {
        checkSelSex("#puerta");
    }
    if ($('#piso').prop('placeholder') == 'Piso'){}
    else if (!$('#piso').prop('disabled')) {
        checkSelSex("#piso");
    }
    $('#mens_info_secureApiKey').empty();
}
function syntaxHighlight(json) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}
function number_format(number, decimals, dec_point, thousands_sep) {
    // Strip all characters but numerical ones.
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}
var normalize = (function() {
    var from = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÇç",
        to   = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuucc",
        mapping = {};

    for(var i = 0, j = from.length; i < j; i++ )
        mapping[ from.charAt( i ) ] = to.charAt( i );

    return function( str ) {
        var ret = [];
        for( var i = 0, j = str.length; i < j; i++ ) {
            var c = str.charAt( i );
            if( mapping.hasOwnProperty( str.charAt( i ) ) )
                ret.push( mapping[ c ] );
            else
                ret.push( c );
        }
        return ret.join( '' );
    }

})();