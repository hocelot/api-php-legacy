var token = $('meta[name="csrf-token"]').attr('content');
$(document).ready(function(e) {
    $("html, body").animate({ scrollTop: 0 }, "slow");
    $('#mensaje').on('focus', function (){
        $('#mensaje').empty();
    });
    $('#submit_button').on('click', function (){
        valForm = [];
        var validateForm = 1;
        validateContactForm();
        for (var i = 0; i < valForm.length; i++){
            if(valForm[i][1] == 0){
                validateForm = 0;
            }
            else{
            }
        }
        if(validateForm == 0){
            $('#error_text').html('Revisa los campos resaltados puede ser que el formato sea incorrecto o sea un campo obligatorio.');
        }
        if(validateForm != 0){
            $('#error_text').empty();
            var send = {
                '_token': token,
                'params': {
                    'nombre': $("#nombre").val(),
                    'apellidos': $("#apellidos").val(),
                    'email': $("#email").val(),
                    'cargo': $("#cargo").val(),
                    'nom_emp': $("#nombre_emp").val(),
                    'tel' : $("#telefono").val(),
                    'origen': 'Contrata Demo'
                }
            }
            $.ajax({
                type: "POST",
                url: '/newContact/solicitar',
                data: send,
                dataType: "json",
                success: function (result) {
                    //console.log(result+'SUCCESS');
                    if (result['status'] == 200){
                        $('#ModalContactBody').html('<div class="col-md-12 col-sm-12 col-xs-12 text-center"><h1>¡Oído cocina! Gracias por tu interés en nuestros servicios.</h1></div>');
                    }else if(result['status'] == 400) {
                        $('#ModalContactBody').html('<div class="col-md-12 col-sm-12 col-xs-12 text-center"><h1>Ha ocurrido un error, no hemos podido realizar tu petición.</h1></div>');
                    }else {
                        $('#ModalContactBody').html('<div class="col-md-12 col-sm-12 col-xs-12 text-center"><h1>Ha ocurrido un error, no hemos podido realizar tu petición.</h1></div>');
                    }
                    $('#contactaSmooth').addClass('disabledButton');
                },
                error: function (result) {
                    $('#ModalContactBody').html('<div class="col-md-12 col-sm-12 col-xs-12 text-center"><h1>Ha ocurrido un error, no hemos podido realizar tu petición.</h1></div>');
                    $('#contactaSmooth').addClass('disabledButton');
                }
            });
        }
    });
})
function validateContactForm(){
    checkInput('#nombre');
    checkInput('#apellidos');
    checkInput('#nombre_emp');
    checkInput('#cargo');
    checkEmail('#email');
    checkTelf("#telefono");
}