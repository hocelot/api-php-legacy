var token = $('meta[name="csrf-token"]').attr('content');
var urlBase = "http://api.hocelot.com/admin";
var grid = $("#grid-data-api").bootgrid({
    caseSensitive:false,
    ajax: true,
    cache: false,
    requestHandler: function (request) {
        request._token = token;
        return request;
    },
    cache: false,
    url: urlBase + '/clients/all',
    formatters: {
        "site": function(column, row){
            if(row.site == 'H'){
                return 'Hocelot';
            }else if(row.site == 'M'){
                return 'Mirating';
            }else if(row.site == 'A'){
                return 'Api';
            }
        },
        "commands": function(column, row)
        {
            return "<button type=\"button\" class=\"btn btn-xs btn-default command-edit\" data-row-id=\"" + row.id + "\"><span class=\"fa fa-edit\"></span></button> " +
                "<button type=\"button\" class=\"btn btn-xs btn-default command-delete\" data-row-id=\"" + row.id + "\"><span class=\"fa fa-trash-o\"></span></button> "+
                "<button type=\"button\" class=\"btn btn-xs btn-default command-view\" data-row-id=\"" + row.id + "\"><span class=\"fa fa-eye\"></span></button>";
        }
    }
});
$('#grid-data-api').on("loaded.rs.jquery.bootgrid", function()	{
    grid.find(".command-edit").on("click", function(e)
    {
        editContactForm($(this).data("row-id"));
    }).end().find(".command-delete").on("click", function(e)
    {
        confirmDelete($(this).data("row-id"));
    }).end().find(".command-view").on("click", function(e)
    {
        getViewContact($(this).data("row-id"));
    });
});

function confirmDelete(rowid) {
    $('#showModal').modal('show');
    $('#showModal').find('.modal-title').text('Borrar cliente.');
    $('#show').empty();
    $('#delContactButton').remove();
    $('#show').append('<p>¿Borrar cliente con tercod '+rowid+'?</p>');
    $('.modal-footer').append('<button id="delContactButton" type="button" class="btn btn-secondary" onclick="javascript:delContact('+rowid+')">Aceptar</button>');
}
function delContact(rowid){
    var send = {
        '_token': token,
        'params': {
            'id': rowid
        }
    };
    $.ajax({
        url: urlBase + '/clients/delClient',
        type: 'POST',
        data: send,
        success: function (result) {
            $('#showModal').modal('hide');
            $('#delContactButton').remove();
            $('#grid-data-api').bootgrid('reload');
            alert(result.msg);
        }
    });
};
function getViewContact(rowid) {
    var send = {
        '_token': token,
        'params': {
            'id': rowid
        }
    };
    $.ajax({
        url: urlBase + '/clients/viewClient',
        type: 'POST',
        data: send,
        success:function(result){
            $('#delContactButton').remove();
            $('#showModal').modal('show');
            $('#showModal').find('.modal-title').text('Cliente: '+result.clientSecret.name);
            $('#show').empty();
            $('#show').append('<p><strong>TerCod:</strong> '+result.client.id+'</p>');
            $('#show').append('<p><strong>Nombre:</strong> '+result.clientSecret.name+'</p>');
            $('#show').append('<p><strong>Email:</strong> '+result.client.email+'</p>');
            $('#show').append('<p><strong>Contraseña encriptada:</strong> '+result.client.password+'</p>');
            $('#show').append('<p><strong>Site:</strong> '+result.client.site+'</p>');
            $('#show').append('<p><strong>Fecha de alta:</strong> '+result.client.join_date+'</p>');
            $('#show').append('<p><strong>Última conexión:</strong> '+result.login.fecha+'</p>');
            $('#show').append('<p><strong>Ip:</strong> '+result.login.ip+'</p>');
            $('#show').append('<p><strong>Navegador:</strong> '+result.login.navegador+'</p>');
            $('#show').append('<p><strong>Id:</strong> '+result.clientSecret.id+'</p>');
            $('#show').append('<p><strong>Api key:</strong> '+result.clientSecret.secret+'</p>');
            $('#show').append('<p><strong>Permisos:</strong> '+result.clientPerm+'</p>');
            $('#show').append('<p><strong>Clave Pública:</strong> '+result.clientKeys.public_key+'</p>');
            $('#show').append('<p><strong>Clave Privada:</strong> '+result.clientKeys.private_key+'</p>');
        }
    });
}
function editContact() {
    if($("#clientPermRisk").attr('checked')){RiskScore = 'RiskScore'}else {RiskScore = ''}
    if($("#clientPermIc").attr('checked')){IdCheck = 'IdCheck'}else {IdCheck = ''}
    if($("#clientPermIf").attr('checked')){IdFraud = 'IdFraud'}else {IdFraud = ''}
    if($("#clientPermGc").attr('checked')){GeoCheck = 'GeoCheck'}else {GeoCheck = ''}
    if($("#clientPermGf").attr('checked')){GeoFraud = 'GeoFraud'}else {GeoFraud = ''}
    if($("#clientPermAdd").attr('checked')){Address = 'Address'}else {Address = ''}
    var perm = [RiskScore, IdCheck, IdFraud, GeoCheck, GeoFraud, Address];
    var send1 = {
        '_token': token,
        'params': {
            'id': $('#terCod').val()
        }
    };
    // $.ajax({
    //     url: urlBase + '/clients/viewClient',
    //     type: 'POST',
    //     data: send1,
    //     success: function (result) {
    //         return result;
    //     }
    // });
    var send = {
        '_token': token,
        'params': {
            'id': $('#terCod').val(),
            'name': $('#name').val(),
            'site': $('#site').val(),
            'email': $('#email').val(),
            'password': $('#password').val(),
            'clientPerm': perm,
            // 'changeVal': change
        }
    };
    $.ajax({
        url: urlBase + '/clients/editClient',
        type: 'POST',
        data: send,
        success: function (result) {
            $('#showModal').modal('hide');
            $('#delContactButton').remove();
            $('#grid-data-api').bootgrid('reload');
            alert(result.msg);
        }
    });
}
function editContactForm(rowid){
    var send = {
        '_token': token,
        'params': {
            'id': rowid
        }
    };
    $.ajax({
        url: urlBase + '/clients/viewClient',
        type: 'POST',
        data: send,
        success:function(result) {
            perm=result.clientPerm.split(' ');
            $('#delContactButton').remove();
            $('#showModal').modal('show');
            $('#showModal').find('.modal-title').text('Editar cliente con TerCod: ' + result.client.id);
            $('#show').empty();
            $('#show').append('<input type="hidden" value="'+result.client.id+'" id="terCod">');
            $('#show').append('<div id="showEdit" class="col-md-6 col-xs-12">');
            $('#showEdit').append('<div class="form-group">'+
                '<label class="col-md-3 col-xs-12" for="name">Nombre</label>' +
                '<input class="col-md-9 col-xs-12" type="text"  id="name"  value="' + result.clientSecret.name + '" placeholder="Nombre">');
            $('#showEdit').append('<div class="form-group">'+
                '<label class="col-md-3 col-xs-12" for="site">Site</label>' +
                '<input class="col-md-9 col-xs-12" type="text"  id="site"  value="' + result.client.site + '" placeholder="Site">');
            $('#show').append('</div>');
            $('#show').append('<div id="showEdit2" class="col-md-6 col-xs-12">');
            $('#showEdit2').append('<div class="form-group">'+
                '<label class="col-md-3 col-xs-12" for="email">Email</label>' +
                '<input class="col-md-9 col-xs-12" type="text"  id="email"  value="' + result.client.email + '" placeholder="Email">');
            $('#showEdit2').append('<div class="form-group">'+
                '<label class="col-md-3 col-xs-12" for="password">Contraseña </label>' +
                '<input class="col-md-9 col-xs-12" type="text"  id="password"  value="' + result.client.password + '" placeholder="Contraseña">');
            $('#show').append('</div>');
            $('#show').append('<div class="col-md-11 col-xs-12" style="margin-top: 20px"><label>Permisos:</label></div>');

            $('#show').append(''+
                '<div class="col-md-4 col-xs-6 form-check" id="div_risk">'+
                '<label class="form-check-label">Risk Score '+
                '<input class="form-check-input" type="checkbox" id="clientPermRisk" value="RiskScore">'+
                '</label>'+
                '</div>'+
                '<div class="col-md-4 col-xs-6 form-check" id="div_idcheck">'+
                '<label class="form-check-label">Id Check '+
                '<input class="form-check-input" type="checkbox" id="clientPermIc" value="IdCheck">'+
                '</label>'+
                '</div>'+
                '<div class="col-md-4 col-xs-6 form-check" id="div_idfraud">'+
                '<label class="form-check-label">Id Fraud '+
                '<input class="form-check-input" type="checkbox" id="clientPermIf" value="IdFraud">'+
                '</label>'+
                '</div>'+
                '<div class="col-md-4 col-xs-6 form-check" id="div_geocheck">'+
                '<label class="form-check-label">Geo Check '+
                '<input class="form-check-input" type="checkbox" id="clientPermGc" value="GeoCheck">'+
                '</label>'+
                '</div>'+
                '<div class="col-md-4 col-xs-6 form-check" id="div_geofraud">'+
                '<label class="form-check-label">Geo Fraud '+
                '<input class="form-check-input" type="checkbox" id="clientPermGf" value="GeoFraud">'+
                '</label>'+
                '</div>'+
                '<div class="col-md-4 col-xs-6 form-check" id="div_address">'+
                '<label class="form-check-label">Address '+
                '<input class="form-check-input" type="checkbox" id="clientPermAdd" value="Address">'+
                '</label>'+
                '</div>');
            $.each( perm, function( index, value ){
                //$('#show').append(index+' '+ value);
                if(value == 'RiskScore'){
                    $('#div_risk').empty();
                    $('#div_risk').append(''+
                        '<label class="form-check-label">Risk Score '+
                        '<input class="form-check-input" type="checkbox" id="clientPermRisk" value="RiskScore" checked>'+
                        '</label>');
                 }
                if(value == 'IdCheck'){
                    $('#div_idcheck').empty();
                    $('#div_idcheck').append(''+
                        '<label class="form-check-label">Id Check '+
                        '<input class="form-check-input" type="checkbox" id="clientPermIc" value="IdCheck" checked>'+
                        '</label>');
                 }
                if(value == 'IdFraud'){
                    $('#div_idfraud').empty();
                    $('#div_idfraud').append(''+
                        '<label class="form-check-label">Id Fraud '+
                        '<input class="form-check-input" type="checkbox" id="clientPermIf" value="IdFraud" checked>'+
                        '</label>');
                }
                if(value == 'GeoCheck'){
                    $('#div_geocheck').empty();
                    $('#div_geocheck').append(''+
                        '<label class="form-check-label">Geo Check '+
                        '<input class="form-check-input" type="checkbox" id="clientPermGc" value="GeoCheck" checked>'+
                        '</label>');
                }
                if(value == 'GeoFraud'){
                    $('#div_geofraud').empty();
                    $('#div_geofraud').append(''+
                        '<label class="form-check-label">Geo Fraud '+
                        '<input class="form-check-input" type="checkbox" id="clientPermGf" value="GeoFraud" checked>'+
                        '</label>');
                }
                if(value == 'Address'){
                    $('#div_address').empty();
                    $('#div_address').append(''+
                        '<label class="form-check-label">Address '+
                        '<input class="form-check-input" type="checkbox" id="clientPermAdd" value="Address" checked>'+
                        '</label>');
                }
            });
            $('.modal-footer').append('<button id="delContactButton" type="button" class="btn btn-secondary" onclick="javascript:editContact()">Modificar</button>');
        }
    });
}