function validateJoinForm(){
    checkEmail('#email');
    checkPass('#password');
    checkInput('#nombre');
    checkInput('#apellidos');
    checkDoc('#doc');
    checkTelf('#telefono');
    checkInput('#nombre_emp');
    checkInput('#direccion');
}
var token = $('meta[name="csrf-token"]').attr('content');
$(document).ready(function(){
    $('#boton_join').on('click',function(){
        // valForm = [];
        // var validateForm = 1;
        // validateJoinForm();
        // for (var i = 0; i < valForm.length; i++){
        //     if(valForm[i][1] == 0){
        //         validateForm = 0;
        //         return false;
        //     }
        //     else{
        //     }
        // }
        // if(validateForm = 0){
            var send = {
                '_token': token,
                'params': {
                    'email': $('#email').val(),
                    'password': $('#password').val(),
                    // 'cpassword': $('#password').val(),
                    'nombre_emp': $('#nombre_emp').val(),
                    'nombre': $('#nombre').val(),
                    'apellidos': $('#apellidos').val(),
                    'doc': $('#doc').val(),
                    'direccion': $('#direccion').val(),
                    'telefono': $('#telefono').val(),
                    'productos': $("#producto").val(),
                }
            };
            $.ajax({
                data: send,
                url: '/admin/clients/add',
                type: 'POST',
                dataType: 'json',
                success: function (result) {
                    $('#loadingModal').modal('hide');
                    if(result['status'] == 200){
                        alert('Cliente creado con éxito');
                    }else{
                        alert(result['data']);
                    }
                    /*if (result['url']) {
                     window.location.href = "http://hocelot.com/app/" + result['url'];
                     }
                     else {
                     $('#loadingModal').modal('hide');
                     $('#alert_content').html(alert_div + result + '</div>');
                     var item = $('#msg_login');
                     item.addClass('alert-danger');
                     item.show();
                     }*/
                },
                error: function (e) {
                      $('#loadingModal').modal('hide');
                    /* $('#alert_content').html(alert_div + '<strong>Error en el envio de datos!</strong>' + '</div>');
                     var item = $('#msg_login');
                     item.addClass('alert-danger');
                     item.show();*/
                    alert(e);
                },
                beforeSend: function () {
                      $('#loadingModal').modal('show');
                }
            });
        // }
        // else{
        //
        // }
    });
});
