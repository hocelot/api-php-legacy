var valForm = [];
/*MENSAJES ALERTA DC*/
function checkEmail(id) {
    $(id).removeClass('caja_focus');
    var ver = $(id).val();
    var idMens = 'mens_info_' + $(id).attr("id");
    var patt = /([\w-\.]+)@((?:[\w]+\.)+)([a-zA-Z]{2,4})$/i;
    if(ver == ''){
        if ($('#'+idMens).length > 0) {
            $('#'+idMens).remove();
            $(id).removeClass('caja_error');
        }
        //console.log($(id).attr("id")+ ' que no entra');
        $(id).addClass('caja_error');
        //$(id).after('<div id="'+idMens+'" class="mens_error"><!--<!--<img src="/assets/img/demo/icono_ERROR.png">-->-->* Campo obligatorio</div>');
        validarForm($(id).attr("id"), 0);
    }
    else {
        if (patt.test(ver)){
            if ($('#'+idMens).length > 0) {
                $('#'+idMens).remove();
            }
            //$(id).after('<div id="'+idMens+'" class="mens_info">Email válido</div>');
            validarForm($(id).attr("id"), 1);
            //console.log($(id).attr("id")+ ' que si que entra');
            $(id).after('<div id="'+idMens+'" class="mens_ok"><img src="/assets/img/new-api/path-2-copy-4.png"/></div>');
            $(id).removeClass('caja_error');
        }
        else{
            if ($('#'+idMens).length > 0) {
                $('#'+idMens).remove();
                $(id).removeClass('caja_error');
            }
            //$(id).after('<div id="'+idMens+'" class="mens_error"><!--<img src="/assets/img/demo/icono_ERROR.png">-->Introduce una dirección de email válida.</div>');
            $(id).addClass('caja_error');
            validarForm($(id).attr("id"), 0);
        }
    }
};
function checkPass(id) {
    $(id).removeClass('caja_focus');
    var ver = $(id).val();
    var patt = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/i;
    var idMens = 'mens_info_' + $(id).attr("id");
    if (ver == '') {
        if ($('#' + idMens).length > 0) {
            $('#' + idMens).remove();
            $(id).removeClass('caja_error');
        }
        $(id).addClass('caja_error');
        //$(id).after('<div id="' + idMens + '" class="mens_error"><!--<img src="../../../app/assets/img/icono_alerta_formulario-01.png">-->* Campo obligatorio</div>');
        validarForm($(id).attr("id"), 0);
    }
    else {
        if (patt.test(ver)) {
            if ($('#' + idMens).length > 0) {
                $('#' + idMens).remove();
            }
            $(id).after('<div id="'+idMens+'" class="mens_ok"><img src="/assets/img/new-api/path-2-copy-4.png"/></div>');
            $(id).removeClass('caja_error');
            validarForm($(id).attr("id"), 1);
        }
        else {
            if ($('#' + idMens).length > 0) {
                $('#' + idMens).remove();
                $(id).removeClass('caja_error');
            }
            //$(id).after('<div id="' + idMens + '" class="mens_error"><!--<img src="/assets/img/demo/icono_ERROR.png">-->Introduce una contraseña segura: Al menos 8 caracteres que contengan letras mayúsculas, minúsculas y números.</div>');
            $(id).addClass('caja_error');
            validarForm($(id).attr("id"), 0);
        }
    }
};
function checkInput(id) {
    $(id).removeClass('caja_focus');
    var ver = $(id).val();
    var patt = /^[0-9a-zA-Z\/ñÑáéíóúÁÉÍÓÚ\., ']{3,}$/i;
    //var patt = /^[a-zA-ZZñÑáéíóúÁÉÍÓÚ ']{3,}$/i;
    var idMens = 'mens_info_' + $(id).attr("id");
    if(ver == ''){
        if ($('#'+idMens).length > 0) {
            $('#'+idMens).remove();
            $(id).removeClass('caja_error');
        }
        $(id).addClass('caja_error');
        //$(id).after('<div id="'+idMens+'" class="mens_error"><!--<!--<img src="/assets/img/demo/icono_ERROR.png">-->-->* Campo obligatorio</div>');
        validarForm($(id).attr("id"), 0);
    }
    else {
        if (patt.test(ver)){
            if ($('#'+idMens).length > 0) {
                $('#'+idMens).remove();
            }
            $(id).after('<div id="'+idMens+'" class="mens_ok"><img src="/assets/img/new-api/path-2-copy-4.png"/></div>');
            $(id).removeClass('caja_error');
            validarForm($(id).attr("id"), 1);
        }
        else{
            if ($('#'+idMens).length > 0) {
                $('#'+idMens).remove();
                $(id).removeClass('caja_error');
            }
            $(id).addClass('caja_error');
            //$(id).after('<div id="'+idMens+'" class="mens_error"><!--<img src="/assets/img/demo/icono_ERROR.png">-->*Campo obligatorio.</div>');
            validarForm($(id).attr("id"), 0);
        }
    }
};
function checkTextArea(id){
    $(id).removeClass('caja_focus');
    var ver = $(id).val();
    var patt = /^[0-9a-zA-Z\/ñÑáéíóúÁÉÍÓÚ\.\n, ']{3,}$/i;
    //var patt = /^[a-zA-ZZñÑáéíóúÁÉÍÓÚ ']{3,}$/i;
    var idMens = 'mens_info_' + $(id).attr("id");
    if(ver == ''){
        if ($('#'+idMens).length > 0) {
            $('#'+idMens).remove();
            $(id).removeClass('caja_error');
        }
        $(id).addClass('caja_error');
        //$(id).after('<div id="'+idMens+'" class="mens_error"><!--<!--<img src="/assets/img/demo/icono_ERROR.png">-->-->* Campo obligatorio</div>');
        validarForm($(id).attr("id"), 0);
    }
    else {
        if (patt.test(ver)){
            if ($('#'+idMens).length > 0) {
                $('#'+idMens).remove();
            }
            $(id).after('<div id="'+idMens+'" class="mens_ok"><img src="/assets/img/new-api/path-2-copy-4.png"/></div>');
            $(id).removeClass('caja_error');
            validarForm($(id).attr("id"), 1);
        }
        else{
            if ($('#'+idMens).length > 0) {
                $('#'+idMens).remove();
                $(id).removeClass('caja_error');
            }
            $(id).addClass('caja_error');
            //$(id).after('<div id="'+idMens+'" class="mens_error"><!--<img src="/assets/img/demo/icono_ERROR.png">-->*Campo obligatorio.</div>');
            validarForm($(id).attr("id"), 0);
        }
    }
}
function checkFech(id) {
    $(id).removeClass('caja_focus');
    var ver = $(id).val();
    // var patt = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
    var patt = /^(0[1-9]|[12][0-9]|3[01])[\/\-](0[1-9]|1[012])[\/\-](19[0-9]\d|200\d|201\d)$/;
    var idMens = 'mens_info_' + $(id).attr("id");
    if(ver == ''){
        if ($('#'+idMens).length > 0) {
            $('#'+idMens).remove();
            $(id).removeClass('caja_error');
        }
        $(id).addClass('caja_error');
        //$(id).after('<div id="'+idMens+'" class="mens_error"><!--<!--<img src="/assets/img/demo/icono_ERROR.png">-->-->* Campo obligatorio</div>');
        validarForm($(id).attr("id"), 0);
    }
    else {
        var first = ver.indexOf('/');
        var last = ver.lastIndexOf('/');
        var dia = ver.substr(0, first);
        var mes = ver.substr(3, 2);
        //var anio = ver.substr(last+1, ver.length-last);
        var anio = ver.substr(6, 4);
        // console.log(dia);
        // console.log(mes);
        // console.log(anio);
        var d = new Date();
        var month = d.getMonth()+1;
        var day = d.getDate();
        var year = d.getFullYear()-18;
        var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];
        if(dia <= monthLength[mes-1]) {
            if (patt.test(ver)) {
                if ($('#' + idMens).length > 0) {
                    $('#' + idMens).remove();
                }
                if (anio > year) {
                    if ($('#' + idMens).length > 0) {
                        $('#' + idMens).remove();
                        $(id).removeClass('caja_error');
                    }
                    $(id).addClass('caja_error');
                    validarForm($(id).attr("id"), 0);
                }
                else if (anio == year) {
                    if (mes > month) {
                        if ($('#' + idMens).length > 0) {
                            $('#' + idMens).remove();
                            $(id).removeClass('caja_error');
                        }
                        $(id).addClass('caja_error');
                        validarForm($(id).attr("id"), 0);
                    }
                    else if (mes == month) {
                        if (dia > day) {
                            if ($('#' + idMens).length > 0) {
                                $('#' + idMens).remove();
                                $(id).removeClass('caja_error');
                            }
                            $(id).addClass('caja_error');
                            validarForm($(id).attr("id"), 0);
                        }
                        else {
                            $(id).after('<div id="' + idMens + '" class="mens_ok"><img src="/assets/img/new-api/path-2-copy-4.png"/></div>');
                            $(id).removeClass('caja_error');
                            validarForm($(id).attr("id"), 1);
                        }
                    }
                    else {
                        $(id).after('<div id="' + idMens + '" class="mens_ok"><img src="/assets/img/new-api/path-2-copy-4.png"/></div>');
                        $(id).removeClass('caja_error');
                        validarForm($(id).attr("id"), 1);
                    }
                } else {
                    $(id).after('<div id="' + idMens + '" class="mens_ok"><img src="/assets/img/new-api/path-2-copy-4.png"/></div>');
                    $(id).removeClass('caja_error');
                    validarForm($(id).attr("id"), 1);
                }
            }
            else {
                if ($('#' + idMens).length > 0) {
                    $('#' + idMens).remove();
                    $(id).removeClass('caja_error');
                }
                $(id).addClass('caja_error');
                validarForm($(id).attr("id"), 0);
            }
        }else{
            if ($('#' + idMens).length > 0) {
                $('#' + idMens).remove();
                $(id).removeClass('caja_error');
            }
            $(id).addClass('caja_error');
            validarForm($(id).attr("id"), 0);
        }

    }
};
function checkDir(id) {
    $(id).removeClass('caja_focus');
    var ver = $(id).val();
    var patt = /^[0-9a-zA-Z\/ñÑáéíóúÁÉÍÓÚ\. ']{3,}$/i;
    var idMens = 'mens_info_' + $(id).attr("id");
    if(ver == ''){
        if ($('#'+idMens).length > 0) {
            $('#'+idMens).remove();
            $(id).removeClass('caja_error');
        }
        $(id).addClass('caja_error');
        //$(id).after('<div id="'+idMens+'" class="mens_error"><!--<!--<img src="/assets/img/demo/icono_ERROR.png">-->-->* Campo obligatorio</div>');
        validarForm($(id).attr("id"), 0);
    }
    else {
        if (patt.test(ver)){
            if ($('#'+idMens).length > 0) {
                $('#'+idMens).remove();
            }
            $(id).after('<div id="'+idMens+'" class="mens_ok"><img src="/assets/img/new-api/path-2-copy-4.png"/></div>');
            $(id).removeClass('caja_error');
            validarForm($(id).attr("id"), 1);
        }
        else{
            if ($('#'+idMens).length > 0) {
                $('#'+idMens).remove();
                $(id).removeClass('caja_error');
            }
            $(id).addClass('caja_error');
            //$(id).after('<div id="'+idMens+'" class="mens_error"><!--<img src="/assets/img/demo/icono_ERROR.png">-->Introduce una contaseña válida.</div>');
            validarForm($(id).attr("id"), 0);
        }
    }
};
function checkTelf(id) {
    $(id).removeClass('caja_focus');
    var ver = $(id).val();
    var idMens = 'mens_info_' + $(id).attr("id");
    var patt = /^[9|8|7|6]\d{8}$/i;
    if(ver == ''){
        if ($('#'+idMens).length > 0) {
            $('#'+idMens).remove();
            $(id).removeClass('caja_error');
        }
        $(id).addClass('caja_error');
        //$(id).after('<div id="'+idMens+'" class="mens_error"><!--<!--<img src="/assets/img/demo/icono_ERROR.png">-->-->* Campo obligatorio</div>');
        validarForm($(id).attr("id"), 0);
    }
    else {
        if (patt.test(ver)){
            if ($('#'+idMens).length > 0) {
                $('#'+idMens).remove();
            }
            $(id).after('<div id="'+idMens+'" class="mens_ok"><img src="/assets/img/new-api/path-2-copy-4.png"/></div>');
            $(id).removeClass('caja_error');
            validarForm($(id).attr("id"), 1);
        }
        else{
            if ($('#'+idMens).length > 0) {
                $('#'+idMens).remove();
                $(id).removeClass('caja_error');
            }
            $(id).addClass('caja_error');
           // $(id).after('<div id="'+idMens+'" class="mens_error"><!--<img src="/assets/img/demo/icono_ERROR.png">-->Formato erróneo, por favor introduce un teléfono válido de 9 dígitos. P.ej: 666001122</div>');
            validarForm($(id).attr("id"), 0);
        }
    }
};
function checkDoc(id) {
    $(id).removeClass('caja_focus');
    //DC VALIDAR DNI-NIE
    dni = $(id).val();
    var idMens = 'mens_info_' + $(id).attr("id");
    var numero;
    var let;
    var letra;
    var letraPrimera;
    var regular = /^(X(-|\.)?0?\d{7}(-|\.)?[A-Z]|[A-Z](-|\.)?\d{7}(-|\.)?[0-9A-Z]|\d{8}(-|\.)?[A-Z])$/;
    var expresion_regular_nie = /^[x-zX-Z]{1}[0-9]{7}[a-zA-Z]{1}$/;
    var expresion_regular_dni = /^\d{8}[a-zA-Z]{1}$/;
    if(dni == ''){
        if ($('#'+idMens).length > 0) {
            $('#'+idMens).remove();
            $(id).removeClass('caja_error');
        }
        $(id).addClass('caja_error');
        //$(id).after('<div id="'+idMens+'" class="mens_error"><!--<!--<img src="/assets/img/demo/icono_ERROR.png">-->-->* Campo obligatorio</div>');
        validarForm($(id).attr("id"), 0);
    }
    else {
        if(expresion_regular_nie.test (dni) == true){
            letraPrimera = dni.substr(0,1);
            letraPrimera = letraPrimera.toUpperCase();
            if (letraPrimera == 'X'){
                numero = dni.substr(1,dni.length-1);
                numero = '0' + numero;
                numero = numero.substr(0,dni.length-1);
                let = dni.substr(dni.length-1,1);
                numero = numero % 23;
                letra='TRWAGMYFPDXBNJZSQVHLCKET';
                letra=letra.substring(numero,numero+1);
                if (letra!=let.toUpperCase()) {
                    //Si la letra falla
                    //alert('KO NIE');
                    if ($('#'+idMens).length > 0) {
                        $('#'+idMens).remove();
                        $(id).removeClass('caja_error');
                    }
                    $(id).addClass('caja_error');
                    //$(id).after('<div id="'+idMens+'" class="mens_error"><!--<img src="/assets/img/demo/icono_ERROR.png">-->NIE no válido</div>');
                    validarForm($(id).attr("id"), 0);
                }
                else{
                    //alert('ok NIE');
                    if ($('#'+idMens).length > 0) {
                        $('#'+idMens).remove();
                    }
                    $(id).removeClass('caja_error');
                    $(id).after('<div id="'+idMens+'" class="mens_ok"><img src="/assets/img/new-api/path-2-copy-4.png"/></div>');
                    validarForm($(id).attr("id"), 1);
                }
            }
            else if (letraPrimera == 'Y'){
                numero = dni.substr(1,dni.length-1);
                numero = '1' + numero;
                numero = numero.substr(0,dni.length-1);
                let = dni.substr(dni.length-1,1);
                numero = numero % 23;
                letra='TRWAGMYFPDXBNJZSQVHLCKET';
                letra=letra.substring(numero,numero+1);
                if (letra!=let.toUpperCase()) {
                    //Si la letra falla
                    //alert('KO NIE');
                    if ($('#'+idMens).length > 0) {
                        $('#'+idMens).remove();
                        $(id).removeClass('caja_error');
                    }
                    $(id).addClass('caja_error');
                    //$(id).after('<div id="'+idMens+'" class="mens_error"><!--<img src="/assets/img/demo/icono_ERROR.png">-->NIE no válido</div>');
                    validarForm($(id).attr("id"), 0);
                }
                else{
                    //alert('ok NIE');
                    if ($('#'+idMens).length > 0) {
                        $('#'+idMens).remove();
                        $(id).removeClass('caja_error');
                    }
                    $(id).removeClass('caja_error');
                    $(id).after('<div id="'+idMens+'" class="mens_ok"><img src="/assets/img/new-api/path-2-copy-4.png"/></div>');
                    validarForm($(id).attr("id"), 1);
                }
            }
            else if (letraPrimera == 'Z'){
                numero = dni.substr(1,dni.length-1);
                numero = '2' + numero;
                numero = numero.substr(0,dni.length-1);
                let = dni.substr(dni.length-1,1);
                numero = numero % 23;
                letra='TRWAGMYFPDXBNJZSQVHLCKET';
                letra=letra.substring(numero,numero+1);
                if (letra!=let.toUpperCase()) {
                    //Si la letra falla
                    //alert('KO NIE');
                    if ($('#'+idMens).length > 0) {
                        $('#'+idMens).remove();
                        $(id).removeClass('caja_error');
                    }
                    $(id).addClass('caja_error');
                    //$(id).after('<div id="'+idMens+'" class="mens_error"><!--<img src="/assets/img/demo/icono_ERROR.png">-->NIE no válido</div>');
                    validarForm($(id).attr("id"), 0);
                }
                else{
                    //alert('ok NIE');
                    if ($('#'+idMens).length > 0) {
                        $('#'+idMens).remove();
                    }
                    $(id).removeClass('caja_error');
                    $(id).after('<div id="'+idMens+'" class="mens_ok"><img src="/assets/img/new-api/path-2-copy-4.png"/></div>');
                    validarForm($(id).attr("id"), 1);
                }
            }
            else {
                //Si la expresion regular falla
                //alert('KO NIE');
                if ($('#'+idMens).length > 0) {
                    $('#'+idMens).remove();
                    $(id).removeClass('caja_error');
                }
                $(id).addClass('caja_error');
                //$(id).after('<div id="'+idMens+'" class="mens_error"><!--<img src="/assets/img/demo/icono_ERROR.png">-->NIE no válido</div>');
                validarForm($(id).attr("id"), 0);
            }
        }
        else if(expresion_regular_dni.test (dni) == true){
            numero = dni.substr(0,dni.length-1);
            let = dni.substr(dni.length-1,1);
            numero = numero % 23;
            letra='TRWAGMYFPDXBNJZSQVHLCKET';
            letra=letra.substring(numero,numero+1);
            if (letra!=let.toUpperCase()) {
                //Si la letra falla
                //alert('KO DNI');
                if ($('#'+idMens).length > 0) {
                    $('#'+idMens).remove();
                    $(id).removeClass('caja_error');
                }
                $(id).addClass('caja_error');
                //$(id).after('<div id="'+idMens+'" class="mens_error"><!--<img src="/assets/img/demo/icono_ERROR.png">-->Formato erróneo, por favor introduce un documento de identidad válido: 8 Números y Letra, sin espacio ni guión. P.ej: 52889884D</div>');
                validarForm($(id).attr("id"), 0);
            }
            else{
                //alert('ok DNI');
                if ($('#'+idMens).length > 0) {
                    $('#'+idMens).remove();
                }
                $(id).removeClass('caja_error');
                $(id).after('<div id="'+idMens+'" class="mens_ok"><img src="/assets/img/new-api/path-2-copy-4.png"/></div>');
                validarForm($(id).attr("id"), 1);
            }
        }
        else if(expresion_regular_dni.test ('0'+dni) == true){
            numero = dni.substr(0,dni.length-1);
            let = dni.substr(dni.length-1,1);
            numero = numero % 23;
            letra='TRWAGMYFPDXBNJZSQVHLCKET';
            letra=letra.substring(numero,numero+1);
            if (letra!=let.toUpperCase()) {
                //Si la letra falla
                //alert('KO DNI');
                if ($('#'+idMens).length > 0) {
                    $('#'+idMens).remove();
                    $(id).removeClass('caja_error');
                }
                $(id).addClass('caja_error');
                //$(id).after('<div id="'+idMens+'" class="mens_error"><!--<img src="/assets/img/demo/icono_ERROR.png">-->Formato erróneo, por favor introduce un documento de identidad válido: 8 Números y Letra, sin espacio ni guión. P.ej: 52889884D</div>');
                validarForm($(id).attr("id"), 0);
            }
            else{
                //alert('ok DNI');
                if ($('#'+idMens).length > 0) {
                    $('#'+idMens).remove();
                }
                $(id).removeClass('caja_error');
                $(id).val('0'+dni)
                $(id).after('<div id="'+idMens+'" class="mens_ok"><img src="/assets/img/new-api/path-2-copy-4.png"/></div>');
                validarForm($(id).attr("id"), 1);
            }
        }
        //CIF
        else if(regular.test (dni) == true){
            var pares = 0;
            var impares = 0;
            var suma;
            var ultima;
            var unumero;
            var uletra = new Array("J", "A", "B", "C", "D", "E", "F", "G", "H", "I");
            var xxx;
            texto = dni.toUpperCase();
            /*if (!regular.exec(texto)) {
             alert('KO CIF');
             }	*/
            ultima = texto.substr(8,1);
            for (var cont = 1 ; cont < 7 ; cont ++){
                xxx = (2 * parseInt(texto.substr(cont++,1))).toString() + "0";
                impares += parseInt(xxx.substr(0,1)) + parseInt(xxx.substr(1,1));
                pares += parseInt(texto.substr(cont,1));
            }
            xxx = (2 * parseInt(texto.substr(cont,1))).toString() + "0";
            impares += parseInt(xxx.substr(0,1)) + parseInt(xxx.substr(1,1));
            suma = (pares + impares).toString();
            unumero = parseInt(suma.substr(suma.length - 1, 1));
            unumero = (10 - unumero).toString();
            if(unumero == 10) unumero = 0;
            if ((ultima == unumero) ||  (ultima == uletra[unumero])){
                //alert('ko CIF');
                if ($('#'+idMens).length > 0) {
                    $('#'+idMens).remove();
                }
                $(id).removeClass('caja_error');
                $(id).after('<div id="'+idMens+'" class="mens_ok"><img src="/assets/img/new-api/path-2-copy-4.png"/></div>');
                validarForm($(id).attr("id"), 1);
            }
            else{
                //alert('ok CIF');
                if ($('#'+idMens).length > 0) {
                    $('#'+idMens).remove();
                    $(id).removeClass('caja_error');
                }
                $(id).addClass('caja_error');
                //$(id).after('<div id="'+idMens+'" class="mens_error"><!--<img src="/assets/img/demo/icono_ERROR.png">-->CIF no válido</div>');
                validarForm($(id).attr("id"), 0);
            }
        }
        else{
            //Si la expresion regular falla
            //alert('KO FALLO EXP REGULAR' + dni );
            if ($('#'+idMens).length > 0) {
                $('#'+idMens).remove();
                $(id).removeClass('caja_error');
            }
            $(id).addClass('caja_error');
            //$(id).after('<div id="'+idMens+'" class="mens_error"><!--<img src="/assets/img/demo/icono_ERROR.png">-->Formato erróneo de documento.</div>');
            validarForm($(id).attr("id"), 0);
        }
    }
};
function checkInt(id) {
    $(id).removeClass('caja_focus');
    var idMens = 'mens_info_' + $(id).attr("id");
    var ver = $(id).val();
    var patt = /^\d{1,}$/i;
    if(ver == ''){
        if ($('#'+idMens).length > 0) {
            $('#'+idMens).remove();
            $(id).removeClass('caja_error');
        }
        $(id).addClass('caja_error');
        //$(id).after('<div id="'+idMens+'" class="mens_error"><!--<!--<img src="/assets/img/demo/icono_ERROR.png">-->-->* Campo obligatorio</div>');
        validarForm($(id).attr("id"), 0);
    }
    else {
        if (patt.test(ver)){
            //alert('ok');
            if ($('#'+idMens).length > 0) {
                $('#'+idMens).remove();
            }
            validarForm($(id).attr("id"), 1);
            $(id).removeClass('caja_error');
            $(id).after('<div id="'+idMens+'" class="mens_ok"><img src="/assets/img/new-api/path-2-copy-4.png"/></div>');
        }
        else{
            if ($('#'+idMens).length > 0) {
                $('#'+idMens).remove();
                $(id).removeClass('caja_error');
            }
            $(id).addClass('caja_error');
            //$(id).after('<div id="'+idMens+'" class="mens_error"><!--<img src="/assets/img/demo/icono_ERROR.png">-->&nbsp;&nbsp;Solo números</div>');
            validarForm($(id).attr("id"), 0);
        }
    }
};
function checkSelSex(id){
    $(id).removeClass('caja_focus');
    //var ver = $(id).dataset.id;
    var ver = $(id);
    //console.log(ver.val());
    var idMens = 'mens_info_' + $(id).attr("id");
    //if(ver.attr('title') == null || ver.attr('title') == '' || ver.prop('title') == 'undefined' || ver.attr('title') == false || ver.attr('title') == 'Sexo') {
    //VALIDACION PARA BAJOS
    if(ver.val() == '00' || ver.val() == '00 ' || ver.val() == '00000'){
        if ($('#'+idMens).length > 0) {
            $('#'+idMens).remove();
        }
        validarForm($(id).attr("id"), 1);
        $(id).parent(this).children(2).children(0).removeClass('caja_error_select');
        $('#place' + $(id).attr("id")).css('display', 'block');
        $(id).after('<div id="'+idMens+'" class="mens_ok_select"><img src="/assets/img/new-api/path-2-copy-4.png"/></div>');
    }else if(ver.val() == null || ver.val() == '' || ver.val() == 'undefined' || ver.val() == false){
        //alert(ver.attr('title'));
        //console.log(ver.attr('title') + ' es: ' + ver.attr('id'));
        if ($('#'+idMens).length > 0) {
            $('#'+idMens).remove();
            $(id).parent(this).children(2).children(0).removeClass('caja_error_select');
        }
        $(id).parent(this).children(2).children(0).addClass('caja_error_select');
        //$(id).after('<div id="'+idMens+'" class="mens_error_select"><!--<img src="/assets/img/demo/icono_ERROR.png">--></div>');
        validarForm($(id).attr("id"), 0);
    }
    else {
        if ($('#'+idMens).length > 0) {
            $('#'+idMens).remove();
        }
        validarForm($(id).attr("id"), 1);
        $(id).parent(this).children(2).children(0).removeClass('caja_error_select');
        $('#place' + $(id).attr("id")).css('display', 'block');
        $(id).after('<div id="'+idMens+'" class="mens_ok_select"><img src="/assets/img/new-api/path-2-copy-4.png"/></div>');
    }
}
function checkSelect2(id){
    $(id).removeClass('caja_focus');
    var ver = $(id);
    //alert(ver.val() + ' es: ' + $(id).attr('id'));
    //alert($('#select2-tipoVia-container').attr('title') + 'Value tipo via');//VACIO
    var idMens = 'mens_info_' + $(id).attr("id");
    //VALIDACION PARA BAJOS
    if(ver.attr('title') == '00' || ver.attr('title') == '00 '){
        if ($('#'+idMens).length > 0) {
            $('#'+idMens).remove();
        }
        validarForm($(id).attr("id"), 1);
        $(id).removeClass('caja_error_select');
        $(id).after('<div id="'+idMens+'" class="mens_ok_select"><img src="/assets/img/new-api/path-2-copy-4.png"/></div>');
    }
    else if(ver.attr('title') == null || ver.attr('title') == '' || ver.prop('title') == 'undefined' || ver.attr('title') == false) {
        //alert(ver.attr('title'));
        //console.log(ver.attr('title') + ' es: ' + ver.attr('id'));

        if ($('#'+idMens).length > 0) {
            $('#'+idMens).remove();
            $(id).removeClass('caja_error_select');
        }
        $(id).addClass('caja_error_select');
        //$(id).after('<div id="'+idMens+'" class="mens_error_select"><!--<img src="/assets/img/demo/icono_ERROR.png">-->Elige una opción.</div>');
        validarForm($(id).attr("id"), 0);
    }
    else {
        if ($('#'+idMens).length > 0) {
            $('#'+idMens).remove();
        }
        validarForm($(id).attr("id"), 1);
        $(id).removeClass('caja_error_select');
        $(id).after('<div id="'+idMens+'" class="mens_ok_select"><img src="/assets/img/new-api/path-2-copy-4.png"/></div>');
    }
};
function checkSel(id) {
    $(id).removeClass('caja_focus');
    var ver = $(id).val();
    var idFech = $(id).attr('id');
    var idMens = 'mens_info_' + $(id).attr("id");

    if(ver == null){
        if ($('#'+idMens).length > 0) {
            $('#'+idMens).remove();
            $(id).removeClass('caja_error');
        }
        if (idFech == 'dia_fn' || idFech == 'mes_fn' || idFech == 'year_fn'){
            if($('#fecha_div').hasClass('div_info_sel') == false){
                $('#fecha_div').addClass('div_info_sel');
            }
            $(id).addClass('caja_error');
            // $('#year_fn').after('<div id="'+idMens+'" class="mens_error">* Elige una opción</div>');
            validarForm($(id).attr("id"), 0);
        }
        else{
            if ($('#'+idMens).length > 0) {
                $('#'+idMens).remove();
                $(id).removeClass('caja_error');
            }
            if($('#fecha_div').hasClass('div_info_sel') == false){
                $('#fecha_div').addClass('div_info_sel');
            }
            $(id).addClass('caja_error');
            //$(id).after('<div id="'+idMens+'" class="mens_error">* Elige una opción</div>');
            validarForm($(id).attr("id"), 0);
        }
    }
    else {
        if (idFech == 'dia_fn' || idFech == 'mes_fn' || idFech == 'year_fn'){
            if ($('#'+idMens).length > 0) {
                $('#'+idMens).remove();
                $(id).removeClass('caja_error');
            }
            if($('#fecha_div').hasClass('div_info_sel') == true){
                $('#fecha_div').removeClass('div_info_sel');
            }
            $('#year_fn').after('<div id="'+idMens+'" class="mens_ok_sel"><img src="/assets/img/new-api/path-2-copy-4.png"/></div>');
            $(id).removeClass('caja_error');
            validarForm($(id).attr("id"), 1);
        }
        else{
            if ($('#'+idMens).length > 0) {
                $('#'+idMens).remove();
                $(id).removeClass('caja_error');
            }
            if($('#fecha_div').hasClass('div_info_sel') == true){
                $('#fecha_div').removeClass('div_info_sel');
            }
            $(id).after('<div id="'+idMens+'" class="mens_ok_sel"><img src="/assets/img/new-api/path-2-copy-4.png"/></div>');
            $(id).removeClass('caja_error');
            validarForm($(id).attr("id"), 1);
        }
    }
};
function checkCheckbox(id){
    // $(id).removeClass('caja_focus');
    var ver = $(id);
    var idMens = 'mens_info_' + $(id).attr("id");
    if(ver.is(':checked')) {
        if ($('#'+idMens).length > 0) {
            $('#'+idMens).remove();
        }
        $('#error_'+$(id).attr("id")).html('');
        validarForm($(id).attr("id"), 1);
        // $(id).after('<div id="'+idMens+'" class="mens_ok"><!--<img src="/assets/img/new-api/path-2-copy-4.png"/>--></div>');
    }
    else {
        if ($('#'+idMens).length > 0) {
            $('#'+idMens).remove();
            $(id).removeClass('caja_error');
        }
        //console.log(id);
        //$(id).after('<div id="'+idMens+'" class="mens_error"><!--<img src="/assets/img/demo/icono_ERROR.png">-->Debes aceptar las Condiciones Generales y la Política de Privacidad.</div>');
        $('#error_'+$(id).attr("id")).html('Necesitamos que aceptes los términos y condiciones de uso para continuar ya que has introducido tus datos en nuestro formulario.')
        validarForm($(id).attr("id"), 0);
    }
}
/** MENSAJES DE INFORMACION DC */
function getPlaceMin(id){
    // console.log($(id).val());
    // console.log($(id).attr("id").length);
    if($(id).val().length > 0) {
        $('#place' + $(id).attr("id")).css('display', 'block');
    }else{
        $('#place' + $(id).attr("id")).css('display', 'none');
    }
}
function infoEmail(id){
    var idMens = 'mens_info_' + $(id).attr("id");
    if ($('#'+idMens).length > 0) {
        $('#'+idMens).remove();
        $(id).removeClass('caja_error');
    }
    $(id).addClass('caja_focus');
    //$(id).after('<div id="'+idMens+'" class="mens_info">Introduce tu dirección de correo electrónico.</div>');
}
function infoPass(id) {
    var idMens = 'mens_info_' + $(id).attr("id");
    if ($('#' + idMens).length > 0) {
        $('#' + idMens).remove();
        $(id).removeClass('caja_error');
    }
    $(id).addClass('caja_focus');
    // $(id).after('<div id="' + idMens + '" class="mens_info">Al menos 8 caracteres que contengan mayúsculas, minúsculas y números.</div>');
}
function infoInput(id){
    var idMens = 'mens_info_' + $(id).attr("id");
    var nomInput = $(id).attr('placeholder').toLowerCase();
    if ($('#'+idMens).length > 0) {
        $('#'+idMens).remove();
        $(id).removeClass('caja_error');
    }
    $(id).addClass('caja_focus');
    //$(id).after('<div id="'+idMens+'" class="mens_info">Introduce tu '+ nomInput +'.</div>');
}
function infoTelf(id){
    var idMens = 'mens_info_' + $(id).attr("id");
    if ($('#'+idMens).length > 0) {
        $('#'+idMens).remove();
        $(id).removeClass('caja_error');
    }
    $(id).addClass('caja_focus');
    //$(id).after('<div id="'+idMens+'" class="mens_info">Introduce tu teléfono de contacto.</div>');
}
function infoInt(id){
    var idMens = 'mens_info_' + $(id).attr("id");
    if ($('#'+idMens).length > 0) {
        $('#'+idMens).remove();
        $(id).removeClass('caja_error');
    }
    $(id).addClass('caja_focus');
    //$(id).after('<div id="'+idMens+'" class="mens_info">Sólo números.</div>');
}
function infoDoc(id){
    var idMens = 'mens_info_' + $(id).attr("id");
    if ($('#'+idMens).length > 0) {
        $('#'+idMens).remove();
        $(id).removeClass('caja_error');
    }
    $(id).addClass('caja_focus');
    //$(id).after('<div id="'+idMens+'" class="mens_info">Introduce tu documento de identidad, <br>sin espacio ni guión.</div>');
}
function infoSelect2(id){
    var idMens = 'mens_info_' + $(id).attr("id");
    if ($('#'+idMens).length > 0) {
        $('#'+idMens).remove();
        $(id).removeClass('caja_error');
    }
    $(id).addClass('caja_focus');
    //$(id).after('<div id="'+idMens+'" class="mens_info_select">Elige una opción.</div>');
}
function infoSel(id){
    var idMens = 'mens_info_' + $(id).attr("id");
    var idFech = $(id).attr('id');
    if ($('#'+idMens).length > 0) {
        $('#'+idMens).remove();
        $(id).removeClass('caja_error');
    }else{}
    if (idFech == 'dia_fn' || idFech == 'mes_fn' || idFech == 'year_fn'){
        if($('#fecha_div').hasClass('div_info_sel') == false){
            $('#fecha_div').addClass('div_info_sel');
        }
        //$('#year_fn').after('<div id="'+idMens+'" class="mens_info_sel">Elige una opción.</div>');
    }
    else{
        //$(id).after('<div id="'+idMens+'" class="mens_info_sel">Elige una opción.</div>');
    }
    $(id).addClass('caja_focus');
}
function infoFech(id){
    var idMens = 'mens_info_' + $(id).attr("id");
    if ($('#'+idMens).length > 0) {
        $('#'+idMens).remove();
        $(id).removeClass('caja_error');
    }
    $(id).addClass('caja_focus');
    //$(id).after('<div id="'+idMens+'" class="mens_info">La fecha de nacimiento debe tener el formato dd/mm/aaaa.</div>');
}
function infoDir(id){
    var idMens = 'mens_info_' + $(id).attr("id");
    if ($('#'+idMens).length > 0) {
        $('#'+idMens).remove();
        $(id).removeClass('caja_error');
    }
    $(id).addClass('caja_focus');
    //$(id).after('<div id="'+idMens+'" class="mens_info">Introduce la dirección completa.</div>');
}
/*CONTROL FORMULARIO*/
function validarForm(id, value){
    valForm.push([id, value]);
    for (var i = 0; i < valForm.length; i++){
        CallFunction(valForm[i][0], valForm[i][1]);
    }
};
function CallFunction(id, value){
    //Debug function DC
    // console.log("ID: " + id + ", value: " + value);
}
//AÑADiMOS LA VALIDACION CUANDO CAMBIE EL SELECT2
$(document).ready(function(){
    /*HACEMOS SLEEP DE 10 MS, SINO NO LE DA TIEMPO A COGER EL TITLE.. */
    $("#tipoVia").change(function() {
        setTimeout(function(){
            checkSelect2('#select2-tipoVia-container');
        },10);
    });
    $("#via").change(function() {
        setTimeout(function(){
            checkSelect2('#select2-via-container');
        },10);
    });
    $("#numero").change(function() {
        setTimeout(function(){
            checkSelect2('#select2-numero-container');
        },10);
    });
    $("#letra").change(function() {
        setTimeout(function(){
            checkSelect2('#select2-letra-container');
        },10);
    });
    $("#km").change(function() {
        setTimeout(function(){
            checkSelect2('#select2-km-container');
        },10);
    });
    $("#bloque").change(function() {
        setTimeout(function(){
            checkSelect2('#select2-bloque-container');
        },10);
    });
    $("#esc").change(function() {
        setTimeout(function(){
            checkSelect2('#select2-esc-container');
        },10);
    });
    $("#planta").change(function() {
        setTimeout(function(){
            checkSelect2('#select2-planta-container');
        },10);
    });
    $("#puerta").change(function() {
        setTimeout(function(){
            checkSelect2('#select2-puerta-container');
        },10);
    });
});