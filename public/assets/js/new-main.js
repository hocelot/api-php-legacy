//SLIDER HOME ESCRITORIO DC
var $item = $('.carousel .item');
if($(window).width() < '765') {
    var $wHeight = $(window).height() + $('.navbar-hocelot-mobile').height()+10;
    $('#mycarousel').css('top', '0px');
    $('#mycarousel').css('margin-bottom', '-60px');
}else{
    var $wHeight = $(window).height() - $('.navbar-hocelot').height() - 1;
    $('#mycarousel').css('top', '-25px');
    $('#mycarousel').css('margin-bottom', '0px');
}
$item.eq(0).addClass('active');
$item.height($wHeight);
$item.addClass('full-screen');
$('.carousel img').each(function() {
    var $src = $(this).attr('src');
    var $color = $(this).attr('data-color');
    $(this).parent().css({
        'background-image' : 'url(' + $src + ')',
        'background-color' : $color
    });
    $(this).remove();
});
$(window).on('resize', function (){
    if($(window).width() < '765') {
        var $wHeight = $(window).height() + $('.navbar-hocelot-mobile').height()+10;
        $('#mycarousel').css('top', '0px');
        $('#mycarousel').css('margin-bottom', '-60px');
    }else{
        var $wHeight = $(window).height() - $('.navbar-hocelot').height() - 1;
        $('#mycarousel').css('top', '-25px');
        $('#mycarousel').css('margin-bottom', '0px');
    }
    $item.height($wHeight);
});
$('.carousel').carousel({
    interval: 6000,
    pause: "false"
});
//FIN SLIDER


//SCROLL MENÚ HOME
var didScroll;
var lastScrollTop = 0;
var delta = 5;
var navbarHeight = $('.header-fixed').outerHeight();
$(window).scroll(function(event){
    didScroll = true;
});
setInterval(function() {
    if (didScroll) {
        hasScrolled();
        didScroll = false;
    }
}, 250);
function hasScrolled() {
    var st = $(this).scrollTop();
    // Make sure they scroll more than delta
    if(Math.abs(lastScrollTop - st) <= delta)
        return;
    // If they scrolled down and are past the navbar, add class .nav-up.
    // This is necessary so you never see what is "behind" the navbar.
    if (st > lastScrollTop && st > navbarHeight){
        // Scroll Down
        $('.header-fixed').removeClass('nav-down').addClass('nav-up');

    } else {
        // Scroll Up
        //CODIGO ANTIGUO COMENTADO DC
        if(st + $(window).height() < $(document).height()) {
        //if(st < $(document).height()) {
            if($('#mycarousel').height() == 'undefined' || $('#mycarousel').height() == 'null'){
                var carousel = 0;
            }else{
                var carousel = $('#mycarousel').height();
            }
            // console.log(carousel);
            // console.log($('#mycarousel').height());
            // console.log($(window).scrollTop());
            if($(window).scrollTop() > carousel+$('.navbar-hocelot').height()){
                $('.header-fixed').removeClass('nav-up').addClass('nav-down');
            }else {
                $('.header-fixed').removeClass('nav-down').addClass('nav-up');
            }

        }
    }
    lastScrollTop = st;
}

$(document).ready(function () {
   $('#buttonRespNav').on("click", function () {
       if($('#buttonRespNav').hasClass("glyphicon-menu-hamburger")){
           $('#buttonRespNav').removeClass('glyphicon-menu-hamburger');
           $('#buttonRespNav').addClass('glyphicon-remove');
       }else{
           $('#buttonRespNav').removeClass('glyphicon-remove');
           $('#buttonRespNav').addClass('glyphicon-menu-hamburger');
       }
   });
   $('#gotTop').on("click", function () {
        $("html, body").animate({ scrollTop: 0 }, "slow");
   });
});
