#¿Que hay que cambiar?
Antes de empezar debemos saber que el código de este repositorio es una copia del código que está en el entorno de pro. Por lo tanto hay que tener cuidado con los despliegues y hay que prestar mucha atención a la IP's que ponemos siempre teniendo en cuenta a que entorno vamos a hacer el despliegue.
##0. ¿Dónde está el código?
Para empezar debemos saber que hay dos entornos dev y pro y están en gcloud como:

| Nombre Instancia  | IP Interna  |   IP Externa   |
| :---: | :---: | :---: |
| hocelot-env-dev   | 10.132.0.8  | 35.190.218.98  |
| hocelot-env-pro   | 10.132.0.9  | 35.195.235.236 |

El código está localizado en la siguiente ruta, en cualquiera de los entornos:
~~~
$ cd /var/www/api
~~~

##1. Bases de datos
Las configuraciones de las bases de datos están en:
~~~
$ config/database.php
~~~

Que contendrá algo como esto:
~~~
return [
    'default' => 'apidb',
    'fetch' => PDO::FETCH_CLASS,
    'connections' => [
        'oauth' => [
            'driver' => 'pgsql',
            'host' => '192.168.82.10', //IP de la bd
            'database' => 'apioauth', //Nombre de la bd
            'username' => 'postgres', //Usuario de la bd
            'password' => '#QwEr1234', //Contraseña de la bd
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'schema' => 'public',
            'strict' => false,
        ],
        'apidb' => [
            'driver' => 'pgsql',
            'host' => '192.168.79.24', //IP de la bd
            'database' => 'apidb', //Nombre de la bd
            'username' => 'postgres', //Usuario de la bd
            'password' => '#QwEr1234', //Contraseña de la bd
            'charset' => 'utf8',
            'collation' => 'utf8_general_ci',
            'prefix' => '',
            'schema' => 'public',
            'strict' => false,
        ],
        'catastro' => [
            'driver' => 'pgsql',
            'host' => '192.168.82.10', //IP de la bd
            'database' => 'catastro', //Nombre de la bd
            'username' => 'postgres', //Usuario de la bd
            'password' => '#QwEr1234', //Contraseña de la bd
            'prefix' => '',
            'charset' => 'utf8',
            'collation' => 'utf8_general_ci',
            'schema' => 'public',
            'strict' => false,
        ]

];
~~~

He añadido comentarios a los valores que únicamente se deberían de cambiar.

##2. ElasticSearch

La configuración del elastic no esta externalizada por lo tanto deberémos de ir a:

~~~
$ app/Http/Models/GeoCheck/GeoCheckModel.php
~~~

Aquí tendremos que cambiar del primer método lo siguiente:

~~~
$client = new \Elastica\Client(array(
            'host' => '192.168.82.10',
            'port' => 9200
        ));
~~~

Lo cambiamos por:

~~~
$authHeader = 'Basic '.base64_encode('elastic:EfUwpFQnIU6HaiMDu1m6UIdo').'==';
$client = new \Elastica\Client(array(
            'host' => 'e26e312b079fec238fb0364c6efe6ee0.europe-west1.gcp.cloud.es.io',
            'port' => 9243,
            'transport' => 'https',
            'headers' => array(
                'Authorization' => $authHeader
            )
        ));
~~~

Eso debería de ser todo los cambios para tener el elastic apuntando a google.

##3. ¿Dónde está la url de ZUUL y la de los WSDL?
Las urls de zuul y de los WSDL, lo podrémos encontrar en el fichero .env que se sitúa en:
~~~
$ .env
~~~

Aquí no encontramos con:

~~~
ZUUL_URL="http://10.132.0.5:30652"
WSDL_clientes = "http://10.132.0.5:30652/powerbuilder/ws/pwbb_clientes/n_pwbb_clientes.asmx?WSDL"
WSDL_admin = "http://10.132.0.5:30652/powerbuilder/ws/pwbb_admin/n_pwbb_admin.asmx?WSDL"
~~~

##4. ¿Cómo subo los cambios?.
Si hacemos los cambios en local,
aquí viene mas o menos lo más complicado como no hay pipelines, hay que desplegar vía scp.
En el directorio ci/scripts nos encontraremos dos ficheros staggin-dev y staggin-pro. Como podemos observar uno es para pro y otro para dev.
Si los abrimos no encontramos tres scp para subir los ficheros comentados con anterioridad, el del .env está comentado porqué este en principio no se debería tocar.
Además para subir primero necesitariamos modificar los permisos del ci-php-key.pem que encontrarémos en la carpeta ci, para ello:
~~~
## Esto solo se tiene que hacer la primera vez.
$ sudo chmod 400 ci/ci-php-key.pem
~~~

Ahora con los permisos correctos, podemos hacer:
~~~
## Para DEV:
$ ./ci/scripts/staggin-dev

## Para PRO:
$ ./ci/scripts/staggin-pro
~~~